// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "orbit/DistantRetrogradeOrbit.hpp"
#include "R3BProblem.hpp"

int main() {
    double m1 = MU_EARTH;
    double m2 = MU_MOON;
    double r12 = DISTANCE_EARTH_MOON;
    double x0 = 1.2;
    double xf = 1.5;
    double dx = 0.001;

    std::ofstream file("dro.txt");
    file.width(18);
    file.precision(12);
    file.setf(std::ios::fixed, std::ios::floatfield);
    if (!file.is_open()) {
        std::cerr << "Cannot create file" << std::endl;
    }       

    time_t* rawtime = new time_t;
    struct tm* timeinfo;
    time(rawtime);
    timeinfo = localtime(rawtime);

    file << "# Example - Distant Retrodrade Orbit around the Moon\n";
    file << "# Computed with R3BP TBX\n";
    file << "# (c) Joris T. Olympio\n";
    file << "# " << asctime(timeinfo) << "\n\n";

    R3bProblem *problem = new R3bProblem(m1, m2, r12);
    double mu_ratio = problem->get_mu_ratio();
    file << "mu_ratio = " << mu_ratio << "\n";
    file << "DU = " << problem->getNormalisationForDistance() << "m\n";
    file << "TU = " << problem->getNormalisationForTime() << "s\n\n";

    // constructing orbit
    DistantRetrogradeOrbit *distantRetrodOrbit = new DistantRetrogradeOrbit(mu_ratio, PrimaryPointId::P2);

    file << "# Family: Ax in [" << x0 << ", " << xf << "]\n";
    file << "#        Period[TU]          X-Position           Y-Position            Z-Position             X-Velocity            Y-Velocity             Z-Velocity      Stability Index\n";
    OrbitSolution initialGuess;
    bool found = false;
    for (double x = x0; x <= xf; x += dx) {
        OrbitSolution droSolution;
        if (!found) {
            droSolution = distantRetrodOrbit->find(x, true, 20, 1e-6);            
        }
        else {
            droSolution = distantRetrodOrbit->find_by_continuation(x, initialGuess, false, 100, 1e-10);
        }
        found = droSolution.is_valid;

        if (found) {
            initialGuess = droSolution;
            file << " " << droSolution.period << " " << droSolution.r0.transpose() << "  " << droSolution.v0.transpose() << "  " << droSolution.get_stability_index() << "\n";
        }
        else {
            file << "# Failed continuing the orbit family\n";
            break;
        }
    }
    file << "\n";

    file.close();
    delete problem;
    delete distantRetrodOrbit;
    return 0;
}


