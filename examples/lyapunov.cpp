// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "orbit/LyapunovOrbit.hpp"
#include "R3BProblem.hpp"

int main() {
    double m1 = MU_EARTH;
    double m2 = MU_MOON;
    double r12 = DISTANCE_EARTH_MOON;
    double x = 1.2;

    std::ofstream file("lyapunov.txt");
    file.width(15);
    file.precision(12);
    file.setf(std::ios::fixed, std::ios::floatfield);
    if (!file.is_open()) {
        std::cerr << "Cannot create file" << std::endl;
    }       

    time_t* rawtime = new time_t;
    struct tm* timeinfo;
    time(rawtime);
    timeinfo = localtime(rawtime);

    file << "# Example - Planar Lyapunov Orbit around the L2 in the Earth-Moon system\n";
    file << "# Computed with R3BP TBX\n";
    file << "# (c) Joris T. Olympio\n";
    file << "# " << asctime(timeinfo) << "\n\n";

    R3bProblem *problem = new R3bProblem(m1, m2, r12);
    double mu_ratio = problem->get_mu_ratio();
    file << "mu_ratio = " << mu_ratio << "\n";
    file << "DU = " << problem->getNormalisationForDistance() << "m\n";
    file << "TU = " << problem->getNormalisationForTime() << "s\n\n";

    // selecting L1
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    file << "Selecting Libration Point" << librationPoint.id << "\n";
    file << "  Pos(DU) = " << librationPoint.L[0] << ", " << librationPoint.L[1] << ", " << librationPoint.L[2] << "\n\n";

    // constructing orbit
    LyapunovOrbit* lyapunov = new LyapunovOrbit(librationPoint, LyapunovOrbitFamilyType::PLANAR);
    OrbitSolution lyapunovSolution = lyapunov->find(x, true, 50, 1e-6);

    file << "T = " << lyapunovSolution.period << " TU\n";
    file << "Orbit initial conditions: \n";
    file << "  pos(DU) = " << lyapunovSolution.r0.transpose() << "\n";
    file << "  vel(DU/TU) = " << lyapunovSolution.v0.transpose() << "\n\n";

    file << "Monodromy matrix\n";
    //file.width(18);
    //file.precision(12);
    Matrix6 monodMatrix = lyapunovSolution.getMonodromyMatrix();
    file << monodMatrix << "\n\n";
    file.width(15);
    file.precision(12);

    // compute trajectory in ECI
    std::vector<Vector7> states = lyapunovSolution.propagate(new double[2]{0, 4 * lyapunovSolution.period}, 60);
    //get_ephemeris_file(std::string filename)

    std::vector<Vector7> statesInEci = lyapunovSolution.propagateToFixedFrame(problem, new double[2]{0, 4 * lyapunovSolution.period}, 60.);

    // compute distance to Earth for each point along the DRO
    Vector3 primaryPosition = problem->get_primary_point(P2);
    double maxDist = 0, minDist = 1e20;
    for (Vector7 state : states) {
        double dist = (state.head(3) - primaryPosition).norm();
        maxDist = std::max(dist, maxDist);
        minDist = std::min(dist, minDist);
    }
    file << "Moon's position: " << primaryPosition.transpose() << " DU\n";
    file << "Distance to Moon:\n";
    file << "  Max distance: " << maxDist << " DU\n";
    file << "  Min distance: " << minDist << " DU\n\n";

    file.close();
    delete problem;
    delete lyapunov;
    return 0;
}


