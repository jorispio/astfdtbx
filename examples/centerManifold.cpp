// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <iomanip>  // setprecision
#include <iostream>

#include "orbit/HaloOrbit.hpp"
#include "Manifold.hpp"
#include "R3BProblem.hpp"

using std::scientific;
using std::setprecision;
using std::showpoint;

void print_vectors(std::vector<VectorXcd> &vec) {
    for (int idx=0; idx < 6; ++idx) {
        for(std::vector<VectorXcd>::iterator v = vec.begin(); v != vec.end(); ++v) {
            std::cout  << "    " << (*v)(idx);
        }
        std::cout << std::endl;
    }
}

int main() {
    double m1 = MU_EARTH;
    double m2 = MU_MOON;
    double r12 = DISTANCE_EARTH_MOON;

    R3bProblem *problem = new R3bProblem(m1, m2, r12);
    double mu_ratio = problem->get_mu_ratio();
    printf("mu_ratio = %12f\n", mu_ratio);

    double L[5][2];
    double Cj[5];
    problem->get_libration_points(L, Cj);
    for (int idx=0; idx < 5; ++idx) {
        printf("L_%d : [%12.6f %12.6f %12.6f]  Cj = %12.6f\n", idx + 1, L[idx][0], L[idx][1], 0., Cj[idx]);
    }
    printf("\n");

    // selecting L1
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    printf("Selecting Libration Point%d. Pos = %f %f %f\n",
                librationPoint.id, librationPoint.L[0], librationPoint.L[1], librationPoint.L[2]);

    std::vector<std::complex<double> > sn, un, cn;
    std::vector<VectorXcd> Ws, Wu, Wc;
    EigenSpace *space = new EigenSpace();
    space->getManifolds(librationPoint.get_state(), mu_ratio,
                                sn, Ws,
                                un, Wu,
                                cn, Wc);

    std::cout << scientific;
    std::cout << setprecision(5);
    std::cout << showpoint;

    printf("Center Manifold:\n");
    print_vectors(Wc);

    printf("Stable Manifold:\n");
    print_vectors(Ws);

    printf("Unstable Manifold:\n");
    print_vectors(Wu);

    printf("Center frequencies:\n");
    int ival = 1;
    for(std::vector<std::complex<double>>::iterator c = cn.begin(); c != cn.end(); ++c) {
        std::cout << "  eigen value " << ival << ": " << *c << std::endl;
        ++ival;
    }

    delete space;
    delete problem;

    return 0;
}