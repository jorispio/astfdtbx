// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <fstream>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#include "orbit/TwoBodyOrbit.hpp"
#include "R3BProblem.hpp"
#include "lowthrust/R3bpLowThrustProblem.hpp"
#include "lowthrust/TrajectoryPropageRtbpWithCostate.hpp"

int main() {
    double mu1 = MU_EARTH;
    double mu2 = MU_MOON;
    double r12 = DISTANCE_EARTH_MOON;
    double x = 1.2;

    R3bProblem *problem = new R3bProblem(mu1, mu2, r12);
    double mu_ratio = problem->get_mu_ratio();
    std::cout << "mu_ratio=" << mu_ratio << std::endl;
    
    double t0 = 0;
    double tf = 10 * 86400 / problem->getNormalisationForTime();
    Vector3d r1, v1; 
    double m1 = 1;
    
    // propulsion
    ThrusterData thrusterData;
    thrusterData.PropType = PROPULSION_TYPE::PROP_NEP; // propulsion type
    thrusterData.Fth = 0.02;            // nominal thrust amplitude
    thrusterData.g0Isp = g0 * 1000;     // exhaust velocity    
    thrusterData.eff = 1;               // efficiency
	thrusterData.adim_m = 1;
	thrusterData.adim_ms = 1;

    // constructing orbits
    //TwoBodyOrbit orbit_earth(mu_ratio, PrimaryPointId::P1);
    r1 << -0.02, 0, 0;
    v1 << 0, 7, 0;   

    //
    std::cout << "Solving" << std::endl;
    R3bpLowThrustProblem *ltproblem = new R3bpLowThrustProblem(mu_ratio, thrusterData);
    Vector7 lambda;
    lambda << 1, 0, 0, 1, 0, 0, 0.1;
    //sol = ltproblem->solve();

    // 
    std::cout << "Propagating" << std::endl;
    TrajectoryPropageRtbpWithCostate *propagator = new TrajectoryPropageRtbpWithCostate(mu1, mu2, 0., thrusterData);
    
    std::vector<double> trajectoryTime;
    std::vector<Vector7> trajectoryState;
    double finalTime, mf;
    Vector3d rf, vf;
    int res = propagator->getTrajectory(r1, v1, m1,
                      lambda,
                      t0, tf, -1,
                      /* OUTPUTS */
                      trajectoryTime, trajectoryState,
                      finalTime,
                      rf, vf, mf);
                      
    delete problem;
    delete propagator;

    std::cout << "END" << std::endl;
    return 0;
}


