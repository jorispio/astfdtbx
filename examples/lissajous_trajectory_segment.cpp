// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "HelioLib/Core.hpp"

#include "orbit/HaloOrbit.hpp"
#include "lissajous/MultiPointLissajousOrbit.hpp"
#include "R3BProblem.hpp"
#include "segments/Segments.hpp"

int main() {
    double m1 = MU_EARTH;
    double m2 = MU_MOON;
    double r12 = DISTANCE_EARTH_MOON;
    double Az = 0.02;

    R3bProblem *problem = new R3bProblem(m1, m2, r12);
    double mu_ratio = problem->get_mu_ratio();
    printf("mu_ratio = %12f\n", mu_ratio);

    double L[5][2];
    double Cj[5];
    problem->get_libration_points(L, Cj);
    for (int idx=0; idx < 5; ++idx) {
        printf("L_%d : [%12.6f %12.6f %12.6f]  Cj = %12.6f\n", idx + 1, L[idx][0], L[idx][1], 0., Cj[idx]);
    }
    printf("\n");

    // selecting L1
    LibrationPointProperty libration_point = problem->get_libration_point_info(LibrationPointId::L1);

    // constructing Halo orbit
    HaloOrbit *haloOrbit = new HaloOrbit(libration_point, NORTHERN);
    //OrbitSolution haloOrbitSolution = haloOrbit->find(Az, true, 10, 1e-6);
    haloOrbit->set_parameter(Az, 0);

    int n_segments = 4;
    Lissajous *lissajous = new Lissajous(libration_point);

    printf("Segments list...\n");
    std::vector<Segment> segments = lissajous->get_analytical_segments(haloOrbit, n_segments);
    int idx = 0;
    for (std::vector<Segment>::iterator segment = segments.begin(); segment != segments.end(); ++segment) {
        std::cout << "  segment " << ++idx << " :" << (*segment).ri.transpose() << "  dt=" << (*segment).get_tof() << std::endl;
    }

    printf("Optimising...\n");
    std::vector<Segment> segments_solution = lissajous->find(segments, true, 10);

    printf("Results...\n");
    std::vector<Vector3> dvs = Segments(segments_solution).compute_dv();
    for (std::vector<Vector3>::iterator dv = dvs.begin(); dv != dvs.end(); ++dv) {
        std::cout << "dv=" << (*dv).transpose() << std::endl;
    }

    delete problem;
    delete haloOrbit;
    return 0;
}


