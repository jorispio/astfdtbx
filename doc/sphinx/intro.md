[![made-with-sphinx-doc](https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg)](https://www.sphinx-doc.org/) 
[![docs](https://img.shields.io/badge/docs-v0.1.0-green.svg)](https://jorispio.gitlab.io/astftbx)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 
[![DOI](https://img.shields.io/badge/DOI-10.5281/zenodo.3884255-orange.svg)](https://doi.org/10.5281/zenodo.3884255)

Introduction
============

![image](_static/Figure_4.png)

*Family of Halo orbits around L1 in the Earth-Moon system*



## Preambule
R3BPTBX is initially a toolbox dedicated to the studying of motion around small bodies in the the three-body problem. 
This toolbox was initially developed in Matlab and C++, while studying the motion around a binary asteroid. 
I later decided to fully port the code to C++, and lately to provide a Python wrapper (Py3BP).
The primary objective is to provide an understanding of complex motion in R3BP, and then a tool for computing Lissajous orbits (incl. Lyapunov, Halo) around libration points of a three-body problem (Restricted-, Circular-).

More resources at: [documentation]

[documentation]: https://jorispio.gitlab.io/astfdtbx

About the author: Joris Olympio <a href="http://orcid.org/0000-0003-3848-5587"><img src="https://img.shields.io/badge/orcid-0000--0003--3848--5587-blue.svg"></a>


## License
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 



Copyright (c) 2010-2014 Joris OLYMPIO



## Problems
If you find any issue with the toolbox, please drop me a message. I will do my best to correct any issue promptly.
I also welcome any contribution that can improve the toolbox. 

