Base
======

Problem
------------

.. doxygenclass:: R3bProblem
   :members:

.. doxygenstruct:: LibrationPointProperty
   :members:
   
Space
------------

.. doxygenclass:: EigenSpace
   :members:



Orbits
======

Orbits around Colinear Points
-----------------------------

.. doxygenclass:: HaloOrbit
   :members:

.. doxygenclass:: LyapunovOrbit
   :members:


.. doxygenclass:: LissajousEight
   :members:

.. doxygenclass:: DistantRetrogradeOrbit
   :members:


Orbits around Triangular Points
-------------------------------

.. doxygenclass:: LongShortPeriodicOrbit
   :members:
   

Other orbits in the RTBP
------------------------

.. doxygenclass:: TwoBodyOrbit
   :members:



