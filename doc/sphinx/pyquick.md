
Python Quickstart
=================

Import
```
    from asttbx.main import Problem
    from asttbx.lissajous import HaloOrbit
```

Define your problem filling the structure
```
    problem  = Problem(mu_ratio)
    libration_point = problem.get_libration_point(ast.main.L2)
```
Then construct the orbit
```
    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az)
    ast.propagate(problemDefinition, problemContext, problemSolution, True)
```
And, propagate and plot
```
    xyz_halo = halo_orbit.propagate([0, orbit_period])
    ast.visu.plot_trajectory(xyz_halo)
```    
Please see the examples for details.


