Propagation & Solvers
=====================

Propagation
-----------
.. doxygenclass:: Propagator
   :members:

Solver
-------
.. doxygenclass:: DifferentialCorrector
   :members:

.. doxygenclass:: R3bpLambertProblemSolver
   :members:
   
Frame
-----
.. doxygenclass:: R3bpFrame
   :members:
   

Analysis
========

