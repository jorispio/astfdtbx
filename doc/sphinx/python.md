[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) 
[![made-with-sphinx-doc](https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg)](https://www.sphinx-doc.org/) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 


Py3BP
=========

## Preambule
R3BPTBX is initially a toolbox dedicated to the studying of motion around small bodies in the the three-body problem. 
This toolbox was initially developed in Matlab and C++, while studying the motion around a binary asteroid. 
I later decided to fully port the code to C++, and lately to provide a Python wrapper (Py3BP).
The primary objective is to provide an understanding of complex motion in R3BP, and then a tool for computing Lissajous orbits (incl. Lyapunov, Halo) around libration points of a three-body problem (Restricted-, Circular-).

## Feature Examples
![image](_static/Figure_1_2.png)
*Zero velocity label and random trajectory in the Earth-Moon system*

![image](_static/Figure_4.png)
*Family of Halo orbits around L1 in the Earth-Moon system*

![image](_static/Figure_7.png)
*DRO around the Moon, in the Earth-Moon rotating frame*

![image](_static/Figure_3.png)
*Stable and unstable manifolds associated to a Halo orbit around L2 in the Earth-Moon system* 


## License
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 



Copyright (c) 2010-2013 Joris OLYMPIO


## Problems
If you find any issue with the toolbox, please drop me a message. I will do my best to correct any issue promptly.
I also welcome any contribution that can improve the toolbox. 
