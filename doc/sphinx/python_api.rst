Python wrapper API
==================

.. automodule:: r3bpy
    :members:
    :special-members:
    :private-members:
    :undoc-members:
    :exclude-members: __safe_for_unpickling__, __instance_size__, __reduce__, __module__, __getinitargs__

.. automodule:: r3bpy.core
    :members:
    :undoc-members:
    :show-inheritance:

Main Module
-----------    
.. automodule:: r3bpy.main
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: r3bpy.main.Problem
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: r3bpy.main.Propagator
    :members:
    :undoc-members:
    :show-inheritance:


               
Lambert's Problem
#################
.. automodule:: r3bpy.main.LambertProblem 
    :members:
    :undoc-members:
    :show-inheritance:

.. automodule:: r3bpy.main.R3bpLambertProblemSolution 
    :members:
    :undoc-members:
    :show-inheritance:

Low Thrust Problem
##################
.. automodule:: r3bpy.main.LowThrustProblem 
    :members:
    :undoc-members:
    :show-inheritance:

Frames
######
.. automodule:: r3bpy.main.Frames
    :members:
    :undoc-members:
    :show-inheritance:
    
.. autoclass:: r3bpy.main.Frame
    :members:


System
######
.. automodule:: r3bpy.main.ThrusterData 
    :members:
    :undoc-members:
    :show-inheritance:


Orbits Module
-------------
.. automodule:: r3bpy.orbits
    :members:
    :undoc-members:
    :show-inheritance:

Orbit families
##############
.. automodule:: r3bpy.orbits.LissajousOrbit
    :members:
    :undoc-members:
    :show-inheritance:


.. autoclass:: r3bpy.orbits.LyapunovOrbitFamilyType
    :members:

Halo
~~~~
.. automodule:: r3bpy.orbits.HaloOrbit
    :members:
    :undoc-members:
    :show-inheritance:
    
.. autoclass:: r3bpy.orbits.orbits.HaloOrbitFamily    
    :members:

Lyapunov
~~~~~~~~
.. automodule:: r3bpy.orbits.LyapunovOrbit
    :members:
    :undoc-members:
    :show-inheritance:

DistantRetrogradeOrbit
~~~~~~~~~~~~~~~~~~~~~~
.. automodule:: r3bpy.orbits.DistantRetrogradeOrbit
    :members:
    :undoc-members:
    :show-inheritance:
    
LongShortPeriodicOrbit
~~~~~~~~~~~~~~~~~~~~~~
.. automodule:: r3bpy.orbits.LongShortPeriodicOrbit
    :members:
    :undoc-members:
    :show-inheritance:
        
OrbitSolution
~~~~~~~~~~~~~~~~~~~~~~
.. automodule:: r3bpy.orbits.OrbitSolution
    :members:
    :undoc-members:
    :show-inheritance:
            
Multi-Points Lissajous Module
#############################   
.. automodule:: r3bpy.orbits.LissajousMultiPointsOrbit
    :members:
    :undoc-members:
    :show-inheritance:
    
.. automodule:: r3bpy.orbits.OrbitSegment
    :members:
    :undoc-members:
    :show-inheritance:
            
Continuation Module
###################
.. automodule:: r3bpy.orbits.ContinuationProcedure
    :members:
    :undoc-members:
    :show-inheritance:
    
.. autoclass:: r3bpy.orbits.ContinuationMethod
    :members:


Visualisation Module
--------------------
.. automodule:: r3bpy.visu
    :members:
    :undoc-members:
    :show-inheritance:


.. autofunction:: r3bpy.visu.init_figure

.. autofunction:: r3bpy.visu.plot_system
.. autofunction:: r3bpy.visu.plot_jacobi_levels
.. autofunction:: r3bpy.visu.plot_trajectory

.. autofunction:: r3bpy.visu.plot_segments
.. autofunction:: r3bpy.visu.plot_manifold

.. autofunction:: r3bpy.visu.plot_libration_point
.. autofunction:: r3bpy.visu.plot_stability
.. autofunction:: r3bpy.visu.plot_period

MatPlotLib
##########

Face
~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: r3bpy.visu.MplFace

Ellipsoid
~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: r3bpy.visu.MplEllipsoid

Mayavi
######
.. autofunction:: r3bpy.visu.mayavi.create_scene
.. autofunction:: r3bpy.visu.mayavi.create_sphere
.. autofunction:: r3bpy.visu.mayavi.create_ellipsoid
.. autofunction:: r3bpy.visu.mayavi.plot_trajectory
.. autofunction:: r3bpy.visu.mayavi.plot_orbit


Face
~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: r3bpy.visu.MayaFace

Ellipsoid
~~~~~~~~~~~~~~~~~~~~~~
.. autoclass:: r3bpy.visu.MayaEllipsoid


