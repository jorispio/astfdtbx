[![made-with-sphinx-doc](https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg)](https://www.sphinx-doc.org/) 
[![docs](https://img.shields.io/badge/docs-v0.1.0-green.svg)](https://jorispio.gitlab.io/astftbx)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 
[![DOI](https://img.shields.io/badge/DOI-10.5281/zenodo.3884255-orange.svg)](https://doi.org/10.5281/zenodo.3884255)

Installation
============

## Requirements
1. A C++11 compiler (The build chain is based on CMake)
2. The application should compile on Windows, Linux and Max. I have actually tested it on Windows (Cygwin) and Linux.
3. Optionally Python

## Download the sources
Download the latest version source code from the main page as a zip.


## Third-parties
There are mandatory third-party products:
1. Eigen, a very nice C++ math library. You can find it here: [eigen]
2. Boost and Numpy for Python wrapper
3. HelioLib, my space mechanics toolbox. A minimalist extract of it is currently included. 
4. Doxygen and Sphinx for document generation

[eigen]: eigen.tuxfamily.org/index.php


## Compiling
The building and compilation is based on cross platform CMake configuration file.
Go into the root folder. 
In a console, type
```
cmake -DCMAKE_INSTALL_PREFIX=[your install path] .
```

where [your install path] defines the installation destination directory ("prefix"). 
(If omitted, the build would most probably be installed in /usr/local.)

### Numpy
Then, compile third parties.
Go into thirdparty/Boost.Numpy,
```
make
make install
```

### HelioLib 
Go into thirdparty/HelioLib,
```
make
make install
```

### Main compilation
Then:
```
make
make install
```

The build [your install path] folder should contain:
- an executable, 
- lib directory with the static and share libraries, 
- the Python package,
- examples directory.

If you want to compile the unit tests, add DBUILD_TESTS to the cmake call, 
```
cmake -DBUILD_TESTS=ON .
```

If you do not want to build the Python Package, 
```
cmake -DBUILD_PYTHON_LIB=OFF .
```


## Problems
If you find any issue with the toolbox, please drop me a message. I will do my best to correct any issue promptly.
I also welcome any contribution that can improve the toolbox. 

