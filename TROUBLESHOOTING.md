## Troubleshooting
```
  Could not find the following Boost libraries:

          boost_python3
```

Solution:          
```
sudo ln -s libboost_python-py3x.so libboost_python3.so
```

Library not found:
```
```
Solution
```
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib:/usr/local/lib64