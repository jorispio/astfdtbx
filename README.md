[![made-with-sphinx-doc](https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg)](https://www.sphinx-doc.org/) 
[![docs](https://img.shields.io/badge/docs-v0.1.0-green.svg)](https://jorispio.gitlab.io/astftbx)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 
[![DOI](https://img.shields.io/badge/DOI-10.5281/zenodo.3884255-orange.svg)](https://doi.org/10.5281/zenodo.3884255)

README
=========

![image](doc/figures/Figure_4.png)

*Family of Halo orbits around L1 in the Earth-Moon system*

![image](doc/sphinx/_static/Figure_15_mayavi.png)

*Low-thrust transfer to a Halo orbit*



## Preambule
R3BPTBX is initially a toolbox dedicated to the studying of motion around small bodies in the the three-body problem. 
This toolbox was initially developed in Matlab and C++, while studying the motion around a binary asteroid. 
I later decided to fully port the code to C++, and lately to provide a Python wrapper (Py3BP).
The primary objective is to provide an understanding of complex motion in R3BP, and then a tool for computing Lissajous orbits (incl. Lyapunov, Halo) around libration points of a three-body problem (Restricted-, Circular-).

More at: [documentation]

[documentation]: https://jorispio.gitlab.io/astfdtbx

About the author: Joris Olympio <a href="http://orcid.org/0000-0003-3848-5587"><img src="https://img.shields.io/badge/orcid-0000--0003--3848--5587-blue.svg"></a>

### Running under python
Import
```
    from asttbx.main import Problem
    from asttbx.lissajous import HaloOrbit
```

Define your problem filling the structure
```
    problem  = Problem(mu_ratio)
    libration_point = problem.get_libration_point(ast.main.L2)
```
Then construct the orbit
```
    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az)
    ast.propagate(problemDefinition, problemContext, problemSolution, True)
```
And, propagate and plot
```
    xyz_halo = halo_orbit.propagate([0, orbit_period])
    ast.visu.plot_trajectory(xyz_halo)
```    
Please see the examples for details.


## Installing

### Requirements
1. A C++11 compiler (The build chain is based on CMake)
2. The application should compile on Windows, Linux and Max. I have actually tested it on Windows (Cygwin) and Linux.
3. Optionally Python

### Download the sources
Download the latest version source code from the main page as a zip.


### Third-parties
There are mandatory third-party products:
1. Eigen, a very nice C++ math library. You can find it here: [eigen]
2. Boost and Numpy for Python wrapper
3. HelioLib, my space mechanics toolbox. A minimalist extract of it is currently included. 
4. Doxygen and Sphinx for document generation

[eigen]: eigen.tuxfamily.org/index.php


### Compiling
The building and compilation is based on cross platform CMake configuration file.
Go into the root folder. 
In a console, type
```
cmake -DCMAKE_INSTALL_PREFIX=[your install path] .
```

where [your install path] defines the installation destination directory ("prefix"). 
(If omitted, the build would most probably be installed in /usr/local.)

#### Numpy
Then, compile third parties.
Go into thirdparty/Boost.Numpy,
```
make
make install
```

#### HelioLib 
Go into thirdparty/HelioLib,
```
make
make install
```

#### Main compilation
Then:
```
make
make install
```

The build [your install path] folder should contain:
- an executable, 
- lib directory with the static and share libraries, 
- the Python package,
- examples directory.

If you want to compile the unit tests, add DBUILD_TESTS to the cmake call, 
```
cmake -DBUILD_TESTS=ON .
```

If you do not want to build the Python Package, 
```
cmake -DBUILD_PY_PKG=OFF .
```


## License
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 



Copyright (c) 2010-2013 Joris OLYMPIO



## Problems
If you find any issue with the toolbox, please drop me a message. I will do my best to correct any issue promptly.
I also welcome any contribution that can improve the toolbox. 


```
