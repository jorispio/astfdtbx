## HelioLib
### Foreword
This folder contains an extract of my C++ space mechanics library (HelioLib), developped throughout my years in school and during my research period.
The initial motivation was simple and purely personnal - having a space mechanics library that is tuned to my space mechanics (and trajectory optimisation) problems.
Over the years, it ressemble a batch of codes (with some coherence though!), but not necessarily in a good-enough format for publishing.
The current folder contains thus a small part of it, that I tried to clean up, slightly. Hopefully, with more time, I shall publish the whole library. 

### Covered Topics
The following topics are covered in this "light" version:
* Time
* Frame conversions
* state and state transition matrix propagation, with event detection
* Force: third-body perturbation 
* Maths: ODE solver and quadrature, polynomial root solving, rotation, 


## License
The MIT License

Copyright (c) 2002-2013 Joris OLYMPIO


