//$Id: HelioLib.hpp 137 2013-03-02 21:06:40Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __HELIO_HPP_
#define __HELIO_HPP_

#include <iostream> // std::cout
#include <limits>   // std::numeric_limits
#include <stddef.h> // avoid issue with Eigen during compilation about missing symbols.

/**
 * @mainpage
 * HelioLib is a space mechanics toolbox, with a set of tool to perform accurate space mechanics
 * computations.
 * It includes:
 *   - conversion of orbital elements (keplerian, equinoctial, cartesian)
 *   - analytical propagation of orbital elements.
 *   - date and time conversion
 *   - Lambert's problem solver
 *   - swingby calculation routines
 *
 * This package is a capitalisation of what I have learn over the years in space flight dynamics.
 * It is though limited to the basic functions I wrote for specific application, but they can have,
 * of course, a wider range of applications.
 * Some part of the code found some inspiration from other famous package (!), but similarities are
 * nonetheless limited as I have often a different approach to things!
 *
 * @version 1.00
 *
 * @licence
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "libversion_config.hpp"

#include "Core.hpp"
#include "Maths.hpp"
#include "Orbits.hpp"

#include "ephemerides/PlanetAnalyticEphemerides.hpp"
#ifdef WITH_EPHEMERIDES
    #include "ephemerides/EphemeridesLoader.hpp"
#endif

#include "Frames.hpp"
#include "Dates.hpp"
#include "Transfers.hpp"
#include "Physics.hpp"

// Guidance
#include "propagation/control/GuidanceControlBase.hpp"
#ifdef WITH_GUIDANCE
    #include "propagation/control/NullGuidance.hpp"
#endif

#include "Dynamics.hpp"
#include "Propagation.hpp"

#include "Io.hpp"

// SYSTEM
#ifdef WITH_SYSTEM
    #include "system/Thruster.hpp"
#endif

#endif  // __HELIO_HPP_
