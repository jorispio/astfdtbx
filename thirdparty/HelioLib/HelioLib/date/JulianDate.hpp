// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_DATE_JULIEN_HPP_
#define __SMTBX_DATE_JULIEN_HPP_

#include <math.h>

#include "DateUtils.hpp"

class JulianDate {

/**
 * JD = julien(jour, mois, annee, heure, minute, seconde)
 * return julien date in days.
 *
 * inputs:
 * Specify 'jour'/'mois'/'annee' and eventually 'heure', 'minute' and
 *'seconde'
 *
 * outputs:
 *   JD      Julian date
 *   MJD2000 Modified Julian date MJD2000 (reference 1 Jan 2000 12:00:00)
 *   MJD     Modified Julian date (reference 5 nov 1858 00:00:00)
 */
#ifdef WITH_ADVANCED_DATES
static
void julien(double date, double* jd_, double* mjd2000_, double* mjd_) {
    int nyear, nmonth, ndate, nhour, nmin;
    double nsec;
    int a, ggg, s, j1, j2, jd, jd1, jd2dat1;

    // conversion de date en dd mm yyyy
    DateConversion::datevec(date, &nyear, &nmonth, &ndate, &nhour, &nmin, &nsec);

    nhour = nhour + (nmin / 60) + (nsec / 3600);
    ggg = 1;

    if (nyear < 1582)
        ggg = 0;
    if (nyear < 1582 && nmonth < 10)
        ggg = 0;
    if (nyear <= 1582 && nmonth == 10 && ndate < 5)
        ggg = 0;

    jd = -1 * floor(7 * (floor((nmonth + 9) / 12) + nyear) / 4);
    s = 1;

    if ((nmonth - 9) < 0)
        s = -1;

    a = abs(nmonth - 9);
    j1 = static_cast<int>(floor(nyear + s * floor(a / 7)));
    j2 = -1 * floor(((floor(j1 / 100) + 1) * 3 / 4));
    jd1 = jd + floor((275 * nmonth / 9) + ndate + (ggg * j2));
    jd2dat1 = jd1 + 1721027 + 2 * ggg + 367 * nyear - 0.5;
    jd2dat1 = jd2dat1 + (nhour / 24);

    // outputs
    *jd_ = jd2dat1;
    *mjd2000_ = *jd_ - 2451545.0;
    *mjd_ = *jd_ - 2400000.5;
}
#endif

/**
% JD = julien(jour, mois, annee, heure, minute, seconde)
% return julien date in days.
%
% inputs:
% Specify 'jour'/'mois'/'annee' and eventually 'heure', 'minute' and
%'seconde'
%
% outputs:
%   JD      Julian date
%   MJD2000 Modified Julian date MJD2000 (reference 1 Jan 2000 12:00:00)
%   MJD     Modified Julian date (reference 5 nov 1858 00:00:00)
%
  */
static
double Gregorian2mjd(int day, int month, int year, int hh, int mm, double ss) {
    // FIXME(jo) mix int/double
    long jj =static_cast<long>((14 - month) / 12);
    long ll = static_cast<long>(year - jj - 1900 * (year / 1900) + 100 * (2000 / (year + 1951)));
    double jday = static_cast<double>(day) - 36496
                    + static_cast<double>((1461 * ll) / 4)
                    + static_cast<double>((367. * static_cast<double>(month - 2 + jj * 12)) / 12.);
    double time = ((hh * 60) + mm * 60) + ss;
    return static_cast<double>(jday + time / 86400.);
}
};  // JulianDate

#endif  // __SMTBX_DATE_JULIEN_HPP_
