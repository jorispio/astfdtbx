// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SMTBX_DATE_UTILS_HPP_
#define __SMTBX_DATE_UTILS_HPP_
// ----------------------------------------------------------------------------
#include <string.h>

#include <iostream>
#include <cstdio>
#include <string>

// ----------------------------------------------------------------------------
namespace HelioLib {
// Date format
enum DATE_FORMAT { JD, MJD, MJD_2000, MJD_CNES };

// Time scales
enum TIME_SCALE { TAI, UTC, UT1, GPS };
}  // namespace SpaceMecaLib

// ----------------------------------------------------------------------------
struct EPOCH {
 public:
    static const EPOCH J2000;

    int yy;
    int MM;
    int dd;
    int hh;
    int mm;
    double ss;

    /** Simple Constructor */
    EPOCH() : yy(0), MM(0), dd(0), hh(0), mm(0), ss(0.) { }

    /** Constructor
     * @brief
     * @param yy_  year
     * @param MM_  month
     * @param dd_  day
     * @param hh_  hour
     * @param mm_  minute
     * @param ss_  second
     */
    EPOCH(int yy_, int MM_, int dd_, int hh_, int mm_, double ss_)
        : yy(yy_),  MM(MM_),  dd(dd_),
        hh(hh_),  mm(mm_),  ss(ss_) { }

    EPOCH(int yy_, int MM_, int dd_)
        : EPOCH(yy_, MM_, dd_, 0, 0, 0) { }

    std::string getString() {
        char str[128];
        snprintf(str, sizeof(str), "%02d/%02d/%02d %02d:%02d:%06.3f UT", dd, MM, yy, hh, mm, ss);
        return std::string(str);
    }
};

// ----------------------------------------------------------------------------
/**
 *
 */
class DateConversion {
 public:
    static void datevec(double date, int& nyear, int& nmonth, int& ndate, int& nhour, int& nmin, double& nsec);

    static double convertGregorianDate(const EPOCH& epoch, HelioLib::DATE_FORMAT format = HelioLib::JD);

#ifdef WITH_ADVANCED_DATES
    static EPOCH convertDate2Epoch(double epoch, HelioLib::DATE_FORMAT format = SpaceMecaLib::JD);
#endif

    static void convertEpoch2StrDate(const EPOCH &epoch, char str[], int n);
    static void convertEpoch2StrDate(const EPOCH &epoch, std::string& str);

    static void duration2DayHrMinSec(double duree, int& iday, int& ihr, int& imin, double& isec);
};
// ----------------------------------------------------------------------------
#endif  // __SMTBX_DATE_UTILS_HPP_
