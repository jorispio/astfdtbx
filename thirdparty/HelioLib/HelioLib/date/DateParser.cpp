// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DateParser.hpp"

#include <stdlib.h> /* atof */
#include <math.h>   /* sin */

#include <iostream>
#include <cstdio>
#include <string>

#include <boost/regex.hpp>

#include "core/HelioException.hpp"

// ----------------------------------------------------------------------------
// Format: yy-mm-ddThh:mm:ss.sss
// REGEX: ^([\d.]*)\s*([A-Z0-9])
// Example: 1999 MJD2000
//
bool DateParser::convertStrDay2Date(const std::string &strdate, GenericDate& date) {
    boost::regex reg("^([\\d.]+)\\s*([A-Z0-9]+)");
    if (!boost::regex_match(strdate, reg)) {
        throw HelioLibException("Invalid syntax for date/day");
        // return false;
    }

    boost::smatch cm;  // same as std::match_results<const char*> cm;
    boost::regex_match(strdate, cm, reg);
    if (cm.size() < 2) {
        throw HelioLibException("Incomplete day. Example: 54000 MJD / 1852 MJD2000 / 15156113 JD");
        // return false;
    }

    double day = atof(cm[1].str().c_str());
    std::string str = cm[2].str();
    DATE_FORMAT format = GenericDate::getTypeFromStr(str);
    date.set(day, format);

    return true;
}
// ----------------------------------------------------------------------------
// Format: yy-mm-ddThh:mm:ss.sss
// REGEX: ^(\d*)-(\d*)-(\d*)T*(\d*)*:*(\d*):*([\d.]*)
//
bool DateParser::convertStrDate2Epoch(const std::string &str, EPOCH& epoch) {
    boost::regex reg("^(\\d*)-(\\d*)-(\\d*)T*(\\d*):*(\\d*):*([\\d.]*)");
    if (!boost::regex_match(str, reg)) {
        throw HelioLibException("Invalid syntax for date/time");
        // return false;
    }

    boost::smatch cm;  // same as std::match_results<const char*> cm;
    boost::regex_match(str, cm, reg);
    if (cm.size() < 3) {
        throw HelioLibException("Incomplete date/time");
        // return false;
    }

    int yy = atoi(cm[1].str().c_str());
    int MM = atoi(cm[2].str().c_str());
    int dd = atoi(cm[3].str().c_str());

    int hh = 0;
    int mm = 0;
    double ss = 0.;
    if (cm.size() > 3) {
        hh = atoi(cm[4].str().c_str());
        if (cm.size() > 4) {
            mm = atoi(cm[5].str().c_str());
            if (cm.size() > 5) {
                ss = atof(cm[6].str().c_str());
            }
        }
    }

    epoch = EPOCH(yy, MM, dd, hh, mm, ss);
    return true;
}
// ----------------------------------------------------------------------------
// Format: yy-mm-ddThh:mm:ss.sss
bool DateParser::parseDateStr(const char str[], GenericDate& date) {
    std::string s(str);

    if (s.find("JD") != std::string::npos) {
        bool res = convertStrDay2Date(s, date);
        return res;
    }

    EPOCH epoch;
    bool res = convertStrDate2Epoch(std::string(str), epoch);
    date = GenericDate(epoch);
    return res;
}
// ----------------------------------------------------------------------------
