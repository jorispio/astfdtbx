// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "GenericDate.hpp"

#include <string>

#include "HelioLib/Core.hpp"

using HelioLib::DATE_FORMAT;
using HelioLib::JD;
using HelioLib::MJD;
using HelioLib::MJD_2000;

// ---------------------------------------------------------------------
GenericDate::GenericDate(double t, DATE_FORMAT f) {
    date = t;
    format = f;
}

// ---------------------------------------------------------------------
GenericDate::GenericDate(EPOCH e) {
    date = DateConversion::convertGregorianDate(e, JD);
    format = JD;
}

// ---------------------------------------------------------------------
void GenericDate::set(double t, DATE_FORMAT f) {
    date = t;
    format = f;
}

// ---------------------------------------------------------------------
/** Add a time offset, shifting the date by dt
 * @param dt duration in seconds
 */
GenericDate GenericDate::addOffset(double dt) const {
    return GenericDate(date + dt / 86400., format);
}

// ---------------------------------------------------------------------
/** Return the duration from datefrom to this date, in days. */
double GenericDate::durationFrom(const GenericDate& datefrom) {
    return (getMJD2000() - datefrom.getMJD2000());
}

// ---------------------------------------------------------------------
/** get Julian Date */
double GenericDate::getJD() const {
    if (format == MJD) {
        return date + MJD_JD_OFFSET;
    } else if (format == MJD_2000) {
        return date + (2400000.5 + 51544.0);
    }
    return date;
}
// ---------------------------------------------------------------------
/** Get Modified Julian Date */
double GenericDate::getMJD() const {
    if (format == JD) {
        return date - MJD_JD_OFFSET;
    } else if (format == MJD_2000) {
        return date + 51544.0;
    }
    return date;
}

// ---------------------------------------------------------------------
/** Get Modified Julian Date J2000 */
double GenericDate::getMJD2000() const {
    if (format == JD) {
        return date - (2400000.5 + 51544.0);
    } else if (format == MJD) {
        return date - (51544.0);
    }

    return date;
}

// ---------------------------------------------------------------------
/** Get Julian Date */
double GenericDate::getJD(double val, DATE_FORMAT format) {
    if (format == MJD) {
        return val + MJD_JD_OFFSET;
    } else if (format == MJD_2000) {
        return val + (2400000.5 + 51544.0);
    }
    return val;
}

// ---------------------------------------------------------------------
/** Get Modified Julian Date */
double GenericDate::getMJD(double val, DATE_FORMAT format) {
    if (format == JD) {
        return val - MJD_JD_OFFSET;
    } else if (format == MJD_2000) {
        return val + 51544.0;
    }
    return val;
}

// ---------------------------------------------------------------------
/** Get Modified Julian Date J2000 */
double GenericDate::getMJD2000(double val, DATE_FORMAT format) {
    if (format == JD) {
        return val - (2400000.5 + 51544.0);
    } else if (format == MJD) {
        return val - (51544.0);
    }

    return val;
}

// ---------------------------------------------------------------------
/** Get enum type from string  */
DATE_FORMAT GenericDate::getTypeFromStr(const std::string& f) {
    if (f.compare("JD") == 0) {
        return JD;
    } else if (f.compare("MJD") == 0) {
        return MJD;
    } else if (f.compare("MJD2000") == 0) {
        return MJD_2000;
    }

    char str[256];
    snprintf(str, sizeof(str), "Unknown day format '%s'. It should belong to the group {JD, MJD, MJD2000}", f.c_str());
    throw HelioLibException(str);
}
