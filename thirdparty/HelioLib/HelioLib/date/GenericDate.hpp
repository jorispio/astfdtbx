/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SMTBX_GENERIC_DATE_HPP_
#define __SMTBX_GENERIC_DATE_HPP_

#include <string>

#include "DateUtils.hpp"

// ----------------------------------------------------------------------------
class GenericDate {
 private:
    HelioLib::DATE_FORMAT format;
    double date;

 public:
    /** Basic constructor. Do not use directly! */
    GenericDate() : date(0.) { }

    /** Constructor */
    GenericDate(double t, HelioLib::DATE_FORMAT f = HelioLib::MJD);

    /** */
    explicit GenericDate(EPOCH e);

    /** Destructor */
    ~GenericDate() { }

    /** Set the date */
    void set(double t, HelioLib::DATE_FORMAT f);

    /** Add a time offset */
    GenericDate addOffset(double dt) const;

    /** */
    double durationFrom(const GenericDate& datefrom);

    /** */
    double getJD() const;

    /** */
    double getMJD() const;

    /** */
    double getMJD2000() const;

    /** */
    static double getJD(double val, HelioLib::DATE_FORMAT format);

    /** */
    static double getMJD(double val, HelioLib::DATE_FORMAT format);

    /** */
    static double getMJD2000(double val, HelioLib::DATE_FORMAT format);

    /** */
    static HelioLib::DATE_FORMAT getTypeFromStr(const std::string& f);
};
// ----------------------------------------------------------------------------
#endif  // __SMTBX_GENERIC_DATE_HPP_
