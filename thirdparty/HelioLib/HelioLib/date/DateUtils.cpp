// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "DateUtils.hpp"

#include "HelioLib/Core.hpp"
#include "GenericDate.hpp"

using HelioLib::DATE_FORMAT;

const EPOCH EPOCH::J2000 = EPOCH(2000, 1, 1, 12, 0, 0);

// ----------------------------------------------------------------------------
/** conversion of scalar date into date components, dd mm yyyy */
#ifdef WITH_ADVANCED_DATES
void DateConversion::datevec(double date, int& nyear, int& nmonth, int& nday, int& nhour, int& nmin, double& nsec) {
    EPOCH epoch = convertDate2Epoch(date, DATE_FORMAT::JD);
    nyear = epoch.yy;
    nmonth = epoch.MM;
    nday = epoch.dd;
    nhour = epoch.hh;
    nmin = epoch.mm;
    nsec = epoch.ss;
}
#endif
// ----------------------------------------------------------------------------
// convert a date
// See : http://www.csgnetwork.com/julianmodifdateconv.html
double DateConversion::convertGregorianDate(const EPOCH& epoch, DATE_FORMAT format) {
    int y = epoch.yy;
    int m = epoch.MM;
    int d = epoch.dd;

    if (m > 2) {
        m = m - 3;
    } else {
        m = m + 9;
        y = y - 1;
    }

    int c = y / 100;
    int ya = y - 100 * c;
    int j = (146097 * c) / 4 + (1461 * ya) / 4 + (153 * m + 2) / 5 + d + 1721119;

    double time = ((epoch.hh * 60) + epoch.mm) * 60 + epoch.ss;

    double jj = (j + time / 86400 - 0.5);  // julian day

    if (format == DATE_FORMAT::MJD) {
        double mjd = jj - MJD_JD_OFFSET;
        return mjd;
    } else if (format == DATE_FORMAT::MJD_2000) {
        double mjd2000 = jj - (2400000.5 + 51544.0);
        return mjd2000;
    } else if (format == DATE_FORMAT::MJD_CNES) {
        return jj - 2433282.5;  // julian day CNES  1950-01-01T00:00:00
    }

    return jj;
}

// ----------------------------------------------------------------------------
/** convert a date
 * julian day CNES  1950-01-01T00:00:00
 * @param val    date value in specified format
 * @param format date format (default = JD)
 * @return epoch
 */
#ifdef WITH_ADVANCED_DATES
EPOCH DateConversion::convertDate2Epoch(double val, DATE_FORMAT format) {
    // double jj = epoch + 2433282.5;
    int yy = 0;
    int MM = 0;
    int dd = 0;
    int hh = 0;
    int mm = 0;  // minutes
    double ss = 0;
    double fd;

    // we request MJD to separate properly the date in two terms
    // MJD2000 should give the smallest value among JD, MJD and MJD2000
    double epochjd = GenericDate::getMJD2000(val, format);

    /* SOFA */
    iauJd2cal(2451544.5, epochjd, &yy, &MM, &dd, &fd);

    double time = fd * 86400;
    hh = static_cast<int>(time / 3600.);
    mm = static_cast<int>((time - hh * 3600) / 60.);
    ss = (((time - hh * 3600) - mm * 60));

    return EPOCH(yy, MM, dd, hh, mm, ss);
}
#endif  // WITH_ADVANCED_DATES
// ----------------------------------------------------------------------------
// convert a date
void DateConversion::duration2DayHrMinSec(double duration, int& day, int& hr, int& min, double& sec) {
    day = static_cast<int>(floor(duration / 86400.));
    hr = static_cast<int>(floor((duration - day * 86400.) / 3600));
    min = static_cast<int>(floor(((duration - day * 86400.) - hr * 3600) / 60.));
    sec = (((duration - day * 86400.) - hr * 3600) - min * 60);
}

// ----------------------------------------------------------------------------
// convert a date
void DateConversion::convertEpoch2StrDate(const EPOCH &epoch, char str[], int n) {
    snprintf(str, n, "%02d-%02d-%02dT%02d:%02d:%06.3f", epoch.yy, epoch.MM, epoch.dd, epoch.hh, epoch.mm, epoch.ss);
}
// ----------------------------------------------------------------------------
// convert a date
void DateConversion::convertEpoch2StrDate(const EPOCH &epoch, std::string& str) {
    char buffer[64];
    snprintf(buffer, sizeof(buffer), "%02d-%02d-%02dT%02d:%02d:%06.3f", epoch.yy, epoch.MM, epoch.dd, epoch.hh, epoch.mm, epoch.ss);
    str = std::string(buffer);
}
