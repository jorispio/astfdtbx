// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_DATE_PARSER_HPP
#define __SMTBX_DATE_PARSER_HPP

#include "DateUtils.hpp"
#include "GenericDate.hpp"

// ----------------------------------------------------------------------------
class DateParser {
 public:
    /** Convert ddddd MJD to Date component */
    static bool convertStrDay2Date(const std::string &str, GenericDate& date);

    /** Convert yyy-mm-ddThh:mm:ss.sss or ddddd MJD to Date component */
    static bool parseDateStr(const char str[], GenericDate& date);

    /** Convert yyyy-mm-ddThh:mm:ss.sss to Epoch component */
    static bool convertStrDate2Epoch(const std::string &str, EPOCH& epoch);
};
// ----------------------------------------------------------------------------
#endif  // __SMTBX_DATE_PARSER_HPP
