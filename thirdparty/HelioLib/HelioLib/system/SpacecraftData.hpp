// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010-2015 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_SPACECRAFT_DATA_HPP_
#define __SMTBX_SPACECRAFT_DATA_HPP_

#include "core/HelioEnumerations.hpp"

/* --------------------------------------------------------------------- */
typedef struct ThrusterData {
    PROPULSION_TYPE PropType; // propulsion type (NEP, SEP)
    double Fth;               // nominal thrust amplitude
    double g0Isp;             // exhaust velocity    
    double eff;               // efficiency
	
	// specific to SEP
	double maxSEPFth;         // maximum thrust for the SEP system
    double rscale;            // distance 1AU
	
	// specific to Solar sails
	double sailSurface;		// sail surface
	double powerDensity;	// power density of the sail, W/m2
	
    bool withEclipse;         // shutdown with eclipse flag

	double adim_m;
	double adim_ms;
	
    ThrusterData() : Fth(0), g0Isp(0), eff(1) {
        adim_m = 1;
        adim_ms = 1;
        withEclipse = false;
    }
    
	ThrusterData(double Fth, double g0Isp) : Fth(Fth), g0Isp(g0Isp), eff(1) {
        adim_m = 1;
        adim_ms = 1;
        withEclipse = false;
	}
	
    void scale(double a_m, double a_ms, double a_N) {
        /* thruster definition  */
        Fth /= a_N;
        g0Isp /= a_ms;
        rscale /= a_m;
        maxSEPFth /= a_N;
		sailSurface /= (a_m * a_m);

		double a_W = a_N * a_ms;  // [W] = [N] [m/s] = [kg] [m^2] [s^-3]
		powerDensity *= (a_m * a_m) / a_W;
		
		adim_m = a_m;
		adim_ms = a_ms;
    }

    void unscale(double a_m, double a_ms, double a_N) {
        scale(1. / a_m, 1. / a_ms, 1. / a_N);
    }
} ThrusterData;

/* --------------------------------------------------------------------- */
typedef struct SpacecraftData {
    std::string name;
    ThrusterData propulsion; // propulsion
    double mass;             // initial spacecraft mass
    double cross_section;    // spacecraft cross section
    double drag_coefficient; // spacecraft drag coefficient
    double srp_absorb;       // spacecraft SRP absoroption coefficient
    double srp_specular;     // spacecraft SRP specular coefficient

    void scale(double a_m, double a_s, double a_kg) {
        mass /= a_kg;

        /* cross section */
        cross_section /= (a_m * a_m);

        /* thruster definition  */
        double a_N = a_kg * a_m / (a_s * a_s);
        propulsion.scale(a_m, a_m / a_s, a_N);
    }

    void unscale(double a_m, double a_s, double a_kg) {
        scale(1. / a_m, 1. / a_s, 1. / a_kg);
    }
} SpacecraftData;

// ---------------------------------------------------------------------
#endif  // __SMTBX_SPACECRAFT_DATA_HPP_
