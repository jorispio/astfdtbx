// $Id$
// ---------------------------------------------------------------------------
/** ---------------------------------------------------------------------------
 * Low-Thrust Thruster model
 *
 * Thruster class to model NEP and SEP systems.
 *
 * NEP systems are characterized with a constant thrust amplitude and fuel mass
 * flow rate. The input and output powers are also fixed.
 *
 * SEP systems performance depends on the distance to the Sun. Those systems
 * explicitely depends on the input power, coming from solar panels. The input
 * and output powers, the thrust amplitude, and the mass flow rate thus vary
 * with the distance to the Sun. However, the thrust amplitude, and subsequent
 * system variables, is limited.
 *
 * Author: Joris Olympio
 * Date: 03/2011
 * @version $Revision$
 * @lastrevision $Date$
 * @modifiedby $LastChangedBy$
 * @lastmodified $LastChangedDate$
 * @filesource $URL$
 *
 * --------------------------------------------------------------------------- */
#ifndef __SMTBX_THRUSTER_HPP_
#define __SMTBX_THRUSTER_HPP_

#include <stdlib.h>
#include <math.h>

#include "core/HelioDataTypes.hpp"
#include "system/SpacecraftData.hpp"
#include "core/HelioEnumerations.hpp"
#include "ephemerides/PlanetAnalyticEphemerides.hpp"
#include "date/GenericDate.hpp"
#include "physics/Eclipse.hpp"
#include "maths/SpecialFunctions/SmoothHeavisideFunction.hpp"

// ---------------------------------------------------------------------------
class Thruster {
 private:

 protected:
    /** Thruster data */
    ThrusterData mThrusterData;

    /** SEP or NEP */
    PROPULSION_TYPE type;
	
    /** thruster nominal thrust amplitude. */
    double Fth;
    /** thruster exhaust velocity. */
    double g0Isp;
    /** thruster mass flow rate */
    double q;
    /** thruster power */
    double P;
    /** thruster efficiency (power conversion electrical->jet) */
    double eff;

    /** flag indicating whether eclipse shutdown is active. */
    bool withEclipse;

    /** consider umbra only for eclipse or pernumbra+umbra */
    bool withUmbra;

    /** Analytical ephemerides to get sun position during eclipses. */
    PlanetAnalyticEphemerides* pl;
    double sun_radius;
    double earth_radius;

    /** Get unperturbed (eclipse) thrust */
    virtual double GetUnperturbedThrust(double t, const Vector3& R_Sc_Cb) = 0;

 public:
    /** Creator */
    Thruster(const ThrusterData& ThrusterDef, PlanetAnalyticEphemerides* plI);

    /** Destructor */
    virtual ~Thruster();

    /** return Thruster type */
    PROPULSION_TYPE GetType(void) {
        return type;
    };

    /** Set the thruster caracteristics. */
    void SetThruster(const ThrusterData& ThrusterDef);

    /** Get thruster data. */
    ThrusterData GetThrusterData() const;

    /** Set the thruster caracteristics */
    void SetPower(double P0_, double g0Isp_, double nu_ = 1);

    /** return the nominal thrust amplitude at 1AU*/
    inline double GetNominalThrust() {
        return Fth;
    };

    /** return the exhaust velocity g0*Isp */
    inline double GetExhaustVelocity() {
        return g0Isp;
    }

    /** return the thrust amplitude */
    virtual double GetThrust(double t, const Vector3& r_sun) = 0;

    /** return the thrust throttle (thrust amplitude/norminal thrust amplitude) */
    double GetThrottle(double t, const Vector3& r_sun);

    /* return the fuel mass flow rate */
    virtual double GetMassFlowRate(double t, const Vector3& r_sun);

    /* returns the jet power */
    inline double getNominalPower(void) {
        return (Fth * g0Isp) / (2 * eff);
    };

    /* returns the jet power */
    inline double GetPower(double t, const Vector3& rsun) {
        return (GetThrust(t, rsun) * g0Isp) / (2 * eff);
    };

    /** return available power ratio [0-1]. */
    double GetAvailablePower(double t, const Vector3& R_Sc_Cb);

    /* Returns the derivative of thrust amplitude with respect to the position
     * for a given throttle, and spacecraft position
     * with respect to the Sun
     */
    virtual double getDerivatives(real_type t, const Vector3& R_sun, Vector3& dFr, Vector3& dQ, bool useSmoothMin = true) = 0;

    /* Return the time derivative of the thrust amplitude. */
    virtual double getTimeDerivative(real_type t, const Vector3& R_Sc_Cb) = 0;

    /* Return second-order derivative of thrust amplitude wrt position vector. */
    virtual void getSecondDerivatives(real_type t, const Vector3& R, Matrix3& d2Fr, Matrix3& d2Q) = 0;

    /* */
    //int CheckDerivatives(real_type t, const Vector3& R0, const Vector3& V0, const real_type m0, const real_type* X);
};

// ---------------------------------------------------------------------------
#endif  // __SMTBX_THRUSTER_HPP_
