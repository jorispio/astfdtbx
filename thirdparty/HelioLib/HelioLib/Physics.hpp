// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PHYSICS_HPP_
#define __SMTBX_PHYSICS_HPP_

#include "physics/forces/CentralForce.hpp"
#include "physics/forces/ConstantForce.hpp"
#include "physics/forces/ThirdBodyPerturbation.hpp"

#ifdef WITH_FORCES
    #include "physics/forces/ZonalPerturbation.hpp"
    #include "physics/forces/SolarRadiationPressure.hpp"
    #include "physics/forces/AtmosphericDrag.hpp"

    #include "physics/forces/Atmosphere/AtmJacchiaBowman.hpp"
    #include "physics/forces/Atmosphere/AtmExponential.hpp"
    #include "physics/forces/GravityField/EGMFormatReader.hpp"
    #include "physics/forces/GravityField/HolmesFeatherstoneAttractionModel.hpp"

    #include "physics/Eclipse.hpp"
    #include "physics/EclipseCylindrical.hpp"
    #include "physics/SwingBy.hpp"
#endif

#endif  // __SMTBX_PHYSICS_HPP_
