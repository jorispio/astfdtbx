/* $Id: Vector3dExt.cpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Vector3dExt.hpp"

#include <cmath>
#include <cstdio>

#include "maths/MathUtils.hpp"
// ---------------------------------------------------------------------------------------
const Vector3dExt Vector3dExt::PLUS_K = Vector3dExt(0., 0., 1.);

const Vector3dExt Vector3dExt::ZERO = Vector3dExt::Zero();

// ---------------------------------------------------------------------
/**
 */
Vector3dExt::Vector3dExt() : Vector3d(0., 0., 0.) { }

// ---------------------------------------------------------------------
/**
 */
Vector3dExt::Vector3dExt(Vector3d& vec) : Vector3d(vec) {
#ifdef DEBUG_VECTOR3D
    printf("Vector3dExt(%f %f %f)\n", vec(0), vec(1), vec(2));
    printf("Vector3dExt(%f %f %f)\n", this->operator()(0), this->operator()(1), this->operator()(2));
#endif
}

// ---------------------------------------------------------------------
/** Constructor from x,y,z coordinates
 * @param x x-coordinate
 * @param y y-coordinate
 * @param z z-coordinate
 */
Vector3dExt::Vector3dExt(double x, double y, double z) : Eigen::Vector3d(x, y, z) {
#ifdef DEBUG_VECTOR3D
    printf("Vector3dExt(%f %f %f)\n", x, y, z);
    printf("Vector3dExt(%f %f %f)\n", this->operator()(0), this->operator()(1), this->operator()(2));
#endif
}

// ---------------------------------------------------------------------
/** Construct from a vector and a scalar multiplier
 * @param module
 * @param vec
 */
Vector3dExt::Vector3dExt(double module, const Vector3dExt& vec) : Vector3d(module * vec) {
#ifdef DEBUG_VECTOR3D
    printf("Vector3dExt(%f %f %f)\n", vec(0), vec(1), vec(2));
    printf("Vector3dExt(%f %f %f)\n", this->operator()(0), this->operator()(1), this->operator()(2));
#endif
}

// ---------------------------------------------------------------------
/** Get a vector orthogonal to the current one.
 * @return a candidate normalized vector orthogonal to the current one
*/
Vector3dExt Vector3dExt::orthogonal() const {
    double threshold = 0.6 * norm();
    if (threshold < epsilon) {
        return Vector3dExt::ZERO;
    }

    double x = getX();
    double y = getY();
    if (fabs(x) <= threshold) {
        double z = getZ();
        double inverse = 1. / std::sqrt(y * y + z * z);
        return Vector3dExt(0, inverse * z, -inverse * y);
    } else if (fabs(y) <= threshold) {
        double z = getZ();
        double inverse = 1. / std::sqrt(x * x + z * z);
        return Vector3dExt(-inverse * z, 0, inverse * x);
    }

    double inverse = 1. / std::sqrt(x * x + y * y);
    return Vector3dExt(inverse * y, -inverse * x, 0);
}

// ---------------------------------------------------------------------
/**
 * @brief  Compute the angular separation between two vectors.
 * @param v
 * @return
 */
// ---------------------------------------------------------------------
double Vector3dExt::angle(const Vector3dExt& v) {
    double normuv = norm() * v.norm();
    if (normuv > 0) {
        double cosangle = dot(v) / normuv;
        if (fabs(cosangle) > (1 - 1e-6)) {
            double sinangle = cross(v).norm() / normuv;
            if (cosangle < 0)
                return M_PI - asin(sinangle);
            return asin(sinangle);
        }

        return acos(cosangle);
    }
    return 0.;
}
// ---------------------------------------------------------------------
/**
 * @brief  Compute derivative of the angular separation between two vectors depending on a parameter vector.
 *
 * @param v  vector v(p)
 * @param dudr  du(p)/dp
 * @param dvdr  dv(p)/dp
 * @return derivative d angle(u(p),v(p)) / dp
 */
// ---------------------------------------------------------------------
Vector3 Vector3dExt::dangledr(const Vector3dExt &u, const Vector3dExt &v, const Matrix3 &dudr, const Matrix3 &dvdr) {
    double normuv = u.norm() * v.norm();
    if (normuv > 0) {
        Vector3 dnormuv = (dudr * u * v.norm() / u.norm() + dvdr * v * u.norm() / v.norm());
        double cosangle = u.dot(v) / normuv;
        Vector3 dcosangle = (dudr * v + dvdr * u) / normuv - v.dot(u) / (normuv * normuv) * dnormuv;
        Vector3 dr = -dcosangle / std::sqrt(1 - cosangle * cosangle);
        return dr;
    }
    return Vector3::Zero();
}
// ---------------------------------------------------------------------
/**
 * @brief
 * @param v
 * @return the distance between two points
 */
// ---------------------------------------------------------------------
double Vector3dExt::distance(const Vector3dExt& v) const {
    return (*this - v).norm();
}
