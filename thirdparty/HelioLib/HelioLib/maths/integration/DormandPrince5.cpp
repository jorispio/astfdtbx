// $Id$
// ---------------------------------------------------------------------------
#include "DormandPrince5.hpp"

#include <math.h>
#include <malloc.h>
#include <limits.h>
#include <memory.h>

#include <cstdio>

#include "core/HelioException.hpp"
#include "maths/MathUtils.hpp"

/* --------------------------------------------------------------------- */
#define fcn(n, a, b, c) DiffEqn(a, b, c)
#define solout(N, a, b, c, n) AtStep(N, a, b, c)

using HelioLib::MathUtils::sign;
using HelioLib::MathUtils::dsign;

#define DEFAULT_ABSTOL 1e-10
#define DEFAULT_RELTOL 1e-10

const char* DOPRI5::message[] {
    "Not enough free memory for the method.\n",
    "Not enough free memory for rcont[12345]\n",
    "Exit of DOPRI5 at x = %.16e\n",
    "Exit of DOPRI5 at x = %.16e, more than nmax = %i are needed\n",
    "Exit of DOPRI5 at x = %.16e, step size too small h = %.16e\n",
    "The problem seems to become stiff at x = %.16e\n"
};
/* --------------------------------------------------------------------- */
/* COEFFICIENTS OF THE METHOD */
#define c2 0.2
#define c3 0.3
#define c4 0.8
#define c5 8.0 / 9.0
#define a21 0.2
#define a31 3.0 / 40.0
#define a32 9.0 / 40.0
#define a41 44.0 / 45.0
#define a42 -56.0 / 15.0
#define a43 32.0 / 9.0
#define a51 19372.0 / 6561.0
#define a52 -25360.0 / 2187.0
#define a53 64448.0 / 6561.0
#define a54 -212.0 / 729.0
#define a61 9017.0 / 3168.0
#define a62 -355.0 / 33.0
#define a63 46732.0 / 5247.0
#define a64 49.0 / 176.0
#define a65 -5103.0 / 18656.0
#define a71 35.0 / 384.0
#define a73 500.0 / 1113.0
#define a74 125.0 / 192.0
#define a75 -2187.0 / 6784.0
#define a76 11.0 / 84.0
#define e1 71.0 / 57600.0
#define e3 -71.0 / 16695.0
#define e4 71.0 / 1920.0
#define e5 -17253.0 / 339200.0
#define e6 22.0 / 525.0
#define e7 -1.0 / 40.0
#define d1 -12715105075.0 / 11282082432.0
#define d3 87487479700.0 / 32700410799.0
#define d4 -10690763975.0 / 1880347072.0
#define d5 701980252875.0 / 199316789632.0
#define d6 -1453857185.0 / 822651844.0
#define d7 69997945.0 / 29380423.0

// ---------------------------------------------------------------------
/** Construct N-dimensional ODE solver
 */
DOPRI5::DOPRI5(unsigned N) {
    nfcn = nstep = naccpt = nrejct = 0;  // statistics
    nrds = 0;
    indir = 0;
    n = N;

    tolarray = false;
    atol = DEFAULT_ABSTOL;
    rtol = DEFAULT_RELTOL;
    irtrn = 0;

    nmax = 0;
    nstiff = 0;
    meth = 0;
    uround = 0;
    safe = 0;
    beta = 0;
    fac1 = 0;
    fac2 = 0;

    verbose = false;

    nrdens = 0;

    icont = NULL;

    /* is there enough free memory for the method ? */
    yy1 = new real_type[n];
    ysti = new real_type[n];
    k1 = new real_type[n];
    k2 = new real_type[n];
    k3 = new real_type[n];
    k4 = new real_type[n];
    k5 = new real_type[n];
    k6 = new real_type[n];
    if (!yy1 || !k1 || !k2 || !k3 || !k4 || !k5 || !k6) {
        throw HelioLibException(message[0]);
    }

    /* is there enough memory to allocate rcont12345678&indir ? */
    rcont1 = new real_type[n];
    rcont2 = new real_type[n];
    rcont3 = new real_type[n];
    rcont4 = new real_type[n];
    rcont5 = new real_type[n];
    if (!rcont1 || !rcont2 || !rcont3 || !rcont4 || !rcont5) {
        throw HelioLibException(message[1]);
    }

    initialized = false;
}

// ---------------------------------------------------------------------
DOPRI5::~DOPRI5() {
    if (tolarray) {
        delete atolarray;
        delete rtolarray;
    }
    if (icont)
        delete[] icont;
    if (ysti)
        delete[] ysti;
    if (k6)
        delete[] k6;
    if (k5)
        delete[] k5; /* reverse order freeing too increase chances */
    if (k4)
        delete[] k4; /* of efficient dynamic memory managing       */
    if (k3)
        delete[] k3;
    if (k2)
        delete[] k2;
    if (k1)
        delete[] k1;
    if (yy1)
        delete[] yy1;
    if (indir)
        delete[] indir;
    delete[] rcont5;
    delete[] rcont4;
    delete[] rcont3;
    delete[] rcont2;
    delete[] rcont1;
}

// ---------------------------------------------------------------------
/** Set the dimension of the dense output */
void DOPRI5::setmaxdense(unsigned Nrdens) {
    if (icont != NULL) {
        delete[] icont;
        icont = NULL;
    }

    if (Nrdens >= n)
        nrdens = n;
    else {
        nrdens = Nrdens;
        icont = new unsigned[nrdens];
        for (unsigned i = 0; i < nrdens; i++)
            icont[i] = -1U;
    }
}

// ---------------------------------------------------------------------
/** Set full-dimension of the dense output */
void DOPRI5::setmaxdense() {
    delete icont;
    icont = NULL;
    nrdens = n;
}

// ---------------------------------------------------------------------
/** Set the dimension of the dense output */
bool DOPRI5::setdense(unsigned i) {
    if (nrdens == n)
        return true;
    if (!icont)
        return false;
    if (i >= nrdens)
        return false;

    for (unsigned j = 0; j < nrdens; j++)
        if (icont[j] != -1U) {
            icont[j] = i;
            return true;
        }
    return false;
}

// ---------------------------------------------------------------------
/**
 * Resize ODE system.
 */
unsigned DOPRI5::resize(unsigned nn) {
    unsigned on = n;
    n = nn;

    if (ysti)
        delete[] ysti;
    if (k6)
        delete[] k6;
    if (k5)
        delete[] k5; /* reverse order freeing too increase chances */
    if (k4)
        delete[] k4; /* of efficient dynamic memory managing       */
    if (k3)
        delete[] k3;
    if (k2)
        delete[] k2;
    if (k1)
        delete[] k1;
    if (yy1)
        delete[] yy1;
    if (rcont5)
        delete[] rcont5;
    if (rcont4)
        delete[] rcont4;
    if (rcont3)
        delete[] rcont3;
    if (rcont2)
        delete[] rcont2;
    if (rcont1)
        delete[] rcont1;

    yy1 = new real_type[n];
    ysti = new real_type[n];
    k1 = new real_type[n];
    k2 = new real_type[n];
    k3 = new real_type[n];
    k4 = new real_type[n];
    k5 = new real_type[n];
    k6 = new real_type[n];
    if (!yy1 || !k1 || !k2 || !k3 || !k4 || !k5 || !k6) {
        throw HelioLibException(message[0]);
    }

    /* is there enough memory to allocate rcont12345678&indir ? */
    rcont1 = new real_type[n];
    rcont2 = new real_type[n];
    rcont3 = new real_type[n];
    rcont4 = new real_type[n];
    rcont5 = new real_type[n];

    return on;
}
// ---------------------------------------------------------------------
/** Set integration tolerances.
 */
void DOPRI5::settol(real_type abstol, real_type reltol) {
    if (tolarray) {
        delete atolarray;
        delete rtolarray;
    }
    tolarray = false;
    atol = abstol;
    rtol = reltol;
}
// ---------------------------------------------------------------------
/**
 * Set integration tolerances.
 */
void DOPRI5::settol(const real_type* abstol, const real_type* reltol) {
    if (!tolarray) {
        atolarray = new real_type[n];
        rtolarray = new real_type[n];
        tolarray = true;
    }
    memcpy(atolarray, abstol, n * sizeof(real_type));
    memcpy(rtolarray, reltol, n * sizeof(real_type));
}

// ---------------------------------------------------------------------
/** Step initialisation.
 */
real_type DOPRI5::hinit(real_type x, real_type* y, real_type posneg, real_type* f0, real_type* f1, real_type* yy1,
                        int iord, real_type hmax) {
    real_type dnf = 0.0;
    real_type dny = 0.0;
    real_type atoli = abstol(0);
    real_type rtoli = reltol(0);

    if (!tolarray) {
        for (unsigned i = 0; i < n; i++) {
            real_type sk = atoli + rtoli * fabs(y[i]);
            real_type sqr = f0[i] / sk;
            dnf += sqr * sqr;
            sqr = y[i] / sk;
            dny += sqr * sqr;
        }
    } else {
        for (unsigned i = 0; i < n; i++) {
            real_type sk = atolarray[i] + rtolarray[i] * fabs(y[i]);
            real_type sqr = f0[i] / sk;
            dnf += sqr * sqr;
            sqr = y[i] / sk;
            dny += sqr * sqr;
        }
    }

    real_type h;
    if ((dnf <= 1.0E-10) || (dny <= 1.0E-10)) {
        h = 1.0E-6;
    } else {
        h = sqrt(dny / dnf) * 0.01;
    }

    h = std::min(h, hmax);
    h = dsign(h, posneg);

    /* perform an explicit Euler step */
    for (unsigned i = 0; i < n; i++) {
        yy1[i] = y[i] + h * f0[i];
    }
    fcn(n, x + h, yy1, f1);

    /* estimate the second derivative of the solution */
    real_type der2 = 0.0;
    for (unsigned i = 0; i < n; i++) {
        real_type sk = abstol(i) + reltol(i) * fabs(y[i]);
        real_type sqr = (f1[i] - f0[i]) / sk;
        der2 += sqr * sqr;
    }
    der2 = sqrt(der2) / h;

    // step size is computed such that h**iord * max_d(norm(f0),norm(der2)) = 0.01
    real_type der12 = std::max(fabs(der2), sqrt(dnf));
    real_type h1;
    if (der12 <= epsilon) {
        h1 = std::max(1.0E-6, fabs(h) * 1.0E-3);
    } else {
        h1 = pow(0.01 / der12, 1.0 / (real_type)iord);
    }
    h = std::min(100.0 * h, std::min(h1, hmax));

    return dsign(h, posneg);
}  // hinit

// ---------------------------------------------------------------------
/**
 * core integrator
 */
OdeSuccess DOPRI5::dopcor(real_type& x, real_type* y, real_type xend, real_type hmax, real_type h,
                            OdeStepperMode stepflags,
                            unsigned* icont) {
    real_type err, sk, hnew, ydiff, bspl;
    real_type facold = 1.0E-4;
    real_type expo1 = 1.0 / 8.0 - beta * 0.2;
    real_type facc1 = 1.0 / fac1;
    real_type facc2 = 1.0 / fac2;
    real_type posneg = dsign(1.0, xend - x);
    int last = 0;
    real_type hlamb = 0.0;
    int iasti = 0;

    fcn(n, x, y, k1);
    hmax = fabs(hmax);
    int iord = 8;
    if (h == 0.0) {
        h = hinit(x, y, posneg, k1, k2, k3, iord, hmax);
    }

    nfcn += 2;
    int reject = 0;
    xold = x;

    if (stepflags != call_never) {
        irtrn = 1;
        hout = 1.0;
        xout = x;

        irtrn = solout(naccpt + 1, xold, x, y, n);
        if (irtrn < 0) {
            if (verbose)
                printf(message[2], double(x));

            return interrupted;
        }
    }

    // basic integration step
    int nonsti = 0;
    unsigned i, j;
    while (1) {
        if (nstep > nmax) {
            if (verbose)
                fprintf(stderr, message[3], double(x), int(nmax));
            xout = x;
            hout = h;

            return nmax_too_small;
        }

        if (0.1 * fabs(h) <= fabs(x) * uround) {
            if (verbose)
                printf(message[4], double(x), double(h));
            xout = x;
            hout = h;

            return step_size_becomes_too_small;
        }

        if ((x + 1.01 * h - xend) * posneg > 0.0) {
            h = xend - x;
            last = 1;
        }

        ++nstep;

        // the first 6 stages of RK
        for (i = 0; i < n; ++i)
            yy1[i] = y[i] + h * a21 * k1[i];
        fcn(n, x + c2 * h, yy1, k2);
        for (i = 0; i < n; ++i)
            yy1[i] = y[i] + h * (a31 * k1[i] + a32 * k2[i]);
        fcn(n, x + c3 * h, yy1, k3);
        for (i = 0; i < n; ++i)
            yy1[i] = y[i] + h * (a41 * k1[i] + a42 * k2[i] + a43 * k3[i]);
        fcn(n, x + c4 * h, yy1, k4);
        for (i = 0; i < n; ++i)
            yy1[i] = y[i] + h * (a51 * k1[i] + a52 * k2[i] + a53 * k3[i] + a54 * k4[i]);
        fcn(n, x + c5 * h, yy1, k5);
        for (i = 0; i < n; ++i)
            ysti[i] = y[i] + h * (a61 * k1[i] + a62 * k2[i] + a63 * k3[i] + a64 * k4[i] + a65 * k5[i]);
        real_type xph = x + h;
        fcn(n, xph, ysti, k6);
        for (i = 0; i < n; ++i)
            yy1[i] = y[i] + h * (a71 * k1[i] + a73 * k3[i] + a74 * k4[i] + a75 * k5[i] + a76 * k6[i]);
        fcn(n, xph, yy1, k2);
        nfcn += 6;

        if (stepflags == OdeStepperMode::call_for_dense_output) {
            if (nrds == n)
                for (i = 0; i < n; ++i) {
                    rcont5[i] = h * (d1 * k1[i] + d3 * k3[i] + d4 * k4[i] + d5 * k5[i] + d6 * k6[i] + d7 * k2[i]);
                }
            else
                for (j = 0; j < nrds; ++j) {
                    i = icont[j];
                    rcont5[j] = h * (d1 * k1[i] + d3 * k3[i] + d4 * k4[i] + d5 * k5[i] + d6 * k6[i] + d7 * k2[i]);
                }
        }

        for (i = 0; i < n; ++i)
            k4[i] = h * (e1 * k1[i] + e3 * k3[i] + e4 * k4[i] + e5 * k5[i] + e6 * k6[i] + e7 * k2[i]);

        /* error estimation */
        err = 0.0;
        for (i = 0; i < n; ++i) {
            sk = abstol(i) + reltol(i) * std::max(fabs(y[i]), fabs(yy1[i]));
            real_type sqr = k4[i] / sk;
            err += sqr * sqr;
        }
        err = sqrt(err / static_cast<double>(n));

        /* computation of hnew */
        real_type fac11 = pow(err, expo1);
        /* Lund-stabilization */
        real_type fac = fac11 / pow(facold, beta);
        /* we require fac1 <= hnew/h <= fac2 */
        fac = std::max(facc2, std::min(facc1, fac / safe));
        hnew = h / fac;

        if (err <= 1.0) {
            // step accepted
            facold = std::max(err, 1.0E-4);
            naccpt++;

            /* stiffness detection */
            if (!(naccpt % nstiff) || (iasti > 0)) {
                real_type stnum = 0.0;
                real_type stden = 0.0;
                for (i = 0; i < n; i++) {
                    real_type sqr = k4[i] - k3[i];
                    stnum += sqr * sqr;
                    sqr = k5[i] - yy1[i];
                    stden += sqr * sqr;
                }
                if (stden > 0.0)
                    hlamb = h * sqrt(stnum / stden);
                if (hlamb > 3.25) {
                    nonsti = 0;
                    iasti++;
                    if (iasti == 15) {
                        if (verbose)
                            printf(message[5], double(x));
                        else {
                            xout = x;
                            hout = h;
                            return problem_is_probably_stiff;
                        }
                    }
                } else {
                    nonsti++;
                    if (nonsti == 6)
                        iasti = 0;
                }
            }

            /* final preparation for dense output */
            if (stepflags == OdeStepperMode::call_for_dense_output) {
                /* save the first function evaluations */
                if (nrds == n)
                    for (i = 0; i < n; i++) {
                        real_type yd0 = y[i];
                        ydiff = yy1[i] - yd0;
                        bspl = h * k1[i] - ydiff;
                        rcont1[i] = y[i];
                        rcont2[i] = ydiff;
                        rcont3[i] = bspl;
                        rcont4[i] = -h * k2[i] + ydiff - bspl;
                    }
                else
                    for (j = 0; j < nrds; j++) {
                        i = icont[j];
                        real_type yd0 = y[i];
                        ydiff = yy1[i] - yd0;
                        bspl = h * k1[i] - ydiff;
                        rcont1[j] = y[i];
                        rcont2[j] = ydiff;
                        rcont3[j] = bspl;
                        rcont4[j] = -h * k2[i] + ydiff - bspl;
                    }
            }
            /* check this */
            memcpy(k1, k2, n * sizeof(double));
            memcpy(y, yy1, n * sizeof(double));
            xold = x;
            x = xph;
            /* end check this */

            if (stepflags != call_never) {
                hout = h;
                xout = x;

                irtrn = solout(naccpt + 1, xold, x, y, n);
                if (irtrn < 0) {
                    if (verbose)
                        printf(message[2], double(x));

                    return interrupted;
                }
            }

            /* normal exit */
            if (last) {
                hout = hnew;
                xout = x;

                return ok;
            }

            if (fabs(hnew) > hmax)
                hnew = posneg * hmax;
            if (reject)
                hnew = posneg * std::min(fabs(hnew), fabs(h));

            reject = 0;
        } else {
            /* step rejected */
            hnew = h / std::min(facc1, fac11 / safe);
            reject = 1;
            if (naccpt >= 1)
                nrejct = nrejct + 1;
            last = 0;
        }

        h = hnew;
    }
} /* dopcor */

// ---------------------------------------------------------------------
/** Initialisation of arrays
 *
 */
OdeSuccess DOPRI5::Initialise(OdeStepperMode stepflags) {
    nfcn = nstep = naccpt = nrejct = 0;
    indir = NULL;

    // n, the dimension of the system
    if (n == UINT_MAX) {
        if (verbose) {
            printf("System too big, max. n = %u\n", UINT_MAX - 1);
        }
        return inputerror;
    }

    /* nmax, the maximal number of steps */
    if (!nmax) {
        nmax = 100000;
    } else if (nmax <= 0) {
        if (verbose)
            printf("Wrong input, nmax = %i\n", int(nmax));
        return inputerror;
    }

    // meth, coefficients of the method
    if (!meth)
        meth = 1;
    else if ((meth <= 0) || (meth >= 2)) {
        if (verbose)
            printf("Curious input, meth = %i\n", meth);
        return inputerror;
    }

    // nstiff, parameter for stiffness detection
    if (!nstiff)
        nstiff = 1000;
    else if (nstiff < 0)
        nstiff = nmax + 10;

    // nrdens, number of dense output components
    if (nrdens > n) {
        if (verbose)
            printf("Curious input, nrdens = %u\n", nrdens);
        return inputerror;
    } else if (nrdens) {
        // is there enough memory to allocate rcont12345678&indir ?
        if (nrdens < n)
            indir = new unsigned[n];

        if ((!indir && (nrdens < n))) {
            if (verbose)
                printf(message[1]);
            return inputerror;
        }

        /* control of length of icont */
        if (nrdens == n) {
            if (icont && verbose)
                printf("Warning : when nrdens = n there is no need allocating memory for icont\n");
            nrds = n;
        } else {
            if (stepflags != OdeStepperMode::call_for_dense_output && verbose)
                printf("Warning : set stepflags = call_for_dense_output for dense output\n");

            nrds = nrdens;
            for (unsigned i = 0; i < n; i++)
                indir[i] = UINT_MAX;
            for (unsigned i = 0; i < nrdens; i++)
                indir[icont[i]] = i;
        }
    }

    // uround, smallest number such that 1.0 + u > 1.0
    if (uround == 0.0) {
        uround = DBL_EPSILON;
    } else if ((uround <= 1.0E-35) || (uround >= 1.0)) { // FIXME
        if (verbose) {
            printf("Which machine do you have ? Your uround was : %.16e\n", double(uround));
        }
        return inputerror;
    }

    // safety factor
    if (safe == 0.0) {
        safe = 0.9;
    } else if ((safe >= 1.0) || (safe <= 1.0E-4)) {
        if (verbose) {
            printf("Curious input for safety factor, safe = %.16e\n", safe);
        }
        return inputerror;
    }

    /* fac1, fac2, parameters for step size selection */
    if (fac1 == 0.0)
        fac1 = 0.333;
    if (fac2 == 0.0)
        fac2 = 6.0;

    /* beta for step control stabilization */
    if (beta == 0.0)
        beta = 0.0;
    else if (beta < 0.0)
        beta = 0.0;
    else if (beta > 0.2) {
        if (verbose)
            printf("Curious input for beta : beta = %.16e\n", double(beta));
        return inputerror;
    }

    initialized = true;
    return ok;
}

// ---------------------------------------------------------------------
/**
 */
OdeSuccess DOPRI5::solve_normal(real_type& x, real_type* y, real_type xend, OdeStepperMode stepflags, real_type hmax, real_type h) {
    OdeSuccess idid;
    if (!initialized) {
        throw HelioLibException("DOPRI5 must be initialised before calling solve()\n");
    }

    /* initializations */
    nfcn = nstep = naccpt = nrejct = 0;

    /* maximal step size */
    if (hmax == 0.0)
        hmax = xend - x;

    idid = dopcor(x, y, xend, hmax, h, stepflags, icont);

    return idid;
}

// ---------------------------------------------------------------------
/** dense output function
 */
real_type DOPRI5::cont(unsigned ii, real_type x) const {
    unsigned i = UINT_MAX;
    if (!indir) {
        i = ii;
    } else {
        i = indir[ii];
    }
    if (i == UINT_MAX || i >= nrdens) {
        char str[128];
        snprintf(str, sizeof(str), "No dense output available for %uth component\n", ii);
        throw HelioLibException(str);
    }

    real_type s = (x - xold) / hout;
    real_type s1 = 1.0 - s;

    return rcont1[i] + s * (rcont2[i] + s1 * (rcont3[i] + s * (rcont4[i] + s1 * rcont5[i])));
} /* contd5 */
// ---------------------------------------------------------------------
/** dense output function
 */
void DOPRI5::cont(OdePolyData5& data, double x, double* y) {
    real_type s = (x - data.xold) / data.hout;
    real_type s1 = 1.0 - s;
    for (uint i = 0; i < data.size; ++i) {
        y[i] = data.rcont1[i] + s * (data.rcont2[i] + s1 * (data.rcont3[i] + s * (data.rcont4[i] + s1 * data.rcont5[i])));
    }
}
// ---------------------------------------------------------------------
/** dense output function
 */
void DOPRI5::getContinuousPolyData(OdePolyData5& data) {
    if (nrdens < 0) {
        throw HelioLibException("Requesting continuous solution while density is null!\n");
    }
    data.size = nrdens;
    data.xold = xold;
    data.hout = hout;
    data.allocate(nrdens);
    memcpy(data.rcont1, rcont1, nrdens * sizeof(double));
    memcpy(data.rcont2, rcont2, nrdens * sizeof(double));
    memcpy(data.rcont3, rcont3, nrdens * sizeof(double));
    memcpy(data.rcont4, rcont4, nrdens * sizeof(double));
    memcpy(data.rcont5, rcont5, nrdens * sizeof(double));
}
// ---------------------------------------------------------------------
/** dense output function
 */
OdePolyData5 DOPRI5::getContinuousPolyData() {
    if (nrdens < 0) {
        throw HelioLibException("Requesting continuous solution while density is null!\n");
    }
    OdePolyData5 data;
    getContinuousPolyData(data);
    return data;
}
