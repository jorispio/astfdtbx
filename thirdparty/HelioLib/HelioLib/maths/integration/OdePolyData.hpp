// $Id$
// ---------------------------------------------------------------------------
/**
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010-2013 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_INTEGRATION_ODEPOLYDATA_HPP_
#define __SMTBX_INTEGRATION_ODEPOLYDATA_HPP_

#include "core/HelioEnumerations.hpp"
#include "core/HelioDataTypes.hpp"
#include "core/HelioException.hpp"

/** Structure embedding all the ODE solver coefficients for interpolation of a ODE solution.
 */
template<typename ScalarType, int Degree>
class OdePolyData {
 private:
    uint degree;
 public:
    OdePolyData() {
        static_assert((Degree == 5) || (Degree == 8) || (Degree == -1), "Order can only be 5 or 8");
        degree = Degree;
        rcont1 = NULL;
        rcont2 = NULL;
        rcont3 = NULL;
        rcont4 = NULL;
        rcont5 = NULL;
        rcont6 = NULL;
        rcont7 = NULL;
        rcont8 = NULL;
        size = 0;
    }

    ~OdePolyData() {
        if (rcont1) {
            delete rcont1;
        }
        if (rcont2) {
            delete rcont2;
        }
        if (rcont3) {
            delete rcont3;
        }
        if (rcont4) {
            delete rcont4;
        }
        if (rcont5) {
            delete rcont5;
        }
        if (rcont6) {
            delete rcont6;
        }
        if (rcont7) {
            delete rcont7;
        }
        if (rcont8) {
            delete rcont8;
        }
    };

    void allocate(int nrdens) {
        if (rcont1 == NULL) {
            rcont1 = new ScalarType[nrdens];
        }

        if (rcont2 == NULL) {
            rcont2 = new ScalarType[nrdens];
        }

        if (rcont3 == NULL) {
            rcont3 = new ScalarType[nrdens];
        }

        if (rcont4 == NULL) {
            rcont4 = new ScalarType[nrdens];
        }

        if (rcont5 == NULL) {
            rcont5 = new ScalarType[nrdens];
        }

        if (degree > 5) {
            if (rcont6 == NULL) {
                rcont6 = new ScalarType[nrdens];
            }
            if (rcont7 == NULL) {
                rcont7 = new ScalarType[nrdens];
            }
            if (rcont8 == NULL) {
                rcont8 = new ScalarType[nrdens];
            }
        }
    }

    void cont5(ScalarType x, ScalarType* y) {
        if (degree != 5) {
            throw HelioLibException("Using wrong order for dense output! Expecting degree 5.");
        }

        ScalarType s = (x - xold) / hout;
        ScalarType s1 = 1.0 - s;
        for (uint i = 0; i < size; ++i) {
            y[i] = rcont1[i] + s * (rcont2[i] + s1 * (rcont3[i] + s * (rcont4[i] + s1 * rcont5[i])));
        }
    }

    ODE_SOLVER_ID solver_type;

    ScalarType* rcont1;
    ScalarType* rcont2;
    ScalarType* rcont3;
    ScalarType* rcont4;
    ScalarType* rcont5;
    ScalarType* rcont6;
    ScalarType* rcont7;
    ScalarType* rcont8;
    ScalarType xold;
    ScalarType hout;
    uint size;
};

// ---------------------------------------------------------------------------
#endif  // __SMTBX_INTEGRATION_ODEPOLYDATA_HPP_
