﻿/* $Id: AngularCoordinates.hpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SMTBX_ANGULAR_COORDS_HPP_
#define __SMTBX_ANGULAR_COORDS_HPP_

#include "maths/coordinates/CartesianCoordinates.hpp"
#include "maths/RotationQuaternion.hpp"

struct CartesianCoordinates;

/** Simple container for rotation/rotation rate/rotation acceleration triplets.
 */
class AngularCoordinates {
    RotationQuaternion rotation;
    Vector3dExt rotationRate;
    Vector3dExt rotationAcceleration;

 public:
    static const AngularCoordinates IDENTITY;

    AngularCoordinates(const RotationQuaternion& rotation = RotationQuaternion::IDENTITY,
                        const Vector3dExt& rotationRate = Vector3dExt::ZERO,
                        const Vector3dExt& rotationAcceleration = Vector3dExt::ZERO);
    AngularCoordinates(const Vector3dExt& u, const Vector3dExt& v);

    /** Estimate rotation rate between two orientations */
    static Vector3dExt estimateRate(const RotationQuaternion& start, const RotationQuaternion& end, double dt);

    /** Inverse the angular coordinate */
    AngularCoordinates negate() const;

    /** Get a time-shifted state*/
    AngularCoordinates propagate(double dt) const;

    /** Get the rotation. */
    RotationQuaternion getRotationQuaternion() const;

    /** Get the rotation rate */
    Vector3dExt getRotationQuaternionRate() const;

    /** Get the rotation acceleration.*/
    Vector3dExt getRotationQuaternionAcceleration() const;

    /** Add an offset from the instance */
    AngularCoordinates operator+(const AngularCoordinates& offset) const {
        Vector3dExt rOmega = rotation * offset.rotationRate;
        Vector3dExt rOmegaDot = rotation * offset.rotationAcceleration;
        return AngularCoordinates(rotation * offset.rotation,
                                  rotationRate + rOmega,
                                  rotationAcceleration + rOmegaDot - rotationRate.cross(rOmega));
    }

    /** Subtract an offset from current coordinates */
    AngularCoordinates operator-(const AngularCoordinates& offset) const {
        return *this + offset.negate();
    }

    friend CartesianCoordinates operator*(const AngularCoordinates& angularCoords, const CartesianCoordinates& pv);
};

CartesianCoordinates operator*(const AngularCoordinates& angularCoords, const CartesianCoordinates& pv);
// ---------------------------------------------------------------------
#endif  // __SMTBX_ANGULAR_COORDS_HPP_
