// $Id: SphericalCoordinates.hpp 132 2013-12-29 20:57:47Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_SPHERICAL_COORDINATES_HPP_
#define __SMTBX_SPHERICAL_COORDINATES_HPP_

#include <math.h>

#include "SpaceMecaLibDataTypes.hpp"
#include "maths/Vector3dExt.hpp"

class SphericalCoordinates {
 private:
    Vector3dExt vCartesian;
    /** vector radius */
    double r;

    /** polar angle, or inclination.
     * Note in some cases, the declination is 90 - inclination.
     * Declination, like a celestial latitude.
 */
    double theta;

    /** azimuth
     * Note in some cases, the azimuth is the right ascension
     * Right Ascension, like a celestial longitude.
     */
    double phi;

    Matrix3d jacobian;
    bool bJacobianSet;

    Matrix3d rHessian;
    Matrix3d phiHessian;
    Matrix3d thetaHessian;
    bool bHessianSet;

    /** Lazy evaluation of (r, &theta;, &phi;) Jacobian.
     */
    void computeJacobian()
    {
        if (!bJacobianSet) {

            // intermediate variables
            double x = vCartesian.getX();
            double y = vCartesian.getY();
            double z = vCartesian.getZ();
            double rho2 = x * x + y * y;
            double rho = sqrt(rho2);
            double r2 = rho2 + z * z;

            // jacobian = new double[3][3];

            // row representing the gradient of r
            jacobian(0, 0) = x / r;
            jacobian(0, 1) = y / r;
            jacobian(0, 2) = z / r;

            // row representing the gradient of theta
            jacobian(1, 0) = -y / rho2;
            jacobian(1, 1) = x / rho2;
            jacobian(1, 2) = 0;

            // row representing the gradient of phi
            jacobian(2, 0) = x * z / (rho * r2);
            jacobian(2, 1) = y * z / (rho * r2);
            jacobian(2, 2) = -rho / r2;

            bJacobianSet = true;
        }
    }

    /** Lazy evaluation of Hessians.
     */
    void computeHessians()
    {

        if (!bHessianSet) {

            // intermediate variables
            double x = vCartesian.getX();
            double y = vCartesian.getY();
            double z = vCartesian.getZ();
            double x2 = x * x;
            double y2 = y * y;
            double z2 = z * z;
            double rho2 = x2 + y2;
            double rho = sqrt(rho2);
            double r2 = rho2 + z2;
            double xOr = x / r;
            double yOr = y / r;
            double zOr = z / r;
            double xOrho2 = x / rho2;
            double yOrho2 = y / rho2;
            double xOr3 = xOr / r2;
            double yOr3 = yOr / r2;
            double zOr3 = zOr / r2;

            // lower-left part of Hessian of r
            // rHessian = new double[3][3];
            rHessian(0, 0) = y * yOr3 + z * zOr3;
            rHessian(1, 0) = -x * yOr3;
            rHessian(2, 0) = -z * xOr3;
            rHessian(1, 1) = x * xOr3 + z * zOr3;
            rHessian(2, 1) = -y * zOr3;
            rHessian(2, 2) = x * xOr3 + y * yOr3;

            // upper-right part is symmetric
            rHessian(0, 1) = rHessian(1, 0);
            rHessian(0, 2) = rHessian(2, 0);
            rHessian(1, 2) = rHessian(2, 1);

            // lower-left part of Hessian of azimuthal angle theta
            // thetaHessian = new double[2][2];
            thetaHessian(0, 0) = 2 * xOrho2 * yOrho2;
            thetaHessian(1, 0) = yOrho2 * yOrho2 - xOrho2 * xOrho2;
            thetaHessian(1, 1) = -2 * xOrho2 * yOrho2;

            // upper-right part is symmetric
            thetaHessian(0, 1) = thetaHessian(1, 0);

            // lower-left part of Hessian of polar (co-latitude) angle phi
            double rhor2 = rho * r2;
            double rho2r2 = rho * rhor2;
            double rhor4 = rhor2 * r2;
            double rho3r4 = rhor4 * rho2;
            double r2P2rho2 = 3 * rho2 + z2;
            // phiHessian = new double[3][3];
            phiHessian(0, 0) = z * (rho2r2 - x2 * r2P2rho2) / rho3r4;
            phiHessian(1, 0) = -x * y * z * r2P2rho2 / rho3r4;
            phiHessian(2, 0) = x * (rho2 - z2) / rhor4;
            phiHessian(1, 1) = z * (rho2r2 - y2 * r2P2rho2) / rho3r4;
            phiHessian(2, 1) = y * (rho2 - z2) / rhor4;
            phiHessian(2, 2) = 2 * rho * zOr3 / r;

            // upper-right part is symmetric
            phiHessian(0, 1) = phiHessian(1, 0);
            phiHessian(0, 2) = phiHessian(2, 0);
            phiHessian(1, 2) = phiHessian(2, 1);

            bHessianSet = true;
        }
    }

 public:
    SphericalCoordinates(const Vector3d& cart) {
        // Cartesian coordinates
        this->vCartesian = cart;

        // remaining spherical coordinates
        r = vCartesian.norm();
        theta = vCartesian.getAlpha();
        phi = acos(vCartesian.getZ() / r);

        bJacobianSet = false;
        bHessianSet = false;
    }

    /**
     * @brief
     * @param r         vector length
     * @param theta  polar angle, or inclination, radians
     * @param phi    azimuth, radians
     * @return
     */
    SphericalCoordinates(double r, double theta, double phi) {

        // spherical coordinates
        this->r = r;
        this->theta = theta;
        this->phi = phi;

        double cosTheta = cos(theta);
        double sinTheta = sin(theta);
        double cosPhi = cos(phi);
        double sinPhi = sin(phi);

        // Cartesian coordinates
        vCartesian = Vector3dExt(r * cosTheta * sinPhi, r * sinTheta * sinPhi, r * cosPhi);

        bJacobianSet = false;
        bHessianSet = false;
    }

    /** Get the Cartesian coordinates.
     * @return Cartesian coordinates
     */
    Vector3dExt getCartesian() {
        return vCartesian;
    }

    /** Get the radius.*/
    double getR() {
        return r;
    }

    /** Get the azimuthal angle in x-y plane.
     * @return azimuthal angle in x-y plane &theta;()
     */
    double getTheta() {
        return theta;
    }

    /** Get the polar (co-latitude) angle. */
    double getPhi() {
        return phi;
    }

    /** Convert a gradient with respect to spherical coordinates into a gradient
     * with respect to Cartesian coordinates.
     * @param sGradient gradient with respect to spherical coordinates
     * {df/dr, df/d&theta;, df/d&Phi;}
     * @return gradient with respect to Cartesian coordinates
     * {df/dx, df/dy, df/dz}
     */
    Vector3d toCartesianGradient(const Vector3d& sGradient) {

        // lazy evaluation of Jacobian
        computeJacobian();

        // compose derivatives as gradient^T . J
        // the expressions have been simplified since we know jacobian[1][2] = dTheta/dZ = 0
        return jacobian * sGradient;
    }

    /** Convert a Hessian with respect to spherical coordinates into a Hessian
     * with respect to Cartesian coordinates.
     *
     * As Hessian are always symmetric, we use only the lower left part of the provided
     * spherical Hessian, so the upper part may not be initialized. However, we still
     * do fill up the complete array we create, with guaranteed symmetry.
     *
     * @param sHessian Hessian with respect to spherical coordinates
     * {{d<sup>2</sup>f/dr<sup>2</sup>, d<sup>2</sup>f/drd&theta;, d<sup>2</sup>f/drd&Phi;},
     *  {d<sup>2</sup>f/drd&theta;, d<sup>2</sup>f/d&theta;<sup>2</sup>, d<sup>2</sup>f/d&theta;d&Phi;},
     *  {d<sup>2</sup>f/drd&Phi;, d<sup>2</sup>f/d&theta;d&Phi;, d<sup>2</sup>f/d&Phi;<sup>2</sup>}
     * @param sGradient gradient with respect to spherical coordinates
     * {df/dr, df/d&theta;, df/d&Phi;}
     * @return Hessian with respect to Cartesian coordinates
     * {{d<sup>2</sup>f/dx<sup>2</sup>, d<sup>2</sup>f/dxdy, d<sup>2</sup>f/dxdz},
     *  {d<sup>2</sup>f/dxdy, d<sup>2</sup>f/dy<sup>2</sup>, d<sup>2</sup>f/dydz},
     *  {d<sup>2</sup>f/dxdz, d<sup>2</sup>f/dydz, d<sup>2</sup>f/dz<sup>2</sup>}}
     */
    Matrix3d toCartesianHessian(const Matrix3d& sHessian, const Vector3& sGradient) {
        computeJacobian();
        computeHessians();

        // compose derivative as J^T . H_f . J + df/dr H_r + df/dtheta H_theta + df/dphi H_phi
        // the expressions have been simplified since we know jacobian[1][2] = dTheta/dZ = 0
        // and H_theta is only a 2x2 matrix as it does not depend on z

        // compute H_f . J
        /*return jacobian * sHessian * jacobian.transpose()
                //            + sGradient.transpose() * (rHessian + thetaHessian + phiHessian);
        */
        Matrix3 hj;
        Matrix3 cHessian;

        // beware we use ONLY the lower-left part of sHessian
        hj(0, 0) = sHessian(0, 0) * jacobian(0, 0) + sHessian(1, 0) * jacobian(1, 0) + sHessian(2, 0) * jacobian(2, 0);
        hj(0, 1) = sHessian(0, 0) * jacobian(0, 1) + sHessian(1, 0) * jacobian(1, 1) + sHessian(2, 0) * jacobian(2, 1);
        hj(0, 2) = sHessian(0, 0) * jacobian(0, 2) + sHessian(2, 0) * jacobian(2, 2);
        hj(1, 0) = sHessian(1, 0) * jacobian(0, 0) + sHessian(1, 1) * jacobian(1, 0) + sHessian(2, 1) * jacobian(2, 0);
        hj(1, 1) = sHessian(1, 0) * jacobian(0, 1) + sHessian(1, 1) * jacobian(1, 1) + sHessian(2, 1) * jacobian(2, 1);
        // don't compute hj[1][2] as it is not used below
        hj(2, 0) = sHessian(2, 0) * jacobian(0, 0) + sHessian(2, 1) * jacobian(1, 0) + sHessian(2, 2) * jacobian(2, 0);
        hj(2, 1) = sHessian(2, 0) * jacobian(0, 1) + sHessian(2, 1) * jacobian(1, 1) + sHessian(2, 2) * jacobian(2, 1);
        hj(2, 2) = sHessian(2, 0) * jacobian(0, 2) + sHessian(2, 2) * jacobian(2, 2);

        // compute lower-left part of J^T . H_f . J
        cHessian(0, 0) = jacobian(0, 0) * hj(0, 0) + jacobian(1, 0) * hj(1, 0) + jacobian(2, 0) * hj(2, 0);
        cHessian(1, 0) = jacobian(0, 1) * hj(0, 0) + jacobian(1, 1) * hj(1, 0) + jacobian(2, 1) * hj(2, 0);
        cHessian(2, 0) = jacobian(0, 2) * hj(0, 0) + jacobian(2, 2) * hj(2, 0);
        cHessian(1, 1) = jacobian(0, 1) * hj(0, 1) + jacobian(1, 1) * hj(1, 1) + jacobian(2, 1) * hj(2, 1);
        cHessian(2, 1) = jacobian(0, 2) * hj(0, 1) + jacobian(2, 2) * hj(2, 1);
        cHessian(2, 2) = jacobian(0, 2) * hj(0, 2) + jacobian(2, 2) * hj(2, 2);

        // add gradient contribution
        cHessian(0, 0) +=
            sGradient(0) * rHessian(0, 0) + sGradient(1) * thetaHessian(0, 0) + sGradient(2) * phiHessian(0, 0);
        cHessian(1, 0) +=
            sGradient(0) * rHessian(1, 0) + sGradient(1) * thetaHessian(1, 0) + sGradient(2) * phiHessian(1, 0);
        cHessian(2, 0) += sGradient(0) * rHessian(2, 0) + sGradient(2) * phiHessian(2, 0);
        cHessian(1, 1) +=
            sGradient(0) * rHessian(1, 1) + sGradient(1) * thetaHessian(1, 1) + sGradient(2) * phiHessian(1, 1);
        cHessian(2, 1) += sGradient(0) * rHessian(2, 1) + sGradient(2) * phiHessian(2, 1);
        cHessian(2, 2) += sGradient(0) * rHessian(2, 2) + sGradient(2) * phiHessian(2, 2);

        // ensure symmetry
        cHessian(0, 1) = cHessian(1, 0);
        cHessian(0, 2) = cHessian(2, 0);
        cHessian(1, 2) = cHessian(2, 1);

        return cHessian;
    }

    /* Convert a gradient with respect to spherical coordinates into a gradient with respect to Cartesian coordinates.
     * input:
     *   sGradient     gradient with respect to spherical coordinates {df/dr, df/d?, df/dF}
     *
     * Return
     *    gradient with respect to Cartesian coordinates {df/dx, df/dy, df/dz}
     */
    Vector3dExt toCartesianGradient(const Vector3dExt& sGradient) {
        // lazy evaluation of Jacobian
        computeJacobian();

        // compose derivatives as gradient^T . J
        // the expressions have been simplified since we know jacobian[1][2] = dTheta/dZ = 0
        return jacobian * sGradient;
    }
};
#endif  // __SMTBX_SPHERICAL_COORDINATES_HPP_
