/* $Id: CartesianCoordinates.hpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_CORE_CARTESIAN_COORDINATES_HPP_
#define __SMTBX_CORE_CARTESIAN_COORDINATES_HPP_

#include "core/HelioDataTypes.hpp"
#include "maths/Vector3dExt.hpp"

class Frame;  // forward declaration

struct CartesianCoordinates {
 private:
    Vector3dExt r;
    Vector3dExt v;
    Vector3dExt a;

 public:
    static const CartesianCoordinates ZERO;

    CartesianCoordinates();
    CartesianCoordinates(const Vector3dExt& pos, const Vector3dExt& vel);
    CartesianCoordinates(const Vector3dExt& pos, const Vector3dExt& vel, const Vector3dExt& acc);
    CartesianCoordinates(const CartesianCoordinates& pv)
                : CartesianCoordinates(pv.getPosition(), pv.getVelocity(), pv.getAcceleration()) { }  // c++11
    ~CartesianCoordinates() { }

    void setPV(const CartesianCoordinates& coord);

    void setPosition(const Vector3dExt& pos);

    void setVelocity(const Vector3dExt& vel);

    Vector3dExt getPosition() const;
    Vector3dExt& getPosition();

    Vector3dExt getVelocity() const;
    Vector3dExt& getVelocity();

    Vector3dExt getAcceleration() const;
    Vector3dExt& getAcceleration();

    Vector3dExt getMomentum() const;

    CartesianCoordinates getCoordinates(const Frame& frame);
    
    // addition of two CartessianCoordinates (p1+ p2, v1 + v2)
    CartesianCoordinates operator+(const CartesianCoordinates& pv2) {
        Vector3dExt pos = getPosition() + pv2.getPosition();
        Vector3dExt vel = getVelocity() + pv2.getVelocity();
        return CartesianCoordinates(pos, vel);
    }
};

#endif  // _SMTBX_CORE_CARTESIAN_COORDINATES_HPP
