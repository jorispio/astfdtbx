/* $Id: CartesianCoordinates.cpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CartesianCoordinates.hpp"

// #define DEBUG_CARTESIAN_COORDINATES

const CartesianCoordinates CartesianCoordinates::ZERO = CartesianCoordinates();

bool operator==(const CartesianCoordinates& ref, const CartesianCoordinates& cmp) {
    if ((ref.getPosition() == cmp.getPosition()) && (ref.getVelocity() == cmp.getVelocity()))
        return true;
    return false;
}

// ---------------------------------------------------------------------
CartesianCoordinates::CartesianCoordinates()
    : r(Vector3dExt::Zero()), v(Vector3dExt::Zero()), a(Vector3dExt::Zero()) { }
// ---------------------------------------------------------------------
CartesianCoordinates::CartesianCoordinates(const Vector3dExt& pos, const Vector3dExt& vel)
    : r(pos), v(vel), a(Vector3dExt::Zero()) {
#ifdef DEBUG_CARTESIAN_COORDINATES
    printf("CartesianCoordinates((%f %f %f), (%f %f %f))\n", pos(0), pos(1), pos(2), vel(0), vel(1), vel(2));
    printf("CartesianCoordinates((%f %f %f), (%f %f %f))\n", r(0), r(1), r(2), v(0), v(1), v(2));
#endif
}
// ---------------------------------------------------------------------
CartesianCoordinates::CartesianCoordinates(const Vector3dExt& pos, const Vector3dExt& vel, const Vector3dExt& acc)
    : r(pos), v(vel), a(acc) {
#ifdef DEBUG_CARTESIAN_COORDINATES
    printf("CartesianCoordinates((%f %f %f), (%f %f %f))\n", pos(0), pos(1), pos(2), vel(0), vel(1), vel(2));
    printf("CartesianCoordinates((%f %f %f), (%f %f %f))\n", r(0), r(1), r(2), v(0), v(1), v(2));
#endif
}

// ---------------------------------------------------------------------
void CartesianCoordinates::setPV(const CartesianCoordinates& coord) {
    setPosition(coord.getPosition());
    setVelocity(coord.getVelocity());
}
// ---------------------------------------------------------------------
void CartesianCoordinates::setPosition(const Vector3dExt& pos) {
    r = pos;
}
// ---------------------------------------------------------------------
void CartesianCoordinates::setVelocity(const Vector3dExt& vel) {
    v = vel;
}

Vector3dExt CartesianCoordinates::getPosition() const {
    return r;
}

Vector3dExt& CartesianCoordinates::getPosition() {
    return r;
}

Vector3dExt CartesianCoordinates::getVelocity() const {
    return v;
}

Vector3dExt& CartesianCoordinates::getVelocity() {
    return v;
}

Vector3dExt CartesianCoordinates::getAcceleration() const {
    return a;
}

Vector3dExt& CartesianCoordinates::getAcceleration() {
    return a;
}

Vector3dExt CartesianCoordinates::getMomentum() const {
    return (Vector3dExt)(r.cross(v));
}
// ---------------------------------------------------------------------
CartesianCoordinates CartesianCoordinates::getCoordinates(const Frame&) {
    return *this;
}
