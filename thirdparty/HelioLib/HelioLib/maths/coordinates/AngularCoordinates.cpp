/* $Id: AngularCoordinates.cpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "AngularCoordinates.hpp"

#include <algorithm>

#include "core/HelioException.hpp"
#include "maths/MathUtils.hpp"

const AngularCoordinates AngularCoordinates::IDENTITY =
    AngularCoordinates(RotationQuaternion::IDENTITY, Vector3dExt::Zero(), Vector3dExt::Zero());

// ---------------------------------------------------------------------
/** Builds a angular coordinate defined with rotation / rotation rate / rotation acceleration.
 * @param rotation angular position
 * @param rotationRate angular velocity (rad/s)
 * @param rotationAcceleration angular acceleration (rad²/s²)
 */
AngularCoordinates::AngularCoordinates(const RotationQuaternion& rotation_,
                                       const Vector3dExt& rotationRate_,
                                       const Vector3dExt& rotationAcceleration_)
    : rotation(rotation_), rotationRate(rotationRate_), rotationAcceleration(rotationAcceleration_) { }
// ---------------------------------------------------------------------
/** Build a candidate static rotation that transform one vector u into another vector v.
 *
 * @param u origin vector
 * @param v desired image of u by the rotation
 */
AngularCoordinates::AngularCoordinates(const Vector3dExt& u, const Vector3dExt& v)
    : rotation(RotationQuaternion(u, v)),
    rotationRate(Vector3dExt::ZERO) , rotationAcceleration(Vector3dExt::ZERO) { }
// ---------------------------------------------------------------------
/** Estimate rotation rate between two orientations start and end, during dt.
 * @return rotation rate that brings a rotation from start to end in dt time
 */
Vector3dExt AngularCoordinates::estimateRate(const RotationQuaternion& startAttitude, const RotationQuaternion& endAttitude, double dt) {
    RotationQuaternion deltaRotation = startAttitude * endAttitude.inverse();
    return Vector3dExt(deltaRotation.getAngle() / dt, deltaRotation.getAxis());
}
// ---------------------------------------------------------------------
/** Return the AngularCoordinates inverse (e.g. inverse rotation, opposite rate and acceleration) */
AngularCoordinates AngularCoordinates::negate() const {
    return AngularCoordinates(
        rotation.inverse(), -rotation.applyInverseTo(rotationRate), -rotation.applyInverseTo(rotationAcceleration));
}
// ---------------------------------------------------------------------
/** Get a time-shifted state.
 *
 * The state can be slightly shifted to close dates. This shift is based on
 * an approximate solution of the fixed acceleration motion. It is <em>not</em>
 * intended as a replacement for proper attitude propagation but should be
 * sufficient for either small time shifts or coarse accuracy.
 *
 * @param dt time shift in seconds
 * @return a new state, shifted with respect to the instance (which is immutable)
 */
AngularCoordinates AngularCoordinates::propagate(double dt) const {
    double rate = rotationRate.norm();
    RotationQuaternion rateContribution = (fabs(rate) < epsilon) ? RotationQuaternion::IDENTITY : RotationQuaternion(rotationRate, -rate * dt);

    AngularCoordinates linearPart = AngularCoordinates(rateContribution * rotation, rotationRate);
    double acc = rotationAcceleration.norm();
    if (acc < epsilon) {
        return linearPart;
    }

    AngularCoordinates quadraticContribution = AngularCoordinates(
        RotationQuaternion(rotationAcceleration, -0.5 * acc * dt * dt), Vector3dExt(dt, rotationAcceleration), rotationAcceleration);

    return quadraticContribution + linearPart;
}
// ---------------------------------------------------------------------
/** Get the rotation.
 */
RotationQuaternion AngularCoordinates::getRotationQuaternion() const {
    return rotation;
}
// ---------------------------------------------------------------------
/** Get the rotation rate (rad/s) */
Vector3dExt AngularCoordinates::getRotationQuaternionRate() const {
    return rotationRate;
}
// ---------------------------------------------------------------------
/** Get the rotation acceleration  (rad²/s²). */
Vector3dExt AngularCoordinates::getRotationQuaternionAcceleration() const {
    return rotationAcceleration;
}
// ---------------------------------------------------------------------
CartesianCoordinates operator*(const AngularCoordinates& angularCoords, const CartesianCoordinates& pv) {
    Vector3dExt newPosition = angularCoords.rotation * pv.getPosition();

    Vector3dExt crossPos = angularCoords.rotationRate.cross(newPosition);
    Vector3dExt newVelocity = angularCoords.rotation * pv.getVelocity() - crossPos;

    Vector3dExt crossV = angularCoords.rotationRate.cross(newVelocity);
    Vector3dExt crossCrossPos = angularCoords.rotationRate.cross(crossPos);
    Vector3dExt crossDotPos = angularCoords.rotationAcceleration.cross(newPosition);
    Vector3dExt newAcceleration = angularCoords.rotation * pv.getAcceleration() - 2 * crossV - crossCrossPos - crossDotPos;

    return CartesianCoordinates(newPosition, newVelocity, newAcceleration);
}