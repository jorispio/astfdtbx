// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 */
#ifndef __SMTBX_MATHS_ARRAY_HPP_
#define __SMTBX_MATHS_ARRAY_HPP_

namespace HelioLib {
namespace MathsArray {

    template <typename T>
    T maxArray(T* a, int n) {
        T vmax = a[0];
        for (int i=0; i<n; ++i) {
            if (a[i] > vmax) vmax = a[i];
        }
        return vmax;
    }

    template <typename T>
    T minArray(T* a, int n) {
        T vmin = a[0];
        for (int i=0; i<n; ++i) {
            if (a[i] < vmin) vmin = a[i];
        }
        return vmin;
    }

    template <typename T>
    T sumArray(T* a, int n) {
        T sum = a[0];
        for (int i=1; i<n; ++i) {
            sum += a[i];
        }
        return sum;
    }
}
}  // namespace HelioLib
//---------------------------------------------------------------------------
#endif  // __SMTBX_MATHS_ARRAY_HPP_
