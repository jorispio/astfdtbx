// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Polynomial.hpp"

#include <vector>

// --------------------------------------------------------------------------
/**
 */
Polynomial::Polynomial() : n(0), order(0) {
}

// --------------------------------------------------------------------------
/**
 * @param coefficients arrays of coefficients, starting from the lowest order
 * @param n number of coefficients
 */
Polynomial::Polynomial(double coefficients[], int n)
    : n(n), order(n-1), coefficients(coefficients) {
}

// --------------------------------------------------------------------------
/**
 */
double Polynomial::eval(double x) {
    double value = coefficients[0];
    for (int i = 1; i < n; ++i) {
        value += coefficients[i] * pow(x, i);
    }
    return value;
}

// --------------------------------------------------------------------------
/**
 */
// --------------------------------------------------------------------------
std::complex<double> Polynomial::eval(std::complex<double> x) {
    std::complex<double> value = coefficients[0];
    for (int i = 1; i < n; ++i) {
        value += coefficients[i] * pow(x, i);
    }
    return value;
}

// --------------------------------------------------------------------------
/** Compute the companion matrix of the monic polynomial
 *      p(x) = a0 + sum(a_i * x^i, i=1, n)
 */
// --------------------------------------------------------------------------
MATRIXVAR Polynomial::getCompanionMatrix() {
    MATRIXVAR matComp = MATRIXVAR::Zero(order, order);
    for (int idx = 0; idx < order; ++idx) {
        if (idx < order - 1)
            matComp(1+idx, idx) = 1;
        matComp(idx, order - 1) =-coefficients[idx] / coefficients[n-1];
    }
    return matComp;
}
// --------------------------------------------------------------------------
/** find polynomial roots
 */
// --------------------------------------------------------------------------
std::vector<std::complex<double> > Polynomial::getRoots() {
    VectorXcd diagonal = getCompanionMatrix().eigenvalues();

    std::vector<std::complex<double> >  roots;
    for (int i = 0; i < order; ++i) {
        roots.push_back(diagonal(i));
    }

    return roots;
}
