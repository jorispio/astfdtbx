/* $Id: RotationQuaternion.cpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "RotationQuaternion.hpp"

#include <cmath>

#include "core/HelioException.hpp"
#include "maths/MathUtils.hpp"

// ---------------------------------------------------------------------------------------
const RotationQuaternion RotationQuaternion::IDENTITY = RotationQuaternion(Vector3dExt(1, 0, 0), 0.);

// ---------------------------------------------------------------------------------------
/**
 * Construct a rotation with quaternion
 */
RotationQuaternion::RotationQuaternion(double q0_, double q1_, double q2_, double q3_, bool normalized)
    : Eigen::Quaterniond(q0_, q1_, q2_, q3_) {
    double norm = 1;
    if (!normalized) {
        norm = sqrt(q0_ * q0_ + q1_ * q1_ + q2_ * q2_ + q3_ * q3_);
        if (norm < 1e-12) {
            std::cout << "RotationQuaternion Q=" << q0_ << "  " << q1_ << "  " << q2_ << "  " << q3_ << "  " << std::endl;
            throw HelioLibException("Invalid quaternion for a rotation!");
        }
    }
    this->q0 = q0_ / norm;
    this->q1 = q1_ / norm;
    this->q2 = q2_ / norm;
    this->q3 = q3_ / norm;
}
// ---------------------------------------------------------------------
/** Construct a rotation of a given angle around one axis
 * Convention:  angles are oriented according to the effect of the rotation on
 * vectors around the axis.
 * Example: for the direct base (i, j, k),
 * the roation RotationQuaternion(i, pi/2) transforms +j to +k.
 */
RotationQuaternion::RotationQuaternion(const Vector3dExt& axis, double angle) : Eigen::Quaterniond() {
    double norm = axis.norm();
    if (norm < epsilon) {
        char str[255];
        snprintf(str, sizeof(str),
                "RotationQuaternion: ZERO_NORM_FOR_ROTATION_AXIS, angle=%f, axis=[%f, %f, %f]",
                angle,
                axis(0),
                axis(1),
                axis(2));
        throw HelioLibException(str);
    }

    double halfAngle = -0.5 * angle;
    double coeff = sin(halfAngle) / norm;

    q0 = cos(halfAngle);
    q1 = coeff * axis.getX();
    q2 = coeff * axis.getY();
    q3 = coeff * axis.getZ();
    m_coeffs << q0, q1, q2, q3;
}
// ---------------------------------------------------------------------
/** Construct the rotation between two vectors.
 */
RotationQuaternion::RotationQuaternion(const Vector3dExt& u, const Vector3dExt& v)
    : Eigen::Quaterniond() {
    double normProduct = u.norm() * v.norm();
    if (normProduct < epsilon) {
        throw HelioLibException("At least one vector is of size zero.");
    }

    double dot = u.dot(v);

    if (dot < ((2.0e-15 - 1.0) * normProduct)) {
        // special case u = -v: we select a PI angle rotation around
        // an arbitrary vector orthogonal to u
        Vector3dExt w = u.orthogonal();
        q0 = 0.0;
        q1 = -w.getX();
        q2 = -w.getY();
        q3 = -w.getZ();
    } else {
        // general case: (u, v) defines a plane, we select
        // the shortest possible rotation: axis orthogonal to this plane
        q0 = sqrt(0.5 * (1.0 + dot / normProduct));
        double coeff = 1.0 / (2.0 * q0 * normProduct);
        Vector3dExt q = v.cross(u);
        q1 = coeff * q.getX();
        q2 = coeff * q.getY();
        q3 = coeff * q.getZ();
    }
    m_coeffs << q0, q1, q2, q3;
}
// ---------------------------------------------------------------------
/** Get the scalar coordinate of the quaternion.
 * @return scalar coordinate of the quaternion
 */
double RotationQuaternion::getQ0() {
    return q0;
}

/** Get the first coordinate of the vectorial part of the quaternion.
 * @return first coordinate of the vectorial part of the quaternion
 */
double RotationQuaternion::getQ1() {
    return q1;
}

/** Get the second coordinate of the vectorial part of the quaternion.
 * @return second coordinate of the vectorial part of the quaternion
 */
double RotationQuaternion::getQ2() {
    return q2;
}

/** Get the third coordinate of the vectorial part of the quaternion.
 * @return third coordinate of the vectorial part of the quaternion
 */
double RotationQuaternion::getQ3() {
    return q3;
}

/** Get the normalized axis of the rotation.
 */
Vector3dExt RotationQuaternion::getAxis() {
    double squaredSine = q1 * q1 + q2 * q2 + q3 * q3;
    if (squaredSine < epsilon) {
        return Vector3dExt(1, 0, 0);
    } else if (q0 < 0) {
        double inverse = 1 / sqrt(squaredSine);
        return Vector3dExt(q1 * inverse, q2 * inverse, q3 * inverse);
    }
    double inverse = -1 / sqrt(squaredSine);
    return Vector3dExt(q1 * inverse, q2 * inverse, q3 * inverse);
}

/** Get the angle of the rotation  (between 0 and pi;)
 */
double RotationQuaternion::getAngle() {
    if ((q0 < -0.1) || (q0 > 0.1)) {
        return 2 * asin(sqrt(q1 * q1 + q2 * q2 + q3 * q3));
    } else if (q0 < 0) {
        return 2 * acos(-q0);
    }
    return 2 * acos(q0);
}

/**
 * @brief
 * @return
 */
Matrix3d RotationQuaternion::getMatrix() {
    // products
    double q0q0 = q0 * q0;
    double q0q1 = q0 * q1;
    double q0q2 = q0 * q2;
    double q0q3 = q0 * q3;
    double q1q1 = q1 * q1;
    double q1q2 = q1 * q2;
    double q1q3 = q1 * q3;
    double q2q2 = q2 * q2;
    double q2q3 = q2 * q3;
    double q3q3 = q3 * q3;

    // create the matrix
    Matrix3d m;
    m(0, 0) = 2.0 * (q0q0 + q1q1) - 1.0;
    m(1, 0) = 2.0 * (q1q2 - q0q3);
    m(2, 0) = 2.0 * (q1q3 + q0q2);

    m(0, 1) = 2.0 * (q1q2 + q0q3);
    m(1, 1) = 2.0 * (q0q0 + q2q2) - 1.0;
    m(2, 1) = 2.0 * (q2q3 - q0q1);

    m(0, 2) = 2.0 * (q1q3 - q0q2);
    m(1, 2) = 2.0 * (q2q3 + q0q1);
    m(2, 2) = 2.0 * (q0q0 + q3q3) - 1.0;

    return m;
}
// ---------------------------------------------------------------------
Vector3dExt RotationQuaternion::applyTo(const Vector3dExt& u) const {
    double x = u.getX();
    double y = u.getY();
    double z = u.getZ();

    double s = q1 * x + q2 * y + q3 * z;

    return Vector3dExt(2 * (q0 * (x * q0 - (q2 * z - q3 * y)) + s * q1) - x,
                    2 * (q0 * (y * q0 - (q3 * x - q1 * z)) + s * q2) - y,
                    2 * (q0 * (z * q0 - (q1 * y - q2 * x)) + s * q3) - z);
}

Vector3dExt RotationQuaternion::operator*(const Vector3dExt& v) const {
    return applyTo(v);
}
// ---------------------------------------------------------------------
/** Compose current rotation with rotation r.
 * The convention is such that:
 * u = r1 * v, v = r2 * w so u = r12 * w
 * with: r12 = r1.compose(r2)
 * The method return r12.
 * @param r rotation to compose with
 * @return composition of current rotation with rotation r
 */
RotationQuaternion RotationQuaternion::compose(const RotationQuaternion& r) const {
    return RotationQuaternion(r.q0 * q0 - (r.q1 * q1 + r.q2 * q2 + r.q3 * q3),
                    r.q1 * q0 + r.q0 * q1 + (r.q2 * q3 - r.q3 * q2),
                    r.q2 * q0 + r.q0 * q2 + (r.q3 * q1 - r.q1 * q3),
                    r.q3 * q0 + r.q0 * q3 + (r.q1 * q2 - r.q2 * q1));
}

RotationQuaternion RotationQuaternion::operator*(const RotationQuaternion& q) const {
    return compose(q);
}
// ---------------------------------------------------------------------
/** Compose the inverse of current rotation with rotation r.
 * The convention is such that:
 * u = r1.inverse() * v, v = r2 * w so u = r12 * w
 * with: r12 = r1.inverse().compose(r2)
 * The method return r12.
 * @param r rotation to compose with
 * @return composition of current inverse rotation with rotation r
*/
RotationQuaternion RotationQuaternion::inverseCompose(const RotationQuaternion& r) const {
    return RotationQuaternion(-r.q0 * q0 - (r.q1 * q1 + r.q2 * q2 + r.q3 * q3),
                    -r.q1 * q0 + r.q0 * q1 + (r.q2 * q3 - r.q3 * q2),
                    -r.q2 * q0 + r.q0 * q2 + (r.q3 * q1 - r.q1 * q3),
                    -r.q3 * q0 + r.q0 * q3 + (r.q1 * q2 - r.q2 * q1));
}
// ---------------------------------------------------------------------
/** Apply the inverse of the rotation to a vector.
 * @param u vector to apply the inverse of the rotation to
 * @return a new vector which such that u is its image by the rotation
 */
Vector3dExt RotationQuaternion::applyInverseTo(const Vector3dExt& u) const {
    double x = u.getX();
    double y = u.getY();
    double z = u.getZ();
    double s = q1 * x + q2 * y + q3 * z;
    double m0 = -q0;
    return Vector3dExt(2 * (m0 * (x * m0 - (q2 * z - q3 * y)) + s * q1) - x,
                    2 * (m0 * (y * m0 - (q3 * x - q1 * z)) + s * q2) - y,
                    2 * (m0 * (z * m0 - (q1 * y - q2 * x)) + s * q3) - z);
}
// ---------------------------------------------------------------------
/** Inverse the rotation
 */
RotationQuaternion RotationQuaternion::inverse() const {
    return RotationQuaternion(-q0, q1, q2, q3, true);
}
