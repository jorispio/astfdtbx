// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 */
#include "MatrixArray.hpp"

namespace MatrixArray {
    Matrix3 skewMatrix(const Vector3& u) {
        Matrix3 skewU;
        skewU <<    0, u(2),-u(1),
                -u(2),    0, u(0),
                 u(1),-u(0), 0;
        return skewU;
    }

    /** Pseudo-inverse (Moore-Penrose) of a matrix (rectangular) */
    MATRIXVAR pinv(const MATRIXVAR& A) {
        MATRIXVAR At = A.transpose();
        return ((At * A).inverse()) * At;
    }
}
