// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_POLYNOMIAL_HPP
#define __SMTBX_POLYNOMIAL_HPP
// --------------------------------------------------------------------------
#include <iostream>
#include <iomanip>
#include <complex>
#include <cmath>
#include <math.h>

#include "core/HelioDataTypes.hpp"

class Polynomial {
 private:
    int n;  // number of coefficient
    int order;  // polynomial order x^order
    double* coefficients;

    MATRIXVAR getCompanionMatrix();

 public:
    Polynomial();
    Polynomial(double coefficients[], int n);
    ~Polynomial() { }

    /** Evaluate the polynomial P(x) */
    double eval(double x);
    std::complex<double> eval(std::complex<double> x);

    /** Compute the roots of the polynomial */
    std::vector<std::complex<double> > getRoots();
};

// --------------------------------------------------------------------------
#endif  // __SMTBX_POLYNOMIAL_HPP