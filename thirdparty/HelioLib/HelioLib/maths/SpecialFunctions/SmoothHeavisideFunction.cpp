// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <cmath>
#include <cfloat>
//#include <math.h>

#include "SmoothHeavisideFunction.hpp"
#include "core/HelioException.hpp"

// ---------------------------------------------------------------------
/** Penalty Function.
 */
// ---------------------------------------------------------------------
SmoothHeavisideFunction::SmoothHeavisideFunction()
{
}
// ---------------------------------------------------------------------
/** Penalty Function.
 *   > 1,  s -> 0
 *   < 0,  s -> 1
 *   = 0,  s -> 0.5
 */
// ---------------------------------------------------------------------
double SmoothHeavisideFunction::Value(double eps, double value) {
    double rho = 0;
    /*
        if(smoothing == S_PENLOG) {    // 1     p�nalit� logarithmique
            rho = 1. / (1 + exp(value / epsilon));
        }
        else if(smoothing == S_BARLOG) {  // 2      barri�re logarithmique
            double denom = (sqrt(value*value + 4*epsilon*epsilon) - value+2*epsilon);
            rho = 1 - 4 * epsilon / denom;
        }
        else if(smoothing == S_DEFAULT) {
    */
    // default
    rho = 2. * eps / (sqrt(value * value + 4 * eps * eps) + value + 2 * eps);
    /*
            } else {
            rho = (value > 0)? 0 : 1;
        }
    */
    if(std::isnan(rho)) {
        char str[1024];
        sprintf(str, "PenaltyFunction returned NaN\n\tepsilon = %f, value=%f\n", eps, value);
        throw HelioLibException(str);
    }

    return rho;
}
// ---------------------------------------------------------------------
/** Penalty Function derivative.
 */
// ---------------------------------------------------------------------
double SmoothHeavisideFunction::Derivative(double eps, double value)
{
    double dS_drho;
    /*
        if(smoothing == S_PENLOG) {    // 1
            // penalite logarithmique
            //   rho = 1. / (1 + exp(value / epsilon))
            double e = exp(value / epsilon);
            dS_drho =-(e / epsilon) / (pow(1. + e, 2.));
        }
        else if(smoothing == S_BARLOG) {  // 2
            // barriere logarithmique
            // P(value) = 1 - 4 * epsilon / (sqrt(value*value + 4*epsilon*epsilon) - value+2*epsilon);
            double sqr = sqrt(value * value + 4. * epsilon * epsilon);
            double denom = (sqr - value + 2. * epsilon);
            dS_drho = 4 * epsilon * (value / sqr - 1.) / pow(denom, 2.);
        }
        else if(smoothing == S_DEFAULT) {
    */
    // P(value) = 2*epsilon / (sqrt(value*value + 4*epsilon*epsilon) + value + 2*epsilon);
    double sqr = sqrt(value * value + 4. * eps * eps);
    double denom = sqr + value + 2. * eps;
    dS_drho = -2. * eps * (1. + value / sqr) / (pow(denom, 2.));
    /*
            } else {
            dS_drho = 0;
        }
    */

    if(std::isnan(dS_drho)) {
        char str[1024];
        sprintf(
            str, "SmoothHeavisideFunction first-order derivative is NaN\n\tepsilon = %f, value=%f\n", eps, value);
        throw HelioLibException(str);
    }

    return dS_drho;
}
// ---------------------------------------------------------------------
/** Penalty Function second order derivative.
 */
// ---------------------------------------------------------------------
double SmoothHeavisideFunction::SecondOrderDerivative(double eps, double value)
{
    double d2S_drho2;
    /*
        if(smoothing == S_PENLOG) {
            // penalite logarithmique
            // -> S = 1./(1+exp((*SwitchFun)/epsilon))

            double e = exp(value / epsilon);
            double denom = e + 1;
            double c = e / pow(denom, 2.);
            double epsilon2 = epsilon * epsilon;

            d2S_drho2 = (2 * denom * pow(c, 2.) - c) / epsilon2;
        }
        else if(smoothing == S_BARLOG) {
            // barriere logarithmique
            // -> S = 1-4*epsilon/(sqrt((*SwitchFun)^2+4*epsilon^2)-(*SwitchFun)+2*epsilon)
            double epsilon2 = epsilon * epsilon;
            double rho2  = value * value;
            double c1 = sqrt(rho2 + 4*epsilon2);
            double denom = c1 - value + 2*epsilon;
            double denom2 = denom*denom;

            d2S_drho2 = 4*epsilon * ( (1./c1 - rho2/(c1*c1*c1)) - 2*pow(value/c1 - 1, 2.) / denom ) / denom2;
        }
        else if(smoothing == S_DEFAULT) { // default
    */
    // barriere logarithmique
    // -> S = 2*epsilon/(sqrt((*SwitchFun)^2+4*epsilon^2)+(*SwitchFun)+2*epsilon)
    double epsilon2 = eps * eps;

    double sqr = sqrt(value * value + 4 * epsilon2);
    double denom = value + sqr + 2 * eps;

    double c1 = value / sqr;
    d2S_drho2 = pow(c1 + 1., 2.) / denom - 2 * (epsilon2 / pow(sqr, 3.));
    d2S_drho2 *= 4 * eps / pow(denom, 2.);
    /*
        }
        else {
            d2S_drho2 = 0;
        }
    */
    if(std::isnan(d2S_drho2)) {
        char str[1024];
        sprintf(str, "PenaltyFunction second-order derivative is NaN\n\tepsilon = %f, value=%f\n", eps, value);
        throw HelioLibException(str);
    }

    return d2S_drho2;
}
/* ---------------------------------------------------------------------
 * Penalty Function second order derivative.
 * --------------------------------------------------------------------- */
/*int PenaltyFunction::CheckDerivatives(double epsilon, double value) {
    double derivative_test_tol = 1e-8;
    double derivative_test_perturbation = 1e-6;
    int nerrors = 0;

    // finite difference
    double step = derivative_test_perturbation * std::max(1., fabs(value));
    double val1 = Value(smoothing, epsilon, value - step);
    double val2 = Value(smoothing, epsilon, value + step);

    double drhoFD = (val2 - val1) / (2 * step);

    double drho = Derivative(smoothing, epsilon, value);

    double err;
    if ((err = FDError::getRelativeError(drhoFD, drho)) > derivative_test_tol) {
        printf("Check dP/dx\n");
        FDError::print(drho, drhoFD, err);
        ++nerrors;
    } else {
        printf("   All derivatives seem OK. (dP/dx)   (PenaltyFunction)\n");
    }

    // 2nd order central finite difference
    step = derivative_test_perturbation * std::max(1., fabs(value));
    //val1 = Value(smoothing, epsilon, value);
    //val2 = Value(smoothing, epsilon, value + step);
    //double val3 = Value(smoothing, epsilon, value - step);
    //double drhoFD2 = (val2 - 2 * val1 + val3) / (step * step);
    val1 = Derivative(smoothing, epsilon, value-step);
    val2 = Derivative(smoothing, epsilon, value+step);
    double drhoFD2 = (val2 - val1) / (2 * step);

    double drho2 = SecondOrderDerivative(smoothing, epsilon, value);
    if ((err = FDError::getRelativeError(drhoFD2, drho2)) > derivative_test_tol) {
        printf("Check d2P/dx2\n");
        FDError::print(drho2, drhoFD2, err);
        ++nerrors;
    } else {
        printf("   All derivatives seem OK. (d2P/dx2)   (PenaltyFunction)\n");
    }

    return nerrors;
}

if(smoothing == S_PENLOG) { // 1      p�nalit� logarithmique
    // p�nalit� logarithmique
    // -> S = 1./(1+exp((*SwitchFun)/epsilon))
    // with SwitchFun = -Fth*(nlV/(*m) + *lM/g0Isp)

    double exps = exp(SwitchFun/epsilon);
    double exp2s = exp(2*SwitchFun/epsilon);
    double denom = pow(exps+1,2);
    double epsilon2 = epsilon*epsilon;

    dS_drho =-exps / (epsilon * denom);
    d2S_drho2 = (2*exp2s/denom - exps)/(epsilon2*denom);
}
else if(smoothing == S_BARLOG) {  // 2      barri�re logarithmique
    // barri�re logarithmique
    // -> S = 1-4*epsilon/(sqrt((*SwitchFun)^2+4*epsilon^2)-(*SwitchFun)+2*epsilon)
    // with SwitchFun = -Fth*(nlV/(*m) + *lM/g0Isp)
    double epsilon2 = epsilon*epsilon;
    double rho2  = SwitchFun*SwitchFun;
    double c1 = sqrt(rho2+4*epsilon2);
    double denom = c1 - SwitchFun + 2*epsilon;
    double denom2 = denom*denom;

    dS_drho = 4*epsilon * (SwitchFun/c1 - 1) / denom2;
    d2S_drho2 = 4*epsilon * ( (1 - rho2/(c1*c1))/c1 - 2*pow(SwitchFun/c1 - 1,2)/denom )/denom2;
}
else {
    // barri�re logarithmique
    // -> S = 2*epsilon/(sqrt((*SwitchFun)^2+4*epsilon^2)+(*SwitchFun)+2*epsilon)
    // with rho: SwitchFun = -Fth*(nlV/(*m) + *lM/g0Isp)
    double epsilon2 = epsilon*epsilon;
    double rho2  = SwitchFun*SwitchFun;

    double denom = SwitchFun+sqrt(rho2+4*epsilon2)+2*epsilon;

    dS_drho = -(2*epsilon*(SwitchFun/sqrt(SwitchFun*SwitchFun+4*epsilon2)+1)) / pow(denom,2);

    double c2 = sqrt(rho2+4*epsilon2);
    double c1 = SwitchFun/c2;
    d2S_drho2 = ( 4*epsilon*pow(c1+1,2)/denom - 2*epsilon*(1/c2-rho2/pow(c2,3)) )/pow(denom,2);
}
*/
