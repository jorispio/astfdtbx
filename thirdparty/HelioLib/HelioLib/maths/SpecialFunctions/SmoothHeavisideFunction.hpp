// $Id$
// ---------------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SMOOTHED_HEAVISIDE_FUNCTION_HPP
#define SMOOTHED_HEAVISIDE_FUNCTION_HPP

// -------------------------------------------------------------------------------
/**
 *
 */
class SmoothHeavisideFunction
{
public:
    SmoothHeavisideFunction();

    /** */
    static double Value(double eps, double value);

    /** */
    static double Derivative(double eps, double value);

    /** */
    static double SecondOrderDerivative(double eps, double value);

    // static
    // int CheckDerivatives(double eps, double value);
};

// -------------------------------------------------------------------------------
#endif
