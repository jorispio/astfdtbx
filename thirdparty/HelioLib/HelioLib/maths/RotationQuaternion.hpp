/* $Id: RotationQuaternion.hpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_MATHS_QUATERNION_HPP_
#define __SMTBX_MATHS_QUATERNION_HPP_

#include "core/HelioException.hpp"
#include "core/HelioDataTypes.hpp"
#include <Eigen/Geometry>
#include "maths/Vector3dExt.hpp"


/** RotationQuaternion class.
 *
 */
class RotationQuaternion : public Eigen::Quaterniond {
 private:
    double q0, q1, q2, q3;

 public:
    static const RotationQuaternion IDENTITY;

    RotationQuaternion(const Vector3dExt& axis, double angle);
    RotationQuaternion(const Vector3dExt& u, const Vector3dExt& v);
    RotationQuaternion(double q0, double q1, double q2, double q3, bool normalized = false);

    /** Get the scalar coordinate of the quaternion.
     * @return scalar coordinate of the quaternion
     */
    double getQ0();

    /** Get the first coordinate of the vectorial part of the quaternion.
     * @return first coordinate of the vectorial part of the quaternion
     */
    double getQ1();

    /** Get the second coordinate of the vectorial part of the quaternion.
     * @return second coordinate of the vectorial part of the quaternion
     */
    double getQ2();

    /** Get the third coordinate of the vectorial part of the quaternion.
     * @return third coordinate of the vectorial part of the quaternion
     */
    double getQ3();

    /** Get the normalized axis of the rotation.
     */
    Vector3dExt getAxis();

    /** Get the angle of the rotation(between 0 and pi)
      */
    double getAngle();

    /** Get the 3X3 matrix corresponding to the instance
     * @return the matrix corresponding to the instance
     */
    Matrix3d getMatrix();

    /** Opposite rotation */
    RotationQuaternion inverse() const;

    /** Apply the rotation to a vector.  */
    Vector3dExt operator*(const Vector3dExt& u) const;
    Vector3dExt applyTo(const Vector3dExt& u) const;
    Vector3dExt applyInverseTo(const Vector3dExt& u) const;

    /** Compose rotation */
    RotationQuaternion operator*(const RotationQuaternion& r) const;
    RotationQuaternion compose(const RotationQuaternion& r) const;
    RotationQuaternion inverseCompose(const RotationQuaternion& r) const;
};

// --------------------------------------------------------------------------
#endif  // __SMTBX_MATHS_QUATERNION_HPP_
