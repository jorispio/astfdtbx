/* $Id: Vector3dExt.hpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_VECTOR3DEXT_HPP_
#define __SMTBX_VECTOR3DEXT_HPP_

#include "core/HelioConstants.hpp"
#include "core/HelioDataTypes.hpp"

/** This class extends 3d vector with specific math.
 *
 */
class Vector3dExt : public Eigen::Vector3d {
 public:
    /** Third canonical vector (coordinates: 0, 0, 1). */
    static const Vector3dExt PLUS_K;
    /** (0,0,0) */
    static const Vector3dExt ZERO;

    // This constructor allows you to construct MyVectorType from Eigen expressions
    template <typename DerivedMatrix>
    Vector3dExt(const Eigen::MatrixBase<DerivedMatrix>& other)
        : Eigen::Vector3d(other) { }

    Vector3dExt();
    explicit Vector3dExt(Vector3d& vec);
    Vector3dExt(double x, double y, double z);
    Vector3dExt(double module, const Vector3dExt& vec);
    ~Vector3dExt(){ }

    // This method allows you to assign Eigen expressions to MyVectorType
    template <typename OtherDerived> Vector3dExt& operator=(const Eigen::MatrixBase<OtherDerived>& other) {
        this->Base::operator=(other);
        return *this;
    }

    /** Get the abscissa of the vector. */
    inline double getX() const {
        return this->operator()(0);
    }

    /** Get the ordinate of the vector. */
    inline double getY() const {
        return this->operator()(1);
    }

    /** Get the height of the vector. */
    inline double getZ() const {
        return this->operator()(2);
    }

    /** Get the azimuth of the vector, between -pi; and +pi */
    inline double getAlpha() const {
        return atan2(getY(), getX());
    }

    /** Get the elevation of the vector, between -pi;/2 and +pi;/2*/
    inline double getDelta() const {
        return asin(getZ() / norm());
    }

    /** Compute the angular separation between two vectors. */
    double angle(const Vector3dExt& v);

    /** Compute derivative of the angular separation between two vectors. */
    static Vector3 dangledr(const Vector3dExt &u, const Vector3dExt &v, const Matrix3 &dudr, const Matrix3 &dvdr);

    /** Compute the distance between two positions vector */
    double distance(const Vector3dExt& position) const;

    /** Get a vector orthogonal to the current one. */
    Vector3dExt orthogonal() const;
};

#endif  // __SMTBX_VECTOR3DEXT_HPP_
