// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_MATHS_ROUTINES_HPP_
#define __SMTBX_MATHS_ROUTINES_HPP_

#include <limits>  // epsilon
#include <cmath>

#include "core/HelioConstants.hpp"
#include "core/HelioDataTypes.hpp"

#define TWO_PI (2 * M_PI)
#define epsilon std::numeric_limits<double>::epsilon() // double: 2e-16

//---------------------------------------------------------------------------
namespace HelioLib {
namespace MathUtils {
    /** compute the angle of a 2d vector (x,y) */
    template <typename T>
    inline T angle(T x, T y) {
        T a = atan2(y, x);  // between [-pi, pi]
        if (a < 0) {
            a = 2 * M_PI + a;
        }
        return a;
    }

    /** sign of val */
    template <typename T>
    inline int sign(T val) {
        return (T(0) < val) - (val < T(0));
    }

    /** return the value of x with the sign of y */
    template <typename T>
    inline T dsign(T x, T y) {
        return ((y < 0) ? -1 : 1) * x;
    }

    inline double modulo(double a, double p) {
        return a - floor(a / p) * p;
    }

    inline double modulo(double x, double a, double b) {
        return x - floor((x-a) / (b-a)) * (b-a);
    }

    /** normalise an angle in radian on the a 2-pi range around 'center' */
    inline double normalizeAngle(double val, double center) {
        // double rev = val/(2. * M_PI);
        // return ((rev - (int)(rev)) * 2. * M_PI);
        return val - TWO_PI * floor((val + M_PI - center) / TWO_PI);
    }
}  // namespace MATHS
}  // namespace HelioLib
//---------------------------------------------------------------------------
#endif  // __SMTBX_MATHS_ROUTINES_HPP_
