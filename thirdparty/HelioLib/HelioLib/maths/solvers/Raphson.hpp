// $Id: Raphson.hpp 132 2013-12-29 20:57:47Z joris $
/*
        Newton-Raphson Solver.

        Project homepage: http://www.holoborodko.com/pavel/?page_id=679
        Contact e-mail:   pavel@holoborodko.com

        Copyright (c) 2012 Joris Olympio
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions
        are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. This software cannot be, by any means, used for any commercial
        purpose without the prior permission of the copyright holder.

        Any of the above conditions can be waived if you get permission from
        the copyright holder.

        THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
        IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
        ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
        FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
        DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
        OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
        HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
        LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
        OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
        SUCH DAMAGE.
*/

#ifndef __SMTBX_RAPHSON_HPP
#define __SMTBX_RAPHSON_HPP

#include <vector>
#include <limits>
#include <algorithm>
#include <functional>
#include <iostream>
#include <cstdio>

#include "core/HelioDataTypes.hpp"

class RAPHSON {
 private:
    static const char* msg[6];

 protected:
    void differentiation(const VECTORVAR& x0, MATRIXVAR& df);

 public:
    RAPHSON(){};
    virtual ~RAPHSON(){};

    template <class DATA, class FUN> static DATA test(FUN fun, DATA x) {
        return (DATA)fun(x);
    };

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    virtual VECTORVAR valuefun(const VECTORVAR& , bool& iret)
    {
        printf("Raphson::valuefun() not implemented!\n");
        iret = true;
        return VECTORVAR::Zero(1);
    };

    virtual int jacobian(const VECTORVAR& , MATRIXVAR& )
    {
        printf("Raphson::jacobian() not implemented!\n");
        return 0;
    };

    int solve(const VECTORVAR& x0, VECTORVAR& sol, bool verbose = false, int maxiter = 100, double tol = 1e-6, int maxfunevals = 5000, double alphaInit=0.1);
};

#endif  // __SMTBX_RAPHSON_HPP
