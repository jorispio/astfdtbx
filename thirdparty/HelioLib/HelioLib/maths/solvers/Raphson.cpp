// $Id: Raphson.cpp 132 2013-12-29 20:57:47Z joris $
/*
        Newton-Raphson Solver.

        Copyright (c) 2012 Joris Olympio
        All rights reserved.

        Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions
        are met:

        1. Redistributions of source code must retain the above copyright
        notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
        notice, this list of conditions and the following disclaimer in the
        documentation and/or other materials provided with the distribution.

        3. This software cannot be, by any means, used for any commercial
        purpose without the prior permission of the copyright holder.

        Any of the above conditions can be waived if you get permission from
        the copyright holder.

        THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
        ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
        IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
        ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
        FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
        DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
        OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
        HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
        LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
        OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
        SUCH DAMAGE.
*/

#include "Raphson.hpp"

#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <float.h>

#include <cstdio>
#include <string>

#include "core/HelioException.hpp"


#define formatstrFirstIter " %5d   %13.6g                  %12.3g\n"
#define formatstr " %5d   %13.6g  %13.6g   %12.3g      %7.0f\n"
#define header                                                                                                         \
    "\n                                          Norm of      First-order \n Iteration     f(x)          step        " \
    "  optimality   CG-iterations\n"


#define FINITE_STEP  1e-7

const char* RAPHSON::msg[] = {
    "Optimization terminated: first-order optimality less than 'TolFun',\n and no negative/zero curvature "
    "detected in trust region model.\n",
    "Optimization terminated: relative function value\n changing by less than TolFun.\n",
    "Optimization terminated: norm of the current step is less\n than 'TolX'.\n",
    "Maximum number of iterations exceeded;\n increase 'MaxIter'\n",
    "Maximum number of function evaluations exceeded;\n increase 'MaxIter'\n",
    "An error occurred during the step.\n"
};

/* --------------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------- */
int RAPHSON::solve(const VECTORVAR& x0, VECTORVAR& sol, bool verbose, int maxiter, double abstol, int maxfunevals,
                    double alphaInit) {
    double tol2 = 1e-6;

    int funevals = 0;
    int ex = 0;
    bool iret = false;

    sol = x0;
    VECTORVAR x = x0;
    VECTORVAR fval = valuefun(x, iret);
    if (iret) {
        ex = 6;
        if (verbose)
            printf("%s", msg[ex - 1]);
        return 9;
    }
    double val = fval.norm();
    double val_old = val;
    VECTORVAR ofval = fval;

    int n = x0.rows();
    int m = fval.rows();
    if ((n == 0) || (m == 0)) {
        throw HelioLibException("Bad problem dimensions. One of the dimensions is null!");
    }

    MATRIXVAR fjac = MATRIXVAR::Zero(m, n);
    jacobian(x, fjac);

    assert((fjac.rows() == m) && (fjac.cols() == n));

    int EXITFLAG = 0;
    double ratio = 0;
    int iter = 0;
    double alpha = alphaInit;

    // print header and first iteration
    if (verbose) {
        printf("%s", header);
        printf(formatstrFirstIter, iter, val, 0.);
    }

    if (iter >= maxiter) {
        ex = 4;
        EXITFLAG = 0;
        return EXITFLAG;
    }

    while (true) {
        iret = false;
        VECTORVAR xold = x;

        // Newton-Raphson
        jacobian(x, fjac);
        funevals += 2;

        VECTORVAR dx;
        if (n == m) {
            dx = fjac.inverse() * fval;
        } else {
            // pseudo-inverse (More-Penrose)
            // see https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
            // dx = (fjac.transpose() * fjac).inverse() * fjac.transpose() * fval;
            dx = fjac.block(0, 0, m, n).colPivHouseholderQr().solve(fval);
        }

        double optnrm = fjac.norm();
        double nrmsx = dx.norm();

        // minor iteration
        alpha = 1.;
        int max_minox_iterations = 10;
        int minor_iteration = 0;
        bool minor_loop_done = false;
        while (!minor_loop_done && (minor_iteration < max_minox_iterations)) {
            VECTORVAR xnew = x - alpha * dx;

            // actual reduction
            double reduction = -1;
            if (val < val_old) {
                reduction = 1 - pow(val / val_old, 2.);
            }

            // predicted reduction
            double temp = val + (fjac * dx).norm();
            double pred_reduction = 0;
            pred_reduction = 1 - pow(temp / val, 2);

            double ratio = 0;
            if (fabs(pred_reduction) > 0) {
                ratio = reduction / pred_reduction;
            }

            //if (ratio > 0.1) {
                // successful step
                // current error
                VECTORVAR newfval = valuefun(xnew, iret);
                if (iret) {
                    ex = 6;
                    EXITFLAG = 9;
                    break;
                }
                double newval = fval.norm();

            if (newfval.norm() < fval.norm()) {
                val = newval;
                fval = newfval;
                x = xnew;
                minor_loop_done = true;
            }
            else {
                // TODO(joris) perform line search (alpha) (backtracking)
                alpha /= 2.;
            }
            ++minor_iteration;
        }  // end minor iteration loop

        // increment iteration counter
        ++iter;

        if (verbose) {
            printf(formatstr, iter, val, nrmsx, optnrm, alpha);
        }

        // Test for convergence
        double diff = fabs(val_old - val);
        val_old = val;
        if (val < abstol) {
            ex = 1;
            EXITFLAG = 1;
        } /*else if ((nrmsx < .9) && (ratio > .25) && (diff < abstol * (1 + abs(val_old)))) {
            ex = 2;
            EXITFLAG = 3;
        }*/ else if ((iter > 1) && (nrmsx < tol2)) {
            ex = 3;
            EXITFLAG = 2;
        } else if (iter > maxiter) {
            ex = 4;
            EXITFLAG = 0;
        } else if (funevals > maxfunevals) {
            ex = 5;
            EXITFLAG = 0;
        }

        if (ex > 0)
            break;
    }

    sol = x;

    // final message
    if (verbose)
        printf("%s", msg[ex - 1]);

    return EXITFLAG;
}
