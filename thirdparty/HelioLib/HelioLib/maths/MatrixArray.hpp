// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 */
#ifndef __SMTBX_MATHS_MATRIX_ARRAY_HPP_
#define __SMTBX_MATHS_MATRIX_ARRAY_HPP_

#include "core/HelioDataTypes.hpp"

#define MATRIX(mat, lin, col, sz) (mat)[(lin) + (col)*sz[0]]

namespace HelioLib {
namespace MatrixArray {
    Matrix3 skewMatrix(const Vector3& u);

    /** Pseudo-inverse (Moore-Penrose) of a matrix (rectangular) */
    MATRIXVAR pinv(const MATRIXVAR& A);

//---------------------------------------------------------------------------
/** Matrix-vector multiplication */
template<typename ScalarType>
void MatrixVectorMux(const ScalarType Mi[9], const ScalarType Vi[3], ScalarType Vo[3]) {
    real_type tmp[3];
    int s[] = { 3, 3 };
    tmp[0] = MATRIX(Mi, 0, 0, s) * Vi[0] + MATRIX(Mi, 0, 1, s) * Vi[1] + MATRIX(Mi, 0, 2, s) * Vi[2];
    tmp[1] = MATRIX(Mi, 1, 0, s) * Vi[0] + MATRIX(Mi, 1, 1, s) * Vi[1] + MATRIX(Mi, 1, 2, s) * Vi[2];
    tmp[2] = MATRIX(Mi, 2, 0, s) * Vi[0] + MATRIX(Mi, 2, 1, s) * Vi[1] + MATRIX(Mi, 2, 2, s) * Vi[2];
    Vo[0] = tmp[0];
    Vo[1] = tmp[1];
    Vo[2] = tmp[2];
}
}
}  // namespace HelioLib
//---------------------------------------------------------------------------
#endif  // __SMTBX_MATHS_MATRIX_ARRAY_HPP_
