// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 */
#ifndef __SMTBX_MATHS_VECTOR_ARRAY_HPP_
#define __SMTBX_MATHS_VECTOR_ARRAY_HPP_

#include "core/HelioDataTypes.hpp"

namespace HelioLib {
namespace Vector3dArray {
    /** construct a vector array */
    template <typename Scalar>
    void createVectorArray(const Scalar alpha, const Scalar beta, const Scalar n, Scalar v[3]) {
        v[0] = n * cos(alpha) * cos(beta);
        v[1] = n * cos(alpha) * sin(beta);
        v[2] = n * sin(alpha);
    }

    /** cross product between 3d vector arrays */
    template <typename Scalar>
    void cross(const Scalar a[3], const Scalar b[3], Scalar c[3]) {
        c[0] = a[1] * b[2] - a[2] * b[1];
        c[1] = a[2] * b[0] - a[0] * b[2];
        c[2] = a[0] * b[1] - a[1] * b[0];
    }

    /** dot product between two 3d vector arrays */
    template <typename Scalar>
    inline Scalar dot(const Scalar a[3], const Scalar b[3]) {
        return (a[0] * b[0] + a[1] * b[1] + a[2] * b[2]);
    }

    /** norm of a 3d vector arrays */
    template <typename Scalar>
    inline Scalar norm(const Scalar a[3]) {
        return sqrt(a[0] * a[0] + a[1] * a[1] + a[2] * a[2]);
    }

    /** UNIT VECTOR */
    template <typename Scalar>
    void uvect(const Scalar a[3], Scalar b[3]) {
        Scalar norm = a[0] * a[0] + a[1] * a[1] + a[2] * a[2];
        norm = sqrt(norm);
        b[0] = a[0] / norm;
        b[1] = a[1] / norm;
        b[2] = a[2] / norm;
    }

    template <typename Scalar>
    void uvectdiff(const Scalar a[3], const Scalar b[3], Scalar c[3]) {
        Scalar d[3], dn;
        SubVectors(a, b, d);
        dn = norm(d);
        if (dn < 1e-6) {
            uvect(a, c);
        } else {
            uvect(d, c);
        }
    }

    template <typename Scalar>
    void svect(const Scalar a[3], Scalar s, Scalar b[3]) {
        // mattime(a, s, 3, b);
    }

    template <typename Scalar>
    void AddVectors(const Scalar A[3], const Scalar B[3], Scalar C[3]) {
        C[0] = A[0] + B[0];
        C[1] = A[1] + B[1];
        C[2] = A[2] + B[2];
    }

    template <typename Scalar>
    void SubVectors(const Scalar A[3], const Scalar B[3], Scalar C[3]) {
        C[0] = A[0] - B[0];
        C[1] = A[1] - B[1];
        C[2] = A[2] - B[2];
    }
}
}  // namespace HelioLib
//---------------------------------------------------------------------------
#endif  // __SMTBX_MATHS_VECTOR_ARRAY_HPP_
