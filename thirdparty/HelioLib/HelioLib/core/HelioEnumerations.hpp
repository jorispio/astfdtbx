// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SMTBX_HELIOLIB_ENUMS_HPP_
#define __SMTBX_HELIOLIB_ENUMS_HPP_

/** \enum ODE_SOLVER
 *  \brief ODE solver enum type
 */
enum ODE_SOLVER_ID { ODE_DOP853 = 50, ODE_DOPRI5 = 51, ODE_RK4 = 52, ODE_UNKNOWN = 0};

/// Switches for calling AtStep():
enum OdeStepperMode {
    call_never,           ///< no call
    call_for_output_only, ///< solout only used for output
    call_for_dense_output ///< dense output is performed in solout (in this case nrdens must be greater than 0)
};

// Return codes of the integration routines
enum OdeSuccess {
    ok = 1,  // Integration complete (end value reached)
    interrupted = 2,  // Integration terminated by virtual routine AtStep()'s return value
    inputerror = -1,  // Some input parameters are wrong
    nmax_too_small = -2,
    step_size_becomes_too_small = -3,  // The step size becomes to small
    problem_is_probably_stiff = -4,  // The problem is probably stiff and can't be solved with DOP853
    invalid_data = -9
};

/*
 */
enum DYNAMICALMODEL_TYPE { D_KEPLERIAN = 1, D_CARTESIAN = 2, D_EQUINOCTIAL = 4, D_EQUINOCTIAL_AVERAGING = 8, D_ERTBP = 16, D_CHLOHESSY = 32, D_ALL = 1 | 2 | 4 | 8 | 16 | 32, D_NOT_SET = 0 };

/*
 */
enum GRAVITYMODEL_TYPE { G_CENTRAL = 70, G_J2, G_EGM96 };

/*
 */
enum PROPULSION_TYPE {
    PROP_NEP = 31, // Nuclear Electric Propulsion
    PROP_SEP,      // Solar Electric Propulsion
    PROP_CUSTOM,   // SEP with specific thruster coefficients
    PROP_VASIMR,   // VAriable Specific Impulse Magnetoplasma Rocket
    PROP_SOLARSAIL // Solar Sailing
};


/* --------------------------------------------------------------------- */
#endif  // __SMTBX_HELIOLIB_ENUMS_HPP
