﻿// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef SMCONSTANTS_HPP
#define SMCONSTANTS_HPP

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327
#endif

#define RAD2DEG (180 / M_PI)
#define DEG2RAD (M_PI / 180.)

#define DAY2SEC 86400.
#define SEC2DAY (1. / 86400.)
#define DAY2YEAR 365.25

// conversion from arcsec to radians
#define ARCSEC2RAD (M_PI / 3600.)

// Arcseconds to radians (source SOFA/IERS)
#define DAS2R 4.848136811095359935899141D - 6

// conversion from arcsec to radians
#define ARCSEC2RAD (M_PI / 3600.)
#define pi_sur_2 (M_PI / 2.)

// ---------------------------------------------------------------------------
#define MJD2000                                       \
    51544.5 /* Modified Julian Date of Epoch J2000.0, \
                               which is defined as January 1, 2000, 12h UT. */

// MJD = JD - 2400000.5
// Modified JD     0h Nov 17, 1858     JD − 2400000.5
#define MJD_JD_OFFSET 2400000.5

#define MJD2000_MJD_OFFSET 51544.5

#define JD_JDCNES 2433282.5

#define MJD50_MJD2000 18262.0

// ---------------------------------------------------------------------------
// Physical Constants
#define TSID 86164.1
#define lightSpeed 299792458.

/** Astronomical Unit: 149597870691 m. */
#define JPL_SSD_AU 149597870691.0
#define AU 149597870660.0

//#define G   6.672e-11
#define SpeedOfLight 299792458 // m/s
#define Wirradiance 1361       // W/m²

// ---------------------------------------------------------------------------
/** Reference distance for the solar radiation pressure (m). */
#define SRP_D_REF 149597870000.0

/** Reference solar radiation pressure at D_REF (N/mÂ²). */
#define SRP_P_REF 4.56e-6

// ---------------------------------------------------------------------------
// Earth
#define EARTH_MU 3.9860064e+14
#define EARTH_RADIUS 6378137
#define EarthOblateness 0.003352836
#define apla 0.003352836

#define WGS84_EARTH_ANGULAR_VELOCITY 7.292115e-5     /** Earth angular velocity from WGS84 model: 7.292115e-5 rad/s. */
#define WGS84_EARTH_EQUATORIAL_RADIUS 6378137.0;     /** Earth equatorial radius from WGS84 model: 6378137.0 m. */
#define WGS84_EARTH_FLATTENING (1.0 / 298.257223563) /** Earth flattening from WGS84 model: 1.0 / 298.257223563. */
#define WGS84_MAJOR 6378137.0                        /* meters */
#define WGS84_MINOR 6356752.3142                     /* meters */
#define WGS84_ECC 0.081819190928906199466            /* eccentricity */
#define WGS84_ECC_SQR 0.006694380004260806515        /* eccentricity squared */

/** Earth equatorial radius from EGM96 */
#define EGM96_EARTH_EQUATORIAL_RADIUS 6378136.3
#define EGM96_EARTH_MU 3.986004415e14

#define EARTH_J2 0.00108263
#define EARTH_J3 -2.54e-06
#define EARTH_J4 -1.62e-06
#define EARTH_J5 -2.3e-07
#define EARTH_J6 0.0000005

#define g0 9.80665

#define criticalInc1 1.1071487
#define criticalInc2 2.0344439

// ---------------------------------------------------------------------------
// Sun
/** Sun radius: 695500000 m. */
#define SUN_MU (1.32712440018 * 1e20)
#define SUN_RADIUS 6.955e8
#define rotrEarthSun 1.990993e-07 // 2 * pi / (365*24*3600.)

// ---------------------------------------------------------------------------
// Moon
#define MOON_MU 4.9027989e+12
#define MOON_RADIUS 1737500 // http://ssd.jpl.nasa.gov/?sat_phys_par

// ---------------------------------------------------------------------------
// Mars
#define MARS_RADIUS (3389 * 1e3)

// ---------------------------------------------------------------------------
// Venus
#define VENUS_RADIUS (6052 * 1e3)

// ---------------------------------------------------------------------------
// Planets
//
#define MU_SUN (1.32712440018 * 1e20)

#define MU_MOON (4902.801 * 1e9)
#define MU_MERCURY (22032.09 * 1e9)
#define MU_VENUS (324858.63 * 1e9)
#define MU_EARTH (3.98600432896939 * 1e014)
#define MU_MARS (42828.3 * 1e9)
#define MU_JUPITER (126686534.92180 * 1e9)
#define MU_SATURN (37931284.5 * 1e9)
#define MU_URANUS (5793947 * 1e9)
#define MU_NEPTUN (6835107 * 1e9)
#define MU_PLUTO (870 * 1e9)

#define DISTANCE_EARTH_MOON 385692 * 1e3  // average
#define DISTANCE_SUN_EARTH AU  // average
#define DISTANCE_SUN_JUPITER (5.2 * AU)  // average

// ---------------------------------------------------------------------------
#define MJD2000 51544.5 // Modified Julian Date of Epoch J2000.0, which is defined as January 1, 2000, 12h UT.

// MJD = JD - 2400000.5
// Modified JD 	0h Nov 17, 1858 	JD − 2400000.5
#define MJD_JD_OFFSET 2400000.5

#define MJD2000_MJD_OFFSET 51544.5

#define JD_JDCNES 2433282.5

#define MJD50_MJD2000 18262.0

// ---------------------------------------------------------------------------
// Physical Constants
#define TSID 86164.1
#define lightSpeed 299792458.

/** Astronomical Unit: 149597870691 m. */
#define JPL_SSD_AU 149597870691.0
#define AU 149597870660.0

//#define G   6.672e-11
#define SpeedOfLight 299792458 // m/s
#define Wirradiance 1361       // W/m²

// ---------------------------------------------------------------------------
/** Reference distance for the solar radiation pressure (m). */
#define SRP_D_REF 149597870000.0

/** Reference solar radiation pressure at D_REF (N/mÂ²). */
#define SRP_P_REF 4.56e-6

// ---------------------------------------------------------------------------
#endif
