// $Id: HelioibException.hpp 132 2013-12-29 20:57:47Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SMTBX_SPACEMECALIB_EXCEPTION_HPP_
#define __SMTBX_SPACEMECALIB_EXCEPTION_HPP_

#include <iostream>
#include <exception>
#include <cstdio>
#include <cstring>

#ifdef _MSC_VER
#if defined(__STDC_LIB_EXT1__)
    #define __STDC_WANT_LIB_EXT1__ 1
#endif
#else
    #ifndef STRNCPY_S
    #define STRNCPY_S
    #define strncpy_s(dest, destsz, src, count) strncpy(dest, src, count)
    #endif
#endif


class HelioLibException : public std::exception {
 private:
    char function[256];
    char msg[256];
    char cause[256];
 public:
    explicit HelioLibException(const char* msg_) {
        strncpy_s(msg, 256, msg_, 256);
        cause[0] = 0;
        function[0] = 0;
    }

    HelioLibException(const char* msg_, const char* cause_) {
        strncpy_s(msg, 256, msg_, 256);
        strncpy_s(cause, 256, cause_, 256);
        function[0] = 0;
    }

    HelioLibException(const char* function_, const char* msg_, const char* cause_) {
        strncpy_s(function, 256, function_, 256);
        strncpy_s(msg, 256, msg_, 256);
        strncpy_s(cause, 256, cause_, 256);
    }

    virtual const char* what() const throw() {
        fprintf(stderr, "cause: %s\n", cause);
        return msg;
    }
};

#endif  // __SMTBX_SPACEMECALIB_EXCEPTION_HPP_
