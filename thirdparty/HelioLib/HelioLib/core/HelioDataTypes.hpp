/* $Id: HelioDataTypes.hpp 132 2013-12-29 20:57:47Z joris $ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_HELIOLIB_DATATYPES_HPP_
#define __SMTBX_HELIOLIB_DATATYPES_HPP_

#include <math.h>
#include <stdlib.h>
//#include <unistd.h>

#include <cfloat>
#include <vector>

#define NOMINMAX
#undef min
#undef max

#include <Eigen/Core>
#include <Eigen/Dense>    // QR
#include <Eigen/Geometry> // cross
#include <Eigen/LU>  // for matrix inverse only.

#ifdef EIGEN2
#include <Eigen/Array>  // for cwise() and comparisons, remove for Eigen-3
#else
#ifndef EIGEN3
#define EIGEN3
#ifdef _MSC_VER  // Microsoft Visual C++
#else
    // #warning Defining Eigen Version 3
#endif
#endif

#include <Eigen/Eigenvalues>
#endif

// --------------------------------------------------------------------- 
// USING_PART_OF_NAMESPACE_EIGEN
using namespace Eigen;

/*! \namespace variable
    \brief Declares main variables type.
*/

// Because we have some optimization involving SIMD,
// and alignement issues, using a:
//  * "long double" will slightly improve accuracy at the cost of speed
//  * "double" will improve speed (*2) as we benefit from 16bytes alignement
//     and SSE functions
#ifdef USE_LONGDOUBLE
typedef long double real_type;
//#warning "Using long double"
#else
typedef double real_type;
#endif

typedef unsigned int size_n;
typedef unsigned int uint;
typedef short smallint;
typedef unsigned long ulong;

// --------------------------------------------------------------------- 

//#define MACHINE_EPSILON    DBL_EPSILON
#define MACHINE_EPSILON LDBL_EPSILON
// std::numeric_limits<real_type>::epsilon()

typedef Matrix<real_type, Eigen::Dynamic, 1> VECTORVAR;  // vector of variable size
typedef Matrix<real_type, Eigen::Dynamic, Eigen::Dynamic> MATRIXVAR;  // matrix of variable size

typedef Matrix<double, 2, 1> Vector2;
typedef Matrix<double, 3, 1> Vector3;
typedef Matrix<double, 6, 1> Vector6;
typedef Matrix<double, 7, 1> Vector7;
typedef Matrix<double, 8, 1> Vector8;
typedef Matrix<double, 14, 1> Vector14;

typedef Matrix<double, 2, 2> Matrix2;
typedef Matrix<double, 3, 3> Matrix3;
typedef Matrix<double, 6, 6> Matrix6;
typedef Matrix<double, 9, 9> Matrix9;
typedef Matrix<double, 6, 3> Matrix6x3;
typedef Matrix<double, 7, 7> Matrix7;
typedef Matrix<double, 7, 3> Matrix7x3;
typedef Matrix<double, 3, 7> Matrix3x7;
typedef Matrix<real_type, 2 * 7, 2 * 7> Matrix14;
typedef Matrix<real_type, 2 * 7, 2 * 7> S14MATRIX;
typedef Matrix<real_type, 7, 2 * 7> S7_14MATRIX;

// --------------------------------------------------------------------- 
#define NaN (*reinterpret_cast<double*>(nan))
#define Inf (std::numeric_limits<double>::infinity())

// --------------------------------------------------------------------- 
#include "maths/integration/OdePolyData.hpp"

typedef OdePolyData<real_type, 5> OdePolyData5;
typedef OdePolyData<real_type, 8> OdePolyData8;
typedef OdePolyData<real_type, 5> OdePolyDataVar;

// ---------------------------------------------------------------------
#endif  // __SMTBX_HELIOLIB_DATATYPES_HPP_
