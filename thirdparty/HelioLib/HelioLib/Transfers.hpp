// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_TRANSFERS_HPP_
#define __SMTBX_TRANSFERS_HPP_

#ifdef WITH_TRANSFER
    #include "transfer/Transfer.hpp"
    #include "transfer/Hohmann.hpp"
    #include "transfer/LambertSolver.hpp"
    #include "transfer/LambertSolverNumerical.hpp"
    #include "transfer/TransferOneDsm.hpp"
    #include "transfer/LowThrust/ExpSin.hpp"
    #include "transfer/LowThrust/QLaw.hpp"
    #include "transfer/Toltoiski.hpp"
    #include "transfer/Impulse.hpp"
#endif


#endif  // __SMTBX_TRANSFERS_HPP_