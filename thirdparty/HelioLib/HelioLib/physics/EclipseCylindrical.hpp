// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECLIPSE_CYLINDRICAL_HPP
#define ECLIPSE_CYLINDRICAL_HPP

#include "core/HelioDataTypes.hpp"
//#include "orbits/Orbit.hpp"
#include "core/HelioConstants.hpp"

#include "Eclipse.hpp"

/** @class Eclipse
 * @brief Eclipse computation class.
 */
class EclipseCylindrical
{
private:
    GenericEphemeridesBase *ephemeris;
    int planet_id;
    double planet_radius;

    /** */
    static double getEclipseSwitchingFunction(const Vector3& rsun,
                                              const Vector3& rplanet,
                                              double planet_radius,
                                              const Vector3& rposition);

public:
    /** Creator. */
    EclipseCylindrical(GenericEphemeridesBase *ephemeris, int planet_id, double planet_radius);

    /** Destructor. */
    ~EclipseCylindrical(){};

    /** Indicate whether the specified point is in eclipse. */
    EclipseCondition get_condition(const GenericDate& date, const Vector3& rposition);

    /** Indicate whether the specified point is in eclipse. */
    static EclipseCondition
    getEclipseCondition(const Vector3& rsun, const Vector3& rplanet, const Vector3& rposition, double planet_radius);

    /** Indicate whether the specified point is in eclipse. */
    double getSmoothedEclipseCondition(const Vector3& rsun,
                                                         const Vector3& rplanet,
                                                         double eps, 
                                                         const Vector3& rposition);
                                                         
    /** */
    static double
    getEclipseRatio(const Vector3& rsun, const Vector3& rplanet, double planet_radius, const Vector3& rposition);

    /** */
    static Vector3 getEclipseRatioDerivative(const Vector3& rsun,
                                             const Vector3& rplanet,
                                             double planet_radius,
                                             const Vector3& rposition);

    /** */
    static Matrix3 getEclipseRatioSecondDerivative(const Vector3& rsun,
                                                   const Vector3& rplanet,
                                                   double planet_radius,
                                                   const Vector3& rposition);

    /** */
    static std::string getString(EclipseCondition val);
};

//---------------------------------------------------------------------------
#endif
