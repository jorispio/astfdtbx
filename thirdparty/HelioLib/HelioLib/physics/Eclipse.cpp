// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Eclipse.hpp"

#include "core/HelioException.hpp"
#include "Maths.hpp"
#include "maths/MathUtils.hpp"

using namespace HelioLib;

//#define DEBUG_SMTBX_ECLIPSE
// ---------------------------------------------------------------------
/** Creator.
 */
// ---------------------------------------------------------------------
Eclipse::Eclipse(double rsun)
{
    sun_radius = rsun;
}
// ---------------------------------------------------------------------
/** Get the eclipse switching function.
 * This function becomes negative when entering the region of shadow and positive when exiting.
 *
 * @param rsun sun position vector in a given frame F.
 * @param rplanet shadowing planet's position vector in frame F.
 * @param planet_radius shadowing planet radius.
 * @param robject object position in frame F.
 * @param bumbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value, between [-1, 1]
 */
// ---------------------------------------------------------------------
double Eclipse::getEclipseSwitchingFunction(const Vector3& rsun,
                                            double sun_radius,
                                            const Vector3& rplanet,
                                            double planet_radius,
                                            const Vector3& robject,
                                            bool bumbra)
{
    // Satellite->Sun vector
    Vector3dExt ps = rsun - robject;
    double normPs = ps.norm();
    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;
    double normPo = po.norm();

    // compute the angle (Satellite-Sun center, Satellite-Earth center), in [0, pi]
    double angle = 0;
    if((po.norm() > 0) && (ps.norm() > 0)) {
        angle = ps.angle(po);
    }

    // compute the apparent Sun radius (angle)
    double sinSunApparentAngle = 1.;
    if(normPs > 0) {
        sinSunApparentAngle = sun_radius / normPs;
        if(fabs(sinSunApparentAngle) > 1.) {
            // we are inside the Sun! Never in eclipse!
            sinSunApparentAngle = 1. * MathUtils::sign<real_type>(sinSunApparentAngle); // return 1;
        }
    }
    double rSunApparentAngle = asin(sinSunApparentAngle); // between [0, pi/2]

    // compute the apparent Planet radius (angle)
    double sinPlanetApparentAngle = 1.;
    if(normPo > 0) {
        sinPlanetApparentAngle = planet_radius / normPo;
        if(fabs(sinPlanetApparentAngle) > 1.) {
            // we are inside the planet! Always in eclipse!
            sinPlanetApparentAngle =
                1. * MathUtils::sign<real_type>(sinPlanetApparentAngle); // return -0.5; // see definition of umbra
        }
    }
    double rPlanetApparentAngle = asin(sinPlanetApparentAngle); // between [0, pi/2]

    // when drawing the schematics, with 4 lines tangent to both bodies,
    // consider the extramal spacecraft position at the frontier of the different
    // area (umbra, penumbra).
    //
    // since we are far from the sun, ro > rs.
    //
    // if angle < ro-rs then we are in umbra
    // if angle < ro+rs then we are in penumbra
    // Therefore:
    //    with umbra= angle - (ro - rs),    umbra is between [-pi/2, 3/2*pi]
    //    with penumbra= angle - (ro + rs), penumbra is between [-pi, 0]
    double umbra = (angle - (rPlanetApparentAngle - rSunApparentAngle)) / (2 * M_PI);
    double penumbra = (angle - (rPlanetApparentAngle + rSunApparentAngle)) / (2 * M_PI);

    // it is between -1 and 1
    // FIXME normalize such that the max/min are 1 /-1
    real_type val = (bumbra) ? umbra : penumbra;

    if(std::isnan(val)) {
        char str[1024];
        sprintf(str, "sun_radius=%f, planet_radius=%f\n", sun_radius, planet_radius);
        sprintf(str,
                "%s.\nEclipse returned NaN\n\tumbra = %f, penumbra=%f\n\t(sina=%f, rs=%f), (sino=%f, ro=%f), "
                "angle=%f\n\tP{Satellite->Sun}=[%f %f %f]. P{Satellite->Earth}=[%f %f %f]\n",
                str,
                umbra,
                penumbra,
                sinSunApparentAngle,
                rSunApparentAngle,
                sinPlanetApparentAngle,
                rPlanetApparentAngle,
                angle,
                ps(0),
                ps(1),
                ps(2),
                po(0),
                po(1),
                po(2));
        throw HelioLibException(str);
    }

    return val;
}

// ---------------------------------------------------------------------
/** Indicate whether the specified point is in eclipse.
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 *
 * @return true if the specified point is in eclipse, false otherwise.
 */
// ---------------------------------------------------------------------
EclipseCondition Eclipse::getEclipseCondition(const Vector3& rsun,
                                              double sun_radius,
                                              const Vector3& rplanet,
                                              double planet_radius,
                                              const Vector3& rposition)
{
    if((rposition - rplanet).norm() < planet_radius) {
        return InsidePlanet;
    }

    // check if total eclipse (umbra)
    double val = getEclipseSwitchingFunction(rsun, sun_radius, rplanet, planet_radius, rposition, true);
    if(val < 0) {
        return InUmbra;
    }

    // otherwise check if in penumbra
    val = getEclipseSwitchingFunction(rsun, sun_radius, rplanet, planet_radius, rposition, false);
    if(val < 0) {
        return InPenumbra;
    }

    return NotInEclipse;
}
// ---------------------------------------------------------------------
/** Indicate whether the specified point is in eclipse.
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 *
 * @return true if the specified point is in eclipse, false otherwise.
 */
// ---------------------------------------------------------------------
EclipseCondition Eclipse::getEclipseCondition(const Vector3& rsun,
                                              const Vector3& rplanet,
                                              double planet_radius,
                                              const Vector3& rposition)
{
    return getEclipseCondition(rsun, sun_radius, rplanet, planet_radius, rposition);
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function.
 * This function becomes negative when entering the region of shadow and positive when exiting.
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value, between [-1, 1]
 */
// ---------------------------------------------------------------------
double Eclipse::getEclipseRatio(const Vector3& rsun,
                                const Vector3& rplanet,
                                double planet_radius,
                                const Vector3& rposition,
                                bool umbra)
{
    return getEclipseSwitchingFunction(rsun, sun_radius, rplanet, planet_radius, rposition, umbra);
}
// ---------------------------------------------------------------------
/** Get the eclipse switching function.
 * This function becomes negative when entering the region of shadow and positive when exiting.
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value, between [-1, 1]
 */
// ---------------------------------------------------------------------
double Eclipse::getEclipseRatio(const Vector3& rsun,
                                double sun_radius,
                                const Vector3& rplanet,
                                double planet_radius,
                                const Vector3& rposition,
                                bool umbra)
{
    return getEclipseSwitchingFunction(rsun, sun_radius, rplanet, planet_radius, rposition, umbra);
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value derivative
 */
// ---------------------------------------------------------------------
Vector3 Eclipse::getEclipseRatioDerivative(const Vector3& rsun,
                                           const Vector3& rplanet,
                                           double planet_radius,
                                           const Vector3& rposition,
                                           bool umbra)
{
    return getEclipseRatioDerivative(rsun, sun_radius, rplanet, planet_radius, rposition, umbra);
}
// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param robject object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value derivative wrt object's position.
 */
// ---------------------------------------------------------------------
Vector3 Eclipse::getEclipseRatioDerivative(const Vector3& rsun,
                                           double sun_radius,
                                           const Vector3& rplanet,
                                           double planet_radius,
                                           const Vector3& robject,
                                           bool umbra)
{
    // Satellite->Sun vector
    Vector3dExt ps = rsun - robject;
    double nps = ps.norm();
    Matrix3 dpsdr = -Matrix3::Identity();

    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;
    double npo = po.norm();
    Matrix3 dpodr = -Matrix3::Identity();

    // compute the angle (Satellite-Sun center, Satellite-Earth center), in [0, pi]
    // double angle  = ps.angle(po);
    //
    // arccos(f(x))' =-f'(x) / sqrt(1 - f(x)^2)
    // arcsin(f(x))' = f'(x) / sqrt(1 - f(x)^2)
    double sinSunApparentAngle = 1.;
    Vector3 diffNormPs = Vector3::Zero();
    Vector3 drSunApparentAngle = Vector3::Zero();
    if(nps > 0) {
        sinSunApparentAngle = sun_radius / nps;
        diffNormPs = (dpsdr * ps) / nps;

        if(fabs(sinSunApparentAngle) < 1.) {
            // we are outside the Sun
            // double rs  = asin(sina);
            //   arcsin(f(x))'  = f'(x) / sqrt(1 - f(x)^2)
            //   arcsin(f(x))'' = f''(x) / sqrt(1 - f(x)^2) + (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
            Vector3 fp = sun_radius * (-dpsdr * ps / pow(nps, 3));
            drSunApparentAngle = fp / sqrt(1 - sinSunApparentAngle * sinSunApparentAngle);
        }
    }

    double sinPlanetApparentAngle = 1.;
    Vector3 diffNormPo = Vector3::Zero();
    Vector3 drPlanetApparentAngle = Vector3::Zero();
    if(npo > 0) {
        sinPlanetApparentAngle = planet_radius / npo;
        diffNormPo = (dpodr * po) / npo;

        if(fabs(sinPlanetApparentAngle) < 1.) {
            // we are outside the planet
            // double ro     = asin(sino);
            //   arcsin(f(x))'  = f'(x) / sqrt(1 - f(x)^2)
            //   arcsin(f(x))'' = f''(x) / sqrt(1 - f(x)^2) + (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
            Vector3 fp = planet_radius * (-dpodr * po / pow(npo, 3));
            drPlanetApparentAngle = fp / sqrt(1. - sinPlanetApparentAngle * sinPlanetApparentAngle);
        }
    }

#ifdef DEBUG_SMTBX_ECLIPSE
    printf("||Planet->Satellite|| = %12.5g; ||ps|| = %12.5g\n", npo, nps);
    printf("d||Planet->Satellite||/dr = %12.5g %12.5g %12.5g\n", diffNormPo(0), diffNormPo(1), diffNormPo(2));
    printf("d||Sun->Satellite||/dr = %12.5g %12.5g %12.5g\n", diffNormPs(0), diffNormPs(1), diffNormPs(2));
#endif

    //
    double normuv = nps * npo;

    // double angle  = 0;
    Vector3 dangledr = Vector3::Zero();
    if(normuv > 0) {
        double normuv2 = normuv * normuv;
        Vector3 dnormuv = diffNormPs * npo + diffNormPo * nps;

        //
        double cosangle = po.dot(ps) / normuv;
#ifdef DEBUG_SMTBX_ECLIPSE
        printf("cosangle = %12.5g\n", cosangle);
#endif

        // if cosangle == 1, Earth, Sun and SC are aligned. The arccos is zero.
        // The derivative of arccos(x)' = -1/sqrt(1+x^2) is undefined.
        // Indeed, in angle() the angle would be computed with an arcsin.
        // And: arcsin(x)' = 1 / sqrt(1-x^2)
        if(fabs(cosangle) > (1 - 1e-6)) {
            double sinangle = (po.cross(ps)).norm() / normuv;
            Vector3 w = (po.cross(ps));
            if(w.norm() > 0)
                w.normalize();
            // fixme
            Matrix3 skewS, skewO;
            skewS << 0, ps(2), -ps(1), -ps(2), 0, ps(0), ps(1), -ps(0), 0;
            skewO << 0, po(2), -po(1), -po(2), 0, po(0), po(1), -po(0), 0;

            Matrix3 dwdr = skewS - skewO; // FIXME
            Vector3 dsinangle = dwdr * w / normuv - (po.cross(ps)).norm() * dnormuv / normuv2;
            dangledr = dsinangle / sqrt(1 - sinangle * sinangle);
#ifdef DEBUG_SMTBX_ECLIPSE
            printf("sinangle = %12.5g\n", sinangle);
            printf("dsinangle= %12.5g %12.5g %12.5g\n", dsinangle(0), dsinangle(1), dsinangle(2));
#endif
        } else {
            Vector3 dcosangle = (dpsdr * po + dpodr * ps) / normuv - po.dot(ps) * dnormuv / normuv2;
            dangledr = -dcosangle / sqrt(1 - cosangle * cosangle);
        }
    }

#ifdef DEBUG_SMTBX_ECLIPSE
    printf("dangle/dr = %12.5g %12.5g %12.5g\n", dangledr(0), dangledr(1), dangledr(2));
    printf(
        "sinPlanetApparentAngle = %12.5g; sinSunApparentAngle = %12.5g\n", sinPlanetApparentAngle, sinSunApparentAngle);
    printf("d||rPlanetApparentAngle||/dr = %12.5g %12.5g %12.5g\n",
           drPlanetApparentAngle(0),
           drPlanetApparentAngle(1),
           drPlanetApparentAngle(2));
    printf("d||rSunApparentAngle||/dr    = %12.5g %12.5g %12.5g\n",
           drSunApparentAngle(0),
           drSunApparentAngle(1),
           drSunApparentAngle(2));
#endif

    // when drawing the schematics, with 4 lines tangent to both bodies,
    // consider the extramal spacecraft position at the frontier of the different
    // area (umbra, penumbra).
    //
    // if angle < ro-rs then we are in umbra
    // if angle < ro+rs then we are in penumbra
    Vector3 vec;
    if(umbra) {
        // umbra
        // umbra = (angle - (ro - rs))  / (2*pi);
        // double numer = (angle - (ro - rs));
        Vector3 diffNumer = (dangledr - (drPlanetApparentAngle - drSunApparentAngle));
        vec = diffNumer / (2 * M_PI);
    } else {
        // penumbra
        // penumbra = (angle - (ro + rs))  / (2*pi);
        // double numer = (angle - (ro + rs));
        Vector3 diffNumer = (dangledr - (drPlanetApparentAngle + drSunApparentAngle));
        vec = diffNumer / (2 * M_PI);
    }

    return vec;
}
// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value derivative
 */
// ---------------------------------------------------------------------
Matrix3 Eclipse::getEclipseRatioSecondDerivative(const Vector3& rsun,
                                                 const Vector3& rplanet,
                                                 double planet_radius,
                                                 const Vector3& rposition,
                                                 bool umbra)
{
    return getEclipseRatioSecondDerivative(rsun, sun_radius, rplanet, planet_radius, rposition, umbra);
}
// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param robject object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value derivative wrt object's position.
 */
// ---------------------------------------------------------------------
Matrix3 Eclipse::getEclipseRatioSecondDerivative(const Vector3& rsun,
                                                 double sun_radius,
                                                 const Vector3& rplanet,
                                                 double planet_radius,
                                                 const Vector3& robject,
                                                 bool umbra)
{
    // Satellite->Sun vector
    Vector3dExt ps = rsun - robject;
    double nps = ps.norm();
    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;
    double npo = po.norm();

    Matrix3 dpsdr = -Matrix3::Identity();
    Matrix3 dpodr = -Matrix3::Identity();

    // compute the angle (Satellite-Sun center, Satellite-Earth center), in [0, pi]
    // double angle  = ps.angle(po);
    // angle = acos(ps.dot(po)/(|ps|*|po|))
    // using f(x) = ps.dot(po)/(|ps|*|po|)
    //   arccos(f(x))'  =-f'(x) / sqrt(1 - f(x)^2)
    //   arccos(f(x))'' =-f''(x) / sqrt(1 - f(x)^2) - (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
    //   arcsin(f(x))'  = f'(x) / sqrt(1 - f(x)^2)
    //   arcsin(f(x))'' = f''(x) / sqrt(1 - f(x)^2) + (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
    //
    Vector3 diffNormPs = (dpsdr * ps) / nps;
    Vector3 diffNormPo = (dpodr * po) / npo;
    Matrix3 diff2NormPs = dpsdr * dpsdr / nps - (dpsdr * ps) * diffNormPs.transpose() / pow(nps, 2);
    Matrix3 diff2NormPo = dpodr * dpodr / npo - (dpodr * po) * diffNormPo.transpose() / pow(npo, 2);
    //
    double normuv = ps.norm() * npo;
    double normuv2 = normuv * normuv;
    Vector3 dnormuv = diffNormPs * npo + diffNormPo * nps;
    Matrix3 d2normuv = diff2NormPs * npo + diffNormPs * diffNormPo.transpose() + diff2NormPo * nps +
                       diffNormPo * diffNormPs.transpose();
    //
    double cosangle = po.dot(ps) / normuv;
    Vector3 dcosangle = (dpsdr * po + dpodr * ps) / normuv - po.dot(ps) * dnormuv / normuv2;
    Matrix3 d2cosangle = 2 * dpsdr * dpodr / normuv - (dpsdr * po + dpodr * ps) * dnormuv.transpose() / normuv2 -
                         dnormuv * (dpsdr * po + dpodr * ps).transpose() / normuv2 - po.dot(ps) * d2normuv / normuv2 +
                         2 * po.dot(ps) * dnormuv * dnormuv.transpose() / (normuv2 * normuv);
    //
    // Vector3 dangledr =-dcosangle /  sqrt(1 - cosangle * cosangle);
    Matrix3 d2angledr2 = -d2cosangle / sqrt(1 - cosangle * cosangle) -
                         (dcosangle * dcosangle.transpose()) * cosangle / pow(1 - cosangle * cosangle, 3 / 2.);

    // compute the apparent Sun radius (angle)
    double sina = sun_radius / nps;
    Matrix3 d2rs = Matrix3::Zero();
    if(fabs(sina) <= 1.) {
        // we are outside the Sun
        // double rs  = asin(sina);
        double f = sina;
        Vector3 fp = sun_radius * (-diffNormPs / pow(nps, 2));
        Matrix3 fpp = sun_radius * (-diff2NormPs / pow(nps, 2) + 2 * diffNormPs * diffNormPs.transpose() / pow(nps, 3));
        //   arcsin(f(x))'  = f'(x) / sqrt(1 - f(x)^2)
        //   arcsin(f(x))'' = f''(x) / sqrt(1 - f(x)^2) + (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
        // Vector3 drs = fp / sqrt(1 - f*f);
        d2rs = (fpp / sqrt(1 - f * f) + fp * fp.transpose() * f / pow(1 - f * f, 3 / 2.));
    }

    // compute the apparent Planet radius (angle)
    double sino = planet_radius / npo;
    Matrix3 d2ro = Matrix3::Zero();
    if(fabs(sino) <= 1.) {
        // we are outside the planet
        // double ro     = asin(sino);
        double f = sino;
        Vector3 fp = planet_radius * (-diffNormPo / pow(npo, 2));
        Matrix3 fpp =
            planet_radius * (-diff2NormPo / pow(npo, 2) + 2 * diffNormPo * diffNormPo.transpose() / pow(npo, 3));
        //   arcsin(f(x))'  = f'(x) / sqrt(1 - f(x)^2)
        //   arcsin(f(x))'' = f''(x) / sqrt(1 - f(x)^2) + (f'(x))^2 * f(x)/ (1 - f(x)^2)^(3/2)
        // Vector3 dro = fp / sqrt(1 - f*f);
        d2ro = (fpp / sqrt(1 - f * f) + fp * fp.transpose() * f / pow(1 - f * f, 3 / 2.));
    }

    // when drawing the schematics, with 4 lines tangent to both bodies,
    // consider the extramal spacecraft position at the frontier of the different
    // area (umbra, penumbra).
    //
    // if angle < ro-rs then we are in umbra
    // if angle < ro+rs then we are in penumbra
    Matrix3 mat;
    if(umbra) {
        // umbra
        // umbra  = (angle - (ro - rs)) / (ro - rs);
        // umbra' = (dangledr - (dro - drs)) / (ro - rs) - (angle - (ro - rs)) * (dro - drs) / pow((ro - rs), 2);
        // double denom = ro - rs;
        // Vector3 diffDenom = dro - drs;
        // Matrix3 diff2Denom = d2ro - d2rs;
        // double numer = (angle - (ro - rs));
        // Vector3 diffNumer = (dangledr - (dro - drs));
        Matrix3 diff2Numer = (d2angledr2 - (d2ro - d2rs));
        mat = diff2Numer / (2 * M_PI);
    } else {
        // penumbra
        // penumbra  = (angle - (ro + rs)) / (ro + rs);
        // penumbra' = (dangledr - (dro + drs)) / (ro + rs) - (angle - (ro + rs)) * (dro + drs) / pow((ro + rs), 2);
        // double denom = ro + rs;
        // Vector3 diffDenom = dro + drs;
        // Matrix3 diff2Denom = d2ro + d2rs;
        // double numer = (angle - (ro + rs));
        // Vector3 diffNumer = (dangledr - (dro + drs));
        Matrix3 diff2Numer = (d2angledr2 - (d2ro + d2rs));
        mat = diff2Numer / (2 * M_PI);
    }

    return mat;
}

// ---------------------------------------------------------------------
/**
 */
// ---------------------------------------------------------------------
std::string Eclipse::getString(EclipseCondition val)
{
    if(val == InUmbra) {
        return "In Umbra";
    } else if(val == InPenumbra) {
        return "In Penumbra";
    } else if(val == NotInEclipse) {
        return "Not In Eclipse";
    } else if(val == InsidePlanet) {
        return "Inside Planet";
    }
    return "N/A";
}
