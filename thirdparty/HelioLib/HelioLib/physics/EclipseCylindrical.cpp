// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "EclipseCylindrical.hpp"

#include "core/HelioException.hpp"
#include "maths/MathUtils.hpp"

// Contrary to the Eclipse class, this eclipse class consider an cylindrical
// approximation for the eclipse region. There are no penumbra region.
//

//#define DEBUG_SMTBX_ECLIPSE
// ---------------------------------------------------------------------
/** Creator.
 */
// ---------------------------------------------------------------------
EclipseCylindrical::EclipseCylindrical(GenericEphemeridesBase *ephemeris, int planet_id, double planet_radius) 
    : ephemeris(ephemeris), planet_id(planet_id), planet_radius(planet_radius) {

}

// ---------------------------------------------------------------------
/** Indicate whether the specified point is in eclipse. */
EclipseCondition EclipseCylindrical::get_condition(const GenericDate& date, const Vector3& rposition) {
    Vector3dExt r, v;
    ephemeris->getPositionVelocity(planet_id, date, r, v);
    Vector3 rsun = -r;
    Vector3 rplanet(0, 0, 0);
    return getEclipseCondition(rsun, rplanet, rposition, planet_radius);
}
// ---------------------------------------------------------------------
/** Indicate whether the specified point is in eclipse.
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 *
 * @return true if the specified point is in eclipse, false otherwise.
 */
// ---------------------------------------------------------------------
EclipseCondition EclipseCylindrical::getEclipseCondition(const Vector3& rsun,
                                                         const Vector3& rplanet,
                                                         const Vector3& rposition,
                                                         double planet_radius) {
    if((rposition - rplanet).norm() < planet_radius) {
        return InsidePlanet;
    }

    // check if total eclipse (umbra)
    double val = getEclipseSwitchingFunction(rsun, rplanet, planet_radius, rposition);
    if((val < 0)) {// && (val <= 1)) {
        return InUmbra;
    }

    return NotInEclipse;
}

// ---------------------------------------------------------------------
double EclipseCylindrical::getSmoothedEclipseCondition(const Vector3& rsun,
                                                         const Vector3& rplanet,
                                                         double eps,
                                                         const Vector3& rposition) {
    double ratio = getEclipseSwitchingFunction(rsun, rplanet, planet_radius, rposition);
    return SmoothHeavisideFunction::Value(eps, ratio);
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function.
 * This function becomes negative when entering the region of shadow and positive when exiting.
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param rposition object position.
 *
 * @return the switching function value. If value between [0, 1] => umbra. Light elsewhere.
 */
// ---------------------------------------------------------------------
double EclipseCylindrical::getEclipseRatio(const Vector3& rsun,
                                           const Vector3& rplanet,
                                           double planet_radius,
                                           const Vector3& rposition) {
    return getEclipseSwitchingFunction(rsun, rplanet, planet_radius, rposition);
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function.
 * This function becomes negative when entering the region of shadow and positive when exiting.
 *
 * @param rsun sun position vector in a given frame F.
 * @param rplanet shadowing planet's position vector in frame F.
 * @param planet_radius shadowing planet radius.
 * @param robject object position in frame F.
 *
 * The umbra region is defined by a positive returned value.
 * @return the switching function value, between [-inf, +inf].
 */
// ---------------------------------------------------------------------
double EclipseCylindrical::getEclipseSwitchingFunction(const Vector3& rsun,
                                                       const Vector3& rplanet,
                                                       double planet_radius,
                                                       const Vector3& robject) {
    if (planet_radius <= 0) {
        return 1;
    }
    
    // Sun->Earth unit vector
    Vector3dExt ps = rsun - rplanet;
    double dist_sun_occulting_body = ps.norm();
    
    if (dist_sun_occulting_body == 0) {
        return 0;
    }
    
    Vector3dExt udir = ps / dist_sun_occulting_body;

    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;

    // in the base [uDir, vDir, kDir], the spacecraft is in the cylinder with direction uDir,
    // if:
    //  - it is beyond Earth
    //  - And, the spacecraft position in the plane (vDir, kDir) has a norm less than the planet's radius

    // compute the tangential component
    // if tangential < 0  no eclipse are possible
    double tangential = udir.dot(po);

    if ( (fabs(tangential / po.norm()) > 1) || (fabs(planet_radius / po.norm()) > 1)) {
        //printf("Ouups: tangential = %f, |po|=%f, planet_radius=%f\n", tangential, po.norm(), planet_radius);
        return 1.;
    }

    // the angular distance between Earth's center and sun direction should be higher than Earth's apparent radius
    return acos(tangential / po.norm()) - asin(planet_radius / po.norm()) ;

    // compute the normal component of the Earth->Satellite vector in the frame constructed from the Sun->Earth vector
    //double normal = (po - tangential * udir).norm();

    // epsilon in [1, +inf[ or ]-inf, -1]
    // if 1 < epsilon < 2   => umbra
    // otherwise, light
    //return (1 + normal / planet_radius) * HelioLib::MathUtils::sign(tangential);  
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param robject object position.
 *
 * @return the switching function value derivative wrt object's position.
 */
// ---------------------------------------------------------------------
Vector3 EclipseCylindrical::getEclipseRatioDerivative(const Vector3& rsun,
                                                      const Vector3& rplanet,
                                                      double planet_radius,
                                                      const Vector3& robject) {
    // Sun->Earth unit vector
    Vector3dExt ps = rsun - rplanet;
    double normSunEarth = ps.norm();
    Vector3dExt udir = ps / normSunEarth;

    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;
    Matrix3 dpodr = -Matrix3::Identity();

    // compute the normal component of the Earth->Satellite vector in the frame constructed from the Sun->Earth vector
    // tangential = udir.dot(po);
    // normal = (po - tangential * udir).norm();
    // epsilon  = (normal / planet_radius) * tangential;
    real_type tangential = udir.dot(po);
    Vector3 dtangential = -udir;

    real_type normal = (po - tangential * udir).norm();
    Vector3 dnormal = (dpodr - dtangential * udir.transpose()) * (po - tangential * udir) / normal;

    Vector3 depsilon = (dnormal / planet_radius) * tangential + (normal / planet_radius) * dtangential;

#ifdef DEBUG_SMTBX_ECLIPSE
    printf("||Planet->Satellite|| = %12.5g; ||ps|| = %12.5g\n", npo, nps);
    printf("dT/dr = %12.5g %12.5g %12.5g\n", dtangential(0), dtangential(1), dtangential(2));
    printf("dN/dr = %12.5g %12.5g %12.5g\n", dnormal(0), dnormal(1), dnormal(2));
#endif

    return depsilon;
}

// ---------------------------------------------------------------------
/** Get the eclipse switching function derivative, dS/dR
 *
 *
 * @param rsun sun position vector.
 * @param rplanet shadowing planet's position vector.
 * @param planet_radius shadowing planet radius.
 * @param robject object position.
 * @param umbra if true detects Umbra, false detects penumbra
 *
 * @return the switching function value derivative wrt object's position.
 */
// ---------------------------------------------------------------------
Matrix3 EclipseCylindrical::getEclipseRatioSecondDerivative(const Vector3& rsun,
                                                            const Vector3& rplanet,
                                                            double planet_radius,
                                                            const Vector3& robject)
{
    // Sun->Earth unit vector
    Vector3dExt ps = rsun - rplanet;
    double normSunEarth = ps.norm();
    Vector3dExt udir = ps / normSunEarth;

    // Satellite->Earth vector
    Vector3dExt po = rplanet - robject;
    Matrix3 dpodr = -Matrix3::Identity();

    // compute the normal component of the Earth->Satellite vector in the frame constructed from the Sun->Earth vector
    // tangential = udir.dot(po);
    // normal = (po - tangential * udir).norm();
    // epsilon  = (normal / planet_radius) * tangential;
    real_type tangential = udir.dot(po);
    Vector3 dtangential = -udir;
    // d2tangential is zero

    real_type normal = (po - tangential * udir).norm();
    // Vector3 dnormal = (dpodr - dtangential * udir.transpose()) * (po - tangential * udir) / normal;
    Matrix3 d2normal = (dpodr - dtangential * udir.transpose()) * (dpodr - dtangential * udir.transpose()).transpose() /
                       (normal * normal);

    Matrix3 d2epsilon = (d2normal / planet_radius) * tangential;

    return d2epsilon;
}

// ---------------------------------------------------------------------
/**
 */
// ---------------------------------------------------------------------
std::string EclipseCylindrical::getString(EclipseCondition val)
{
    if(val == InUmbra) {
        return "In Umbra";
    } else if(val == NotInEclipse) {
        return "Not In EclipseCylindrical";
    } else if(val == InsidePlanet) {
        return "Inside Planet";
    }
    return "N/A";
}
