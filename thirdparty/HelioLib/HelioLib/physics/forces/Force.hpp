// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PHYSICS_FORCE_HPP_
#define __SMTBX_PHYSICS_FORCE_HPP_

#include <stdlib.h>

#include <cstdio>
#include <string>

#include "frames/Frame.hpp"
#include "frames/BodyPseudoInertialFrame.hpp"
#ifdef WITH_ADVANCED_FRAME
    #include "frames/FramesFactory.hpp"
#endif
#include "date/GenericDate.hpp"
#include "GradientHessian.hpp"
#include "maths/Vector3dExt.hpp"

/** Class describing the general interface to force models */
class Force {
 protected:
    std::string name;
    Frame* frame;
    double mass;

 public:
    /** Constructor. */
    Force(const std::string& name)
        : name(name) {
        #ifdef WITH_ADVANCED_FRAME
            this->frame = (Frame*)FramesFactory::GCRF;
        #else
            this->frame = new BodyPseudoInertialFrame("default", 0);
        #endif
    };

    /** Destructor. */
    virtual ~Force() { }

    /** @return force name. */
    std::string getName() {
        return name;
    }

    /** @return force name. */
    void setName(const std::string& desc) {
        name = desc;
    }

    /** Get the frame the force is expressed in
     * @brief
     * @return
     */
    Frame* getFrame() {
        return frame;
    }

    /** Set the working frame for this force
     * @brief
     * @param f
     */
    void setFrame(Frame* f) {
        frame = f;
    }

    /** Set the S/C mass
     * @brief
     * @return
     */
    double getMass() {
        return mass;
    }

    /** Get the S/C mass
     * @brief
     * @param m
     */
    void setMass(double m) {
        mass = m;
    }

    /** Get the force acceleration in N
     *
     * @param date current date
     * @param pos  position of the S/C
     * @return
     */
    virtual Vector3dExt getAcceleration(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) const = 0;

    /** Get the Jacobian of the force, dF/dXYZ
     *
     * @param date
     * @param pos        S/C position vector
     * @return
     */
    virtual Matrix3 getJacobian(const GenericDate& date, const Vector3dExt& pos) = 0;

    /** Get the Jacobian of the force, dF/dXYZ
     *
     * @param date
     * @param pos        S/C position vector
     * @param vel        S/C velocity vector
     * @return
     */
    virtual Matrix6x3 getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) = 0;

    /** Get the Hessian of the force, d^2F/d(R)^2
     *
     * @param date
     * @param pos        S/C position vector
     * @return
     */
    virtual void getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]) = 0;
};

#endif  // __SMTBX_PHYSICS_FORCE_HPP_
