// $Id: Force.hpp 102 2013-10-27 22:01:02Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "CentralForce.hpp"

// ---------------------------------------------------------------------
/** Constructor.
 */
// --------------------------------------------------------------------- */
CentralForce::CentralForce(real_type mu)
    : Force("Central Force"), mu(mu) { }

// ---------------------------------------------------------------------
/** Gravity Gradient matrix
 * d/dr (-mu * r/||r||^3) = mu*(3*r'*r/r2 - I)/r3
 */
// --------------------------------------------------------------------- */
real_type CentralForce::getGravitationalConstant() {
    return mu;
}
// ---------------------------------------------------------------------
/** Gravity Gradient matrix
 * d/dr (-mu * r/||r||^3) = mu*(3*r'*r/r2 - I)/r3
 */
void CentralForce::getGravityMatrix(const real_type* X, real_type mu, Matrix3& matG) {
    double x = X[0];
    double y = X[1];
    double z = X[2];
    double x2 = x * x;
    double y2 = y * y;
    double z2 = z * z;
    double xy = x * y;
    double xz = x * z;
    double yz = y * z;
    double r2 = x2 + y2 + z2;
    double r3 = r2 * sqrt(r2);
    double r5 = r2 * r3;

    /* g =-(µ/r^3) * r
       G = dg/dr (r=ref)
       G = mu*(3*r'*r/r2 - I)/r3
     */
    matG(0, 0) = (3.0 * x2 - r2) * mu / r5;
    matG(0, 1) = 3.0 * xy * mu / r5;
    matG(0, 2) = 3.0 * xz * mu / r5;
    matG(1, 0) = matG(0, 1);
    matG(1, 1) = (3.0 * y2 - r2) * mu / r5;
    matG(1, 2) = 3.0 * yz * mu / r5;
    matG(2, 0) = matG(0, 2);
    matG(2, 1) = matG(1, 2);
    matG(2, 2) = (3.0 * z2 - r2) * mu / r5;

    /*
    // part for the J2
    if (J0 > 0) {
        double r4 = r2*r2;
        double r7 = r2*r5;
        Vector3dExt R = Vector3dExt(X[0], X[1], X[2]);
        Vector3dExt K = Vector3dExt(0,0,1);
        Vector3dExt dJrdR = -5 * R / r7 * (1 - 5*z2/r2)
                         - 5/r5 * (2 * z * K/r2 - 2 * z2 * R / r4);
        Vector3dExt dJkdR = 2 * (-5. * R * R.dot(K) / r7 + K / r5);
        double   Jr = 1./r5 * (1 - 5*pow(R[2],2)/r2);
        Matrix3 dP = dJrdR*R.transpose() + Jr*Matrix3::Identity() + dJkdR*K.transpose();
        matG -= mu * J0 * dP;
    }*/
}

// ---------------------------------------------------------------------
/** Get the force acceleration in N
 *
 * @param date current date
 * @param pos  position of the S/C
 * @return
 */
Vector3dExt CentralForce::getAcceleration(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) {
    return -mu * pos / pow(pos.norm(), 3);
}

// ---------------------------------------------------------------------
/** Get the Jacobian of the force, dF/dXYZ
 *
 * @param date
 * @param pos        S/C position vector
 * @param vel        S/C velocity vector
 * @return
 */
Matrix3 CentralForce::getJacobian(const GenericDate& date, const Vector3dExt& pos) {
    Matrix3 matG;
    double X[] = { pos(0), pos(1), pos(2) };
    getGravityMatrix(X, mu, matG);
    return matG;
}
// ---------------------------------------------------------------------
/** Get the Jacobian of the force, dF/dXYZ
 *
 * @param date
 * @param pos        S/C position vector
 * @param vel        S/C velocity vector
 * @return
 */
Matrix6x3 CentralForce::getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) {
    Matrix6x3 jac = Matrix6x3::Zero();
    jac.block(0, 0, 3, 3) = getJacobian(date, pos);
    return jac;
}

// ---------------------------------------------------------------------
/** Get the Hessian of the force, d^2F/d(R)^2
 *
 * @param date
 * @param pos        S/C position vector
 * @return
 */
void CentralForce::getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]) {
    real_type x = pos(0);
    real_type x2 = x * x;
    real_type x3 = x2 * x;
    real_type y = pos(1);
    real_type y2 = y * y;
    real_type y3 = y2 * y;
    real_type z = pos(2);
    real_type z2 = z * z;
    real_type z3 = z2 * z;

    real_type r = pos.norm();
    real_type r2 = r * r;

    // computed with wxMaxima
    Matrix3 Gx, Gy, Gz;
    Gx(0, 0) = ((9 * x) - (15 * x3) / r2);     // Gxx/x
    Gx(1, 0) = ((3 * y) - (15 * x2 * y) / r2);  // Gxx/y
    Gx(2, 0) = ((3 * z) - (15 * x2 * z) / r2);  // Gxx/z
    Gx(0, 1) = ((3 * y) - (15 * x2 * y) / r2);  // Gxy/x
    Gx(1, 1) = ((3 * x) - (15 * x * y2) / r2);  // Gxy/y
    Gx(2, 1) = -(15 * x * y * z) / r2;         // Gxy/z
    Gx(0, 2) = ((3 * z) - (15 * x2 * z) / r2);  // Gxz/x
    Gx(1, 2) = -(15 * x * y * z) / r2;         // Gxz/y
    Gx(2, 2) = ((3 * x) - (15 * x * z2) / r2);  // Gxz/z
    //
    Gy(0, 0) = ((3 * y) - (15 * x2 * y) / r2);  // Gyx/x
    Gy(1, 0) = ((3 * x) - (15 * x * y2) / r2);  // Gyx/y
    Gy(2, 0) = -(15 * x * y * z) / r2;         // Gyx/z
    Gy(0, 1) = ((3 * x) - (15 * x * y2) / r2);  // Gyy/x
    Gy(1, 1) = ((9 * y) - (15 * y3) / r2);     // Gyy/y
    Gy(2, 1) = ((3 * z) - (15 * y2 * z) / r2);  // Gyy/z
    Gy(0, 2) = -(15 * x * y * z) / r2;         // Gyz/x
    Gy(1, 2) = ((3 * z) - (15 * y2 * z) / r2);  // Gyz/y
    Gy(2, 2) = ((3 * y) - (15 * y * z2) / r2);  // Gyz/z
    //
    Gz(0, 0) = ((3 * z) - (15 * x2 * z) / r2);  // Gyx/x
    Gz(1, 0) = -(15 * x * y * z) / r2;         // Gyx/y
    Gz(2, 0) = ((3 * x) - (15 * x * z2) / r2);  // Gyx/z
    Gz(0, 1) = -(15 * x * y * z) / r2;         // Gyy/x
    Gz(1, 1) = ((3 * z) - (15 * y2 * z) / r2);  // Gyy/x
    Gz(2, 1) = ((3 * y) - (15 * y * z2) / r2);  // Gyy/y
    Gz(0, 2) = ((3 * x) - (15 * x * z2) / r2);  // Gyz/x
    Gz(1, 2) = ((3 * y) - (15 * y * z2) / r2);  // Gyz/y
    Gz(2, 2) = ((9 * z) - (15 * z3) / r2);     // Gyz/z

    h[0] = Gx;
    h[1] = Gy;
    h[2] = Gz;
}
