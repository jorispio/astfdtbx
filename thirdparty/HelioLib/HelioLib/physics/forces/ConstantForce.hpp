/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_CONSTANT_FORCES_HPP_
#define __SMTBX_CONSTANT_FORCES_HPP_

#include "core/HelioConstants.hpp"
#include "date/GenericDate.hpp"
#include "ephemerides/PlanetAnalyticEphemerides.hpp"
#include "Force.hpp"
#include "frames/Frame.hpp"

class ConstantForce : public Force {
 private:
    Vector3 force;

 public:
    /** Creator */
    ConstantForce(Vector3 &force, Frame *frameIn = (Frame*)FramesFactory::ICRF, double am = 1., double as = 1.);

    /** Destructor */
    ~ConstantForce() { };

    /** */
    Vector3dExt getAcceleration(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) const;

    /** */
    Matrix3 getJacobian(const GenericDate& date, const Vector3dExt& r);

    /** */
    Matrix6x3 getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel);

    /** */
    void getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]);
};

#endif  // __SMTBX_CONSTANT_FORCES_HPP_
