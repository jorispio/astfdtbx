// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PHYSICS_FORCES_GRADIENT_HESSIAN_HPP_
#define __SMTBX_PHYSICS_FORCES_GRADIENT_HESSIAN_HPP_


class GradientHessian {
 private:
    Vector3dExt gradient;
    Matrix3d hessian;

 public:
    /** Simple constructor. */
    GradientHessian(const Vector3dExt& gradient_, const Matrix3d& hessian_) {
        gradient = gradient_;
        hessian = hessian_;
    }

    /** Get a reference to the gradient. */
    Vector3dExt getGradient() {
        return gradient;
    }

    /** Get a reference to the Hessian. */
    Matrix3d getHessian() {
        return hessian;
    }
};
#endif  // __SMTBX_PHYSICS_FORCES_GRADIENT_HESSIAN_HPP_
