/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <iostream>
#include <string>

#include "ThirdBodyPerturbation.hpp"

// ---------------------------------------------------------------------
/** Constructor.
 */
// ---------------------------------------------------------------------
ThirdBodyPerturbation::ThirdBodyPerturbation(int bodyId, Frame *frameIn, double am, double as)
    : Force("Third Body Perturbation with Body "/* + std::to_string(i)*/) {
    ibody = bodyId;
    eph = new PlanetAnalyticEphemerides(am, as);
    mu = eph->getGravitionalParameter(ibody);

    frame = frameIn;
    if (frameIn == NULL) {
        frame = const_cast<IcrfFrame*>(FramesFactory::ICRF);
    }
}

// ---------------------------------------------------------------------
/** Body position in frame frameOut */
// ---------------------------------------------------------------------
Vector3dExt ThirdBodyPerturbation::getBodyPosition(const GenericDate& date) const {
    double r[3];
    eph->getEphemeris(date.getMJD2000(), ibody, r, NULL, NULL);

    Vector3dExt pos;
    pos << r[0], r[1], r[2];
    FrameTransform transform = (const_cast<IcrfFrame*>(FramesFactory::ICRF))->getTransformTo(frame, date);
    return transform.transformPosition(pos);
}
// ---------------------------------------------------------------------
/** */
// ---------------------------------------------------------------------
Vector3dExt ThirdBodyPerturbation::getAcceleration(const GenericDate& date, const Vector3dExt& posInFrame, const Vector3dExt& velInFrame) const {
    Vector3dExt posBodyInFrame = getBodyPosition(date);
    double rb3 = std::pow(posBodyInFrame.norm(), 3);
    Vector3dExt dR = posBodyInFrame - posInFrame;
    double dr3 = std::pow(dR.norm(), 3);
    return -mu * ( dR / dr3 - posBodyInFrame / rb3);
}
// ---------------------------------------------------------------------
/** Derivative of acceleration wrt spacecraft position */
// ---------------------------------------------------------------------
Matrix3 ThirdBodyPerturbation::getJacobian(const GenericDate& date, const Vector3dExt& pos) {
    Vector3dExt dR = getBodyPosition(date) - pos;

    double x = dR(0);
    double y = dR(1);
    double z = dR(2);
    double x2 = x * x;
    double y2 = y * y;
    double z2 = z * z;
    double xy = x * y;
    double xz = x * z;
    double yz = y * z;
    double r = dR.norm();
    double r2 = dR.squaredNorm();
    double r3 = std::pow(r, 3);
    double r5 = r3 * r2;

    Matrix3 jac = Matrix3::Zero();

    /* g =-(µ/r^3) * r
  * G = dg/dr (r=ref)
  * G = mu*(3*r'*r/r2 - I)/r3
     */
    jac(0, 0) = (3.0 * x2 - r2);
    jac(0, 1) = 3.0 * xy;
    jac(0, 2) = 3.0 * xz;
    jac(1, 0) = jac(0, 1);
    jac(1, 1) = (3.0 * y2 - r2);
    jac(1, 2) = 3.0 * yz / r5;
    jac(2, 0) = jac(0, 2);
    jac(2, 1) = jac(1, 2);
    jac(2, 2) = (3.0 * z2 - r2);
    jac *= -mu / r5;

    return jac;
}
// ---------------------------------------------------------------------
/**
 *
 * Gravity Gradient matrix
 * d/dr (-mu * r/||r||^3) = mu*(3*r'*r/r2 - I)/r3
 * G(R) = mu*(3*R'*R/r5 - I/r3)
 *
 * see also:
 *     TrajectoryPropageCartesian::getGravityMatrix
 */
// ---------------------------------------------------------------------
Matrix6x3 ThirdBodyPerturbation::getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) {
    Matrix6x3 jac = Matrix6x3::Zero();
    jac.block<3, 3>(0, 0) = getJacobian(date, pos);
    return jac;
}
// ---------------------------------------------------------------------
/**
 */
// ---------------------------------------------------------------------
void ThirdBodyPerturbation::getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]) {
    Vector3dExt dR = getBodyPosition(date) - pos;

    double x = dR(0);
    double y = dR(1);
    double z = dR(2);
    real_type x2 = x * x;
    real_type x3 = x2 * x;
    real_type y2 = y * y;
    real_type y3 = y2 * y;
    real_type z2 = z * z;
    real_type z3 = z2 * z;

    real_type r = pos.norm();
    real_type r2 = r * r;

    // computed with wxMaxima
    Matrix3 Gx, Gy, Gz;
    Gx(0, 0) = ((9 * x) - (15 * x3) / r2);     // Gxx/x
    Gx(1, 0) = ((3 * y) - (15 * x2 * y) / r2);  // Gxx/y
    Gx(2, 0) = ((3 * z) - (15 * x2 * z) / r2);  // Gxx/z
    Gx(0, 1) = ((3 * y) - (15 * x2 * y) / r2);  // Gxy/x
    Gx(1, 1) = ((3 * x) - (15 * x * y2) / r2);  // Gxy/y
    Gx(2, 1) = -(15 * x * y * z) / r2;         // Gxy/z
    Gx(0, 2) = ((3 * z) - (15 * x2 * z) / r2);  // Gxz/x
    Gx(1, 2) = -(15 * x * y * z) / r2;         // Gxz/y
    Gx(2, 2) = ((3 * x) - (15 * x * z2) / r2);  // Gxz/z
    //
    Gy(0, 0) = ((3 * y) - (15 * x2 * y) / r2);  // Gyx/x
    Gy(1, 0) = ((3 * x) - (15 * x * y2) / r2);  // Gyx/y
    Gy(2, 0) = -(15 * x * y * z) / r2;         // Gyx/z
    Gy(0, 1) = ((3 * x) - (15 * x * y2) / r2);  // Gyy/x
    Gy(1, 1) = ((9 * y) - (15 * y3) / r2);     // Gyy/y
    Gy(2, 1) = ((3 * z) - (15 * y2 * z) / r2);  // Gyy/z
    Gy(0, 2) = -(15 * x * y * z) / r2;         // Gyz/x
    Gy(1, 2) = ((3 * z) - (15 * y2 * z) / r2);  // Gyz/y
    Gy(2, 2) = ((3 * y) - (15 * y * z2) / r2);  // Gyz/z
    //
    Gz(0, 0) = ((3 * z) - (15 * x2 * z) / r2);  // Gyx/x
    Gz(1, 0) = -(15 * x * y * z) / r2;         // Gyx/y
    Gz(2, 0) = ((3 * x) - (15 * x * z2) / r2);  // Gyx/z
    Gz(0, 1) = -(15 * x * y * z) / r2;         // Gyy/x
    Gz(1, 1) = ((3 * z) - (15 * y2 * z) / r2);  // Gyy/x
    Gz(2, 1) = ((3 * y) - (15 * y * z2) / r2);  // Gyy/y
    Gz(0, 2) = ((3 * x) - (15 * x * z2) / r2);  // Gyz/x
    Gz(1, 2) = ((3 * y) - (15 * y * z2) / r2);  // Gyz/y
    Gz(2, 2) = ((9 * z) - (15 * z3) / r2);     // Gyz/z

    h[0] = Gx;
    h[1] = Gy;
    h[2] = Gz;
}
