/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <math.h>
#include <iostream>
#include <string>

#include "ConstantForce.hpp"

// ---------------------------------------------------------------------
/** Constructor.
 */
ConstantForce::ConstantForce(Vector3 &force, Frame *frameIn, double am, double as)
    : Force("Constant force"), force(force) {
    frame = frameIn;
    if (frameIn == NULL) {
        frame = const_cast<IcrfFrame*>(FramesFactory::ICRF);
    }
}

// ---------------------------------------------------------------------
/** */
Vector3dExt ConstantForce::getAcceleration(const GenericDate& date, const Vector3dExt& posInFrame, const Vector3dExt& velInFrame) const {
    return force;
}
// ---------------------------------------------------------------------
/** Derivative of acceleration wrt spacecraft position */
Matrix3 ConstantForce::getJacobian(const GenericDate& date, const Vector3dExt& pos) {
    Matrix3 jac = Matrix3::Zero();
    return jac;
}
// ---------------------------------------------------------------------
/**
 *
 * Gravity Gradient matrix
 * d/dr (-mu * r/||r||^3) = mu*(3*r'*r/r2 - I)/r3
 * G(R) = mu*(3*R'*R/r5 - I/r3)
 *
 * see also:
 *     TrajectoryPropageCartesian::getGravityMatrix
 */
Matrix6x3 ConstantForce::getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel) {
    Matrix6x3 jac = Matrix6x3::Zero();
    jac.block<3, 3>(0, 0) = getJacobian(date, pos);
    return jac;
}
// ---------------------------------------------------------------------
/**
 */
void ConstantForce::getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]) {
    h[0] = Matrix3::Zero();
    h[1] = Matrix3::Zero();
    h[2] = Matrix3::Zero();
}
