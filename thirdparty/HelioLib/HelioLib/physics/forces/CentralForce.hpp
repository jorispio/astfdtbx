// $Id: Force.hpp 102 2013-10-27 22:01:02Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PHYSICS_FORCES_CENTRAL_FORCE_HPP_
#define __SMTBX_PHYSICS_FORCES_CENTRAL_FORCE_HPP_

#include "Force.hpp"

class CentralForce : public Force {
private:
    real_type mu;

 public:
    /** Constructor. */
    CentralForce(real_type mu);

    /** Destructor. */
    virtual ~CentralForce() { }

    /** Get gravitational constant. */
    real_type getGravitationalConstant();

    /* */
    static void getGravityMatrix(const real_type* X, real_type mu, Matrix3& matG);

    Vector3dExt getAcceleration(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel);

    /** Get the Jacobian of the force, dF/dXYZ
     * @param date
     * @param pos        S/C position vector
     * @return
     */
    Matrix3 getJacobian(const GenericDate& date, const Vector3dExt& pos);

    /** Get the Jacobian of the force, dF/dXYZ
     * @param date
     * @param pos        S/C position vector
     * @param vel        S/C velocity vector
     * @return
     */
    Matrix6x3 getJacobian(const GenericDate& date, const Vector3dExt& pos, const Vector3dExt& vel);

    /** Get the Hessian of the force, d^2F/d(R)^2
     * @param date
     * @param pos        S/C position vector
     * @return
     */
    void getHessian(const GenericDate& date, const Vector3dExt& pos, Matrix3 h[3]);
};
#endif  // __SMTBX_PHYSICS_FORCES_CENTRAL_FORCE_HPP_
