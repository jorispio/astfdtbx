// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ECLIPSE_HPP
#define ECLIPSE_HPP

#include "core/HelioDataTypes.hpp"
#include "core/HelioConstants.hpp"


enum EclipseCondition {
    InUmbra, // full eclipse
    InPenumbra,
    NotInEclipse,
    InsidePlanet
};

/** @class Eclipse
 * @brief Eclipse computation class.
 */
class Eclipse
{
private:
    double sun_radius;

    /** */
    static double getEclipseSwitchingFunction(const Vector3& rsun,
                                              double sun_radius,
                                              const Vector3& rplanet,
                                              double planet_radius,
                                              const Vector3& rposition,
                                              bool total);

public:
    /** Creator. */
    Eclipse(double sun_radius = SUN_RADIUS);

    /** Destructor. */
    ~Eclipse(){};

    /** Indicate whether the specified point is in eclipse. */
    EclipseCondition
    getEclipseCondition(const Vector3& rsun, const Vector3& rplanet, double planet_radius, const Vector3& rposition);

    /** */
    double getEclipseRatio(const Vector3& rsun,
                           const Vector3& rplanet,
                           double planet_radius,
                           const Vector3& rposition,
                           bool total);

    /** */
    Vector3 getEclipseRatioDerivative(const Vector3& rsun,
                                      const Vector3& rplanet,
                                      double planet_radius,
                                      const Vector3& rposition,
                                      bool total);

    /** */
    Matrix3 getEclipseRatioSecondDerivative(const Vector3& rsun,
                                            const Vector3& rplanet,
                                            double planet_radius,
                                            const Vector3& rposition,
                                            bool umbra);

    /** */
    static double getEclipseRatio(const Vector3& rsun,
                                  double sun_radius,
                                  const Vector3& rplanet,
                                  double planet_radius,
                                  const Vector3& rposition,
                                  bool total);

    /** */
    static Vector3 getEclipseRatioDerivative(const Vector3& rsun,
                                             double sun_radius,
                                             const Vector3& rplanet,
                                             double planet_radius,
                                             const Vector3& rposition,
                                             bool total);

    /** */
    static Matrix3 getEclipseRatioSecondDerivative(const Vector3& rsun,
                                                   double sun_radius,
                                                   const Vector3& rplanet,
                                                   double planet_radius,
                                                   const Vector3& rposition,
                                                   bool total);

    /** Indicate whether the specified point is in eclipse. */
    static EclipseCondition getEclipseCondition(const Vector3& rsun,
                                                double sun_radius,
                                                const Vector3& rplanet,
                                                double planet_radius,
                                                const Vector3& rposition);

    /** */
    static std::string getString(EclipseCondition val);
};

//---------------------------------------------------------------------------
#endif
