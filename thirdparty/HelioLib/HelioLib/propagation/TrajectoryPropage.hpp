// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General PublinDimensionc License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class TrajectoryPropage
 * return non-autonomous State Transition Matrix
 * The state is extended with the time (parameter).
 * This class implements all the dynamical equations for the problem.
 * Other class such as TPRIMERDYNAMICS or TSTMExt_t use static member
 * of this class for dynamics.
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.3
 * History:
 *        - change into a class
 *        - correct a buf related to the allocation of STM(i)
 *        - Add function GravityMatrix to be called by externally.
 *        - Use MATRIX macro to solve convention issue in the code.
 *        - Extended state Z = [X, t] with X = [R, V]. I included the time.
 *          STM can now be used for time derivatives.
 *        -
 * --------------------------------------------------------------------- */
#ifndef __SMTBX_TRAJECTORY_PROPAGE_HPP_
#define __SMTBX_TRAJECTORY_PROPAGE_HPP_

#include "maths/coordinates/CartesianCoordinates.hpp"
#include "physics/forces/Force.hpp"
#include "propagation/dynamics/DynamicsBase.hpp"
#include "propagation/SimulationEventManager.hpp"

/* ------------------------------------------------------------------------------- */
// also defined in StateTransitionMatrix.hpp
#include "maths/integration/DormandPrince5.hpp"
#define ODESOLVER DOPRI5
/* ------------------------------------------------------------------------------- */
// also defined in StateTransitionMatrix.hpp
#define X_SIZE 7
#define Xt_SIZE 7
#define Xmt_SIZE 8

#define STMt_SIZE (Xt_SIZE * Xt_SIZE)
#define X_STMt_SIZE (Xt_SIZE + STMt_SIZE)

#define RTOL 1e-13
#define ATOL 1e-13

class TDynamicsBase;

/* ------------------------------------------------------------------------------- */
template<int ODE_SIZE = X_SIZE>
class TrajectoryPropage : public ODESOLVER {
 protected:
    /* physical parameters */
    double constMu;
    double constJ2;
    double constJ0;
    double constRe;

    const Frame *frame;

    /* mass flow rate */
    double qfuel;

    double tof;

    /** */
    double rtoler, atoler;
    uint nDimension;

    double trajectoryStep;
    std::vector<double> trajectoryTmesh;
    std::vector<Matrix<real_type, ODE_SIZE, 1>> trajectoryState;
    std::vector<Vector7> trajectoryStateAux;
    std::vector<double> trajectoryTime;

    std::vector<double> auxiliaryEquationInitialState;
    std::vector<double> auxiliaryEquationValue;

    std::vector<const Force*> forces;
    TDynamicsBase *dynamicalModel;

    SimulationEventManager* mEventsHandler;

    /* */
    virtual void DiffEqn(real_type x, const real_type* y, real_type* f) = 0;

    /* */
    int AtStep(long nr, real_type xold, real_type x, const real_type* y) {
        bool isStoppingEvent = false;
        if (mEventsHandler != nullptr) {
            OdePolyDataVar odedata = ODESOLVER::getContinuousPolyData();
            isStoppingEvent |= mEventsHandler->AtStep(nr, xold, x, y, odedata);

            if (isStoppingEvent) {
                // TODO(joris) find the earliest stopping event
                x = mEventsHandler->getEvents().at(0).date;
            }
        }

        if (trajectoryStep > 0) {
            // store all intermediate points, with time step trajectoryStep (dense output)
            double lastTime = trajectoryTime.at(trajectoryTime.size() - 1);
            double xstep = lastTime;
            while (xstep + trajectoryStep < x) {
                xstep += trajectoryStep;
                Vector7 point;
                for (int i = 0; i < ODE_SIZE; ++i) {
                    point(i) = ODESOLVER::cont(i, xstep);
                }
                trajectoryState.push_back(point);
                trajectoryTime.push_back(xstep);

                // auxiliary equations
                if (nDimension > ODE_SIZE) {
                    for (uint i = ODE_SIZE; i < nDimension; ++i) {
                        point(i - ODE_SIZE) = ODESOLVER::cont(i, xstep);
                    }
                    trajectoryStateAux.push_back(point);
                }
            }
        } else if (trajectoryTmesh.size() > 0) {
            // store all intermediate points, with time mesh trajectoryTmesh (dense output)
            double timePoint = trajectoryTmesh.at(0);
            while (x > timePoint) {  // && (xold <= timePoint))
                Vector7 point;
                for (uint i = 0; i < ODE_SIZE; ++i) {
                    point(i) = ODESOLVER::cont(i, timePoint);
                }
                trajectoryState.push_back(point);
                trajectoryTime.push_back(timePoint);

                // auxiliary equations
                if (nDimension > ODE_SIZE) {
                    Vector7 pointAux;
                    for (uint i = ODE_SIZE; i < nDimension; ++i) {
                        pointAux(i - ODE_SIZE) = ODESOLVER::cont(i, timePoint);
                    }
                    trajectoryStateAux.push_back(pointAux);
                }

                trajectoryTmesh.erase(trajectoryTmesh.begin());  // remove first time point
                timePoint = trajectoryTmesh.at(0);
            }
        } else {
            Vector7 point;
            if (isStoppingEvent) {
                for (uint i = 0; i < ODE_SIZE; ++i) {
                    point(i) = ODESOLVER::cont(i, x);
                }        
            } 
            else {
                point << y[0], y[1], y[2], y[3], y[4], y[5], y[6];
            }
            trajectoryState.push_back(point);
            trajectoryTime.push_back(x);

            // auxiliary equations
            if (nDimension > ODE_SIZE) {
                Vector7 pointAux;
                for (uint i = ODE_SIZE; i < nDimension; ++i) {
                    pointAux(i - ODE_SIZE) = y[i];
                }
                trajectoryStateAux.push_back(pointAux);
            }
        }

        return (isStoppingEvent)? -1 : 0;
    }


    // ---------------------------------------------------------------------
    /** Input must be 6 dim. We fill then the remaining dim.
     *
     * @param initialState  initial position-velocity state
     * @param t0            initial date, sec
     * @param ti            final date, sec
     * @param step
     */
    int getTrajectory(const Matrix<real_type, ODE_SIZE, 1>& initialState,
                      double t0,
                      double ti,
                      double step,
                      /* OUTPUTS */
                      std::vector<double>& trajectoryTimeOut,
                      std::vector<Matrix<real_type, ODE_SIZE, 1>>& trajectoryStateOut,
                      double &finalTime,
                      Matrix<real_type, ODE_SIZE, 1>& finalState) {
        double* STATE0 = new double[nDimension];
        double tspan[2];

        for (int i = 0; i < ODE_SIZE; ++i) {
            STATE0[i] = initialState(i);
        }

        //if (STATE0[6] <= 0) {
        //    delete [] STATE0;
        //    char str[255];
        //    snprintf(str, sizeof(str), "Incompatible initial mass (m = %f)\n", STATE0[6]);
        //    throw HelioLibException(str);
        //}

        // auxiliary equations
        if (nDimension > ODE_SIZE) {
            if ((nDimension - ODE_SIZE) != auxiliaryEquationInitialState.size()) {
                char str[255];
                snprintf(str, sizeof(str),
                        "Bad size of auxiliary initial guess. Expected %d, but it is %d\n",
                        nDimension - ODE_SIZE,
                        static_cast<int>(auxiliaryEquationInitialState.size()));
                fprintf(stderr, "%s", str);
                throw HelioLibException(str);
            }

            for (uint i = ODE_SIZE; i < nDimension; ++i) {
                STATE0[i] = auxiliaryEquationInitialState.at(i - ODE_SIZE);
            }
        }

        // first point
        trajectoryState.clear();
        trajectoryState.push_back(initialState);
        trajectoryTime.clear();
        trajectoryTime.push_back(t0);
        if (nDimension > ODE_SIZE) {
            trajectoryStateAux.clear();
            Vector7 auxi;
            for (int idx=0; idx<auxiliaryEquationInitialState.size(); ++idx) {
                auxi(idx) = auxiliaryEquationInitialState.at(idx);
            }
            trajectoryStateAux.push_back(auxi);
        }

        if (mEventsHandler != NULL) {
            mEventsHandler->clearEvents();
        }

        trajectoryStep = step;
        if (trajectoryStep > 0) {  // allow dense output if required
            setmaxdense(nDimension);
        }

        // we are only interested by the final result, i.e. no dense output
        int ret = 0;
        if (fabs(t0 - ti) > 1e-6) {
            tspan[0] = t0;
            tspan[1] = ti;
            tof = (ti - t0);

            // ODESOLVER::Initialise(OdeStepperMode::call_for_output_only);
            ODESOLVER::Initialise(OdeStepperMode::call_for_dense_output);

            ret = solve(tspan[0], STATE0, tspan[1]);
            if (ret < 1) {
                printf("TrajectoryPropage res = %d (problem during integration on [%f, %f]=[%g] at t=%g)\n",
                       ret,
                       t0,
                       ti,
                       ti - t0,
                       tspan[0]);
                printf("                    initialState = [%7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f",
                       initialState(0),
                       initialState(1),
                       initialState(2),
                       initialState(3),
                       initialState(4),
                       initialState(5),
                       initialState(6));
                if (nDimension > ODE_SIZE) {
                    for (int idx = 0; idx < auxiliaryEquationInitialState.size(); ++idx) {
                        printf(", %7.4f", auxiliaryEquationInitialState.at(idx));
                    }
                }
                printf("]\n");
                Vector7 currentPoint = trajectoryState.at(trajectoryState.size() - 1);
                printf("                    currentState = [%7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f ",
                       currentPoint(0),
                       currentPoint(1),
                       currentPoint(2),
                       currentPoint(3),
                       currentPoint(4),
                       currentPoint(5),
                       currentPoint(6));
                if (nDimension > ODE_SIZE) {
                    for (int idx = ODE_SIZE; idx < nDimension; ++idx) {
                        printf(", %7.4f", STATE0[idx]);
                    }
                }
                printf("]\n");

                // copy computed points
                trajectoryStateOut = trajectoryState;
                trajectoryTimeOut = trajectoryTime;
                
                delete[] STATE0;
        
                // throw HelioLibException();
                //assert(0);
                return ret;
            }
        }

        // final point
        finalState << STATE0[0], STATE0[1], STATE0[2], STATE0[3], STATE0[4], STATE0[5], STATE0[6];
        finalTime = tspan[0];

        if (fabs(trajectoryTime.at(trajectoryTime.size() - 1) - tspan[1]) > 1e-9) {
            trajectoryState.push_back(finalState);
            trajectoryTime.push_back(tspan[1]);
        }

        trajectoryStateOut = trajectoryState;
        trajectoryTimeOut = trajectoryTime;

        delete[] STATE0;
        return ret;
    }

    // ---------------------------------------------------------------------
    /** Input must be 6 dim. We fill then the remaining dim.
     *
     * @param initialState  initial position-velocity state
     * @param t0            initial date, sec
     * @param ti            final date, sec
     * @param step
     */
    int getTrajectory(const Matrix<real_type, ODE_SIZE, 1>& initialState,
                      const std::vector<double>& tmesh,
                      /* OUTPUTS */
                      std::vector<Matrix<real_type, ODE_SIZE, 1>>& trajectoryStateOut) {
        std::vector<double> trajectoryTimeOut;
        double finalTime;
        Vector7 finalState;

        assert(tmesh.size() > 0);
        // throw HelioLibException()

        trajectoryTmesh = tmesh;
        trajectoryTmesh.erase(
            trajectoryTmesh.begin());  // remove first time point. It is already push_back as initial point of integration.

        setmaxdense(nDimension);

        int ret = getTrajectory(
            initialState, tmesh.at(0), tmesh.at(tmesh.size() - 1), 0, trajectoryTimeOut, trajectoryStateOut, finalTime, finalState);

        assert(tmesh.size() == trajectoryTimeOut.size()); // little check
        return ret;
    }

    std::vector<Matrix<real_type, ODE_SIZE, 1>> getTrajectoryAuxiliary(void)
    {
        return trajectoryStateAux;
    }

    void getTrajectoryAuxiliary(int i, std::vector<double>& auxout){
        auxout.clear();
        std::vector<Matrix<real_type, ODE_SIZE, 1>> aux = getTrajectoryAuxiliary();
        int sz = aux.size();
        for (int i = 0; i < sz; ++i) {
            auxout.push_back(aux.at(i)(index));
        }
    }

 public:
     
    /* ---------------------------------------------------------------------
     * Creator
     */
    TrajectoryPropage(double mu_, const Frame *frameIn, double J2_=0, double rE_=0, double q=0)
        : ODESOLVER(ODE_SIZE),
          frame(frameIn),
          nDimension(ODE_SIZE) {
        mEventsHandler = NULL;
        setConstants(mu_, J2_, rE_);
        qfuel = q;

        setmaxdense(0);
        rtoler = RTOL;
        atoler = ATOL;
        settol(atoler, rtoler);
    }
    /* ---------------------------------------------------------------------
     * Creator
     */
    TrajectoryPropage(uint nAuxEqu, double mu_, const Frame *frameIn, double J2_=0, double rE_=0, double q=0)
        : ODESOLVER(ODE_SIZE + nAuxEqu),
          frame(frameIn),
          nDimension(ODE_SIZE + nAuxEqu) {
        assert(nAuxEqu < 8);  // for now the number of auxiliary equations is limited to 7
        
        mEventsHandler = NULL;
        setConstants(mu_, J2_, rE_);
        qfuel = q;

        setmaxdense(0);
        rtoler = RTOL;
        atoler = ATOL;
        settol(atoler, rtoler);
        
        auxiliaryEquationInitialState.assign(nAuxEqu, 0.);
    }

    ~TrajectoryPropage(void) { }

    /* */
    void setConstants(double mu_, double J2_ = 0, double rE_ = 0) {
        constMu = mu_;
        constJ2 = J2_;
        constRe = rE_;
        constJ0 = 3. / 2. * constJ2 * pow(constRe, 2);
    }

    double getConstantMu() {
        return constMu;
    }
    double getCOnstantJ2() {
        return constJ2;
    }
    double getConstantRe() {
        return constRe;
    }

    /** Return pointer to the dynamical model. */
    TDynamicsBase* get_dynamical_model() const {
        return dynamicalModel;
    }
    
    /** Return event manager. */
    SimulationEventManager* get_event_manager() {
        return mEventsHandler;
    }
    
    /** Add event handler. */
    void set_event_manager(SimulationEventManager* handler) {
        mEventsHandler = handler;
        if (handler != nullptr) {
            setmaxdense(nDimension);
        }
    }

    /** Clear event handler list. */
    void clear_event_manager() {
        if (mEventsHandler != nullptr) {
            mEventsHandler->clearEventHandler();
        }
        mEventsHandler = NULL;
    }

    /** Add a force to the dynamical model */
    void addForce(const Force *force) {
        forces.push_back(force);
        if (dynamicalModel) {
            dynamicalModel->addForce(force);
        }
    }

    /** remove dynamical forces */
    void clearForces(void){
        forces.clear();
        if (dynamicalModel) {
            dynamicalModel->clearForces();
        }
    }
    
    /** */
    std::vector<const Force*> getForces() const {
        return forces;
    }
    
    /** */
    void setMaxSteps(int n) {
        nmax = n;
    }

    /* */
    void setTolerances(double atol, double rtol) {
        atoler = atol;
        rtoler = rtol;
    }

    /* */
    void AddAuxiliaryEquationInitialState(int index, real_type value){
        auxiliaryEquationInitialState.at(index) = value;
    }

    /* */
    void AddAuxiliaryEquationValue(int index, real_type value){
        auxiliaryEquationValue.push_back(value);
    }

    /** */
    virtual int getFinalPoint(const Matrix<real_type, ODE_SIZE, 1>& x0,
                              double t0,
                              double ti,
                              /* OUTPUTS */
                              double &tf,
                              Matrix<real_type, ODE_SIZE, 1>& xf) = 0;

    /** */
    virtual void getStateDynamics(real_type t, const real_type* X, const real_type* uqsw, real_type* X_dot) = 0;

    /** */
    virtual void getCoStateDynamics(real_type t, const real_type* X, const real_type* uqsw, real_type* Ldot) = 0;

    virtual void toFile(std::string filename,
                       double a_m,
                       double a_kg) = 0;

    virtual void toFile(std::string filename,
                       const Frame *frame,
                       double a_m,
                       double a_kg) = 0;
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_TRAJECTORY_PROPAGE_HPP_
