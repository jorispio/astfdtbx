// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class STM
 * return non-autonomous State Transition Matrix
 * The state is extended with the time (parameter).
 * This class implements all the dynamical equations for the problem.
 * Other class such as TPRIMERDYNAMICS or TSTMExt_t use static member
 * of this class for dynamics.
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.3
 * History:
 *        - change into a class
 *        - correct a buf related to the allocation of STM(i)
 *        - Add function GravityMatrix to be called by externally.
 *        - Use MATRIX macro to solve convention issue in the code.
 *        - Extended state Z = [X, t] with X = [R, V]. I included the time.
 *          STM can now be used for time derivatives.
 *        -
 * --------------------------------------------------------------------- */
#include "StateTransitionMatrixRtbpWithCostate.hpp"

#include <float.h> /* DBL_EPSILON */
#include <math.h>  /* fabs */
#include <stdlib.h>
#include <string.h>

#include <cstdio>
#include <vector>

#include "maths/MatrixArray.hpp"
#include "StateTransitionMatrix.hpp"
#include "TrajectoryPropageRtbp.hpp"

// ---------------------------------------------------------------------
/** Creator
 */
StateTransitionMatrixRtbpWithCostate::StateTransitionMatrixRtbpWithCostate(double mu_ratio)
    : StateTransitionMatrix(mu_ratio, 0, 0) {
    uxyzDefault[0] = 0.;
    uxyzDefault[1] = 0.;
    uxyzDefault[2] = 0.;

    dynamicalModel = new EllipticThreeBodyProblemWithCostate(mu_ratio, ThrusterData());
    dynamicalModel->setTimeNormalisation(1.);
}
// ---------------------------------------------------------------------
/**
 */
void StateTransitionMatrixRtbpWithCostate::setDefaultControl(const Vector3& u) {
    uxyzDefault[0] = u(0);
    uxyzDefault[1] = u(1);
    uxyzDefault[2] = u(2);
}
// ---------------------------------------------------------------------
/** Input must be 6 dim. We fill then the remaining dim.
 * Return 6x6 transition matrix STM(t0, ti)
 * contruct a multi impulse trajectory, with patched conic approximation.
 * Each legs are computed individually and then patched together.
 */
int StateTransitionMatrixRtbpWithCostate::getSTM(const Vector3d& r1,
                                           const Vector3d& v1,
                                           double t0,
                                           double &ti,
                                           /* OUTPUTS */
                                           Matrix3& STM1,
                                           Matrix3& STM2,
                                           Matrix3& STM3,
                                           Matrix3& STM4,
                                           Matrix7& STMf,
                                           Vector3dExt& P1,
                                           Vector3dExt& P2,
                                           double& P3,
                                           Vector3d& rf,
                                           Vector3d& vf) {
    double X0[Xt_SIZE];
    for (int i = 0; i < 3; ++i) {
        X0[i] = r1(i);
        X0[i + 3] = v1(i);
    }
    X0[6] = 0;

    Matrix7 STM0_notused = Matrix7::Identity();
    double Xf[Xt_SIZE];
    int ret = StateTransitionMatrix::getSTM(X0, STM0_notused, t0, ti, STMf, Xf);

/* The result of solve() is PHI(tf, t0) when integrating from t0 to tf.
     *
     * use simplectic property for inversion for a better precision
 * STM = [STM1, STM2; STM3, STM4]
 * invSTM = [STM4, -STM2; -STM3, STM1]
     * inv(STM(t, t0)) = STM(t0, t)
 *        STM1 -> STM4'
     *        STM2 -> -STM2'
     *        STM3 -> -STM3'
     *        STM4 -> STM1'
     */
/* CAREFUL: C and MATLAB array conventions */

    STMdispatch(STMf, STM1, STM2, STM3, STM4);
    P1 = STMf.block(0, 6, 3, 1);
    P2 = STMf.block(3, 6, 3, 1);
    P3 = 1.;

    for (int i = 0; i < 3; ++i) {
        rf(i) = Xf[i];
        vf(i) = Xf[i + 3];
    }

    return ret;
}

// ---------------------------------------------------------------------
/**
 */
int StateTransitionMatrixRtbpWithCostate::getSTM(const Vector3d& r1,
                                           const Vector3d& v1,
                                           double t0,
                                           double &ti,
                                           /* OUTPUTS */
                                           Matrix3& STM1,
                                           Matrix3& STM2,
                                           Matrix3& STM3,
                                           Matrix3& STM4,
                                           Matrix6& STMf,
                                           Vector3d& rf,
                                           Vector3d& vf) {
    Vector3dExt P1, P2;
    Matrix7 STMf7;
    double P3 = 1;
    int res = getSTM(r1, v1, t0, ti, STM1, STM2, STM3, STM4, STMf7, P1, P2, P3, rf, vf);
    STMf = STMf7.block(0, 0, 6, 6);
    return res;
}

// ---------------------------------------------------------------------
/** DYNAMIC
 * Integrate state dynamic equation, and state transition matrix
 * simulteneously
 */
void StateTransitionMatrixRtbpWithCostate::DiffEqn(real_type tau, const real_type* X, real_type* Xdot) {
    int sz[] = { Xt_SIZE, Xt_SIZE };

    // SpaceCraft Dynamic, no mass
    CommonVariables cV;
    if (dynamicalModel->ComputeCommonVariablesForStateOnly(tau, X, cV)) {
        dynamicalModel->GetStateDynamics(tau, cV, Xdot);
    }

    // t -> tau
    // double dt_dtau = tof;
    Xdot[6] = 0;
    if (fabs(tof) > 1e-13) {
        Xdot[6] = 1 / tof;
    }

    // STMSYN
    // State Transition Matrix Dynamic
    // --------------------------------
    Matrix7 STM;
    for (int i = 0; i < Xt_SIZE; ++i)
        for (int j = 0; j < Xt_SIZE; ++j)
            STM(i, j) = MATRIX(X + Xt_SIZE, i, j, sz);

    MATRIXVAR dFdX = MATRIXVAR::Zero(STATE_SIZE, STATE_SIZE);
    dynamicalModel->JacobianMatrixState(cV, dFdX);

    /* diff(F, t) */
    Vector7 dFdT;
    dynamicalModel->DynamicsTime(X, Xdot, dFdT);
    dFdX(0, 6) /* A(1,7) */ = dFdT(0);
    dFdX(1, 6) /* A(2,7) */ = dFdT(1);
    dFdX(2, 6) /* A(3,7) */ = dFdT(2);
    dFdX(3, 6) /* A(4,7) */ = dFdT(3);
    dFdX(4, 6) /* A(5,7) */ = dFdT(4);
    dFdX(5, 6) /* A(6,7) */ = dFdT(5);
    dFdX(6, 6) = dFdT(6);

    if (forces.size() > 0) {
        for (std::vector<Force*>::iterator f = forces.begin(); f != forces.end(); ++f) {
            // TODO(joris) Update A with forces Jacobian
        }
    }

    Matrix7 dSTM = dFdX * STM;
    for (int i = 0; i < Xt_SIZE; ++i) {
        for (int j = 0; j < Xt_SIZE; ++j) {
            MATRIX(Xdot + Xt_SIZE, i, j, sz) = dSTM(i, j);  // transpose
        }
    }
}

// ---------------------------------------------------------------------
/**
 * Dispatches elements regarding state [R, V]
 * These are submatrices of size 3 x 3
 */
void StateTransitionMatrixRtbpWithCostate::STMdispatch(const Matrix7& STM,
                                                 Matrix3& STM1,
                                                 Matrix3& STM2,
                                                 Matrix3& STM3,
                                                 Matrix3& STM4) {
    /* STM1 = STM(1:3,1:3); */
    STM1 = STM.block(0, 0, 3, 3);
    /* STM2 = STM(1:3,4:6); */
    STM2 = STM.block(0, 3, 3, 3);
    /* STM3 = STM(4:6,1:3); */
    STM3 = STM.block(3, 0, 3, 3);
    /* STM4 = STM(4:6,4:6); */
    STM4 = STM.block(3, 3, 3, 3);
}

// ---------------------------------------------------------------------
/** Differentiation of the dynamics with respect to time. No mass.
 * Can be used for some derivatives.
 *
 * @param x  state and costate vector
 */
void StateTransitionMatrixRtbpWithCostate::getJacobian(real_type tau,
                                                 const real_type* X,
                                                 real_type* uxyz,
                                                 real_type mu,
                                                 real_type J0,
                                                 std::vector<Force*> &forces,
                                                 double tof,
                                                 /* OUTPUTS */ Matrix7& Jacobian) {
    throw HelioLibException("getJacobian()", "Not implemented", "Not implemented");
}
