// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class TrajectoryPropageRtbp
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.0
 * --------------------------------------------------------------------- */
#ifndef __SMTBX_TRAJECTORY_PROPAGE_RTBP_HPP_
#define __SMTBX_TRAJECTORY_PROPAGE_RTBP_HPP_

#include "control/ControlEnum.hpp"
#include "dynamics/DynamicsBase.hpp"
#include "maths/coordinates/CartesianCoordinates.hpp"
#include "TrajectoryPropage.hpp"


/* ------------------------------------------------------------------------------- */
class TrajectoryPropageRtbp : public TrajectoryPropage<X_SIZE> {
 private:
    double a_s;

    /* */
    void dynamic(unsigned n, double t, double* X, double* Xdot);

    CONTROL_DIRECTION controlDirection;
    double controlAmplitude;
    double uDefault[3];

 protected:
    /* */
    void DiffEqn(double x, const double* y, double* f);

 public:
    /* */
    static void getStateDynamics(double tau,
                                 const double* X,
                                 double mass,
                                 const double* uxyz,
                                 double mu,
                                 double J0,
                                 std::vector<Force*> forces,
                                 double qfuel,
                                 double* Xdot,
                                 bool withmass);

    static void getCoStateDynamics(double tau, const double* X, const double* uxyz, double mu, double J0, double q, double* Ldot);

 public:
    TrajectoryPropageRtbp(double mu1_, double mu2_, double ecc);
    TrajectoryPropageRtbp(double mu_ratio_, double ecc);
    ~TrajectoryPropageRtbp(void) { }

    void setDefaultControl(CONTROL_DIRECTION direction, double amplitude, const Vector3& u);

    /** */
    void getStateDynamics(double tau, const double* X, const double* uqsw, double* X_dot);

    /** */
    void getCoStateDynamics(double tau, const double* X, const double* uqsw, double* Ldot);


    int getFinalPoint(const Vector7& x0,
                      double t0,
                      double ti,
                      /* OUTPUTS */
                      double &finalTime,
                      Vector7& xf);

    /* */
    int getFinalPoint(const Vector3d& r1,
                      const Vector3d& v1,
                      double m1,
                      double t0,
                      double ti,
                      /* OUTPUTS */
                      double &finalTime,
                      Vector3d& rf,
                      Vector3d& vf,
                      double& mf);

    /* */
    int getTrajectory(const Vector3d& r1,
                      const Vector3d& v1,
                      double m1,
                      double t0,
                      double ti,
                      double step,
                      /* OUTPUTS */
                      std::vector<double>& trajectoryTime,
                      std::vector<Vector7>& trajectoryState,
                      double &finalTime,
                      Vector3d& rf,
                      Vector3d& vf,
                      double& mf);

    int getTrajectory(const Vector3d& r1,
                      const Vector3d& v1,
                      double m1,
                      std::vector<double>& tmesh,
                      /* OUTPUTS */
                      std::vector<Vector7>& trajectory);

    /** */
    void toFile(std::string filename,
                       double a_m,
                       double a_kg);

    /** */
    void toFile(std::string filename,
                       const Frame *frameOut,
                       double a_m,
                       double a_kg);

    /** */
    static void toFile(FILE* fid,
                       std::vector<double>& trajectoryTime,
                       std::vector<Vector7>& trajectoryState,
                       const Frame *frameIn,
                       const Frame *frameOut,
                       double a_m,
                       double a_s,
                       double a_kg);
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_TRAJECTORY_PROPAGE_RTBP_HPP_
