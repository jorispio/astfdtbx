// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SIMULATION_EVENT_MANAGER_HPP
#define __SIMULATION_EVENT_MANAGER_HPP

#include "core/HelioConstants.hpp"
#include "core/HelioDataTypes.hpp"
#include "core/HelioEnumerations.hpp"
#include "propagation/SimulationEvent.hpp"
#include "maths/integration/OdePolyData.hpp"

class SimulationEventManager : public SimulationEventHandler {
private:
    double maxCheckInterval;
    int maxiter;
    double tol;

    /** event handlers */
    std::vector<const SimulationEventHandler*> mEventHandlers;

    /** */
    std::vector<SimulationEvent> events;

    /** */
    bool findCrossing(long nr,
                        real_type xold,
                        real_type x,
                        const real_type* y,
                        SimulationEventHandler* handler,
                        OdePolyDataVar& data);

    ODE_SOLVER_ID ode_solver;

public:
    /** Creator. */
    SimulationEventManager(ODE_SOLVER_ID solver = ODE_DOPRI5);

    /** Destructor. */
    ~SimulationEventManager() { };

    /** Set zero finding tolerance*/
    void setTolerance(double tol_);

    /** Set max check interval */
    void setMaxCheckInterval(double maxCheckInterval_);

    /** Add event handler. */
    void addEventHandler(const SimulationEventHandler* handler);

    /** Clear event handler list. */
    void clearEventHandler();

    /** Add event. */
    void addEvent(double t, const char* stype, const char* str);

    /** */
    void clearEvents() { events.clear(); };

    /** */
    std::vector<SimulationEvent> getEvents() const;

    /** Events handler. */
    bool AtStep(long nr, real_type xold, real_type x, const real_type* y, OdePolyDataVar& data);

    real_type switchingFunction(real_type , real_type* ) { return 0; };
};
// ---------------------------------------------------------------------
#endif  // __SIMULATION_EVENT_MANAGER_HPP

