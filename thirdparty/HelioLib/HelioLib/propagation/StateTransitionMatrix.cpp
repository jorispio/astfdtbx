// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
 /* --------------------------------------------------------------------- *
  * @class STM
  * return non-autonomous State Transition Matrix
  * The state is extended with the time (parameter).
  * This class implements all the dynamical equations for the problem.
  * Other class such as TPRIMERDYNAMICS or TSTMExt_t use static member
  * of this class for dynamics.
  *
  * Author: Joris Olympio
  * Last Revision:
  * Ver: 1.3
  * History:
  *        - change into a class
  *        - correct a buf related to the allocation of STM(i)
  *        - Use MATRIX macro to solve convention issue in the code.
  *        - Extended state Z = [X, t] with X = [R, V]. I included the time.
  *          STM can now be used for time derivatives.
  * --------------------------------------------------------------------- */
#include "StateTransitionMatrix.hpp"

#include <float.h> /* DBL_EPSILON */
#include <math.h>  /* fabs */

#include <cstdio>
#include <stdlib.h>
#include <string.h>

#include <vector>





