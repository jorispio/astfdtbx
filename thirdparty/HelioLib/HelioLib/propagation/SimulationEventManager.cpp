// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "SimulationEventManager.hpp"

#include <string>
#include <vector>

#include "maths/MathUtils.hpp"
#include "maths/integration/DormandPrince5.hpp"

// ---------------------------------------------------------------------
/** Creator.
 */
SimulationEventManager::SimulationEventManager(ODE_SOLVER_ID solver)
    : SimulationEventHandler() {
    ode_solver = solver;

    // checking time interval. Period is split in N intervals of size at most maxCheckInterval.
    maxCheckInterval = 1e-5;
    maxiter = 100;
    tol = 1e-9;
}

// ---------------------------------------------------------------------
void SimulationEventManager::setMaxCheckInterval(double maxCheckInterval_) {
    maxCheckInterval = maxCheckInterval_;
}
// ---------------------------------------------------------------------
void SimulationEventManager::setTolerance(double tol_) {
    tol = tol_;
}
/* --------------------------------------------------------------------- */
/** Set event handler.
 */
void SimulationEventManager::addEventHandler(const SimulationEventHandler* handler) {
    mEventHandlers.push_back(handler);
}

/* --------------------------------------------------------------------- */
/** Clear event handler.
 */
/* --------------------------------------------------------------------- */
void SimulationEventManager::clearEventHandler() {
    mEventHandlers.clear();
}

/* --------------------------------------------------------------------- */
/** Add event.
 */
void SimulationEventManager::addEvent(double t, const char* type, const char* desc) {
    std::string sType(type);
    std::string sDesc(desc);
    SimulationEvent event(t, sType, sDesc);
    events.push_back(event);
}

// ---------------------------------------------------------------------
/** Find crossing of the swtitching function.
 * @param xold
 * @param x
 * @param y
 * @param xs  root
 * @return true if a crossing occured
 */
bool SimulationEventManager::findCrossing(long,
                                          real_type xold,
                                          real_type x,
                                          const real_type* y,
                                          SimulationEventHandler* handler,
                                          OdePolyDataVar& data) {
    uint sz = data.size;
    double* ya = new double[sz];
    double* yb = new double[sz];

    // find crossings of the switching function
    // with a bracketing approach
    real_type xa = xold;
    DOPRI5::cont(/*ode_solver, */data, xa, ya);
    real_type fa = handler->switchingFunction(xa, ya);

    real_type xb = x;
    memcpy(yb, y, sz * sizeof(real_type));
    real_type fb = handler->switchingFunction(xb, yb);
    if (fa * fb >= 0) {
        // no crossing
        delete ya;
        delete yb;
        return false;
    }

    bool decreasing = false;
    if (fa > fb) {
        decreasing = true;
    }
    if (!((decreasing && (handler->getDirection() <= 0)) ||
        (!decreasing && (handler->getDirection() >= 0)))) {
        return false;    
    }

    int iter = 0;
    double xc = xa;
    real_type fc = 0;
    double* yc = new double[sz];
    bool found = false;
    while ((fabs(xa - xb) > tol) && (iter < maxiter)) {
        real_type xc = (xa + xb) / 2.;

        // get function value at xc
        //data.cont5(xc, yc)
        DOPRI5::cont(data, xc, yc);

        // get switching function value at (xc, yc)
        fc = handler->switchingFunction(xc, yc);

        if (fa * fc < 0) {
            // root is in [xa, xc]
            xb = xc;
            fb = fc;
        } else if (fc * fb < 0) {
            // root is in [xc, xb]
            xa = xc;
            fa = fc;
        } else {
            // root is xc
            found = true;
            break;
        }

        ++iter;
    }

    if ((fabs(xa - xb) > tol) && !found) {
        printf("Could not refine event. fa=%g fb=%g, fc=%g\n   |xa-xb|=%g, xa=%g, xb=%g xc=%g. iter=%d\n",
                    fa, fb, fc, fabs(xa - xb), xa, xb, xc, iter);
    }
    addEvent(xc, handler->getTypeId().c_str(), handler->getName().c_str());

    delete[] ya;
    delete[] yb;
    delete[] yc;

    return true;
}
// ---------------------------------------------------------------------
/** step handler.
 */
bool SimulationEventManager::AtStep(long nr, real_type xold, real_type x, const real_type* y, OdePolyDataVar& data) {
    // time interval in normalized seconds
    double dt = x - xold;
    if (fabs(dt) < epsilon) {
        return false;
    }

    long nsteps = ceil(dt / maxCheckInterval);

    // loops
    uint sz = data.size;
    double* y2 = new double[sz];
    bool isStopping = false;
    for (auto istep = 0; istep < nsteps - 1; ++istep) {
        double t1 = xold + dt / static_cast<double>(nsteps - 1) * istep;
        double t2 = t1 + dt / static_cast<double>(nsteps - 1);
        DOPRI5::cont(/*ode_solver, */data, t2, y2);

        for (std::vector<const SimulationEventHandler*>::iterator it = mEventHandlers.begin();
                it != mEventHandlers.end();
                ++it) {
            SimulationEventHandler* handler = const_cast<SimulationEventHandler*>(*it);
            if (handler != NULL) {
                isStopping |= findCrossing(nr, t1, t2, y2, handler, data);
            }
        }
    }

    delete[] y2;

    return isStopping;
}

// ---------------------------------------------------------------------
/** Get all detected events.
 * @return all detected events.
 */
std::vector<SimulationEvent> SimulationEventManager::getEvents() const {
    return events;
}
