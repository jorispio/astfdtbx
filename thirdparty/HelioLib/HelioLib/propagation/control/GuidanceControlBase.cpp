// $Id$
// ---------------------------------------------------------------------
/**
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "GuidanceControlBase.hpp"

// #include "utils/FDError.hpp"
/* --------------------------------------------------------------------- */
/**
 * Creator.
 * @param dynamic dynamical model
 * @param critere objective function type
 */
TGuidanceControlBase::TGuidanceControlBase(const TDynamicsBase* dynamic_)
    : dynamic(dynamic_), forced_coast(false) { }

/* --------------------------------------------------------------------- */
/** Check derivatives.
 * Some of these test cannot be in the Switching class because we need access
 * to the dynamics.
 *
 * @param stateRef  reference state vector
 * @param lambdaRef  reference costate vector
 *
 * @return the number of derivative errors.
 */
/*
int TGuidanceControlBase::CheckDerivatives(real_type t, const real_type* stateRef, const Vector7& lambdaRef)
{
    real_type derivative_test_tol = 1e-8;
    real_type h = 1e-8;
    int nerrors = 0;

    real_type x_dot[2 * STATE_SIZE];

    // prepare computations 
    real_type StateLagranges[2 * STATE_SIZE];
    memcpy(StateLagranges, stateRef, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagranges + STATE_SIZE, (real_type*)lambdaRef.data(), STATE_SIZE * sizeof(real_type));
    MATRIXVAR jacobian = MATRIXVAR::Zero(STATE_SIZE, STATE_SIZE);
    // reference values
    real_type state_costate_ref[2 * STATE_SIZE];
    real_type state_costate[2 * STATE_SIZE];
    memcpy(state_costate_ref, StateLagranges, 2 * STATE_SIZE * sizeof(real_type));
    memcpy(state_costate, StateLagranges, 2 * STATE_SIZE * sizeof(real_type));

    CommonVariables common;
    TDynamicsBase* dynmodel = const_cast<TDynamicsBase*>(dynamic);
    dynmodel->ComputeCommonVariables(t, StateLagranges, common);
    dynmodel->JacobianMatrixState(common, jacobian);

    // current reference for the switching function
    real_type SwitchingFunctionValueRef =
        GetSwitchingFunction(t, state_costate_ref, state_costate_ref + STATE_SIZE, common);

    // current reference for derivatives
    SwitchingFunctionDerivatives mSwitchingFunctionDerivatives = common.Sderivatives.SwitchingFunctionDerivs;
    GetSwitchingFunctionDerivatives(t, stateRef, lambdaRef, common, jacobian, mSwitchingFunctionDerivatives);

    // time derivatives
    dynmodel->GetDynamics(t, common, x_dot);
    GetSwitchingFunctionTimeDerivatives(t, stateRef, lambdaRef, common, x_dot, jacobian, mSwitchingFunctionDerivatives);

    real_type state[STATE_SIZE];
    memcpy(state, stateRef, STATE_SIZE * sizeof(double));

    std::vector<FDError> fderror;
    Vector7 diffRho_dstate;
    for (int irow = 0; irow < STATE_SIZE; ++irow) {
        // / State
        // the step is relative to the current value.
        real_type step = h * std::max(1., fabs(state_costate_ref[irow]));

        // update x_dot
        //((TDynamicsBase *)dynamic)->ComputeCommonVariables(0., state_costate, common);
        state_costate[irow] = state_costate_ref[irow] + step;
        CommonVariables common1;
        dynmodel->ComputeCommonVariables(t, state_costate, common1);
        real_type SwitchingFunctionValue2 = GetSwitchingFunction(t, state_costate, state_costate + STATE_SIZE, common1);

        state_costate[irow] = state_costate_ref[irow] - step;
        CommonVariables common2;
        dynmodel->ComputeCommonVariables(t, state_costate, common2);
        real_type SwitchingFunctionValue1 = GetSwitchingFunction(t, state_costate, state_costate + STATE_SIZE, common2);

        // reset
        state_costate[irow] = state_costate_ref[irow];

        diffRho_dstate(irow) = (SwitchingFunctionValue2 - SwitchingFunctionValue1) / (2 * step);

        real_type diffError =
            FDError::getRelativeError(mSwitchingFunctionDerivatives.SwitchFun_dstate(irow), diffRho_dstate(irow));

        if ((fabs(diffError) > derivative_test_tol) || std::isnan(diffError)) {
            fderror.push_back(FDError(
                irow, 0, mSwitchingFunctionDerivatives.SwitchFun_dstate(irow), diffRho_dstate(irow), diffError));
        }
    }

    // display results
    if (fderror.size() == 0) {
        printf("   All derivatives seem OK. (dRho/dState)   (SwitchingFunction)\n");
    } else {
        printf("dRho/dState  (SwitchingFunction)\n");
        FDError::printHeader();
        for (uint i = 0; i < fderror.size(); ++i) {
            fderror.at(i).print();
        }
        nerrors += fderror.size();
    }

    //
    fderror.clear();
    Vector7 diffRho_dlambda;
    for (int irow = 0; irow < STATE_SIZE; ++irow) {
        // / State
        // the step is relative to the current value.
        real_type step = h * std::max(1., fabs(state_costate_ref[irow + STATE_SIZE]));

        // update x_dot
        //((TDynamicsBase *)dynamic)->ComputeCommonVariables(0., state_costate, common);
        state_costate[irow + STATE_SIZE] = state_costate_ref[irow + STATE_SIZE] + step;
        CommonVariables common1;
        dynmodel->ComputeCommonVariables(t, state_costate, common1);
        real_type SwitchingFunctionValue2 = GetSwitchingFunction(t, state_costate, state_costate + STATE_SIZE, common1);

        state_costate[irow + STATE_SIZE] = state_costate_ref[irow + STATE_SIZE] - step;
        CommonVariables common2;
        dynmodel->ComputeCommonVariables(t, state_costate, common2);
        real_type SwitchingFunctionValue1 = GetSwitchingFunction(t, state_costate, state_costate + STATE_SIZE, common2);

        // reset
        state_costate[irow + STATE_SIZE] = state_costate_ref[irow + STATE_SIZE];

        diffRho_dlambda(irow) = (SwitchingFunctionValue2 - SwitchingFunctionValue1) / (2 * step);

        real_type diffError =
            FDError::getRelativeError(mSwitchingFunctionDerivatives.SwitchFun_dlambda(irow), diffRho_dlambda(irow));

        if ((fabs(diffError) > derivative_test_tol) || std::isnan(diffError)) {
            fderror.push_back(FDError(
                irow, 0, mSwitchingFunctionDerivatives.SwitchFun_dlambda(irow), diffRho_dlambda(irow), diffError));
        }
    }

    // display results
    if (fderror.size() == 0) {
        printf("   All derivatives seem OK. (dRho/dCostate)   (SwitchingFunction)\n");
    } else {
        printf("dRho/dCostate  (SwitchingFunction)\n");
        FDError::printHeader();
        for (uint i = 0; i < fderror.size(); ++i) {
            fderror.at(i).print();
        }
        nerrors += fderror.size();
    }

    // check SwitchFun_dt
    // diff2S_dm_dt
    // to differentiate wrt time we need to propagate with dt=h
    h = 1e-10;
    for (int irow = 0; irow < 2 * STATE_SIZE; ++irow) {
        state_costate[irow] = state_costate_ref[irow] + h * x_dot[irow];
    }
    CommonVariables commont;
    dynmodel->ComputeCommonVariables(t, state_costate, commont);
    real_type SwitchingFunctionValue2 = GetSwitchingFunction(t, state_costate, state_costate + STATE_SIZE, commont);

    // finite difference
    real_type dRhodt_fd = (SwitchingFunctionValue2 - SwitchingFunctionValueRef) / h;
    real_type diffError = FDError::getRelativeError(mSwitchingFunctionDerivatives.SwitchFun_dt, dRhodt_fd);
    if (diffError > derivative_test_tol) {
        printf("Check dRho/dt   \n");
        FDError::print(mSwitchingFunctionDerivatives.SwitchFun_dt, dRhodt_fd, diffError);
        ++nerrors;
    } else {
        printf("   All derivatives seem OK. (dRho/dt)   (SwitchingFunction)\n");
    }

    // check SwitchFun_dt_dm
    // diff2S_dm_dt
    // to differentiate wrt time we need to propagate with dt=h
    h = 1e-6;
    memcpy(state_costate, state_costate_ref, 2 * STATE_SIZE * sizeof(real_type));
    state_costate[6] -= h;  // varied the mass
    CommonVariables commonVarm;
    dynmodel->ComputeCommonVariables(t, state_costate, commonVarm);
    /*
    SwitchingFunctionDerivatives mSwitchingFunctionDerivatives2;
    GetSwitchingFunctionDerivatives(
        t, commonVarm.state.data(), commonVarm.lambda, commonVarm, jacobian, mSwitchingFunctionDerivatives2);
    GetSwitchingFunctionTimeDerivatives(
        t, commonVarm.state.data(), commonVarm.lambda, commonVarm, x_dot, jacobian, mSwitchingFunctionDerivatives2);
        
    // finite difference
    real_type dRhodmdt_fd =
        (mSwitchingFunctionDerivatives.SwitchFun_dt - mSwitchingFunctionDerivatives2.SwitchFun_dt) / h;

    diffError = FDError::getRelativeError(mSwitchingFunctionDerivatives.SwitchFun_dm_dt, dRhodmdt_fd);
    if (diffError > derivative_test_tol) {
        printf("Check dRho/dmdt   \n");
        FDError::print(mSwitchingFunctionDerivatives.SwitchFun_dm_dt, dRhodmdt_fd, diffError);
        ++nerrors;
    } else {
        printf("   All derivatives seem OK. (dRho/dmdt)   (SwitchingFunction)\n");
    }
*/
/*
    return nerrors;
}
*/
