// $Id$
/* ------------------------------------------------------------------------------- */
#ifndef __SMTBX_PROPAGATION_CONTROL_ENUM_HPP_
#define __SMTBX_PROPAGATION_CONTROL_ENUM_HPP_

enum CONTROL_DIRECTION { CUSTOM, RADIAL, TANGENTIAL };

#endif  // __SMTBX_PROPAGATION_CONTROL_ENUM_HPP_
