// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of LT-IPOPT.
 *
 *   Copyright (C) 2010 Joris Olympio
 *
 *   LT-IPOPT is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   LT-IPOPT is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with LT-IPOPT.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_GUIDANCE_CONTROL_HPP
#define __SMTBX_GUIDANCE_CONTROL_HPP

#include "core/HelioDataTypes.hpp"
#include "propagation/dynamics/DynamicsBase.hpp"
#include "propagation/dynamics/DynamicsCommon.hpp"

class TDynamicsBase;

/* ------------------------------------------------------------------------------- */
class TGuidanceControlBase
{
 protected:
    /** Dynamics. */
    const TDynamicsBase* dynamic;

    /** coast flag. */
    bool forced_coast;

 public:
    /** Creator. */
    TGuidanceControlBase(const TDynamicsBase* dynamic);

    virtual ~TGuidanceControlBase() { }

    /** Compute and return the optimal control
 * @param state state vector
 * @param lambda costate vector
     * */
    virtual Vector3 GetControlDirection(const real_type* state, const real_type* lambda) = 0;

    /**Derivative of the optimal control direction with respect to state.
     */
    virtual void ControlDerivativeState(const CommonVariables& cV, Matrix3x7& QSW_State) = 0;

    /** Derivative of the optimal control direction with respect to costate.
     */
    virtual void ControlDerivativeLambda(const CommonVariables& cV, Matrix3x7& QSW_Lambda) = 0;

    virtual Vector3 GetControlDirectionTimeDeriv(const real_type* x, const real_type* lambda, const real_type* xdot) = 0;

    /** Get the switching function value.
     * @param state state vector
     * @param lambda costate vector
     *
     * @return the switching function value.
     * */
    virtual real_type GetSwitchingFunction(real_type t,
                                           const real_type* state,
                                           const real_type* lambda,
                                           const CommonVariables& common) = 0;

    /** Get the switching function derivatives.
     *
     * @param t time
     * @param state state vector
     * @param lambda costate vector
     * @param x_dot time derivative of state/costate
     * @param jacobian jacobian of the state dynamic vector
     * @param sd switching function derivatives
     * */
    /*virtual void GetSwitchingFunctionDerivatives(real_type t,
                                                 const real_type* state,
                                                 const Vector7& lambda,
                                                 const CommonVariables& common,
                                                 const Matrix7& jacobian,
                                                 SwitchingFunctionDerivatives& sd) = 0;
*/
    /** Get the switching function derivatives.
     *
     * @param t time
     * @param state state vector
     * @param lambda costate vector
     * @param x_dot time derivative of state/costate
     * @param jacobian jacobian of the state dynamic vector
     * @param sd switching function derivatives
     * */
/*    virtual void GetSwitchingFunctionTimeDerivatives(real_type t,
                                                 const real_type* state,
                                                 const Vector7& lambda,
                                                 const CommonVariables& common,
                                                 const real_type* x_dot,
                                                 const Matrix7& jacobian,
                                                 SwitchingFunctionDerivatives& sd) = 0;
*/
    /** Check derivatives of the switching function. */
//    int CheckDerivatives(real_type t, const real_type* stateRef, const Vector7& lambdaRef);
};

/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_GUIDANCE_CONTROL_HPP
