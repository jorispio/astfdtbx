// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2015 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "NullGuidance.hpp"
#include "HelioLib.hpp"

#define MIN_MAX2 -1. // to change sign of switching function TO REMOVE

// ---------------------------------------------------------------------
/** Creator.
 */
// ---------------------------------------------------------------------
NullGuidance::NullGuidance(TDynamicsBase* dynamic)
    : TGuidanceControlBase(dynamic)
{
}

// ---------------------------------------------------------------------
/** Compute and return the optimal control direction.
 */
// ---------------------------------------------------------------------
Vector3 NullGuidance::GetControlDirection(const real_type* x, const real_type* lambda)
{
    Vector3 U;
    U << 0, 0, 0;
    return U;
}
// ---------------------------------------------------------------------
/** Derivative of the control vector, with respect to time
 * @param x      state vector
 * @param lambda costate vector
 * @param x_dot  time derivative
 * @return control time derivative
 */
// ---------------------------------------------------------------------
Vector3 NullGuidance::GetControlDirectionTimeDeriv(const real_type* x,
                                                                     const real_type* lambda,
                                                                     const real_type* xdot)
{
    Vector3 Udot;
    Udot.setZero();
    return Udot;
}

// ---------------------------------------------------------------------
/** Derivative of optimal control direction with respect to state.
 */
// ---------------------------------------------------------------------
void NullGuidance::ControlDerivativeState(const CommonVariables& cV, Matrix3x7& dU_State)
{
    dU_State.setZero();
}

// ---------------------------------------------------------------------
/** Derivative of the optimal control direction with respect to costate.
 *
 */
// ---------------------------------------------------------------------
void NullGuidance::ControlDerivativeLambda(const CommonVariables& cV, Matrix3x7& dU_Lambda)
{
    dU_Lambda.setZero();
}
// ---------------------------------------------------------------------
/** Get the switching function value.
 *
 * @param  x      state vector
 * @param lambda costate vector
 * @param common
 */
// ---------------------------------------------------------------------
real_type NullGuidance::GetSwitchingFunction(real_type t,
                                                        const real_type* state,
                                                        const real_type* lambda,
                                                        const CommonVariables& common)
{
    return 0;
}

// ---------------------------------------------------------------------
/** Derivative of the Smoothing function, with respect to
 * mass, lm, lV and time.
 * On purpose, to ease debugging and reading of the code, we do not perform
 * factorisation/simplification of the equations. The overcost should be marginal.
 */
// ---------------------------------------------------------------------
void NullGuidance::GetSwitchingFunctionDerivatives(real_type t,
                                                              const real_type* state,
                                                              const Vector7& lambda,
                                                              const CommonVariables& common,
                                                              const Matrix7& jacobian,
                                                              SwitchingFunctionDerivatives& sd)
{
    sd.SwitchFun_dt = 0;
    sd.SwitchFun_dstate.setZero();
    sd.SwitchFun_dm = sd.SwitchFun_dstate(6);
	//
	sd.SwitchFun_dlambda.setZero();
	sd.SwitchFun_dlV.setZero();
	sd.SwitchFun_dlm = 0;
	// sd.SwitchFun_dstate_dlambda;
	//
	sd.SwitchFun_dm2 = 0;
	sd.SwitchFun_dm_dlV.setZero();
    sd.SwitchFun_dm_dlm = 0.;
    sd.SwitchFun_dm_dt = 0;
}

// ---------------------------------------------------------------------
/** Time Derivative of the Smoothing function.
 * By construction, we need the dynamics to compute those derivatives. Therefore,
 * it is not possible to compute them at the same time as other derivatives since
 * the dynamics (in particular costate) requires some derivatives of the switching function.
 * 
 * On purpose, to ease debugging and reading of the code, we do not perform
 * factorisation/simplification of the equations. The overcost should be marginal.
 * 
 * @param t current time
 * @param state state vector
 * @param lambda costate vector
 * @param x_dot time derivative of state/costate
 * @param jacobian jacobian of the state dynamic vector
 * @param sd switching function derivatives
 */
// ---------------------------------------------------------------------
void NullGuidance::GetSwitchingFunctionTimeDerivatives(real_type t,
                                                              const real_type* state,
                                                              const Vector7& lambda,
                                                              const CommonVariables& common,
                                                              const real_type* x_dot,
                                                              const Matrix7& jacobian,
                                                              SwitchingFunctionDerivatives& sd)
{
	// time derivative of the switching function
    sd.SwitchFun_dt = 0;
    sd.SwitchFun_dm_dt = 0;
}
