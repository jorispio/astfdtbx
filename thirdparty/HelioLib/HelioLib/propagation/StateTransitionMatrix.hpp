// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_STATE_TRANSITION_MATRIX_HPP_
#define __SMTBX_STATE_TRANSITION_MATRIX_HPP_

#include "maths/coordinates/CartesianCoordinates.hpp"
#include "physics/forces/Force.hpp"
#include "propagation/SimulationEventManager.hpp"
#include "maths/MatrixArray.hpp"

/* ------------------------------------------------------------------------------- */
// also defined in TrajectoryPropage.hpp
#define ODESOLVER DOPRI5
//#define ODESOLVER        DOP853

#include "maths/integration/DormandPrince5.hpp"

/* ------------------------------------------------------------------------------- */
// also defined in TrajectoryPropage.hpp
#define X_SIZE 7
#define Xt_SIZE 7  // ..., time
#define Xmt_SIZE 8 // ..., mass, time

#define STMt_SIZE (Xt_SIZE * Xt_SIZE)
#define X_STMt_SIZE (Xt_SIZE + STMt_SIZE)

#define STM_ODE_RTOL 1e-13
#define STM_ODE_ATOL 1e-13

/* ------------------------------------------------------------------------------- */
template<int NX = X_SIZE>
class StateTransitionMatrix : public ODESOLVER {
 protected:
    /* physical parameters */
    double constMu;
    double constJ2;
    double constJ0;
    double constRe;

    double tof;
    double rtoler, atoler;
    uint nDimension;

    double DOPRI5OUT[NX + NX * NX];

    SimulationEventManager* mEventsHandler;

    /** dynamical forces */
    std::vector<Force*> forces;

    /* */
    virtual void DiffEqn(real_type x, const real_type* y, real_type* f) = 0;

    /* */
    int AtStep(long nr, real_type xold, real_type x, const real_type* y) {
        memcpy(DOPRI5OUT, y, (NX + NX * NX) * sizeof(double));

        bool isStoppingEvent = false;
        if (mEventsHandler != NULL) {
            OdePolyDataVar odedata = ODESOLVER::getContinuousPolyData();
            isStoppingEvent |= mEventsHandler->AtStep(nr, xold, x, y, odedata);

            if (isStoppingEvent) {
                double *ye = new double[nDimension];
                // TODO(joris) find the earliest stopping event
                double te = mEventsHandler->getEvents().at(0).date;
                ODESOLVER::cont(odedata, te, ye);
                memcpy(DOPRI5OUT, ye, nDimension * sizeof(double));

                delete[] ye;
            }
        }

        return (isStoppingEvent) ? -1 : 0;        
    }

    /* */
    void dynamic(unsigned n, double t, double* X, double* Xdot);

    /* */
    void solout(long nr, double xold, double x, double* y, unsigned n, int* irtrn);

    /* */
    void STMdispatch(const double STM[NX * NX], Matrix<real_type, NX, NX>& STMout) {
        int sz_stm[] = { NX, NX };
        int lin, col;
        for (lin = 0; lin < NX; ++lin) {
            for (col = 0; col < NX; ++col) {
                STMout(lin, col) = MATRIX(STM, lin, col, sz_stm);
            }
        }
    }    
    

    /* */
    // ---------------------------------------------------------------------
    /** Input must be 6 dim. We fill then the remaining dim.
     * Return 6x6 transition matrix STM(t0, ti)
     * contruct a multi impulse trajectory, with patched conic approximation.
     * Each legs are computed individually and then patched together.
     */
    int getSTM(const double X0[NX],
        const Matrix<real_type, NX, NX>& STM0,
        double t0,
        double& ti,
        /* OUTPUTS */
        Matrix<real_type, NX, NX>& STMf,
        double Xf[NX]) {
        
        double STATE0[NX + NX * NX];
        double Xf_[NX], STMf_[NX * NX];
        double tSTM0[NX * NX];

        memcpy(STATE0, X0, NX * sizeof(double));

        /* initial extended state transition matrix */
        for (int i = 0; i < NX; ++i) {
            for (int j = 0; j < NX; ++j) {
                tSTM0[j + NX * i] = STM0(i, j);
            }
        }
        
        memcpy(STATE0 + NX, tSTM0, NX * NX * sizeof(double));

        // ODESOLVER::Initialise(ODESOLVER::call_for_output_only);
        ODESOLVER::Initialise(OdeStepperMode::call_for_dense_output);

        if (mEventsHandler != NULL) {
            mEventsHandler->clearEvents();
        }

        /* we are only interested by the final result, i.e. no dense output */
        int ret = 1;
        if (fabs(t0 - ti) > 1e-8) {
            real_type tspan[2] = { t0, ti };
            tof = (ti - t0);

            ret = solve(tspan[0], STATE0, tspan[1]);

            if (ret < 1) {
                printf("StateTransitionMatrix::getSTM res = %d (problem during integration on [%f, %f]=[%g] at %g)\n",
                    ret,
                    t0,
                    ti,
                    ti - t0, tspan[0]);
                printf("StateTransitionMatrix::getSTM t = [%f, %f] \n", tspan[0], tspan[1]);
                printf("StateTransitionMatrix::getSTM x = [");
                for (int idx=0; idx <  NX + NX * NX; ++idx) {
                    printf("%7.4f ", STATE0[idx]);
                    }
                printf("] \n");

                char msg[1024];
                snprintf(msg, 1024, "Failed integrating: res = %d (problem during integration on [%f, %f] at %g. x = [%7.4f, %7.4f, %7.4f, %7.4f, %7.4f, %7.4f]\n", 
                    ret, t0, ti, tspan[1],
                    STATE0[0], STATE0[1], STATE0[2], STATE0[3], STATE0[4], STATE0[5]);
                throw HelioLibException(msg);
            }

            ti = tspan[0];
        }
        else {
            memcpy(DOPRI5OUT, STATE0, (NX + NX * NX) * sizeof(double));
        }

        /* copy dopri output */
        memcpy(Xf_, DOPRI5OUT, NX * sizeof(double));
        memcpy(STMf_, DOPRI5OUT + NX, (NX * NX) * sizeof(double));

        /* The result of solve() is PHI(tf, t0) when integrating from t0 to tf.
             *
             * use simplectic property for inversion for a better precision
         * STM = [STM1, STM2; STM3, STM4]
         * invSTM = [STM4, -STM2; -STM3, STM1]
             * inv(STM(t, t0)) = STM(t0, t)
         *        STM1 -> STM4'
             *        STM2 -> -STM2'
             *        STM3 -> -STM3'
             *        STM4 -> STM1'
             */
             /* CAREFUL: C and MATLAB array conventions */
        STMdispatch(STMf_, STMf);

        if (Xf) {
            memcpy(Xf, Xf_, NX * sizeof(double));
        }

        return ret;
    }


 public:
    /** Constructor. */
    StateTransitionMatrix(double mu_, double J2_ = 0, double rE_ = 0) : ODESOLVER(NX + NX * NX), nDimension(NX + NX * NX) {
        constMu = mu_;
        constJ2 = J2_;
        constRe = rE_;
        constJ0 = 3. / 2. * constMu * constJ2 * pow(constRe, 2);

        mEventsHandler = NULL;

        setmaxdense(0);
        rtoler = STM_ODE_RTOL;
        atoler = STM_ODE_ATOL;
        settol(atoler, rtoler);
    }

    /** Destructor. */
    ~StateTransitionMatrix(void) { }

    /* */
    void setConstants(double mu_, double J2_ = 0, double rE_ = 0) {
        constMu = mu_;
        constJ2 = J2_;
        constRe = rE_;
        constJ0 = 3. / 2. * constMu * constJ2 * pow(constRe, 2);
    }    

    double getConstantMu() {
        return constMu;
    }
    double getCOnstantJ2() {
        return constJ2;
    }
    double getConstantRe() {
        return constRe;
    }

    /** Add event handler. */
    void setEventHandler(SimulationEventManager* handler) {
        mEventsHandler = handler;
        if (handler != NULL) {
            setmaxdense(nDimension);
        }
    }    
    
    void clearEventHandler() {
        mEventsHandler = NULL;
    }

    /** Add a force to the dynamical model */
    void addForce(Force *force) {
        forces.push_back(force);
    }    

    /** remove dynamical forces */
    void clearForces(void) {
        forces.clear();
    }    

    /** */
    std::vector<Force*> getForces() const {
        return forces;
    }    

    /* */
    bool setMode(int ) {  // for compatibility with STM()
        return true;
    }
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_STATE_TRANSITION_MATRIX_HPP_
