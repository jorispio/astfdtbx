// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012-2013 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __SIMULATION_EVENT_HANDLER_HPP
#define __SIMULATION_EVENT_HANDLER_HPP

#include "core/HelioDataTypes.hpp"

/** Event action flag. Defines what happens when an specified event occurs.
 */
enum EventAction { EventActionContinue, EventActionStop };

/**
 */
struct SimulationEvent {
    /** date of the event. */
    double date;
    /** 2-character ID of the event */
    std::string type;
    /** description of the event. */
    std::string text;

    /** Defautl constructor. */
    SimulationEvent() : date  (0), type  ("--"),  text  ("") { }

    SimulationEvent(double t, std::string stype, std::string sdesc) :
        date  (t), type  (stype), text  (sdesc) { }
};

/** @class SimulationEventHandler
 * Generic handler for event handler.
 */
class SimulationEventHandler {
 protected:
    /** */
    std::string name;
    /** */
    std::string type;
    /** */
    int bDirection;
    /** */
    EventAction actionOnEvent;

 public:
    SimulationEventHandler(const char* st="", const char* sd="");
    virtual ~SimulationEventHandler() { }

    /** */
    virtual std::string getName() {
        return name;
    }

    /** */
    virtual std::string getTypeId() {
        return type;
    }

    /** Set the direction of the event. */
    void setDirection(int sens);
    
    /** Get the direction of the event. */
    int getDirection() { return bDirection; }
        
    /** Indicate whether the event stops the propagation */
    void setActionOnEvent(EventAction stop);
    
    /** */
    EventAction getAction();

    virtual real_type switchingFunction(real_type t, real_type* x) = 0;
};
// ---------------------------------------------------------------------
#endif  // __SIMULATION_EVENT_HANDLER_HPP
