// $Id: DynamicssEllipticThreeBodyProblem.cpp 30 2013-06-22 10:21:41Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <math.h>  /* fabs */
#include <float.h> /* DBL_EPSILON */
#include <string.h>

#include <cstdio>

#include "DynamicsEllipticThreeBodyProblem.hpp"
#ifdef WITH_GUIDANCE
    #include "propagation/control/NullGuidance.hpp"
#endif

using HelioLib::MJD_2000;

// ---------------------------------------------------------------------
/**
 * Constructor
 */
EllipticThreeBodyProblem::EllipticThreeBodyProblem(real_type mu1_, real_type mu2_, real_type e_,
                                                    const ThrusterData &ThrusterDef)
    : TDynamicsBase(D_ERTBP, ThrusterDef, mu2_ / (mu1_ + mu2_)),
      mu1(mu1_ / (mu1_ + mu2_)), mu2(mu2_ / (mu1_ + mu2_)), ecc(e_) {
#ifdef WITH_GUIDANCE
    optimalControl = new NullGuidance(this);
#endif
    body1_pos << -constMu, 0, 0;
    body2_pos << 1 - constMu, 0, 0;
}

/* ---------------------------------------------------------------------
 * Constructor
 * --------------------------------------------------------------------- */
EllipticThreeBodyProblem::EllipticThreeBodyProblem(real_type mu1_, real_type mu2_, const ThrusterData &ThrusterDef)
    : TDynamicsBase(D_ERTBP, ThrusterDef, mu1_ / (mu1_ + mu2_)),
      mu1(mu1_ / (mu1_ + mu2_)), mu2(mu2_ / (mu1_ + mu2_)), ecc(0) {
#ifdef WITH_GUIDANCE
    optimalControl = new NullGuidance(this);
#endif
    body1_pos << -constMu, 0, 0;
    body2_pos << 1 - constMu, 0, 0;
}

/* ---------------------------------------------------------------------
 * Constructor
 * --------------------------------------------------------------------- */
EllipticThreeBodyProblem::EllipticThreeBodyProblem(real_type mu_ratio, const ThrusterData &ThrusterDef)
    : TDynamicsBase(D_ERTBP, ThrusterDef, mu_ratio), mu1(1 - mu_ratio), mu2(mu_ratio), ecc(0) {
#ifdef WITH_GUIDANCE
    optimalControl = new NullGuidance(this);
#endif
    body1_pos << -constMu, 0, 0;
    body2_pos << 1 - constMu, 0, 0;
}
// ---------------------------------------------------------------------
EllipticThreeBodyProblem::~EllipticThreeBodyProblem() {
#ifdef WITH_GUIDANCE
    if (optimalControl) {
        delete optimalControl;
    }
#endif
}

/* --------------------------------------------------------------------- *
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::Init(const real_type* S0_, const ThrusterData& thrusterDefinition,
                                    double,
                                    real_type) {
    memcpy(S0, S0_, 2 * STATE_SIZE * sizeof(real_type));
#ifdef WITH_SYSTEM
    if (mThruster) {
        mThruster->SetThruster(thrusterDefinition);
    }
#endif
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::SetInitialConditions(const double* S0_) {
    //
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
bool EllipticThreeBodyProblem::ComputeCommonVariablesForStateOnly(real_type t, const real_type* StateLagranges,
                                                                    CommonVariables &common) {
    for (int i = 0; i < STATE_SIZE; ++i) {
        common.state(i) = StateLagranges[i];
    }

    common.date = GenericDate(t * a_s / 86400., HelioLib::MJD_2000);

    // position
    common.R(0) = StateLagranges[0];
    common.R(1) = StateLagranges[1];
    common.R(2) = StateLagranges[2];
    // speed
    common.V(0) = StateLagranges[3];
    common.V(1) = StateLagranges[4];
    common.V(2) = StateLagranges[5];
    // mass
    common.mass = StateLagranges[6];

    return true;
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
bool EllipticThreeBodyProblem::ComputeCommonVariablesForOptimalControl(real_type t, const real_type* StateLagranges,
                                                                        CommonVariables &common) {
    /* costates */
    for (int i = 0; i < STATE_SIZE; ++i) {
        common.lambda(i) = StateLagranges[STATE_SIZE + i];
    }

#ifdef WITH_SYSTEM
    common.Thrust = 0;
    common.qfuel = 0;
    common.g0Isp = 0;

    if (mThruster) {
        //
    }
#endif

    common.U.setZero();
#ifdef WITH_GUIDANCE
    if (optimalControl) {
        common.U = optimalControl->GetControlDirection(common.state.data(), common.lambda.data());
    }
#endif
    return true;
}
/* ---------------------------------------------------------------------
 * true anomaly f(t), the angle that the line segment joining the rightmost focus of
 * m2’s elliptical orbit to m2’s position at periapsis makes with the line segment
 * joining that focus to m2’s position at time t.
 *
 * Normalizing units so that the pair of primary masses has unit angular momentum and the
 * distance between the two primaries at f=pi/2 is unity.
 *
 * e is the eccentricity of m2’s elliptical orbit
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::getPrimaryPositions(const real_type f, real_type *xy1, real_type *xy2) {
    if (xy1 != NULL) {
        double den = 1 + ecc * cos(f);
        xy1[0] = -constMu * cos(f) / den;
        xy1[1] = -constMu * sin(f) / den;
    }

    if (xy2 != NULL) {
        double den = 1 + ecc * cos(f);
        xy2[0] = (1 - constMu) * cos(f) / den;
        xy2[1] = (1 - constMu) * sin(f) / den;
    }
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::GetStateDynamics(real_type t,
                                  const CommonVariables& common,
                     /* OUTPUT */ real_type* x_dot) {
    double vx = common.V(0), vy = common.V(1), vz = common.V(2);
    Vector3 dUdx = EllipticThreeBodyProblem::compute_gravity(constMu, common.R);

    x_dot[0] = vx;
    x_dot[1] = vy;
    x_dot[2] = vz;
    x_dot[3] = 2 * vy - dUdx(0);
    x_dot[4] =-2 * vx - dUdx(1);
    x_dot[5] =-dUdx(2);
    x_dot[6] = 1;  // time
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::GetCostateDynamics(real_type t,
                                    const CommonVariables& cV,
                       /* OUTPUT */ real_type* x_dot) {
    throw HelioLibException("JacobianMatrixCoState()", "Not implemented", "Not implemented");
}

#ifdef WITH_GUIDANCE
/* ---------------------------------------------------------------------
 * Get the optimal control class.
 * --------------------------------------------------------------------- */
TGuidanceControlBase* EllipticThreeBodyProblem::GetControlInstance() {
    return optimalControl;
}
#endif
/* ---------------------------------------------------------------------
 * Compute time derivative of state dynamics.
 *
 * @see GetStateDynamics()
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::DynamicsTime(const real_type* x, const real_type* x_dot, Vector7& dFdT) {
    double rx = x[0], ry = x[1], rz = x[2];
    Vector3 R; R << rx, ry, rz;
    Vector3 V; V << x[3], x[4], x[5];
    Vector3 R1 = R - body1_pos;
    Vector3 R2 = R - body2_pos;
    
    // 1st order partial derivatives of the potential U(x,y, z)
    //Vector3 dUdt = compute_time_derivative_gravity(constMu, R, V);
    Vector3 dUdx = compute_gravity(constMu, R);
    
    dFdT(0) = x_dot[3];
    dFdT(1) = x_dot[4];
    dFdT(2) = x_dot[5];
    dFdT(3) = 2 * x_dot[4] - dUdx(0);
    dFdT(4) =-2 * x_dot[3] - dUdx(1);
    dFdT(5) =-dUdx(2);
    dFdT(6) = 0;
}

// ---------------------------------------------------------------------
/**
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::JacobianMatrixState(const CommonVariables& common, MATRIXVAR& dFdX, int row0, int col0) {
    real_type t = 0;
    real_type x_dot[STATE_SIZE];
    GetStateDynamics(t, common, x_dot);

    real_type x[STATE_SIZE];
    for (int i = 0; i < STATE_SIZE; ++i) {
        x[i] = common.state(i);
    }
    GetJacobianDynamics(t, x, common, x_dot, dFdX);
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::JacobianMatrixCoState(const CommonVariables& commonVariables, MATRIXVAR& dFdL_,
                                                        int row0, int col0) {
    throw HelioLibException("JacobianMatrixCoState()", "Not implemented", "Not implemented");
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::GetJacobianDynamics(real_type t,
                                     const real_type* x,
                                     const CommonVariables& cv,
                                     const real_type* x_dot,
                                     MATRIXVAR& dFdX) {
    Vector3 R; R << x[0], x[1], x[2];
    Matrix3 dUdR = EllipticThreeBodyProblem::compute_gravity_gradient(constMu, R);    

    dFdX(0, 3) = 1;
    dFdX(1, 4) = 1;
    dFdX(2, 5) = 1;
    dFdX(3, 0) = -dUdR(0,0);
    dFdX(3, 1) = -dUdR(0,1);
    dFdX(3, 2) = -dUdR(0,2);
    dFdX(3, 4) = 2;
    dFdX(4, 0) = -dUdR(1,0);
    dFdX(4, 1) = -dUdR(1,1);
    dFdX(4, 2) = -dUdR(1,2);
    dFdX(4, 3) =-2;
    dFdX(5, 0) = -dUdR(2,0);
    dFdX(5, 1) = -dUdR(2,1);
    dFdX(5, 2) = -dUdR(2,2);
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblem::DynamicsTimeCoast(const real_type* x,
                                   real_type* x_dot,
                                   Vector7& reference_state,
                                   Vector7& modified_state,
                                   Vector7& dFdT) {
    double rx = x[0], ry = x[1], rz = x[2];
    double vx = x[3], vy = x[4];

    Vector3 R; R << rx, ry, rz;
    Vector3 R1; R1 << rx - body1_pos(0), ry - body1_pos(1), rz - body1_pos(2);
    Vector3 R2; R2 << rx - body2_pos(0), ry - body2_pos(1), rz - body2_pos(2);

    double r1 = R2.norm();
    double r13 = r1 * r1 * r1;
    double r2 = R2.norm();
    double r23 = r2 * r2 * r2;

    // 1st order partial derivatives of the potential U(x,y, z)
    Vector3 U = compute_gravity(constMu, R);

    dFdT(0) = x[3];
    dFdT(1) = x[4];
    dFdT(2) = x[4];
    dFdT(3) = 2*vy - U(0);
    dFdT(4) =-2*vx - U(1);
    dFdT(5) =-U(2);
    dFdT(6) = 0;
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
real_type EllipticThreeBodyProblem::GetThrustAmplitude(real_type t, const real_type* state, const real_type* lambda) {
    return 0.;
}

/* ---------------------------------------------------------------------
 * Gravitational potential, in the rotating frame.
 * For the augmented potentiel, add 0.5 (x * x + y * y).
 * --------------------------------------------------------------------- */
real_type EllipticThreeBodyProblem::getPotential(double mu_ratio, const real_type *xyz) {
    double x = xyz[0];
    double y = xyz[1];

    double r1 = sqrt((x + mu_ratio) * (x + mu_ratio) + y * y);
    double r2 = sqrt((x + mu_ratio - 1) * (x + mu_ratio - 1) + y * y);
    double U =-(1 - mu_ratio) / r1 - mu_ratio / r2 - 0.5 * mu_ratio * (1 - mu_ratio);
    return -0.5 * (x * x + y * y) + U;
}
/* ---------------------------------------------------------------------
 * First partial derivatives of the potential U;
 * --------------------------------------------------------------------- */
Vector3 EllipticThreeBodyProblem::compute_gravity(double mu_ratio, const Vector3& R) { 
    Vector3 body1_pos, body2_pos;
    body1_pos << -mu_ratio, 0, 0;
    body2_pos << 1 - mu_ratio, 0, 0;    
    double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
    Vector3 R1 =  R - body1_pos;
    Vector3 R2 =  R - body2_pos;
    double rx = R(0), ry = R(1);
        
    double r1 = R1.norm();
    double r13 = r1 * r1 * r1;
    double r2 = R2.norm();
    double r23 = r2 * r2 * r2;
    return (mu1 * (R1 / r13) + mu2 * (R2 / r23)) - Vector3(rx, ry, 0);
}
/* ---------------------------------------------------------------------
 * Time derivative of first partial derivatives of the potential U;
 * --------------------------------------------------------------------- */
Vector3 EllipticThreeBodyProblem::compute_time_derivative_gravity(double mu_ratio, const Vector3& R, const Vector3& V) {
    Vector3 body1_pos, body2_pos;
    body1_pos << -mu_ratio, 0, 0;
    body2_pos << 1 - mu_ratio, 0, 0;    
    double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
    Vector3 R1 =  R - body1_pos;
    Vector3 R2 =  R - body2_pos;
    double rx = R(0), ry = R(1);
    double vx = V(0), vy = V(1);    
    double r1 = R1.norm();
    double r13 = r1 * r1 * r1;
    double r15 = r13 * r1 * r1;    
    double r2 = R2.norm();
    double r23 = r2 * r2 * r2;
    double r25 = r23 * r2 * r2;
    return mu1 * (V / r13 - 3 * R1 * R1.dot(V) / r15) + mu2 * (V / r23 - 3 * R2 * R2.dot(V) / r25) - Vector3(vx, vy, 0);
}    
/* ---------------------------------------------------------------------
 * Second partial derivatives of the potential U;
 * First derivatives of gradient compute_gravity()
 * --------------------------------------------------------------------- */
Matrix3 EllipticThreeBodyProblem::compute_gravity_gradient(double mu_ratio, const Vector3& R) { 
    Vector3 body1_pos, body2_pos;
    body1_pos << -mu_ratio, 0, 0;
    body2_pos << 1 - mu_ratio, 0, 0;    
    double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
    
    double r1x = R(0) - body1_pos(0);
    double r1y = R(1) - body1_pos(1);
    double r1z = R(2) - body1_pos(2);        
    double r1 = (R - body1_pos).norm();
    double r12 = r1 * r1;
    double r13 = r12 * r1;
    double r15 = r13 * r12;
    
    double r2x = R(0) - body2_pos(0);
    double r2y = R(1) - body2_pos(1);
    double r2z = R(2) - body2_pos(2);      
    double r2 = (R - body2_pos).norm();
    double r22 = r2 * r2;
    double r23 = r22 * r2;
    double r25 = r23 * r22;
    
    // Derivative of dU
    Matrix3 ddUdR;
    ddUdR(0, 0) /*G[0]*/ /*G(1,1)*/ =-1 + mu1 * (r12 - 3 * r1x * r1x) / r15 + mu2 * (r22 - 3 * r2x * r2x) / r25;
    ddUdR(0, 1) /*G[1]*/ /*G(1,2)*/ =    -3 * (mu1 * r1x * r1y / r15 + mu2 * r2x * r2y / r25); // FIXME
    ddUdR(0, 2) /*G[2]*/ /*G(1,3)*/ =    -3 * (mu1 * r1x * r1z / r15 + mu2 * r2x * r2z / r25); // FIXME
    ddUdR(1, 0) /*G[3]*/ /*G(2,1)*/ = ddUdR(0, 1);
    ddUdR(1, 1) /*G[4]*/ /*G(2,2)*/ =-1 + mu1 * (r12 - 3 * r1y * r1y) / r15 + mu2 * (r22 - 3 * r2y * r2y) / r25;
    ddUdR(1, 2) /*G[5]*/ /*G(2,3)*/ =    -3 * (mu1 * r1y * r1z / r15 + mu2 * r2y * r2z / r25);
    ddUdR(2, 0) /*G[6]*/ /*G(3,1)*/ = ddUdR(0, 2);
    ddUdR(2, 1) /*G[7]*/ /*G(3,2)*/ = ddUdR(1, 2);
    ddUdR(2, 2) /*G[8]*/ /*G(3,3)*/ =     mu1 * (r12 - 3 * r1z * r1z) / r15 + mu2 * (r22 - 3 * r2z * r2z) / r25;
    
    return ddUdR;
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
int EllipticThreeBodyProblem::CheckDerivatives(real_type t, const real_type* state, const real_type* lambda) {

    double mu_ratio = 0.01;
    real_type xyz[] = {0.95, 0.01, 0};
    Vector3 R; R << xyz[0], xyz[1], xyz[2];
    
    double omg = getPotential(mu_ratio, xyz);
    Vector3 g = EllipticThreeBodyProblem::compute_gravity(mu_ratio, R);
    Matrix3 matG = compute_gravity_gradient(mu_ratio, R);
    
    double h = 1e-5;
    Vector3 fd_g;    
    for (int idx=0; idx<3; ++idx) {
        xyz[idx] += h;
        double omgh = getPotential(mu_ratio, xyz);
        xyz[idx] -= h;        
        fd_g(idx) = (omgh - omg) / h;
    }
    
    Vector3 errg = fd_g - g;
    std::cout << "acceration vector       : " << g.transpose() << std::endl;
    std::cout << "acceration vector by fd : " << fd_g.transpose() << std::endl;        
    std::cout << "acceration error        : " << errg.transpose() << std::endl;
    
    Matrix3 fd_matG;    
    for (int idx=0; idx<3; ++idx) {
        R(idx) += h;
        Vector3 gh = EllipticThreeBodyProblem::compute_gravity(mu_ratio, R);
        R(idx) -= h;        
        fd_matG.block(0, idx, 3, 1) = (gh - g) / h;
    } 
    Matrix3 err_matG = fd_matG - matG;

    std::cout << "Mat       : " <<  matG << std::endl;
    std::cout << "Mat by fd : " << fd_matG << std::endl;
    std::cout << "Error     : " << err_matG << std::endl;

    return 0;
}

