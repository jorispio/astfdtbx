// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012-2013 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class EllipticThreeBodyProblem
 *
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.0
 * History:
 * --------------------------------------------------------------------- */
#ifndef __SMTBX_DYNAMICS_ELLIPTIC_THREE_BODY_PROBLEM_HPP
#define __SMTBX_DYNAMICS_ELLIPTIC_THREE_BODY_PROBLEM_HPP

#include "propagation/dynamics/DynamicsBase.hpp"
#include "propagation/dynamics/DynamicsCommon.hpp"

#ifdef WITH_GUIDANCE
    #include "propagation/control/GuidanceControlBase.hpp"
#endif

#ifdef WITH_SYSTEM
    #include "system/Thruster.hpp"
#endif

/* ------------------------------------------------------------------------------- */
class EllipticThreeBodyProblem : public TDynamicsBase {
private:
    real_type mu1;
    real_type mu2;
    real_type ecc;
    Vector3 body1_pos;  // position of larger mass (1)
    Vector3 body2_pos;  // position of smaller mass (2)
#ifdef WITH_GUIDANCE
    TGuidanceControlBase* optimalControl;
#endif

 public:
    /** Creator. */
    EllipticThreeBodyProblem(real_type mu1_, real_type mu2_, real_type ecc, const ThrusterData &ThrusterDef);
    EllipticThreeBodyProblem(real_type mu1_, real_type mu2_, const ThrusterData &ThrusterDef);
    EllipticThreeBodyProblem(real_type mu_ratio, const ThrusterData &ThrusterDef);

    /** Destructor */
    ~EllipticThreeBodyProblem();

    void Init(const real_type* S0_, const ThrusterData& ThrusterDef, double mu_, real_type epsilon_);

    void SetInitialConditions(const double* S0_);

    bool ComputeCommonVariablesForStateOnly(real_type t, const real_type* StateLagranges, CommonVariables &common);
    bool ComputeCommonVariablesForOptimalControl(real_type t, const real_type* StateLagranges, CommonVariables &common);

    /** */
    void getPrimaryPositions(const real_type f, real_type *xy1, real_type *xy2);

    void GetStateDynamics(real_type t,
                                  const CommonVariables& cV,
                     /* OUTPUT */ real_type* x_dot);

    void GetCostateDynamics(real_type t,
                                    const CommonVariables& cV,
                       /* OUTPUT */ real_type* x_dot);

    TGuidanceControlBase* GetControlInstance();

    void DynamicsTime(const real_type* x, const real_type* x_dot, Vector7& dFdT);

    void JacobianMatrixState(const CommonVariables& cv, MATRIXVAR& dFdX, int row0 = 0, int col0 = 0);

    void
    JacobianMatrixCoState(const CommonVariables& commonVariables, MATRIXVAR& dFdL_, int row0 = 0, int col0 = 0);

    void GetJacobianDynamics(real_type t,
                                     const real_type* X,
                                     const CommonVariables& cv,
                                     const real_type* x_dot,
                                     MATRIXVAR& dFdX);
    void DynamicsTimeCoast(const real_type* x,
                                   real_type* x_dot,
                                   Vector7& reference_state,
                                   Vector7& modified_state,
                                   Vector7& dFdT);

    real_type GetThrustAmplitude(real_type t, const real_type* state, const real_type* lambda);

    static real_type getPotential(double mu_ratio, const real_type *xyz);
    static Vector3 compute_time_derivative_gravity(double mu_ratio, const Vector3& R, const Vector3& V);
    static Vector3 compute_gravity(double mu_ratio, const Vector3& R);
    static Matrix3 compute_gravity_gradient(double constMu, const Vector3& R);

    int CheckDerivatives(real_type t, const real_type* state, const real_type* lambda);
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_DYNAMICS_ELLIPTIC_THREE_BODY_PROBLEM_HPP
