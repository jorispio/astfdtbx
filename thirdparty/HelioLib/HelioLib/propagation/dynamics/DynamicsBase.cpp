// $Id: DynamicsBase.cpp 459 2013-10-31 22:16:04Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DynamicsBase.hpp"

#include <math.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <time.h>

#include <cstdio>
#include <vector>

/* --------------------------------------------------------------------- */
/** Creator.
 *
 * @param model_ dynamical model
 * @param ThrusterDef  thruster definition
 * @param smoothingMethod thrust amplitude smoothing method
 * @param mu  gravitational parameter
 */
/* --------------------------------------------------------------------- */
TDynamicsBase::TDynamicsBase(DYNAMICALMODEL_TYPE model_,
                             const ThrusterData& thrusterDef,
                             real_type mu)
    : model(model_), a_m(1.0), a_s(1.0), constMu(mu),
      useConstantMass(false),
      implicit_integration(false),
      t0(0.), tf(0.), forced_coast(false) {
#ifdef WITH_SYSTEM
    withEclipse = thrusterDef.withEclipse;

    // Smoother = new SMOOTHTECH(model);
    // Smoother->SetSmoothing(smoothingMethod);
    // Smoother->SetThrust(ThrusterDef.Fth, ThrusterDef.g0Isp);
    // Smoother->SetObjective(critere);
    
    mThruster = NULL;
#endif

    Vinf[0] = 0;
    Vinf[1] = 0;
    Vinf[2] = 0;

    forces = std::vector<const Force*>();
}
/* --------------------------------------------------------------------- */
/** Set implicit integration flag. With implicit integration, the thrust
 * is activated directly on the switching function value, without smoothing.
 *
 * @param impl flag to enable implicit integration.
 * --------------------------------------------------------------------- */
void TDynamicsBase::ImplicitIntegration(bool impl) {
    implicit_integration = impl;
}
/* --------------------------------------------------------------------- */
/** Add a force to the dynamical model
 *
 * @return the dynamic model type.
 */
/* --------------------------------------------------------------------- */
DYNAMICALMODEL_TYPE TDynamicsBase::GetModelType() {
    return model;
}
/* --------------------------------------------------------------------- */
/** Get time normalisation coefficient
 *
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::setTimeNormalisation(real_type a_s_) {
    a_s = a_s_;
}
/* --------------------------------------------------------------------- */
/** Get the gravitational constant.
 *
 * @return Get the gravitational constant
 */
/* --------------------------------------------------------------------- */
double TDynamicsBase::getGravitationalConstant() {
    return constMu;
}

/* --------------------------------------------------------------------- */
/** Add a force to the dynamical model
 * @param force Force model
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::addForce(const Force *force) {
    forces.push_back(force);
}
/* --------------------------------------------------------------------- */
/** remove dynamical forces
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::clearForces(void) {
    forces.clear();
}
/* --------------------------------------------------------------------- */
/** Return the list of dynamical forces.
 */
/* --------------------------------------------------------------------- */
std::vector<const Force*> TDynamicsBase::getForces() const {
    return forces;
}

#ifdef WITH_SYSTEM
/* --------------------------------------------------------------------- */
/** Set thruster data.
 * @param ThrusterDef  thruster definition
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::SetThrusterData(const ThrusterData& ThrusterDef) {
    if (mThruster) {
        mThruster->SetThruster(ThrusterDef);
    }
    //Smoother->SetThrust(ThrusterDef.Fth, ThrusterDef.g0Isp);
}

/* --------------------------------------------------------------------- */
/** Set thruster data.
 * @param ThrusterDef  thruster definition
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::SetThruster(Thruster* mThrusterIn) {
    mThruster = mThrusterIn;
    //Smoother->SetThrust(Thruster.GetMaxSEPThrust(), Thruster.GetExhaustVelocity());
}

/* --------------------------------------------------------------------- */
/** Get thruster.
 * @return thruster
 */
/* --------------------------------------------------------------------- */
Thruster* TDynamicsBase::GetThruster() const {
    return mThruster;
}
#endif
/* --------------------------------------------------------------------- */
/** Set the constant mass flag.
 */
/* --------------------------------------------------------------------- */
void TDynamicsBase::SetConstantMassFlag(bool constant) {
    useConstantMass = constant;
}
/* --------------------------------------------------------------------- */
/** Get the constant mass flag.
 */
/* --------------------------------------------------------------------- */
bool TDynamicsBase::GetConstantMassFlag() {
    return useConstantMass;
}

/* --------------------------------------------------------------------- */
/** Get the switching function value.
 *
 * @param x state vector
 * @param lambda costate vector
 * @param uqsw control vector
 */
/* --------------------------------------------------------------------- */
real_type TDynamicsBase::GetHamiltonian(const real_type* x, const real_type* lambda, const Vector3& uqsw) {
    real_type StateLagranges[2 * STATE_SIZE];
    memcpy(StateLagranges, x, STATE_SIZE * sizeof(real_type));
    memcpy(StateLagranges + STATE_SIZE, lambda, STATE_SIZE * sizeof(real_type));

    CommonVariables cv;
    ComputeCommonVariables(0., StateLagranges, cv);
    cv.U = uqsw;
    cv.uThrustAcceleration = cv.U * cv.Thrust / cv.mass;  // we do not put the smoothed thrust amplitude

    real_type dx[2 * STATE_SIZE];
    GetDynamics(0., cv, dx);
    Vector7 f;
    f << dx[0], dx[1], dx[2], dx[3], dx[4], dx[5], dx[6];
    Vector7 vlambda;
    vlambda << lambda[0], lambda[1], lambda[2], lambda[3], lambda[4], lambda[5], lambda[6];

    return f.dot(vlambda);
}

/* ---------------------------------------------------------------------
 * Compute common variables for the State+CoState Dynamics
 * This function can be used externally (Dynamics_stm).
 * --------------------------------------------------------------------- */
bool TDynamicsBase::ComputeCommonVariables(real_type t,
                                           const real_type* StateLagranges,
                             /* OUTPUTS */ CommonVariables& common) {
    bool res1 = ComputeCommonVariablesForStateOnly(t, StateLagranges, common);
    bool res2 = ComputeCommonVariablesForOptimalControl(t, StateLagranges, common);
    return res1 && res2;
}
/* ---------------------------------------------------------------------
 * State+CoState Dynamics
 *
 * --------------------------------------------------------------------- */
void TDynamicsBase::GetDynamics(real_type t,
                                const CommonVariables& cV,
                   /* OUTPUT */ real_type* x_dot) {
    GetStateDynamics(t, cV, x_dot);
    GetCostateDynamics(t, cV, x_dot + 7);
}
