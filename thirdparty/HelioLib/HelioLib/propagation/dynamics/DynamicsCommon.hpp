// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_DYNAMICS_COMMON_HPP
#define __SMTBX_DYNAMICS_COMMON_HPP

#include "core/HelioDataTypes.hpp"
#include "physics/forces/Force.hpp"


/**
 */
struct SwitchingFunctionDerivatives {
    real_type SwitchFun_dt;

    Vector7 SwitchFun_dstate;

    Vector7 SwitchFun_dlambda;
    Vector3 SwitchFun_dlV;
    real_type SwitchFun_dlm;
    real_type SwitchFun_dm;

    real_type SwitchFun_dm2;
    Vector3 SwitchFun_dm_dlV;
    real_type SwitchFun_dm_dlm;
    real_type SwitchFun_dm_dt;
    Matrix7 SwitchFun_dstate_dlambda;

    /** Clear data. */
    void clear()
    {
        SwitchFun_dstate.setZero();
        SwitchFun_dlambda.setZero();
        SwitchFun_dlV.setZero();
        SwitchFun_dlm = 0;
        SwitchFun_dm = 0;

        SwitchFun_dm2 = 0;
        SwitchFun_dm_dlV.setZero();
        SwitchFun_dm_dlm = 0;
        SwitchFun_dm_dt = 0;

        SwitchFun_dstate_dlambda.setZero();
    }
};

/**
 */
struct SmoothedAmplitudeDerivatives {
    // first order
    real_type diffS_dm;
    real_type diffS_dlm;
    real_type diffS_dt;
    Vector3 diffS_dlV;     // cartesian formulation
    Vector7 diffS_dstate;  // equinoctial formulation
    Vector7 diffS_dlambda;  // equinoctial formulation

    // second order
    real_type diff2S_dm2;
    real_type diff2S_dm_dlm;
    real_type diff2S_dm_dt;
    Vector3 diff2S_dm_dlV;  // cartesian formulation
    Matrix7 diff2S_dstate2;
    Matrix7 diff2S_dlambda2;
    Matrix7 diff2S_dstatedlambda;

    SwitchingFunctionDerivatives SwitchingFunctionDerivs;

    /** Clear data. */
    void clear()
    {
        diffS_dm = 0;
        diffS_dlm = 0;
        diffS_dt = 0;
        diffS_dlV << 0, 0, 0;
        diffS_dstate << 0, 0, 0, 0, 0, 0, 0;
        diffS_dlambda << 0, 0, 0, 0, 0, 0, 0;
        diff2S_dm2 = 0;
        diff2S_dm_dlm = 0;
        diff2S_dm_dt = 0;
        diff2S_dm_dlV << 0, 0, 0;
        diff2S_dstate2.setZero();
        diff2S_dlambda2.setZero();
        diff2S_dstatedlambda.setZero();

        SwitchingFunctionDerivs.clear();
    }
};


/* ------------------------------------------------------------------------------- */
struct CommonVariables {
    GenericDate date;
    double t;

    Vector7 state;
    Vector7 lambda;

    // cartesian state
    Vector3 R;
    Vector3 V;

    // costate for cartesian state
    Vector3 lambda_R;
    Vector3 lambda_V;

    // equinoctial state
    double p;
    double ex;
    double ey;
    double hx;
    double hy;
    double Lv;  // true longitude

    // costate for equinoctial state
    double lambda_p;
    double lambda_ex;
    double lambda_ey;
    double lambda_hx;
    double lambda_hy;
    double lambda_L;

    // mass
    double mass;
    //

    double lambda_m;
    double lambda_m_dt;

    Vector3 U;
    double uAlpha;  // [Equinoctial] control spherical angle 1
    double uBeta;  // [Equinoctial] control spherical angle 2
    double uDelta;  // [Equinoctial] control amplitude

    Vector3 uThrustAcceleration;

    Vector3 fG;
    Matrix3 matG;

    /** dynamical perturbations */
    Vector3 fPerturbationsCartesian;
    Matrix6x3 jacPerturbationsCartesian;
    Vector6 fPerturbationsEquinoctial;
    Matrix6 jacPerturbationsEquinoctial;

    /** Smoothed thrust amplitude/throttle */
    double SmoothedAmplitude;

    /** Switching function value */
    double SwitchFun;
    //SwitchingFunctionDerivatives sd;

    /** Switching and smoothing functions, and derivatives */
    SmoothedAmplitudeDerivatives Sderivatives;

    /** Thruster variables and derivatives */
    real_type Thrust;
    real_type qfuel;
    real_type g0Isp;
    //
    Vector3 dThrustdR;
    Vector3 dQfueldR;
    Vector7 dThrustdX;
    Vector7 dQfueldX;
    // second order derivative
    Matrix3 d2ThrustdR;
    Matrix3 d2QfueldR;
    Matrix7 d2ThrustdX;
    Matrix7 d2QfueldX;

    //
    Matrix6x3 JacPosToState;

    /** Default constructor. */
    CommonVariables()
    {
        t = 0;
        p = ex = ey = hx = hy = Lv = 0;
        lambda_p = lambda_ex = lambda_ey = lambda_hx = lambda_hy = lambda_L = 0;
        mass = 0;
        lambda_m = 0;
        lambda_m_dt = 0;
        uAlpha = uBeta = uDelta = 0;
        SmoothedAmplitude = 0;
        SwitchFun = 0;

        Thrust = 0;
        qfuel = 0;
        g0Isp = 0;
        qfuel = 0;
    }

    /** */
    CommonVariables(const real_type* x, const real_type* l)
    {
        t = 0;
        state << x[0], x[1], x[2], x[3], x[4], x[5], x[6];
        lambda << l[0], l[1], l[2], l[3], l[4], l[5], l[6];
        mass = x[6];

        p = x[0];
        ex = x[1];
        ey = x[2];
        hx = x[3];
        hy = x[4];
        Lv = x[5];
        lambda_p = l[0];
        lambda_ex = l[1];
        lambda_ey = l[2];
        lambda_hx = l[3];
        lambda_hy = l[4];
        lambda_L = l[5];

        lambda_m = l[6];
        lambda_m_dt = 0;

        U.setZero();
        uAlpha = uBeta = uDelta = 0;

        Thrust = 1;
        dThrustdR.setZero();
        dThrustdX.setZero();
        dQfueldR.setZero();
        dQfueldX.setZero();

        uThrustAcceleration.setZero();

        SmoothedAmplitude = 1;
        SwitchFun = 0;

        Sderivatives.diffS_dstate.setZero();
    }
};

#endif  // __SMTBX_DYNAMICS_COMMON_HPP
