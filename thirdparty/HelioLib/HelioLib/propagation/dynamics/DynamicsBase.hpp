// $Id: DynamicsBase.hpp 459 2013-10-31 22:16:04Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_DYNAMICS_BASE_HPP
#define __SMTBX_DYNAMICS_BASE_HPP
/* --------------------------------------------------------------------- */
#define STATE_SIZE 7


#include "propagation/dynamics/DynamicsCommon.hpp"
#include "core/HelioEnumerations.hpp"

#ifdef WITH_CONTROL
    #include "propagation/control/GuidanceControlBase.hpp"
#else
    class TGuidanceControlBase;
#endif

#ifdef WITH_SYSTEM
    #include "system/Thruster.hpp"
#else
    class Thruster;
    struct ThrusterData { };
#endif

class TGuidanceControlBase;

/* ------------------------------------------------------------------------------- */
class TDynamicsBase {
 protected:
    DYNAMICALMODEL_TYPE model;

    double a_m;
    double a_s;

    /* internal constants */
    /** Central body gravitational constant. */
    double constMu;
    /** Central body gravitational J2-zonal constant. */
    double constJ2;
    double constJ0;

    /* constant mass flag. */
    bool useConstantMass;

    /** dynamical forces */
    std::vector<const Force*> forces;

    /** Indicate whether eclipse shutdown is active */
    bool withEclipse;

#ifdef WITH_SYSTEM
    /** Thruster. */
    Thruster *mThruster;
#endif

    /* initial state/costate vector. */
    double S0[2 * STATE_SIZE];

    /** implicit integration flag. */
    bool implicit_integration;

    /** hyperbolic initial velocity vector. */
    double Vinf[3];

    /** initial date. */
    double t0;

    /** final date. */
    double tf;

    /** coast flag. */
    bool forced_coast;

 public:
    /** Creator. */
    TDynamicsBase(DYNAMICALMODEL_TYPE type, const ThrusterData& ThrusterDef, real_type mu);

    /** Destructor. */
    virtual ~TDynamicsBase(void) { }

    /** Return the model type */
    DYNAMICALMODEL_TYPE GetModelType();

    /** Set normalising coefficient for time */
    void setTimeNormalisation(real_type a_s_);

    /** Set initial state conditions */
    virtual void SetInitialConditions(const double* S0_) = 0;

    /** Initialise the instance */
    virtual void Init(const real_type* S0_, const ThrusterData& ThrusterDef, double mu_, real_type epsilon_) = 0;

    /** Set hyperbolic initial velocity vector. */
    void SetVinf(double* Vinf_);

#ifdef WITH_SYSTEM
    /** Set thruster. */
    void SetThrusterData(const ThrusterData& ThrusterDef);

    /** Set thruster. */
    void SetThruster(Thruster* ThrusterDef);

    /** Get thruster data. */
    Thruster* GetThruster() const;
#endif

    /** Get the gravitational constant. */
    double getGravitationalConstant();

    /** Add a force to the dynamical model */
    void addForce(const Force *force);

    /** remove dynamical forces */
    void clearForces(void);

    /** */
    std::vector<const Force*> getForces() const;

    /** Set implicit integration. No continuation. */
    void ImplicitIntegration(bool impl);

    /** Flag to set the constant mass. */
    void SetConstantMassFlag(bool constant);

    /** Get the constant mass flag. */
    bool GetConstantMassFlag();

    /** Compute all variables common to the methods of the class.
     *
     * @param t  time
     * @param StateLagranges state/costate vector
     * @param common common variables
     * */
    virtual bool ComputeCommonVariablesForStateOnly(real_type t, const real_type* StateLagranges, CommonVariables &common) = 0;
    virtual bool ComputeCommonVariablesForOptimalControl(real_type t, const real_type* StateLagranges, CommonVariables &common) = 0;

    bool ComputeCommonVariables(real_type t,
                                        const real_type* StateLagranges,
                                        /* OUTPUTS */ CommonVariables& common);

    /** */
    virtual void GetStateDynamics(real_type t,
                                  const CommonVariables& cV,
                     /* OUTPUT */ real_type* x_dot) = 0;

    virtual void GetCostateDynamics(real_type t,
                                    const CommonVariables& cV,
                       /* OUTPUT */ real_type* x_dot) = 0;

    /** Get the dynamics.
     *
     * @param t  time
     * @param cV common variables
     * @param x_dot state time derivative
     * */
    void GetDynamics(real_type t,
                     const CommonVariables& cV,
        /* OUTPUT */ real_type* x_dot);

#ifdef WITH_GUIDANCE
    /** Get the optimal control class.
     * @return the optimal control class instance
     * */
    virtual TGuidanceControlBase* GetControlInstance() = 0;
#endif

    /** Get the Hamiltonian function value.
     * @param x state vector
     * @param lambda costate vector
     * @param u control vector
     *
     * @return the hamiltonian value
     * */
    real_type GetHamiltonian(const real_type* x, const real_type* lambda, const Vector3& u);

    /** Get the time derivative of the dynamics.
     * @param x state vector
     * @param x_dot
     * @param dFdT
     * */
    virtual void DynamicsTime(const real_type* x, const real_type* x_dot, Vector7& dFdT) = 0;

    /** Jacobian of the dynamics. elementary function
      * Part state/state  (7x7)
      * Jacobian df/dx (state only)
      *
      * @param cv  common variable
      * @param dFdX
      * */
    virtual void JacobianMatrixState(const CommonVariables& cv, MATRIXVAR& dFdX, int row0 = 0, int col0 = 0) = 0;

    /** Jacobian of the dynamics. elementary function
      * Part costate/costate  (7x7)
      *
      * @param cv  common variable
      * @param dFdX
      *  */
    virtual void
    JacobianMatrixCoState(const CommonVariables& commonVariables, MATRIXVAR& dFdL_, int row0 = 0, int col0 = 0) = 0;

    /** Get the dynamics of the dynamics Jacobian.
     *
     * @param t  time
     * @param X  state vector
     * @param cv  common variable
     * @param x_dot
     * @param dFdX
     * */
    virtual void GetJacobianDynamics(real_type t,
                                     const real_type* X,
                                     const CommonVariables& cv,
                                     const real_type* x_dot,
                                     MATRIXVAR& dFdX) = 0;

    /** Get the dynamics for a coast segment.
     *
     * @param x  state vector
     * @param x_dot
     * @param reference_state
     * @param modified_state
     * @param dFdT
     * */
    virtual void DynamicsTimeCoast(const real_type* x,
                                   real_type* x_dot,
                                   Vector7& reference_state,
                                   Vector7& modified_state,
                                   Vector7& dFdT) = 0;

    /** Get the thrust amplitude.
     *
     * @param t  time
     * @param state state vector
     * @param lambda costate vector
     * @return the thrust amplitude
     * */
    virtual real_type GetThrustAmplitude(real_type t, const real_type* state, const real_type* lambda) = 0;

    /** Check derivative. state/costate. No time.
     *
     * @param t  time
     * @param state reference state vector
     * @param lambda reference costate vector
     *
     * @return the number of derivative errors
     * */
    virtual int CheckDerivatives(real_type t, const real_type* state, const real_type* lambda) = 0;
};

/* --------------------------------------------------------------------- */
#endif  // __SMTBX_DYNAMICS_BASE_HPP
