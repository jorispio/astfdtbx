// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_StateTransitionMatrixRTBP_HPP
#define __SMTBX_StateTransitionMatrixRTBP_HPP

#include "maths/coordinates/CartesianCoordinates.hpp"
#include "propagation/StateTransitionMatrix.hpp"
#include "propagation/dynamics/DynamicsEllipticThreeBodyProblem.hpp"

/* ------------------------------------------------------------------------------- */
class StateTransitionMatrixRtbp : public StateTransitionMatrix<X_SIZE> {
private:
    /** control */
    double uxyzDefault[3];

    EllipticThreeBodyProblem *dynamicalModel;

    /* */
    void DiffEqn(real_type x, const real_type* y, real_type* f);

    /* */
    void dynamic(unsigned n, double t, double* X, double* Xdot);

    /* */
    void solout(long nr, double xold, double x, double* y, unsigned n, int* irtrn);

    /* */
    void STMdispatch(const Matrix7& STM, Matrix3& STM1, Matrix3& STM2, Matrix3& STM3, Matrix3& STM4);

    MATRIXVAR dFdX;

 public:
    StateTransitionMatrixRtbp(double mu_ratio);
    ~StateTransitionMatrixRtbp(void) { }

    /** */
    void setDefaultControl(const Vector3& u);

    /* */
    int getFinalPoint(const Vector3d& r1,
                      const Vector3d& v1,
                      double t0,
                      double ti,
                      /* OUTPUTS */
                      Vector3d& rf,
                      Vector3d& vf);

    /* */
    int getSTM(const Vector3d& r1,
               const Vector3d& v1,
               double t0,
               double &ti,
               /* OUTPUTS */
               Matrix3& STM1,
               Matrix3& STM2,
               Matrix3& STM3,
               Matrix3& STM4,
               Matrix7& STM,
               Vector3dExt& P1,
               Vector3dExt& P2,
               double& P3,
               Vector3d& rf,
               Vector3d& vf);

    /* */
    int getSTM(const Vector3d& r1,
               const Vector3d& v1,
               double t0,
               double &ti,
               /* OUTPUTS */
               Matrix3& STM1,
               Matrix3& STM2,
               Matrix3& STM3,
               Matrix3& STM4,
               Matrix6& STM,
               Vector3d& rf,
               Vector3d& vf);

    static void getJacobian(real_type tau, const real_type* x,
                            real_type* uxyz,
                            real_type mu,
                            real_type J0,
                            std::vector<Force*> &forces,
                            double tof,
                            /* OUTPUTS */ Matrix7& Jacobian);
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_StateTransitionMatrixRTBP_HPP
