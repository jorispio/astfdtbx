// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2012 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#include "SimulationEvent.hpp"

// ---------------------------------------------------------------------
/** Constructor.
 */
// ---------------------------------------------------------------------
SimulationEventHandler::SimulationEventHandler(const char* st, const char* sd)
    : name(sd), type(st) {
    bDirection = 0; // 0: both, -1: increasing, +1: decreasing
    actionOnEvent = EventActionContinue;
}
// ---------------------------------------------------------------------
/** Destructor.
 */
// ---------------------------------------------------------------------
void SimulationEventHandler::setDirection(int sens) {
    bDirection = sens;
}
// ---------------------------------------------------------------------
/** Indicate whether the event stops the propagation */
void SimulationEventHandler::setActionOnEvent(EventAction stop) {
    actionOnEvent = stop;
}
// ---------------------------------------------------------------------
/** Destructor.
 */
// ---------------------------------------------------------------------
EventAction SimulationEventHandler::getAction() {
    return actionOnEvent;
}
// ---------------------------------------------------------------------
