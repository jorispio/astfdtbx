// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_ORBITS_HPP_
#define __SMTBX_ORBITS_HPP_

#ifdef WITH_ORBIT
    #include "orbit/CartesianOrbit.hpp"
    #include "orbit/EquinoctialOrbit.hpp"
    #include "orbit/KeplerianOrbit.hpp"
    #include "orbit/Orbit.hpp"
    #include "orbit/OrbitClasses.hpp"
    #include "orbit/OrbitParameterJacobian.hpp"
#endif

#endif  // __SMTBX_ORBITS_HPP_