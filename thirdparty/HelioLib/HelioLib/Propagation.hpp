// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PROPAGATION_HPP_
#define __SMTBX_PROPAGATION_HPP_

//#include "Propagation/TrajectoryPropage.hpp"
#ifdef WITH_R3BP
    #include "propagation/TrajectoryPropageRtbp.hpp"
    #include "propagation/StateTransitionMatrixRtbp.hpp"
#endif
#ifndef LIGHT_VERSION
    #include "propagation/TrajectoryPropageCartesian.hpp"
    #include "propagation/TrajectoryPropageEquinoctial.hpp"
    #include "propagation/StateTransitionMatrixCartesian.hpp"
    #include "propagation/StateTransitionMatrixEquinoctial.hpp"
    #include "propagation/TrajectoryPropageCartesian.hpp"
    #include "propagation/TrajectoryPropageEquinoctial.hpp"
    #include "propagation/StateTransitionMatrixCartesian.hpp"
    #include "propagation/StateTransitionMatrixEquinoctial.hpp"
#endif

#include "propagation/StateTransitionMatrix.hpp"

#ifdef WITH_EVENT
    #include "propagation/SimulationEvent.hpp"
#endif

#ifdef WITH_ANALYTICAL_PROPAGATION
    #include "propagation/propageAnalyticalWithJ2.hpp"
#endif

#endif  // __SMTBX_PROPAGATION_HPP_
