// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_SPACEMECALIB_VERSION_CONFIG_HPP_
#define __SMTBX_SPACEMECALIB_VERSION_CONFIG_HPP_

#include <cstdio>
#include <string>

// alternatively you could add your global method getLibInterfaceVersion here
class HelioLibVersion {
 public:
    static
    std::string getLibVersion() {
        return "0.4.1  [23/10/2013]";
    }

    static
    void printDisclaimer() {
        printf("****************************************\n");
        printf("Space Mechanics Library - @LIB_VERSION\n");        
        printf("****************************************\n");
    }
};

#endif  // __SMTBX_SPACEMECALIB_VERSION_CONFIG_HPP_
