// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_MATHS_HPP_
#define __SMTBX_MATHS_HPP_

#include "maths/Array.hpp"
#include "maths/MathUtils.hpp"
#include "maths/MatrixArray.hpp"
#include "maths/RotationQuaternion.hpp"
#include "maths/Vector3dArray.hpp"
#include "maths/Vector3dExt.hpp"

#include "maths/SpecialFunctions/SmoothHeavisideFunction.hpp"

// COORDINATES
#include "maths/coordinates/CartesianCoordinates.hpp"
#include "maths/coordinates/AngularCoordinates.hpp"
#ifndef LIGHT_VERSION
    #include "maths/coordinates/SphericalCoordinates.hpp"
    #include "maths/coordinates/GeodeticCoordinates.hpp"
#endif

// INTERPOLATION
#ifdef WITH_INTERPOLATION
    #include "maths/CLegendre.hpp"
    #include "maths/interpolation/CLagrange.hpp"
    #include "maths/interpolation/AkimaSpline.hpp"
#endif
#include "maths/Polynomial.hpp"

// QUADRATURE
#ifdef WITH_QUADRATURE
    #include "maths/quadrature/GaussLegendre.hpp"
#endif

// ODE SOLVER
#ifdef WITH_ODE_SOLVERS
    #include "maths/integration/dop853_cpp.hpp"
    #include "maths/integration/rk4solver.hpp"
#endif
#include "maths/integration/DormandPrince5.hpp"

// SOLVERS
#include "maths/solvers/Raphson.hpp"
#ifdef WITH_SOLVERS
    #include "maths/solvers/Raphson.hpp"
    #include "maths/solvers/LeastSquare.hpp"
    #include "maths/solvers/NonLinearEquationSolver.hpp"
    #include "maths/specialFunctions/SmoothHeavisideFunction.hpp"
    #include "maths/specialFunctions/SmoothRectangularFunction.hpp"
#endif

#endif  // __SMTBX_MATHS_HPP_
