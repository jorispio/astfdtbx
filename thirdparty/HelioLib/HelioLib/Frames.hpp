// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_FRAMES_HPP_
#define __SMTBX_FRAMES_HPP_

#include "frames/Frame.hpp"
#include "frames/FrameTransform.hpp"
#include "frames/BodyPseudoInertialFrame.hpp"
#ifdef WITH_ADVANCED_FRAMES
    #include "frames/BodyRotatingFrame.hpp"
    #include "frames/FramesFactory.hpp"
    #include "frames/GeodeticFrame.hpp"
    #include "frames/LocalOrbitalFrame.hpp"
#endif

#endif  // __SMTBX_FRAMES_HPP_