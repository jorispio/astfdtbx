/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_FRAME_HPP_
#define __SMTBX_FRAME_HPP_

#include "FrameTransform.hpp"

//
class Frame {
  protected:
    /** frame name */
    std::string name;

    /** parent frame */
    Frame* parent;

 private:
    /** Depth of the frame with respect to tree root. */
    int depth;

    /** Find the deepest common ancestor of two frames in the frames tree.
     * @param from origin frame
     * @param to destination frame
     * @return an ancestor frame of both <code>from</code> and <code>to</code>
     */
    static Frame* findCommon(const Frame* from, const Frame* to);

    /** Get the n<sup>th</sup> ancestor of the frame.
     * @param n index of the ancestor (0 is the instance, 1 is its parent,
     * 2 is the parent of its parent...)
     * @return n<sup>th</sup> ancestor of the frame (must be between 0
     * and the depth of the frame)
     * @exception IllegalArgumentException if n is larger than the depth
     * of the instance
     */
    Frame* getAncestor(int n) const;

 public:
    /** Constructor */
    Frame(std::string name = "", Frame* parent = NULL);

    /** Constructor */
    Frame(std::string name, const Frame& parent);

    /** Desctructor */
    virtual ~Frame() { };

    /** Get the name.
     * @return the name
     */
    std::string getName() const {
        return name;
    }

    /** Get the Transform corresponding to specified date from this frame to parent.
     * @param date current date
     * @return transform at specified date from this frame to parent.
         */
    virtual FrameTransform getTransform(const GenericDate& date) const = 0;

    /** Get the transform from the instance to another frame.
     * @param destination destination frame to which we want to transform vectors
     * @param date the date (can be null if it is sure than no date dependent frame is used)
     * @return transform from the instance to the destination frame
     */
    FrameTransform getTransformTo(const Frame* destination, const GenericDate& date) const;

    /**
     * @brief
     * @param frame
     * @return
     */
    bool isEqual(const Frame& frame) const;

    /** Return parent frame. */
    Frame* getParent() const {
        return parent;
    }
};

#ifndef WITH_ADVANCED_FRAME
    class GcrfFrame : public Frame {
     public:
        GcrfFrame()
            : Frame(std::string("GCRF")){};

        ~GcrfFrame(){};

        FrameTransform getTransform(const GenericDate& ) const {
            return FrameTransform::IDENTITY;  // FIXME(jo) convenience implementation but wrong
        }
    };

    class IcrfFrame : public Frame {
     public:
        IcrfFrame(const GcrfFrame& parent)
            : Frame(std::string("ICRF"), parent){};

        ~IcrfFrame(){};

        FrameTransform getTransform(const GenericDate& ) const {
            return FrameTransform::IDENTITY;  // FIXME(jo) convenience implementation but wrong
        }
    };

    class FramesFactory {
     public:
        /** Root frame. GCRF */
        static const GcrfFrame* GCRF;

        /** The International Celestial Reference Frame (ICRF)
         * It is a quasi-inertial reference frame centered at the barycenter of the Solar System*/
        static const IcrfFrame* ICRF;
    };
#endif

#endif  // __SMTBX_FRAME_HPP_
