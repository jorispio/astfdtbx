/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_FRAMES_FRAME_TRANSFORM_HPP_
#define __SMTBX_FRAMES_FRAME_TRANSFORM_HPP_

#include "core/HelioDataTypes.hpp"
#include "date/GenericDate.hpp"
#include "maths/coordinates/AngularCoordinates.hpp"
#include "maths/coordinates/CartesianCoordinates.hpp"
#include "maths/RotationQuaternion.hpp"
#include "maths/Vector3dExt.hpp"


/** Frame transformation class.
 *
 * This class represents the transformation engine between frames.
 * It uses the relationship between frames and their respective
 * parent frames to construct an overall transform.
 *
 * The convention used is vectorial transformation, with the transform notation
 *   M_A->B
 * such that
 *  R_B = M_A->B * R_A
 *
 * That is, M_A->B is the transform to apply to the
 * coordinates of a vector expressed in the old frame, A, to obtain the
 * same vector expressed in the new frame, B.
 *
 */
class FrameTransform {
 private:
    /** Date of the transform. */
    GenericDate date;

    /** Name of the transform */
    std::string name;

    /** Cartesian coordinates of the target frame with respect to the original frame. */
    CartesianCoordinates cartesian;

    /** Angular coordinates of the target frame with respect to the original frame. */
    AngularCoordinates angular;

    /** Compute a composite translation.*/
    static Vector3dExt compositeTranslation(const FrameTransform& first, const FrameTransform& second);

    /** Compute a composite velocity. */
    static Vector3dExt compositeVelocity(const FrameTransform& first, const FrameTransform& second);

    /** Compute a composite acceleration.*/
    static Vector3dExt compositeAcceleration(const FrameTransform& first, const FrameTransform& second);

    /** Compute a composite rotation.*/
    static RotationQuaternion compositeRotation(const FrameTransform& first, const FrameTransform& second);

    /** Compute a composite rotation rate. */
    static Vector3dExt compositeRotationRate(const FrameTransform& first, const FrameTransform& second);

    /** Compute a composite rotation acceleration. */
    static Vector3dExt compositeRotationAcceleration(const FrameTransform& first, const FrameTransform& second);

 public:
    /** Identity transform. */
    static const FrameTransform IDENTITY;

    FrameTransform();

    /** Build a transform.
     * @param date date of the transform
     * @param cartesian Cartesian coordinates of the target frame with respect to the original frame
     * @param angular angular coordinates of the target frame with respect to the original frame
     */
    FrameTransform(std::string name,
                   const GenericDate& date,
                   const CartesianCoordinates& cartesian,
                   const AngularCoordinates& angular = AngularCoordinates::IDENTITY);

    /** Build a rotation transform.
     * @param date date of the transform
     * @param angular angular part of the transformation to apply (i.e. rotation to
     * apply to the coordinates of a vector expressed in the old frame to obtain the
     * same vector expressed in the new frame, with its rotation rate)
     */
    FrameTransform(std::string name, const GenericDate& date, const AngularCoordinates& angular);

    /** Build a translation transform.
     * @param date date of the transform
     * @param translation translation to apply (i.e. coordinates of
     * the transformed origin, or coordinates of the origin of the
     * old frame in the new frame)
     * @param velocity the velocity of the translation (i.e. origin
     * of the old frame velocity in the new frame)
     * @param acceleration the acceleration of the translation (i.e. origin
     * of the old frame acceleration in the new frame)
     */
    FrameTransform(std::string name,
                   const GenericDate& date,
                   const Vector3dExt& translation,
                   const Vector3dExt& velocity = Vector3dExt::ZERO,
                   const Vector3dExt& acceleration = Vector3dExt::ZERO);

    /** Build a rotation transform.
     * @param date date of the transform
     * @param rotation rotation to apply ( i.e. rotation to apply to the
     * coordinates of a vector expressed in the old frame to obtain the
     * same vector expressed in the new frame )
     * @param rotationRate the axis of the instant rotation
     * @param rotationAcceleration the axis of the instant rotation
     * expressed in the new frame. (norm representing angular rate)
     */
    FrameTransform(std::string name,
                   const GenericDate& date,
                   const RotationQuaternion& rotation,
                   const Vector3dExt& rotationRate = Vector3dExt::ZERO,
                   const Vector3dExt& rotationAcceleration = Vector3dExt::ZERO);

    /** Build a transform by combining two transforms */
    FrameTransform(const GenericDate& date, FrameTransform& first, FrameTransform& second);

    std::string getName() const {
        return name;
    }

    GenericDate getDate() const {
        return date;
    }

    FrameTransform propagate(double dt) const;

    /** Get the inverse transform of the instance.*/
    FrameTransform getInverse() const;

    /** Transform a position vector (including translation effects).*/
    Vector3dExt transformPosition(const Vector3dExt& position) const;

    /** Transform including kinematic effects.*/
    CartesianCoordinates transform(CartesianCoordinates &pva) const;

    /** Transform a vector (ignoring translation effects).*/
    Vector3dExt transform(const Vector3dExt& vector) const;

    /** Compute the Jacobian of the transform. */
    void getJacobian(Matrix9& jacobian);

    /** @return underlying elementary translation with its derivative*/
    inline CartesianCoordinates getCartesian() { return cartesian; }

    /** @return underlying elementary translation*/
    inline Vector3dExt getTranslation() { return cartesian.getPosition(); }

    /** @return first time derivative of the translation  */
    inline Vector3dExt getVelocity() { return cartesian.getVelocity(); }

    /** @return second time derivative of the translation */
    Vector3dExt getAcceleration() { return cartesian.getAcceleration(); }

    /** @return underlying elementary rotation with its derivative */
    AngularCoordinates getAngular() { return angular; }

    /** @return underlying elementary rotation */
    RotationQuaternion getRotationQuaternion() { return angular.getRotationQuaternion(); }

    /** @return First time derivative of the rotation */
    Vector3dExt getRotationQuaternionRate() { return angular.getRotationQuaternionRate(); }

    /** @return Second time derivative of the rotation */
    Vector3dExt getRotationQuaternionAcceleration() { return angular.getRotationQuaternionAcceleration(); }
};

#endif  // __SMTBX_FRAMES_FRAME_TRANSFORM_HPP_
