/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_BODY_PSEUDO_INERTIAL_FRAME_HPP_
#define __SMTBX_BODY_PSEUDO_INERTIAL_FRAME_HPP_

#include "ephemerides/PlanetAnalyticEphemerides.hpp"
#include "Frame.hpp"
#include "FrameTransform.hpp"

/** @class BodyPseudoInertialFrame
 * @brief define a pseudo inertial frame attached to a celestial body.
 */
class BodyPseudoInertialFrame : public Frame {
 private:
    int planetid;
    PlanetAnalyticEphemerides* planetEph;

 public:
    BodyPseudoInertialFrame(std::string name, int planetid, double a_m = 1., double a_s = 1.);
    ~BodyPseudoInertialFrame();

    /** get the body this frame is attached to */
    int getBody();

    /** Get the Transform corresponding to specified date. */
    FrameTransform getTransform(const GenericDate& date) const;

    Vector3dExt getPosition(const GenericDate& date, const Vector3dExt& position) const;
};
//---------------------------------------------------------------------------
#endif  // __SMTBX_BODY_PSEUDO_INERTIAL_FRAME_HPP_
