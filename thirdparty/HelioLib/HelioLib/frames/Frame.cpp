/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Frame.hpp"

#include <string>

//#define DEBUG_FRAME
#ifndef WITH_ADVANCED_FRAME
    const GcrfFrame* FramesFactory::GCRF = new GcrfFrame();
    const IcrfFrame* FramesFactory::ICRF = new IcrfFrame(*FramesFactory::GCRF);
#endif

// ---------------------------------------------------------------------------------------
/**
 * @brief
 * @param lhs
 * @param rhs
 * @return
 */
bool operator==(const Frame& lhs, const Frame& rhs) {
    return lhs.isEqual(rhs);
}

/**
 * @brief
 * @param lhs
 * @param rhs
 * @return
 */
bool operator!=(const Frame& lhs, const Frame& rhs) {
    return !(lhs == rhs);
}

// ---------------------------------------------------------------------------------------
/** Constructor */
Frame::Frame(std::string name, Frame* parent_) {
    this->name = name;
    parent = parent_;
    depth = 0;
    if (parent != NULL) {
        depth = parent->depth + 1;
    }
}

/** Constructor
 * @brief
 * @param name
 * @param parent
 * @return
 */
Frame::Frame(std::string name, const Frame& parent_) : name(name) {
    parent = const_cast<Frame*>(&parent_);
    depth = parent->depth + 1;
}

/** Find the deepest common ancestor of two frames in the frames tree.
 * @param from origin frame
 * @param to destination frame
 * @return an ancestor frame of both <code>from</code> and <code>to</code>
 */
Frame* Frame::findCommon(const Frame* from, const Frame* to) {
#ifdef DEBUG_FRAME
    std::cout << "frames  " << from->getName() << " -> " << to->getName() << std::endl;
    std::cout << "depth  " << from->depth << "  " << to->depth << std::endl;
#endif

    // select deepest frames that could be the common ancestor
    Frame* currentF = const_cast<Frame*>((from->depth > to->depth) ? from->getAncestor(from->depth - to->depth) : from);
    Frame* currentT = const_cast<Frame*>((from->depth > to->depth) ? to : to->getAncestor(to->depth - from->depth));

    // go upward until we find a match
    while (*currentF != *currentT) {
        currentF = (currentF->parent);
        currentT = (currentT->parent);
    }

#ifdef DEBUG_FRAME
    std::cout << "common  " << currentT->getName() << std::endl;
#endif
    return currentF;
}

/** Get the n<sup>th</sup> ancestor of the frame.
 * @param n index of the ancestor (0 is the instance, 1 is its parent,
 * 2 is the parent of its parent...)
 * @return n<sup>th</sup> ancestor of the frame (must be between 0
 * and the depth of the frame)
 * @exception IllegalArgumentException if n is larger than the depth
 * of the instance
 */
Frame* Frame::getAncestor(int n) const {
    // safety check
    if (n > depth) {
        throw HelioLibException("Frame with no ancestor");
    }

    // go upward to find ancestor
    Frame* current = (this->parent);
    for (int i = 1; i < n; ++i) {
        current = (current->parent);
    }

    return current;
}

/** Get the transform from the instance to another frame.
 * @param destination destination frame to which we want to transform vectors
 * @param date the date (can be null if it is sure than no date dependent frame is used)
 * @return transform from the instance to the destination frame
 */
FrameTransform Frame::getTransformTo(const Frame* destination, const GenericDate& date) const {
    if (destination == NULL) {
        // no parent frame set!
    }

    if (*this == *destination) {
        // shortcut for special case that may be frequent
        return FrameTransform::IDENTITY;
    }

    // common ancestor to both frames in the frames tree
    Frame* common = findCommon(const_cast<Frame*>(this), destination);

    // transform from common to instance
    FrameTransform tInstanceToCommon = FrameTransform::IDENTITY;
    Frame* frame1 = const_cast<Frame*>(this);
    while (*frame1 != *common) {
#ifdef DEBUG_FRAME
        std::cout << "frame1  " << frame1->getName() << "  ->  " << frame1->parent->getName() << std::endl;
#endif
        // transform to parent
        FrameTransform transform = frame1->getTransform(date);
        // frame1 -> parent -> common
        tInstanceToCommon = FrameTransform(date, tInstanceToCommon, transform);
        frame1 = frame1->parent;

#ifdef DEBUG_FRAME
        std::cout << " transform=" << transform.getName() << " translation=" << transform.getTranslation().transpose();
        std::cout << "    rotation=" << transform.getRotationQuaternion().getAngle() << std::endl;

        std::cout << " CurrentTotaltransform=" << tInstanceToCommon.getName()
                  << " translation=" << tInstanceToCommon.getTranslation().transpose();
        std::cout << "    rotation=" << tInstanceToCommon.getRotationQuaternion().getAngle() << std::endl;
#endif
    }

    // transform from destination up to common
    FrameTransform tDestinationToCommon = FrameTransform::IDENTITY;
    Frame* frame = const_cast<Frame*>(destination);
    while (*frame != *common) {
        FrameTransform transform = frame->getTransform(date);
        tDestinationToCommon = FrameTransform(date, transform, tDestinationToCommon);
        frame = frame->parent;
    }

    // transform from instance to destination via common
    FrameTransform tCommonToDestination = tDestinationToCommon.getInverse();

#ifdef DEBUG_FRAME
    std::cout << " transform1=" << tInstanceToCommon.getName() << std::endl;
    std::cout << "    translation=" << tInstanceToCommon.getTranslation().transpose() << std::endl;
    std::cout << "    rotation=" << tInstanceToCommon.getRotationQuaternion().getAngle() << std::endl;
    std::cout << " transform2=" << tCommonToDestination.getName() << std::endl;
    std::cout << "    translation=" << tCommonToDestination.getTranslation().transpose() << std::endl;
    std::cout << "    rotation=" << tCommonToDestination.getRotationQuaternion().getAngle() << std::endl;
#endif

    return FrameTransform(date, tInstanceToCommon, tCommonToDestination);
}

/** Compare two frames.
 * @param frame  Frame to compare to
 * @return true if frames are equal
 */
bool Frame::isEqual(const Frame& frame) const {
    return (name.compare(frame.getName()) == 0);
}
