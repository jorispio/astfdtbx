/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <memory.h>

#include <algorithm>
#include <string>

#include "FrameTransform.hpp"

// #define DEBUG_FRAME_TRANSFORM

// ---------------------------------------------------------------------------------------
/** Specialized class for identity transform. */
class IdentityTransform : public FrameTransform {
 public:
    /** Simple constructor. */
    IdentityTransform()
        : FrameTransform(std::string("Identity"),
                         GenericDate(EPOCH::J2000),
                         CartesianCoordinates::ZERO,
                         AngularCoordinates::IDENTITY) {  }

    ~IdentityTransform() { }

    FrameTransform propagate(double ) const {
        return *this;
    }
};

const FrameTransform FrameTransform::IDENTITY = IdentityTransform();

// ---------------------------------------------------------------------------------------
using namespace std;

/**
 *
 */
FrameTransform::FrameTransform()
    : date(GenericDate(0)),
    name("empty"),
    cartesian(CartesianCoordinates::ZERO),
    angular(AngularCoordinates::IDENTITY) { }
// ---------------------------------------------------------------------
/** Build a transform from its primitive operations.
 * @param date date of the transform
 * @param cartesian Cartesian coordinates of the target frame with respect to the original frame
 * @param angular angular coordinates of the target frame with respect to the original frame
 */
FrameTransform::FrameTransform(std::string name,
                               const GenericDate& date_,
                               const CartesianCoordinates& cartesian_,
                               const AngularCoordinates& angular_)
    : date(date_), name(name), cartesian(cartesian_), angular(angular_) { }
// ---------------------------------------------------------------------
/** Build a rotation transform.
 * @param date date of the transform
 * @param angular angular part of the transformation to apply (i.e. rotation to
 * apply to the coordinates of a vector expressed in the old frame to obtain the
 * same vector expressed in the new frame, with its rotation rate)
 */
FrameTransform::FrameTransform(std::string name, const GenericDate& date, const AngularCoordinates& angular)
    : date(date),
     name(name),
     cartesian(CartesianCoordinates::ZERO),
     angular(angular) { }
// ---------------------------------------------------------------------
/** Build a translation transform, with its first and second time derivatives.
 * @param date date of the transform
 * @param translation translation to apply (i.e. coordinates of
 * the transformed origin, or coordinates of the origin of the
 * old frame in the new frame)
 * @param velocity the velocity of the translation (i.e. origin
 * of the old frame velocity in the new frame)
 * @param acceleration the acceleration of the translation (i.e. origin
 * of the old frame acceleration in the new frame)
 */
FrameTransform::FrameTransform(std::string name,
                               const GenericDate& date,
                               const Vector3dExt& translation,
                               const Vector3dExt& velocity,
                               const Vector3dExt& acceleration)
    : date(date), name(name),
    cartesian(CartesianCoordinates(translation, velocity, acceleration)),
    angular(AngularCoordinates::IDENTITY) { }

// ---------------------------------------------------------------------
/** Build a rotation transform.
 * @param date date of the transform
 * @param rotation rotation to apply ( i.e. rotation to apply to the
 * coordinates of a vector expressed in the old frame to obtain the
 * same vector expressed in the new frame )
 * @param rotationRate the axis of the instant rotation
 * @param rotationAcceleration the axis of the instant rotation
 * expressed in the new frame. (norm representing angular rate)
 */
FrameTransform::FrameTransform(std::string name,
                               const GenericDate& date,
                               const RotationQuaternion& rotation,
                               const Vector3dExt& rotationRate,
                               const Vector3dExt& rotationAcceleration)
    : date(date), name(name),
    cartesian(CartesianCoordinates::ZERO),
    angular(AngularCoordinates(rotation, rotationRate, rotationAcceleration)) { }
// ---------------------------------------------------------------------
/** Build a transform by combining two existing ones.
 *
 * @param date date of the transform
 * @param first first transform applied
 * @param second second transform applied
 */
FrameTransform::FrameTransform(const GenericDate& date, FrameTransform& first, FrameTransform& second)
    : date(date), name(first.getName() + "/" + second.getName()),
    cartesian(CartesianCoordinates(compositeTranslation(first, second),
                                     compositeVelocity(first, second),
                                     compositeAcceleration(first, second))),
    angular(AngularCoordinates(compositeRotation(first, second),
                                 compositeRotationRate(first, second),
                                 compositeRotationAcceleration(first, second))) { }
// ---------------------------------------------------------------------
/** Compute a composite translation.
 * @param first first applied transform
 * @param second second applied transform
 * @return translation part of the composite transform
 */
Vector3dExt FrameTransform::compositeTranslation(const FrameTransform& first, const FrameTransform& second) {
    Vector3dExt p1 = first.cartesian.getPosition();
    RotationQuaternion r1 = first.angular.getRotationQuaternion();
    Vector3dExt p2 = second.cartesian.getPosition();
    // RotationQuaternion r2 = second.angular.getRotationQuaternion();
    return p1 + r1.applyInverseTo(p2);
}
// ---------------------------------------------------------------------
/** Compute a composite velocity.
 * @param first first applied transform
 * @param second second applied transform
 * @return velocity part of the composite transform
 */
Vector3dExt FrameTransform::compositeVelocity(const FrameTransform& first, const FrameTransform& second) {
    Vector3dExt v1 = first.cartesian.getVelocity();
    RotationQuaternion q1 = first.angular.getRotationQuaternion();
    Vector3dExt o1 = first.angular.getRotationQuaternionRate();
    Vector3dExt p2 = second.cartesian.getPosition();
    Vector3dExt v2 = second.cartesian.getVelocity();
    Vector3dExt crossP = o1.cross(p2);
    return v1 + (q1.applyInverseTo(v2 + crossP));
}
// ---------------------------------------------------------------------
/** Compute a composite acceleration.
 * @param first first applied transform
 * @param second second applied transform
 * @return acceleration part of the composite transform
 */
Vector3dExt FrameTransform::compositeAcceleration(const FrameTransform& first, const FrameTransform& second) {
    Vector3dExt a1 = first.cartesian.getAcceleration();
    RotationQuaternion q1 = first.angular.getRotationQuaternion();
    Vector3dExt o1 = first.angular.getRotationQuaternionRate();
    Vector3dExt oDot1 = first.angular.getRotationQuaternionAcceleration();
    Vector3dExt p2 = second.cartesian.getPosition();
    Vector3dExt v2 = second.cartesian.getVelocity();
    Vector3dExt a2 = second.cartesian.getAcceleration();
    Vector3dExt crossCrossP = o1.cross(o1.cross(p2));
    Vector3dExt crossV = o1.cross(v2);
    Vector3dExt crossDotP = oDot1.cross(p2);
    return a1 + (q1.applyInverseTo(a2 + 2 * crossV + crossCrossP + crossDotP));
}
// ---------------------------------------------------------------------
/** Compute a composite rotation.
 * @param first first applied transform
 * @param second second applied transform
 * @return rotation part of the composite transform
 */
RotationQuaternion FrameTransform::compositeRotation(const FrameTransform& first, const FrameTransform& second) {
    RotationQuaternion q1 = first.angular.getRotationQuaternion();
    RotationQuaternion q2 = second.angular.getRotationQuaternion();
    return q2.compose(q1);
}
// ---------------------------------------------------------------------
/** Compute a composite rotation rate.
 * @param first first applied transform
 * @param second second applied transform
 * @return rotation rate part of the composite transform
 */
Vector3dExt FrameTransform::compositeRotationRate(const FrameTransform& first, const FrameTransform& second) {
    Vector3dExt o1 = first.angular.getRotationQuaternionRate();
    RotationQuaternion r2 = second.angular.getRotationQuaternion();
    Vector3dExt o2 = second.angular.getRotationQuaternionRate();
    return o2 + (r2.applyTo(o1));
}
// ---------------------------------------------------------------------
/** Compute a composite rotation acceleration.
 * @param first first applied transform
 * @param second second applied transform
 * @return rotation acceleration part of the composite transform
 */
Vector3dExt FrameTransform::compositeRotationAcceleration(const FrameTransform& first, const FrameTransform& second) {
    Vector3dExt o1 = first.angular.getRotationQuaternionRate();
    Vector3dExt oDot1 = first.angular.getRotationQuaternionAcceleration();
    RotationQuaternion r2 = second.angular.getRotationQuaternion();
    Vector3dExt o2 = second.angular.getRotationQuaternionRate();
    Vector3dExt oDot2 = second.angular.getRotationQuaternionAcceleration();
    return (oDot2 + r2.applyTo(oDot1) - o2.cross(r2.applyTo(o1)));
}
// ---------------------------------------------------------------------
FrameTransform FrameTransform::propagate(double dt) const {
    return FrameTransform(name, date.addOffset(dt), cartesian/*.shiftedBy(dt)*/, angular.propagate(dt));
}
// ---------------------------------------------------------------------
/** Get the inverse transform of the instance. */
FrameTransform FrameTransform::getInverse() const {
    RotationQuaternion r = angular.getRotationQuaternion();
    Vector3dExt o = angular.getRotationQuaternionRate();
    Vector3dExt oDot = angular.getRotationQuaternionAcceleration();
    Vector3dExt rp = r.applyTo(cartesian.getPosition());
    Vector3dExt rv = r.applyTo(cartesian.getVelocity());
    Vector3dExt ra = r.applyTo(cartesian.getAcceleration());
    Vector3dExt pInv = -rp;
    Vector3dExt crossP = o.cross(rp);
    Vector3dExt vInv = crossP - rv;
    Vector3dExt crossV = o.cross(rv);
    Vector3dExt crossDotP = oDot.cross(rp);
    Vector3dExt crossCrossP = o.cross(crossP);
    Vector3dExt aInv = -ra + 2 * crossV + crossDotP - crossCrossP;
    return FrameTransform("Inversed_" + name, date, CartesianCoordinates(pInv, vInv, aInv), angular.negate());
}
// ---------------------------------------------------------------------
/** Transform a position vector (including translation effects).
 * @param position vector to transform
 * @return transformed position
 */
Vector3dExt FrameTransform::transformPosition(const Vector3dExt& position) const {
    return angular.getRotationQuaternion().applyTo(cartesian.getPosition() + position);
}
// ---------------------------------------------------------------------
/** Transform including kinematic effects.
 * @param pva the position-velocity-acceleration triplet to transform.
 * @return transformed position-velocity-acceleration
 */
CartesianCoordinates FrameTransform::transform(CartesianCoordinates &pva) const {
    return angular * (CartesianCoordinates(pva + cartesian));
}
// ---------------------------------------------------------------------
/** Transform a vector (no translation effects). */
Vector3dExt FrameTransform::transform(const Vector3dExt& vector) const {
    return angular.getRotationQuaternion().applyTo(vector);
}
// ---------------------------------------------------------------------
/** Compute the Jacobian of the transform.
 *
 * @param jacobian placeholder 6x6 (or larger) matrix to be filled with
 * the Jacobian, only the upper left 6x6 corner will be modified
 */
void FrameTransform::getJacobian(Matrix9& jacobian) {
    // elementary matrix for rotation
    Matrix3d mData = angular.getRotationQuaternion().getMatrix();

    // dP1/dP0
    //    arraycopy(mData[0], 0, jacobian[0], 0, 3);
    //    arraycopy(mData[1], 0, jacobian[1], 0, 3);
    //    arraycopy(mData[2], 0, jacobian[2], 0, 3);
    //    jacobian = mData;

    // dP1/dV0
    // fill(jacobian[0], 3, 6, 0.0);
    // fill(jacobian[1], 3, 6, 0.0);
    // fill(jacobian[2], 3, 6, 0.0);

    // dV1/dP0
    Vector3dExt o = angular.getRotationQuaternionRate();
    double ox = o.getX();
    double oy = o.getY();
    double oz = o.getZ();
    for (int i = 0; i < 3; ++i) {
        jacobian(3, i) = -(oy * mData(2, i) - oz * mData(1, i));
        jacobian(4, i) = -(oz * mData(0, i) - ox * mData(2, i));
        jacobian(5, i) = -(ox * mData(1, i) - oy * mData(0, 0));
    }

    // dV1/dV0
    // arraycopy(mData[0], 0, jacobian[3], 3, 3);
    // arraycopy(mData[1], 0, jacobian[4], 3, 3);
    // arraycopy(mData[2], 0, jacobian[5], 3, 3);
    // jacobian = mData;


    // dP1/dA0
    // fill(jacobian[0], 6, 9, 0.0);
    // fill(jacobian[1], 6, 9, 0.0);
    // fill(jacobian[2], 6, 9, 0.0);

    // dV1/dA0
    // fill(jacobian[3], 6, 9, 0.0);
    // fill(jacobian[4], 6, 9, 0.0);
    // fill(jacobian[5], 6, 9, 0.0);

    // dA1/dP0
    Vector3dExt oDot = angular.getRotationQuaternionAcceleration();
    double oDotx = oDot.getX();
    double oDoty = oDot.getY();
    double oDotz = oDot.getZ();
    for (int i = 0; i < 3; ++i) {
        jacobian(6, i) =
            -(oDoty * mData(2, i) - oDotz * mData(1, i)) - (oy * jacobian(5, i) - oz * jacobian(4, i));
        jacobian(7, i) =
            -(oDotz * mData(0, i) - oDotx * mData(2, i)) - (oz * jacobian(3, i) - ox * jacobian(5, i));
        jacobian(8, i) =
            -(oDotx * mData(1, i) - oDoty * mData(0, i)) - (ox * jacobian(4, i) - oy * jacobian(3, i));
    }

    // dA1/dV0
    for (int i = 0; i < 3; ++i) {
        jacobian(6, i + 3) = -2 * (oy * mData(2, i) - oz * mData(1, i));
        jacobian(7, i + 3) = -2 * (oz * mData(0, i) - ox * mData(2, i));
        jacobian(8, i + 3) = -2 * (ox * mData(1, i) - oy * mData(0, i));
    }

    // dA1/dA0
    // mData[0], 0, jacobian[6], 6, 3;
    // mData[1], 0, jacobian[7], 6, 3;
    // mData[2], 0, jacobian[8], 6, 3;
    //  jacobian= mData;

}
