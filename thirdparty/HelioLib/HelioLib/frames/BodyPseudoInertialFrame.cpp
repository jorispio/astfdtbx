/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BodyPseudoInertialFrame.hpp"

#include <string>

#ifdef WITH_ADVANCED_FRAME
    #include "FramesFactory.hpp"
#endif

//---------------------------------------------------------------------------
BodyPseudoInertialFrame::BodyPseudoInertialFrame(std::string name, int planetid_, double a_m, double a_s)
    :
#ifdef WITH_ADVANCED_FRAME
    Frame(name, reinterpret_cast<Frame*>(FramesFactory::ICRF)),
#else
    Frame(name, nullptr),
#endif
    planetid(planetid_) {
    planetEph = new PlanetAnalyticEphemerides(a_m, a_s);
}
//---------------------------------------------------------------------------
BodyPseudoInertialFrame::~BodyPseudoInertialFrame() {
    if (planetEph)
        delete planetEph;
}

//---------------------------------------------------------------------------
int BodyPseudoInertialFrame::getBody() {
    return planetid;
}
//---------------------------------------------------------------------------
/**
 * Get the transformation from this pseudo inertial frame to parent frame
 * @param date date of the transformation
 * @return a transformation this -> parent frame
 */
//---------------------------------------------------------------------------
FrameTransform BodyPseudoInertialFrame::getTransform(const GenericDate& date) const {
    // position of the planet in parent frame (ICRF)
    Vector3dExt pos = Vector3dExt::Zero();
    Vector3dExt vel = Vector3dExt::Zero();
    planetEph->getPositionVelocity(planetid, (GenericDate&)date, pos, vel);

    // translation from this to ICRF
    // if position of this frame/ICRF is rf, the translation from this frame to ICRF
    // is +rf.
    CartesianCoordinates cartesian = CartesianCoordinates(pos, vel);

    // rotation from this to ICRF
    // Assume the axes of the frame are aligned, same equinox.
    AngularCoordinates angular = AngularCoordinates::IDENTITY;

    // transformation from this to parent frame
    FrameTransform transform =
        FrameTransform(getName() + std::string("-> ICRF"), date, cartesian, AngularCoordinates::IDENTITY);
    return transform;
}
//---------------------------------------------------------------------------
/** Transform a position vector (including translation effects) in Body Inertial Frame to ICRF.
 * @param position vector in Body Centred Frame
 * @return transformed position from Body Inertial Frame to ICRF.
 */
//---------------------------------------------------------------------------
Vector3dExt BodyPseudoInertialFrame::getPosition(const GenericDate& date, const Vector3dExt& position) const {
    FrameTransform transform = getTransform(date);
    return transform.transformPosition(position);
}
