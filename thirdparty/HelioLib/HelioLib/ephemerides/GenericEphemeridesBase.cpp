// $Id: GenericEphemeridesBase.cpp 82 2013-09-09 20:15:39Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "GenericEphemeridesBase.hpp"

#include "core/HelioConstants.hpp"

//---------------------------------------------------------------------------
GenericEphemeridesBase::GenericEphemeridesBase(double am, double as)
    : a_m(am), a_s(as) {

    bodyOrbits.clear();
    /*
        double amu = am * am * am / (as*as);
            BODY temp;
        temp.id = 0; temp.name = "Sun"; temp.mu = MU_SUN / amu; orbits.push_back(temp);
        temp.id = 1; temp.name = "Mercury"; temp.mu = MU_MERCURY / amu; orbits.push_back(temp);
        temp.id = 2; temp.name = "Venus";   temp.mu = MU_VENUS / amu; orbits.push_back(temp);
        temp.id = 3; temp.name = "Earth";   temp.mu = MU_EARTH / amu; orbits.push_back(temp);
        temp.id = 4; temp.name = "Mars";    temp.mu = MU_MARS / amu; orbits.push_back(temp);
        temp.id = 5; temp.name = "Jupiter"; temp.mu = MU_JUPITER / amu; orbits.push_back(temp);
        temp.id = 6; temp.name = "Saturn";  temp.mu = MU_SATURN / amu; orbits.push_back(temp);
        temp.id = 7; temp.name = "Uranus";  temp.mu = MU_URANUS / amu; orbits.push_back(temp);
        temp.id = 8; temp.name = "Neptun";  temp.mu = MU_NEPTUN / amu; orbits.push_back(temp);
        temp.id = 9; temp.name = "Pluto";   temp.mu = MU_PLUTO / amu; orbits.push_back(temp);
        temp.id = 301; temp.name = "Moon";  temp.mu = MU_MOON / amu; orbits.push_back(temp);
        */
}
//---------------------------------------------------------------------------
/**
 * Get the gravitational constant of a celestial body.
 * @param bodyid   id of the planet body
 * @return the gravitational constant of a celestial body.
 */
/*
double GenericEphemeridesBase::getGravitionalParameter(uint bodyid) {
    int len = orbits.size();
    for (int i=0; i<len; ++i) {
        if (orbits.at(i).id == bodyid) {
            return orbits.at(i).mu;
        }
    }

    char str[255];
    snprintf(str, sizeof(str), "Unknown Central body with id=%d\n", bodyid);
    throw HelioLibException(str);
}
*/
//---------------------------------------------------------------------------
/** */
double GenericEphemeridesBase::GetSunRadius() {
    return SUN_RADIUS / a_m;
}

//---------------------------------------------------------------------------
/** */
double GenericEphemeridesBase::GetEarthRadius() {
    return EARTH_RADIUS / a_m;
}
