// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PlanetAnalyticEphemerides.hpp"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include <cstdio>
#include <vector>

#include "core/HelioException.hpp"
#include "maths/MatrixArray.hpp"

using namespace std;
using HelioLib::DATE_FORMAT;
using HelioLib::MJD;
using HelioLib::MatrixArray::MatrixVectorMux;

// #define DEBUG_EPH

//---------------------------------------------------------------------------
#define PI M_PI
#define KM AU

//---------------------------------------------------------------------------
/** Constructor.
 */
//---------------------------------------------------------------------------
PlanetAnalyticEphemerides::PlanetAnalyticEphemerides(double am, double as)
    : GenericEphemeridesBase(am, as) {
    // gravitational constant normalizing coefficient
    double amu = am * am * am / (as * as);  // m^3 / s^2

    BODY temp;
    temp.id = 0;
    temp.name = "Sun";
    temp.mu = MU_SUN / amu;
    temp.radius = SUN_RADIUS / am;
    bodyOrbits.push_back(temp);

    temp.id = 1;
    temp.name = "Mercury";
    temp.mu = MU_MERCURY / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 2;
    temp.name = "Venus";
    temp.mu = MU_VENUS / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 3;
    temp.name = "Earth";
    temp.mu = MU_EARTH / amu;
    temp.radius = EARTH_RADIUS / am;
    bodyOrbits.push_back(temp);

    temp.id = 4;
    temp.name = "Mars";
    temp.mu = MU_MARS / amu;
    temp.radius = MARS_RADIUS / am;
    bodyOrbits.push_back(temp);

    temp.id = 5;
    temp.name = "Jupiter";
    temp.mu = MU_JUPITER / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 6;
    temp.name = "Saturn";
    temp.mu = MU_SATURN / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 7;
    temp.name = "Uranus";
    temp.mu = MU_URANUS / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 8;
    temp.name = "Neptun";
    temp.mu = MU_NEPTUN / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 9;
    temp.name = "Pluto";
    temp.mu = MU_PLUTO / amu;
    temp.radius = 0 / am;
    bodyOrbits.push_back(temp);

    temp.id = 301;
    temp.name = "Moon";
    temp.mu = MU_MOON / amu;
    temp.radius = MOON_RADIUS / am;
    bodyOrbits.push_back(temp);

    muSun = MU_SUN / amu;
    muEarth = MU_EARTH / amu;
}

//---------------------------------------------------------------------------
/** Destructor.
 */
//---------------------------------------------------------------------------
PlanetAnalyticEphemerides::~PlanetAnalyticEphemerides() {
    bodyOrbits.clear();
}

//---------------------------------------------------------------------------
/**
 * @param mu
 * @param radius
 */
//---------------------------------------------------------------------------
bool PlanetAnalyticEphemerides::setPlanetParameters(uint id, double mu, double radius) {
    for (std::vector<BODY>::iterator it = bodyOrbits.begin(); it != bodyOrbits.end(); ++it) {
        if ((*it).id == id) {
            (*it).mu = mu;
            (*it).radius = radius;
        }
    }

    return true;
}
//---------------------------------------------------------------------------
/**
 * @brief get the gravitational constant of a celestial body.
 * @param bodyid   id of the planet body
 * @return the gravitational constant of a celestial body.
 */
//---------------------------------------------------------------------------
double PlanetAnalyticEphemerides::getGravitionalParameter(uint bodyid) {
    int len = bodyOrbits.size();
    for (int i = 0; i < len; ++i) {
        if (bodyOrbits.at(i).id == bodyid) {
            return bodyOrbits.at(i).mu;
        }
    }

    char str[255];
    snprintf(str, sizeof(str), "Unknown Central body with id=%d\n", bodyid);
    throw HelioLibException(str);
}

//---------------------------------------------------------------------------
/**
 * @brief get the gravitational constant of a celestial body.
 * @param bodyid   id of the planet body
 * @return the gravitational constant of a celestial body.
 */
//---------------------------------------------------------------------------
double PlanetAnalyticEphemerides::getRadius(uint bodyid) {
    int len = bodyOrbits.size();
    for (int i = 0; i < len; ++i) {
        if (bodyOrbits.at(i).id == bodyid) {
            return bodyOrbits.at(i).radius;
        }
    }

    char str[128];
    snprintf(str, sizeof(str), "Unknown Central body with id=%d\n", bodyid);
    throw HelioLibException(str);
}
//---------------------------------------------------------------------------
/** */
double PlanetAnalyticEphemerides::GetSunRadius() {
    return getRadius(0);
}
//---------------------------------------------------------------------------
/** */
double PlanetAnalyticEphemerides::GetEarthRadius() {
    return getRadius(3);
}
//---------------------------------------------------------------------------
/** Convert a mean anomaly to an eccentric anomaly */
double PlanetAnalyticEphemerides::convertMeanAnomaly2Eccentric(double M, double e) {
    double tol = 1e-14;
    int iter = 0;
    double err = 1;
    double E;

    E = M + e * cos(M); /* initial guess */
    while ((err > tol) && (iter < 100)) {
        double Enew = E - (E - e * sin(E) - M) / (1 - e * cos(E));
        err = fabs(E - Enew);
        E = Enew;
        iter++;
    }

    if (err > tol) {
        char str[128];
        snprintf(str, sizeof(str), "M=%f ecc=%f, iter=%d, err=%g\n", M, e, iter, err);
        throw HelioLibException("PlanetAnalyticEphemerides::convertMeanAnomaly2Eccentric", "", str);
    }

    return E;
}

// ---------------------------------------------------------------------------
/**
 * The analytical ephemerides of the nine planets of our solar system are
 * returned in rectangular coordinates referred to the ecliptic J2000
 * reference frame. Using analytical ephemerides instead of real ones (for
 * examples JPL ephemerides) is faster but not as accurate, especially for
 * the outer planets
 *
 * Original version:  http://www.esa.int/gsp/ACT/doc/INF/Code/globopt/mga/mga.m
 *
 * @param mjd2000 Days elapsed from 1st of January 2000 counted from midnight.
 *  @param planet  Integer form 1 to 9 containing the number
 *           of the planet (Mercury, Venus, Earth, Mars, Jupiter, Saturn,
 *           Uranus, Neptune, Pluto)
 *
 * @param r vector containing (in km) the position of the planet in the ecliptic J2000
 * @param v vector containing (in km/sec.) the velocity of the planet in the ecliptic J2000
 * @param E Vectors containing the six keplerian parameters, (a,e,i,OM,om,Eccentric Anomaly)
 * --------------------------------------------------------------------------- */
bool PlanetAnalyticEphemerides::getEphemeris(double mjd2000,
                                         int planet,
                                         /* outputs */ double rp[3],
                                         double vp[3],
                                         double Ep[6]) {
    double T, TT, TTT, TTTT, TTTTT;
    double XM;
    int I;
    double E[6];

    if ((planet > 9) || (planet < 0)) {
        char str[128];
        snprintf(str, sizeof(str), "Unknown Planet with id=%d\n", planet);
        throw HelioLibException("PlanetAnalyticEphemerides::getEphemeris", "Planet id must be >= 1, < 9. Use 0 for Sun.\n", str);
    }

    if (planet == 0) {
        // sun
        if (rp) {
            rp[0] = 0; rp[1] = 0; rp[2] = 0;
        }
        if (vp) {
            vp[0] = 0; vp[1] = 0; vp[2] = 0;
        }
        if (Ep) {
            Ep[0] = 0; Ep[1] = 0; Ep[2] = 0; Ep[3] = 0; Ep[4] = 0; Ep[5] = 0;
        }
        return true;
    }

    /* T = JULIAN CENTURIES SINCE 1900 */
    T = (mjd2000 + 36525.00) / 36525.00;
    TT = T * T;
    TTT = T * TT;

    /* CLASSICAL PLANETARY ELEMENTS ESTIMATION IN MEAN ECLIPTIC OF DATE */
    switch (planet) {
    /* MERCURY */
    case 1:
        E[0] = 0.38709860;
        E[1] = 0.205614210 + 0.000020460 * T - 0.000000030 * TT;
        E[2] = 7.002880555555555560 + 1.86083333333333333e-3 * T - 1.83333333333333333e-5 * TT;
        E[3] = 4.71459444444444444e+1 + 1.185208333333333330 * T + 1.73888888888888889e-4 * TT;
        E[4] = 2.87537527777777778e+1 + 3.70280555555555556e-1 * T + 1.20833333333333333e-4 * TT;
        XM = 1.49472515288888889e+5 + 6.38888888888888889e-6 * T;
        E[5] = 1.02279380555555556e2 + XM * T;
        break;
    /*  VENUS */
    case 2:
        E[0] = 0.72333160;
        E[1] = 0.006820690 - 0.000047740 * T + 0.0000000910 * TT;
        E[2] = 3.393630555555555560 + 1.00583333333333333e-3 * T - 9.72222222222222222e-7 * TT;
        E[3] = 7.57796472222222222e+1 + 8.9985e-1 * T + 4.1e-4 * TT;
        E[4] = 5.43841861111111111e+1 + 5.08186111111111111e-1 * T - 1.38638888888888889e-3 * TT;
        XM = 5.8517803875e+4 + 1.28605555555555556e-3 * T;
        E[5] = 2.12603219444444444e2 + XM * T;
        break;
    /*  EARTH */
    case 3:
        E[0] = 1.000000230;
        E[1] = 0.016751040 - 0.000041800 * T - 0.0000001260 * TT;
        E[2] = 0.00;
        E[3] = 0.00;
        E[4] = 1.01220833333333333e+2 + 1.7191750 * T + 4.52777777777777778e-4 * TT + 3.33333333333333333e-6 * TTT;
        XM = 3.599904975e+4 - 1.50277777777777778e-4 * T - 3.33333333333333333e-6 * TT;
        E[5] = 3.58475844444444444e2 + XM * T;
        break;
    /* MARS */
    case 4:
        E[0] = 1.5236883990;
        E[1] = 0.093312900 + 0.0000920640 * T - 0.0000000770 * TT;
        E[2] = 1.850333333333333330 - 6.75e-4 * T + 1.26111111111111111e-5 * TT;
        E[3] = 4.87864416666666667e+1 + 7.70991666666666667e-1 * T - 1.38888888888888889e-6 * TT -
               5.33333333333333333e-6 * TTT;
        E[4] = 2.85431761111111111e+2 + 1.069766666666666670 * T + 1.3125e-4 * TT + 4.13888888888888889e-6 * TTT;
        XM = 1.91398585e+4 + 1.80805555555555556e-4 * T + 1.19444444444444444e-6 * TT;
        E[5] = 3.19529425e2 + XM * T;
        break;
    /* JUPITER */
    case 5:
        E[0] = 5.2025610;
        E[1] = 0.048334750 + 0.000164180 * T - 0.00000046760 * TT - 0.00000000170 * TTT;
        E[2] = 1.308736111111111110 - 5.69611111111111111e-3 * T + 3.88888888888888889e-6 * TT;
        E[3] = 9.94433861111111111e+1 + 1.010530 * T + 3.52222222222222222e-4 * TT - 8.51111111111111111e-6 * TTT;
        E[4] = 2.73277541666666667e+2 + 5.99431666666666667e-1 * T + 7.0405e-4 * TT + 5.07777777777777778e-6 * TTT;
        XM = 3.03469202388888889e+3 - 7.21588888888888889e-4 * T + 1.78444444444444444e-6 * TT;
        E[5] = 2.25328327777777778e2 + XM * T;
        break;
    /* SATURN */
    case 6:
        E[0] = 9.5547470;
        E[1] = 0.055892320 - 0.00034550 * T - 0.0000007280 * TT + 0.000000000740 * TTT;
        E[2] = 2.492519444444444440 - 3.91888888888888889e-3 * T - 1.54888888888888889e-5 * TT +
               4.44444444444444444e-8 * TTT;
        E[3] = 1.12790388888888889e+2 + 8.73195138888888889e-1 * T - 1.52180555555555556e-4 * TT -
               5.30555555555555556e-6 * TTT;
        E[4] = 3.38307772222222222e+2 + 1.085220694444444440 * T + 9.78541666666666667e-4 * TT +
               9.91666666666666667e-6 * TTT;
        XM = 1.22155146777777778e+3 - 5.01819444444444444e-4 * T - 5.19444444444444444e-6 * TT;
        E[5] = 1.75466216666666667e2 + XM * T;
        break;
    /* URANUS */
    case 7:
        E[0] = 19.218140;
        E[1] = 0.04634440 - 0.000026580 * T + 0.0000000770 * TT;
        E[2] = 7.72463888888888889e-1 + 6.25277777777777778e-4 * T + 3.95e-5 * TT;
        E[3] = 7.34770972222222222e+1 + 4.98667777777777778e-1 * T + 1.31166666666666667e-3 * TT;
        E[4] = 9.80715527777777778e+1 + 9.85765e-1 * T - 1.07447222222222222e-3 * TT - 6.05555555555555556e-7 * TTT;
        XM = 4.28379113055555556e+2 + 7.88444444444444444e-5 * T + 1.11111111111111111e-9 * TT;
        E[5] = 7.26488194444444444e1 + XM * T;
        break;
    /* NEPTUNE */
    case 8:
        E[0] = 30.109570;
        E[1] = 0.008997040 + 0.0000063300 * T - 0.0000000020 * TT;
        E[2] = 1.779241666666666670 - 9.54361111111111111e-3 * T - 9.11111111111111111e-6 * TT;
        E[3] = 1.30681358333333333e+2 + 1.0989350 * T + 2.49866666666666667e-4 * TT - 4.71777777777777778e-6 * TTT;
        E[4] = 2.76045966666666667e+2 + 3.25639444444444444e-1 * T + 1.4095e-4 * TT + 4.11333333333333333e-6 * TTT;
        XM = 2.18461339722222222e+2 - 7.03333333333333333e-5 * T;
        E[5] = 3.77306694444444444e1 + XM * T;
        break;
    /*PLUTO */
    case 9:
        T = mjd2000 / 36525;
        TT = T * T;
        TTT = TT * T;
        TTTT = TTT * T;
        TTTTT = TTTT * T;
        E[0] = 39.34041961252520 + 4.33305138120726 * T - 22.93749932403733 * TT + 48.76336720791873 * TTT -
               45.52494862462379 * TTTT + 15.55134951783384 * TTTTT;
        E[1] = 0.24617365396517 + 0.09198001742190 * T - 0.57262288991447 * TT + 1.39163022881098 * TTT -
               1.46948451587683 * TTTT + 0.56164158721620 * TTTTT;
        E[2] = 17.16690003784702 - 0.49770248790479 * T + 2.73751901890829 * TT - 6.26973695197547 * TTT +
               6.36276927397430 * TTTT - 2.37006911673031 * TTTTT;
        E[3] = 110.222019291707 + 1.551579150048 * T - 9.701771291171 * TT + 25.730756810615 * TTT -
               30.140401383522 * TTTT + 12.796598193159 * TTTTT;
        E[4] = 113.368933916592 + 9.436835192183 * T - 35.762300003726 * TT + 48.966118351549 * TTT -
               19.384576636609 * TTTT - 3.362714022614 * TTTTT;
        E[5] = 15.17008631634665 + 137.023166578486 * T + 28.362805871736 * TT - 29.677368415909 * TTT -
               3.585159909117 * TTTT + 13.406844652829 * TTTTT;
        break;
    }

    /* CONVERSION OF AU INTO KM, DEG INTO RAD */
    E[0] = E[0] * KM / a_m;
    for (I = 2; I < 6; I++)
        E[I] = E[I] * DEG2RAD;

#ifdef DEBUG_EPH
    printf("E = %f %f %f %f %f %f\n", E[0], E[1], E[2], E[3], E[4], E[5]);
#endif

    E[5] = (E[5] / (2 * PI) - static_cast<int>(E[5] / (2 * PI))) * 2 * PI;
    E[5] = convertMeanAnomaly2Eccentric(E[5], E[1]);

    /* Position and Velocity in the J2000 inertial system */
    convertOrbit(E, muSun, rp, vp);
    if (Ep != NULL) {
        memcpy(Ep, E, 6 * sizeof(double));
    }

    return true;
}

// -------------------------------------------------------------------------
/** Venus ephemeris
 * @param t  date in days
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
void PlanetAnalyticEphemerides::getVenusEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]) {
    double E[6];
    getEphemeris(mjd2000, 2, r, v, E);
}
// -------------------------------------------------------------------------
/** Earth ephemeris
 * @param t  date in days
 * @param fmt date scale for t
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
void PlanetAnalyticEphemerides::getEarthEphemeris(double t, DATE_FORMAT fmt, /* outputs */ double r[3], double v[3]) {
    double mjd2000 = t;
    if (fmt == MJD) {
        mjd2000 = ((t * a_s) - MJD2000_MJD_OFFSET) / a_s;
    }
    getEarthEphemeris(mjd2000, r, v);
}

// -------------------------------------------------------------------------
/** Earth ephemeris
 * @param t  date in days
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
void PlanetAnalyticEphemerides::getEarthEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]) {
    double E[6];
    getEphemeris(mjd2000, 3, r, v, E);
}
// -------------------------------------------------------------------------
/** Mars ephemeris
 * @param t  date in days
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
void PlanetAnalyticEphemerides::getMarsEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]) {
    double E[6];
    getEphemeris(mjd2000, 4, r, v, E);
}

// -------------------------------------------------------------------------
/** Moon ephemeris
 * @param t  date in days
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
void PlanetAnalyticEphemerides::getMoonEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]) {
    // http://ssd.jpl.nasa.gov/?sat_elem
    // Epoch 2000 Jan. 1.50 TT
    double E[6];
    E[0] = 384399 * 1000 / a_m;
    E[1] = 0.0549006;
    E[2] = 5.145 * DEG2RAD;   // inclination to ecliptic
    E[3] = 125.08 * DEG2RAD;  // ascending node (\Omega)
    E[4] = 318.15 * DEG2RAD;  // argument of pericenter (\omega)
    E[5] = 135.27 * DEG2RAD;  // mean anomaly

    double n = 13.176358 * DEG2RAD / a_s;  // in rad/day

    E[5] = E[5] + n * mjd2000;
    convertOrbit(E, muEarth, r, v);
}

// -------------------------------------------------------------------------
/**
 * @param E  orbit bulletin
 * @param mjd2000  date, MJD2000
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
// -------------------------------------------------------------------------
void PlanetAnalyticEphemerides::ast_eph_an(const double E_[7], double mjd2000, /* outputs */ double r[3], double v[3]) {
    // FIXME(joris)  scale of E has to be defined
    double E[7];
    memcpy(E, E_, 6 * sizeof(double));

    //
    E[0] = E[0] / a_m;

    double n = sqrt(muSun / (E[0] * E[0] * E[0]));
    double M = E[5] + n * (mjd2000 - E[6]) * 86400.;

    M = (M / (2 * PI) - static_cast<int>(M / (2 * PI))) * 2 * PI;
    E[5] = convertMeanAnomaly2Eccentric(M, E[1]);
    convertOrbit(E, muSun, r, v);

    double a_ms = a_m / a_s;
    r[0] *= a_m;
    r[1] *= a_m;
    r[2] *= a_m;
    v[0] *= a_ms;
    v[1] *= a_ms;
    v[2] *= a_ms;

    // printf("n = %f\n", n*180./M_PI*86400.);
}

// -------------------------------------------------------------------------
/**
 * @param mjd2000  date, MJD2000
 * @param r  position vector, m
 * @param v  velocity vector, m/s
 */
// -------------------------------------------------------------------------
void PlanetAnalyticEphemerides::getBodyEphemeris(const BODY body, double mjd2000, /* outputs */ double r[3], double v[3]) {
    double mu = getGravitionalParameter(body.centralBody);
    double sma = body.orbitParam[0] / a_m;
    double n = sqrt(mu / (sma * sma * sma));

    double M0 = body.orbitParam[5];
    double refEpoch = body.orbitEpoch - MJD2000;  // conversion from mjd to mjd2000: MJD2000 = MJD - 51544.5.
    double M = M0 + n * (mjd2000 - refEpoch) * 86400.;

    double E[6];
    for (int i = 0; i < 6; ++i)
        E[i] = body.orbitParam[i];
    E[5] = convertMeanAnomaly2Eccentric(M, E[1]);
    E[0] = E[0] / a_m;

    convertOrbit(E, 1, r, v);

    // FIXME should we scale back. In which scale is body?
    double a_ms = a_m / a_s;
    r[0] *= a_m;
    r[1] *= a_m;
    r[2] *= a_m;
    v[0] *= a_ms;
    v[1] *= a_ms;
    v[2] *= a_ms;
}
// --------------------------------------------------------------------------
/** Convert orbital elements (a, e, i, gom, pom, E) into cartesian coordinates.
 * inputs are in radians for the angle.
 * The units of mu and a must be consistent. r and a are thus in the same unit.
 */
void PlanetAnalyticEphemerides::convertOrbit(const double E[6], double mu, /*outputs*/ double r[3], double v[3]) {
    double a = E[0];
    double e = E[1];
    double i = E[2];
    double omg = E[3];
    double omp = E[4];
    double EA = E[5];

    double b = a * sqrt(1 - e * e);
    double n = sqrt(mu / (a * a * a));
    double xper = a * (cos(EA) - e);
    double yper = b * sin(EA);
    double xdotper = -(a * n * sin(EA)) / (1 - e * cos(EA));
    double ydotper = (b * n * cos(EA)) / (1 - e * cos(EA));

    /* transformation matrix in ECI */
    double R[9];
    int sz[] = { 3, 3 };
    MATRIX(R, 0, 0, sz) = cos(omg) * cos(omp) - sin(omg) * sin(omp) * cos(i);
    MATRIX(R, 0, 1, sz) = -cos(omg) * sin(omp) - sin(omg) * cos(omp) * cos(i);
    MATRIX(R, 0, 2, sz) = sin(omg) * sin(i);
    MATRIX(R, 1, 0, sz) = sin(omg) * cos(omp) + cos(omg) * sin(omp) * cos(i);
    MATRIX(R, 1, 1, sz) = -sin(omg) * sin(omp) + cos(omg) * cos(omp) * cos(i);
    MATRIX(R, 1, 2, sz) = -cos(omg) * sin(i);
    MATRIX(R, 2, 0, sz) = sin(omp) * sin(i);
    MATRIX(R, 2, 1, sz) = cos(omp) * sin(i);
    MATRIX(R, 2, 2, sz) = cos(i);

    double Ri[3];
    if (r != NULL) {
        Ri[0] = xper;
        Ri[1] = yper;
        Ri[2] = 0;
        MatrixVectorMux(R, Ri, r);
    }

    if (v != NULL) {
        Ri[0] = xdotper;
        Ri[1] = ydotper;
        Ri[2] = 0;
        MatrixVectorMux(R, Ri, v);
    }
}
//---------------------------------------------------------------------------
std::string PlanetAnalyticEphemerides::getName(uint bodyid) {
    int len = bodyOrbits.size();
    for (int i = 0; i < len; ++i) {
        if (bodyOrbits.at(i).id == bodyid) {
            return std::string(bodyOrbits.at(i).name);
        }
    }

    char str[128];
    snprintf(str, sizeof(str), "Unknown Planet with id=%d\n", bodyid);
    throw HelioLibException(str);
}

//---------------------------------------------------------------------------
/** Compute the position and velocity of a planetary body bodyid at the given
 * date.
 * @param bodyid  body identifier from 1 (MERCURY) to 9 (PLUTO)
 * @param date  date
 * @param bodyPosition position vector
 * @param bodyVelocity velocity vector
 */
bool PlanetAnalyticEphemerides::getPositionVelocity(int bodyid,
                                                    GenericDate& date,
                                                    Vector3dExt& bodyPosition,
                                                    Vector3dExt& bodyVelocity) {
    double rp[3];
    double vp[3];
    if (!getEphemeris(date.getMJD2000(), bodyid, rp, vp, NULL)) {
        return false;
    }

    bodyPosition(0) = rp[0];
    bodyPosition(1) = rp[1];
    bodyPosition(2) = rp[2];
    bodyVelocity(0) = vp[0];
    bodyVelocity(1) = vp[1];
    bodyVelocity(2) = vp[2];
    return true;
}
