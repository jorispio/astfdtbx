// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_EPHEMERIDES_BASE_HPP_
#define __SMTBX_EPHEMERIDES_BASE_HPP_

#include "date/GenericDate.hpp"
#include "core/HelioDataTypes.hpp"
#include "maths/Vector3dExt.hpp"

/** THis class allows building a database of objects defined with a name, id, orbital parameters, and other properties.
 */
class GenericEphemeridesBase {
 protected:
    double a_m;
    double a_s;

    typedef struct BODY {
        uint id;
        std::string name;
        double centralBody;
        double mu;
        double radius;
        double orbitEpoch;
        double orbitParam[6];
    } BODY;

    std::vector<BODY> bodyOrbits;

 public:
    GenericEphemeridesBase(double am, double as);
    virtual ~GenericEphemeridesBase() { }

    /** */
    double GetSunRadius();

    /** */
    double GetEarthRadius();

    /** */
    virtual bool
    getPositionVelocity(int bodyid, GenericDate& date, Vector3dExt& bodyPosition, Vector3dExt& bodyVelocity) = 0;
};

#endif  // __SMTBX_EPHEMERIDES_BASE_HPP_
