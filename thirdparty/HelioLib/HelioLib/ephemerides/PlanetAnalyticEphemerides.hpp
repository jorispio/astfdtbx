/* $Id$ */
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_PLANET_ANALYTIC_EPHEMERIDES_HPP_
#define __SMTBX_PLANET_ANALYTIC_EPHEMERIDES_HPP_
//---------------------------------------------------------------------------
#include <math.h>
#include <stdlib.h>

#include <iostream>
#include <cstdio>
#include <string>

#include "core/HelioConstants.hpp"
#include "core/HelioDataTypes.hpp"
#include "maths/Vector3dExt.hpp"
#include "GenericEphemeridesBase.hpp"

enum CelestialBodyId {
    MERCURY = 1,
    VENUS = 3,
    EARTH = 4,
    MARS = 5,
    JUPITER = 6,
    URANUS = 7,
    NEPTUN = 8,
    PLUTO = 9,
    SUN = 0
};

/* ------------------------------------------------------------------------- */
class PlanetAnalyticEphemerides : public GenericEphemeridesBase {
private:
    /** Sun gravitational constant. */
    double muSun;
    /** Earth gravitational constant. */
    double muEarth;

    double convertMeanAnomaly2Eccentric(double M, double e);
    void convertOrbit(const double E[6], double mu, /*outputs*/ double r[3], double v[3]);

    void getBodyEphemeris(const BODY body, double mjd2000, /* outputs */ double r[3], double v[3]);

 public:
    /** Simple creator */
    PlanetAnalyticEphemerides(double am, double as);

    /** Destructor */
    ~PlanetAnalyticEphemerides();

    /** */
    std::string getName(uint bodyid);

    /** set parameters of the planet body */
    bool setPlanetParameters(uint id, double mu, double rad);

    /** get the gravitational parameter of the planet body */
    double getGravitionalParameter(uint bodyid);

    /** get the radius of the planet body */
    double getRadius(uint bodyid);

    /** */
    double GetSunRadius();

    /** */
    double GetEarthRadius();

    /** get Position and Velocity vectors */
    bool getPositionVelocity(int bodyid, GenericDate& date, Vector3dExt& bodyPosition, Vector3dExt& bodyVelocity);

    /** */
    bool getEphemeris(double mjd2000,
                  int planet,
                  /* outputs */ double r[3],
                  double v[3],
                  double E[6]);

    void getVenusEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]);

    void getEarthEphemeris(double t, HelioLib::DATE_FORMAT fmt, /* outputs */ double r[3], double v[3]);
    void getEarthEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]);

    void getMarsEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]);

    void getMoonEphemeris(double mjd2000, /* outputs */ double r[3], double v[3]);

    /** generic convenience method */
    void ast_eph_an(const double E[7], double mjd2000, /* outputs */ double r[3], double v[3]);
};

/* ------------------------------------------------------------------------- */
#endif  // __SMTBX_PLANET_ANALYTIC_EPHEMERIDES_HPP_
