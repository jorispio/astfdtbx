// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "orbit/HaloOrbit.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("Compute Initial conditions for Halo Orbit", "[single-file]" ) {
    // example for the Earth-Moon system, Ax=384km, near L1
    //     findHaloOrbit(0.01215058147718, 0.001, 1)
    //         with DU = 384 400 km
    //              T = 3.7519*1e5 s
    double mu_ratio = 0.01215058147718;
    double Az = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    double xguess[7];
    double Tguess = 1;
    bool res = haloOrbit->get_initial_conditions(Az, xguess, Tguess);

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
            xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif
    delete problem;
    delete haloOrbit;

    double tol = 1e-6;
    REQUIRE(res == true);
    REQUIRE(fabs(Tguess - 2.74496695) < tol);
    REQUIRE(fabs(xguess[0] - 0.82387106) < tol);
    REQUIRE(fabs(xguess[1]) < tol);
    REQUIRE(fabs(xguess[2] - 0.02140192) < tol);
    REQUIRE(fabs(xguess[3]) < tol);
    REQUIRE(fabs(xguess[4] - 0.13245336) < tol);
    REQUIRE(fabs(xguess[5]) < tol);
}

TEST_CASE("Compute Initial conditions for Halo Orbit for small amplitude", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Az = 0.05;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    double xguess[7];
    double Tguess = 1;
    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    bool res = haloOrbit->get_initial_conditions(Az, xguess, Tguess);
    delete problem;
    delete haloOrbit;

    REQUIRE(res == true);
}

TEST_CASE("Compute the monodromy matrix", "[single-file]") {
    double mu_ratio = 0.01215058147718;
    double Az = 0.05;
    double tol = 1e-6;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    OrbitSolution orbitProperty = haloOrbit->find(Az, false, 5, 1e-6, 1.);
    REQUIRE(orbitProperty.is_valid);

    Propagator *propagator = new Propagator(librationPoint.mu);
    double tspan[2] = {0, orbitProperty.period };
    double x[7] = {orbitProperty.r0(0), orbitProperty.r0(1), orbitProperty.r0(2),
                    orbitProperty.v0(0), orbitProperty.v0(1), orbitProperty.v0(2), 1};
    Matrix7 phi77 = propagator->propagate_state_transition_matrix(x, tspan, -1);

    delete problem;
    delete haloOrbit;
    delete propagator;

    Matrix6 e = orbitProperty.monodromy - phi77.block(0, 0, 6, 6);
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}
