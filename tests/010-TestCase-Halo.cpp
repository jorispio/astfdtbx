// $Id$
// ---------------------------------------------------------------------------
// Let Catch provide main()
#define CATCH_CONFIG_MAIN

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "orbit/HaloOrbit.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("Compute and Propagate Halo", "[single-file]" ) {
    // Example for the Earth-Moon system, Ax=384km, near L1
    //    findHaloOrbit(0.01215058147718, 0.001, 1)
    //         with DU = 384 400 km
    //              T = 3.7519*1e5 s
    double mu_ratio = 0.01215058147718;
    double Az = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    OrbitSolution haloOrbitSolution = haloOrbit->find(Az, false, 5, 1e-6, 1e-6, 1.);

    #ifdef PRINT_DEBUG
    printf("Libration Point%d. Pos = %f %f %f\n",
                librationPoint.id, librationPoint.L[0], librationPoint.L[1], librationPoint.L[2]);
    printf("T = %f\n", haloOrbitSolution.period );
    printf("Orbit amplitudes: %f %f %f\n", haloOrbitSolution.Ax, haloOrbitSolution.Ay, haloOrbitSolution.Az);

    std::cout << "pos = " << haloOrbitSolution.r0.transpose() << std::endl;
    std::cout << "vel = " << haloOrbitSolution.v0.transpose() << std::endl;
    #endif

    delete problem;
    delete haloOrbit;

    double tol = 1e-4;
    REQUIRE(haloOrbitSolution.is_valid);
    REQUIRE(fabs(haloOrbitSolution.r0(0) - 0.823384) < tol);
    REQUIRE(fabs(haloOrbitSolution.r0(1)) < tol);
    REQUIRE(fabs(haloOrbitSolution.r0(2) - 0.0214019) < tol);
    REQUIRE(fabs(haloOrbitSolution.v0(0)) < tol);
    REQUIRE(fabs(haloOrbitSolution.v0(1) - 0.133606) < tol);
    REQUIRE(fabs(haloOrbitSolution.v0(2)) < tol);
}


TEST_CASE("Find Halo Orbit for ISEE3", "[single-file]" ) {
    // International Sun-Earth Explorer 3 (ISEE3) Halo orbit around Earth-Moon L1
    double Az = 110000 * 1e3;     // m
    double Axref = 206000 * 1e3;  // m
    //double Ayref = 665000 * 1e3;  // m

    R3bProblem *problem = new R3bProblem(MU_SUN, MU_EARTH, DISTANCE_SUN_EARTH);
    double a_m = problem->getNormalisationForDistance();

    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    OrbitSolution orbitProperty = haloOrbit->find(Az / a_m, false, 10, 1e-6, 1e-6, 1.);

#ifdef PRINT_DEBUG
    printf("Az=%.8f  ~  %.8f\n", orbitProperty.Az, Az / a_m);
    printf("Ax=%.8f  ~  %.8f\n", orbitProperty.Ax, Axref / a_m);
    printf("Ay=%.8f  ~  %.8f\n", orbitProperty.Ay, Ayref / a_m);
#endif
    delete problem;
    delete haloOrbit;

    double tol = 1e-5;
    REQUIRE(orbitProperty.is_valid);
    REQUIRE(fabs(orbitProperty.Az - Az / a_m) < tol);
    REQUIRE(fabs(orbitProperty.Ax - Axref / a_m) < tol);
    REQUIRE(fabs(orbitProperty.Ay) < tol);
}


TEST_CASE("Find Halo Orbit", "[single-file]" ) {
    // Example for the Earth-Moon system, Ax=384km, near L1
    //    findHaloOrbit(0.01215058147718, 0.001, 1)
    //         with DU = 384 400 km
    //              T = 3.7519*1e5 s
    double mu_ratio = 0.01215058147718;
    double Az = 0.05;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    OrbitSolution orbitProperty = haloOrbit->find(Az, false, 7, 1e-6, 1e-6, 1.);

#ifdef PRINT_DEBUG
    printf("Halo orbit period T = %.8f\n", orbitProperty.period );
    std::cout << "  pos = " << orbitProperty.r0.transpose() << std::endl;
    std::cout << "  vel = " << orbitProperty.v0.transpose() << std::endl;
#endif
    delete problem;
    delete haloOrbit;

    double tol = 1e-6;
    REQUIRE(orbitProperty.is_valid);
    REQUIRE(fabs(orbitProperty.period  - 2.76110230) < tol);
}
