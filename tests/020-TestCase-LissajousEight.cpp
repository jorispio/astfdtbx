// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "orbit/special/LissajousEight.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("Compute Initial conditions for Lissajous Eight Orbit", "[single-file]" ) {
    // example for the Earth-Moon system, Ax=384km, near L1
    //     findHaloOrbit(0.01215058147718, 0.001, 1)
    //         with DU = 384 400 km
    //              T = 3.7519*1e5 s
    double mu_ratio = 0.01215058147718;
    double Az = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    delete problem;

    double xguess[7];
    double Tguess = 1;
    LissajousEight *lissajous = new LissajousEight(librationPoint);
    lissajous->HaloOrbit::get_initial_conditions(Az, xguess, Tguess);
    delete lissajous;

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
                xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif

    double tol = 1e-6;
    REQUIRE(fabs(Tguess - 5.48993390) < tol);
    REQUIRE(fabs(xguess[0] - 0.85655523) < tol);
    REQUIRE(fabs(xguess[1] - 0) < tol);
    REQUIRE(fabs(xguess[2] + 0.01879656) < tol);
    REQUIRE(fabs(xguess[3] - 0) < tol);
    REQUIRE(fabs(xguess[4] + 0.14154310) < tol);
    REQUIRE(fabs(xguess[5] - 0) < tol);
}

TEST_CASE("Find Lissajous Eight Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Az = 0.01;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    delete problem;

    LissajousEight *lissajous = new LissajousEight(librationPoint);
    OrbitSolution orbitProperty = lissajous->find(Az, false, 100, 1e-6, 1.);
    delete lissajous;

#ifdef PRINT_DEBUG
    printf("Lissajous Eight orbit period T = %f\n", orbitProperty.period );
    std::cout << "  pos = " << orbitProperty.r0.transpose() << std::endl;
    std::cout << "  vel = " << orbitProperty.v0.transpose() << std::endl;
#endif

    double tol = 1e-6;
    REQUIRE(orbitProperty.is_valid);
    REQUIRE(fabs(orbitProperty.period  - 6.332142) < tol);
}

