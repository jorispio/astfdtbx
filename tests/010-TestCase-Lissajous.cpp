// $Id$
// ---------------------------------------------------------------------------
#define CATCH_CONFIG_MAIN

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "lissajous/MultiPointLissajousOrbit.hpp"
#include "orbit/HaloOrbit.hpp"
#include "orbit/LissajousOrbit.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

// #define PRINT_DEBUG

TEST_CASE("Compute Initial conditions for Lissajous Orbit at t=0", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;
    double phix = 0;
    double Az = 0.01;
    double phiz = M_PI / 2.;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LissajousOrbit *lissajous = new LissajousOrbit(librationPoint);
    lissajous->set_parameters(Ax, phix, Az, phiz);
    double xguess[7];
    lissajous->interpolate_initial_conditions(0., xguess);
    double Tguess = lissajous->get_analytical_period();

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
                xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif

    delete problem;
    delete lissajous;

    double tol = 1e-6;
    REQUIRE(fabs(Tguess - 2.7594474498) < tol);
    REQUIRE(fabs(xguess[0] - 0.8399338316) < tol);
    REQUIRE(fabs(xguess[1] - 0) < tol);
    REQUIRE(fabs(xguess[2] - 0) < tol);
    REQUIRE(fabs(xguess[3] - 0) < tol);
    REQUIRE(fabs(xguess[4] + 0.0248736533) < tol);
    REQUIRE(fabs(xguess[5] + 0.00333586) < tol);
}

TEST_CASE("Compute n-segment analytical solution for Lissajous Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;
    double phix = 0;
    double Az = 0.01;
    double phiz = M_PI / 2.;
    int n_segments = 10;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LissajousOrbit *lissajousOrbit = new LissajousOrbit(librationPoint);
    lissajousOrbit->set_parameters(Ax, phix, Az, phiz);

    Lissajous *lissajous = new Lissajous(librationPoint);
    std::vector<Segment> segments = lissajous->get_analytical_segments(lissajousOrbit, n_segments);

    delete problem;
    delete lissajous;

    REQUIRE(segments.size() == 10);
}

TEST_CASE("Compute n-segment analytical solution for Halo Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Az = 0.01;
    double phiz = 0.;
    int n_segments = 10;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    haloOrbit->set_parameter(Az, phiz);

    Lissajous *lissajous = new Lissajous(librationPoint);
    std::vector<Segment> segments = lissajous->get_analytical_segments(haloOrbit, n_segments);

    delete problem;
    delete lissajous;
    delete haloOrbit;

    REQUIRE(segments.size() == 10);
}


TEST_CASE("Find Halo Orbit using Lissajous solver", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Az = 0.01;
    int n_segments = 3;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    Lissajous *lissajous = new Lissajous(librationPoint);

    HaloOrbit *haloOrbit = new HaloOrbit(librationPoint, NORTHERN);
    haloOrbit->set_parameter(Az);
    std::vector<Segment> segments = lissajous->get_analytical_segments(haloOrbit, n_segments);

    lissajous->find(segments, true, 10);
    OrbitSolution orbitProperty = lissajous->get_orbit_property();

    delete problem;
    delete lissajous;
    delete haloOrbit;

    double tol = 1e-6;
    REQUIRE(fabs(orbitProperty.period  - 2.7426159071) < tol);
}


TEST_CASE("Find Lissajous Orbit using Analytical Approximation", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;
    double phix = 0;
    double Az = 0.01;
    double phiz = M_PI / 2.;
    int n_segments = 20;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    Lissajous *lissajous = new Lissajous(librationPoint);

    LissajousOrbit *lissajousOrbit = new LissajousOrbit(librationPoint);
    lissajousOrbit->set_parameters(Ax, phix, Az, phiz);
    std::vector<Segment> segments = lissajous->get_analytical_segments(lissajousOrbit, n_segments);

    lissajous->find(segments, true, 10);
    OrbitSolution orbitProperty = lissajous->get_orbit_property();

    delete problem;
    delete lissajous;

    double tol = 1e-6;
    REQUIRE(fabs(orbitProperty.period  - 2.7594474498) < tol);
}
