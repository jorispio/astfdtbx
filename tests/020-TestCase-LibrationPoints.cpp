// $Id$
// ---------------------------------------------------------------------------
#include <stdio.h>

#include <catch2/catch.hpp>
#include "Jacobi.hpp"
#include "R3BProblem.hpp"

#define EARTH_MU 3.9860064e+14
#define MOON_MU  4.9027989e+12


TEST_CASE("Check mu", "[single-file]" ) {
    double m1 = 1;
    double m2 = 0.001;
    double mu_ratio = libration_point::computeGravitationalConstantRatio(m1, m2);
    double tol = 1e-6;
    REQUIRE(fabs(mu_ratio - 0.001) < tol);
}

TEST_CASE("Check Jacobi constant", "[single-file]" ) {
    double tol = 1e-6;
    double p1 = jacobi::JacobiC(1., 0.1, 0.2, 0.3, 0.0, 0.0, 0.0);
    REQUIRE(fabs(p1 + 5.4852248382) < tol);
}

TEST_CASE("Check Libration points", "[single-file]" ) {
    double L[5][2], Cj[5];
    double mu = 0.01215058147718;  // Earth-Moon
    double Lref[][3] = {{ 0.832579098877, 0.0},
                        { 1.150481712172, 0.0},
                        {-1.005062644785, 0.0},
                        { 0.487849418523, 0.866025403784},
                        { 0.487849418523, -0.866025403784}};
    double Cjref[] = {3.184126918757, 3.200327694059, 3.024150450561};
    double tol = 1e-6;

    libration_point::computeLibrationPoints(mu, L, Cj);

    for (int i = 0; i < 5; ++i) {
        REQUIRE(fabs(L[i][0] - Lref[i][0]) < tol);
        REQUIRE(fabs(L[i][1] - Lref[i][1]) < tol);
    }

    REQUIRE(fabs(Cj[0] - Cjref[0]) < tol);
    REQUIRE(fabs(Cj[1] - Cjref[1]) < tol);
    REQUIRE(fabs(Cj[2] - Cjref[2]) < tol);
}

TEST_CASE("Check mu_ratio", "[single-file]" ) {
    double tol = 1e-6;
    R3bProblem *problem = new R3bProblem(0.1, EARTH_MU, 1, MOON_MU);
    REQUIRE(fabs(problem->get_mu_ratio() - 0.012150575255) < tol);
    delete problem;
}

TEST_CASE("Check gamma", "[single-file]" ) {
    double mu = 0.01215058147718;
    double tol = 1e-6;
    R3bProblem *problem = new R3bProblem(mu);

    LibrationPointProperty librationPoint1 = problem->get_libration_point_info(LibrationPointId::L1);
    REQUIRE(fabs(librationPoint1.gamma() - 0.1509342724) < tol);

    LibrationPointProperty librationPoint2 = problem->get_libration_point_info(LibrationPointId::L2);
    REQUIRE(fabs(librationPoint2.gamma() - 0.167832731028) < tol);

    LibrationPointProperty librationPoint3 = problem->get_libration_point_info(LibrationPointId::L3);
    REQUIRE(fabs(librationPoint3.gamma() - 0.9929120626) < tol);

    delete problem;
}

TEST_CASE("Check Libration point L1 for Earth-Moon system", "[single-file]" ) {
    double r1 = 0.0;
    double r2 = 1.0;
    double m1 = EARTH_MU;
    double m2 = MOON_MU;
    double tol = 1e-6;
    double Lref[3] = {0.832579098877, 0.0, 0.0};
    double mu_ratio = 0.01215058147718;  // Earth-Moon
    double Cjacobi = 3.1841268635;

    R3bProblem *problem = new R3bProblem(r1, m1, r2, m2);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    delete problem;

    REQUIRE(librationPoint.id == 0);
    REQUIRE(fabs(librationPoint.mu - mu_ratio) < tol);
    REQUIRE(fabs(librationPoint.C - Cjacobi) < tol);
    for (int i = 0; i < 2; ++i) {
        REQUIRE(fabs(librationPoint.L[i] - Lref[i]) < tol);
    }
}
