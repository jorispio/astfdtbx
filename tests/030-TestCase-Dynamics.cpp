// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <ctime>

#include <catch2/catch.hpp>
#include "orbit/solvers/SymmetricOrbitProblemSolver.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

// #define PRINT_DEBUG

TEST_CASE("Test Dynamics 1", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double x[] = {1, 0, 0.1, 0, 1, 0, 1};
    double tol = 1e-6;
    double x_dot_ref[] = {x[3], x[4], x[5], 1.9052480527756646, 0, -1.282529957713521, 0};
    EllipticThreeBodyProblem *problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());

    CommonVariables cV;
    problem->ComputeCommonVariablesForStateOnly(0, x, cV);
    double x_dot[7];
    problem->GetStateDynamics(0, cV, x_dot);

    for (int idx = 0; idx < 6; ++idx) {
        if (fabs(x_dot_ref[idx]) > 0) {
            REQUIRE(fabs((x_dot_ref[idx] - x_dot[idx]) / x_dot_ref[idx]) < tol);
        } else {
            REQUIRE(fabs(x_dot_ref[idx] - x_dot[idx]) < tol);
        }
    }

    delete problem;
}

TEST_CASE("Test Dynamics 2", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double x[] = {1.01, 0., -0.00076, 0, 0.0097, 0, 1};
    double tol = 1e-6;
    double x_dot_ref[] = {x[3], x[4], x[5], -24.6367750908822, 0, 0.84888455810260166, 0};
    EllipticThreeBodyProblem *problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());

    CommonVariables cV;
    problem->ComputeCommonVariablesForStateOnly(0, x, cV);
    double x_dot[7];
    problem->GetStateDynamics(0, cV, x_dot);

    for (int idx = 0; idx < 6; ++idx) {
        if (fabs(x_dot_ref[idx]) > 0) {
            REQUIRE(fabs((x_dot_ref[idx] - x_dot[idx]) / x_dot_ref[idx]) < tol);
        } else {
            REQUIRE(fabs(x_dot_ref[idx] - x_dot[idx]) < tol);
        }
    }

    delete problem;
}

TEST_CASE("Test Dynamics with constant force", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double ti = 1;
    double finalTime;
    Vector7 xf;
    Vector7 x0; x0 << 0.88, 0, 0.02, 0., -0.18, 0, 1;
    Vector3 f; f << 0, 0, 0;
    ConstantForce *force = new ConstantForce(f);
    TrajectoryPropageRtbp *propagator = new TrajectoryPropageRtbp(mu_ratio, 0);
    propagator->addForce(force);
    propagator->getFinalPoint(x0, 0, ti,
                      finalTime, xf);
#ifdef PRINT_DEBUG
    std::cout << xf.transpose() << std::endl;
#endif
    double tol = 1e-4;
    REQUIRE(fabs(finalTime - 1) < tol);
    REQUIRE( fabs(xf(0) - 0.956588) < tol);
    REQUIRE( fabs(xf(1) - 0.0556535) < tol);
    REQUIRE( fabs(xf(2) - 0.0171496) < tol);
    REQUIRE( fabs(xf(3) + 0.373629) < tol);
    REQUIRE( fabs(xf(4) - 0.0571108) < tol);
    REQUIRE( fabs(xf(5) - 0.0348438) < tol);
}

MATRIXVAR compute_test_case(double mu_ratio, double *x) {
    CommonVariables cV;
    MATRIXVAR dFdXref = MATRIXVAR::Zero(7, 7);

    EllipticThreeBodyProblem *problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());

    problem->ComputeCommonVariablesForStateOnly(0, x, cV);
    problem->JacobianMatrixState(cV, dFdXref);

    MATRIXVAR dFdX = MATRIXVAR::Zero(7, 7);
    real_type x_dot_ref[7];
    problem->GetStateDynamics(0, cV, x_dot_ref);
    double h = 1e-7;
    for (int idx = 0; idx < 7; ++idx) {
        real_type x_dot[7];
        x[idx] += h;
        problem->ComputeCommonVariablesForStateOnly(0, x, cV);
        problem->GetStateDynamics(0, cV, x_dot);
        x[idx] -= h;

        for (int jdx = 0; jdx < 7; ++jdx) {
            dFdX(jdx, idx) = (x_dot[jdx] - x_dot_ref[jdx])/ h;
        }
    }

    delete problem;

    MATRIXVAR e = dFdX - dFdXref;
    return e;
}

TEST_CASE("Test Jacobian 1", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double x[] = {1, 0, 0.1, 0, 1, 0, 1};
    double tol = 1e-4;
    MATRIXVAR e =  compute_test_case(mu_ratio, x);
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}

TEST_CASE("Test Jacobian 2", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double x[] = {1.01, 0., -0.00076, 0, 0.0097, 0, 1};
    double tol = 1e-4;
    MATRIXVAR e =  compute_test_case(mu_ratio, x);
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}

MATRIXVAR compute_stm_test_case(double mu_ratio, double tof, double *x) {
    double timeStep = 0.1;
    double tspan[] = {0, tof};
    Vector3 r0; r0 << x[0], x[1], x[2];
    Vector3 v0; v0 << x[3], x[4], x[5];
    CommonVariables cV;
    MATRIXVAR Phiref = MATRIXVAR::Zero(7, 7);

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STMref;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    StateTransitionMatrixRtbp *stmPropagator = new StateTransitionMatrixRtbp(mu_ratio);
    stmPropagator->getSTM(r0, v0, tspan[0], tspan[1], STM1, STM2, STM3, STM4, STMref, P1, P2, P3, rf, vf);

    MATRIXVAR Phi = MATRIXVAR::Zero(7, 7);
    std::vector<Vector7> points = propagation(x, tspan, mu_ratio, timeStep);
    Vector7 refPoint = points.at(points.size() - 1);
    double h = 0.5 * 1e-9;
    for (int idx = 0; idx < 6; ++idx) {
        x[idx] += h;
        points = propagation(x, tspan, mu_ratio, timeStep);
        Vector7 curPoint = points.at(points.size() - 1);
        x[idx] -= h;

        Vector7 df = curPoint - refPoint;
        for (int jdx = 0; jdx < 6; ++jdx) {
            Phi(jdx, idx) = df(jdx) / h;
        }
    }

    delete stmPropagator;

    // test without the time sensitivity
    MATRIXVAR e = Phi - STMref;

#ifdef PRINT_DEBUG
    std::cout << "e_stm=" << e.block(0, 0, 6, 6) << std::endl;
    std::cout << "stm_cp=" << Phi.block(0, 0, 6, 6) << std::endl;
    std::cout << "stm_ref=" << STMref.block(0, 0, 6, 6) << std::endl;
#endif
    return e;
}

TEST_CASE("Test STM 1", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double tof = 2;
    double x[] = {1, 0, 0.1, 0, 1, 0, 1};

    // test without the time sensitivity
    double tol = 1e-4;
    MATRIXVAR e = compute_stm_test_case(mu_ratio, tof, x);
    REQUIRE(fabs(e.block(0, 0, 6, 6).lpNorm<Infinity>()) < tol);
}

TEST_CASE("Test STM 2", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double tof = 1.54944080774885710;
    double x[] = {1.0083957966084649, 0., -0.00075816090441909, 0, 0.00987718328845414, 0, 1};

    // test without the time sensitivity
    double tol = 1e-4;
    MATRIXVAR e = compute_stm_test_case(mu_ratio, tof, x);
    REQUIRE(fabs(e.block(0, 0, 6, 6).lpNorm<Infinity>()) < tol);
}


MATRIXVAR compute_jac_test_case(double mu_ratio, double *x_, double tof) {
    Vector3 r0; r0 << x_[0], x_[1], x_[2];
    Vector3 v0; v0 << x_[3], x_[4], x_[5];
    CommonVariables cV;
    MATRIXVAR Phiref = MATRIXVAR::Zero(7, 7);

    SymmetricOrbitProblemSolver *solver = new SymmetricOrbitProblemSolver(mu_ratio,
                                                        StateFilter(V_RX, V_VY),
                                                        ConstraintDerivativeFilter(C_RY, C_VX));
    solver->set_duration(1);
    solver->set_initial_conditions(x_);

    VECTORVAR zref = VECTORVAR::Zero(2);
    MATRIXVAR Jref = MATRIXVAR::Zero(2, 2);
    Matrix6 phix;
    VECTORVAR x = VECTORVAR::Zero(2);
    x << x_[0], x_[4];
    solver->zerof(x, zref, Jref, phix);

    MATRIXVAR Phi = MATRIXVAR::Zero(2, 2);
    VECTORVAR z = VECTORVAR::Zero(2);
    double h = 1e-9;
    for (int idx = 0; idx < x.size(); ++idx) {
        x(idx) += h;
        solver->valuefun(x, z);
        x(idx) -= h;

        Vector2 df = z - zref;
        for (int jdx = 0; jdx < 2; ++jdx) {
            Phi(jdx, idx) = df(jdx) / h;
        }
    }

    delete solver;

    // test without the time sensitivity
    MATRIXVAR e = ((Phi - Jref).array().cwiseProduct(Jref.array().cwiseInverse())).matrix();
    return e;
}


TEST_CASE("Test Jac 2", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double tof = 1.54944080774885710;
    double x[] = {1.0083957966084649, 0., -0.00075816090441909, 0, 0.00987718328845414, 0, 1};

    // test without the time sensitivity
    double tol = 1e-4;
    MATRIXVAR e = compute_jac_test_case(mu_ratio, x, tof);
    REQUIRE(fabs(e.block(0, 0, 2, 2).lpNorm<Infinity>()) < tol);
}

MATRIXVAR compute_jac_time_test_case(double mu_ratio, double *x_, double tof) {
    Vector3 r0; r0 << x_[0], x_[1], x_[2];
    Vector3 v0; v0 << x_[3], x_[4], x_[5];
    CommonVariables cV;
    MATRIXVAR Phiref = MATRIXVAR::Zero(7, 7);

    TimeSymmetricOrbitProblemSolver *solver = new TimeSymmetricOrbitProblemSolver(mu_ratio,
                                                            StateFilter(V_RX, V_VY, V_TIME),
                                                            ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
    solver->set_initial_conditions(x_);

    VECTORVAR zref = VECTORVAR::Zero(3);
    MATRIXVAR Jref = MATRIXVAR::Zero(3, 3);
    Matrix6 phix;
    VECTORVAR x = VECTORVAR::Zero(3);
    x << x_[0], x_[4], tof;
    solver->zerof(x, zref, Jref, phix);

    MATRIXVAR Phi = MATRIXVAR::Zero(3, 3);
    VECTORVAR z = VECTORVAR::Zero(3);
    double h = 1e-9;
    for (int idx = 0; idx < 3; ++idx) {
        x(idx) += h;
        solver->valuefun(x, z);
        x(idx) -= h;

        Vector3 df = z - zref;
        for (int jdx = 0; jdx < 3; ++jdx) {
            Phi(jdx, idx) = df(jdx) / h;
        }
    }

    delete solver;

    // test without the time sensitivity
    MATRIXVAR e = ((Phi - Jref).array().cwiseProduct(Jref.array().cwiseInverse())).matrix();
    return e;
}

TEST_CASE("Test Jac 2 with unknown time", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double tof = 1.54944080774885710;
    double x[] = {1.0083957966084649, 0., -0.00075816090441909, 0, 0.00987718328845414, 0, 1};

    // test without the time sensitivity
    double tol = 1e-4;
    MATRIXVAR e = compute_jac_time_test_case(mu_ratio, x, tof);
    REQUIRE(fabs(e.block(0, 0, 3, 3).lpNorm<Infinity>()) < tol);
}

MATRIXVAR compute_jac_planar_test_case(double mu_ratio, double parameter, double *x_, double tof) {
    Vector3 r0; r0 << x_[0], x_[1], x_[2];
    Vector3 v0; v0 << x_[3], x_[4], x_[5];
    CommonVariables cV;
    MATRIXVAR Phiref = MATRIXVAR::Zero(7, 7);

    StateFilter stateFilter = StateFilter(V_VY, V_TIME);
    ConstraintDerivativeFilter constraintFilter = ConstraintDerivativeFilter(C_RY, C_VX);
    PlanarSymmetricOrbitProblemSolver *solver = new PlanarSymmetricOrbitProblemSolver(mu_ratio, parameter,
                                                            stateFilter,
                                                            constraintFilter);
    solver->set_initial_conditions(x_);

    VECTORVAR zref = VECTORVAR::Zero(constraintFilter.size());
    MATRIXVAR Jref = MATRIXVAR::Zero(constraintFilter.size(), stateFilter.size());
    Matrix6 phix;
    VECTORVAR x = stateFilter.vector(x_, tof);
    solver->zerof(x, zref, Jref, phix);

    REQUIRE(x.rows() == 2);
    REQUIRE(zref.rows() == 2);

    MATRIXVAR Phi = MATRIXVAR::Zero(2, 2);
    VECTORVAR z = VECTORVAR::Zero(constraintFilter.size());
    double h = 1e-9;
    for (int idx = 0; idx < stateFilter.size(); ++idx) {
        x(idx) += h;
        solver->valuefun(x, z);
        x(idx) -= h;

        Vector2 df = z - zref;
        for (int jdx = 0; jdx < constraintFilter.size(); ++jdx) {
            Phi(jdx, idx) = df(jdx) / h;
        }
    }

    delete solver;

    // test without the time sensitivity
    MATRIXVAR e = ((Phi - Jref).array().cwiseProduct(Jref.array().cwiseInverse())).matrix();
    return e;
}

TEST_CASE("Test Jac for planar orbit", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double tof = 1.54944080774885710;
    double x[] = {0.831405, 0., 0, 0, 0.00987718328845414, 0, 1};
    double parameter = x[0];

    // test without the time sensitivity
    double tol = 1e-4;
    MATRIXVAR e = compute_jac_planar_test_case(mu_ratio, parameter, x, tof);
    REQUIRE(fabs(e.block(0, 0, 2, 2).lpNorm<Infinity>()) < tol);
}

