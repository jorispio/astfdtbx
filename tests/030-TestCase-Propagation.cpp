// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <vector>

#include <catch2/catch.hpp>
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

// #define PRINT_DEBUG

TEST_CASE("Propagate Lissajous Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double timeStep = 0.1;
    double tspan[] = {0, 1};
    double x[] = {1, 0, 0.1, 0, 1, 0, 1};
    double xfref[] = {1.8378835339, 0.3817680097, -0.0327765486, 1.4275340797, -0.5930498642, -0.1484413387, 2};
    double tol = 1e-6;

    std::vector<Vector7> points = propagation(x, tspan, mu_ratio, timeStep);
    auto n = points.size();
    REQUIRE(n == 11);

    Vector7 x0 = points.at(0);
#ifdef PRINT_DEBUG
    std::cout << "x0=" << x0.transpose() << std::endl;
#endif
    for (int i = 0; i < 7; ++i) REQUIRE(fabs(x0(i) - x[i]) < tol);
    Vector7 xf = points.at(n - 1);
#ifdef PRINT_DEBUG
    std::cout << "xf=" << xf.transpose() << std::endl;
#endif
    for (int i = 0; i < 7; ++i) REQUIRE(fabs(xf(i) - xfref[i]) < tol);
}

TEST_CASE("Propagate point and STM", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    Vector3 r0; r0 <<  0.1, 0, .01;
    Vector3 v0; v0 << 0, 0.1, 0;
    double dt = 1;

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    StateTransitionMatrixRtbp *propagator = new StateTransitionMatrixRtbp(mu_ratio);

    propagator->getSTM(r0, v0,
               0, dt,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);

#ifdef PRINT_DEBUG
    std::cout << "rf=" << rf.transpose() << std::endl;
    std::cout << "vf=" << vf.transpose() << std::endl;
    std::cout << "diag(STM) = " << STM.diagonal() << std::endl;
#endif

    delete propagator;

    double tol = 1e-6;
    REQUIRE(fabs(rf(0) - 0.0390008114) < tol);
    REQUIRE(fabs(rf(1) + 0.0862053375) < tol);
    REQUIRE(fabs(rf(2) - 0.0089357423) < tol);
    REQUIRE(fabs(vf(0) - 0.850724443) < tol);
    REQUIRE(fabs(vf(1) + 1.16278555) < tol);
    REQUIRE(fabs(vf(2) - 0.1283754874) < tol);
    REQUIRE(fabs(STM(0, 0) + 12.0707017987) < tol);
    REQUIRE(fabs(STM(5, 5) - 0.8838780507) < tol);
}
