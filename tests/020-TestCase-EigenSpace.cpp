// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <complex>

#include <catch2/catch.hpp>
#include "Manifold.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("R3BP manifold eigen vectors", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    Vector6 ep; ep << 8.3591514610616369e-001,  0.0000000000000000e+000,  0.0000000000000000e+000,
                        8.4348145120205324e-003,  2.6922338541050563e+000,  0.0000000000000000e+000;
    std::vector<std::complex<double> > sn;
    std::vector<VectorXcd> Ws;
    std::vector<std::complex<double> > un;
    std::vector<VectorXcd> Wu;
    std::vector<std::complex<double> > cn;
    std::vector<VectorXcd> Wc;

    EigenSpace *sp = new EigenSpace();
    sp->getManifolds(ep, mu_ratio,
                sn, Ws,
                un, Wu,
                cn, Wc);

    delete sp;

    REQUIRE(sn.size() == Ws.size());
    REQUIRE(un.size() == Wu.size());
    REQUIRE(cn.size() == Wc.size());
    REQUIRE(sn.size() == 1);
    REQUIRE(un.size() == 1);
    REQUIRE(cn.size() == 4);
}

TEST_CASE("R3BP center manifold Eigen vector for Sun-Earth L1", "[single-file]" ) {
    // Reference: M.W. Lo, HALO ORBIT GENERATION USING THE CENTER MANIFOLD, AAS 97-105
    //
    double mu_ratio = 3.003480924985e-6;
    std::complex<double> sl1 = std::complex<double>(0, 2.0864);
    Vector6 l1; l1 << 0.1279, 0.41129, 0, 0.2668, -0.8614, 0;
    std::complex<double> sl2 = std::complex<double>(0, -2.0864);
    Vector6 l2; l2 << 0.1279, 0.41129, 0, 0.2668, -0.8614, 0;
    std::complex<double> sl3 = std::complex<double>(0, 1.7495206);
    Vector6 l3; l3 << 0, 0, 0.4962, 0, 0, 0.8682;
    std::complex<double> sl4 = std::complex<double>(0, -1.7495206);
    Vector6 l4; l4 << 0, 0, 0.4962, 0, 0, -0.8682;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    Vector6 ep = librationPoint.get_state();
    std::vector<std::complex<double> > sn;
    std::vector<VectorXcd> Ws;
    std::vector<std::complex<double> > un;
    std::vector<VectorXcd> Wu;
    std::vector<std::complex<double> > cn;
    std::vector<VectorXcd> Wc;

    MATRIXVAR Df = MATRIXVAR::Zero(6, 6);
    CommonVariables cv;
    real_type *X = &ep(0);
    X[0] = ep(0); X[1] = ep(1); X[2] = ep(2); X[3] = ep(3); X[4] = ep(4); X[5] = ep(5); X[6] = 1;

    EllipticThreeBodyProblem *dynamics = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    if (dynamics->ComputeCommonVariablesForStateOnly(0, X, cv)) {
        real_type x_dot[7];
        dynamics->GetJacobianDynamics(0, X, cv, x_dot, Df);
    }
#ifdef PRINT_DEBUG
    std::cout << Df << std::endl;
#endif
    EigenSpace *sp = new EigenSpace();
    bool res = sp->getManifolds(ep, mu_ratio,
                sn, Ws,
                un, Wu,
                cn, Wc);

#ifdef PRINT_DEBUG
    printf("L%d: %f %f %f\n", librationPoint.id, librationPoint.L[0], librationPoint.L[1], librationPoint.L[2]);
    std::cout << "ep=" << ep.transpose() << std::endl;
    for (uint i=0; i < Wc.size(); ++i) {
        std::cout << "lambda_" << i << ": " << cn.at(i) << std::endl;
        std::cout << "     vect : " << Wc.at(i).transpose() << std::endl;
    }

    std::cout << cn.at(0) << std::endl;
    std::cout << cn.at(1) << std::endl;
    std::cout << cn.at(2) << std::endl;
    std::cout << cn.at(3) << std::endl;
#endif

    delete problem;
    delete sp;

    REQUIRE(cn.size() == Wc.size());
    REQUIRE(cn.size() == 4);
    REQUIRE(res);
    //double tol = 1e-6;
    //REQUIRE(abs(cn.at(0) - fabs(sl1)) < tol);
    //REQUIRE(abs(cn.at(1) + fabs(sl2)) < tol);
    //REQUIRE(abs(cn.at(2) - fabs(sl3)) < tol);
    //REQUIRE(abs(cn.at(3) + fabs(sl4)) < tol);
}
