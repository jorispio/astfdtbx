// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <catch2/catch.hpp>
#include "orbit/DistantRetrogradeOrbit.hpp"
#include "orbit/R3bpOrbitClass.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("Compute Initial conditions for DRO", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double xpos = 1.2;

    double xguess[7];
    double Tguess = 1;
    std::vector<VectorXcd> Vc;

    DistantRetrogradeOrbit *dro = new DistantRetrogradeOrbit(mu_ratio);
    dro->get_initial_conditions(xpos, xguess, Tguess);
    delete dro;

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
                xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif

    double tol = 1e-6;
    REQUIRE(fabs(Tguess - 3.23711344) < tol);
    REQUIRE(fabs(xguess[0] - 1.2) < tol);
    REQUIRE(fabs(xguess[1] - 0) < tol);
    REQUIRE(fabs(xguess[2] - 0) < tol);
    REQUIRE(fabs(xguess[3] - 0) < tol);
    REQUIRE(fabs(xguess[4] + 0.7) < tol);
    REQUIRE(fabs(xguess[5] - 0) < tol);
}

TEST_CASE("Find DRO", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double xpos = 1.2;

    DistantRetrogradeOrbit *dro = new DistantRetrogradeOrbit(mu_ratio);
    OrbitSolution orbitProperty = dro->find(xpos, false, 10);

#ifdef PRINT_DEBUG
    printf("DRO orbit period T = %f\n", orbitProperty.period );
    std::cout << "  pos = " << orbitProperty.r0.transpose() << std::endl;
    std::cout << "  vel = " << orbitProperty.v0.transpose() << std::endl;
#endif

    delete dro;

    double tol = 1e-6;
    REQUIRE(fabs(orbitProperty.period  - 3.5753291854) < tol);
}
