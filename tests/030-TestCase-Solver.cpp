// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "LissajousOrbit.hpp"
#include "lissajous/MultiPointLissajousOrbit.hpp"
#include "lissajous/LissajousProblemSolver.hpp"
#include "lissajous/MultipleArcsProblemSolver.hpp"
#include "lissajous/SingleArcProblemSolver.hpp"
#include "orbit/solvers/ConstraintDerivativeFilter.hpp"
#include "orbit/solvers/StateFilter.hpp"
#include "orbit/solvers/SymmetricOrbitProblemSolver.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

#define PRINT_DEBUG

MATRIXVAR relative_error(const MATRIXVAR& mRef, const MATRIXVAR& mCur) {
    ArrayXd array = (mRef - mCur).array().abs();
    for (int m = 0; m < array.cols(); ++m) {
        for (int n = 0; n < array.rows(); ++n) {
            if (mRef(n, m) != 0) {
                array(n, m) /= fabs(mRef(n, m));
            }
        }
    }
    return array.matrix();
}

TEST_CASE("Test StateFilter", "[single-file]" ) {
    StateFilter filter = StateFilter(V_RX, V_RY, V_VY);
    REQUIRE(filter.size() == 3);
    REQUIRE(filter.at(0) == V_RX);
    REQUIRE(filter.at(1) == V_RY);
    REQUIRE(filter.at(2) == V_VY);
}

TEST_CASE("Test ConstraintDerivativeFilter", "[single-file]" ) {
    ConstraintDerivativeFilter filter = ConstraintDerivativeFilter(C_RX, C_RY, C_VY);
    REQUIRE(filter.size() == 3);
    REQUIRE(filter.at(0) == C_RX);
    REQUIRE(filter.at(1) == C_RY);
    REQUIRE(filter.at(2) == C_VY);
}

TEST_CASE("Test DifferentialCorrector variatic argmument", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    TimeSymmetricOrbitProblemSolver *solver = new TimeSymmetricOrbitProblemSolver(mu_ratio,
                                                        StateFilter(V_RX, V_VY, V_TIME),
                                                        ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));

    solver->set_maximum_step(1., 3., 2.);
    ArrayXd max_step = solver->get_maximum_step();

    REQUIRE(max_step.size() == 3);
    REQUIRE(max_step(0) == 1);
    REQUIRE(max_step(1) == 3);
    REQUIRE(max_step(2) == 2);
}

MATRIXVAR test_jacobian_halo(double mu_ratio, const double x0[], const VECTORVAR &z) {
    TimeSymmetricOrbitProblemSolver *solver = new TimeSymmetricOrbitProblemSolver(mu_ratio,
                                                        StateFilter(V_RX, V_VY, V_TIME),
                                                        ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
    solver->set_initial_conditions(x0);

    MATRIXVAR FjacRef = MATRIXVAR::Zero(3, 3);
    VECTORVAR vref = VECTORVAR::Zero(3);
    solver->valuefun(z, vref);
    solver->jacobian(z, FjacRef);

    VECTORVAR xsol = z;
    MATRIXVAR Fjac = MATRIXVAR::Zero(3, 3);
    VECTORVAR v = VECTORVAR::Zero(3);
    double h = 1e-9;
    for (int idx = 0; idx < 3; ++idx) {
        xsol(idx) += h;
        solver->valuefun(xsol, v);
        xsol(idx) -= h;

        for (int jdx = 0; jdx < 3; ++jdx) {
            Fjac(jdx, idx) = (v(jdx) - vref(jdx)) / h;
        }
    }

    delete solver;

    return ((Fjac - FjacRef).array().cwiseProduct(FjacRef.array().cwiseInverse())).matrix();
}

TEST_CASE("Test Zero function Jacobian for Halo1", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double x[] = {1.0083957966084649, 0., -0.00075816090441909, 0, 0.00987718328845414, 0, 1};
    VECTORVAR z = VECTORVAR::Zero(3);
    z << 1.0083957966084649, 0.00987718328845414, 1.54944080774885710;

    double tol = 1e-4;
    MATRIXVAR e = test_jacobian_halo(mu_ratio, x, z);
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}

TEST_CASE("Test Zero function Jacobian for Halo2", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    double x[] = {-1.38672718, 0., -0.00000648, 0, 0.80001009, 0, 1};
    VECTORVAR z = VECTORVAR::Zero(3);
    z << 1.0083957966084649, 0.00987718328845414, 1.54944080774885710;

    double tol = 1e-4;
    MATRIXVAR e = test_jacobian_halo(mu_ratio, x, z);
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}

TEST_CASE("Test solving function  for Halo", "[single-file]" ) {
    double mu_ratio = 3.040423392272346e-06;
    TimeSymmetricOrbitProblemSolver *solver = new TimeSymmetricOrbitProblemSolver(mu_ratio,
                                                        StateFilter(V_RX, V_VY, V_TIME),
                                                        ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
    double x[] = {1.0083957966084649, 0., -0.00075816090441909, 0, 0.00987718328845414, 0, 1};
    solver->set_initial_conditions(x);

    VECTORVAR z = VECTORVAR::Zero(3);
    VECTORVAR zs = VECTORVAR::Zero(3);
    z << 1.0083957966084649, 0.00987718328845414, 1.54944080774885710;

    solver->solve(z, zs, false, 10, 1e-6);
    delete solver;
#ifdef PRINT_DEBUG
    std::cout << "zs=" << zs.transpose() << std::endl;
#endif

    double tol = 1e-4;
    REQUIRE(fabs(zs(0) - 1.0083527128509064) < tol);
    REQUIRE(fabs(zs(1) - 0.01000592310968758) < tol);
    REQUIRE(fabs(zs(2) - 1.5508643534) < tol);
}


TEST_CASE("Test 1st-pass Zero function Jacobian for Lissajous", "[single-file]" ) {
    double mu_ratio = 0.0125;
    Vector6 init_state; init_state << 1, 0.0, 0.1, 0., -0.1, 0.1;
    Vector6 final_state; final_state << 1.1, 0.0, 0.01, 0., -0.1, 0.1;
    VECTORVAR x; x = VECTORVAR::Zero(3); x << 1.1, 0.1, 0.02;

    SingleArcProblemSolver *solver = new SingleArcProblemSolver(mu_ratio);
    solver->set_initial_state(init_state);
    solver->set_terminal_state(C_POSITION, final_state);
    solver->set_duration(1.4);

    bool iret;
    MATRIXVAR FjacRef = MATRIXVAR::Zero(3, 3);
    VECTORVAR vref = solver->valuefun(x, iret);
    solver->jacobian(x, FjacRef);

    MATRIXVAR Fjac = MATRIXVAR::Zero(3, 3);
    double h = 1e-6;
    for (int idx = 0; idx < 3; ++idx) {
        x(idx) += h;
        VECTORVAR v = solver->valuefun(x, iret);
        x(idx) -= h;

        for (int jdx = 0; jdx < 3; ++jdx) {
            Fjac(jdx, idx) = (v(jdx) - vref(jdx))/ h;
        }
    }
    delete solver;

#ifdef PRINT_DEBUG
    std::cout << "Fjac=" << Fjac << std::endl;
#endif

    double tol = 1e-4;
    MATRIXVAR e = Fjac - FjacRef;
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}


MATRIXVAR test_jacobian_multiple_shooting(int n_segments) {
    double mu_ratio = 0.0125;
    Vector6 final_state; final_state << 1.1, 0.0, 0.01, 0., -0.1, 0.1;
    int nx = (n_segments + 1) * (3 + 1);
    int ny = (n_segments - 1) * 3;
    VECTORVAR x; x = VECTORVAR::Zero(nx);  // unknown vector
    double Ax = 0.01;
    double Az = 0.001;
    double phix = 0; double phiz = 0;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);
    delete problem;

    LissajousOrbit *orbit = new LissajousOrbit(librationPoint);
    orbit->set_parameters(Ax, phix, Az, phiz);

    Lissajous *lissajous = new Lissajous(librationPoint);
    std::vector<Segment> segments = lissajous->get_analytical_segments(orbit, n_segments);
    delete lissajous;
    delete orbit;

    // unknown vector [ri, ti]
    for (int i_segment = 0; i_segment < n_segments + 1; ++i_segment) {
        if (i_segment < n_segments) {
            x.segment(4 * i_segment, 3) = segments.at(i_segment).ri;
            x(4 * i_segment + 3) = 0;
        } else {  // i_segment = n_segment
            x.segment(4 * i_segment, 3) = segments.at(i_segment - 1).rf;
            x(4 * i_segment + 3) = 0;
        }
    }

    MultipleArcsProblemSolver *solver = new MultipleArcsProblemSolver(mu_ratio, n_segments);
    solver->set_states(segments);
    solver->set_terminal_state(final_state);

    REQUIRE(solver->get_n() == nx);
    REQUIRE(solver->get_m() == ny);

    bool iret;
    VECTORVAR vref = solver->valuefun(x, iret);
    REQUIRE(vref.rows() == ny);

    MATRIXVAR FjacRef = MATRIXVAR::Zero(ny, nx);
    solver->jacobian(x, FjacRef);

    MATRIXVAR FjacFd = MATRIXVAR::Zero(ny, nx);
    double h = 1e-9;
    for (int idx=0; idx < nx; ++idx) {
        double xh = h * std::max(fabs(x(idx)), 1.);
        x(idx) += xh;
        VECTORVAR v = solver->valuefun(x, iret);
        x(idx) -= xh;
        for (int jdx=0; jdx < ny; ++jdx) {
            FjacFd(jdx, idx) = (v(jdx) - vref(jdx)) / xh;
        }
    }    
    
    delete solver;
#ifdef PRINT_DEBUG
    std::cout << "Ri, Ti, Ri+1, Ti+1, ...., Rn, Tn" << std::endl;
    std::cout << "FjacRef=\n" << FjacRef << std::endl;
    std::cout << "-----------" << std::endl;
    std::cout << "FjacFd =\n" << FjacFd << std::endl;
#endif
    return relative_error(FjacRef, FjacFd);
}

TEST_CASE("Test 2nd-pass Zero function Jacobian for Lissajous (2 segments)", "[single-file]" ) {   
    MATRIXVAR e = test_jacobian_multiple_shooting(2);
    double tol = 1e-4;
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}

TEST_CASE("Test 2nd-pass Zero function Jacobian for Lissajous (3 segments)", "[single-file]" ) {
    MATRIXVAR e = test_jacobian_multiple_shooting(3);
    double tol = 1e-3;
    REQUIRE(fabs(e.lpNorm<Infinity>()) < tol);
}
