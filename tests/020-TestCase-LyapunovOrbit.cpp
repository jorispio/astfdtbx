// $Id$
// ---------------------------------------------------------------------------
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <catch2/catch.hpp>
#include "orbit/LyapunovOrbit.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "R3BProblem.hpp"

//#define PRINT_DEBUG

TEST_CASE("Compute Initial conditions for Planar Lyapunov Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LyapunovOrbit *lyapunovOrbit = new LyapunovOrbit(librationPoint, LyapunovOrbitFamilyType::PLANAR);
    double xguess[7];
    double Tguess = 1;
    bool res = lyapunovOrbit->get_initial_conditions(Ax, xguess, Tguess);

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
            xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif
    delete problem;
    delete lyapunovOrbit;

    double tol = 1e-6;
    REQUIRE(res == true);
    REQUIRE(fabs(Tguess - 2.76209524) < tol);
    REQUIRE(fabs(xguess[0] - 0.86026463) < tol);
    REQUIRE(fabs(xguess[1]) < tol);
    REQUIRE(fabs(xguess[2]) < tol);
    REQUIRE(fabs(xguess[3]) < tol);
    REQUIRE(fabs(xguess[4] + 0.17097377) < tol);
    REQUIRE(fabs(xguess[5]) < tol);
}


TEST_CASE("Find Planar Lyapunov Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LyapunovOrbit *lyapunovOrbit = new LyapunovOrbit(librationPoint, LyapunovOrbitFamilyType::PLANAR);
    OrbitSolution orbitProperty = lyapunovOrbit->find(Ax, false, 5, 1e-6, 1.);

#ifdef PRINT_DEBUG
    printf("Lyapunov orbit period T = %.8f\n", orbitProperty.period );
    std::cout << "  pos = " << orbitProperty.r0.transpose() << std::endl;
    std::cout << "  vel = " << orbitProperty.v0.transpose() << std::endl;
#endif
    delete problem;
    delete lyapunovOrbit;

    double tol = 1e-6;
    REQUIRE(orbitProperty.is_valid);
    REQUIRE(fabs(orbitProperty.period  - 2.77474213) < tol);
}

TEST_CASE("Compute Initial conditions for Vertical Lyapunov Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.02;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LyapunovOrbit *lyapunovOrbit = new LyapunovOrbit(librationPoint, LyapunovOrbitFamilyType::VERTICAL);
    double xguess[7];
    double Tguess = 1;
    bool res = lyapunovOrbit->get_initial_conditions(Ax, xguess, Tguess);

#ifdef PRINT_DEBUG
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
            xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("tguess = %.8f\n", Tguess);
#endif
    delete problem;
    delete lyapunovOrbit;

    double tol = 1e-6;
    REQUIRE(res == true);
    REQUIRE(fabs(Tguess - 2.64074713) < tol);
    REQUIRE(fabs(xguess[0] - 0.83771519) < tol);
    REQUIRE(fabs(xguess[1]) < tol);
    REQUIRE(fabs(xguess[2] - 0.01999256) < tol);
    REQUIRE(fabs(xguess[3]) < tol);
    REQUIRE(fabs(xguess[4] + 0.00056652) < tol);
    REQUIRE(fabs(xguess[5]) < tol);
}

TEST_CASE("Find Vertical Lyapunov Orbit", "[single-file]" ) {
    double mu_ratio = 0.01215058147718;
    double Ax = 0.05;

    R3bProblem *problem = new R3bProblem(mu_ratio);
    LibrationPointProperty librationPoint = problem->get_libration_point_info(LibrationPointId::L1);

    LyapunovOrbit *lyapunovOrbit = new LyapunovOrbit(librationPoint, LyapunovOrbitFamilyType::VERTICAL);
    OrbitSolution orbitProperty = lyapunovOrbit->find(Ax, false, 5);

#ifdef PRINT_DEBUG
    printf("Lyapunov orbit period T = %.8f\n", orbitProperty.period );
#endif
    delete problem;
    delete lyapunovOrbit;

    double tol = 1e-6;
    REQUIRE(orbitProperty.is_valid);
    REQUIRE(fabs(orbitProperty.period  - 2.81368345) < tol);
}
