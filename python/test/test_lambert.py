# -*- coding: utf-8 -*-
"""
Test Lambert's problem in the RTBP
=========================================

"""

# Joris T. OLYMPIO

import unittest

import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as r3bp
from r3bpy.main import LambertProblem, Propagator, Problem


class TestLambertProblem(unittest.TestCase):
    '''Test Lambert Problem'''
    def test_lambert(self):
        r12 = constants.DISTANCE_EARTH_MOON
        m1 = constants.MU_EARTH
        m2 = constants.MU_MOON

        problem = Problem(m1, m2, r12)

        r0 = [1.,0.,0., 0., 1., 0.]
        rf = [0.,1.,1.]
        tof = 1.0
        lambert_problem = LambertProblem(problem.get_mu_ratio())
        v1, v2 = lambert_problem.solve(r0, rf, tof, True)
        
        x0 = [r0[0], r0[1], r0[2], v1[0][0], v1[1][0], v1[2][0]] # FIXME
        
        # propagate
        propagator = Propagator(problem.get_mu_ratio())
        t, x = propagator.propagate(x0, [0, tof])
        
        xf = x[-1]
    
    

if __name__ == '__main__':
    unittest.main(verbosity=2)
    
    
