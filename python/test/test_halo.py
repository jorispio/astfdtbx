# -*- coding: utf-8 -*-
"""
Test Halo orbit construction
============================

"""

# Joris T. OLYMPIO
import unittest

import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
from r3bpy.main import LambertProblem, Propagator, Problem
from r3bpy.orbits import HaloOrbit
import r3bpy.main as r3bp


class TestHalo(unittest.TestCase):
    '''Test Halo'''
    def test_lambert(self):
        mu_ratio = 0.01215058147718
        Az = 0.001

        problem = Problem(mu_ratio)
        libration_point = problem.get_libration_point_info(main.L2)

        # compute Halo orbit
        halo = HaloOrbit(libration_point)
        halo_orbit = halo.find_orbit(Az)
        print("Halo period: ", halo_orbit.period )
        print("Orbit amplitudes: ", halo_orbit.Ax, halo_orbit.Ay, halo_orbit.Az)

        orbit_period = halo_orbit.period
        xyz_halo = halo_orbit.propagate([0, orbit_period])    
    

if __name__ == '__main__':
    unittest.main(verbosity=2)
    
    
