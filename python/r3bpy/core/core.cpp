
#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>		// to expose c++ enum
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/python/scope.hpp>
#include <boost/utility.hpp>

#include "HelioLib.hpp"

#include "DeclareConverters.hpp"

   
#define PYTHON_DECLARE_CONSTANT(arg) scope().attr(#arg)=arg;

using namespace boost::python;

// ------------------------------------------------------------------------- 
BOOST_PYTHON_MODULE(_core)
{
    // Register std converters to python lists if not already registered by some
    DECLARE_CONVERTERS();

    // Expose the astrodynamical constants.
    PYTHON_DECLARE_CONSTANT(AU);
    PYTHON_DECLARE_CONSTANT(MU_SUN);;
    PYTHON_DECLARE_CONSTANT(MU_MERCURY);
    PYTHON_DECLARE_CONSTANT(MU_VENUS);
    PYTHON_DECLARE_CONSTANT(MU_EARTH);
    PYTHON_DECLARE_CONSTANT(MU_MOON)    
    PYTHON_DECLARE_CONSTANT(MU_MARS);
    PYTHON_DECLARE_CONSTANT(MU_JUPITER);
    PYTHON_DECLARE_CONSTANT(MU_SATURN);
    PYTHON_DECLARE_CONSTANT(MU_URANUS);
    PYTHON_DECLARE_CONSTANT(MU_NEPTUN);
    PYTHON_DECLARE_CONSTANT(MU_PLUTO);
    PYTHON_DECLARE_CONSTANT(DISTANCE_EARTH_MOON);
    PYTHON_DECLARE_CONSTANT(DISTANCE_SUN_EARTH);
    PYTHON_DECLARE_CONSTANT(DISTANCE_SUN_JUPITER);
    PYTHON_DECLARE_CONSTANT(EARTH_RADIUS);
    PYTHON_DECLARE_CONSTANT(EGM96_EARTH_EQUATORIAL_RADIUS);
    PYTHON_DECLARE_CONSTANT(EGM96_EARTH_MU);
    PYTHON_DECLARE_CONSTANT(DEG2RAD);
    PYTHON_DECLARE_CONSTANT(RAD2DEG);
    PYTHON_DECLARE_CONSTANT(DAY2SEC);
    PYTHON_DECLARE_CONSTANT(SEC2DAY);
    PYTHON_DECLARE_CONSTANT(DAY2YEAR);
    PYTHON_DECLARE_CONSTANT(MJD2000_MJD_OFFSET);
    PYTHON_DECLARE_CONSTANT(MJD_JD_OFFSET);
}



