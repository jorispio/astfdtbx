#ifndef PYTHON_EXCEPTION_HPP
#define PYTHON_EXCEPTION_HPP

#include <boost/python.hpp>
#include <exception>
#include <string>

/*
 * Catchable Python exception.
 */
class PythonException : public std::exception
{
private:
     static void translateException( PythonException const & e);
     static PyObject * pyType;
protected:
     std::string message;
     
public:
    PythonException() : message() {}
    
    PythonException(const std::string & msg) : message(msg) {}
    const char *what() const throw()
    {
       return this->getMessage().c_str();
    }
    
    ~PythonException() throw() {}
    
    virtual const std::string & getMessage() const { return message; }
    
    std::string copyMessage() const { return getMessage(); }
 
    static void declareExceptionInPython();     
};
    
#endif  // PYTHON_EXCEPTION_HPP