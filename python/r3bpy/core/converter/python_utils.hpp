#ifndef __PYTHON_CONVERTER_UTILS_HPP_
#define __PYTHON_CONVERTER_UTILS_HPP_

template<typename T>
inline bool check_registration() {
    namespace bp = boost::python;
    const bp::type_info info = bp::type_id<T>();
    const bp::converter::registration* reg = bp::converter::registry::query(info);
    if (reg == NULL) return false;
    else if ((*reg).m_to_python == NULL) return false;

    return true;
}

#endif  // __PYTHON_CONVERTER_UTILS_HPP_
