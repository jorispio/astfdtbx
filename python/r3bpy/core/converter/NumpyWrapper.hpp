#ifndef NUMPY_WRAPPER_HPP
#define NUMPY_WRAPPER_HPP

#include <iostream>

#include <boost/python.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/list.hpp>
#include <boost/python/to_python_converter.hpp>

#include "PythonException.hpp"

namespace bp = boost::python;
namespace registry = bp::converter::registry;


enum NP_TYPE {
    MATRIX_TYPE,
    ARRAY_TYPE
};


template<typename MatType, typename InputScalar, int IsVector>
struct MapNumpyTraits {};

/* Wrap a numpy::array with an Eigen::Map. No memory copy. */
template<typename MatType, typename InputScalar>
struct MapNumpy {
    typedef MapNumpyTraits<MatType, InputScalar, MatType::IsVectorAtCompileTime> Impl;
    typedef typename Impl::EigenMap EigenMap;
    //typedef typename Impl::Stride Stride;

    static inline EigenMap map( PyArrayObject* pyArray);
};


// as_to_python_function.hpp
template<typename MatType, typename InputScalar>
struct MapNumpyTraits<MatType,InputScalar,0> {
    //typedef typename StrideType<MatType>::type Stride;
    typedef Eigen::Matrix<InputScalar,MatType::RowsAtCompileTime,MatType::ColsAtCompileTime> EquivalentInputMatrixType;
    typedef Eigen::Map<EquivalentInputMatrixType, DEFAULT_ALIGNMENT_VALUE/*,Stride*/> EigenMap;
 
    static EigenMap mapImpl( PyArrayObject* pyArray) {
        assert( PyArray_NDIM(pyArray) == 2);
       
        assert( (PyArray_DIMS(pyArray)[0] < INT_MAX)
               && (PyArray_DIMS(pyArray)[1] < INT_MAX)
               && (PyArray_STRIDE(pyArray, 0) < INT_MAX)
               && (PyArray_STRIDE(pyArray, 1) < INT_MAX));
 
        const int R = (int)PyArray_DIMS(pyArray)[0];
        const int C = (int)PyArray_DIMS(pyArray)[1];
        //const long int itemsize = PyArray_ITEMSIZE(pyArray);
        //const int stride1 = (int)PyArray_STRIDE(pyArray, 0) / (int)itemsize;
        //const int stride2 = (int)PyArray_STRIDE(pyArray, 1) / (int)itemsize;
        //Stride stride(stride2,stride1);
       
        if ( (MatType::RowsAtCompileTime!=R) && (MatType::RowsAtCompileTime!=Eigen::Dynamic)) {
            throw PythonException("The number of rows does not fit with the matrix type."); 
        }
       
        if ( (MatType::ColsAtCompileTime!=C) && (MatType::ColsAtCompileTime!=Eigen::Dynamic)) {
            throw PythonException("The number of columns does not fit with the matrix type.");
        }
       
        InputScalar* pyData = reinterpret_cast<InputScalar*>(PyArray_DATA(pyArray));
       
        return EigenMap( pyData, R,C/*, stride*/);
    }
};
   
   
 
template<typename MatType, typename InputScalar>
struct MapNumpyTraits<MatType,InputScalar,1> {
    //typedef typename StrideType<MatType>::type Stride;
    typedef Eigen::Matrix<InputScalar, MatType::RowsAtCompileTime, MatType::ColsAtCompileTime> EquivalentInputMatrixType;
    typedef Eigen::Map<EquivalentInputMatrixType, DEFAULT_ALIGNMENT_VALUE> EigenMap;
  
    static EigenMap mapImpl( PyArrayObject* pyArray) {
        assert( PyArray_NDIM(pyArray) <= 2);
 
        int rowMajor;
        if (  PyArray_NDIM(pyArray)==1) rowMajor = 0;
        else if (PyArray_DIMS(pyArray)[0] == 0) rowMajor = 0;  // handle zero-size vector
        else if (PyArray_DIMS(pyArray)[1] == 0) rowMajor = 1;  // handle zero-size vector
        else rowMajor = (PyArray_DIMS(pyArray)[0]>PyArray_DIMS(pyArray)[1])?0:1;
 
        assert( (PyArray_DIMS(pyArray)[rowMajor]< INT_MAX) && (PyArray_STRIDE(pyArray, rowMajor)));
        
        const int R = (int)PyArray_DIMS(pyArray)[rowMajor];
        //const long int itemsize = PyArray_ITEMSIZE(pyArray);
        //const int stride = (int) PyArray_STRIDE(pyArray, rowMajor) / (int) itemsize;;
 
        if ( (MatType::MaxSizeAtCompileTime != R)
          && (MatType::MaxSizeAtCompileTime != Eigen::Dynamic)) {
            throw PythonException("The number of elements does not fit with the vector type."); 
        }
 
        InputScalar* pyData = reinterpret_cast<InputScalar*>(PyArray_DATA(pyArray));
       
        return EigenMap( pyData, R);
    }
};

struct NumpyType {
    static NumpyType & getInstance() {
        static NumpyType instance;
        return instance;
    }

    operator bp::object () { return CurrentNumpyType; }

    bp::object make(PyArrayObject* pyArray, bool copy = false) {
        return make((PyObject*)pyArray,copy); 
    }
    
    bp::object make(PyObject* pyObj, bool copy = false) {
        bp::object m; 
        bp::object raw_object = bp::object(bp::handle<>(pyObj));  // nothing to do here
        if (PyType_IsSubtype(reinterpret_cast<PyTypeObject*>(CurrentNumpyType.ptr()), NumpyMatrixType)) {
            m = NumpyMatrixObject(raw_object, bp::object(), copy);
        }
        else if (PyType_IsSubtype(reinterpret_cast<PyTypeObject*>(CurrentNumpyType.ptr()), NumpyArrayType)) {
            m = raw_object;
        }
        Py_INCREF(m.ptr());
        return m;
    }
    
    static void setNumpyType(bp::object & obj) {
        PyTypeObject * obj_type = PyType_Check(obj.ptr()) ? reinterpret_cast<PyTypeObject*>(obj.ptr()) : obj.ptr()->ob_type;
        if (PyType_IsSubtype(obj_type, getInstance().NumpyMatrixType))
            switchToNumpyMatrix();
        else if (PyType_IsSubtype(obj_type, getInstance().NumpyArrayType))
            switchToNumpyArray();
    }
    
    static void switchToNumpyArray() {
        getInstance().CurrentNumpyType = getInstance().NumpyArrayObject;
        getType() = ARRAY_TYPE;
    }
    
    static void switchToNumpyMatrix() {
        getInstance().CurrentNumpyType = getInstance().NumpyMatrixObject;
        getType() = MATRIX_TYPE;
    }
    
    static NP_TYPE & getType() {
        static NP_TYPE np_type;
        return np_type;
    }
    
    static bp::object getNumpyType() {
        return getInstance().CurrentNumpyType;
    }

protected:
    NumpyType() {
        numpyModule = bp::import("numpy");
        Py_INCREF(numpyModule.ptr());
      
        NumpyMatrixObject = numpyModule.attr("matrix");
        NumpyMatrixType = reinterpret_cast<PyTypeObject*>(NumpyMatrixObject.ptr());
        NumpyArrayObject = numpyModule.attr("ndarray");
        NumpyArrayType = reinterpret_cast<PyTypeObject*>(NumpyArrayObject.ptr());
      
        //CurrentNumpyType = NumpyMatrixObject;  // default conversion
        //getType() = MATRIX_TYPE;        
        CurrentNumpyType = NumpyArrayObject;  // default conversion
        getType() = ARRAY_TYPE;
    }

    bp::object CurrentNumpyType;
    bp::object numpyModule;
    
    // Numpy types
    bp::object NumpyMatrixObject; 
    PyTypeObject * NumpyMatrixType;
    bp::object NumpyArrayObject; 
    PyTypeObject * NumpyArrayType;    
};
   
  
template<typename MatType, typename InputScalar>
typename MapNumpy<MatType,InputScalar>::EigenMap MapNumpy<MatType,InputScalar>::map( PyArrayObject* pyArray) {
    return Impl::mapImpl(pyArray); 
}


#endif  // NUMPY_WRAPPER_HPP
