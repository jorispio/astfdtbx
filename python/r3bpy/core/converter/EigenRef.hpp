#ifndef EIGEN_REF_HPP
#define EIGEN_REF_HPP

#define DEFAULT_ALIGNMENT_VALUE Eigen::AlignmentType::Unaligned
#include <Eigen/Core>

template<typename MatType, bool IsVectorAtCompileTime = MatType::IsVectorAtCompileTime>
struct StrideType
{
    typedef Eigen::Stride<Eigen::Dynamic,Eigen::Dynamic> type;
};

template<typename MatType>
struct StrideType<MatType, true>
{
    typedef Eigen::InnerStride<Eigen::Dynamic> type;
};

template<typename PlainObjectTypeT>
struct EigenRef
   : Eigen::Ref<PlainObjectTypeT, DEFAULT_ALIGNMENT_VALUE, typename StrideType<PlainObjectTypeT>::type>
{
public:
    // For VC    
#ifdef _MSC_VER
    //typedef typename StrideType<PlainObjectTypeT> StrideType_;    
    typedef Eigen::Ref<PlainObjectTypeT, DEFAULT_ALIGNMENT_VALUE> Base;
#else
    typedef typename Eigen::Ref<PlainObjectTypeT, DEFAULT_ALIGNMENT_VALUE, typename StrideType<PlainObjectTypeT>::type> Base;
#endif
 
private:
    typedef Eigen::internal::traits<Base> Traits;
    template<typename Derived>
    EIGEN_DEVICE_FUNC inline EigenRef(const Eigen::PlainObjectBase<Derived>& expr,
                                  typename Eigen::internal::enable_if<bool(Traits::template match<Derived>::MatchAtCompileTime),Derived>::type* = 0);
     
public:
     
    typedef typename Eigen::internal::traits<Base>::Scalar Scalar;  \
    typedef typename Eigen::NumTraits<Scalar>::Real RealScalar;  \
    typedef typename Base::CoeffReturnType CoeffReturnType; 
    typedef typename Eigen::internal::ref_selector<Base>::type Nested;
    typedef typename Eigen::internal::traits<Base>::StorageKind StorageKind;
#if EIGEN_VERSION_AT_LEAST(3,2,90)
    typedef typename Eigen::internal::traits<Base>::StorageIndex StorageIndex;
#else
    typedef typename Eigen::internal::traits<Base>::Index StorageIndex;
#endif
    enum { RowsAtCompileTime = Eigen::internal::traits<Base>::RowsAtCompileTime,
       ColsAtCompileTime = Eigen::internal::traits<Base>::ColsAtCompileTime,
       Flags = Eigen::internal::traits<Base>::Flags,
       SizeAtCompileTime = Base::SizeAtCompileTime,
       MaxSizeAtCompileTime = Base::MaxSizeAtCompileTime,
       IsVectorAtCompileTime = Base::IsVectorAtCompileTime };
    using Base::derived;
    using Base::const_cast_derived;
    typedef typename Base::PacketScalar PacketScalar;
     
    template<typename Derived>
    EIGEN_DEVICE_FUNC inline EigenRef(Eigen::PlainObjectBase<Derived>& expr,
                                  typename Eigen::internal::enable_if<bool(Traits::template match<Derived>::MatchAtCompileTime),Derived>::type* = 0)
     : Base(expr.derived())
     {}
     
    template<typename Derived>
    EIGEN_DEVICE_FUNC inline EigenRef(const Eigen::DenseBase<Derived>& expr,
                                  typename Eigen::internal::enable_if<bool(Traits::template match<Derived>::MatchAtCompileTime),Derived>::type* = 0)
     : Base(expr.derived())
     {}
     
#if EIGEN_COMP_MSVC_STRICT && (EIGEN_COMP_MSVC < 1900 ||  defined(__CUDACC_VER__)) // for older MSVC versions, as well as 1900 && CUDA 8, using the base operator is sufficient (cf Bugs 1000, 1324)
    using Base::operator =;
#elif EIGEN_COMP_CLANG // workaround clang bug (see http://forum.kde.org/viewtopic.php?f=74&t=102653)
    using Base::operator =; \
    EIGEN_DEVICE_FUNC EIGEN_STRONG_INLINE EigenRef& operator=(const EigenRef& other) { Base::operator=(other); return *this; } \
    template <typename OtherDerived> \
     EIGEN_DEVICE_FUNC EIGEN_STRONG_INLINE EigenRef& operator=(const Eigen::DenseBase<OtherDerived>& other) { Base::operator=(other.derived()); return *this; }
#else
    using Base::operator =; \
    EIGEN_DEVICE_FUNC EIGEN_STRONG_INLINE EigenRef& operator=(const EigenRef& other) \
    { \
       Base::operator=(other); \
       return *this; \
    }
#endif
     
};  // struct EigenRef<PlainObjectType>
   
   
#endif  // EIGEN_REF_HPP
