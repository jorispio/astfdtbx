// $Id$
// ---------------------------------------------------------------------------
#ifndef PYTHON_EIGEN_CONVERTER_HPP
#define PYTHON_EIGEN_CONVERTER_HPP
// Import an C++ Eigen type to/from a Python Numpy type.
//
// References:
// https://www.boost.org/doc/libs/1_57_0/libs/python/doc/tutorial/doc/html/python/exposing.html
// https://www.boost.org/doc/libs/1_66_0/libs/python/doc/html/article.html#boost-python-design-goals
// https://sixty-north.com/blog/how-to-write-boost-python-type-converters.html


#include <iostream>
#include <complex>
#include <algorithm>

#include <boost/python.hpp>
#include <boost/python/extract.hpp>
#include <boost/python/list.hpp>
#include <boost/python/to_python_converter.hpp>
#include <boost/numpy.hpp>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h> // for PyArrayObject

#include "PythonException.hpp"
#include "converter/EigenRef.hpp"
#include "converter/NumpyWrapper.hpp"
#include "converter/python_utils.hpp"

 
namespace bp = boost::python;
namespace registry = bp::converter::registry;
namespace np = boost::numpy;

#define GET_PY_ARRAY_TYPE(array) PyArray_ObjectType(reinterpret_cast<PyObject *>(array), 0)

template <typename SCALAR>  struct NumpyEquivalentType {};
template <> struct NumpyEquivalentType<double>  { enum { type_code = NPY_DOUBLE };};
template <> struct NumpyEquivalentType<int>     { enum { type_code = NPY_INT    };};
template <> struct NumpyEquivalentType<long>    { enum { type_code = NPY_LONG    };};
template <> struct NumpyEquivalentType<float>   { enum { type_code = NPY_FLOAT  };};
template <> struct NumpyEquivalentType<std::complex<double> >  { enum { type_code = NPY_COMPLEX128 };};

template <typename SCALAR1, typename SCALAR2>
struct FromTypeToType : public boost::false_type {};

template <typename SCALAR>
struct FromTypeToType<SCALAR,SCALAR> : public boost::true_type {};

template <> struct FromTypeToType<int,long> : public boost::true_type {};
template <> struct FromTypeToType<int,float> : public boost::true_type {};
template <> struct FromTypeToType<int,double> : public boost::true_type {};

template <> struct FromTypeToType<long,float> : public boost::true_type {};
template <> struct FromTypeToType<long,double> : public boost::true_type {};

template <> struct FromTypeToType<float,double> : public boost::true_type {};
template <> struct FromTypeToType<double, std::complex<double> > : public boost::true_type {};


template<typename MatType>
struct EigenObjectAllocator {
    typedef MatType Type;
    typedef typename MatType::Scalar Scalar;

    static void allocate(PyObject * pyObj, void * storage) {
        PyArrayObject * pyArray = reinterpret_cast<PyArrayObject*>(pyObj);
        int ndims = PyArray_NDIM(pyArray);
        const int rows = (int) PyArray_DIMS(pyArray)[0];
        int cols = 1;
        if (ndims == 2) {
            cols = (int) PyArray_DIMS(pyArray)[1];
        }

        np::ndarray arr = bp::extract<np::ndarray>(pyObj);
        Type *mat_ptr = new (storage) Type(rows, cols);  // create matrix in allocation storage

        // Resize matrix if (half-)dynamic-sized matrix
        if (MatType::RowsAtCompileTime == Eigen::Dynamic || MatType::ColsAtCompileTime == Eigen::Dynamic) {
            mat_ptr->resize(rows, cols);
        }

        // Note: we have to split in two branches...
        if (ndims == 2) {
            for (int i = 0; i < rows; ++i) {
                for (int j = 0; j < cols; ++j) {
                    std::cout << i << ", " << j << std::endl;
                    (*mat_ptr)(i, j) = bp::extract<typename MatType::Scalar>(arr[i][j]);
                }
            }
        } else {
            for (int i = 0; i < rows; ++i) {
                (*mat_ptr)(i,0) = bp::extract<typename MatType::Scalar>(arr[i]);
            }
        }
    }

    template<typename MatrixDerived>
    static void copy(const PyObject * pyObj, Eigen::MatrixBase<MatrixDerived> &mat) {
        PyArrayObject * pyArray = reinterpret_cast<PyArrayObject*>(pyObj);
        const int rows = (int) PyArray_DIMS(pyArray)[0];
        const int cols = (int) PyArray_DIMS(pyArray)[1];
        np::ndarray arr = bp::extract<np::ndarray>(pyObj);

        if (MatType::RowsAtCompileTime == Eigen::Dynamic || MatType::ColsAtCompileTime == Eigen::Dynamic) {
            mat.resize(rows, cols);
        }

        for (long i = 0; i < rows; ++i) {
            for (long j = 0; j < cols; ++j) {
                mat(i, j) = bp::extract<typename MatType::Scalar>(arr[i][j]);
            }
        }
    }
     
    template<typename MatrixDerived>
    static void copy(const Eigen::MatrixBase<MatrixDerived> & mat_,
                      PyArrayObject * pyArray) {
        Scalar *mOut = (Scalar*)PyArray_DATA(pyArray);
        for (long i = 0; i < mat_.rows(); ++i)
            for (long j = 0; j < mat_.cols(); ++j)
                mOut[i * mat_.cols() +j] = mat_(i, j);
    }
};

template<typename MatType>
struct EigenObjectAllocator< EigenRef<MatType> > {
    typedef Ref<MatType> Type;
    typedef typename MatType::Scalar Scalar;
     
    static void allocate(PyArrayObject * pyArray, void * storage) {
       typename MapNumpy<MatType, Scalar>::EigenMap numpyMap = MapNumpy<MatType, Scalar>::map(pyArray);
       new (storage) Type(numpyMap);  // map the numpy array to the allocated space
    }
     
    static void copy(Type const & mat, PyArrayObject * pyArray) {
       EigenObjectAllocator<MatType>::copy(mat,pyArray);
    }
};



// From Python to C++
template<typename MatType>
struct EigenFromPython {
    /** check if the type is convertible. Return NULL if not. */
    static void* convertible(PyObject* pyArray) {
        bp::extract<np::ndarray> arr(pyArray);
        if (!arr.check()) {           // Check it is a numpy array
            return NULL;
        }

        if ((arr().get_dtype() != np::dtype::get_builtin<typename MatType::Scalar>()) // Check type
            || (MatType::RowsAtCompileTime != Eigen::Dynamic && MatType::RowsAtCompileTime != arr().shape(0)) // Check rows are the same
            /*|| (MatType::ColsAtCompileTime != Eigen::Dynamic && MatType::ColsAtCompileTime != arr().shape(1))*/) { // Check cols are the same
            std::cout << "Cannot convert to Eigen from Numpy Python" << std::endl;
            return NULL;
        }
        
        return pyArray;
    }

  
    static void construct(PyObject* pyObj, bp::converter::rvalue_from_python_stage1_data* data) {
    #ifdef DEBUG
        PyArrayObject * pyArray = reinterpret_cast<PyArrayObject*>(pyObj);
        assert((PyArray_DIMS(pyArray)[0]<INT_MAX) && (PyArray_DIMS(pyArray)[1]<INT_MAX));
    #endif
        void* storage = ((bp::converter::rvalue_from_python_storage<MatType>*)((void*)data))->storage.bytes;
        EigenObjectAllocator<MatType>::allocate(pyObj, storage);
        data->convertible = storage;
    }
     
    static void registration() {
        bp::converter::registry::push_back(reinterpret_cast<void *(*)(_object *)>(&EigenFromPython::convertible),
                    &EigenFromPython::construct, bp::type_id<MatType>());
    }
};  // EigenFromPython
   


template<typename MatType>
struct EigenFromPython< Eigen::MatrixBase<MatType> > {
     typedef EigenFromPython<MatType> EigenFromPythonDerived;
     typedef Eigen::MatrixBase<MatType> Base;
     
     static void* convertible(PyObject* pyObj) {
       return EigenFromPythonDerived::convertible(pyObj);
     }
     
     static void construct(PyObject* pyObj,
                           bp::converter::rvalue_from_python_stage1_data* memory) {
       EigenFromPythonDerived::construct(pyObj,memory);
     }
     
     static void registration() {
       registry::push_back(reinterpret_cast<void *(*)(_object *)>(&EigenFromPython::convertible), &EigenFromPython::construct,bp::type_id<MatType>());
     }
};  // EigenFromPython
   
// class for converting Eigen vector type to Python understandable type     
//   The Python type of the ndarray is PyArray_Type. In C, every ndarray is a pointer to a PyArrayObject structure. 
//    The ob_type member of this structure contains a pointer to the PyArray_Type typeobject.
//
template<typename MatType>
struct EigenToPython {
    static void* convertible(PyArrayObject* pyArray) {
        if (!PyArray_Check(pyArray))
            return NULL;
 
        if (MatType::IsVectorAtCompileTime) {
            // Special care of scalar matrix of dimension 1x1.
            if (PyArray_DIMS(pyArray)[0] == 1 && PyArray_DIMS(pyArray)[1] == 1)
                return pyArray;
         
            if (PyArray_DIMS(pyArray)[0] > 1 && PyArray_DIMS(pyArray)[1] > 1) {
                std::cerr << "The number of dimensions of the object is not the one of a vector" << std::endl;
                return NULL;
            }
         
            if (((PyArray_DIMS(pyArray)[0] == 1) && (MatType::ColsAtCompileTime == 1))
                || ((PyArray_DIMS(pyArray)[1] == 1) && (MatType::RowsAtCompileTime == 1))) {
                if (MatType::ColsAtCompileTime == 1)
                    std::cerr << "The object is not a column vector" << std::endl;
                else
                    std::cerr << "The object is not a row vector" << std::endl;
                return NULL;
            }
       }
       
        if (PyArray_NDIM(pyArray) != 2) {
            if ( (PyArray_NDIM(pyArray) !=1) || (! MatType::IsVectorAtCompileTime)) {
                std::cerr << "The number of dimensions of the object is not correct." << std::endl;
                return NULL;
            }
        }
       
        if (PyArray_NDIM(pyArray) == 2) {
            const int R = (int)PyArray_DIMS(pyArray)[0];
            const int C = (int)PyArray_DIMS(pyArray)[1];
            if ( (MatType::RowsAtCompileTime != R) && (MatType::RowsAtCompileTime != Eigen::Dynamic))
                return NULL;
            if ( (MatType::ColsAtCompileTime != C) && (MatType::ColsAtCompileTime != Eigen::Dynamic))
                return NULL;
        }

       int arrayDataType = GET_PY_ARRAY_TYPE(pyArray);

        // Check if the Scalar type of the obj_ptr is compatible with the Scalar type of MatType
        if (arrayDataType == NPY_INT) {
             if (!FromTypeToType<int,typename MatType::Scalar>::value) {
                std::cerr << "The Python matrix scalar type (int) cannot be converted into the scalar type of the Eigen matrix. Loss of arithmetic precision" << std::endl;
                return NULL;
             }
        }
        else if (arrayDataType == NPY_LONG) {
            if (!FromTypeToType<long,typename MatType::Scalar>::value) {
                std::cerr << "The Python matrix scalar type (long) cannot be converted into the scalar type of the Eigen matrix. Loss of arithmetic precision" << std::endl;
                return NULL;
            }
        }
        else if (arrayDataType == NPY_FLOAT) {
            if (!FromTypeToType<float,typename MatType::Scalar>::value) {
                std::cerr << "The Python matrix scalar type (float) cannot be converted into the scalar type of the Eigen matrix. Loss of arithmetic precision" << std::endl;
                return NULL;
            }
        }
        else if (arrayDataType == NPY_DOUBLE) {
            if (!FromTypeToType<double,typename MatType::Scalar>::value) {
                std::cerr << "The Python matrix scalar (double) type cannot be converted into the scalar type of the Eigen matrix. Loss of arithmetic precision." << std::endl;
                return NULL;
            }
        }
        else if (arrayDataType == NPY_COMPLEX128) {
         //if (!FromTypeToType<std::complex<double>, typename MatType::Scalar>::value)
         {
            std::cerr << "The Python matrix scalar (complex) type cannot be converted into the scalar type of the Eigen matrix. Loss of arithmetic precision." << std::endl;
            return NULL;
         }
        }
        else if (arrayDataType != NumpyEquivalentType<typename MatType::Scalar>::type_code) {
            std::cerr << "The internal type as no Eigen equivalent." << std::endl;
            return NULL;
        }
       
 #ifdef NPY_1_8_API_VERSION
        if (!(PyArray_FLAGS(pyArray)))
 #else
        if (!(PyArray_FLAGS(pyArray) & NPY_ALIGNED))
 #endif
        {
            std::cerr << "NPY non-aligned matrices are not implemented." << std::endl;
            return NULL;
        }

        return pyArray;
     }
  
     static void construct(PyObject* pyObj, bp::converter::rvalue_from_python_stage1_data* memory) {
        PyArrayObject * pyArray = reinterpret_cast<PyArrayObject*>(pyObj);
        assert((PyArray_DIMS(pyArray)[0] < INT_MAX) && (PyArray_DIMS(pyArray)[1] < INT_MAX));
       
        void* storage = ((bp::converter::rvalue_from_python_storage<MatType>*) ((void*)memory))->storage.bytes;
        EigenObjectAllocator<MatType>::allocate(pyArray,storage);
        memory->convertible = storage;
    }
/*     
    static void registration() {
        bp::converter::registry::push_back(reinterpret_cast<void *(*)(_object *)>(&EigenFromPython<MatType>::convertible), 
                                                    &EigenFromPython<MatType>::construct,
                                                    bp::type_id<MatType>());
    }
*/
    // Given a typesafe to_python conversion function, produces a
    // to_python_function_t which can be registered in the usual way.     
    // required by as_to_python_function.hpp
    static PyObject* convert(MatType const & mat) {
        typedef typename MatType::Scalar Scalar;
        const Index R = mat.rows();
        const Index C = mat.cols();

        // Allocate Python memory
        // Create a new uninitialized array of type
        PyArrayObject* pyArray;
        if (C == 1 && NumpyType::getType() == ARRAY_TYPE) {
            // vectors
            npy_intp shape[1] = { R };
            pyArray = (PyArrayObject*) PyArray_SimpleNew(1, shape,
                                                      NumpyEquivalentType<Scalar>::type_code);
        }
        else {
            // matrices
            npy_intp shape[2] = {R, C};
            pyArray = (PyArrayObject*) PyArray_SimpleNew(2, shape,
                                                      NumpyEquivalentType<Scalar>::type_code);
        }

         // Allocate memory
        EigenObjectAllocator<MatType>::copy(mat, pyArray);

        // Create a np array or matrix instance
        return NumpyType::getInstance().make(pyArray).ptr();
    }
};



template<typename MatType>
struct EigenFromPythonConverter {
    EigenFromPythonConverter() {
        registration();
    }

    static void registration() {
        EigenFromPython<MatType>::registration();

        // Add also conversion to Eigen::MatrixBase<MatType>
        //typedef Eigen::MatrixBase<MatType> MatTypeBase;
        //EigenFromPython<MatTypeBase>::registration();
    }
};  // EigenFromPythonConverter

/*  
//
template<typename MatType>
struct EigenFromPythonConverter< EigenRef<MatType> > {
    EigenFromPythonConverter() {
        registration();
    }

    static void registration() {
        registry::push_back(reinterpret_cast<void *(*)(_object *)>(&EigenFromPython<MatType>::convertible), &EigenFromPython<MatType>::construct, bp::type_id<MatType>());
    }
};  // EigenFromPythonConverter
*/
  
// function to enable type   
template<typename MatType>
void enableEigenPySpecific() {
    //Py_Initialize();
    //np::initialize();
    
    int n = _import_array();
    if (n < 0) {
        PyErr_Print();
        PyErr_SetString(PyExc_ImportError, "XXX failed to import");    
    }   
    
    EigenFromPythonConverter<MatType>();    

    if (check_registration<MatType>()) return;
    bp::to_python_converter<MatType, EigenToPython<MatType> >();
    //EigenToPython<MatType>::registration();
}

/*
void finalizeEigenPySpecific() {
	Py_Finalize();
}
*/ 

// ---------------------------------------------------------------------------
#endif  // PYTHON_EIGEN_CONVERTER_HPP
