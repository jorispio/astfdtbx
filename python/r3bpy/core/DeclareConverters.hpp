#ifndef DECLARE_CONVERTERS_HPP
#define DECLARE_CONVERTERS_HPP

#include <vector>

#include "PythonEigenDeclare.hpp"
#include "converter/python_container_conversion.h"

#include "R3bPyDataTypes.hpp" // for std::vector<Segment>
#include "solver/R3bpLambertProblemSolution.hpp" // for std::vector<R3bpLambertProblemSolution>
#include "propagation/SimulationEvent.hpp" // for std::vector<SimulationEvent>

//
#define PYTHON_REGISTER_CONTAINER_CONVERTER(T, policy)                                                                 \
    {                                                                                                                  \
        boost::python::type_info info = boost::python::type_id<T>();                                                   \
        const boost::python::converter::registration *reg = boost::python::converter::registry::query(info);           \
        if (reg == NULL) {                                                                                             \
            to_tuple_mapping<T>();                                                                                     \
            from_python_sequence<T, policy>();                                                                         \
        } else if ((*reg).m_to_python == NULL) {                                                                       \
            to_tuple_mapping<T>();                                                                                     \
            from_python_sequence<T, policy>();                                                                         \
        }                                                                                                              \
    }

#define PYTHON_REGISTER_EIGEN_CONVERTER(VectorType) enableEigenPySpecific<VectorType>();


inline void DECLARE_CONVERTERS() {
    Py_Initialize();
    np::initialize();

    PYTHON_REGISTER_EIGEN_CONVERTER(Vector3);
    PYTHON_REGISTER_EIGEN_CONVERTER(Vector3cd);
    PYTHON_REGISTER_EIGEN_CONVERTER(VectorXcd);
    PYTHON_REGISTER_EIGEN_CONVERTER(Vector6);
    PYTHON_REGISTER_EIGEN_CONVERTER(Vector7);
    PYTHON_REGISTER_EIGEN_CONVERTER(Vector14);    
    PYTHON_REGISTER_EIGEN_CONVERTER(Matrix3);
    PYTHON_REGISTER_EIGEN_CONVERTER(Matrix6);
    PYTHON_REGISTER_EIGEN_CONVERTER(Matrix7);
    PYTHON_REGISTER_EIGEN_CONVERTER(S14MATRIX);    

    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<double>, variable_capacity_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array2D, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array3D, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array4D, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array5D, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array6D, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(array5D2, fixed_size_policy)
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Vector3>, variable_capacity_policy);
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Vector3cd>, variable_capacity_policy);
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Vector6>, variable_capacity_policy);
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Vector7>, variable_capacity_policy);
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<VectorXcd>, variable_capacity_policy);

    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<SimulationEvent>, variable_capacity_policy);
    
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Segment>, variable_capacity_policy);
    
    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<R3bpLambertProblemSolution>, variable_capacity_policy);    
}

#endif  // DECLARE_CONVERTERS_HPP
