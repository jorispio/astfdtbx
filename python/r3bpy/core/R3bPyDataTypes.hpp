/* --------------------------------------------------------------------------- *
 *   This file is part of ASTTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   ASTTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   ASTTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with ASTTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef ASTTBX_PY_DATA_TYPES_HPP
#define ASTTBX_PY_DATA_TYPES_HPP
// -------------------------------------------------------------------------
#include <array>
#include <vector>

#include <boost/python/list.hpp>

#include "segments/Segments.hpp"
#include "HelioLib.hpp"

typedef boost::python::list pyList;
typedef boost::python::tuple pyTuple;

typedef std::vector<double> array1D;

typedef std::array<double, 2> array2D;
typedef std::array<double, 3> array3D;
typedef std::array<double, 4> array4D;
typedef std::array<double, 5> array5D;
typedef std::array<double, 6> array6D;

typedef std::array<array2D, 5> array5D2;

// ------------------------------------------------------------------------- 
#endif

