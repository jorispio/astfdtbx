// $Id$
// -------------------------------------------------------------------------
#ifndef __PY_CONTINUATION_PROCEDURE_HPP__
#define __PY_CONTINUATION_PROCEDURE_HPP__

#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#include "orbit/continuation/ContinuationProcedure.hpp"


// -------------------------------------------------------------------------
struct PyContinuationProcedure : public ContinuationProcedure {
 public:
    PyContinuationProcedure(ContinuationMethod method, double s_start, double s_stop, double s_max_step, double min_step, double tol, int max_iter, double redfac, bool verbose=false) 
            : ContinuationProcedure(method, s_start, s_stop, s_max_step, min_step, tol, max_iter, redfac, verbose) { };
};


#endif  // __PY_CONTINUATION_PROCEDURE_HPP__

