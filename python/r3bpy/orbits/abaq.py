"""Managemnt of abcus data.
"""
import os
from os.path import sep as SEPARATOR

import h5py
import json

import numpy as np

SRC_DIR_NAME = os.path.dirname(__file__) + '/'


class Abaq():
    """Abaq class is used to update and read an abacus of R3BP solution orbits.
    """

    def __init__(self, directory=SRC_DIR_NAME, file='r3bp_abaq.hdf5'):
        """Initialize class with
            directory    (optional) directory path of the HDF5 abacus file
            file   (optional) abacus file name
        """        
        self.i = 0
        self.file = ''.join([directory, SEPARATOR, file])
        if os.path.exists(file):
            self.f = h5py.File(file, 'a')
        else:
            self.f = h5py.File(file, "w")
        self.f.close()

    def get_source(self):
        return self.file
        
    def add_to_abaq(self, cb1, cb2, family, point, mu_ratio, init_point, period, description, dict=None, trajectory=None):
        """Add data to abacus file
            cb1 is the primary object
            cb2 is the primary object
            family is the orbit family
            point is the center point among P1 and P2 for primaries, L1, L2 L3, L4 and L5.
            mu_ratio mu ratio used for computation
            init_point orbit initial point
            period orbital period
            description short orbit descrtion
            trajectory (optional) trajectory points of the orbit solution
        """
        with h5py.File(self.file, 'a') as f:
            path = cb1 + '/' + cb2 + '/' + family + '/' + point
            is_group = path in f
            if not is_group:
                grp = f.create_group(path)
                self._create_values(grp, mu_ratio, init_point, period, description, trajectory)
                
            else:
                grp = f[path]
                self._update_values(grp, mu_ratio, init_point, period, description, trajectory)

            f.flush()
            
    def remove_from_abaq(self, cb1, cb2, family, point):
        """Remove an orbit from abaq.
        """
        last_removed_data = {}
        last_removed_data["cb1"] = cb1
        last_removed_data["cb2"] = cb2
        last_removed_data["family"] = family
        last_removed_data["point"] = point
        dsname = cb1 + "/" + cb2 + "/" + family + "/" + point 
        with h5py.File(self.file, 'a') as f:
            try:
                grp = f[dsname]
                for k in grp.keys():
                    last_removed_data[k] = grp[k] [()]
                del f[dsname]
            except KeyError:
                print("Missing key. Object does not exist")
        return last_removed_data
        
    def _create_values(self, grp, mu_ratio, init_point, period, description, trajectory=None):
        values = [mu_ratio, init_point]
        
        dataset = grp.create_dataset("mu_ratio", (1,))
        dataset[0] = mu_ratio

        dataset = grp.create_dataset("init_point", data=init_point, compression="gzip", compression_opts=9)
        
        dataset = grp.create_dataset("period", (1,))
        dataset[0] = period

        dataset = grp.create_dataset("description", data=description)
                
        if trajectory is not None:        
            dataset = grp.create_dataset("trajectory", data=trajectory, dtype='f', compression="gzip", compression_opts=9)

    def _update_values(self, grp, mu_ratio, init_point, period, description, trajectory=None):        
        grp["mu_ratio"][0] = mu_ratio
        grp["description"][()] = description
        grp["init_point"][...] = init_point
        grp["period"][...] = period
        
        if "trajectory" in grp:
            grp["trajectory"][...] = trajectory
        elif trajectory is not None:
            dataset = grp.create_dataset("trajectory", data=trajectory, compression="gzip", compression_opts=9)

    def _append_to_dataset(self, dset, values):        
        shape = (4, )
        dset.resize((self.i + 1, ) + shape)
        dset[self.i] = [values]
        self.i += 1
        h5f.flush()

    def get_source(self):
        """Return the current abacus file name."""
        return self.file
        
    def list(self, dsname=None):
        """List available data in the group dsname.
        dsname is constructed from primary name, orbit family and center point.
        For instance:
            dsname = "Earth/Moon/Halo/L1"
        """
        #print(list(f))
        #print("->",list(f["Earth"]))
        #print("-->",list(f["Earth"]["Moon"]))
        #print("-->",list(f["Earth"]["Moon"][family]))
        grps = []
        with h5py.File(self.file, 'r') as f:
            if dsname:
                grps = list(f[dsname].keys())
            else:
                grps = list(f.keys())
        return grps

    def _get_data(self, f, path):
        if path in f:
            grp = f[path]
            mu_ratio = grp['mu_ratio'][()]
            period = grp['period'][()]
            init_point = grp['init_point'][()]
            trajectory = None
            if 'trajectory' in grp:
                trajectory = grp['trajectory'][()]
            description = grp['description'][()]
        else:
            raise ValueError('Unknown ' + family + ' family for ' + cb1 + ' and ' + cb2)
        return mu_ratio, init_point, period, trajectory, description

    def get_data(self, cb1, cb2, family, point=None):
        """Get data from abacus 
        """
        with h5py.File(self.file, 'r') as f:
            if point:
                path = cb1 + '/' + cb2 + '/' + family + '/' + point                
                if path in f:
                    mu_ratio, init_point, period, trajectory, description = self._get_data(f, path)
                else:
                    raise ValueError('Unknown '+ family + ' family for ' + cb1 + ' and ' + cb2)
                out = (mu_ratio, init_point, period, trajectory, description)

            else:
                out = []
                path = cb1 + '/' + cb2 + '/' + family
                l = list(f[path])
                for sg in l:
                    grp = f[path + '/' + sg]
                    mu_ratio, init_point, period, trajectory, description = self._get_data(f, path + '/' + sg)                    
                    out.append((mu_ratio, init_point, period, trajectory, description))

        return out

    def interpolate_orbit(self, cb1, cb2, parameter):
        data = self.get_data(self, cb1, cb2)
        dataset = []
        with h5py.File(self.file, 'r') as f:
            dataset = f[cb1 + '/' + cb2]

        return dataset

    def import_from_json(self, path, name):
        """Import a JSON file to current abacus.
            The JSON file should include the following attributes:
                group   concatenation of primary name, secondary name
                family  orbit family type among, for instance {'Halo', 'NRHO', 'DRO', 'Lyapunov'}
                mu_ratio  value of the mu ratio used for computation
                init_point orbit starting point, in adimensional unit of position and velocity
                period orbit period, in adimensional unit of time
                description  short description string
                trajectory (optional)  point
        """
        pth = ''.join([path, "/", name, '.json'])
        with open(path) as fid:
            abacus_dict = json.load(fid)

        group_name = abacus_dict["group"] + '/' + abacus_dict["family"]
        trajectory = None
        if "trajectory" in abacus_dict:
            trajectory = abacus_dict["trajectory"]
        self._create_values(group_name, abacus_dict["mu_ratio"], abacus_dict["init_point"], abacus_dict["period"], abacus_dict["description"], trajectory)

        return abacus_dict

    def export_to_json(self, name):
        """Export current abacus to a JSON file. """
        abacus_dict = []

        with h5py.File(self.file, 'r') as f:            
            for cb1 in list(f):
                for cb2 in list(f[cb1]):
                    abacus_subdict = {}
                    abacus_subdict["group"] = cb1 + '/' + cb2
                    for fam in list(f[cb1 + '/' + cb2 + '/']):                        
                        abacus_subdict["family"] = fam
                        for cp in list(f[cb1 + '/' + cb2 + '/' + fam]):
                            abacus_subdict["center_point"] = cp
                            
                            mu_ratio, init_point, period, trajectory, description = self.get_data(cb1, cb2, fam, cp)
                            abacus_subdict["mu_ratio"] = mu_ratio.tolist()
                            abacus_subdict["period"] = period.tolist()
                            abacus_subdict["init_point"] = init_point.tolist()
                            abacus_subdict["trajectory"] = trajectory.tolist()
                            abacus_subdict["description"] = description
                            abacus_dict.append(abacus_subdict)

        filepath = ''.join([SRC_DIR_NAME, name, '.json'])
        with open(filepath, 'w') as f:
            json.dump(abacus_dict, f, indent=4, sort_keys=True)


    def __str__(self):
        s = "Abaq=\n"
        l1 = self.list()
        for s1 in l1:
            s = s + s1 + "\n"
            l2 = self.list(s1)
            for s2 in l2:
                s = s + "  " + s2 + "\n"      
                l3 = self.list(s1 + "/" + s2)
                for s3 in l3:                
                    l4 = self.list(s1 + "/" + s2 + "/" + s3)
                    s = s + "  " + "  " + s3 + ": " + str(l4) + "\n"
        return s
        
        
