// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>		// to expose c++ enum
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/return_internal_reference.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>


#include "pyLissajous.hpp"
#include "pyOrbits.hpp"

using namespace boost::python;

#include "DeclareConverters.hpp"

#include "segments/OrbitSegment.hpp"
#include "segments/Segments.hpp"
#include "orbit/DistantRetrogradeOrbit.hpp"
#include "orbit/HaloOrbit.hpp"
#include "orbit/LissajousOrbit.hpp"
#include "orbit/LongShortPeriodicOrbit.hpp"
#include "orbit/LyapunovOrbit.hpp"
#include "orbit/special/LissajousEight.hpp"
#include "R3BProblem.hpp"
#include "R3bPyDataTypes.hpp"

#include "pyContinuation.hpp"

// ------------------------------------------------------------------------
boost::python::tuple propagate_to_list(OrbitSolution &orbit, pyList &tspan_, double timeStep = -1) {
    // FIXME accept numpy.ndarray instead of list for x_

    if (boost::python::len(tspan_) != 2) {
        throw PythonException("Wrong size for time span. Must be 2.");
    }

    double tspan[2];
    for ( int i = 0; i < 2; i++){
        tspan[i] = (boost::python::extract<double>((tspan_)[i]));
    }

    std::vector<double> trajectoryTime;
    std::vector<Vector7> trajectoryState;
    orbit.propagate(tspan, timeStep, trajectoryTime, trajectoryState);

    return boost::python::make_tuple(trajectoryTime, trajectoryState);
}



BOOST_PYTHON_FUNCTION_OVERLOADS(OrbitSolution_propagate_list_overloads, propagate_to_list, 2, 3)

// -------------------------------------------------------------------------

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLyapunovOrbit_overloads, PyLyapunovOrbit::find, 1, 5)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLyapunovOrbit_InitGuess_overloads, PyLyapunovOrbit::findOrbitByContinuation, 2, 6)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findPeriodOrbit_overloads, PyPeriodicOrbit::find, 1, 5)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findPeriodOrbit_InitGuess_overloads, PyPeriodicOrbit::findOrbitByContinuation, 2, 6)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findDrOrbit_overloads, PyDistantRetrogradeOrbit::find, 1, 5)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousMultiPointsOrbit_overloads1, PyLissajous::find, 2, 6)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousMultiPointsOrbit_overloads2, PyLissajous::find_segments, 1, 5)

BOOST_PYTHON_MODULE(_orbits)
{
    // Register std converters to python lists if not already registered by some
    DECLARE_CONVERTERS();

    PYTHON_REGISTER_CONTAINER_CONVERTER(std::vector<Segment>, variable_capacity_policy)


    enum_<HaloOrbitFamily>("HaloOrbitFamily")
        .value("NORTHERN", NORTHERN)
        .value("SOUTHERN", SOUTHERN)
        .export_values();

    enum_<LyapunovOrbitFamilyType>("LyapunovOrbitFamilyType")
        .value("PLANAR", PLANAR)
        .value("VERTICAL", VERTICAL)
        .export_values();

    enum_<PeriodicOrbitClass>("PeriodicOrbitClass")
        .value("LONG_PERIOD", LONG_PERIOD)
        .value("SHORT_PERIOD", SHORT_PERIOD)
        .export_values();

    enum_<ContinuationMethod>("ContinuationMethod")
        .value("AMPLITUDE_PARAMETER", ContinuationMethod::AMPLITUDE_PARAMETER)
        .value("POSITION", ContinuationMethod::POSITION)
        .value("VELOCITY", ContinuationMethod::VELOCITY)
        .value("PSEUDO_ARC_LENGTH", ContinuationMethod::PSEUDO_ARC_LENGTH)
        .value("JACOBI_CONSTANT", ContinuationMethod::JACOBI_CONSTANT)
        .export_values();

    /** Continuation wrapper. */
    class_<PyContinuationProcedure>("ContinuationProcedure", init<ContinuationMethod,double,double,double,double,double,int,double,bool>(
                    (boost::python::arg("method"),
                    boost::python::arg("start"),
                    boost::python::arg("stop"),
                    boost::python::arg("step"),
                    boost::python::arg("min_step")=1e-6,
                    boost::python::arg("tol")=1e-9,
                    boost::python::arg("max_iter")=10000,
                    boost::python::arg("redfac")=2.0,                    
                    boost::python::arg("verbose")=false)))
        .def("is_completed", &PyContinuationProcedure::is_completed)
        .def("next", &PyContinuationProcedure::next)
        .def("get_parameter_value", &PyContinuationProcedure::get_parameter_value);
    

    class_<OrbitSolution>("OrbitSolution")
        .def(init<double, Vector6, double>((boost::python::arg("mu_ratio"),
                    boost::python::arg("x_init"),
                    boost::python::arg("period"))))
        .def_readonly("is_valid", &OrbitSolution::is_valid)
        .def_readonly("type", &OrbitSolution::type)
        .def_readonly("librationPoint", &OrbitSolution::librationPoint)
        .def_readonly("mu", &OrbitSolution::mu)
        .def_readonly("Ax", &OrbitSolution::Ax)
        .def_readonly("Ay", &OrbitSolution::Ay)
        .def_readonly("Az", &OrbitSolution::Az)
        .def_readonly("r0", &OrbitSolution::get_initial_position)
        .def_readonly("v0", &OrbitSolution::get_initial_velocity)
        .def_readonly("period", &OrbitSolution::period)
        .def_readonly("omega", &OrbitSolution::omega)
        .def_readonly("nu", &OrbitSolution::nu)
        .def_readonly("mnu1", &OrbitSolution::mnu1)
        .def_readonly("mnu2", &OrbitSolution::mnu2)
        .def_readonly("jacobi_constant", &OrbitSolution::get_jacobi_constant)
        .def("get_jacobi_constant", &OrbitSolution::get_jacobi_constant)        
        .def_readonly("monodromy", &OrbitSolution::getMonodromyMatrix)
        .def_readonly("jacobian", &OrbitSolution::jacobian)        
        .def("get_stability_index", &OrbitSolution::get_stability_index)
        .def("get_periapsis", &OrbitSolution::get_periapsis)
        .def("propagate", &propagate_to_list, OrbitSolution_propagate_list_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("tspan"),
                    boost::python::arg("tstep")=-1)));

        
    class_<Segment>("OrbitSegment")
        .def_readonly("r0", &Segment::ri)
        .def_readonly("v0", &Segment::vi)
        .def_readonly("tau", &Segment::get_tof)
        .def_readonly("rf", &Segment::rf)
        .def_readonly("vf", &Segment::vf)
        .def("get_init_state", &Segment::get_init_state);

    class_<Segments>("Segments", init< std::vector<Segment> >())
        .def("compute_dv", &Segments::compute_dv);
            
    /** Multi-segment Lissajous orbit. */
    class_<PyLissajous>("LissajousMultiPointsOrbit", init<LibrationPointProperty>())
        .def("find", &PyLissajous::find, findLissajousMultiPointsOrbit_overloads1())
        .def("find", &PyLissajous::find_segments, findLissajousMultiPointsOrbit_overloads2())
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyHaloOrbit>)
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyLissajousOrbit>)
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyLissajousEightOrbit>)
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyPeriodicOrbit>)
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyLyapunovOrbit>)
        .def("get_segments", &PyLissajous::get_segments_from_solution)
        .def("get_segments", &PyLissajous::get_segment_initial_state_from_orbit<PyLissajousOrbit>)
        .def("get_orbit_property", &PyLissajous::get_orbit_property);

           

    /** Lissajous orbit. */
    class_<PyLissajousOrbit>("LissajousOrbit", init<LibrationPointProperty>())
        .def("set_parameters", &LissajousOrbit::set_parameters)
        .def("get_inplane_frequency", &LissajousOrbit::get_inplane_frequency)
        .def("find_orbit", &PyLissajousOrbit::find, findLissajousOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step")=1.0)))
        .def("find_orbit", &PyLissajousOrbit::findOrbitByContinuation, findLissajousOrbit_InitGuess_overloads(
                   (boost::python::arg("x"), 
                    boost::python::arg("init_orbit"), 
                    boost::python::arg("verbose")=false, 
                    boost::python::arg("max_iter")=100, 
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9)));

    // see https://stackoverflow.com/questions/35886682/passing-specific-arguments-to-boost-python-function-with-default-arguments
    class_<PyHaloOrbit>("HaloOrbit", init<LibrationPointProperty, optional<HaloOrbitFamily> >())
        .def("set_parameter", &HaloOrbit::set_parameter)
        .def("get_inplane_frequency", &HaloOrbit::get_inplane_frequency)
        .def("find_orbit", &PyHaloOrbit::find, findHaloOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step")=1.0)))
        .def("find_orbit", &PyHaloOrbit::findOrbitByContinuation, 
                findHaloOrbit_InitGuess_overloads(
                   (boost::python::arg("init_orbit"), 
                    boost::python::arg("procedure"),
                    boost::python::arg("verbose")=false, 
                    boost::python::arg("max_iter")=20, 
                    boost::python::arg("tol")=1e-6, 
                    boost::python::arg("xtol")=1e-9,                     
                    boost::python::arg("max_step")=1.0)));

    class_<PyLissajousEightOrbit>("LissajousEightOrbit", init<LibrationPointProperty>())
        .def("set_parameter", &LissajousEight::set_parameter)
        .def("get_inplane_frequency", &LissajousEight::get_inplane_frequency)
        .def("find_orbit", &PyLissajousEightOrbit::find, findLissajousEightOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step")=1.0)))
        .def("find_orbit", &PyLissajousEightOrbit::findOrbitByContinuation, findLissajousEightOrbit_InitGuess_overloads());

    /** Lyapunov orbit. */
    class_<PyLyapunovOrbit>("LyapunovOrbit", init<LibrationPointProperty, LyapunovOrbitFamilyType>())
        .def("set_parameter", &LyapunovOrbit::set_parameter)
        .def("get_inplane_frequency", &LyapunovOrbit::get_inplane_frequency)
        .def("find_orbit", &PyLyapunovOrbit::find, findLyapunovOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step"))))
        .def("find_orbit", &PyLyapunovOrbit::findOrbitByContinuation, findLyapunovOrbit_InitGuess_overloads(
                   (boost::python::arg("x"), 
                    boost::python::arg("init_orbit"), 
                    boost::python::arg("verbose")=false, 
                    boost::python::arg("max_iter")=100, 
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9)));

    /** DRO. */
    class_<PyDistantRetrogradeOrbit>("DistantRetrogradeOrbit", init<double, PrimaryPointId>())
        .def("find_orbit", &PyDistantRetrogradeOrbit::find, findDrOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step")=1.0)))
        .def("find_orbit", &PyDistantRetrogradeOrbit::findOrbitByContinuation);

    class_<PyPeriodicOrbit>("LongShortPeriodicOrbit", init<LibrationPointProperty, PeriodicOrbitClass>())
        .def("get_inplane_frequency", &LongShortPeriodicOrbit::get_inplane_frequency)
        .def("set_parameter", &LongShortPeriodicOrbit::set_parameter)
        .def("find_orbit", &PyPeriodicOrbit::find, findPeriodOrbit_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("verbose")=false,
                    boost::python::arg("max_iter")=100,
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9,
                    boost::python::arg("max_step")=1.0)))
        .def("find_orbit", &PyPeriodicOrbit::findOrbitByContinuation, findPeriodOrbit_InitGuess_overloads(
                   (boost::python::arg("x"), 
                    boost::python::arg("init_orbit"), 
                    boost::python::arg("verbose")=false, 
                    boost::python::arg("max_iter")=100, 
                    boost::python::arg("tol")=1e-6,
                    boost::python::arg("xtol")=1e-9)));

}

