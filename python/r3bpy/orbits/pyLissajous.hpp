// $Id$
// -------------------------------------------------------------------------
#ifndef _PY_LISSAJOUS_HPP__
#define _PY_LISSAJOUS_HPP__

#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#include <boost/python/tuple.hpp>
#include "boost/tuple/tuple_io.hpp"

#include "R3bPyDataTypes.hpp"
#include "R3BProblem.hpp"
#include "orbit/LissajousOrbit.hpp"
#include "orbit/LyapunovOrbit.hpp"
#include "lissajous/MultiPointLissajousOrbit.hpp"
#include "segments/OrbitSegment.hpp"
#include "segments/Segments.hpp"

struct PyLissajous : public Lissajous {
public:
    PyLissajous(const LibrationPointProperty &point) : Lissajous(point) { }

    template<class OrbitClass>
    std::vector<Segment> get_segment_initial_state_from_orbit(OrbitClass &orbit, uint n_segments) {
        return Lissajous::get_analytical_segments(&orbit, n_segments);
    }

    std::vector<Segment> get_segments_from_solution(const OrbitSolution& orbit, uint n_segments) {
        return Lissajous::get_segments_from_solution(orbit, n_segments);
    }

    std::vector<Segment> find(const OrbitSolution& orbit, uint n_segments, bool verbose = true, int max_iter = 100, double atol=1e-6, double rtol=1e-6) {
        std::vector<Segment> segments = Lissajous::get_segments_from_solution(orbit, n_segments);
        return Lissajous::find(segments, verbose, max_iter, atol, rtol);
    }

    std::vector<Segment> find_segments(std::vector<Segment> segments, bool verbose = true, int max_iter = 100, double atol=1e-6, double rtol=1e-6) {
        return Lissajous::find(segments, verbose, max_iter, atol, rtol);
    }
};

#endif  // _PY_LISSAJOUS_HPP__