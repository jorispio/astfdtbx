# We import all symbols in the lissajous namespace (also the ones we do not use
# in this file, but we still want in the namespace lissajous)
from ._orbits import *


def print_info(orbit, TU=1):
    print("Orbit type           : {:s}".format(orbit.type))
    print("      period         : {:f}".format(orbit.period * TU))
    print("      Libration Point: L{:d}".format(orbit.librationPoint.id+1))
    print("      Primaries      :  {:s}".format(""))



def build_orbit_family(orbit_type_inst, ax_min, ax_max, ax_step, ax_min_step):
    """Compute by discrete continuation a family orbit."""
    current_orbit = None
    orbit_family_sols = []

    x = ax_min
    xold = x
    current_step = ax_step
    while (x < ax_max) and (current_step > ax_min_step):
        if current_orbit is None:
            orbit = orbit_type_inst.find_orbit(x, False, 50, 1e-6, 1.0)
        else:  # use previous solution to compute new Halo
            orbit = orbit_type_inst.find_orbit(x, current_orbit, False, 20, 1e-6, 1.)        

        if (orbit and orbit.is_valid) or (orbit is not None):
            current_orbit = orbit
            orbit_family_sols.append(current_orbit)
            current_step = ax_step
            xold = x
            x = xold + current_step
        else:
            #current_halo_orbit = None
            current_step = current_step / 2
            x = xold + current_step

    return orbit_family_sols
    

