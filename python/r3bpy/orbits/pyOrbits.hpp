// $Id$
// -------------------------------------------------------------------------
#ifndef _PY_ORBITS_HPP__
#define _PY_ORBITS_HPP__

#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#include "DeclareConverters.hpp"
#include "segments/OrbitSegment.hpp"
#include "segments/Segments.hpp"
#include "orbit/LissajousOrbit.hpp"
#include "orbit/HaloOrbit.hpp"
#include "orbit/DistantRetrogradeOrbit.hpp"
#include "orbit/special/LissajousEight.hpp"
#include "orbit/LongShortPeriodicOrbit.hpp"
#include "orbit/continuation/ContinuationMethod.hpp"
#include "orbit/LyapunovOrbit.hpp"
#include "R3BProblem.hpp"
#include "R3bPyDataTypes.hpp"

#include "pyContinuation.hpp"

// -------------------------------------------------------------------------
struct PyLissajousOrbit : public LissajousOrbit {
public:
    PyLissajousOrbit(const LibrationPointProperty &point) : LissajousOrbit(point) { }

    std::vector<Segment> get_analytical_segments(uint n_segments, uint n_revolutions,
                                                   double Ax_, double phix_,
                                                   double Az_, double phiz_) {
        set_parameters(Ax_, phix_, Az_, phiz_);
        return LissajousOrbit::get_analytical_segments(n_segments);
    }

    OrbitSolution find(double Ax_, double phi_x, double Az_, double phi_z, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = LissajousOrbit::find(Ax_, phi_x, Az_, phi_z, verbose, max_iter, ftol);
        return property;
    }

    OrbitSolution findOrbitByContinuation(double Az_, OrbitSolution &initialGuess, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = LissajousOrbit::find_by_continuation(Az_, initialGuess, verbose, max_iter, ftol);
        return property;
    }
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousOrbit_overloads, PyLissajousOrbit::find, 4, 8)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousOrbit_InitGuess_overloads, PyLissajousOrbit::findOrbitByContinuation, 2, 6)

// -------------------------------------------------------------------------
struct PyHaloOrbit : public HaloOrbit {
public:
    PyHaloOrbit(const LibrationPointProperty &point,
                const HaloOrbitFamily &family = HaloOrbitFamily::NORTHERN)
            : HaloOrbit(point, family) { }

    OrbitSolution find(double Az_, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9, double max_step=0.1) {
        return HaloOrbit::find(Az_, verbose, max_iter, ftol, xtol, max_step);
    }
    
    OrbitSolution findOrbitByContinuation(OrbitSolution &init_orbit, PyContinuationProcedure& procedure, bool verbose = false, 
                                            int max_iter = 100, double ftol=1e-6, double xtol=1e-9, double max_step=0.1) {
        return HaloOrbit::find_by_continuation(init_orbit, procedure, verbose, max_iter, ftol, xtol, max_step);
    }
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findHaloOrbit_overloads, PyHaloOrbit::find, 1, 6)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findHaloOrbit_InitGuess_overloads, PyHaloOrbit::findOrbitByContinuation, 2, 7)

// -------------------------------------------------------------------------
struct PyLissajousEightOrbit : public LissajousEight {
public:
    PyLissajousEightOrbit(const LibrationPointProperty &point) : LissajousEight(point, HaloOrbitFamily::NORTHERN) { }

    OrbitSolution find(double Az_, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9, double max_step=0.1) {
        OrbitSolution property = LissajousEight::find(Az_, verbose, max_iter, ftol, xtol, max_step);
        return property;
    }

    OrbitSolution findOrbitByContinuation(double p, OrbitSolution &init_orbit, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-6, double max_step=0.1) {
        OrbitSolution property = LissajousEight::find_by_continuation(p, init_orbit, ContinuationMethod::AMPLITUDE_PARAMETER, verbose, max_iter, ftol, xtol, max_step);
        return property;
    }
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousEightOrbit_overloads, PyLissajousEightOrbit::find, 1, 6)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(findLissajousEightOrbit_InitGuess_overloads, PyLissajousEightOrbit::findOrbitByContinuation, 2, 7)

// -------------------------------------------------------------------------
struct PyPeriodicOrbit : public LongShortPeriodicOrbit {
public:
    PyPeriodicOrbit(const LibrationPointProperty &point, PeriodicOrbitClass periodicOrbitClass_)
            : LongShortPeriodicOrbit(point, periodicOrbitClass_) { }

    OrbitSolution find(double Az_, bool verbose = true, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        return LongShortPeriodicOrbit::find(Az_, verbose, max_iter, ftol);
    }

    OrbitSolution findOrbitByContinuation(double Az_, OrbitSolution &initialGuess, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        return LongShortPeriodicOrbit::find_by_continuation(Az_, initialGuess, verbose, max_iter, ftol);
    }
};


// -------------------------------------------------------------------------
struct PyLyapunovOrbit : public LyapunovOrbit {
public:
    PyLyapunovOrbit(const LibrationPointProperty &point, LyapunovOrbitFamilyType planarType) : LyapunovOrbit(point, planarType) { }

    OrbitSolution find(double dx, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = LyapunovOrbit::find(dx, verbose, max_iter, ftol);
        return property;
    }

    OrbitSolution findOrbitByContinuation(double dx, OrbitSolution &initialGuess, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = LyapunovOrbit::find_by_continuation(dx, initialGuess, verbose, max_iter, ftol);
        return property;
    }
};

// -------------------------------------------------------------------------
struct PyDistantRetrogradeOrbit : public DistantRetrogradeOrbit {
public:
    PyDistantRetrogradeOrbit(double mu_ratio, PrimaryPointId primaryPointId) : DistantRetrogradeOrbit(mu_ratio, primaryPointId) { }

    OrbitSolution find(double vydot, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = DistantRetrogradeOrbit::find(vydot, verbose, max_iter, ftol);
        return property;
    }

    OrbitSolution findOrbitByContinuation(double vydot, OrbitSolution &initialGuess, bool verbose = false, int max_iter = 100, double ftol=1e-6, double xtol=1e-9) {
        OrbitSolution property = DistantRetrogradeOrbit::find_by_continuation(vydot, initialGuess, verbose, max_iter, ftol);
        return property;
    }
};

#endif  // _PY_ORBITS_HPP__
