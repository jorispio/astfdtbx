"""Some bodies of the Solar System to define RTBP. """
import numpy as np
import math
from collections import namedtuple

from .. import core as constants
from ..main import BodyEphemerides
from ..visu import plot_trajectory

REF_EPOCH = 2451545.0
EPHEMERIS = BodyEphemerides()


class Body(namedtuple("_Body", ["parent", "id", "name", "mu", "orbit", "epoch", "frame", "radius", "texture"],)):

    def __new__(cls, parent, id, name, mu, orbit, epoch, frame, radius, texture):
        return super().__new__(cls, parent, id, name, mu, orbit, epoch, frame, radius, texture)

    def __str__(self):
        return f"{self.name}"



class CelestialBody(Body):

    def plot_orbit(self, timespan, ax, a_m=1, color=None, is2d=False, alpha=1.0, legend=None):
        ts = np.arange(timespan[0], timespan[1])
        rs = []
        for t in ts:
            r, _ = EPHEMERIS.get_position(self.id, t)
            rs.append(r)
            
        plot_trajectory(rs, a_m=a_m, color=color, alpha=alpha, legend=legend, ax=ax)



# [1] Standish, E.M. and Williams, E.M. (1992) Orbital Ephemerides of the Sun, Moon, and Planets. 
#     In: P.K., S., Ed., Explanatory Supplement to the Astronomical Almanac, University Books, Mill Valley, CA, 279-374.  
# [2] https://ssd.jpl.nasa.gov/sats/elem/
#

Sun = CelestialBody(
    parent=None,
    id=0,
    name="Sun",
    mu=constants.MU_SUN,
    orbit = [0, 0, 0, 0, 0, 0],
    epoch = 0,
    frame=None,
    radius=None,
    texture=None
)

Earth = CelestialBody(
    parent=Sun,
    id=3,
    name="Earth",
    mu=constants.MU_EARTH,
    orbit = [0, 0, 0, 0, 0, 0],
    epoch = 0,
    frame="EME2000",
    radius=6378.,
    texture='data/world.topo.bathy.200412.3x5400x2700.jpg'
)

Moon = CelestialBody(
    parent=Earth,
    id=301,    
    name="Moon",
    mu=constants.MU_MOON,
    orbit = [384400.e3, 0.0554,	5.16, 318.15, 125.08, 135.27], # sma(m), ecc, inc(deg), pom(deg), node(deg), mean anomaly(deg)
    epoch = 0, 
    frame=None,
    radius=1737.,
    texture='data/lroc_color_poles_1k.jpg'
)

Jupiter = CelestialBody(
    parent=Sun,
    id=5,
    name="Jupiter",
    mu=constants.MU_JUPITER,
    orbit = [0, 0, 0, 0, 0, 0],
    epoch = 0,
    frame=None,
    radius=69911.,
    texture='data/world.topo.bathy.200412.3x5400x2700.jpg'
)


