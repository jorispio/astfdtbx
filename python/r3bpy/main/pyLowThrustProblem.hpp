#ifndef __PY_R3BP_LOW_TRUST_PROBLEM_HPP__
#define __PY_R3BP_LOW_TRUST_PROBLEM_HPP__

#include <cmath>
#include <iostream>
#include <list>
#include "boost/tuple/tuple_io.hpp"
#include "core/converter/python_totuple.hpp"

#include "lowthrust/propulsion.hpp"
#include "lowthrust/R3bpLowThrustProblem.hpp"
#include "lowthrust/R3bpLowThrustPropagator.hpp"
#include "lowthrust/R3bpOptimalGuidance.hpp"

// -------------------------------------------------------------------------
struct PyLowThrustPropulsionSystem : public ThrusterData {
 public:
    PyLowThrustPropulsionSystem(double thrust, double isp) : ThrusterData(thrust, g0 * isp) {}
    
    double get_force() {
        return Fth;
    }

    double get_g0Isp() {
        return g0Isp;
    }
    
    double get_isp() {
        return g0Isp / g0;
    }
};

// -------------------------------------------------------------------------
struct PyLowThrustProblem : public R3bpLowThrustProblem {
 public:
    PyLowThrustProblem(double mu, const PyLowThrustPropulsionSystem& propulsion_system) : R3bpLowThrustProblem(mu, propulsion_system) {};
  
    R3bpLowThrustProblemSolution py_solve(const pyList& x0, const pyList& xf, double tof, pyList& lambda0) {
        Vector7 pv0, pvf, lambda;
        for ( int i = 0; i < 7; i++){
            pv0[i] = (boost::python::extract<double>((x0)[i]));
            pvf[i] = (boost::python::extract<double>((xf)[i]));
            lambda[i] = (boost::python::extract<double>((lambda0)[i]));
        }  
        return R3bpLowThrustProblem::solve(pv0, pvf, tof, lambda);
    }    
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyLowThrustProblem_overloads, PyLowThrustProblem::py_solve, 4, 4)

// -------------------------------------------------------------------------
class PyR3bpOptimalGuidance : public R3bpOptimalGuidance {
 public:
    /** */
    PyR3bpOptimalGuidance(const R3bpLowThrustPropagator& propagator) : R3bpOptimalGuidance((const TDynamicsBase*) propagator.get_dynamical_model()) { 

    }

    /** */
    std::vector<Vector3> get_control_direction_list(pyList &states, pyList &costates) {
        std::vector<Vector3> uctl;
        for(int idx = 0; idx < boost::python::len(states); ++idx) {
            Vector7 state, costate;
            for ( int i = 0; i < 7; i++){
                state[i] = (boost::python::extract<double>((states[idx])[i]));
                costate[i] = (boost::python::extract<double>((costates[idx])[i]));
            }              
            uctl.push_back(get_control_direction(state, costate));
        }
        return uctl;
    }
    
    /** */
    std::vector<Vector3> get_control_direction_ndarray(std::vector<Vector7> states, std::vector<Vector7> costates) {
        std::vector<Vector3> uctl;
        for(int idx = 0; idx < states.size(); ++idx) {
            uctl.push_back(get_control_direction(states.at(idx), costates.at(idx)));
        }
        return uctl;
    }
    
    /** */
    Vector3 get_control_direction(Vector7 &state, Vector7 &costate) {
        return R3bpOptimalGuidance::GetControlDirection(&state[0], &costate[0]);
    }
};

// -------------------------------------------------------------------------
class PyLowThrustPropagator : public R3bpLowThrustPropagator {
 public:
    /** */
    PyLowThrustPropagator(double mu, const PyLowThrustPropulsionSystem& propulsion_system) : R3bpLowThrustPropagator(mu, propulsion_system, 0.) { 

    }
    
    /** Add an external force to the dynamical model */
    template<typename ForceModel>
    void add_force(const ForceModel &force) {
        addForce(&force);
    }

    /** Set integration tolerances. */
    void set_tol(double atol, double rtol) {
        setTolerances(atol, rtol);
    }

    PyR3bpOptimalGuidance get_guidance() {
        return PyR3bpOptimalGuidance(*this);
    }
    
    inline boost::python::tuple propagate_sol(pyList &x_, const R3bpLowThrustProblemSolution &sol, pyList &tspan_, double timeStep = -1) {
        return propagate_vector(x_, sol.x, tspan_, timeStep);            
    }        
    
    inline boost::python::tuple propagate_array(pyList &x_, pyList &lambda_, pyList &tspan_, double timeStep = -1) {
        Vector7 lambda;
        for ( int i = 0; i < 7; i++){
            lambda(i) = (boost::python::extract<double>((lambda_)[i]));
        }
        return propagate_vector(x_, lambda, tspan_, timeStep);
    } 
        
    /** */
    inline boost::python::tuple propagate_vector(pyList &x_, const Vector7 &lambda, pyList &tspan_, double timeStep = -1) {
        // FIXME accept numpy.ndarray instead of list for x_  

        if (boost::python::len(x_) != 7) {
            //throw PythonException("Wrong size for state. Must be 7.");
            PyErr_SetString(PyExc_RuntimeError, "Wrong size for state. Must be 7.");
        }
        if (boost::python::len(tspan_) != 2) {
            //throw PythonException("Wrong size for time span. Must be 2.");
            PyErr_SetString(PyExc_RuntimeError, "Wrong size for time span. Must be 2.");
        }       

        double x[7];
        for ( int i = 0; i < 7; i++){
            x[i] = (boost::python::extract<double>((x_)[i]));
        }

        double tspan[2];
        for ( int i = 0; i < 2; i++){
            tspan[i] = (boost::python::extract<double>((tspan_)[i]));
        }

        std::vector<Vector7> outputStates;
        std::vector<Vector7> outputCostates;
        std::vector<double> outputTimes;
        try {
            outputStates = propagate(x, lambda, tspan, timeStep);
            outputCostates = getTrajectoryAuxiliary();
            outputTimes = get_time();
        }
        catch (HelioLibException& e) {
            PyErr_SetString(PyExc_RuntimeError, e.what());
        }

        // construct tuple (time, xyz)
        return boost::python::make_tuple(outputTimes, outputStates, outputCostates);
    }
    
    inline S14MATRIX propagate_stm_propagate_array(pyList &x_, pyList &lambda_, pyList &tspan_, double timeStep = -1) {
        Vector7 lambda;
        for ( int i = 0; i < 7; i++){
            lambda(i) = (boost::python::extract<double>((lambda_)[i]));
        }
        return propagate_stm_vector(x_, lambda, tspan_, timeStep);
    } 
            
    /** */
    inline S14MATRIX propagate_stm_vector(pyList &x_, const Vector7 &lambda, pyList &tspan_, double timeStep = -1) {
        // FIXME accept numpy.ndarray instead of list for x_  

        if (boost::python::len(x_) != 7) {
            //throw PythonException("Wrong size for state. Must be 7.");
            PyErr_SetString(PyExc_RuntimeError, "Wrong size for state. Must be 7.");
        }
        if (boost::python::len(tspan_) != 2) {
            //throw PythonException("Wrong size for time span. Must be 2.");
            PyErr_SetString(PyExc_RuntimeError, "Wrong size for time span. Must be 2.");
        }       

        double x[7];
        for ( int i = 0; i < 7; i++){
            x[i] = (boost::python::extract<double>((x_)[i]));
        }

        double tspan[2];
        for ( int i = 0; i < 2; i++){
            tspan[i] = (boost::python::extract<double>((tspan_)[i]));
        }

        S14MATRIX stm;
        try {
            stm = propagate_state_transition_matrix(x, lambda, tspan, timeStep);
        }
        catch (HelioLibException& e) {
            PyErr_SetString(PyExc_RuntimeError, e.what());
        }

        return stm;
    }
     
    /** Provide time derivative from input state. */
    inline Vector14 compute_time_derivative_list(pyList &state_, pyList &lambda_) {        
        Vector7 state;
        for ( int i = 0; i < 7; i++){
            state[i] = (boost::python::extract<double>((state_)[i]));
        }
        Vector7 lambda;
        for ( int i = 0; i < 7; i++){
            lambda[i] = (boost::python::extract<double>((lambda_)[i]));
        }

        return compute_time_derivative(state, lambda);
    }

    /** export ephemeris to file */
    inline void toFile(std::string filename) {
        get_ephemeris_file(filename);
    }
};


#endif  // __PY_R3BP_LOW_TRUST_PROBLEM_HPP__
