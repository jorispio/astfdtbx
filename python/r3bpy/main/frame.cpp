
#if defined(_WIN32)
#include <Python.h>
#include <cmath>
#else
#include <Python.h>
#include <cmath>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <iostream>

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>		// to expose c++ enum
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>

#include <list>

#include "R3bPyDataTypes.hpp"
#include "LibrationPoint.hpp"
#include "propagator/R3bpFrame.hpp"

#include "pyProblem.hpp"

#include "DeclareConverters.hpp"
#include "PythonException.hpp"
#include "HelioLib.hpp"

using namespace boost::python;
namespace p = boost::python;

class PyR3bFrame : public R3bpFrame
{
 private:
    //
    std::vector<Vector3> _to_frame(std::vector<double> &time, std::vector<Vector3> &pos) {
        int n = time.size();
        if (pos.size() != n) {
            throw PythonException("List must have the same size.");
        }

        std::vector<Vector3> output;
        for (int idx=0; idx < n; ++idx) {
            FrameTransform transform = getTransform(GenericDate(time.at(idx)));
            output.push_back(transform.transformPosition(pos.at(idx)));
        }
        return output;
    } 

 public:
    /** Construct frame from problem definition */
    PyR3bFrame(PyR3bProblem problem, PrimaryPointId pointId = P1) : R3bpFrame(problem.get_mu_ratio(), problem.get_omega(), problem.get_primary_point(pointId)) { }
    PyR3bFrame(PyR3bProblem problem, Vector3 point) : R3bpFrame(problem.get_mu_ratio(), problem.get_omega(), point) { }    

    //
    std::vector<Vector3> change_frame_pos_list(pyList &time_, pyList &pos_) { // FIXME accept numpy.ndarray instead of list for x_
        int listLen = boost::python::len(time_);
        int listLen2 = boost::python::len(pos_);        
        if (listLen != listLen2) {
            throw PythonException("List must have the same size.");
        }
        
        std::vector<double> time;
        for ( int i = 0; i < listLen; i++){
            time.push_back(boost::python::extract<double>((time_)[i]));
        }

        std::vector<Vector3> pos;
        for ( int i = 0; i < listLen; i++){
            Vector3 p = boost::python::extract<Vector3>((pos_)[i]);
            pos.push_back(p);
        }
        
        return _to_frame(time, pos);
    }

    //
    std::vector<Vector3> change_pos_frame(double time_, pyList &pos_) { 
        int vec_size = boost::python::len(pos_);
        if (vec_size != 3) {
            throw PythonException("Position must be a 3-element vector.");
        }
        
        std::vector<double> time;
        time.push_back(time_);

        Vector3 p;
        for ( int i = 0; i < 3; i++){
            p[i] = boost::python::extract<double>((pos_)[i]);
        }
        std::vector<Vector3> pos;
        pos.push_back(p);
                            
        return _to_frame(time, pos);
    }   
};

// ------------------------------------------------------------------------- 
BOOST_PYTHON_MODULE(_frame)
{
    // Register std converters to python lists if not already registered by some
    DECLARE_CONVERTERS();

    class_<FramesFactory>("Frames");/*
        .def("GCRF", &FramesFactory::GCRF)
        .def("EME2000", &FramesFactory::EME2000)
        .def("ICRF", &FramesFactory::ICRF)
        .def("ITRF", &FramesFactory::ITRF); */


    class_<PyR3bFrame>("Frame", init<PyR3bProblem, optional<PrimaryPointId>>())
        .def("to_frame", &PyR3bFrame::change_pos_frame)
        .def("to_frame", &PyR3bFrame::change_frame_pos_list);

}

