#ifndef __PY_PROPAGATOR_HPP__
#define __PY_PROPAGATOR_HPP__

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>		// to expose c++ enum
#include <boost/python/list.hpp>        // list
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>

#include <cmath>
#include <iostream>
#include <list>

#include "boost/tuple/tuple_io.hpp"
#include "core/converter/python_totuple.hpp"
#include "propagator/R3bpPropagator.hpp"
#include "propagator/detectors/AxisCrossingDetector.hpp"


/** Expose R3BP propagator */
struct PyPropagator : private Propagator {
public:
    /** */
    PyPropagator(double mu) : Propagator(mu, 0.) { }

    /** Add an external force to the dynamical model */
    template<typename ForceModel>
    void add_force(const ForceModel &force) {
        addForce(&force);
    }

    /** Set integration tolerances. */
    void set_tol(double atol, double rtol) {
        setTolerances(atol, rtol);
    }
    
    void add_axis_crossing_detector(AxisId axis_id, int direction = 0) {    
        AxisCrossingDetector *detector = new AxisCrossingDetector();
        detector->set_axis(axis_id);
        detector->setDirection(-1);
        Propagator::add_detector(detector);
    }
    
    
    /** */
    inline boost::python::tuple propagate_to_list(pyList &x_, pyList &tspan_, double timeStep = -1) {
        // FIXME accept numpy.ndarray instead of list for x_

        /* FIXME
        int nx = boost::python::extract<int>(x_.attr("__len__")());
        if (nx != 6) {
            throw PythonException("Wrong size for input state vector. Must be 6.");
        }

        int nt = boost::python::extract<int>(tspan_.attr("__len__")());
        if (nt != 2) {
            throw PythonException("Wrong size for time span. Must be 2.");
        }
        */

        Vector6 x;
        for ( int i = 0; i < 6; i++){
            x[i] = (boost::python::extract<double>((x_)[i]));
        }

        return propagate_eigen(x, tspan_, timeStep);
    }
    
    
    inline boost::python::tuple propagate_eigen(Vector6 x, pyList &tspan_, double timeStep = -1) {            
        double tspan[2];
        for ( int i = 0; i < 2; i++){
            tspan[i] = (boost::python::extract<double>((tspan_)[i]));
        }
            
        std::vector<Vector7> outputStates;
        std::vector<double> outputTimes;
        try {
            outputStates = propagate(x, tspan, timeStep);
            outputTimes = get_time();
        }
        catch (HelioLibException& e) {
            PyErr_SetString(PyExc_RuntimeError, e.what());
        }

        // construct tuple (time, xyz)
        return boost::python::make_tuple(outputTimes, outputStates);
    }

    /** */
    Matrix6 propagate_stm(pyList &x_, pyList &tspan_) { // FIXME accept numpy.ndarray instead of list for x_
        double timeStep = -1;
        double x[7];
        for ( int i = 0; i < 6; i++){
            x[i] = (boost::python::extract<double>((x_)[i]));
        }
        x[6] = 0;

        double tspan[2];
        for ( int i = 0; i < 2; i++){
            tspan[i] = (boost::python::extract<double>((tspan_)[i]));
        }

        Matrix6 mat;
        try {
            Matrix7 mat77 = propagate_state_transition_matrix(x, tspan, timeStep);
            mat = mat77.block(0, 0, 6, 6);
        }
        catch (HelioLibException& e) {
            PyErr_SetString(PyExc_RuntimeError, e.what());
        }
        return mat;
    }

    std::vector<SimulationEvent> get_events() {
        std::vector<SimulationEvent> events;
        if (get_event_manager() != nullptr) {
            events = get_event_manager()->getEvents();
        }
        return events;
    }

    /** export ephemeris to file */
    inline void toFile(std::string filename) {
        get_ephemeris_file(filename);
    }
};
// -------------------------------------------------------------------------
// overload methods for variable number of inputs
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyPropagator_add_axis_crossing_detector, PyPropagator::add_axis_crossing_detector, 1, 2)

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyPropagator_propagate_eigen_overloads, PyPropagator::propagate_eigen, 2, 3)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyPropagator_propagate_list_overloads, PyPropagator::propagate_to_list, 2, 3)

// -------------------------------------------------------------------------
#endif  //__PY_PROPAGATOR_HPP__
