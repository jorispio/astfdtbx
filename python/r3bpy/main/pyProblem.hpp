#ifndef __PY_R3BP_PROBLEM_HPP__
#define __PY_R3BP_PROBLEM_HPP__

#include <cmath>
#include <iostream>
#include <list>
#include "boost/tuple/tuple_io.hpp"
#include "core/converter/python_totuple.hpp"

#include "R3BProblem.hpp"

struct PyR3bProblem : public R3bProblem {
public:
    PyR3bProblem(double mu) : R3bProblem(mu) {};
    PyR3bProblem(double m1, double m2) : R3bProblem(m1, m2) {};
    PyR3bProblem(double m1, double m2, double r12) : R3bProblem(m1, m2, r12) {};
    PyR3bProblem(double r1, double m1, double r2, double m2) : R3bProblem(r1, m1, r2, m2) {};
            
    std::vector<Vector3> get_libration_points() const {
        double L[5][2], C[5];
        R3bProblem::get_libration_points(L, C);
        std::vector<Vector3> librPoints;
        for (int idx=0; idx < 5; ++idx) {
            librPoints.push_back( Vector3(L[idx][0], L[idx][1], 0));
        }
        return librPoints;
    }

    std::vector<Vector7> to_fixed_frame(std::vector<double> time, std::vector<Vector7> xyz) {
        return R3bProblem::to_fixed_frame(time, xyz);
    }
};

#endif  // __PY_R3BP_PROBLEM_HPP__

