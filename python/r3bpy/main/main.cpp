// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#if defined(_WIN32)
#include <Python.h>
#else
#include <Python.h>
#endif

#if PY_MAJOR_VERSION < 2 || (PY_MAJOR_VERSION == 2 && PY_MINOR_VERSION < 6)
#error Minimum supported Python version is 2.6.
#endif

#include <cmath>
#include <iostream>
#include <list>

#include <boost/python.hpp>
#include <boost/python/class.hpp>
#include <boost/python/converter/registry.hpp>
#include <boost/python/copy_const_reference.hpp>
#include <boost/python/def.hpp>
#include <boost/python/docstring_options.hpp>
#include <boost/python/enum.hpp>		// to expose c++ enum
#include <boost/python/list.hpp>        // list
#include <boost/python/module.hpp>
#include <boost/python/operators.hpp>
#include <boost/python/overloads.hpp>
#include <boost/python/register_ptr_to_python.hpp>
#include <boost/python/self.hpp>
#include <boost/utility.hpp>
#include "boost/tuple/tuple_io.hpp"

#include "R3bPyDataTypes.hpp"
#include "DeclareConverters.hpp"
#include "Jacobi.hpp"
#include "LibrationPoint.hpp"
#include "Manifold.hpp"
#include "PrimaryPoint.hpp"
#include "R3BProblem.hpp"
#include "solver/R3bpLambertProblemSolver.hpp"
#include "PythonException.hpp"

#include "HelioLib.hpp"

#include "pyProblem.hpp"
#include "pyPropagator.hpp"
#include "pyForces.hpp"
#include "pyEphemeris.hpp"
#include "pyLowThrustProblem.hpp"

using namespace std;
using namespace boost::python;
namespace p = boost::python;
typedef boost::python::list pyList;

/*
 * return tuble (L, C)
 *    L: Libration points coordinates in the 2D rotating reference frame. size: 5 x 2
 *    C: Jacobi constant of the non-stable libration points, L1, L2 and L3. size 3x1
 */
inline boost::python::tuple pyComputeLibrationPoints_(double r1, double r2, double m1, double m2) {
    double L_[5][2];
    double Cj_[5];
    libration_point::computeLibrationPoints(r1, r2, m1, m2, L_, Cj_);

    array5D2 L = {{{L_[0][0], L_[0][1]}, {L_[1][0], L_[1][1]}, {L_[2][0], L_[2][1]}, {L_[3][0], L_[3][1]}, {L_[4][0], L_[4][1]}}};    
    array3D C; C = {Cj_[0], Cj_[1], Cj_[2]};
    return boost::python::make_tuple(L, C);
}


// -------------------------------------------------------------------------
struct PyLambertProblem : public R3bpLambertProblemSolver {
 public:
    PyLambertProblem(double mu) : R3bpLambertProblemSolver(mu) { };
    
    boost::python::tuple solve(pyList &x_i, pyList &x_f, double tof, bool print_iteration = false, int maxiter = 100, double xtol=1e-6) {
        double r_init[6];
        Vector3 v_init = Vector3::Zero();
        for ( int i = 0; i < 3; i++){
            r_init[i] = (boost::python::extract<double>((x_i)[i]));
            /*if (len(x_i) > 3)*/ {
                v_init[i] = (boost::python::extract<double>((x_i)[i + 3]));
            }
        }

        double r_final[6];
        for ( int i = 0; i < 3; i++){
            r_final[i] = (boost::python::extract<double>((x_f)[i]));
        }
    
        Vector3 v1, v2;
        int res = R3bpLambertProblemSolver::solve(r_init, r_final, tof, v_init, v1, v2, print_iteration, maxiter, xtol);
        if (res != 1) {
            PyErr_SetString(PyExc_RuntimeError, "Cannot find solution");
        }
        return boost::python::make_tuple(v1, v2);
    }
    
    std::vector<R3bpLambertProblemSolution> solve_tau(OrbitSolution &orbit_dep, OrbitSolution &orbit_arr, double tof, double dtau, bool print_iteration = false, int maxiter = 100, double xtol=1e-6) {
        std::vector<R3bpLambertProblemSolution> solutions;
        int res = R3bpLambertProblemSolver::solve(orbit_dep, orbit_arr, tof, dtau, solutions, print_iteration, maxiter, xtol);
        if (res != 1) {
            PyErr_SetString(PyExc_RuntimeError, "Cannot find solution");
        }        
        return solutions;
    }
};

BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyLambertProblem_solve_overloads, PyLambertProblem::solve, 3, 6)
BOOST_PYTHON_MEMBER_FUNCTION_OVERLOADS(PyLambertProblem_solve_tau_overloads, PyLambertProblem::solve_tau, 4, 7)



// -------------------------------------------------------------------------
struct PyManifold : public EigenSpace {
private:
    std::vector<complex<double> > sn;
    std::vector<VectorXcd> w_stable;
    std::vector<complex<double> > un;
    std::vector<VectorXcd> w_unstable;
    std::vector<complex<double> > cn;
    std::vector<VectorXcd> w_center;
public:
    PyManifold(const OrbitSolution &lissajousOrbit, double tol=1e-3) : EigenSpace() {
        getManifolds(lissajousOrbit.monodromy, false, sn, w_stable, un, w_unstable, cn, w_center, tol);
    }

    std::vector<VectorXcd> get_centerManifold_EigenValues() {
        return w_center;
    }

    std::vector<VectorXcd> get_centerManifold_basis() {
        return w_center;
    }

    std::vector<VectorXcd> get_unstableManifold_eigenValues() {
        return w_unstable;
    }

    std::vector<VectorXcd> get_unstableManifold_basis() {
        return w_unstable;
    }

    std::vector<VectorXcd> get_stableManifold_eigenValues() {
        return w_stable;
    }

    std::vector<VectorXcd> get_stableManifold_basis() {
        return w_stable;
    }
};

inline Vector3 pyRead_eigenVector3() {
    Vector3 v; v << 1, 2, 3;
    return v;
}

inline Matrix3 pyRead_eigenMatrix3() {
    Matrix3 m;
    m << 1, 2, 3,
         4, 5, 6,
         7, 8, 9;
    return m;
}

inline Matrix6 pyRead_eigenMatrix6() {
    Matrix6 m = Matrix6::Zero();
    return m;
}

// -------------------------------------------------------------------------
// wrappers
double JacobiCscalar(double mu, double x, double y, double z, double vx, double vy, double vz) { return jacobi::JacobiC(mu, x, y, z, vx, vy, vz); }
double JacobiCvector(double mu, const Vector3& pos, const Vector3& vel) { return jacobi::JacobiC(mu, pos, vel); }
double JacobiVelocityMagnitude(double mu, double C, const Vector3& pos) { return jacobi::velocity_magnitude(mu, C, pos); }

// ------------------------------------------------------------------------- 
BOOST_PYTHON_MODULE(_main)
{
    // Register std converters to python lists if not already registered by some
    DECLARE_CONVERTERS();

    enum_<LibrationPointId>("LibrationPointId")
        .value("L1", LibrationPointId::L1)
        .value("L2", LibrationPointId::L2)
        .value("L3", LibrationPointId::L3)
        .value("L4", LibrationPointId::L4)
        .value("L5", LibrationPointId::L5)
        .export_values();

    enum_<PrimaryPointId>("PrimaryPointId")
        .value("P1", PrimaryPointId::P1)
        .value("P2", PrimaryPointId::P2)
        .export_values();

    enum_<AxisId>("AxisId")
        .value("X_AXIS", AxisId::X_AXIS)
        .value("Y_AXIS", AxisId::Y_AXIS)
        .value("Z_AXIS", AxisId::Z_AXIS)
        .export_values();

    enum_<R3bpLowThrustTerminalConstraint>("R3bpLowThrustTerminalConstraint")
        .value("RENDEZVOUS", R3bpLowThrustTerminalConstraint::E_RENDEZVOUS)
        .value("POSITION", R3bpLowThrustTerminalConstraint::E_POSITION);

    // Declare functions
    def("computeLibrationPoints", &pyComputeLibrationPoints_);	
    
    def("Jacobi_constant", &JacobiCscalar);
    def("Jacobi_constant", &JacobiCvector);
    def("Jacobi_velocity_magnitude", &JacobiVelocityMagnitude);
        
    class_<PyLowThrustPropulsionSystem>("ThrusterData", init<double,double>(
                   (boost::python::arg("thrust"),
                    boost::python::arg("isp"))))
        .def("force", &PyLowThrustPropulsionSystem::get_force)
        .def("isp", &PyLowThrustPropulsionSystem::get_isp);
    
    class_<PyR3bProblem>("Problem", init<double>())
        .def(init<double, double>())
        .def(init<double, double, double>())
        .def(init<double, double, double, double>())
        .def("get_mu_ratio", &PyR3bProblem::get_mu_ratio)
        .def("get_libration_points", &PyR3bProblem::get_libration_points)
        .def("get_libration_point_info", &PyR3bProblem::get_libration_point_info)
        .def("get_synodic_period", &PyR3bProblem::get_synodic_period)
        .def_readonly("a_m", &PyR3bProblem::getNormalisationForDistance)
        .def_readonly("a_s", &PyR3bProblem::getNormalisationForTime)        
        .def("set_distance_normalisation", &PyR3bProblem::setNormalisationForDistance)
        .def("get_distance_normalisation", &PyR3bProblem::getNormalisationForDistance)
        .def("get_time_normalisation", &PyR3bProblem::getNormalisationForTime)
        .def("get_mass_normalisation", &PyR3bProblem::getNormalisationForMass)
        .def("to_fixed_frame", &PyR3bProblem::to_fixed_frame);
    

    class_<LibrationPointProperty>("LibrationPointProperty")
        .def_readonly("id", &LibrationPointProperty::id)
        .def_readonly("mu", &LibrationPointProperty::mu)
//        .def_readwrite("position", &LibrationPointProperty::L)
        .def_readwrite("energy", &LibrationPointProperty::C)
        .def("get_state", &LibrationPointProperty::get_state);

    class_<SimulationEvent>("SimulationEvent")
        .def_readonly("date", &SimulationEvent::date)
        .def_readonly("type", &SimulationEvent::type)
        .def_readonly("text", &SimulationEvent::text);

    class_<PyPropagator>("Propagator", init<double>())
        .def("set_tol", &PyPropagator::set_tol)
        .def("propagate", &PyPropagator::propagate_to_list, PyPropagator_propagate_list_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("tspan"),
                    boost::python::arg("tstep")=-1)))
        .def("propagate", &PyPropagator::propagate_eigen, PyPropagator_propagate_eigen_overloads(
                   (boost::python::arg("x"),
                    boost::python::arg("tspan"),
                    boost::python::arg("tstep")=-1)))
        .def("propagate_stm", &PyPropagator::propagate_stm)
        .def("add_force", &PyPropagator::add_force<PyThirdBodyPerturbation>)
        .def("add_force", &PyPropagator::add_force<PyConstantForce>)
        .def("add_detector", &PyPropagator::add_axis_crossing_detector, PyPropagator_add_axis_crossing_detector(
                    (boost::python::arg("axis"),
                    boost::python::arg("direction")=0)))    
        .def("get_events", &PyPropagator::get_events)
        .def("toFile", &PyPropagator::toFile);
    
    class_<PyManifold>("Manifold", init<OrbitSolution, optional<double> >())
        .def("get_centerManifold_eigenValues", &PyManifold::get_centerManifold_EigenValues)
        .def("get_centerManifold_basis", &PyManifold::get_centerManifold_basis)
        .def("get_unstableManifold_eigenValues", &PyManifold::get_unstableManifold_eigenValues)
        .def("get_stableManifold_basis", &PyManifold::get_stableManifold_basis)
        .def("get_stableManifold_eigenValues", &PyManifold::get_stableManifold_eigenValues)
        .def("get_unstableManifold_basis", &PyManifold::get_unstableManifold_basis);

    class_<PyLambertProblem>("LambertProblem", init<double>())
        .def("solve", &PyLambertProblem::solve, PyLambertProblem_solve_overloads())
        .def("solve_tau", &PyLambertProblem::solve_tau, PyLambertProblem_solve_tau_overloads());        

    class_<R3bpLambertProblemSolution>("R3bpLambertProblemSolution")
        .def_readonly("tau1", &R3bpLambertProblemSolution::tau1)
        .def_readonly("tau2", &R3bpLambertProblemSolution::tau1)
        .def_readonly("r0", &R3bpLambertProblemSolution::r0)
        .def_readonly("v0", &R3bpLambertProblemSolution::v0)
        .def_readonly("rf", &R3bpLambertProblemSolution::rf)
        .def_readonly("vf", &R3bpLambertProblemSolution::vf)
        .def_readonly("v1", &R3bpLambertProblemSolution::v1)
        .def_readonly("v2", &R3bpLambertProblemSolution::v2)
        .def_readonly("tof", &R3bpLambertProblemSolution::tof)        
        .def("get_dv1", &R3bpLambertProblemSolution::get_dv1)
        .def("get_dv2", &R3bpLambertProblemSolution::get_dv2)
        .def("get_init_state", &R3bpLambertProblemSolution::get_init_state)
        .def("get_final_state", &R3bpLambertProblemSolution::get_final_state);                

    class_<R3bpLowThrustProblemSolution>("R3bpLowThrustProblemSolution")  
        .def_readonly("sol", &R3bpLowThrustProblemSolution::x);
                                        
    class_<PyLowThrustPropagator>("LowThrustPropagator", init<double, PyLowThrustPropulsionSystem>())
        .def("set_tol", &PyLowThrustPropagator::set_tol)
        .def("propagate", &PyLowThrustPropagator::propagate_sol)
        .def("propagate", &PyLowThrustPropagator::propagate_array)
        //.def("propagate", &PyLowThrustPropagator::propagate_vector)                        
        .def("propagate_stm", &PyLowThrustPropagator::propagate_stm_propagate_array)
        .def("time_derivative", &PyLowThrustPropagator::compute_time_derivative_list)        
        .def("time_derivative", &PyLowThrustPropagator::compute_time_derivative)
        .def("add_force", &PyLowThrustPropagator::add_force<PyThirdBodyPerturbation>)
        .def("add_force", &PyLowThrustPropagator::add_force<PyConstantForce>)
        .def("get_guidance", &PyLowThrustPropagator::get_guidance)
        .def("toFile", &PyLowThrustPropagator::toFile);
                
    class_<PyLowThrustProblem>("LowThrustProblem", init<double, PyLowThrustPropulsionSystem>())
        .def("solve", &PyLowThrustProblem::py_solve, PyLowThrustProblem_overloads(
            (boost::python::arg("x0"),
            boost::python::arg("xf"),
            boost::python::arg("tof"),
            boost::python::arg("lambda0"))))
        .def("set_final_pos", &PyLowThrustProblem::set_final_position)
        .def("set_final_vel", &PyLowThrustProblem::set_final_velocity)
        .def("compute_transversality_condition", &PyLowThrustProblem::compute_transversality_conditions)            
        .def("compute_time_optimal_condition", &PyLowThrustProblem::compute_time_optimal_condition);
                             
                             
    class_<PyR3bpOptimalGuidance>("R3bpOptimalGuidance", init<PyLowThrustPropagator>())
        .def("control_direction", &PyR3bpOptimalGuidance::get_control_direction)
        .def("control_direction", &PyR3bpOptimalGuidance::get_control_direction_ndarray)        
        .def("control_direction", &PyR3bpOptimalGuidance::get_control_direction_list);
                                                                
    Vector3 GetControlDirection(const real_type* x, const real_type* lambda);
                                    
    class_<PyPlanetEphemerides>("BodyEphemerides", init<optional<double, double>>())
        .def("get_position", &PyPlanetEphemerides::get_position)
        .def("get_position", &PyPlanetEphemerides::get_positions);        

    class_<PyThirdBodyPerturbation>("ThirdBodyPerturbation", init<int, double, double>());
    class_<PyConstantForce>("ConstantForce", init<Vector3>());

    def("testVector3", &pyRead_eigenVector3);
    def("testMatrix3", &pyRead_eigenMatrix3);
    def("testMatrix6", &pyRead_eigenMatrix6);
}
