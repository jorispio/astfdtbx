# We import all symbols in the core namespace (also the ones we do not use
# in this file, but we still want in the namespace core)
from ._main import Problem
from ._main import LibrationPointProperty
from ._main import Propagator
from ._main import Manifold
from ._main import LibrationPointId
from ._main import *

from .bodies import Body

from ._frame import *

import numpy as np

#from . import __extensions__
#
#if (__extensions__['mplot3d']):
#    from ._plotZeroVelocityCurve import *   
#    from ._plotLowEnergyTransfer import *


def JacobiC(mu, x, y, z):
    import numpy as np 
    (n, m) = x.shape
    val = np.zeros((n, m))
    for i in range(0, n):
        for j in range(0, m):
            val[i][j] = Jacobi_constant(mu, x[i][j], y[i][j], z, 0., 0., 0.)
            
    return val
    
def print_lambert_tau(sols, n=0):    
    print("#   | tau_1 tau_2 |     dv1       dv2  |  dv_total")
    print("==================================================")
    i = 0
    for sol in sols:
        dv1 = np.linalg.norm(sol.get_dv1());
        dv2 = np.linalg.norm(sol.get_dv2())
        print("{:3}| {:.3}  {:.3}  |  {:8.3} {:8.3} |    {:.3}".format(i, sol.tau1, sol.tau2, dv1, dv2, dv1+dv2))    
        i = i + 1
        if (n > 0) and (i > n):
            break
