#ifndef __PY_FORCES_HPP__
#define __PY_FORCES_HPP__

#include <cmath>
#include <iostream>
#include <list>

#include "HelioLib.hpp"

struct PyThirdBodyPerturbation : public ThirdBodyPerturbation {
 public:
    PyThirdBodyPerturbation(int ibody, double am, double as)
        : ThirdBodyPerturbation(ibody, (Frame*)FramesFactory::ICRF, am, as) {
    }
};

struct PyConstantForce : public ConstantForce {
 public:
    PyConstantForce(Vector3 force)
        : ConstantForce(force, (Frame*)FramesFactory::ICRF) {
    }
};

#endif  // __PY_FORCES_HPP__