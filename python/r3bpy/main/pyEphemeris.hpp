#ifndef __PY_EPHEMERIS_HPP__
#define __PY_EPHEMERIS_HPP__

#include <cmath>
#include <iostream>
#include <list>

#include "HelioLib.hpp"

// -------------------------------------------------------------------------
struct PyPlanetEphemerides : public PlanetAnalyticEphemerides {
 public:
    PyPlanetEphemerides(double am=1, double as=1) : PlanetAnalyticEphemerides(am, as) { }
    ~PyPlanetEphemerides() { };
    
    boost::python::tuple get_position(int planet, double mjd2000) {
        double rp[3], vp[3], Ep[6];
        if (planet == 301) {
            getMoonEphemeris(mjd2000, rp, vp);      
        }
        else {
            getEphemeris(mjd2000, planet, rp, vp, Ep);
        }
        Vector3 bodyPosition; bodyPosition << rp[0], rp[1], rp[2];
        Vector3 bodyVelocity; bodyVelocity << vp[0], vp[1], vp[2];
        return boost::python::make_tuple(bodyPosition, bodyVelocity);
    }
    
    boost::python::tuple get_positions(int planet, pyList& mjd2000_) {
        double rp[3], vp[3], Ep[6];
        int listLen = boost::python::len(mjd2000_);
               
        Vector3 bodyPosition, bodyVelocity; 
        std::vector<Vector3> pos, vel;
        for ( int i = 0; i < listLen; i++){    
            double mjd2000 = boost::python::extract<double>((mjd2000_)[i]);
            if (planet == 301) {
                getMoonEphemeris(mjd2000, rp, vp);      
            }
            else {
                getEphemeris(mjd2000, planet, rp, vp, Ep);
            }
            bodyPosition << rp[0], rp[1], rp[2];
            pos.push_back(bodyPosition);
            bodyVelocity << vp[0], vp[1], vp[2];
            vel.push_back(bodyVelocity);
        }
        return boost::python::make_tuple(pos, vel);
    }
};

#endif // __PY_EPHEMERIS_HPP__

