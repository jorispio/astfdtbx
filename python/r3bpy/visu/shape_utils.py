""" """
import numpy as np




def get_cdg(verts):
    """Compute the center of mass, assuming homogeneous shape volume."""
    return np.array([np.sum(verts[:, 0]), np.sum(verts[:,1]), np.sum(verts[:,1])]) / len(verts)

def point_inside (pt, v1, v2, v3):
    """ """
    denom = (v1[0]*(v2[1] - v3[1]) + v1[1]*(v3[0] - v2[0]) + v2[0]*v3[1] - v2[1]*v3[0])
    u = (pt[0]*(v3[1] - v1[1]) + pt[1]*(v1[0] - v3[0]) - v1[0]*v3[1] + v1[1]*v3[0]) / denom;
    v = (pt[0]*(v2[1] - v1[1]) + pt[1]*(v1[0] - v2[0]) - v1[0]*v2[1] + v1[1]*v2[0]) / -denom;
    #print(u,v)
    return (u >= 0) and (v >= 0) and (u + v < 1)

def compute_line_plane_intersection(planeNormal, planePoint, rayDirection, rayPoint, epsilon=1e-6):
    """ """
    fproj = planeNormal.dot(rayDirection)
    if abs(fproj) < epsilon:
        return None # no intersection or line is within plane
 
    d = rayPoint - planePoint
    s = -planeNormal.dot(d) / fproj
    p = d + s * rayDirection + planePoint
    return p
    
def find_ground_track(verts, faces, x, y, z):
    """ """
    ground = []
    face_gt_id = []
    for i in range(0, faces.shape[0]):
        # the loop are done this way to have point ordering and to be a bit more efficient 
        # as face computations are factored
        
        fid = [faces[i,0]-1, faces[i,1]-1, faces[i,2]-1]
        face_center = np.sum([verts[fid, 0], verts[fid,1], verts[fid, 2]], 1) / 3.
        normal = np.cross(verts[fid[0], :] - face_center, verts[fid[1], :] - face_center)
        face_normal = normal / np.linalg.norm(normal)    
        
        # rotate to have triangle in 2d canonical space        
        # plane basis
        u1 = verts[fid[0], :] - face_center        
        u1 = u1 / np.linalg.norm(u1)
        u3 = face_normal        
        u2 = np.cross(u3, u1)
        mat = np.array([u1, u2, u3])    
       
        # apply rotation
        v1 = np.dot(mat, verts[fid[0], :] - face_center)
        v2 = np.dot(mat, verts[fid[1], :] - face_center)
        v3 = np.dot(mat, verts[fid[2], :] - face_center)

        for j in range(0, len(x)):
            r = np.array([x[j], y[j], z[j]])
            
            # pre check
            ur = r / np.linalg.norm(r)
            if abs(np.dot(face_normal, ur)) > 1e-6:
                # find coordinates in the triangle plane            
                # intersection point of radius line and plane
                rp = compute_line_plane_intersection(face_normal, verts[fid[0], :], ur, r, epsilon=1e-6)

                # apply rotation
                rpp = np.dot(mat, rp - face_center)
                
                # check if point is inside
                if point_inside(rpp, v1, v2, v3):
                    ground.append(rp)
                    face_gt_id.append(i)
    ground = np.array(ground)
    return ground





