#
import matplotlib.pyplot as pyplot
import numpy as np


def init_figure(is2d=False):
    fig = pyplot.figure()
    if is2d:
        ax = pyplot.gca()
        pyplot.xlabel("x (DU)")
        pyplot.ylabel("y (DU)")
    else:
        ax = pyplot.gca(projection='3d')
        ax.set_xlabel("x (DU)")
        ax.set_ylabel("y (DU)")
        ax.set_zlabel("z (DU)")

    return ax, fig


def plot_system(problem, a_m=1, ax=None, is2d=True, 
        withPrimaries=True, primaryList=range(1, 3), 
        withCollinearPoints=True, colinearPointsList=range(1, 4),
        withTriangularPoints=True,
        withAxes=False):
    """Visualizes the three body problem.

    # Arguments
        problem: The model describing the three body problem.
        ax: Current axe. Can be None.
        is2d: whether it is a 2d plot, or 3d.


    #Returns
        Current axe
    """
    if ax is None:
        ax, fig = init_figure(is2d)

    if withPrimaries:
        if is2d:
            if primaryList.__contains__(1):
                ax.scatter(-problem.get_mu_ratio() * a_m, 0, s=30, marker='*', c='blue')
            if primaryList.__contains__(2):
                ax.scatter((1 - problem.get_mu_ratio()) * a_m, 0, s=30, marker='*', c='blue')
        else:
            if primaryList.__contains__(1):
                ax.scatter(-problem.get_mu_ratio() * a_m, 0, 0, s=30, marker='*', c='blue')
            if primaryList.__contains__(2):
                ax.scatter((1 - problem.get_mu_ratio()) * a_m, 0, 0, s=30, marker='*', c='blue')

    libration_points = problem.get_libration_points()

    if withCollinearPoints:
        for i in colinearPointsList:
            point = libration_points[i-1] * a_m
            if is2d:
                ax.scatter(point[0], point[1], s=30, marker='o', c='red')
            else:
                ax.scatter(point[0], point[1], point[2], s=30, marker='o', c='red')

    if withTriangularPoints:
        for i in range(4, 6):
            point = libration_points[i-1] * a_m
            if is2d:
                ax.scatter(point[0], point[1], s=30, marker='o', c='red')
            else:
                ax.scatter(point[0], point[1], point[2], s=30, marker='o', c='red')

    if withAxes:
        ax.plot([0, 1], [0, 0], [0, 0], 'k')
        ax.plot([0, 0], [0, 1], [0, 0], 'k')
        ax.plot([0, 0], [0, 0], [0, 1], 'k')

    return ax

