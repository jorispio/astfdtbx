# -*- coding: utf-8 -*-
import numpy as np

from mayavi.mlab import *


def plot_trajectory(xyz, a_m=1, fig=None, color=(1.0, 1.0, 1.0), lw=1.0, tube_radius=0.001, alpha=1.0, legend=None):
    """Visualizes the trajectory points.

    # Arguments
        xyz: position points
        color: RGB value for line color
        lw: line width
        tube_radius: tube radius
        alpha: line transparency

    #Returns
        current figure
    """
    xyz = np.array(xyz) * a_m
    line3d = plot3d(xyz[:, 0], xyz[:, 1], xyz[:, 2], color=color, line_width=lw, tube_radius=tube_radius, opacity=alpha) #, label=legend)

    return fig

    
def plot_orbit(orbit, a_m=1, color=None, zoom=True, fig=None, is2d=False, lw=None, alpha=1.0, legend=None):
    """Plot one revolution of the Libration point orbit.

       # Returns
         current axe
    """    
    orbit_period = orbit.period
    _, xyz = orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, color=color, alpha=alpha, legend=legend, a_m=a_m, fig=fig)

