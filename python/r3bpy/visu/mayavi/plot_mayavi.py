from mayavi import mlab
from mayavi.mlab import *
from tvtk.api import tvtk # python wrappers for the C++ vtk ecosystem

import numpy as np

import matplotlib.pyplot as plt

image_file_earth='data/world.topo.bathy.200412.3x5400x2700.jpg'
image_file_moon='data/lroc_color_poles_1k.jpg'

bgcolor=(0,0,0)  # background color

def create_scene(fig, objects):
    """Create celestial body scene.
    """
    for obj in objects:
        center, radius, texture = obj
        create_ellipsoid(fig, image_file=texture, R=radius, center=center)
    return fig

#
def create_sphere(fig, image_file, R=1.0, center=[0.0,0.0,0.0]):
    """
        R radius of sphere
        center   ellipsoid center position
        image_file  texture file        
    """
    img = tvtk.JPEGReader()
    img.file_name = image_file
    texture = tvtk.Texture(input_connection=img.output_port, interpolate=1)

    Nrad = 180
    sphere = tvtk.TexturedSphereSource(radius=R, theta_resolution=Nrad,
                                       phi_resolution=Nrad)

    sphere_mapper = tvtk.PolyDataMapper(input_connection=sphere.output_port)
    sphere_actor = tvtk.Actor(mapper=sphere_mapper, texture=texture)
    fig.scene.add_actor(sphere_actor)
    return fig


def create_ellipsoid(fig, image_file, R=(1.0,1.0,1.0), center=[0.0,0.0,0.0]):
    """
        R radiuses of sphere
        center   ellipsoid center position
        image_file  texture file        
    """
    img = plt.imread(image_file) # shape (N,M,3), flip along first dim
    img_flpd = image_file.replace('.jpg', '_flpd.jpg')
    # flip output along first dim to get right mapping
    img = img[::-1,...]
    plt.imsave(img_flpd, img)

    # semi-major and minor axes
    a, b, c = R
    
    # parameters for the ellipsoid
    Nrad = 90   # points along theta
    Mrad = 45   # points along phi
    phi = np.linspace(0, 2 * np.pi, Nrad) 
    theta = np.linspace(0, np.pi, Mrad)
    phigrid, thetagrid = np.meshgrid(phi, theta) 
    
    # compute actual points on the sphere    
    x = center[0] + a * np.sin(thetagrid) * np.cos(phigrid)
    y = center[1] + b * np.sin(thetagrid) * np.sin(phigrid)
    z = center[2] + c * np.cos(thetagrid)

    # create mesh
    mesh = mlab.mesh(x,y,z)
    mesh.actor.actor.mapper.scalar_visibility = False
    mesh.actor.enable_texture = True  # probably redundant assigning the texture later

    # load the (flipped) texture
    img = tvtk.JPEGReader(file_name=img_flpd)
    texture = tvtk.Texture(input_connection=img.output_port, interpolate=0, repeat=0)
    mesh.actor.actor.texture = texture

    # tell mayavi that the mapping from points to pixels is via a sphere
    mesh.actor.tcoord_generator_mode = 'sphere'
    cylinder_mapper = mesh.actor.tcoord_generator
    cylinder_mapper.prevent_seam = 0
    return fig


if __name__ == "__main__":
    # create an Earth-Moon plot
    DU = 6378.
    objects = [([0.0,0.0,0.0], (1., 1.0, 1.0), image_file_earth ), 
                ([300000. / DU, 0.0, 0.0], (1737./DU,1737./DU,1737./DU), image_file_moon)]

    fig = mlab.figure(size=(1024, 1024), bgcolor=bgcolor)
    create_scene(fig, objects)
    mlab.show()
   

