"""
Most of the features here can be done with mayavi or scikit-image.
"""
import numpy as np
from numpy import pi 

from ..shapes import Face, Ellipsoid

from mayavi import mlab
from mayavi.mlab import *
from tvtk.api import tvtk # python wrappers for the C++ vtk ecosystem

bgcolor=(0,0,0)  # background color

class MayaFace(Face):
    def __init__(self, verts, normal=None):
        Face.__init__(verts, normal)        
        
    def plot(self, fig=None, color='b', edge_color='k', alpha=0.7, texture=None, texture_file=None):
        if ax == None:
            fig = mlab.figure(size=(1024, 1024), bgcolor=bgcolor)

        polyc = Face.polyc #np.array([self.polyc[1][0], self.polyc[1][1], self.polyc[0][1], self.polyc[0][0]])       
        poly = Poly3DCollection([polyc])  
        poly.set_color(color)
        poly.set_alpha(alpha)          
        poly.set_edgecolor(edge_color)  
        ax.add_collection3d(poly)


class MayaEllipsoid(Ellipsoid):
    """Define an ellipsoid shape.
    """
    def __init__(self, radius, center):
        Ellipsoid.__init__(radius, center)
                        
    def _import_texture_as_colors(image_file):
        """
        """

        return image_file

    def _draw_object(self, fig, num, color, alpha, texture):
        """
        """
        if not self.__computed:
            self.xell, self.yell, self.zell = Ellipsoid._generate_ellipsoid_vertex(self.radius, self.center, num=num)
            self.__computed = True
        
        # create mesh
        mesh = mlab.mesh(self.xell, self.yell, self.zell)
        mesh.actor.actor.mapper.scalar_visibility = False
        mesh.actor.enable_texture = True  # probably redundant assigning the texture later

        # load the (flipped) texture
        img = tvtk.JPEGReader(file_name=texture)
        texture = tvtk.Texture(input_connection=img.output_port, interpolate=0, repeat=0)
        mesh.actor.actor.texture = texture

        # tell mayavi that the mapping from points to pixels happens via a sphere
        mesh.actor.tcoord_generator_mode = 'sphere' # map is already given for a spherical mapping
        cylinder_mapper = mesh.actor.tcoord_generator
        cylinder_mapper.prevent_seam = 0
        return fig
        
    def plot(self, fig=None, num=50, color='b', alpha=0.7, texture=None, texture_file=None, rstride=4, cstride=4):
        if fig == None:
            fig = mlab.figure(size=(1024, 1024), bgcolor=bgcolor)

        self._draw_object(self, fig, num, color, 
                alpha,
                texture_file,
                rstride, cstride)

