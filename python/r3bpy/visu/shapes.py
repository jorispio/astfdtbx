"""

"""
import numpy as np
from numpy import pi 


class Face():
    def __init__(self, verts, normal=None):
        self.polyc = np.array([verts[1][0], verts[1][1], verts[0][1], verts[0][0]])
        self.normal = normal
        
    def get_center(self):
        """ """
        return np.sum(self.polyc.T, 1) / len(self.polyc)
        
    def get_normal(self):
        """ """
        if self.normal is None:
            c = self.get_center().T
            u1 = self.polyc[0] - c
            u2 = self.polyc[1] - c
            self.normal = np.cross(u1 / np.linalg.norm(u1), u2  / np.linalg.norm(u2))
            self.normal = c / np.linalg.norm(c)
        return self.normal     


class Shape():
    """Define an random OBJ shape.
    """
    def __init__(self, obj_file):
        self.faces, self.verts, self.normals = self._load(obj_file)
        
    def _load(obj_file):
        txt = np.loadtxt(obj_file, dtype={'names': ('t', 'u', 'v', 'w'),'formats': ('S1', 'f', 'f', 'f')})
        faces = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'f']).astype(int)
        verts = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'v'])
        normals = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'n'])
        return faces, verts, normals

    def get_face(self, iface):
        """ """
        return self.faces[iface]
        
        
class Ellipsoid():
    """Define an ellipsoid shape.
    """
    def __init__(self, radius, center):
        self.radius = radius
        self.center = center
        self.xell = [] 
        self.yell = [] 
        self.zell = []
        self.faces = []
        self.__computed = False
        pass
            
    def _generate_ellipsoid_vertex(radius, center, num):
        """Generate coordinates of an ellipsoid of axes a, b, c and center 'center'
        """    
        a, b, c = radius
        xc, yc, zc = center
        
        u = np.linspace(0, 2 * pi, num)
        v = np.linspace(0, pi, num)    
        u, v = np.meshgrid(u, v)
        cosu = np.cos(u)
        x = xc + a * np.cos(v) * cosu
        y = yc + a * np.sin(v) * cosu
        z = zc + c * np.sin(u)
        
        return x, y, z        

    def get_faces(self):
        """ """
        # faces are quad constructed from consecutive lats and longs
        if not self.__computed:
            self.xell, self.yell, self.zell = Ellipsoid._generate_ellipsoid_vertex(self.radius, self.center, num=50)
            self.__computed = True

        X, Y, Z = np.broadcast_arrays(self.xell, self.yell, self.zell)
        rstride = 1
        cstride = 1
        rows, cols = Z.shape
        row_inds = list(range(0, rows-1, rstride)) + [rows-1]
        col_inds = list(range(0, cols-1, cstride)) + [cols-1]
        
        self.faces = []
        for row, next_row in zip(row_inds[:-1], row_inds[1:]):
            facecs = []
            for col, next_col in zip(col_inds[:-1], col_inds[1:]):
                ps = [verts[row:next_row+1, col:next_col+1] for verts in (X, Y, Z)]
                ps = np.array(ps).T
                facecs.append(Face(ps))
            self.faces.append(facecs)
        return self.faces

    def get_face(self, iface):
        """ """
        return self.faces[iface]


