import matplotlib.pyplot as pyplot
import numpy as np


def plot_stability(p, stability_index, xlabel="parameter"):
    ''' plot stability index evolution '''
    pyplot.figure()
    ax = pyplot.gca()
    ax.grid(c='k', ls='-', alpha=0.3)
    ax.set_title('Stability Index')
    pyplot.plot(p, stability_index)
    pyplot.plot(p, 1 * np.ones(len(p)), "k:")
    pyplot.plot(p, -1 * np.ones(len(p)), "k:")
    pyplot.xlabel(xlabel)
    pyplot.ylabel("Stability Indices")
    

def plot_period(p, period, xlabel="parameter"):
    ''' plot stability index evolution '''
    pyplot.figure()
    ax = pyplot.gca()
    ax.grid(c='k', ls='-', alpha=0.3)
    ax.set_title('Orbital period')
    pyplot.plot(p, period)
    pyplot.xlabel(xlabel)
    pyplot.ylabel("Orbital period, day")    


