import numpy as np



class Shape()
    def __init__():
        pass

    def _load_shape(obj_filename):
        txt = np.loadtxt(obj_filename, dtype={'names': ('t', 'u', 'v', 'w'),'formats': ('S1', 'f', 'f', 'f')})

        faces = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'f']).astype(int)
        verts = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'v'])
        normals = np.array([[t[1],t[2],t[3]] for t in txt if t[0] == b'n'])

        return faces, verts, normals
        

    def get_center_of_mass(self):
    """Return the center of mass, assuming an homegenous object"""
        return np.array([np.sum(verts[:,0]), np.sum(verts[:,1]), np.sum(verts[:,2])]) / len(verts)

    def _plot_shape(verts, faces, ax=None):
        fig = plt.figure()
        if not ax:
            ax = fig.add_subplot(111, projection='3d')

        ax.plot_trisurf(verts[:, 0], verts[:,1], faces-1, verts[:, 2],
                    linewidth=0.2, antialiased=True)


