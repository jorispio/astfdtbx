import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from .plot_system import *


def plot_trajectory(xyz, a_m=1, color=None, zoom=True, ax=None, is2d=False, lw=None, alpha=1.0, legend=None):
    """Visualizes the trajectory points.

    # Arguments
        xyz: position points
        color: line color.
        zoom:
        ax: current axes. Can be None.
        lw: line width
        alpha: line transparency

    #Returns
        current axe
    """

    if ax is None:
        ax, fig = init_figure(is2d)

    xyz = np.array(xyz) * a_m
    if is2d:
        line2d = pyplot.plot(xyz[:, 0], xyz[:, 1], color=color, linewidth=lw, alpha=alpha, label=legend)
    else:
        line3d = pyplot.plot(xyz[:, 0], xyz[:, 1], xyz[:, 2], color=color, linewidth=lw, alpha=alpha, label=legend)
        # line3d.axes.autoscale(enable=True)

    return ax

    
def plot_orbit(orbit, a_m=1, color=None, zoom=True, ax=None, is2d=False, lw=None, alpha=1.0, legend=None):
    """Plot one revolution of the Libration point orbit.

       # Returns
         current axe
    """    
    orbit_period = orbit.period
    _, xyz = orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, color=color, alpha=alpha, legend=legend, a_m=a_m, ax=ax)

