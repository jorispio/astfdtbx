from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as pyplot
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np
from .plot_system import *


def plot_jacobi_levels(x, y, c, ax=None, levels=None, libration_points=[], is3d=False):
    """Visualizes the jacobi contour levels.

    # Arguments
        x: x values
        y: y values
        c: c values
        levels: levels to display
        libration_points: libration point list.
        is2d: whether it is a 2d plot, or 3d.


    #Returns
        Current axe.
    """
    if levels is None:
        levels = [-3., -2.8, 0.1, 0.5, 1., 3., 5.]

    if ax is None:
        if is3d:
            ax, fig = init_figure(False)
        else:
            ax, fig = init_figure(True)
    origin = 'lower'

    if is3d:
        surf = ax.plot_surface(x, y, c, rstride=8, cstride=8, cmap=cm.coolwarm,
                               linewidth=0, antialiased=True, alpha=0.6)
        cset = ax.contour(x, y, c, zdir='z', offset=-7, cmap=cm.coolwarm)

        # Add a color bar which maps values to colors.
        fig.colorbar(surf, ax=ax, shrink=0.5, aspect=5)
        ax.zaxis.set_major_locator(LinearLocator(10))
        ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

    else:
        cs = pyplot.contourf(x, y, c, cmap=pyplot.cm.bone, origin=origin)  #, levels=levels)
        cs.cmap.set_under('yellow')
        cs.cmap.set_over('cyan')
        ax.contour(cs, colors='k')
        ax.clabel(cs, fmt='%2.1f', colors='w', inline=1, fontsize=8)

        ax.grid(c='k', ls='-', alpha=0.3)

    if libration_points:
        (n, m) = np.array(libration_points).shape
        for i in range(0, n):
            ax.scatter(libration_points[i][0], libration_points[i][1], s=30, marker='o', c='red')

    ax.set_title('Jacobi levels')

    return ax
