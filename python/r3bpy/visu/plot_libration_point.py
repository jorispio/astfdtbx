#
import matplotlib.pyplot as pyplot
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from .plot_system import *

def plot_libration_point(problem, i, ax=None, is2d=True):
    """Plot a libration point.

    # Arguments
        problem: The model describing the three body problem.
        i: libration point number, 1 to 5
        ax: Current axe. Can be None.
        is2d: whether it is a 2d plot, or 3d.


    #Returns
        Current axe
    """
    if ax is None:
        ax, fig = init_figure(is2d)

    libration_points = problem.get_libration_points()
    if is2d:
        ax.scatter(libration_points[i][0], libration_points[i][1], s=30, marker='o', c='red')
    else:
        ax.scatter(libration_points[i][0], libration_points[i][1], libration_points[i][2], marker='o', c='red')

    return ax

