"""
"""
import matplotlib.pyplot as pyplot
import matplotlib as plot
import numpy as np
from numpy import linalg as LA

from ..main import *
from . import *


def plot_manifold(mu_ratio, xyz_halo, halo_period, manifold_vector, color, max_duration=1.0, displace=1e-2, npts=30, ax=None):
    """Visualizes manifold described by the manifold vector.

    # Arguments
        mu_ratio: gravitational constant of the three body problem
        xyz_halo: orbit trajectory points
        halo_period: step size for gradient ascent.
        manifold_vector: manifold basis vector. Must be either stable or unstable manifold
        color: color line
        max_duration: propagation duration of the manifold
        displace: perturbation displacement value. Can be positive or negative.
        npts: number of point per revolution of the generating orbit.

    """

    if not manifold_vector.size:
        print("Empty manifold vector. Cannot compute manifold.")
        return

    propagator = Propagator(mu_ratio)

    # compute new points along the manifold, assuming linear approximation is
    # valid, for small d enough
    x0 = [xyz_halo[0][0], xyz_halo[0][1], xyz_halo[0][2], xyz_halo[0][3], xyz_halo[0][4], xyz_halo[0][5]]
    pos_vel = displace_state(propagator, mu_ratio, x0, halo_period, manifold_vector, xyz_halo, npts,
                             displace=displace, ax=ax)

    manifold = []
    for i in range(1, npts):
        # stable manifold results from a backward integration
        # unstable manifold results from a forward integration
        x0 = pos_vel[i].tolist()
        x0 = [x0[0].real, x0[1].real, x0[2].real, x0[3].real, x0[4].real, x0[5].real]
        t, xyz = propagator.propagate(x0, [0, max_duration])
        ax = plot_trajectory(xyz, ax=ax, color=color, lw=1, alpha=0.5)
        #xyz = propagator.propagate(x0, [0, -max_duration])
        #ax = plot_trajectory(xyz, ax=ax, color=color, lw=1, alpha=0.5)
        manifold.append((t, xyz))

    return manifold

def displace_state(propagator, mu_ratio, x0, halo_period, manifold_vector, xyz_halo, npts, displace=1e-5, ax=None):
    # translate state in the direction of the eigen vector, of the stable or
    # unstable invariant manifold (depending whether we use Ws or Wu as input)
    vmfld = np.array(manifold_vector.real)

    displaced_states = []
    for i in range(0, npts):
        ti = i * halo_period / npts
        t_halo, xyz_halo = propagator.propagate(x0, [0, ti])
        stm = propagator.propagate_stm(x0, [0, ti])
        stm = np.array(stm)
        wmfld = stm.dot(vmfld)

        xyz_halo = xyz_halo[-1]
        #print("xyz_halo =", xyz_halo, "  wmfld", wmfld)
        new_state = xyz_halo[0:6] + displace * wmfld / LA.norm(wmfld[3:6])
        displaced_states.append(new_state)
        #if ax:
        #    ax.quiver(xyz_halo[0], xyz_halo[1], xyz_halo[2], wmfld[0], wmfld[1], wmfld[2], length=0.1)
        #    ax.scatter(new_state[0], new_state[1], new_state[2])

    return displaced_states
