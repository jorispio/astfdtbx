#
cmake_minimum_required(VERSION 3.3)
include(CheckCXXCompilerFlag)

# ====================================================================
# Installation of the header files.
list(APPEND INSTALL_VISU_FILE_LIST "__init__.py" 
        "plot_libration_point.py" 
        "plot_manifold.py" 
        "plot_segments.py"         
        "plot_stability.py" 
        "plot_system.py" 
        "plot_trajectory.py"         
        "plot_zero_velocity_curve.py"         
        "shapes.py"
        "shape_utils.py"                   
        "default/__init__.py"
        "default/plot_object.py"
        "mayavi/__init__.py"
        "mayavi/plot_object.py"
        "mayavi/plot_mayavi.py"
        "mayavi/plot_trajectory.py"        
        )

foreach(file ${INSTALL_VISU_FILE_LIST})
    get_filename_component( dir ${file} DIRECTORY )
    install(FILES "${file}"
            DESTINATION "${R3BPY_INSTALL_PATH}/r3bpy/visu/${dir}")
endforeach()

# ====================================================================
# Packaging 
if(CREATE_PIP_PACKAGING)
    foreach(file ${INSTALL_VISU_FILE_LIST})
        get_filename_component( dir ${file} DIRECTORY )    
        install(FILES "${file}"
                DESTINATION "${PROJECT_PIPPKG_SRC_PATH}/visu/${dir}")
    endforeach()
endif()
