"""
"""
from .plot_trajectory import plot_trajectory
from ..orbits import OrbitSegment


def plot_segments(propagator, period, points, ax=None, color=None, legend=None):
    """Visualizes the segments of a trajectory.

    # Arguments
        propagator: propagator
        points: segment tuple or point
        period: define segment length if points are provided instead of segments
        color: line color.
        ax: current axes. Can be None.

    #Returns
        current axe
    """

    for idx in range(0, len(points)):
        if idx > 0:
            legend = None

        if isinstance(points[idx], OrbitSegment):
            pt = points[idx].get_init_state().tolist()
            period = points[idx].tau
        else:
            pt = points[idx].tolist()

        t, xyz = propagator.propagate(pt, [0, period])
        ax = plot_trajectory(xyz, ax=ax, color=color, alpha=0.5, legend=legend)
        ax.scatter(pt[0], pt[1], pt[2], c='blue')

        label = '(%d)' % idx
        ax.text(pt[0], pt[1], pt[2], label,
                horizontalalignment='right',
                verticalalignment='center',
                rotation='vertical',
                color='red')
    return ax