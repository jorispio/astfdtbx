"""
Most of the features here can be done with mayavi or scikit-image.
"""
import numpy as np
from numpy import pi 

from ..shapes import Face, Ellipsoid

from mpl_toolkits.mplot3d import Axes3D
from mpl_toolkits.mplot3d.art3d import Poly3DCollection

import matplotlib.pyplot as pyplot
from matplotlib import cm
import matplotlib.pyplot as plt
from matplotlib.ticker import LinearLocator, FormatStrFormatter



class MplFace(Face):
    def __init__(self, verts, normal=None):
        Face.__init__(verts, normal)        
        
    def plot(self, ax=None, color='b', edge_color='k', alpha=0.7, texture=None, texture_file=None):
        if ax == None:
            fig = plt.figure()
            ax = Axes3D(fig, auto_add_to_figure=False)
            fig.add_axes(ax)

        polyc = self.polyc #np.array([self.polyc[1][0], self.polyc[1][1], self.polyc[0][1], self.polyc[0][0]])       
        poly = Poly3DCollection([polyc])  
        poly.set_color(color)
        poly.set_alpha(alpha)          
        poly.set_edgecolor(edge_color)  
        ax.add_collection3d(poly)


class MplEllipsoid(Ellipsoid):
    """Define an ellipsoid shape.
    """
    def __init__(self, radius, center):
        Ellipsoid.__init__(radius, center)
                        
    def _import_texture_as_colors(image_file):
        """
        """
        image = plt.imread(image_file)
        return image

    def _draw_object(self, ax, num, color, alpha, texture, rstride, cstride):
        """
        """
        if not self.__computed:
            self.xell, self.yell, self.zell = Ellipsoid._generate_ellipsoid_vertex(self.radius, self.center, num=num)
            self.__computed = True
        
        sphere = ax.plot_surface(self.xell, self.yell, self.zell, rstride=rstride, cstride=cstride, 
            color=color, alpha=alpha,
            linewidth=0,
            antialiased=True)
         #   facecolors=texture)
        
    def plot(self, ax=None, num=50, color='b', alpha=0.7, texture=None, texture_file=None, rstride=4, cstride=4):
        if ax == None:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')

        if not texture and texture_file:
            texture = _import_texture_as_colors(texture_file)

        self._draw_object(self, ax, num, color, 
                alpha,
                texture,
                rstride, cstride)

