# -*- coding: utf-8 -*-
"""
Example 12: Family of axial orbits
==================================
This example plots a family of axial orbits around L1
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_12.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
from r3bpy.visu import plot_system, plot_trajectory
from r3bpy.main import Problem
from r3bpy.orbits import LyapunovOrbit, LyapunovOrbitFamilyType


def run_example12():
    # distance from L1 to the Moon is around 58000 km, while L2 to the Moon is 65000km
    r12 = constants.DISTANCE_EARTH_MOON
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON

    x1_init = 6000 * 1e3
    x1_final = 15000 * 1e3
    x1_step = 500 * 1e3

    problem = Problem(m1, m2, r12)

    pyplot.figure()
    ax = pyplot.gca(projection='3d')
    plot_system(problem, ax=ax, is2d=False, withPrimaries=False, withTriangularPoints=False, colinearPointsList=[1])

    libration_point = problem.get_libration_point_info(main.L1)

    # in current implementation, we can only construct Axial from the Lyapunov vertical solver.
    az_grid = np.arange(x1_init, x1_final, x1_step)
    compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.VERTICAL, az_grid, 'red', ax, plot_frequency=5)

    ax.view_init(elev=90., azim=-90)
    pyplot.title('Planar and Vertical orbits around L1')
    pyplot.show()


def compute_orbits(problem, libration_point, planarType, az_grid, color, ax, plot_frequency=5):
    lyapunov = LyapunovOrbit(libration_point, planarType)
    lyapunov_orbit = None

    i = 0
    for Ax in az_grid:
        if lyapunov_orbit is None:
            lyapunov_orbit = lyapunov.find_orbit(Ax / problem.getNormalisationForDistance(), True, 50)
        else:  # use previous solution to compute new Halo
            lyapunov_orbit = lyapunov.find_orbit(Ax / problem.getNormalisationForDistance(), lyapunov_orbit, False, 20, 1e-6)

        if lyapunov_orbit.is_valid:
            orbit_period = lyapunov_orbit.period
            print("Ax = {:f} km   period: {:f} days".format(Ax / 1000., orbit_period * problem.getNormalisationForTime() / 86400.))

            if i % plot_frequency == 0:
                ax = plot_orbit(lyapunov_orbit, color=color, ax=ax)
        else:
            lyapunov_orbit = None
        i = i + 1

#
def plot_orbit(orbit, color, ax):
    orbit_period = orbit.period
    xyz = orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, color=color, alpha=0.5, ax=ax)


if __name__ == "__main__":
    run_example12()
