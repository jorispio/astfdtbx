# -*- coding: utf-8 -*-
"""
Example 10: Family of Lyapunov planar orbits
============================================
This example plots a family of planar Lyapunov orbit around L1, L2 and L3
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_10.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
import r3bpy.visu as visu
from r3bpy.main import Problem
from r3bpy.orbits import LyapunovOrbit, LyapunovOrbitFamilyType


def run_example10():
    # distance from L1 to the Moon is around 58000 km, while L2 to the Moon is 65000km
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON

    x1_init = 5000 * 1e3
    x1_final = 50000 * 1e3
    x1_step = 100 * 1e3

    x2_init = 6000 * 1e3
    x2_final = 25000 * 1e3
    x2_step = 500 * 1e3

    x3_init = -20000 * 1e3
    x3_final = -300000 * 1e3
    x3_step = -1000 * 1e3

    problem = Problem(m1, m2, r12)

    ax = visu.plot_system(problem, is2d=False, withTriangularPoints=False)

    x_grid = np.arange(x1_init, x1_final, x1_step)
    libration_point = problem.get_libration_point_info(main.L1)
    compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.PLANAR, x_grid, 'red', ax, plot_frequency=5)

    x_grid = np.arange(x2_init, x2_final, x2_step)
    libration_point = problem.get_libration_point_info(main.L2)
    compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.PLANAR, x_grid, 'blue', ax, plot_frequency=5)

    x_grid = np.arange(x3_init, x3_final, x3_step)
    libration_point = problem.get_libration_point_info(main.L3)
    compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.PLANAR, x_grid, 'green', ax, plot_frequency=10)

    pyplot.title('Planar Lyapunov Orbit around L1, L2 and L3')
    pyplot.grid(True)
    pyplot.show()


def compute_orbits(problem, libration_point, planarType, x_grid, color, ax, plot_frequency=5):
    lyapunov = LyapunovOrbit(libration_point, planarType)
    lyapunov_orbit = None

    i = 0
    for x in x_grid:


        alpha = 0.8
        if lyapunov_orbit is None:
            lyapunov_orbit = lyapunov.find_orbit(x / problem.getNormalisationForDistance(), True, 10)
        else:  # use previous solution to compute new Halo
            alpha = 0.4
            lyapunov_orbit = lyapunov.find_orbit(x / problem.getNormalisationForDistance(), lyapunov_orbit, False, 20, 1e-6)

        if lyapunov_orbit.is_valid:
            orbit_period = lyapunov_orbit.period
            print("x_0 = {:f} km  period: {:f} days".format(x / 1000., orbit_period * problem.getNormalisationForTime() / 86400.))

            if i % plot_frequency == 0:
                ax = plot_orbit(lyapunov_orbit, color=color, alpha=alpha, ax=ax)
        else:
            lyapunov_orbit = None
        i = i + 1


def plot_orbit(orbit, color, alpha, ax):
    orbit_period = orbit.period
    xyz = orbit.propagate([0, orbit_period])
    return visu.plot_trajectory(xyz, is2d=True, color=color, alpha=alpha,  ax=ax)


if __name__ == "__main__":
    run_example10()
