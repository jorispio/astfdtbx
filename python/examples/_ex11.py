# -*- coding: utf-8 -*-
"""
Example 11: Family of vertical orbits
=====================================
This example plots a family of vertical orbits around L1, L2 and L3
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_11.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
from r3bpy.visu import plot_system, plot_trajectory
from r3bpy.main import Problem
from r3bpy.orbits import LyapunovOrbit, LyapunovOrbitFamilyType


def run_example11():
    # distance from L1 to the Moon is around 58000 km, while L2 to the Moon is 65000km
    r12 = constants.DISTANCE_EARTH_MOON
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON

    x1_init = 50000 * 1e3
    x1_final = 51000 * 1e3
    x1_step = 1000 * 1e3

    x2_init = 8000 * 1e3
    x2_final = 25000 * 1e3
    x2_step = 500 * 1e3

    problem = Problem(m1, m2, r12)

    pyplot.figure()
    ax = pyplot.gca(projection='3d')
    plot_system(problem, ax=ax,
                     is2d=False, withTriangularPoints=False, colinearPointsList=[1,2])

    az_grid = np.arange(x1_init, x1_final, x1_step)
    libration_point = problem.get_libration_point_info(main.L1)
    compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.VERTICAL, az_grid, 'red', ax, plot_frequency=5)

    az_grid = np.arange(x2_init, x2_final, x2_step)
    libration_point = problem.get_libration_point_info(main.L2)
    #compute_orbits(problem, libration_point, LyapunovOrbitFamilyType.VERTICAL, az_grid, 'blue', ax, plot_frequency=5)

    ax.view_init(elev=90., azim=-90)
    pyplot.title('Vertical Lyapunov Orbit around L1 and L2')
    pyplot.show()


def compute_orbits(problem, libration_point, planarType, az_grid, color, ax, plot_frequency=5):
    lyapunov = LyapunovOrbit(libration_point, planarType)
    lyapunov_orbit = None

    i = 0
    for Ax in az_grid:
        print("Ax = {:f} km".format(Ax / 1000.))

        if lyapunov_orbit is None:
            lyapunov_orbit = lyapunov.find_orbit(Ax / problem.getNormalisationForDistance(), True, 10)
        else:  # use previous solution to compute new Halo
            lyapunov_orbit = lyapunov.find_orbit(Ax / problem.getNormalisationForDistance(), lyapunov_orbit, False, 20, 1e-6)

        if True:  # lyapunov_orbit.is_valid:
            orbit_period = lyapunov_orbit.period
            print("period: {:f} days".format(orbit_period * problem.getNormalisationForTime() / 86400.))

            if i % plot_frequency == 0:
                ax = plot_orbit(lyapunov_orbit, color=color, ax=ax)
        else:
            lyapunov_orbit = None
        i = i + 1
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')


#
def plot_orbit(orbit, color, ax, a_m=1):
    orbit_period = orbit.period
    xyz = orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, a_m=a_m, color=color, alpha=0.5, ax=ax)


if __name__ == "__main__":
    run_example11()
