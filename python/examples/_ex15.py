# -*- coding: utf-8 -*-
"""Example 15: Low-thrust transfer
==================================
This example computes a low thrust transfer between a LEO and a L1 NRHO.
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_15_mayavi.png'
# Joris T. OLYMPIO

import matplotlib.pyplot as pyplot
import numpy as np
from numpy import cos, sin

from scipy.optimize import fsolve, leastsq

import r3bpy.core as constants
from r3bpy.main import Problem, Propagator, ThrusterData, LowThrustProblem, LowThrustPropagator
from r3bpy.orbits import HaloOrbit
import r3bpy.main as r3bp
import r3bpy.visu as visu
from r3bpy.visu import plot_system, plot_trajectory, plot_orbit
import r3bpy.visu.mayavi as mlab

image_file_earth='data/world.topo.bathy.200412.3x5400x2700.jpg'
image_file_moon='data/lroc_color_poles_1k.jpg'
bgcolor=(0,0,0)


cache_x = [] # use to prevent propagate the state twice, in func and dfunc
cache_l = []

def get_depature_state(r, theta, deltaV, mu_ratio, w0, DU, VU):
    """Compute departure state in the rotating frame."""
    v = np.sqrt((1.0 - mu_ratio) / (r / DU)) * VU
    x0 = r * cos(theta) / DU - mu_ratio
    y0 = r * sin(theta) / DU
    return [x0, y0, 0., -(v + deltaV) / VU * sin(theta) + y0, (v + deltaV) / VU * cos(theta) - x0 - (r / DU - mu_ratio) * w0, 0.0]

        
def func(x, lt_propagator, x0, tof, xf):
    """Constraint function psi(x, t)."""
    global cache_x
    global cache_l

    lmd = list(x[0:7])
    dt = x[6]
    _, ltp, ltl = lt_propagator.propagate(x0, lmd, [0.,  tof + dt], -1)
    cache_x = ltp[-1]
    cache_l = ltl[-1]    
    ltp = np.array(ltp[-1])    
    err = ltp[0:6] - xf[0:6]
    err = list(err)
    ham_tf = 0 #np.dot(ltp[0:6], ltp[7:13]) - 1
    err.append(ham_tf)
    print(err)
    return np.array(err)
                

def dfunc(x, lt_propagator, x0, tof, xf):
    """Jacobian [dpsi/dlambda, dpsi/dt]"""
    global cache_x
    global cache_l

    lmd = list(x[0:7])
    dt = x[6]
    stm = lt_propagator.propagate_stm(x0, lmd, [0.,  tof + dt], -1)
    
    #_, ltx, ltl = lt_propagator.propagate(x0, lmd, [0.,  tof + dt], -1)
    dpf_dt = lt_propagator.time_derivative(cache_x, cache_l)

    jac = np.zeros((7,7))
    jac[0:7, 0:7] = stm[0:7, 7:14]
    jac[0:6, 6] = dpf_dt[0:6] # dx/dt
    return jac

def test_dfunc(x, lt_propagator, x0, tof, xf):
    """Test function for verifying the Jacobian."""
    fref = func(x, lt_propagator, x0, tof, xf)    

    # finite differences
    h = 1e-6
    j = []
    for irow in np.arange(0, 7):
        x[irow] = x[irow] + h
        f = func(x, lt_propagator, x0, tof, xf)
        x[irow] = x[irow] - h
        j.append((f - fref) / h)

    print("Computing reference")
    jac_ref = dfunc(x, lt_propagator, x0, tof, xf)
    
    print("Result")
    print("  STM ref")
    print(jac_ref)
    print("  STM by F-D")
    print(np.array(j).T)
                
def run_example15(do_solve=True, do_test_dfunc=True, mayavi_plot=False):
    r12 = constants.DISTANCE_EARTH_MOON
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON
    Az = 40000. * 1e3   
    mu = constants.MU_EARTH
    
    thrust = 0.5
    isp = 3000.0
    init_mass = 1000.0
    r = constants.EGM96_EARTH_EQUATORIAL_RADIUS + 36000.e3
    theta = np.pi      # design variable for Earth departure
    deltaV0 = 0.   # design variable for Earth departure


    # problem creation and normalisations
    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    libration_point = problem.get_libration_point_info(r3bp.L1)
    DU = problem.get_distance_normalisation()
    TU = problem.get_time_normalisation()  
    VU = DU / TU
    MU = 1000.
    FU = MU * DU / (TU * TU)
    
    # destination Halo orbit
    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az / DU, verbose=False, max_iter=50, tol=1e-8, max_step=1.0)
    # propagate with 20 points per orbit
    _, xyz_halo = halo_orbit.propagate([0, halo_orbit.period], halo_orbit.period / 20)
    _, xyz_halo_discrete = halo_orbit.propagate([0, halo_orbit.period], halo_orbit.period / 20) # bug
    
    # start LEO orbit
    period = 2 * np.pi * np.sqrt(r**3 / mu)
    w0 = np.sqrt((m1 + m2) / r12**3) * TU
    print("w0=", w0, "  T=", (2 * np.pi / w0) * (TU / 86400.), " days")
    print("Dist_Earth=", mu_ratio * DU, " m")

    init_orbit_point_rtbp = get_depature_state(r, 0., 0., mu_ratio, w0, DU, VU)
    propagator = Propagator(mu_ratio)
    propagator.set_tol(1e-9, 1e-9)
    t, p_leo = propagator.propagate(init_orbit_point_rtbp, [0,  period/TU])

    # start transfer point
    init_point_eci = [r / DU, 0., 0., 0., np.sqrt(mu / r) / VU, 0.]
    print("x0_eci=", init_point_eci)
    init_point = get_depature_state(r, theta, deltaV0, mu_ratio, w0, DU, VU)
    print("x0_rtbp=", init_point)
    init_point = p_leo[300]  # random start point
    print("x0_rtbp=", init_point)
    
    # define Low-thrust system
    tof = 65. * 86400. / TU - 2.5 # fixed transfer duration
    eps = ThrusterData(thrust=thrust/FU, isp=isp/VU)

    # define Low-thrust transfer problem
    IDX_HALO_PT = 1  # random point along the HALO
    lambda0 = [0.8, 0.5, 0.01, -0.02, -0.1, 0.01, 0.2] # [lx, ly, lz, lvx, lvy, lvz, dt]
    x0 = [init_point[0], init_point[1], init_point[2], init_point[3], init_point[4], init_point[5], init_mass / MU]
    target_xf = list(xyz_halo_discrete[IDX_HALO_PT])
    print("x0=", x0)
    print("xf=", target_xf)        

    lt_propagator = LowThrustPropagator(mu_ratio, eps)
    lt_propagator.set_tol(1e-10, 1e-10)  
    lambdas = lambda0
    if do_test_dfunc:
        print("Derivative check")
        test_dfunc(lambda0, lt_propagator, x0, tof / 10, target_xf)
        
    dt = 0
    if do_solve:
        print("Solving")
        # Solution : [ 0.6520422   0.61742272  0.53292605  0.01924395  0.01221598 -0.00111315].  dt=-0.3345777330652422
        #       
        #lambdas, info, ier, mesg = fsolve(func, lambda0, Dfun=dfunc, 
        #                                    args=(lt_propagator, x0, tof, target_xf), 
        #                                    epsfcn=1e-7, factor=0.1, full_output=True)           
        lambdas, _, info, mesg, ier = leastsq(func, lambda0, Dfun=dfunc, 
                                            args=(lt_propagator, x0, tof, target_xf), 
                                            epsfcn=1e-7, factor=0.1, full_output=True)            
        print("Message : ", mesg)
        print("fval : ", info['fvec'])
        print("nfev : ", info['nfev'])        
        dt = lambdas[6]
        print("Solution : {}.  dt={}".format(lambdas[0:6], dt))

    # propagate solution
    print("propagating")
    t, ltx, ltl = lt_propagator.propagate(x0, list(lambdas), [0.,  tof + dt], -1)   
    ltx = np.array(ltx)
    ltl = np.array(ltl)    

    u = []
    guidance = lt_propagator.get_guidance()
    guidance.control_direction(list(ltx), list(ltl))

    # plots
    if not mayavi_plot:
        ax = plot_system(problem, is2d=False, withTriangularPoints=False, primaryList=[2], colinearPointsList=[1])
        ax = plot_orbit(halo_orbit, ax=ax, color='orange', legend='Halo')    
        plot_trajectory(p_leo, ax=ax, color='blue', legend='LEO')
        plot_trajectory(ltx, ax=ax, color='red', alpha=0.7, legend='Low thrust transfer')
        for pt in xyz_halo_discrete:
            ax.scatter(pt[0], pt[1], pt[2], marker='o', color='orange', alpha=0.3)
        ax.scatter(target_xf[0], target_xf[1], target_xf[2], marker='o')
        pyplot.title("Low thrust transfer to Earth-Moon L1 NRHO")
        pyplot.show()

    if mayavi_plot:
        DU = problem.get_distance_normalisation()
        earth_pos = [-mu_ratio,0.0,0.0]
        moon_pos = [1.0-mu_ratio, 0.0, 0.0]
        objects = [(earth_pos, (6378.e3/DU, 6378.e3/DU, 6378.e3/DU), image_file_earth), 
                   (moon_pos, (1737.e3/DU,1737.e3/DU,1737.e3/DU), image_file_moon)]
        fig = mlab.figure("NLow thrust transfer to Earth-Moon L1 NRHO", size=(1024, 1024), bgcolor=bgcolor)
        fig = mlab.create_scene(fig, objects)
        mlab.plot_trajectory(p_leo, fig=fig, color=(0.0, 0.84, 1.0), legend="Medium Earth Orbit")
        mlab.plot_orbit(halo_orbit, fig=fig, color=(1.0, 0.40, 0.0), legend="South Halo")
        mlab.plot_trajectory(ltx, fig=fig, color=(1.0, 0.84, 0.0), legend="low thrust transfer")
        mlab.view(75, 140, focalpoint=moon_pos, figure=fig) 
        mlab.show()
    
if __name__ == "__main__":
    run_example15(mayavi_plot=True)

