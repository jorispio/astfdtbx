# -*- coding: utf-8 -*-
"""
Example 1: Zero Velocity Curve and Jacobi constant
==================================================
This example shows the Jacobi level curves, and a trajectory at constant energy."""
# sphinx_gallery_thumbnail_path = '_static/Figure_1_1.png'
# Joris T. OLYMPIO

# Read packages into Python library:
import numpy as np
import matplotlib.pyplot as pyplot
from matplotlib import cm

import r3bpy.core as constants
from r3bpy.main import Problem
import r3bpy.main as main
import r3bpy.visu as visu


def propagate_from_jacobi_constant(pos, mu_ratio, c_particle, duration=5.0):
    x0 = pos[0]
    y0 = pos[1]
    z0 = pos[2]
    v = main.Jacobi_velocity_magnitude(mu_ratio, c_particle, np.array([x0, y0, z0]))
    posvel = np.array([x0, y0, z0, -v/np.sqrt(2), v/np.sqrt(2), 0.])
    print("vel=", v)
    print("Jacobi constant=", main.Jacobi_constant(mu_ratio, x0, y0, z0, -v/np.sqrt(2), v/np.sqrt(2), 0.))
    
    propagator = main.Propagator(mu_ratio)
    t, xyz = propagator.propagate(posvel, [0, duration])
    t, xyz = propagator.propagate(posvel, [0, duration])    
    return np.array(xyz)


def run_example1():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    print("mu_ratio: ", mu_ratio)
    
    # equivalent to calling problem.get_libration_points(m1, m2, r12)
    # Note we use 1 instead of r12 to have normalised distance
    Li, Cj = main.computeLibrationPoints(0, 1, m1, m2)
    print("Libration point Jacobi constants:", Cj)
    
    x, y = np.meshgrid(np.arange(-1.5, 1.5, 0.005), np.arange(-2., 2., 0.005))
    z = 0.0
    C = main.JacobiC(mu_ratio, x, y, z)
    #C[C > 7.0] = np.nan

    levels = [3.0, 3.02, 3.20, 3.3, 3.5, 5.0] 
    
    # 3D plot
    c3d = C
    c3d[c3d>7] = np.nan
    ax = visu.plot_jacobi_levels(x, y, c3d, libration_points=None, levels=levels, is3d=True)
    ax.set_xlabel('x (DU)')
    ax.set_ylabel('y (DU)')
    ax.set_zlabel('C')
    ax.set_title("Zero velocity curves")
     
    xyz_1 = propagate_from_jacobi_constant([0.1, 0.5, 0.], mu_ratio, 3.02, duration=5)
    xyz_2 = propagate_from_jacobi_constant([0.1, 0.5, 0.], mu_ratio, 3.2178, duration=20)

    # 2D plot
    ax = visu.plot_jacobi_levels(x, y, C, libration_points=Li, levels=levels)
    ax.plot(xyz_1[:, 0], xyz_1[:, 1])
    ax.plot(xyz_2[:, 0], xyz_2[:, 1])
    visu.plot_system(problem, ax=ax)

    ax.set_xlim(-1.5, 1.5)
    ax.set_ylim(-2.0, 2.0)    
    ax.set_xlabel('x (DU)')
    ax.set_ylabel('y (DU)')
    ax.legend(loc="upper left", fontsize=14)

    pyplot.title("Zero velocity contours with trajectory")
    pyplot.show()


if __name__ == "__main__":
    run_example1()
