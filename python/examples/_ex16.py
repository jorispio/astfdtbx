# -*- coding: utf-8 -*-
"""Example 16: multiple-segment orbit solution
===========================================
This example plots a Lissajous orbit around L1, from a multiple-segment initial guess generated from a first order approximation.
The lissajous orbit is constructed minimising the defects at the patch points of the initial segmented trajectory.
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_16_all.png'
# Joris T. OLYMPIO
from math import pi

import matplotlib.pyplot as pyplot
import numpy as np


# Read packages into Python library:
import r3bPy.core as constants
import r3bPy.main as main
from r3bPy.main import Propagator
from r3bPy.main import Problem
from r3bPy.orbits import LissajousOrbit
from r3bPy.orbits import HaloOrbit
from r3bPy.orbits import OrbitSegment
from r3bPy.orbits import Segments
from r3bPy.orbits import LissajousMultiPointsOrbit
from r3bPy.visu import plot_segments
from r3bPy.visu import plot_libration_point
from r3bPy.visu import plot_trajectory


def run_example16():
    Ax = 3000 * 1e3
    phi_x = 180 * pi / 180.
    Az = 3000 * 1e3
    phi_z = 90 * pi / 180.
    n_revolutions = 1
    n_segments = 3

    problem = Problem(constants.MU_EARTH, constants.MU_MOON, constants.DISTANCE_EARTH_MOON)
    mu_ratio = problem.get_mu_ratio()
    DU = problem.getNormalisationForDistance()
    Ax = Ax / DU
    Az = Az / DU

    libration_point = problem.get_libration_point_info(main.L1)

    reference_orbit = HaloOrbit(libration_point)
    reference_orbit.set_parameter(Az, 0)
    #print("Period: {:f}".format(reference_orbit.get_analytical_period()));

    lissajous = LissajousMultiPointsOrbit(libration_point)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    print("{:d} segments".format(len(segments)))

    # generate and plot initial segments
    propagator = Propagator(mu_ratio)
    period = 2 * pi / reference_orbit.get_inplane_frequency()

    segments_solution = lissajous.find(segments, True, 10)

    ams = problem.getNormalisationForDistance() / problem.getNormalisationForTime()
    dvs = Segments(segments_solution).compute_dv()
    for dv in dvs:
        print("dv={:f} m/s".format(np.linalg.norm(dv)))  # * ams))

    # plots
    ax = plot_segments(propagator, period / n_segments, segments, color='red', legend='Initial segments')
    ax = plot_segments(propagator, period / n_segments, segments_solution, ax, color='blue', legend='Solution segments')
    plot_libration_point(problem, main.L1, ax)
    ax.legend(loc='upper right', shadow=True)
    pyplot.show()


if __name__ == "__main__":
    run_example16()
