# -*- coding: utf-8 -*-
"""
Example 31: Perturbations
=========================
This example shows the effect of a perturbation force over a Halo orbit.
Station Keeping maneuvers (delta-v) along the orbit are implemented.
"""
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bPy.core as constants
from r3bPy.main import Problem
from r3bPy.main import Propagator
from r3bPy.main import ConstantForce
from r3bPy.orbits import LyapunovOrbit
from r3bPy.orbits import HaloOrbit
from r3bPy.orbits import LissajousOrbit
from r3bPy.orbits import LissajousMultiPointsOrbit
import r3bPy.core as constants
import r3bPy.main as main
import r3bPy.visu as visu
from r3bPy.visu import plot_segments
from r3bPy.visu import plot_trajectory


def run_example31():
    # distance from L1 to the Moon is around 58000 km, while L2 to the Moon is 65000km
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    Az = 5000. * 1e3  # m

    problem = Problem(m1, m2, r12)
    libration_point = problem.get_libration_point_info(main.L2)
    mu_ratio = problem.get_mu_ratio()
    propagator = Propagator(mu_ratio)

    Az = Az / problem.getNormalisationForDistance()

    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az, True, 100, 1e-6, 0.1)
    orbit_period = halo_orbit.period
    print("Halo period: {:f} days".format(orbit_period * problem.getNormalisationForTime() / 86400.))
    print("Orbit amplitudes: Ax={:f} km; Ay={:f} km; Az={:f} km; ".format(
        halo_orbit.Ax * problem.getNormalisationForDistance() / 1e3,
        halo_orbit.Ay * problem.getNormalisationForDistance() / 1e3,
        halo_orbit.Az * problem.getNormalisationForDistance() / 1e3))
    xyz_halo = halo_orbit.propagate([0, orbit_period])

    # Segment the Halo orbit solution
    n_segments = 10
    lissajous = LissajousMultiPointsOrbit(libration_point)
    segments = lissajous.get_segments(halo_orbit, n_segments)
    ax = plot_segments(propagator, orbit_period / n_segments, segments, color='red', legend='Initial segments')

    # add sun perturbation to propagator
    f = constants.MU_SUN / (m1 + m2) / ((constants.DISTANCE_SUN_EARTH + libration_point.get_state()[0]) / r12)**3
    print("Add constant direction force {:f}".format(f))
    force = ConstantForce(np.array([f, 0, 0]))
    propagator.add_force(force)

    # Propagate the halo orbit solution with sun perturbation
    print("Propagating with perturbation force")
    pos = halo_orbit.r0
    vel = halo_orbit.v0
    pos_vel = [pos[0], pos[1], pos[2], vel[0], vel[1], vel[2]]
    t_halo_sun, xyz_halo_sun = propagator.propagate(pos_vel, [0, orbit_period])

    # propagate the segmented Halo orbit solution with sun perturbation
    plot_segments(propagator, orbit_period / n_segments, segments, ax=ax, color='purple', legend='Perturbed segments')

    #
    visu.plot_system(problem, ax=ax, is2d=False, withPrimaries=True, withColinearPoints=True, withTriangularPoints=False, colinearPointsList=[1, 2])
    plot_trajectory(xyz_halo, ax=ax, color="blue", legend='R3BP orbit')
    plot_trajectory(xyz_halo_sun, ax=ax, color="orange", legend='Perturbed orbit')
    pyplot.legend(loc='lower right')
    pyplot.show()


def plot_orbit(orbit, color, ax):
    orbit_period = orbit.period
    xyz = orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, color=color, ax=ax)


if __name__ == "__main__":
    run_example31()
