# -*- coding: utf-8 -*-
"""Example 9: long period orbit
================================
This example plots a long period orbit.
 The long period orbit is computed around the L5 libration point of the Sun-Earth three body problem.
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_9.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
from r3bpy.main import Problem
import r3bpy.main as main
from r3bpy.orbits import LongShortPeriodicOrbit, PeriodicOrbitClass
import r3bpy.visu as visu


def run_example9():

    lambda1_init = 0.1
    lambda1_final = 0.4
    lambda1_step = 0.1

    problem = Problem(0.00095)  # Sun-Jupiter system

    libration_point4 = problem.get_libration_point_info(main.L4)
    short_orbit_L4 = LongShortPeriodicOrbit(libration_point4, PeriodicOrbitClass.SHORT_PERIOD)

    az_grid1 = np.arange(lambda1_init, lambda1_final, lambda1_step)

    ax = visu.plot_system(problem, is2d=True)

    for parameter in az_grid1:
        orbit = short_orbit_L4.find_orbit(parameter, True, 30, 1e-6)
        orbit_period = orbit.period
        print("period: {:f} days".format(orbit_period))
        xyz = orbit.propagate([0, orbit_period])
        visu.plot_trajectory(xyz, is2d=True, ax=ax)

    pyplot.title("Family of long period orbits around L4")
    pyplot.show()


if __name__ == "__main__":
    run_example9()
