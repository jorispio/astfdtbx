# -*- coding: utf-8 -*-
"""Example 14: L1 NRHO
======================
This example computes L1 NRHO for the Earth-Moon RTBP.
"""
# Joris T. OLYMPIO

import matplotlib.pyplot as pyplot
import numpy as np

import r3bpy.core as constants
from r3bpy.main import Problem
from r3bpy.orbits import HaloOrbit, LissajousOrbit
import r3bpy.main as r3bp
import r3bpy.visu as visu
from r3bpy.visu import plot_system, plot_orbit
from r3bpy.orbits import ContinuationProcedure, AMPLITUDE_PARAMETER

def run_example14():
    r12 = constants.DISTANCE_EARTH_MOON
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON
    AzInit = 40000 * 1e3
    AzFinal = 79000 * 1e3
    AzStep = 50 * 1e3
    rp = 70000. * 1e3 / r12

    problem = Problem(m1, m2, r12)
    libration_point = problem.get_libration_point_info(r3bp.L1)
    DU = problem.get_distance_normalisation()
    TU = problem.get_time_normalisation()  
    
    az_grid = np.arange(AzInit, AzFinal, AzStep)
    stability_index = []


    halo = HaloOrbit(libration_point)
    halo_orbit = None

    continuation = ContinuationProcedure(AMPLITUDE_PARAMETER, AzInit / DU, AzFinal / DU, AzStep / DU, tol=1e-6, max_iter=100)
    
    halo_orbit = halo.find_orbit(AzInit / DU, verbose=False, max_iter=50, tol=1e-8, xtol=1e-8, max_step=1.0)
    
    colors = pyplot.cm.rainbow(np.linspace(0, 1, az_grid.size))
    icolor = 0
    ax = plot_system(problem, is2d=False, withTriangularPoints=False, primaryList=[2], colinearPointsList=[1])

    while not continuation.is_completed():
        halo_orbit_current = halo.find_orbit(halo_orbit, continuation, max_iter=50, tol=1e-8, max_step=1.0)

        if halo_orbit_current.is_valid:
            halo_orbit = halo_orbit_current
            orbit_period = halo_orbit.period
            smin = np.min([halo_orbit.mnu1, halo_orbit.mnu2])
            smax = np.max([halo_orbit.mnu1, halo_orbit.mnu2])
            print("Halo: period: {:f} days, stability index: {}, {}".format(
                                                           orbit_period * TU / 86400.,
                                                           smin, smax))

            stability_index.append((smax, smin))
            if abs(smax - 1) < 0.005 or abs(smin + 1) < 0.005:
                col = "black"
            else:
                col = colors[icolor]
                icolor = icolor + 1
            ax = plot_orbit(halo_orbit, col, ax)

        else:
            halo_orbit = None
            stability_index.append((np.nan, np.nan))

    pyplot.title("NRHO for L1/L2")

    visu.plot_stability(az_grid / 1000., stability_index, xlabel="Az (km")
    pyplot.title("NRHO for L1/L2")

    pyplot.show()



if __name__ == "__main__":
    run_example14()
