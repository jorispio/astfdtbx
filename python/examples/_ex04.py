# -*- coding: utf-8 -*-
"""Example 4: family of Halo orbit around L1
=============================================
This example plots a family of Halo orbit around L1, with history of the stability index."""
# sphinx_gallery_thumbnail_path = '_static/Figure_4.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as r3bp
from r3bpy.main import Problem
from r3bpy.orbits import HaloOrbit, HaloOrbitFamily
from r3bpy.orbits import ContinuationProcedure, PSEUDO_ARC_LENGTH, AMPLITUDE_PARAMETER, JACOBI_CONSTANT
from r3bpy.visu import plot_system, plot_stability, plot_period, plot_trajectory, plot_orbit

#from r3bpy.orbits import abaq

#abacus = abaq.Abaq()
#mu_ratio, init_point, period, trajectory, description = abacus.get_data("Earth", "Moon", "NRHO", "L2")



def plot_jacobi_constant(rp, jaccst, title=None):
    ''' plot j(rp) '''
    pyplot.figure()
    ax = pyplot.gca()
    ax.grid(c='k', ls='-', alpha=0.3)
    ax.set_title('Jacobi constant of the family')
    pyplot.plot(rp, jaccst)
    pyplot.xlabel("perilune, km")
    pyplot.ylabel("J")    
    pyplot.title(title)

def plot_xy(x0z0, title=None):
    ''' plot x(0), z(0) '''
    pyplot.figure()
    ax = pyplot.gca()
    ax.grid(c='k', ls='-', alpha=0.3)
    ax.set_title('Initial state of the family')
    pyplot.plot(np.array(x0z0[:,0]), np.array(x0z0[:,1]))
    pyplot.xlabel("x_0, km")
    pyplot.ylabel("z_0, km")    
    pyplot.title(title)

def run_example4():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    AzInit = 15000 * 1e3
    AzFinal = 995000 * 1e3
    AzStep = 500 * 1e3

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    TU = problem.get_time_normalisation()
    DU = problem.get_distance_normalisation()
    print("mu_ratio=", mu_ratio)

    libration_point = problem.get_libration_point_info(r3bp.L2)

    halo = HaloOrbit(libration_point, HaloOrbitFamily.NORTHERN)

    continuation = ContinuationProcedure(PSEUDO_ARC_LENGTH, 0.0, 0.35, 3e-4, tol=1e-6, max_iter=200000, verbose=True)

    halo_orbit = halo.find_orbit(AzInit / DU, verbose=False, max_iter=50, tol=1e-10, xtol=1e-9, max_step=1.)
    
    orbits = []
    halo_orbit_current = halo_orbit
    n = 0
    while not continuation.is_completed():
        # use previous solution to compute new Halo
        try:
            halo_orbit_current = halo.find_orbit(halo_orbit, continuation, max_iter=50, max_step=1.0, tol=1e-10, xtol=1e-16)
        except:
            break

        if halo_orbit_current.is_valid:
            halo_orbit = halo_orbit_current
            
            orbit_period = halo_orbit.period
            if n % 10 == 0:
                print(" Halo ({:f}): period: {:f} days, stability index: {:f}, jacobi={:f}, rp={:f} km".format(                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                           continuation.get_parameter_value(),
                                                           orbit_period * TU / 86400., 
                                                           halo_orbit.get_stability_index(),
                                                           halo_orbit.jacobi_constant,
                                                           halo_orbit.get_periapsis(r3bp.P2) * DU / 1000.))
                orbits.append(halo_orbit)
            n = n + 1
      
    
    # plotting                                                                                                                                                          
    colors = pyplot.cm.rainbow(np.linspace(0, 1, len(orbits)))
    print(len(orbits), " orbits")    
    ax = plot_system(problem, is2d=False, withTriangularPoints=False, primaryList=[2], colinearPointsList=[1, 2])    
    icolor = 0    
    rp = []
    period = []    
    x0z0 = []
    stability_index = []
    jacobi_constant = []
    for halo_orbit in orbits: #[0:50:-1]:
        ax = plot_orbit(halo_orbit, colors[icolor], ax)
        rp.append(halo_orbit.get_periapsis(r3bp.P2))
        period.append(halo_orbit.period)
        x0z0.append([halo_orbit.r0[0], halo_orbit.r0[2]])
        jacobi_constant.append(halo_orbit.jacobi_constant)
        stability_index.append((halo_orbit.mnu1, halo_orbit.mnu2))
        icolor = icolor + 1    
    pyplot.title("Family of Halo orbits around L1")        
    
    rp = np.array(rp)
        
    plot_xy(np.array(x0z0) * DU / 1000., "Family of Halo orbits around L1")
    
    plot_jacobi_constant(rp * DU / 1000., jacobi_constant, "Family of Halo orbits around L1")
    
    plot_stability(rp * DU / 1000., stability_index, xlabel="rp (km)")

    period = np.array(period)
    plot_period(rp * DU / 1000., period * TU / 86400, xlabel="Perilune Radius, km")

    pyplot.show()



if __name__ == "__main__":
    run_example4()
