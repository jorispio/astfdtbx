"""Example 13: Pointcaré section map
====================================
This example plots a Poincaré section (z=0) of the orbits in the central manifold of L1, for the Jacobi constant Cj=3.198.
Warning, this example takes significant time to run. To reduce computational time, the process is parallelised and will use 
most of your CPUs.
The result is stored in a numpy data file for post processing if necessary.
"""
# sphinx_gallery_thumbnail_path = '_static/Figure_13.png'
# Joris T. OLYMPIO

import os.path

import matplotlib.pyplot as pyplot
import numpy as np
from math import sqrt, sin, cos

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
from r3bpy.main import Problem, Jacobi_constant, Propagator, AxisId
from r3bpy.orbits import LyapunovOrbit
from r3bpy.visu import plot_system, plot_trajectory

import time
import multiprocessing as mp


outfile_fmt="poincare_data_{}.npy"
pointcare_pts = []

def update_state(posvel, mu_ratio, Cj):
    """Update state so that it satisfy the Jacobi constant."""
    CX = Jacobi_constant(mu_ratio, posvel[0], posvel[1], posvel[2], 0., 0., 0.)  # X = -2U
    vsq = (CX - Cj)

    if vsq < 0:
        return []

    v = sqrt(vsq)
    uv = posvel[3:6] / np.linalg.norm(posvel[3:6])
    vy = uv[0] * v
    vx = uv[1] * v
    vz = uv[2] * v
    return np.array([posvel[0], posvel[1], posvel[2], vx, vy, vz])
    

def compute_returns(x, nu, eta, mu_ratio, Cj, depth=100, propagator=None, max_xdot=2.5, yCrossTol=1e-6):
    """Compute returns. This function can be used in a parallised process."""
    if not propagator:
        propagator = Propagator(mu_ratio)
        propagator.set_tol(1e-10, 1e-10) 
        propagator.add_detector(AxisId.Y_AXIS) 

    propagation_duration = 5.0  
    y = 1e-3 # start a bit off axis
    z = 0.001
    # Cj = -(vx * vx + vy * vy + vz * vz) + X
    CX = Jacobi_constant(mu_ratio, x, y, z, 0., 0., 0.)  # X = -2U
    vsq = (CX - Cj)

    if vsq < 0:
        return []

    v =-sqrt(vsq)
    #if abs(v) > max_xdot: # limit the map
    #    return []
    
    vy = v * nu * cos(eta)  # preferentiably normal to x-axis
    vz = v * nu * sin(eta)
    vx = sqrt(vsq - vy * vy - vz * vz)
    pos_vel = np.array([x, y, z, vx, vy, vz])

    _pointcare_pts = []
    
    # compute 1st return
    for it in range(0, depth):        
        pos_vel = update_state(pos_vel, mu_ratio, Cj) # enforce the Jacobi energy
        if len(pos_vel) == 0:
            break

        try:
            t, xyz = propagator.propagate(pos_vel, [0.0, propagation_duration])
            #xyz = np.array(xyz)
            pos_vel = xyz[-1]
        except ValueError:
            pass

        #(pos_vel[4] > 0) and
        if  (abs(pos_vel[1]) < yCrossTol): # ydot > 0                        
            p = list(pos_vel)
            _pointcare_pts.append(np.array(p))
        else:
            # fix point with Newton step
            if (abs(pos_vel[4]) > 0)  and (abs(pos_vel[1]) < 50 * yCrossTol):
                s = pos_vel[1] / pos_vel[4]
                posvel_cross = pos_vel[0:3] - s * pos_vel[3:6]
                posvel_cross = np.append(posvel_cross, pos_vel[3:6], 0)
                p = list(posvel_cross)
                _pointcare_pts.append(np.array(p))
    return _pointcare_pts
    

def collect_result(results):
    """callback function to collect the output in `pointcare_pts`"""
    global pointcare_pts
    if len(results) > 0:
        pointcare_pts.extend(results)

    
def plot_pmap(problem, Cj, xlim, pointcare_pts):
    """Plot Pointcaré section map."""
    # prepare data for plots
    pointcare_pts = list(pointcare_pts)
    x = [p[0] for p in pointcare_pts]
    dxdt = [p[3] for p in pointcare_pts]
    
    # plots maps (x, dx/dt) and (y, dy/dt)
    fig, ax = pyplot.subplots()
    plot_system(problem, ax=ax, withTriangularPoints=False)
    ax.scatter(x, dxdt, 0.01)#, alpha=0.5)
    ax.set_xlabel("x")
    ax.set_ylabel("\dot{x}")    
    ax.set_xlim(xlim[0], xlim[1])
    ax.set_ylim(-1.0, 1.0)
    ax.set_title("Pointcaré map for plane y=0, Cj={}".format(Cj))
    pyplot.show()


def run_example13(Cj, outfile="poincare_data.npy"):
    r12 = constants.DISTANCE_EARTH_MOON
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON

    depth = 100 # number of returns
    nx_pts = 500 # discretization of x-axis
    neta = 20
    xlim = [0.8, 1.1]

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    print("mu_ratio = ", mu_ratio)
    print("Jacobi constant set to Cj = ", Cj)    
    
    _, Cji = main.computeLibrationPoints(0, r12, m1, m2)
    print("Libration point Jacobi Constants:", Cji)

    if os.path.exists(outfile):
        print("Loading computed data")
        global pointcare_pts
        pointcare_pts = np.load(outfile, allow_pickle=True)
        pointcare_pts = list(pointcare_pts)
        print("nb of pts: ", len(pointcare_pts))

    if len(pointcare_pts) == 0:
        libration_point = problem.get_libration_point_info(main.L1)

        ts = time.time()

        print("Number of processors: ", mp.cpu_count())
        pool = mp.Pool(mp.cpu_count() - 1)          
        
        for x in np.linspace(xlim[0], xlim[1], nx_pts):  # x-axis          
            for nu in np.arange(0.9, 1.0, 0.01):
                for eta in np.linspace(0.0, np.pi/2., neta):
                    r = pool.apply_async(compute_returns, args=(x, nu, eta, mu_ratio, Cj, depth), callback=collect_result)                 
                    #print(r.get())
                    #compute_returns(x, eta, mu_ratio, Cj, depth, propagator, yCrossTol)                
                                                                   
        # close Pool and let all the processes to complete   
        pool.close()        
        pool.join() 
    
        print("size=", len(pointcare_pts))
        print('Time in parallel: {} s'.format(time.time() - ts))

        #pointcare_pts = np.array(pointcare_pts)
        np.save(outfile, pointcare_pts)

    plot_pmap(problem, Cj, xlim, pointcare_pts)



if __name__ == "__main__":
    c = 3.179
    run_example13(c, outfile_fmt.format(c))

