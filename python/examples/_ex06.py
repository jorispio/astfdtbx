# -*- coding: utf-8 -*-
"""Example 6: North and South Halo orbits around L2
====================================================
This example computes North and South Halo orbits around L2, and plots using mayavi."""
# sphinx_gallery_thumbnail_path = '_static/Figure_6_mayavi.png'
#
# Joris T. OLYMPIO


import numpy as np

import r3bpy.core as constants
import r3bpy.main as r3bp
import r3bpy.visu.mayavi as visu
from r3bpy.main import Problem
from r3bpy.orbits import HaloOrbit, HaloOrbitFamily, print_info

from mayavi import mlab

image_file_earth='data/world.topo.bathy.200412.3x5400x2700.jpg'
image_file_moon='data/lroc_color_poles_1k.jpg'
bgcolor=(0,0,0)

def run_example6():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    Az = 0.05

    problem = Problem(m1, m2, r12)
    libration_point = problem.get_libration_point_info(r3bp.L2)

    # compute Halo orbit
    halo = HaloOrbit(libration_point, HaloOrbitFamily.NORTHERN)
    halo_orbit_north = halo.find_orbit(Az)
    print("Orbit amplitudes: ", halo_orbit_north.Ax, halo_orbit_north.Ay, halo_orbit_north.Az)
    print_info(halo_orbit_north)

    halo = HaloOrbit(libration_point, HaloOrbitFamily.SOUTHERN)
    halo_orbit_south = halo.find_orbit(Az)
    print("Orbit amplitudes: ", halo_orbit_south.Ax, halo_orbit_south.Ay, halo_orbit_south.Az)
    print_info(halo_orbit_south)

    _, xyz_halo_south = halo_orbit_south.propagate([0, halo_orbit_south.period ])
    _, xyz_halo_north = halo_orbit_north.propagate([0, halo_orbit_north.period ])
    _, xyz_halo_south = halo_orbit_south.propagate([0, halo_orbit_south.period ])

    # plot
    DU = problem.get_distance_normalisation()
    moon_pos = [1.0, 0.0, 0.0]
    objects = [(moon_pos, (1737.e3/DU,1737.e3/DU,1737.e3/DU), image_file_moon)]
    fig = mlab.figure("North and South Halo orbit around L2", size=(1024, 1024), bgcolor=bgcolor)
    
    #ax = visu.plot_system(problem, is2d=False, withPrimaries=False, withTriangularPoints=False, colinearPointsList=[2])
    fig = visu.create_scene(fig, objects)
                    
    visu.plot_trajectory(xyz_halo_north, fig=fig, color=(0.06, 0.32, 0.73), legend="North Halo")
    visu.plot_trajectory(xyz_halo_south, fig=fig, color=(1.0, 0.84, 0.0), legend="South Halo")
    #ax.legend(loc="upper left", fontsize=14)
    mlab.view(75, 140, focalpoint=moon_pos, figure=fig) 
    mlab.show()


if __name__ == "__main__":
    run_example6()
