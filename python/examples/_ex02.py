# -*- coding: utf-8 -*-
"""
Example 2: Earth-Moon Halo orbit
=================================
This example plots a Halo orbit, and produce an ephemeris file.
 The Halo orbit is computed around the L2 colinear point of the Sun-Earth three body problem.
 An z-excursion amplitude is chosen.

  MU_SUN (1.32712440018 * 1e20)
  MU_EARTH (3.98600432896939 * 1e014)
  AU 149597870660.0
"""
# sphinx_gallery_thumbnail_path = '../figures/Figure_2.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
import r3bpy.visu as visu
from r3bpy.main import Problem, Propagator
from r3bpy.orbits import HaloOrbit, print_info


def run_example2():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    Az = 5000. * 1e3  # m

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    print("mu_ratio=", mu_ratio)

    libration_point = problem.get_libration_point_info(main.L2)

    Az = Az / problem.getNormalisationForDistance()

    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az, verbose=True, max_iter=20, tol=1e-6, max_step=1.0)
    orbit_period = halo_orbit.period
    print("Halo period: {:f} days".format(orbit_period * problem.getNormalisationForTime() / 86400.))
    print("Orbit amplitudes: Ax={:f} km; Ay={:f} km; Az={:f} km; ".format(
        halo_orbit.Ax * problem.getNormalisationForDistance() / 1e3,
        halo_orbit.Ay * problem.getNormalisationForDistance() / 1e3,
        halo_orbit.Az * problem.getNormalisationForDistance() / 1e3))

    print_info(halo_orbit)

    pos = halo_orbit.r0
    vel = halo_orbit.v0
    pos_vel = [pos[0][0], pos[1][0], pos[2][0], vel[0], vel[1], vel[2]]
    propagator = Propagator(mu_ratio)
    t, xyz = propagator.propagate(pos_vel, [0, orbit_period])
    propagator.toFile('halo_ephemeris.txt')

    # conversion to Earth fixed frame
    xyz_eci = problem.to_fixed_frame(t, xyz)

    # plots
    visu.plot_trajectory(xyz)
    pyplot.title("Trajectory in the R3BP rotating frame")

    ax = visu.plot_trajectory(xyz_eci)
    ax.scatter(0, 0, 0, s=30, marker='*', c='blue')
    pyplot.title("Trajectory in the pseudo-inertial frame attached to the primary")

    pyplot.show()


if __name__ == "__main__":
    run_example2()
