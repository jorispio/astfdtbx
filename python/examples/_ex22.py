# -*- coding: utf-8 -*-
"""
Example 22: 
===========
This example plots a Lissajous orbit around L1, from a multiple-segment initial guess.
The lissajous orbit is constructed minimising the defects at the patch points of the initial segmented trajectory.
"""
# Joris T. OLYMPIO
from math import pi

import matplotlib.pyplot as pyplot
import numpy as np


# Read packages into Python library:
from r3bPy.main import Problem
import r3bPy.main as main
from r3bPy.orbits import LissajousOrbit
from r3bPy.orbits import OrbitSegment
from r3bPy.orbits import Segments
from r3bPy.orbits import LissajousMultiPointsOrbit
import r3bPy.core as constants
import r3bPy.visu as visu
from r3bPy.visu import plot_segments


def run_example22():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    Ax = 3000 * 1e3
    phi_x = 180 * pi / 180.
    Az = 3000 * 1e3
    phi_z = 90 * pi / 180.
    n_revolutions = 1
    n_segments = 3

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    Ax = Ax / problem.getNormalisationForDistance()
    Az = Az / problem.getNormalisationForDistance()

    libration_point = problem.get_libration_point_info(main.L3)

    lissajousOrbit = LissajousOrbit(libration_point)
    lissajousOrbit.set_parameters(Ax, phi_x, Az, phi_z)

    # generate and plot initial segments
    propagator = main.Propagator(mu_ratio)
    period = 2 * pi / lissajousOrbit.get_inplane_frequency()

    lissajous = LissajousMultiPointsOrbit(libration_point)
    segments = lissajous.get_segments(lissajousOrbit, n_segments)
    ax = None
    ax = plot_segments(propagator, period / n_segments, segments, ax, color='red', legend='Initial segments')

    segments_solution = lissajous.find(segments, True, 20)

    print("Patch velocities:")
    ams = problem.getNormalisationForDistance() / problem.getNormalisationForTime()
    dvs = Segments(segments_solution).compute_dv()
    for dv in dvs:
        print("  dv={:f} m/s".format(np.linalg.norm(dv))) # * ams))
    ax = plot_segments(propagator, period / n_segments, segments_solution, ax, color='blue', legend='Solution segments')

    #lissajous_orbit = lissajous.get_orbit_property()

    visu.plot_libration_point(problem, main.L3, ax)
    ax.legend(loc='upper right', shadow=True)
    pyplot.show()


if __name__ == "__main__":
    run_example22()
