# -*- coding: utf-8 -*-
"""Example 7: DRO around P2
============================
This example plots a DRO around P2."""
# sphinx_gallery_thumbnail_path = '_static/Figure_7.png'
# Joris T. OLYMPIO

import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
from r3bpy.main import Problem, PrimaryPointId
from r3bpy.orbits import DistantRetrogradeOrbit
from r3bpy.visu import plot_system, plot_trajectory, plot_stability


def run_example7():
    m1 = constants.MU_EARTH
    r12 = constants.DISTANCE_EARTH_MOON
    m2 = constants.MU_MOON
    x_init = 1.1
    x_final = 1.30
    x_step = 0.01

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()

    dro = DistantRetrogradeOrbit(mu_ratio, PrimaryPointId.P2)

    stability_index = []

    xlist = np.arange(x_init, x_final, x_step)
    colors = pyplot.cm.rainbow(np.linspace(0, 1, xlist.size))

    ax = plot_system(problem, is2d=False, withPrimaries=True, withTriangularPoints=False, withColinearPoints=False)
    dro_orbit = None
    icolor = 0
    for xpos in xlist:
        if dro_orbit is None:
            dro_orbit = dro.find_orbit(xpos, False, 50)
        else:  # use previous solution to compute new DRO
            dro_orbit = dro.find_orbit(xpos, False, 50)

        if dro_orbit.is_valid:
            smin = np.min([dro_orbit.mnu1, dro_orbit.mnu2])
            smax = np.max([dro_orbit.mnu1, dro_orbit.mnu2])
            stability_index.append((smax, smin))

        ax = plot_dro(dro_orbit, colors[icolor], ax)
        icolor = icolor + 1
    pyplot.title("Distant Retrograde Orbit around Secondary")

    plot_stability((xlist - 1) * problem.getNormalisationForDistance() / 1000., stability_index, xlabel="Ax (km")
    pyplot.title("Distant Retrograde Orbit around Secondary")

    pyplot.show()


def plot_dro(dro_orbit, color, ax):
    orbit_period = dro_orbit.period
    xyz = dro_orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, color=color, ax=ax)


if __name__ == "__main__":
    run_example7()
