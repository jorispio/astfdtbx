# -*- coding: utf-8 -*-
"""
Example 20: 
===========
This example plots differents Lissajous orbits, using analytical approximation and segmented construction.
Each analytical computed segment point is propagated. We can thus evaluate the analytical solution to the linearized
problem, in the nonlinear dynamics problem.
"""
# Joris T. OLYMPIO
from math import pi

import matplotlib.pyplot as pyplot
import numpy as np


# Read packages into Python library:
import r3bPy.core as constants
import r3bPy.main as main
from r3bPy.main import Problem
from r3bPy.orbits import HaloOrbit
from r3bPy.orbits import LyapunovOrbit
from r3bPy.orbits import LyapunovOrbitFamilyType
from r3bPy.orbits import LissajousEightOrbit
from r3bPy.orbits import LissajousOrbit
from r3bPy.orbits import LongShortPeriodicOrbit
from r3bPy.orbits import PeriodicOrbitClass
from r3bPy.orbits import LissajousMultiPointsOrbit
from r3bPy.visu import plot_segments
from r3bPy.visu import plot_libration_point
from r3bPy.visu import plot_trajectory


def run_example20():
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON
    r12 = constants.DISTANCE_EARTH_MOON  # m
    Az_halo1 = 100 * 1e3
    Az1 = 100 * 1e3
    Az2 = 50000 * 1e3
    n_segments = 20

    problem = Problem(m1, m2, r12)
    mu_ratio = problem.get_mu_ratio()
    libration_point1 = problem.get_libration_point_info(main.L1)
    libration_point3 = problem.get_libration_point_info(main.L3)
    libration_point4 = problem.get_libration_point_info(main.L4)
    propagator = main.Propagator(mu_ratio)

    pyplot.figure(figsize=(12, 5))

    #
    ax = pyplot.subplot(2, 3, 1, projection='3d')
    reference_orbit = HaloOrbit(libration_point3)
    Az = Az_halo1 / problem.getNormalisationForDistance()
    reference_orbit.set_parameter(Az, 0)
    lissajous = LissajousMultiPointsOrbit(libration_point3)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    period = 2 * pi / reference_orbit.get_inplane_frequency()
    plot_segments(propagator, period / n_segments, segments, ax=ax, color='red')
    plot_libration_point(problem, main.L3, ax)
    ax.legend(loc='upper right', shadow=True)
    ax.set_title("Halo orbit")
    ax.set_xlabel("x (DU)")
    ax.set_ylabel("y (DU)")
    ax.set_zlabel("z (DU)")

    #
    ax = pyplot.subplot(2, 3, 2, projection='3d')
    reference_orbit = LissajousEightOrbit(libration_point1)
    Az = Az1 / problem.getNormalisationForDistance()
    reference_orbit.set_parameter(Az, 0)
    lissajous = LissajousMultiPointsOrbit(libration_point1)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    period = 2 * pi / reference_orbit.get_inplane_frequency()
    plot_segments(propagator, period / n_segments, segments, ax=ax, color='red')
    plot_libration_point(problem, main.L1, ax)
    ax.legend(loc='upper right', shadow=True)
    ax.set_title("Eight-shape Lissajous")
    ax.set_xlabel("x (DU)")
    ax.set_ylabel("y (DU)")
    ax.set_zlabel("z (DU)")

    #
    ax = pyplot.subplot(2, 3, 3, projection='3d')
    reference_orbit = LyapunovOrbit(libration_point1, LyapunovOrbitFamilyType.PLANAR)
    Az = Az2 / problem.getNormalisationForDistance()
    reference_orbit.set_parameter(Az, 0)
    lissajous = LissajousMultiPointsOrbit(libration_point1)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    period = 2 * pi / reference_orbit.get_inplane_frequency()
    plot_segments(propagator, period / n_segments, segments, ax=ax, color='red')
    plot_libration_point(problem, main.L1, ax)
    ax.legend(loc='upper right', shadow=True)
    ax.set_title("Planar Lyapunov")
    ax.set_xlabel("x (DU)")
    ax.set_ylabel("y (DU)")
    ax.set_zlabel("z (DU)")

    #
    ax = pyplot.subplot(2, 3, 4, projection='3d')
    reference_orbit = LyapunovOrbit(libration_point1, LyapunovOrbitFamilyType.VERTICAL)
    Az = Az2 / problem.getNormalisationForDistance()
    reference_orbit.set_parameter(Az, 0)
    lissajous = LissajousMultiPointsOrbit(libration_point1)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    period = 2 * pi / reference_orbit.get_inplane_frequency()
    plot_segments(propagator, period / n_segments, segments, ax=ax, color='red')
    plot_libration_point(problem, main.L1, ax)
    ax.legend(loc='upper right', shadow=True)
    ax.set_title("Vertical Lyapunov")
    ax.set_xlabel("x (DU)")
    ax.set_ylabel("y (DU)")
    ax.set_zlabel("z (DU)")

    #
    ax = pyplot.subplot(2, 3, 5, projection='3d')
    reference_orbit = LongShortPeriodicOrbit(libration_point4, PeriodicOrbitClass.SHORT_PERIOD)
    reference_orbit.set_parameter(0.0001)
    lissajous = LissajousMultiPointsOrbit(libration_point4)
    segments = lissajous.get_segments(reference_orbit, n_segments)
    period = 2 * pi / reference_orbit.get_inplane_frequency()
    plot_segments(propagator, period / n_segments, segments, ax=ax, color='red')
    plot_libration_point(problem, main.L4, ax)
    ax.legend(loc='upper right', shadow=True)
    ax.set_title("Short Periodic Orbit")
    ax.set_xlabel("x (DU)")
    ax.set_ylabel("y (DU)")
    ax.set_zlabel("z (DU)")

    pyplot.show()


if __name__ == "__main__":
    run_example20()
