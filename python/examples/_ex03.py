# -*- coding: utf-8 -*-
"""Example 3: Halo orbit around L2, and the stable and unstable manifolds
==========================================================================
This example computes and plots a Halo orbit around L2, and the stable and unstable manofolds from the Halo."""
# sphinx_gallery_thumbnail_path = '_static/Figure_3.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import matplotlib as plot
import numpy as np

# Read packages into Python library:
import r3bpy.main as main
from r3bpy.main import Problem, Manifold
from r3bpy.orbits import HaloOrbit
from r3bpy.visu import plot_manifold, plot_system, plot_trajectory


def run_example3():
    mu_ratio = 0.01215058147718
    Az = 0.001

    problem = Problem(mu_ratio)
    libration_point = problem.get_libration_point_info(main.L2)

    # compute Halo orbit
    halo = HaloOrbit(libration_point)
    halo_orbit = halo.find_orbit(Az)
    print("Halo period: ", halo_orbit.period )
    print("Orbit amplitudes: ", halo_orbit.Ax, halo_orbit.Ay, halo_orbit.Az)

    # compute stable and unstable manifolds comming from the Halo
    eigspace = Manifold(halo_orbit)
    stable_vector_space = eigspace.get_stableManifold_basis()
    print("Stable: ", stable_vector_space)
    unstable_vector_space = eigspace.get_unstableManifold_basis()
    print("Unstable: ", unstable_vector_space)
    center_vector_space = eigspace.get_centerManifold_basis()
    print("Center: ", center_vector_space)

    # propagate halo
    orbit_period = halo_orbit.period
    xyz_halo = halo_orbit.propagate([0, orbit_period])

    # plot
    ax = plot_system(problem, is2d=False)
    plot_trajectory(xyz_halo, ax=ax)
    if stable_vector_space:
        plot_manifold(mu_ratio, xyz_halo, orbit_period, stable_vector_space[0], max_duration=-3, displace=0.01, color='g', ax=ax)
        plot_manifold(mu_ratio, xyz_halo, orbit_period, stable_vector_space[0], max_duration=5, displace=-0.01, color='g', ax=ax)
    if unstable_vector_space:
        plot_manifold(mu_ratio, xyz_halo, orbit_period, unstable_vector_space[0], max_duration=3, displace=0.01, color='r', ax=ax)
        plot_manifold(mu_ratio, xyz_halo, orbit_period, unstable_vector_space[0], max_duration=-5, displace=-0.01, color='r', ax=ax)

    pyplot.title("Stable and unstable manifolds from Halo orbit around L2")
    pyplot.show()


if __name__ == "__main__":
    run_example3()
