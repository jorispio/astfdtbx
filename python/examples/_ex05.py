# -*- coding: utf-8 -*-
"""Example 5: Lissajous-Eight orbit around L1 and L2
=====================================================
This example plots a Lissajous-Eight orbit around L1 and L2."""
# sphinx_gallery_thumbnail_path = '_static/Figure_5.png'
# Joris T. OLYMPIO


import matplotlib.pyplot as pyplot
import numpy as np

# Read packages into Python library:
import r3bpy.core as constants
import r3bpy.main as main
import r3bpy.visu as visu
from r3bpy.main import Problem
from r3bpy.orbits import LissajousEightOrbit
from r3bpy.visu import plot_trajectory


def run_example5():
    m1 = constants.MU_EARTH
    m2 = constants.MU_MOON
    r12 = constants.DISTANCE_EARTH_MOON

    problem = Problem(m1, m2, r12)

    az1_grid = np.arange(100, 101, 1) * 1e3 / problem.getNormalisationForDistance()
    az2_grid = np.arange(500, 510, 100) * 1e3 / problem.getNormalisationForDistance()

    ax = visu.plot_libration_point(problem, main.L1, is2d=False)
    #visu.plot_libration_point(problem, main.L2, ax=ax, is2d=False)

    compute_orbits(problem, main.L1, az1_grid, "blue", ax)
    #compute_orbits(problem, main.L2, az2_grid, "red", ax)

    pyplot.title("Family of Lissajous-8 orbits around L1")
    pyplot.show()


def compute_orbits(problem, libration_point, az_grid, color, ax, plot_frequency=1):
    libration_point_prop = problem.get_libration_point_info(libration_point)
    lissajous = LissajousEightOrbit(libration_point_prop)
    lissajous_orbit = None

    i = 0
    for amplitude in az_grid:
        print("Ax = {:f} km".format(amplitude * problem.getNormalisationForDistance() / 1000.))

        if lissajous_orbit is None:
            lissajous_orbit = lissajous.find_orbit(amplitude, True, 0, 1e-6, .1)

            orbit_period = lissajous_orbit.period
            print("Lissajous-Eight orbit period: ", orbit_period * problem.getNormalisationForTime() / 86400., " days")
            print("Orbit amplitudes: ", lissajous_orbit.Ax * problem.getNormalisationForDistance() / 1e3, " km",
                  lissajous_orbit.Ay * problem.getNormalisationForDistance() / 1e3, " km",
                  lissajous_orbit.Az * problem.getNormalisationForDistance() / 1e3, " km")
            print("First frequency: ", lissajous_orbit.omega)
            print("Second frequency: ", lissajous_orbit.nu)
        else:  # use previous solution to compute new Halo
            lissajous_orbit = lissajous.find_orbit(amplitude, lissajous_orbit, False, 20, 1e-6)

        if True: #lissajous_orbit.is_valid:
            orbit_period = lissajous_orbit.period
            print("period: {:f} days".format(orbit_period * problem.getNormalisationForTime() / 86400.))

            if i % plot_frequency == 0:
                plot_lissajous(lissajous_orbit, ax)
        else:
            lissajous_orbit = None
        i = i + 1


def plot_lissajous(lissajous_orbit, ax):
    orbit_period = lissajous_orbit.period
    xyz = lissajous_orbit.propagate([0, orbit_period])
    return plot_trajectory(xyz, ax=ax)


if __name__ == "__main__":
    run_example5()

