[![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) 
[![made-with-sphinx-doc](https://img.shields.io/badge/Made%20with-Sphinx-1f425f.svg)](https://www.sphinx-doc.org/) 
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) 


Py3BP
=========

## Preambule
R3BPTBX is initially a toolbox dedicated to the studying of motion around small bodies in the the three-body problem. 
This toolbox was initially developed in Matlab and C++, while studying the motion around a binary asteroid. 
I later decided to fully port the code to C++, and lately to provide a Python wrapper (Py3BP).
The primary objective is to provide an understanding of complex motion in R3BP, and then a tool for computing Lissajous orbits (incl. Lyapunov, Halo) around libration points of a three-body problem (Restricted-, Circular-).


## Installation
Configure cmake to build the Python package
```
    cmake -DBUILD_PYTHON_LIB=ON .
```

Then build
```
    make
    make install
```

To create a wheel
```
    python3  setup.py -v bdist_wheel
```    
will generate a r3bpy_pkg_x.y-cp3z-cp3z-[distrib].whl file in the dist directory.
x.y is the library version, 3z is the Python version, and distrib is your OS (e.g. Linux x86_64).

To install the wheel
```
    python3 -m pip install [wheel_name]
```    
Beware that this will is only compatible with the Python version you used for compilation.

## License
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT) The MIT License



Copyright (c) 2010-2013 Joris OLYMPIO


## Problems
If you find any issue with the toolbox, please drop me a message. I will do my best to correct any issue promptly.
I also welcome any contribution that can improve the toolbox. 
