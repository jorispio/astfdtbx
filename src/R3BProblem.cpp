// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "R3BProblem.hpp"

#include "R3bpProblemException.hpp"

/**
 */
R3bProblem::R3bProblem(double r1, double m1, double r2, double m2) {
    r12 = fabs(r1 + r2);
    a_m = r12;
    a_kg = m1 + m2;
    a_s = sqrt(a_m * a_m * a_m / (m1 + m2));
    mu_ratio = libration_point::computeGravitationalConstantRatio(m1, m2);
    libration_point::computeLibrationPoints(mu_ratio, Lxy, Cj);
    omega = sqrt((m1 + m2) / (r12 * r12 * r12));
    synodic_period = 2 * M_PI / omega;
}

// -------------------------------------------------------------------------
/**
 */
R3bProblem::R3bProblem(double mu_ratio_) : a_m(1), a_s(1), a_kg(1), mu_ratio(mu_ratio_) {
    double r1 =-mu_ratio / (1 + mu_ratio);
    double r2 = 1. / (1 + mu_ratio);
    r12 = fabs(r1 - r2);
    a_m = r12;
    libration_point::computeLibrationPoints(mu_ratio, Lxy, Cj);
    omega = 1;
    synodic_period = 2 * M_PI * sqrt((r12 * r12 * r12));
}

// -------------------------------------------------------------------------
/** Return the position of the primary point */
Vector3 R3bProblem::get_primary_point(PrimaryPointId primaryId) const {
    Vector3 position;
    if (primaryId == P1) {
        position << -mu_ratio, 0, 0;
        return position;
    }
    if (primaryId == P2) {
        position << 1 - mu_ratio, 0, 0;
        return position;
    }
    throw R3bpProblemException("Unknown primary point!");
}
// -------------------------------------------------------------------------
/** Return the coordinates of all libration points, and the jacobi constant values.
 */
void R3bProblem::get_libration_points(double L_[5][2], double Cj_[5]) const {
    memcpy(L_, Lxy, 10 * sizeof(double));
    memcpy(Cj_, Cj, 5 * sizeof(double));
}

// -------------------------------------------------------------------------
/** Return the property of libration point.
 * The output is a structure (@see LibrationPointProperty).
 */
LibrationPointProperty R3bProblem::get_libration_point_info(LibrationPointId iLibrationPoint) const {
    assert(iLibrationPoint < 5);

    LibrationPointProperty prop;
    prop.id = iLibrationPoint;           // libration point
    prop.mu = mu_ratio;          // mass ratio of the problem
    prop.L[0] = Lxy[iLibrationPoint][0];              // coordinate of the libration point
    prop.L[1] = Lxy[iLibrationPoint][1];
    prop.L[2] = 0;
    prop.C = Cj[iLibrationPoint];

    return prop;
}

// -------------------------------------------------------------------------
/** get the binary system barycenter in the given frame */
Vector3 R3bProblem::get_barycenter(double date, Frame *frame) {
    Vector7 pv0 = Vector7::Zero();
    if (frame == NULL) {
        return pv0.head(3);
    }
    Vector7 pv = to_fixed_frame(date, pv0);
    return pv.head(3);
}

// -------------------------------------------------------------------------
/** Compute the (position, velocity) in the fixed frame of the primary using a frame transform */
Vector7 R3bProblem::to_fixed_frame_(double date, const Vector7& state, FrameTransform* transform) const {
    CartesianCoordinates coordinates = CartesianCoordinates(state.head(3), state.segment(3,3));
    CartesianCoordinates coordinates_in_frame = transform->transform(coordinates);
    Vector7 x;
    x << coordinates_in_frame.getPosition(), coordinates_in_frame.getVelocity(), state(6);
    return x;
}
// -------------------------------------------------------------------------
/** Compute the (position, velocity) in the fixed frame of the primary
 * @param date  date
 * @param state (position,velocity) in the R3BP rotating frame
 */
Vector7 R3bProblem::to_fixed_frame(double date, const Vector7& state) const {
    double rcirc = mu_ratio;  // circular orbit around the barycenter
    double mu1 = 1 - mu_ratio;
    double mean_motion = 1;
    double theta = mean_motion * date; // angular position around the barycenter
    Vector3 omg; omg << 0, 0, mean_motion;
    double c_theta = cos(theta);
    double s_theta = sin(theta);
    Vector3 rpos = state.segment(0, 3);
    Vector3 pos_inertial;
    pos_inertial << rcirc * cos(theta) + rpos(0) * c_theta + rpos(1) * s_theta,
                    rcirc * sin(theta) + -rpos(0) * s_theta + rpos(1) * c_theta,
                    rpos(2);
    Vector3 vel_inertial;
    vel_inertial = state.segment(3, 3) + omg.cross(rpos);
    Vector7 s; s << pos_inertial, vel_inertial, 1.;
    return s;
}
// -------------------------------------------------------------------------
std::vector<Vector7> R3bProblem::to_fixed_frame(const std::vector<double>& dates, std::vector<Vector7>& states) const {
    std::vector<Vector7> eci_states;
    int idx = 0;
    for (std::vector<Vector7>::iterator state = states.begin(); state != states.end(); ++state) {
        eci_states.push_back(to_fixed_frame(dates.at(idx), *state));
        ++idx;
    }
    return eci_states;
}
