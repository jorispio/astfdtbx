// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2011 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __ASTTBX_POTENTIAL_HPP__
#define __ASTTBX_POTENTIAL_HPP__

class Potential {
 public:
 
    /* ---------------------------------------------------------------------
     * Gravitational potential, in the rotating frame.
     * For the augmented potentiel, add 0.5 (x * x + y * y).
     * --------------------------------------------------------------------- */
    static real_type getPotential(double mu_ratio, const real_type *xyz) {
        double x = xyz[0];
        double y = xyz[1];

        double r1 = sqrt((x + mu_ratio) * (x + mu_ratio) + y * y);
        double r2 = sqrt((x + mu_ratio - 1) * (x + mu_ratio - 1) + y * y);
        double U =-(1 - mu_ratio) / r1 - mu_ratio / r2 - 0.5 * mu_ratio * (1 - mu_ratio);
        return -0.5 * (x * x + y * y) + U;
    }
    
    /* ---------------------------------------------------------------------
     * First partial derivatives of the potential U;
     * --------------------------------------------------------------------- */
    static Vector3 compute_gravity(double mu_ratio, const Vector3& R) { 
        Vector3 body1_pos, body2_pos;
        body1_pos << -mu_ratio, 0, 0;
        body2_pos << 1 - mu_ratio, 0, 0;    
        double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
        Vector3 R1 =  R - body1_pos;
        Vector3 R2 =  R - body2_pos;
        double rx = R(0), ry = R(1);
            
        double r1 = R1.norm();
        double r13 = r1 * r1 * r1;
        double r2 = R2.norm();
        double r23 = r2 * r2 * r2;
        return (mu1 * (R1 / r13) + mu2 * (R2 / r23)) - Vector3(rx, ry, 0);
    }
    /* ---------------------------------------------------------------------
     * Time derivative of first partial derivatives of the potential U;
     * --------------------------------------------------------------------- */
    static Vector3 compute_time_derivative_gravity(double mu_ratio, const Vector3& R, const Vector3& V) {
        Vector3 body1_pos, body2_pos;
        body1_pos << -mu_ratio, 0, 0;
        body2_pos << 1 - mu_ratio, 0, 0;    
        double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
        Vector3 R1 =  R - body1_pos;
        Vector3 R2 =  R - body2_pos;
        double rx = R(0), ry = R(1);
        double vx = V(0), vy = V(1);    
        double r1 = R1.norm();
        double r13 = r1 * r1 * r1;
        double r15 = r13 * r1 * r1;    
        double r2 = R2.norm();
        double r23 = r2 * r2 * r2;
        double r25 = r23 * r2 * r2;
        return mu1 * (V / r13 - 3 * R1 * R1.dot(V) / r15) + mu2 * (V / r23 - 3 * R2 * R2.dot(V) / r25) - Vector3(vx, vy, 0);
    }    
    
    /* ---------------------------------------------------------------------
     * Second partial derivatives of the potential U;
     * --------------------------------------------------------------------- */
    static Matrix3 compute_gravity_gradient(double mu_ratio, const Vector3& R) { 
        Vector3 body1_pos, body2_pos;
        body1_pos << -mu_ratio, 0, 0;
        body2_pos << 1 - mu_ratio, 0, 0;    
        double mu1 = 1 - mu_ratio, mu2 = mu_ratio;
        
        double r1x = R(0) - body1_pos(0);
        double r1y = R(1) - body1_pos(1);
        double r1z = R(2) - body1_pos(2);        
        double r1 = (R - body1_pos).norm();
        double r12 = r1 * r1;
        double r13 = r12 * r1;
        double r15 = r13 * r1 * r1;
        
        double r2x = R(0) - body2_pos(0);
        double r2y = R(1) - body2_pos(1);
        double r2z = R(2) - body2_pos(2);      
        double r2 = (R - body2_pos).norm();
        double r22 = r2 * r2;
        double r23 = r22 * r2;
        double r25 = r23 * r2 * r2;
        
        // Derivative of dU
        Matrix3 ddUdR;
        ddUdR(0, 0) /*G[0]*/ /*G(1,1)*/ =-1 + mu1 * (r12 - 3 * r1x * r1x) / r15 + mu2 * (r22 - 3 * r2x * r2x) / r25;
        ddUdR(0, 1) /*G[1]*/ /*G(1,2)*/ =    -3 * mu1 * r1x * r1y / r15 - 3 * mu2 * r2x * r2y / r25;
        ddUdR(0, 2) /*G[2]*/ /*G(1,3)*/ =    -3 * mu1 * r1x * r1z / r15 - 3 * mu2 * r2x * r2z / r25;
        ddUdR(1, 0) /*G[3]*/ /*G(2,1)*/ = ddUdR(0, 1);
        ddUdR(1, 1) /*G[4]*/ /*G(2,2)*/ =-1 + mu1 * (r12 - 3 * r1y * r1y) / r15 + mu2 * (r22 - 3 * r2y * r2y) / r25;
        ddUdR(1, 2) /*G[5]*/ /*G(2,3)*/ =    -3 * mu1 * r1y * r1z / r15 - 3 * mu2 * r2y * r2z / r25;
        ddUdR(2, 0) /*G[6]*/ /*G(3,1)*/ = ddUdR(0, 2);
        ddUdR(2, 1) /*G[7]*/ /*G(3,2)*/ = ddUdR(1, 2);
        ddUdR(2, 2) /*G[8]*/ /*G(3,3)*/ =     mu1 * (r12 - 3 * r1z * r1z) / r15 + mu2 * (r22 - 3 * r2z * r2z) / r25;
        
        return ddUdR;
    }

};

#endif // __ASTTBX_POTENTIAL_HPP__
