// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Jacobi.hpp"

#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "SpaceMechanicsToolBox.hpp"

namespace jacobi {
// -------------------------------------------------------------------------
double potential(double mu_ratio, double x, double y, double z) {
    double r1 = sqrt((x + mu_ratio) * (x + mu_ratio) + y * y);
    double r2 = sqrt((x + mu_ratio - 1) * (x + mu_ratio - 1) + y * y);
    double U =-(1 - mu_ratio) / r1 - mu_ratio / r2 - 0.5 * mu_ratio * (1 - mu_ratio);
    return -0.5 * (x * x + y * y) + U;
}

// -------------------------------------------------------------------------
// Jacobi energy
double Jacobi_energy(double mu_ratio, double x, double y, double z, double vx, double vy, double vz) {
    return 0.5 * (vx * vx + vy * vy + vz * vz) + potential(mu_ratio, x, y, z);
}
// -------------------------------------------------------------------------
// Jacobi constant C = -2 E.
double JacobiC(double mu_ratio, double x, double y, double z, double vx, double vy, double vz) {
    return -2.0 * Jacobi_energy(mu_ratio, x, y, z, vx, vy, vz);
}

// -------------------------------------------------------------------------
double JacobiC(double mu_ratio, const Vector3& pos, const Vector3& vel) {
    return JacobiC(mu_ratio, pos(0), pos(1), pos(2), vel(0), vel(1), vel(2));
}
// -------------------------------------------------------------------------
double velocity_magnitude(double mu_ratio, double C, const Vector3& pos) {
    double x = pos(0);
    double y = pos(1);
    double z = pos(2);
    double P = potential(mu_ratio, x, y, z);
    return sqrt(-2.0 * P - C);
}
// -------------------------------------------------------------------------
// gradient of the Jacobi constant
Vector6 JacobiC_gradient(double mu_ratio, const Vector3& pos, const Vector3& vel) {
    double x = pos(0);
    double y = pos(1);
    double z = pos(2);
    double vx = vel(0);
    double vy = vel(1);
    double vz = vel(2);
    
    double rl1 = -mu_ratio;
    double r1 = sqrt((x - rl1) * (x - rl1) + y * y + z * z);
    Vector3 dr1_dp;
    dr1_dp << (x - rl1), y, z;
    dr1_dp /= r1;

    double rl2 = 1 - mu_ratio;
    double r2 = sqrt((x - rl2) * (x - rl2) + y * y + z * z);
    Vector3 dr2_dp;
    dr2_dp << (x - rl2), y,  z;
    dr2_dp /= r2;
    
    Vector3 dC_dp;
    dC_dp = 2. * ((1 - mu_ratio) * dr1_dp / (r1 * r1) + mu_ratio * dr2_dp / (r2 * r2));
    dC_dp(0) += 4 * x;
    dC_dp(1) += 4 * y;
    
    Vector3 dC_dv;
    dC_dv << -2, -2, -2;
    
    Vector6 dCdpv; dCdpv << dC_dp, dC_dv;
    return dCdpv;
}
// -------------------------------------------------------------------------
// gradient of the Jacobi constant
Vector6 JacobiC_gradient(double mu_ratio, const Vector6& posvel) {
    return JacobiC_gradient(mu_ratio, posvel.segment(0,3), posvel.segment(3,3));
}
    
}  // namespace jacobi
