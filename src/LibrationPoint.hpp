/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_LIBRATION_POINT_HPP_
#define __R3BP_LIBRATION_POINT_HPP_

#include <algorithm>

#include "SpaceMechanicsToolBox.hpp"

//
//            L4
// L3   (mu1)     L1  (mu2)  L2
//            L5
//
enum LibrationPointId {
    L1 = 0,
    L2 = 1,
    L3 = 2,
    L4 = 3,
    L5 = 4
};


namespace libration_point {
void computeLibrationPoints(double r1, double r2, double m1, double m2, double Lxy[5][2], double Cj[5]);
void computeLibrationPoints(double mu, double Lxy[5][2], double Cj[5]);

/**
 * @param m1 mass of primary
 * @param m2 mass of secondary
 * with M1 the 'big' primary and 'M2' the small primary.
 */
double computeGravitationalConstantRatio(double m1, double m2);
}  // namespace libration_point


struct LibrationPointProperty {
 private:
    bool is_gamma_computed;
    double __cached_gamma;

 public:
    double mu;          // mass ratio of the problem
    LibrationPointId id;            // libration point
    double L[3];        // coordinates of the libration point, provided by R3bProblem()
    double C;           // energy

    /** Constructor */
    explicit
    LibrationPointProperty(LibrationPointId id = L1, double mu = 0) : mu(mu), id(id) {
        L[0] = 0.; L[1] = 0.; L[2] = 0.;
        C = 0.;
        is_gamma_computed = false;
    }

    /** Constructor */
    LibrationPointProperty(LibrationPointId id, double mu, const double pos[3], double energy = 0)
                : mu(mu), id(id), C(energy) {
        std::copy(pos, pos + 3, std::begin(L));
        is_gamma_computed = false;
    }

    /** Constructor */
    LibrationPointProperty(const LibrationPointProperty &librationPoint) {
        mu = librationPoint.mu;
        id = librationPoint.id;
        std::copy(librationPoint.L, librationPoint.L + 3, std::begin(L));
        C = librationPoint.C;
        is_gamma_computed = false;
    }

    /** Libration point pos-vel state in rotating frame. */
    Vector6 get_state() {
        Vector6 eq;
        eq << L[0], L[1], L[2], 0, 0, 0;
        return eq;
    }

    /** adimensional distance to the closest primary*/
    double gamma();
    /** */
    double gl();
    /** */
    double calc_c2();
};

#endif  // __R3BP_LIBRATION_POINT_HPP_
