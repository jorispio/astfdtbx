// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LISSAJOUS_MULTIPLE_ARCS_PROBLEM_SOLVER_HPP_
#define __R3BP_LISSAJOUS_MULTIPLE_ARCS_PROBLEM_SOLVER_HPP_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "orbit/solvers/ConstraintsType.hpp"
#include "segments/OrbitSegment.hpp"
#include "segments/PatchPoint.hpp"
#include "SpaceMechanicsToolBox.hpp"

class MultipleArcsProblemSolver: public RAPHSON {
 private:
    EllipticThreeBodyProblem *problem;
    StateTransitionMatrixRtbp *propagator;

    double mu_ratio;
    double Ax;
    double phi_x;
    double Az;
    double phi_z;
    int n_segments;

    Vector6 initial_state;
    Vector6 desired_xf;
    std::vector<PatchPoint> patch_points;
    std::vector<PatchPoint> patch_points_out;
    MATRIXVAR matJacobian;

    Vector6 eval_point(const Vector3 &r0, const Vector3 &v0, double dt);
    Vector6 eval_segment(const Vector3 &r0, const Vector3 &v0, double dt, Matrix6 &stm);

    void compute_elementary_jacobian(const Vector6 &xc,
                                     const Vector6 &x_left, const Vector6 &x_right,
                                     const Matrix6 &phi_m, const Matrix6 &phi_p,
                                     MatrixBase<MATRIXVAR> &jacobian);

    void zerof(const VECTORVAR &x, bool with_jacobian,
                /* outputs */
                VECTORVAR &z, MATRIXVAR &J);

 public:
    MultipleArcsProblemSolver(double mu_ratio, uint n_segments);
    ~MultipleArcsProblemSolver();

    void set_terminal_state(const Vector6 &desired_xf);
    void set_states(const std::vector<Segment> &segments);
    void set_states(const std::vector<PatchPoint> &xi);

    int get_n();
    int get_m();

    std::vector<PatchPoint> get_patch_points();

    inline Vector6 get_terminal_state() const { return desired_xf; }

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    VECTORVAR valuefun(const VECTORVAR& x, bool& iret) override;
    int jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) override;

    void zerof(const VECTORVAR& x, /* outputs */ VECTORVAR &z);
    void zerof(const VECTORVAR& x,
            /* outputs */
            VECTORVAR &z, MATRIXVAR &J);

    /** */
    VECTORVAR compute_update(const VECTORVAR& x);
    double compute_cost(const VECTORVAR& zDeltaV);
};

#endif  // __R3BP_LISSAJOUS_MULTIPLE_ARCS_PROBLEM_SOLVER_HPP_
