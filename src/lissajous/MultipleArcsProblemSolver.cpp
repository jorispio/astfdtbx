// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
// Reference:
//  K.C. Howell, H.J. Pernicka, Numerical Determination of Lissajous Trajectories in the Restricted Three-Body Problem,
//      Celestial mechanics, Vol.41, No.1, pp.107--124, Mar 1987, 10.1007/BF01238756
//
#include "MultipleArcsProblemSolver.hpp"

#include "R3bpLissajousSolverException.hpp"

// #define DEBUG_MULTI_ARCS

// ------------------------------------------------------------------------
MultipleArcsProblemSolver::MultipleArcsProblemSolver(double mu_ratio_, uint n_segments)
                                                    : RAPHSON(),
                                                    mu_ratio(mu_ratio_), n_segments(n_segments) {
    if (n_segments < 2) {
        throw R3bpLissajousSolverException("Not enough segment. 2 segments minimum are required!\n");
    }

    problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    matJacobian = MATRIXVAR::Zero(get_m(), get_n());

    initial_state = Vector6::Zero();
    desired_xf = Vector6::Zero();
}
// ------------------------------------------------------------------------
MultipleArcsProblemSolver::~MultipleArcsProblemSolver() {
    if (problem != NULL) {
        delete problem;
    }
    if (propagator != NULL) {
        delete propagator;
    }
}

// ------------------------------------------------------------------------
void MultipleArcsProblemSolver::set_terminal_state(const Vector6 &desired_xf_) {
    desired_xf = desired_xf_;
}
// ------------------------------------------------------------------------
int MultipleArcsProblemSolver::get_m() {
    return 3 * (n_segments - 1);
}
// ------------------------------------------------------------------------
int MultipleArcsProblemSolver::get_n() {
    return 4 * (n_segments + 1);
}
// ------------------------------------------------------------------------
// ------------------------------------------------------------------------
void MultipleArcsProblemSolver::set_states(const std::vector<PatchPoint> &patch_points_) {
    patch_points = patch_points_;
}
// ------------------------------------------------------------------------
void MultipleArcsProblemSolver::set_states(const std::vector<Segment> &segments) {
    patch_points.clear();
    Segment segment = segments.at(0);
    patch_points.push_back(PatchPoint(0, segment.ri, DeltaV(segment.vi, segment.vi)));

    for (uint idx = 0; idx < n_segments - 1; ++idx) {
        patch_points.push_back(PatchPoint(segments.at(idx), segments.at(idx + 1)));
    }
    segment = segments.at(n_segments - 1);
    patch_points.push_back(PatchPoint(segment.tf, segment.rf, DeltaV(segment.vf, segment.vf)));

    patch_points_out = patch_points;
}
// ------------------------------------------------------------------------
std::vector<PatchPoint> MultipleArcsProblemSolver::get_patch_points() {
    return patch_points_out;
}
// ------------------------------------------------------------------------
/** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
*/
// ------------------------------------------------------------------------
VECTORVAR MultipleArcsProblemSolver::valuefun(const VECTORVAR& x, bool& iret) {
    iret = false;
    VECTORVAR z = VECTORVAR::Zero(get_m());
    zerof(x, false, z, matJacobian);
    return z;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 */
// ------------------------------------------------------------------------
int MultipleArcsProblemSolver::jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) {
    VECTORVAR z = VECTORVAR::Zero(get_m());
    zerof(x, true, z, matJacobian);
    Fjac = matJacobian;
    return 0;
}
// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the positions.
 *
 * Jacobian of valuefun() for the zero-finding solver.
 * The zero vectorial function is the one that nullify all delta-vs.
 * There is a dv (velocity increment) at each patch point. The dv is dependant
 * on the state of the patch points immediately before and after the current
 * path point.
 *
 */
void MultipleArcsProblemSolver::zerof(const VECTORVAR &x,
                                    /* outputs */
                                    VECTORVAR &z) {
    MATRIXVAR jacobian;
    zerof(x, false, z, jacobian);
}
// ------------------------------------------------------------------------
void MultipleArcsProblemSolver::zerof(const VECTORVAR &x,
                                    /* outputs */
                                    VECTORVAR &z, MATRIXVAR &jacobian) {
    zerof(x, true, z, jacobian);
}
// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the positions.
 *
 * Jacobian of valuefun() for the zero-finding solver.
 * The zero vectorial function is the one that nullify all delta-vs.
 * There is a dv (velocity increment) at each patch point. The dv is dependant
 * on the state of the patch points immediately before and after the current
 * path point.
 *
 */
void MultipleArcsProblemSolver::zerof(const VECTORVAR &x,
                                    bool with_jacobian,
                                    /* outputs */
                                    VECTORVAR &z, MATRIXVAR &jacobian) {
    assert(patch_points.size() == n_segments + 1);
    assert(jacobian.rows() == z.rows());
    assert(jacobian.cols() == x.rows());
#ifdef DEBUG_MULTI_ARCS
    printf("n_segment = %d\n", n_segments);
    printf("patch_points.size() = %d\n", patch_points.size());
    printf("x.rows = %d\n", x.rows());
    printf("z.rows = %d\n", z.rows());
    printf("jacobian.rows = %d\n", jacobian.rows(), jacobian.cols());
#endif

    //patch_points_out = patch_points;

    for (auto i_segment = 0; i_segment < n_segments - 1; ++i_segment) {
        // unknowns are intermediate positions vectors and times that nullify delta-vs
        // forward propagation
        double dt0 = x(i_segment * 4 + 3);
        Vector3 r0 = x.segment(i_segment * 4, 3);
        Vector3 v0 = patch_points.at(i_segment).deltaV.Vp;
        double tof1 = patch_points.at(i_segment + 1).t - patch_points.at(i_segment).t;
        double dti = x((i_segment + 1) * 4 + 3);
        double dtof1 = dti - dt0;
        Matrix6 phi_m;
        Vector6 xi_m = eval_segment(r0, v0, tof1 + dtof1, phi_m);

        // backward propagation
        double dt2 = x((i_segment + 2) * 4 + 3);
        Vector3 rf = x.segment((i_segment + 2) * 4, 3);
        Vector3 vf = patch_points.at(i_segment + 2).deltaV.Vm;
        double tof2 = patch_points.at(i_segment + 2).t - patch_points.at(i_segment + 1).t;
        double dtof2 = dt2 - dti;
        Matrix6 phi_p;
        Vector6 xi_p = eval_segment(rf, vf, -(tof2 + dtof2), phi_p);  // backwards

        // eval the mid-point state (-)
        Vector3 ri = x.segment((i_segment + 1) * 4, 3);
        Vector3 vim = patch_points.at(i_segment + 1).deltaV.Vm;
        Vector6 xcm = eval_point(ri, vim, dt2);

        // eval the mid-point state (+)
        Vector3 vip = patch_points.at(i_segment + 1).deltaV.Vp;
        Vector6 xcp = eval_point(ri, vip, dti);

        Vector6 z_nom = (xi_p - xi_m).segment(3, 3);  // velocity part only
        //Vector3 dv_new = 0*(xcp - xcm).segment(3, 3);
        z.segment(i_segment * 3, 3) = z_nom;

        // construct list of patch points for post-processing
        patch_points_out.at(i_segment).t = patch_points.at(i_segment).t + dt0;
        patch_points_out.at(i_segment).rm = r0;
        patch_points_out.at(i_segment).rp = r0;
        patch_points_out.at(i_segment + 1).t = patch_points.at(i_segment + 1).t + dti;
        patch_points_out.at(i_segment + 1).rm = ri; //xi_m.segment(0, 3);
        patch_points_out.at(i_segment + 1).deltaV.Vm = xi_m.segment(3, 3);
        patch_points_out.at(i_segment + 1).rp = ri; //xi_p.segment(0, 3);
        patch_points_out.at(i_segment + 1).deltaV.Vp = xi_p.segment(3, 3);
        patch_points_out.at(i_segment + 2).t = patch_points.at(i_segment + 2).t + dt2;
        patch_points_out.at(i_segment + 2).rm = rf;
        patch_points_out.at(i_segment + 2).rp = rf;

        if (with_jacobian) {
            // compute the State Relationship matrix
            MATRIXVAR jac_i = MATRIXVAR::Zero(3, 4 * 3);
            int i = 3 * i_segment;
            int j = 4 * i_segment;
            compute_elementary_jacobian(xcm,
                                        xi_m, xi_p,
                                        phi_m, phi_p, jac_i);  // FIXME(jo) [Improve] avoid unecessary copy of the matrix
            jacobian.block<3, 4 * 3>(i, j) = jac_i;
        }


#ifdef DEBUG_MULTI_ARCS
        printf("Segment %d\n", i_segment);
        std::cout << "x=" << x.transpose() << std::endl;
        std::cout << " r0 = " << r0.transpose() << "; v0 = " << v0.transpose() << std::endl;
        std::cout << " -> r(t-)=" << (xi1).segment(0, 3).transpose() << std::endl;
        std::cout << " -> v(t-)=" << (xi1).segment(3, 3).transpose() << std::endl;
        std::cout << " rf = " << rf.transpose() << "; vf = " << vf.transpose() << std::endl;
        std::cout << " -> r(t+)=" << (xi2).segment(0, 3).transpose() << std::endl;
        std::cout << " -> v(t+)=" << (xi2).segment(3, 3).transpose() << std::endl;
        std::cout << " dv=" << dv.transpose() << " (current optimisation)" << std::endl;
        std::cout << " dv=" << (vip - vim).transpose() << " (former patch point)" << std::endl;
        std::cout << " ri=" << ri.transpose() << std::endl;
        std::cout << "---------" << std::endl;
#endif
    }  // end for
}
// ------------------------------------------------------------------------
Vector6 MultipleArcsProblemSolver::eval_segment(const Vector3 &r0, const Vector3 &v0, double dt, Matrix6 &stm) {
#ifdef DEBUG_MULTI_ARCS
    printf("eval_segment r0 = [%8f %8f %8f] v0 = [%8f %8f %8f]  dt=%g\n", r0(0), r0(1), r0(2), v0(0), v0(1), v0(2), dt);
#endif

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
            0, dt,
            STM1, STM2, STM3, STM4,
            STM,
            P1, P2, P3,
            rf, vf);
    stm = STM.block(0, 0, 6, 6);
    Vector6 xi; xi << rf, vf;

    return xi;
}
// ------------------------------------------------------------------------
Vector6 MultipleArcsProblemSolver::eval_point(const Vector3 &r0, const Vector3 &v0, double dt) {
    Vector6 xf;
    xf << r0, v0, 0;
    CommonVariables cV;
    real_type af[7];
    const real_type x_array[7] = {r0(0), r0(1), r0(2), v0(0), v0(1), v0(2), 0};
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array, cV)) {
        problem->GetStateDynamics(0., cV, af);
        xf << r0(0) + af[0] * dt, r0(1) + af[1] * dt, r0(2) + af[2] * dt,
              v0(0) + af[3] * dt, v0(1) + af[4] * dt, v0(2) + af[5] * dt;
    }
    return xf;
}

// ------------------------------------------------------------------------
/**
 * Block Jacobian of valuefun() for the zero-finding solver.
 */
void MultipleArcsProblemSolver::compute_elementary_jacobian(
                                        const Vector6 &xc,
                                        const Vector6 &x_left, const Vector6 &x_right,
                                        const Matrix6 &phi_m, const Matrix6 &phi_p,
                                        MatrixBase<MATRIXVAR> &J) {
    CommonVariables cV;
    real_type afm[7], afp[7];
    const real_type x_array_m[7] = {x_left(0), x_left(1), x_left(2), x_left(3), x_left(4), x_left(5), 0};
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array_m, cV)) {
        problem->GetStateDynamics(0., cV, afm);
    }

    const real_type x_array_p[7] = {x_right(0), x_right(1), x_right(2), x_right(3), x_right(4), x_right(5), 0};
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array_p, cV)) {
        problem->GetStateDynamics(0., cV, afp);
    }

    Vector3 Vmdoti; Vmdoti << afm[3], afm[4], afm[5];
    Vector3 Vpdoti; Vpdoti << afp[3], afp[4], afp[5];

    // express the relationship between V(ti-) and the previous patch point state
    Matrix3 Am = phi_m.block(0, 0, 3, 3);  // dR(ti)/dR(t0)
    Matrix3 Bm = phi_m.block(0, 3, 3, 3);  // dR(ti)/dV(t0)
    Matrix3 Cm = phi_m.block(3, 0, 3, 3);  // dV(ti)/dR(t0)
    Matrix3 Dm = phi_m.block(3, 3, 3, 3);  // dV(ti)/dV(t0)
    Matrix3 DmiBm = Dm * Bm.inverse();
    Matrix3 dVim_dRm1 = 0*DmiBm * Am - Cm;
    Vector3 dVim_dtm1 = Vmdoti - 0*DmiBm * x_left.segment(3, 3);
    Matrix3 dVim_dR = DmiBm;  // dV(ti)/dR(ti)
    Vector3 dVim_dt = Vmdoti + 0*DmiBm * x_left.segment(3, 3);

    // express the relationship between V(ti+) and the next patch point state
    Matrix3 Ap = phi_p.block(0, 0, 3, 3);  // dR(ti)/dR(tf)
    Matrix3 Bp = phi_p.block(0, 3, 3, 3);  // dV(ti)/dR(tf)
    Matrix3 Cp = phi_p.block(3, 0, 3, 3);  // dV(ti)/dR(t0)
    Matrix3 Dp = phi_p.block(3, 3, 3, 3);  // dV(ti)/dV(t0)
    Matrix3 DpiBp = Dp * Bp.inverse();
    Matrix3 dVip_dRp1 = Cp - 0*DpiBp * Ap;
    Vector3 dVip_dtp1 =-0*DpiBp * x_right.segment(3, 3) - Vpdoti;
    Matrix3 dVip_dR = DpiBp;
    Vector3 dVip_dt = Vpdoti + 0*DpiBp * x_right.segment(3, 3);

    // state as: x = [R_im1, t_im1, R_i, t_i, R_ip1, t_ip1]
    // derivation of d(dV) / dx = d(V(ti+) - V(ti-)) / dx
    J.block<3, 3>(0, 0) = dVim_dRm1;
    J.block<3, 1>(0, 3) = dVim_dtm1;
    J.block<3, 3>(0, 4) = (dVip_dR - dVim_dR) * 0;
    J.block<3, 1>(0, 7) = dVip_dt - dVim_dt;
    J.block<3, 3>(0, 8) = dVip_dRp1;
    J.block<3, 1>(0, 11) = dVip_dtp1;
#ifdef DEBUG_MULTI_ARCS
    std::cout << dVip_dR << std::endl;
    std::cout << dVim_dR << std::endl;
    std::cout << "-----" << std::endl;
#endif
}
// ------------------------------------------------------------------------
/** Compute decision vector update */
VECTORVAR MultipleArcsProblemSolver::compute_update(const VECTORVAR& x) {
    VECTORVAR zDeltaV = VECTORVAR::Zero(get_m());
    MATRIXVAR Fjac = MATRIXVAR::Zero(get_m(), get_n());
    zerof(x, zDeltaV, Fjac);
    patch_points_out = patch_points;
    return Fjac.colPivHouseholderQr().solve(-zDeltaV);
}
// ------------------------------------------------------------------------
double MultipleArcsProblemSolver::compute_cost(const VECTORVAR& zDeltaV) {
    double dv = 0;
    for (auto idx = 0; idx < n_segments - 1; ++idx) {
        dv += zDeltaV.segment(3 * idx, 3).norm();
    }
    return dv;
}
