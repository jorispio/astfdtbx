// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LISSAJOUS_PROBLEM_SOLVER_HPP_
#define __R3BP_LISSAJOUS_PROBLEM_SOLVER_HPP_

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "orbit/solvers/ConstraintsType.hpp"
#include "solver/DifferentialCorrector.hpp"
#include "SpaceMechanicsToolBox.hpp"

class LissajousProblemSolver: public DifferentialCorrector {
 private:
    EllipticThreeBodyProblem *problem;
    StateTransitionMatrixRtbp *propagator;

    double mu_ratio;
    double Ax;
    double phi_x;
    double Az;
    double phi_z;
    int n_segments;

    ConstraintType constraint_type;
    Vector6 initial_state;
    Vector6 desired_xf;
    MATRIXVAR matJacobian;

 public:
    LissajousProblemSolver(double mu_ratio, double Ax, double phi_x, double Az, double phi_z, uint n_segments);
    ~LissajousProblemSolver();

    uint get_n() {
        return 3;
    }

    uint get_m() {
        if (constraint_type == C_POSITION) {
            return 3;
        }
        else if (constraint_type == C_POSITION_VELOCITY) {
            return 6;
        }
        return 0;
    }

    void set_initial_state(const Vector6 &initial_state);
    void set_terminal_state(ConstraintType constraint_type, const Vector6 &desired_xf);

    inline Vector6 get_terminal_state() const { return desired_xf; }

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    bool valuefun(const VECTORVAR& x, VECTORVAR& f) override;

    int jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) override;

    void zerof(const VECTORVAR &x,
            /* outputs */
            VECTORVAR &z, VECTORVAR &xf, MATRIXVAR &J);
};

#endif /* __R3BP_LISSAJOUS_PROBLEM_SOLVER_HPP_ */
