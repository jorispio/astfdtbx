// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LissajousProblemSolver.hpp"

#include "lissajous/R3bpLissajousSolverException.hpp"

// ------------------------------------------------------------------------
LissajousProblemSolver::LissajousProblemSolver(double mu_ratio_,
                                                double Ax, double phi_x, double Az, double phi_z,
                                                uint n_segments)
    : DifferentialCorrector(),
    mu_ratio(mu_ratio_), Ax(Ax), phi_x(phi_x), Az(Az), phi_z(phi_z), n_segments(n_segments) {
    problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    matJacobian = MATRIXVAR::Zero(3, 4);

    constraint_type = C_POSITION;
    initial_state = Vector6::Zero();
    desired_xf = Vector6::Zero();
}
// ------------------------------------------------------------------------
LissajousProblemSolver::~LissajousProblemSolver() {
    if (problem != NULL) {
        delete problem;
    }
    if (propagator != NULL) {
        delete propagator;
    }
}

// ------------------------------------------------------------------------
void LissajousProblemSolver::set_initial_state(const Vector6 &initial_state_) {
    initial_state = initial_state_;
}
// ------------------------------------------------------------------------
void LissajousProblemSolver::set_terminal_state(ConstraintType constraint_type_, const Vector6 &desired_xf_) {
    constraint_type = constraint_type_;
    desired_xf = desired_xf_;
}
// ------------------------------------------------------------------------
/** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
*/
// ------------------------------------------------------------------------
bool LissajousProblemSolver::valuefun(const VECTORVAR& x, VECTORVAR& f) {
    VECTORVAR xf = VECTORVAR::Zero(6);
    zerof(x, f, xf, matJacobian);
    return true;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 */
// ------------------------------------------------------------------------
int LissajousProblemSolver::jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) {
    VECTORVAR z = VECTORVAR::Zero(4);
    VECTORVAR xf = VECTORVAR::Zero(6);
    zerof(x, z, xf, matJacobian);
    Fjac = matJacobian;
    return 0;
}

// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the inital point (y,z)
 * The periodic Halo orbit should be symmetric with the x-z plane. Thus,
 * considering only half the orbit, the desired terminal conditions are of
 * the form
 *     [xd, 0, zd, 0, vyd, 0]
 * The same applies for the initial conditions then.
 * A is a parameter giving the size of the Halo.
 *
 */
void LissajousProblemSolver::zerof(const VECTORVAR &x,
        /* outputs */
        VECTORVAR &z, VECTORVAR &xf, MATRIXVAR &J) {
    Vector3 r0, v0;
    // conditions to intersect the x-z plane orthogonally after half an orbit
    if (constraint_type == C_POSITION) {
        r0 = initial_state.segment(0, 3);
        v0 = x.segment(0, 3);
    }
    else if (constraint_type == C_POSITION_VELOCITY) {
        r0 = x.segment(0, 3);
        v0 = initial_state.segment(3, 3);
    }
    else {
     throw R3bpLissajousSolverException("Unknown constraint");
    }

    double dt =  x(3);

    Matrix6 phix;
    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, dt,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    phix = STM.block(0, 0, 6, 6);
    xf << rf(0), rf(1), rf(2), vf(0), vf(1), vf(2);

    CommonVariables cV;
    real_type af[7];
    const real_type x_array[7] = {rf(0), rf(1), rf(2), vf(0), vf(1), vf(2), 0};
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array, cV)) {
        problem->GetStateDynamics(dt, cV, af);
    }

    if (constraint_type == C_POSITION) {
        z = rf - desired_xf.segment(0, 3);
        J = phix.block<3,3>(0, 3);
    }
    else if (constraint_type == C_POSITION_VELOCITY) {
        z = xf - desired_xf;
        J = phix;
    }
    else {
        throw R3bpLissajousSolverException("Unknown constraint");
    }
}
