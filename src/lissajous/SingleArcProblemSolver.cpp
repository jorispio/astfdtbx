// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SingleArcProblemSolver.hpp"

#include "R3bpLissajousSolverException.hpp"

// ------------------------------------------------------------------------
SingleArcProblemSolver::SingleArcProblemSolver(double mu_ratio) : RAPHSON() {
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    matJacobian = MATRIXVAR::Zero(3, 3);
    constraint_type = C_POSITION;
    initial_state = Vector6::Zero();
    desired_xf = Vector6::Zero();
}
// ------------------------------------------------------------------------
SingleArcProblemSolver::~SingleArcProblemSolver() {
    if (propagator != NULL) {
        delete propagator;
    }
}

// ------------------------------------------------------------------------
void SingleArcProblemSolver::set_initial_state(const Vector6 &initial_state_) {
    initial_state = initial_state_;
}
// ------------------------------------------------------------------------
void SingleArcProblemSolver::set_terminal_state(ConstraintType constraint_type_, const Vector6 &desired_xf_) {
    constraint_type = constraint_type_;
    desired_xf = desired_xf_;
}
// ------------------------------------------------------------------------
void SingleArcProblemSolver::set_duration(double duration_) {
    duration = duration_;
}
// ------------------------------------------------------------------------
/** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
*/
VECTORVAR SingleArcProblemSolver::valuefun(const VECTORVAR& x, bool& iret) {
    iret = false;
    VECTORVAR z = VECTORVAR::Zero(3);
    VECTORVAR xf = VECTORVAR::Zero(6);
    zerof(x, z, xf, matJacobian);
    return z;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 */
int SingleArcProblemSolver::jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) {
    VECTORVAR z = VECTORVAR::Zero(3);
    VECTORVAR xf = VECTORVAR::Zero(6);
    zerof(x, z, xf, matJacobian);
    Fjac = matJacobian;
    return 0;
}

// ------------------------------------------------------------------------
/**
 *
 */
void SingleArcProblemSolver::zerof(const VECTORVAR &x,
                                    /* outputs */
                                    VECTORVAR &z, VECTORVAR &xf, MATRIXVAR &J) {
    Vector3 r0, v0;
    if (constraint_type == C_POSITION) {
        r0 = initial_state.segment(0, 3);
        v0 = x.segment(0, 3);
    }
    else if (constraint_type == C_POSITION_VELOCITY) {
        r0 = x.segment(0, 3);
        v0 = x.segment(3, 3);
    }
    else {
        throw R3bpLissajousSolverException("Unknown constraint");
    }

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, duration,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    Matrix6 phix = STM.block(0, 0, 6, 6);
    xf << rf, vf;

    if (constraint_type == C_POSITION) {
        z = rf - desired_xf.segment(0, 3);
        J = phix.block(0, 3, 3, 3);
    }
    else if (constraint_type == C_POSITION_VELOCITY) {
        z = xf - desired_xf;
        J = phix;
    }
    else {
        throw R3bpLissajousSolverException("Unknown constraint");
    }
}
// ------------------------------------------------------------------------
