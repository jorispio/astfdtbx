// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LISSAJOUS_HPP_
#define __R3BP_LISSAJOUS_HPP_

#include <math.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "Jacobi.hpp"
#include "lissajous/EndPointsConstraint.hpp"
#include "lissajous/LissajousProblemSolver.hpp"
#include "lissajous/MultipleArcsProblemSolver.hpp"
#include "lissajous/SingleArcProblemSolver.hpp"
#include "orbit/OrbitSolution.hpp"
#include "orbit/R3bpCollinearPointOrbit.hpp"
#include "segments/OrbitSegment.hpp"
#include "SpaceMechanicsToolBox.hpp"

/**
 * Lissajous orbits, usually around L1,2,3, are the results of linearisation of
 * the dynamical equations around one of the Libration point.
 * They do not however exactly orbit the Libration points. They are the
 * general period solution of the R3BP
 */
class Lissajous {
 private:
    LibrationPointProperty librationPoint;
    OrbitSolution orbitSolution;

 protected:
    SingleArcProblemSolver *single_arc_solver;
    MultipleArcsProblemSolver *multiple_arc_solver;
    EndPointsConstraint endpoints_constraint;
    static const char* msg[4];

    double solve_first_pass(const std::vector<Segment> &segments, std::vector<Segment> &xs,
            bool print_iteration = true,
            int max_iterations = 100,
            double rtol = 1e-10);

    void get_scnd_pass_decision_vector(const std::vector<Segment> &segments, VECTORVAR &x);
    double solve_scnd_pass(const std::vector<Segment> &segments, std::vector<Segment> &x_solution,
            bool print_iteration = true);

    void print_patch_data(uint n_segments, const std::vector<Segment> &x_solution);
    void print_patch_update(uint n_segments, const std::vector<Segment> &x_before, const std::vector<Segment> &x_after);

    void conclude(const Vector6 &xs, double period, const Matrix6 &phi);

 public:
    explicit Lissajous(const LibrationPointProperty& property) : librationPoint(property) { }
    ~Lissajous() { }

    std::vector<Segment> get_analytical_segments(R3bpOrbitClass *orbit, uint n_segments);

    std::vector<Segment> get_segments_from_solution(const OrbitSolution& orbit, uint n_segments);

    void set_end_points_constraint(const EndPointsConstraint &constraint);

    /** Optimise the orbit segments accordingly with segment patching constraints. */
    std::vector<Segment> find(
            std::vector<Segment> &x_solution,
            /* options */
            bool print_iteration = false,
            uint max_iterations = 100,
            double atol = 1e-10, double rtol = 1e-6);

    /** Return an orbit solution structure so that initial condition can be propagated to the solution orbit */
    OrbitSolution get_orbit_property() { return orbitSolution; }
};

// ------------------------------------------------------------------------
#endif  // __R3BP_LISSAJOUS_HPP_
