// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LISSAJOUS_SINGLE_ARC_PROBLEM_SOLVER_HPP
#define __R3BP_LISSAJOUS_SINGLE_ARC_PROBLEM_SOLVER_HPP

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include "orbit/solvers/ConstraintsType.hpp"
#include "SpaceMechanicsToolBox.hpp"

class SingleArcProblemSolver: public RAPHSON {
 private:
    StateTransitionMatrixRtbp *propagator;
    double duration;
    ConstraintType constraint_type;
    Vector6 initial_state;
    Vector6 desired_xf;
    MATRIXVAR matJacobian;

 public:
    SingleArcProblemSolver(double mu_ratio);
    ~SingleArcProblemSolver();

    void set_initial_state(const Vector6 &initial_state);
    void set_terminal_state(ConstraintType constraint_type, const Vector6 &desired_xf);

    inline Vector6 get_terminal_state() const { return desired_xf; }

    void set_duration(double duration);

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    VECTORVAR valuefun(const VECTORVAR& x, bool& iret) override;

    int jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) override;

    void zerof(const VECTORVAR &x,
            /* outputs */
            VECTORVAR &z, VECTORVAR &xf, MATRIXVAR &J);
};

#endif  // __R3BP_LISSAJOUS_SINGLE_ARC_PROBLEM_SOLVER_HPP
