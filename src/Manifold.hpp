// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_MANIFOLD_HPP_
#define __R3BP_MANIFOLD_HPP_

#include <stdlib.h>

#include <cmath>
#include <complex>
#include <string>
#include <vector>

#include "orbit/OrbitSolution.hpp"
#include "SpaceMechanicsToolBox.hpp"

class EigenSpace {
 private:
    OrbitSolution orbitSolution;
    VectorXcd removeInfinitesimals(VectorXcd &A);
    MatrixXcd removeInfinitesimals(MatrixXcd &A);

 public:
    EigenSpace() {}
    explicit EigenSpace(const OrbitSolution& orbitSolution) : orbitSolution(orbitSolution) { }

    ~EigenSpace() {}

    bool getManifolds(const Vector6 &ep, double mu_ratio,
                    /* outputs */
                    std::vector<std::complex<double> > &sn, std::vector<VectorXcd> &Ws,
                    std::vector<std::complex<double> > &un, std::vector<VectorXcd> &Wu,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &Wc);

    bool getManifolds(const Matrix6 &A, bool isContinuous,
                    std::vector<std::complex<double> > &sn, std::vector<VectorXcd> &Ws,
                    std::vector<std::complex<double> > &un, std::vector<VectorXcd> &Wu,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &Wc,
                    double eps = 1e-3);

    bool getManifolds(const OrbitSolution& lissajous,
                    std::vector<std::complex<double> > &sn, std::vector<VectorXcd> &Ws,
                    std::vector<std::complex<double> > &un, std::vector<VectorXcd> &Wu,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &Wc,
                    double eps = 1e-3);

    bool getCenterManifolds(const Vector6 &ep, double mu_ratio,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center);

    bool getCenterManifolds(const Matrix6 &A, bool isContinuous,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center,
                    double eps = 1e-3);

    bool getCenterManifolds(const OrbitSolution& lissajous,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center,
                    double eps = 1e-3);

    std::vector<Vector7> computeDirections(const double x[7], double ti, double timeStep, double mu_ratio,
            const Vector6 &Ws, double displacement,
            /* outputs */
            std::vector<Vector6> &directions);
};

// ------------------------------------------------------------------------
#endif  // __R3BP_MANIFOLD_HPP_
