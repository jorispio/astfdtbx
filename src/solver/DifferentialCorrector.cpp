// $Id: DifferentialCorrector.cpp 132 2015-12-29 20:57:47Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   SpaceMechanicsToolBox is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   SpaceMechanicsToolBox is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SpaceMechanicsToolBox.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "DifferentialCorrector.hpp"

#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <string>

#include "DifferentialCorrectorException.hpp"

#define formatstrFirstIter " %5d   %13.6g\n"
#define formatstr " %5d   %13.6g  %13.6g   %12.3g  %s\n"
#define header  "\n Iteration     f(x)          step          optimality\n"


#define FINITE_STEP  1e-7

const char* DifferentialCorrector::msg[] = {
    "Optimization terminated: first-order optimality less than 'TolFun'\n",
    "Optimization terminated: relative function value changing by less than TolFun.\n",
    "Optimization terminated: norm of the current step is less than 'TolX'.\n",
    "Maximum number of iterations exceeded;\n increase 'MaxIter'\n",
    "Maximum number of function evaluations exceeded;\n increase 'MaxIter'\n",
    "An error occurred during the step.\n"
};


// --------------------------------------------------------------------------
void DifferentialCorrector::set_variable_minimum(const VECTORVAR& vmin) {
    vminimum = vmin.array();
}
// --------------------------------------------------------------------------
ArrayXd DifferentialCorrector::get_variable_minimum() {
    return vminimum;
}
// --------------------------------------------------------------------------
void DifferentialCorrector::set_variable_maximum(const VECTORVAR& vmax) {
    vmaximum = vmax.array();
}
// --------------------------------------------------------------------------
ArrayXd DifferentialCorrector::get_variable_maximum() {
    return vmaximum;
}
// --------------------------------------------------------------------------
void DifferentialCorrector::set_maximum_step(const VECTORVAR& max_step_) {
    max_step = max_step_.array();
}
// --------------------------------------------------------------------------
ArrayXd DifferentialCorrector::get_maximum_step() {
    return max_step;
}
// --------------------------------------------------------------------------
/**
 *
 */
int DifferentialCorrector::solve(const VECTORVAR& x0, VECTORVAR& sol, bool verbose,
                                int maxiter, double abstol, double xtol, int maxfunevals, double alphaInit) {
    int funevals = 0;
    int ex = 0;

    int n = get_n();
    if ((vminimum.size() == n) && (vmaximum.size() == n)) {
        for (int i = 0; i < n; ++i) {
            if (vmaximum(i) < vminimum(i)) {
        char str_cause[128];
                snprintf(str_cause, sizeof(str_cause), "at vector index=%d, vmin = %f but vmax = %f\n", i, vmaximum(i), vminimum(i));
                throw DifferentialCorrectorException("Differential correction exception: Invalid variable bounds!",
                            std::string(str_cause));
            }
        }
    }

    if (max_step.size() == n) {
        for (int i = 0; i < n; ++i) {
            if (max_step(i) <= 0) {
                throw DifferentialCorrectorException("Differential correction exception: Invalid variable maximum step!",
                        "Maximum step shall be > 0");
            }
        }
    }

    if (x0.rows() != n) {
        char str_cause[128];
        snprintf(str_cause, sizeof(str_cause), "Variable size is %d, expected %d\n", x0.rows(), n);
        throw DifferentialCorrectorException("Differential correction exception: Invalid variable size!",
                            std::string(str_cause));
    }

    sol = x0;
    VECTORVAR x = x0;
    int m = get_m();
    VECTORVAR f_value = VECTORVAR::Zero(m);
    bool iret = valuefun(x, f_value);
    if (!iret) {
        ex = 6;
        if (verbose) {
            printf("%s", msg[ex - 1]);
        }
        return 9;
    }
    double val = f_value.norm();
    double val_old = val;

    MATRIXVAR f_jac = MATRIXVAR::Zero(m, n);
    jacobian(x, f_jac);

    int exitflag = 0;
    int iter = 0;
    double alpha = alphaInit;

    // print header and first iteration
    if (verbose) {
        printf("%s", header);
        printf(formatstrFirstIter, iter, val);
    }

    if (iter >= maxiter) {
        ex = 4;
        exitflag = 0;
        return exitflag;
    }

    VECTORVAR f_newval = VECTORVAR::Zero(f_value.rows());
    std::string flag;
    while (true) {
        flag = "";
        VECTORVAR xold = x;
        jacobian(x, f_jac);
        funevals += 1;

        VECTORVAR dx;
        if (n == m) {
            dx = f_jac.lu().solve(f_value);
        } else {
            // pseudo-inverse (More-Penrose)
            // see https://eigen.tuxfamily.org/dox/group__TutorialLinearAlgebra.html
            // dx = (f_jac.transpose() * f_jac).inverse() * f_jac.transpose() * f_value;
            dx = f_jac.block(0, 0, m, n).colPivHouseholderQr().solve(f_value);
        }

        alpha = alphaInit;
        if (max_step.size() == n) {
            if ((dx.array().abs() > max_step).any()) {
                alpha = (max_step / dx.array().abs()).array().minCoeff();
                flag = "s";
            }
        }
        VECTORVAR xnew = x - alpha * dx;

        if ((vminimum.size() == n) && (vmaximum.size() == n)) {
            VECTORVAR xupd = (xnew.array().sign() * vminimum.max(vmaximum.min(xnew.array()))).matrix();
            flag += ((xnew - xupd).lpNorm<Infinity>() > 0) ? "b" : "";
            xnew = xupd;
        }

        // gradient norm
        double optnrm = f_jac.norm();

        // norm of step
        double nrmsx = dx.norm();

        // current error
        bool iret = valuefun(xnew, f_newval);
        if (!iret) {
            ex = 6;
            exitflag = 9;
            break;
        }

        double newval = f_value.lpNorm<Infinity>();
        val = newval;
        f_value = f_newval;
        x = xnew;

        // increment iteration counter
        ++iter;

        if (verbose) {
            printf(formatstr, iter, val, alpha, optnrm, flag.c_str());
        }

        // Test for convergence
        double diff = fabs(val_old - val);
        val_old = val;
        if (val < abstol) {
            ex = 1;
            exitflag = 1;
        //} else if (diff < abstol * (1 + fabs(val_old))) {
            //ex = 2;
            //exitflag = 3;
        } else if ((iter > 1) && (nrmsx < xtol)) {
            ex = 3;
            exitflag = 2;
        } else if (iter >= maxiter) {
            ex = 4;
            exitflag = 0;
        } else if (funevals > maxfunevals) {
            ex = 5;
            exitflag = 0;
        }

        if (ex > 0) break;
    }

    sol = x;

    // final message
    if (verbose)
        printf("%s", msg[ex - 1]);

    return exitflag;
}
