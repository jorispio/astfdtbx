// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __R3BP_LAMBERT_PROBLEM_SOLUTION_HPP__
#define __R3BP_LAMBERT_PROBLEM_SOLUTION_HPP__

#include "SpaceMechanicsToolBox.hpp"

struct R3bpLambertProblemSolution {
    Vector3 r0;  // position vector on the departure Libration point orbit
    Vector3 v0; // velocity vector on the departure Libration point orbit
    double tau1; // tau defining the position along the departure Libration point orbit
    
    Vector3 rf; // position vector on the arrival Libration point orbit
    Vector3 vf; // velocity vector on the arrival Libration point orbit
    double tau2;// tau defining the position along the arrival Libration point orbit
            
    Vector3 v1; // initial departure velocity
    Vector3 v2; // final arrival velocity    
    
    double tof; // transfer duration

    Vector6 get_init_state() const {
        Vector6 s; s << r0, v1;
        return s;
    }

    Vector6 get_final_state() const {
        Vector6 s; s << rf, v2;
        return s;
    }    
                    
    Vector3 get_dv1() {
        return v1 - v0;
    }
    
    Vector3 get_dv2() {
        return vf - v2;
    }    
};


#endif // __R3BP_LAMBERT_PROBLEM_SOLUTION_HPP__

