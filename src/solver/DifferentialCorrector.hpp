// $Id: DifferentialCorrector.hpp 132 2015-12-29 20:57:47Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   SpaceMechanicsToolBox is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   SpaceMechanicsToolBox is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SpaceMechanicsToolBox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_DIFFERENTIAL_CORRECTOR_HPP_
#define __R3BP_DIFFERENTIAL_CORRECTOR_HPP_

#include <stdio.h>

#include <algorithm>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

#include "SpaceMechanicsToolBox.hpp"
#include "DifferentialCorrectorException.hpp"

class DifferentialCorrector {
 private:
    static const char* msg[6];
    ArrayXd vminimum;
    ArrayXd vmaximum;
    ArrayXd max_step;

 protected:
    void differentiation(const VECTORVAR& x0, MATRIXVAR& df);

 public:
    DifferentialCorrector() { }
    virtual ~DifferentialCorrector() { }

    template <class DATA, class FUN> static DATA test(FUN fun, DATA x) {
        return (DATA)fun(x);
    }

    virtual uint get_n() = 0;
    virtual uint get_m() = 0;

    void set_variable_minimum(const VECTORVAR& vminimum);
    template <typename... T>
    void set_variable_minimum(T... values) {
        const int size = sizeof...(values);
        double res[size]= {values...};
        vminimum = Map<ArrayXd>(res, size);
    }
    ArrayXd get_variable_minimum();

    void set_variable_maximum(const VECTORVAR& vmaximum);
    template <typename... T>
    void set_variable_maximum(T... values) {
        const int size = sizeof...(values);
        double res[size]= {values...};
        vmaximum = Map<ArrayXd>(res, size);
    }
    ArrayXd get_variable_maximum();

    void set_maximum_step(const VECTORVAR& max_step_);
    template <typename... T>
    void set_maximum_step(T... values) {
        const int size = sizeof...(values);
        double res[size]= {values...};
        max_step = Map<ArrayXd>(res, size);
    }
    ArrayXd get_maximum_step();

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    virtual bool valuefun(const VECTORVAR&, VECTORVAR&) {
        throw DifferentialCorrectorException("DifferentialCorrector::valuefun() not implemented!\n");
        return false;
    }

    virtual int jacobian(const VECTORVAR&, MATRIXVAR&) {
        throw DifferentialCorrectorException("DifferentialCorrector::jacobian() not implemented!\n");
        return 0;
    }

    int solve(const VECTORVAR& x0, VECTORVAR& sol,
                bool verbose = false,
                int maxiter = 100, double tol = 1e-6, double xtol=1e-6, int maxfunevals = 5000, double alphaInit = 0.1);
};

#endif  // __R3BP_DIFFERENTIAL_CORRECTOR_HPP_
