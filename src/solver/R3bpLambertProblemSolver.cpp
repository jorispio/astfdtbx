// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "R3bpLambertProblemSolver.hpp"

#include <algorithm>
#include <cstring>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

// ------------------------------------------------------------------------
R3bpLambertProblemSolver::R3bpLambertProblemSolver(double mu_ratio_)
    : RAPHSON(), mu_ratio(mu_ratio_) {
    problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    matJacobian = Matrix3::Zero();
}
// ------------------------------------------------------------------------
R3bpLambertProblemSolver::~R3bpLambertProblemSolver() {
    if (problem != NULL) {
        delete problem;
    }
    if (propagator != NULL) {
        delete propagator;
    }
}
// ------------------------------------------------------------------------
/** Set the orbit initial conditions, that will be used, resp. partly updated
 * when integrating, resp. finding, an orbit
 */
void R3bpLambertProblemSolver::set_boundary_conditions(const double *x_init_, const double *x_final_) {
    memcpy(x_init, x_init_, 3 * sizeof(double));
    memcpy(x_final, x_final_, 3 * sizeof(double));    
}

// ------------------------------------------------------------------------
/** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
*/
// ------------------------------------------------------------------------
VECTORVAR R3bpLambertProblemSolver::valuefun(const VECTORVAR& x, bool& iret) {
    iret = false;
    Vector3 z;
    Matrix6 phix;
    zerof(x, z, matJacobian, phix);
    return z;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 */
// ------------------------------------------------------------------------
int R3bpLambertProblemSolver::jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) {
    Vector3 z;
    Matrix6 phix;
    zerof(x, z, matJacobian, phix);
    Fjac = matJacobian;
    return 0;
}
// ------------------------------------------------------------------------
Vector3 R3bpLambertProblemSolver::get_initial_position() {
    Vector3 r0;
    double x0 = x_init[0];
    double y0 = x_init[1];
    double z0 = x_init[2];
    r0 <<  x0, y0, z0;
    return r0;
}
// ------------------------------------------------------------------------
Vector3 R3bpLambertProblemSolver::get_initial_velocity(const VECTORVAR &x) {
    Vector3 v0;
    v0 << x(0), x(1), x(2);
    return v0;
}
// ------------------------------------------------------------------------
Vector3 R3bpLambertProblemSolver::get_final_velocity(const VECTORVAR &x) {
    Vector3 r0 = get_initial_position();
    Vector3 v0 = get_initial_velocity(x);

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;

    try {
        propagator->getSTM(r0, v0,
               0, tof,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    }
    catch (HelioLibException &e) {
        char msg[1024];
        snprintf(msg, 1024, "R3bpLambertProblemSolver: Failed getting velocity. %s", e.what());
        throw new HelioLibException(msg);
    }
    
    return vf;
}
// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the inital point (y,z)
 * The periodic Halo orbit should be symmetric with the x-z plane. Thus,
 * considering only half the orbit, the desired terminal conditions are of
 * the form
 *     [xd, 0, zd, 0, vyd, 0]
 * The same applies for the initial conditions then.
 * A is a parameter giving the size of the Halo.
 *
 */
Vector3 R3bpLambertProblemSolver::zerof(const VECTORVAR &x,
        /* outputs */
        Vector3 &z, Matrix3 &J, Matrix6 &phix) {
    Vector3 r0 = get_initial_position();
    Vector3 v0 = get_initial_velocity(x);

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, tof,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    phix = STM.block(0, 0, 6, 6);  // stm matrix after one revolution

    // matching final position
    z <<  rf(0) - x_final[0],
          rf(1) - x_final[1],
          rf(2) - x_final[2];

    J = phix.block(0, 3, 3, 3);

    return z;
}

// ------------------------------------------------------------------------
// v0   is the current velocity vector, or an initial guess for v1
int R3bpLambertProblemSolver::solve(const double *p0, const double *pf, double tof_, 
            const Vector3& v0, 
            Vector3& v1, Vector3& v2, bool verbose, int maxiter, double tol) {
    tof = tof_;
    set_boundary_conditions(p0, pf); 
    
    VECTORVAR x0 = v0;
    VECTORVAR sol = VECTORVAR::Zero(3);
    int res = RAPHSON::solve(x0, sol, verbose, maxiter, tol);
    v1 = sol;
    v2 = get_final_velocity(sol);
    
    return res;
}

// ------------------------------------------------------------------------
// compute the set of tau-parameter transfers between orbit0 and orbitf
// dtau is the step increment on parameter tau describing the position along the orbit. tau in [0, 1]
//
int R3bpLambertProblemSolver::solve(OrbitSolution& orbit_dep, OrbitSolution& orbit_arr, 
                    double tof_, double dtau, 
                    std::vector<R3bpLambertProblemSolution>& solutions, bool verbose, int maxiter, double tol) {
    tof = tof_;
    
    int nstep = (int) ceil(1. / dtau);
    
    double period_dep = orbit_dep.get_period();    
    double timestep_dep = period_dep / nstep;
    double tspan_dep[2] = {0, period_dep};
    std::vector<Vector7> xdep = orbit_dep.propagate(tspan_dep, timestep_dep);
   
    double period_arr = orbit_arr.get_period();
    double timestep_arr = period_arr / nstep;
    double tspan_arr[2] = {0, period_dep};    
    std::vector<Vector7> xarr = orbit_arr.propagate(tspan_arr, timestep_arr); 

    double p0[3], pf[3];
    solutions.clear();
    for (int idx = 0; idx < nstep; ++idx) {    
        double tau1 = idx * dtau;
        for (int jdx = 0; jdx < nstep; ++jdx) {
            double tau2 = jdx * dtau;
        
            R3bpLambertProblemSolution solution;
            solution.tof = tof_;
            
            solution.r0 = xdep.at(idx).segment(0,3);
            solution.v0 = xdep.at(idx).segment(3,3);        
            solution.tau1 = tau1;
            
            solution.rf = xarr.at(jdx).segment(0,3);
            solution.vf = xarr.at(jdx).segment(3,3);
            solution.tau2 = tau2;
                    
            // first orbit
            p0[0] = solution.r0[0]; p0[1] = solution.r0[1]; p0[2] = solution.r0[2];

            // second orbit
            pf[0] = solution.rf[0]; pf[1] = solution.rf[1]; pf[2] = solution.rf[2];
            
            // Lambert's problem
            int res = solve(p0, pf, tof_, solution.v0, solution.v1, solution.v2, verbose, maxiter, tol);

            solutions.push_back(solution);
        }
    }

    return 1;
}


                    
