// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LOWTHRUST_PROBLEM_HPP__
#define __R3BP_LOWTHRUST_PROBLEM_HPP__

#include <ctime>
#include <string>
#include <vector>

#include "propulsion.hpp"

#include "SpaceMechanicsToolBox.hpp"

enum R3bpLowThrustTerminalConstraint {
    E_RENDEZVOUS,
    E_POSITION
};

struct R3bpLowThrustProblemSolution {
    Vector7 x;
};


class R3bpLowThrustProblemConstraints {
 private:
    Vector3 final_pos;
    Vector3 final_vel;

 public:
    R3bpLowThrustProblemConstraints() {}
    
    void set_final_position(Vector3& pos) {
        final_pos = pos;
    }

    void set_final_velocity(Vector3& vel) {
        final_vel = vel;
    }
    
    /** constraints + transversality conditions. */
    std::vector<double> compute_transversality_conditions(Vector7& state, Vector7& costate, R3bpLowThrustTerminalConstraint constraint_type);

    /** time optimality transversality condition. */
    double compute_time_optimal_condition(Vector7& state, Vector7& dstate_dt, Vector7& costate);
    
    /** compute jacobian d(transversality conditions)/d(costate). */
    void compute_jacobian(Vector7& state, Vector7& costate, MATRIXVAR &jacobian);
};


class R3bpLowThrustProblem : public R3bpLowThrustProblemConstraints{
 private:
    double mu_ratio;
    std::vector<double> trajectory_time;
    std::vector<Vector7> trajectory_state;
    ThrusterData propulsion_system;


    //TDYNAMICS_STM *dynamics;
 public:
    R3bpLowThrustProblem(double mu_ratio, const ThrusterData& propulsion_system, double ecc = 0);
    ~R3bpLowThrustProblem() { };
    
    R3bpLowThrustProblemSolution solve(const Vector7& x0, const Vector7& xf, double tof, Vector7& lambda0);
};

// ---------------------------------------------------------------------------
#endif  // __R3BP_LOWTHRUST_PROBLEM_HPP__
