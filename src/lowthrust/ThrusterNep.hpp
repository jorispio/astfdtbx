// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LOWTHRUST_THRUSTER_NEP_HPP__
#define __R3BP_LOWTHRUST_THRUSTER_NEP_HPP__

#include "SpaceMechanicsToolBox.hpp"

class ThrusterNep : public Thruster {
 protected:
    /** Get unperturbed (eclipse) thrust */
    virtual double GetUnperturbedThrust(double t, const Vector3& R_Sc_Cb) {
        return mThrusterData.Fth;
    }
    
 public:
    ThrusterNep(const ThrusterData& thrusterDef) : Thruster(thrusterDef, NULL) { };
    ~ThrusterNep() {};
    
    /** return the thrust amplitude */
    virtual double GetThrust(double t, const Vector3& r_sun) {
        return mThrusterData.Fth;
    }

    /* return the fuel mass flow rate */
    virtual double GetMassFlowRate(double t, const Vector3& r_sun) {
        return mThrusterData.Fth / mThrusterData.g0Isp;
    };

    /* Returns the derivative of thrust amplitude with respect to the position
     * for a given throttle, and spacecraft position
     * with respect to the Sun
     */
    virtual double getDerivatives(real_type t, const Vector3& R_sun, Vector3& dFr, Vector3& dQ, bool useSmoothMin = true) {
        return 0;
    }

    /* Return the time derivative of the thrust amplitude. */
    virtual double getTimeDerivative(real_type t, const Vector3& R_Sc_Cb) {
        return 0;
    }

    /* Return second-order derivative of thrust amplitude wrt position vector. */
    virtual void getSecondDerivatives(real_type t, const Vector3& R, Matrix3& d2Fr, Matrix3& d2Q) {
    
    }
};

#endif // __R3BP_LOWTHRUST_THRUSTER_NEP_HPP__
