// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __SMTBX_STATETRANSITIONMATRIX_RTBP_FOR_LOWTHRUST_HPP__
#define __SMTBX_STATETRANSITIONMATRIX_RTBP_FOR_LOWTHRUST_HPP__

#include "maths/coordinates/CartesianCoordinates.hpp"
#include "propagation/StateTransitionMatrix.hpp"
#include "DynamicsEllipticThreeBodyProblemWithCostate.hpp"

/* ------------------------------------------------------------------------------- */
class StateTransitionMatrixRtbpForLowThrust : public StateTransitionMatrix<2 * X_SIZE> {
private:
    /** control */
    double uxyzDefault[3];

    EllipticThreeBodyProblemWithCostate *dynamicalModel;

    /* */
    void DiffEqn(real_type x, const real_type* y, real_type* f);

    /* */
    void dynamic(unsigned n, double t, double* X, double* Xdot);

    /* */
    void solout(long nr, double xold, double x, double* y, unsigned n, int* irtrn);

    /* */
    void STMdispatch(const Matrix14& STM, Matrix7& STM1, Matrix7& STM2, Matrix7& STM3, Matrix7& STM4);

    MATRIXVAR dFdX;

 public:
    StateTransitionMatrixRtbpForLowThrust(double mu_ratio, TDynamicsBase *dynamicalModel);
    ~StateTransitionMatrixRtbpForLowThrust(void) { }

    /** */
    void setDefaultControl(const Vector3& u);

    /* */
    int getFinalPoint(const Vector3d& r1,
                      const Vector3d& v1,
                      double t0,
                      double ti,
                      /* OUTPUTS */
                      Vector3d& rf,
                      Vector3d& vf);

    /* */
    int getSTM(const Vector3d& r1,
               const Vector3d& v1,
               double mass,
               const Vector7& lambda,
               double t0,
               double &ti,
               /* OUTPUTS */
               Matrix7& STM1,
               Matrix7& STM2,
               Matrix7& STM3,
               Matrix7& STM4,
               Matrix14& STM,
               Vector3dExt& P1,
               Vector3dExt& P2,
               double& P3,
               Vector3d& rf,
               Vector3d& vf);

    /* */
    int getSTM(const Vector3d& r1,
               const Vector3d& v1,
               double mass,
               const Vector7& lambda,
               double t0,
               double &ti,
               /* OUTPUTS */
               Matrix7& STM1,
               Matrix7& STM2,
               Matrix7& STM3,
               Matrix7& STM4,
               Matrix14& STM,
               Vector3d& rf,
               Vector3d& vf);
};
/* ------------------------------------------------------------------------------- */
#endif  // __SMTBX_STATETRANSITIONMATRIX_RTBP_FOR_LOWTHRUST_HPP__

