// $Id: DynamicssEllipticThreeBodyProblem.cpp 30 2013-06-22 10:21:41Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <stdlib.h>
#include <math.h>  /* fabs */
#include <float.h> /* DBL_EPSILON */
#include <string.h>

#include <cstdio>

#include "ThrusterNep.hpp"
#include "R3bpOptimalGuidance.hpp"

#include "DynamicsEllipticThreeBodyProblemWithCostate.hpp"
#include "propagation/control/NullGuidance.hpp"
#include "propagation/dynamics/DynamicsEllipticThreeBodyProblem.hpp"


using HelioLib::MJD_2000;

// ---------------------------------------------------------------------
/**
 * Constructor
 */
EllipticThreeBodyProblemWithCostate::EllipticThreeBodyProblemWithCostate(real_type mu1_, real_type mu2_, real_type e_,
                                                    const ThrusterData &thruster_data)
    : TDynamicsBase(D_ERTBP, thruster_data, mu2_ / (mu1_ + mu2_)),
      mu1(mu1_ / (mu1_ + mu2_)), mu2(mu2_ / (mu1_ + mu2_)), ecc(e_) {

    optimalControl = new R3bpOptimalGuidance(this);

    body1_pos << -constMu, 0, 0;
    body2_pos << 1 - constMu, 0, 0;   
    
    SetThruster(new ThrusterNep(thruster_data));
}

/* ---------------------------------------------------------------------
 * Constructor
 * --------------------------------------------------------------------- */
EllipticThreeBodyProblemWithCostate::EllipticThreeBodyProblemWithCostate(real_type mu_ratio, const ThrusterData &thruster_data)
    : TDynamicsBase(D_ERTBP, thruster_data, mu_ratio), mu1(1 - mu_ratio), mu2(mu_ratio), ecc(0) {

    optimalControl = new R3bpOptimalGuidance(this);

    body1_pos << -constMu, 0, 0;
    body2_pos << 1 - constMu, 0, 0;
    
    SetThruster(new ThrusterNep(thruster_data));
}
// ---------------------------------------------------------------------
EllipticThreeBodyProblemWithCostate::~EllipticThreeBodyProblemWithCostate() {
    if (optimalControl != nullptr) {
        delete optimalControl;
    }
    if (mThruster != nullptr) {
        delete mThruster;
    }    
}

/* --------------------------------------------------------------------- *
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::Init(const real_type* S0_, const ThrusterData& thrusterDefinition,
                                    double,
                                    real_type) {
    memcpy(S0, S0_, 2 * STATE_SIZE * sizeof(real_type));
    if (mThruster != nullptr) {
        mThruster->SetThruster(thrusterDefinition);
    }
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::SetInitialConditions(const double* S0_) {
    //
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
bool EllipticThreeBodyProblemWithCostate::ComputeCommonVariablesForStateOnly(real_type t, const real_type* StateLagranges,
                                                                    CommonVariables &common) {

    common.Thrust = 0;
    common.qfuel = 0;
    common.g0Isp = 0;
    if (mThruster != nullptr) {
        common.Thrust = mThruster->GetNominalThrust();
        common.qfuel = mThruster->GetMassFlowRate(t, common.R);
        common.g0Isp = mThruster->GetExhaustVelocity();
    }

    for (int i = 0; i < STATE_SIZE; ++i) {
        common.state(i) = StateLagranges[i];
    }

    common.date = GenericDate(t * a_s / 86400., HelioLib::MJD_2000);

    // position
    common.R = common.state.segment(0, 3);
    // speed
    common.V = common.state.segment(3, 3);
    // mass
    common.mass = 1; // FIXME
    double tau = common.state(6);   

    return true;
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
bool EllipticThreeBodyProblemWithCostate::ComputeCommonVariablesForOptimalControl(real_type t, const real_type* StateLagranges,
                                                                        CommonVariables &common) {
    /* costates */
    for (int i = 0; i < STATE_SIZE; ++i) {
        common.lambda(i) = StateLagranges[STATE_SIZE + i];
    }

    common.Thrust = 0;
    common.qfuel = 0;
    common.g0Isp = 0;
    if (mThruster != nullptr) {
        common.Thrust = mThruster->GetNominalThrust();
        common.qfuel = mThruster->GetMassFlowRate(t, common.R);
        common.g0Isp = mThruster->GetExhaustVelocity();
    }
    else {
        std::cout << "no thrust!" << std::endl;
    }

    common.lambda_R = common.lambda.segment(0, 3);
    common.lambda_V = common.lambda.segment(3, 3);
    common.lambda_m = common.lambda(6);
    
    common.U.setZero();
    if (optimalControl != nullptr) {
        common.U = optimalControl->GetControlDirection(common.state.data(), common.lambda.data());
    }

    return true;
}
/* ---------------------------------------------------------------------
 * true anomaly f(t), the angle that the line segment joining the rightmost focus of
 * m2’s elliptical orbit to m2’s position at periapsis makes with the line segment
 * joining that focus to m2’s position at time t.
 *
 * Normalizing units so that the pair of primary masses has unit angular momentum and the
 * distance between the two primaries at f=pi/2 is unity.
 *
 * e is the eccentricity of m2’s elliptical orbit
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::getPrimaryPositions(const real_type f, real_type *xy1, real_type *xy2) {
    if (xy1 != nullptr) {
        double den = 1 + ecc * cos(f);
        xy1[0] = -constMu * cos(f) / den;
        xy1[1] = -constMu * sin(f) / den;
    }

    if (xy2 != nullptr) {
        double den = 1 + ecc * cos(f);
        xy2[0] = (1 - constMu) * cos(f) / den;
        xy2[1] = (1 - constMu) * sin(f) / den;
    }
}

/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::GetStateDynamics(real_type t,
                                  const CommonVariables& common,
                     /* OUTPUT */ real_type* x_dot) {

    double vx = common.V(0), vy = common.V(1), vz = common.V(2);
    Vector3 dUdx = EllipticThreeBodyProblem::compute_gravity(constMu, common.R);

    x_dot[0] = vx;
    x_dot[1] = vy;
    x_dot[2] = vz;
    x_dot[3] = 2 * vy - dUdx(0);
    x_dot[4] =-2 * vx - dUdx(1);
    x_dot[5] =-dUdx(2);
    x_dot[6] =-common.qfuel; // mass
    x_dot[6] = 1; // time

    // control contribution
    x_dot[3] += common.U[0] * common.Thrust / common.mass;
    x_dot[4] += common.U[1] * common.Thrust / common.mass;
    x_dot[5] += common.U[2] * common.Thrust / common.mass;
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::GetCostateDynamics(real_type t,
                                    const CommonVariables& common,
                       /* OUTPUT */ real_type* x_dot) {
    double r1x = common.R(0) - body1_pos(0);
    double r1y = common.R(1) - body1_pos(1);
    double r1z = common.R(2) - body1_pos(2);        
    double r1 = (common.R - body1_pos).norm();
    double r12 = r1 * r1;
    double r13 = r12 * r1;
    double r15= r13 * r1 * r1;
    
    double r2x = common.R(0) - body2_pos(0);
    double r2y = common.R(1) - body2_pos(1);
    double r2z = common.R(2) - body2_pos(2);      
    double r2 = (common.R - body2_pos).norm();
    double r22 = r2 * r2;
    double r23 = r22 * r2;
    double r25= r23 * r2 * r2;
    
    double x = common.R(0);
    double y = common.R(1);
    double z = common.R(2);

    Matrix3 dUdR = EllipticThreeBodyProblem::compute_gravity_gradient(constMu, common.R);
    Vector3 l_dot = dUdR * common.lambda_V;
    x_dot[7] = l_dot(0);
    x_dot[8] = l_dot(1);
    x_dot[9] = l_dot(2);
    x_dot[10] =-common.lambda_R(0) + 2 * common.lambda_V(1);
    x_dot[11] =-common.lambda_R(1) - 2 * common.lambda_V(0);
    x_dot[12] =-common.lambda_R(2);
    
    //if (critere == C_TIME) { /* temps min */
        x_dot[13] = common.lambda_V.dot(common.U) * common.Thrust / (common.mass * common.mass);
    //} else {
    //    throw LTIpoptException("Unknown objective function for Dynamics!");
    //}    
    //x_dot[14] = 0; // time
}


/* ---------------------------------------------------------------------
 * Get the optimal control class.
 * --------------------------------------------------------------------- */
TGuidanceControlBase* EllipticThreeBodyProblemWithCostate::GetControlInstance() {
    return optimalControl;
}

/* ---------------------------------------------------------------------
 * Compute time derivative of state dynamics.
 *
 * @see GetStateDynamics()
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::DynamicsTime(const real_type* x, const real_type* x_dot, Vector7& dFdT) {
    double rx = x[0], ry = x[1], rz = x[2];
    Vector3 R1; R1 << rx - body1_pos(0), ry - body1_pos(1), rz - body1_pos(2);
    Vector3 R2; R2 << rx - body2_pos(0), ry - body2_pos(1), rz - body2_pos(2);

    double r1 = R2.norm();
    double r13 = r1 * r1 * r1;
    double r2 = R2.norm();
    double r23 = r2 * r2 * r2;

    // 1st order partial derivatives of the potential U(x,y, z)
    double dUxdt = -rx + mu1 * R1(0) / r13 + mu2 * R2(0) / r23;
    double dUydt = -ry + mu1 * R1(1) / r13 + mu2 * R2(1) / r23;
    double dUzdt = mu1 * R1(2) / r13 + mu2 * R2(2) / r23;

    dFdT(0) = x_dot[3];
    dFdT(1) = x_dot[4];
    dFdT(2) = x_dot[5];
    dFdT(3) = 2 * x_dot[4] - dUxdt;
    dFdT(4) =-2 * x_dot[3] - dUydt;
    dFdT(5) =-dUzdt;
    dFdT(6) = 0; // mass, time-constant flow rate
    //dFdT(6) = 1; // time

    //Vector3 dUdt = optimalControl->Vector3 GetControlDirectionTimeDeriv(x, const real_type* lambda, xdot);
}

// ---------------------------------------------------------------------
/**
 * df/dX, where f = [fx, fl]
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::JacobianMatrixState(const CommonVariables& common, MATRIXVAR& dFdX, int row0, int col0) {
    real_type t = 0;
    real_type x_dot[2 * STATE_SIZE];
    GetStateDynamics(t, common, x_dot);

    real_type x[2 * STATE_SIZE];
    for (int i = 0; i < STATE_SIZE; ++i) {
        x[i] = common.state(i);
        x[STATE_SIZE + i] = common.lambda(i);
    }
    GetJacobianDynamics(t, x, common, x_dot, dFdX);
}
/* ---------------------------------------------------------------------
 * Derivatives wrt costate
 * df/dL, where f = [fx, fl]
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::JacobianMatrixCoState(const CommonVariables& common, MATRIXVAR& dFdL,
                                                        int row0, int col0) {
    // Jacobian matrix dcostate/dstate 
    Vector3 R = common.R;  
    Matrix3 dl_dotdR1, dl_dotdR2;
    Vector3 R1 = R - body1_pos;
    Vector3 R2 = R - body2_pos;
    GravityGradientTimeLambdadR(R1, common.lambda_V, mu1, dl_dotdR1); 
    GravityGradientTimeLambdadR(R2, common.lambda_V, mu2, dl_dotdR2);  
    dFdL.block(7, 0, 3, 3) =-(dl_dotdR1 + dl_dotdR2);
        
    // Jacobian matrix dcostate/dcostate        
    // x_dot[7..9] = l_dot(0..2);
    // with: l_dot = dUdR * common.lambda_V;
    Matrix3 dUdR = EllipticThreeBodyProblem::compute_gravity_gradient(constMu, common.R);
    dFdL.block(7, 10, 3, 3) = dUdR;

    // x_dot[10] =-common.lambda_R(0) + 2 * common.lambda_V(1);
    dFdL(10, 7) =-1;
    dFdL(10, 11) = 2;

    // x_dot[11] =-common.lambda_R(1) - 2 * common.lambda_V(0);
    dFdL(11, 8) =-1;
    dFdL(11, 10) =-2;

    // x_dot[12] =-common.lambda_R(2);
    dFdL(12, 9) =-1;
    
    // x_dot[13] = common.lambda_V.dot(common.U) * common.Thrust / (common.mass * common.mass);
    Matrix3x7 dU_LambdaV;
    optimalControl->ControlDerivativeLambda(common, dU_LambdaV);
    dFdL.block(13, 10, 1, 3) = (common.U + dU_LambdaV.block(0, 3, 3, 3) * common.lambda_V).transpose() * common.Thrust / (common.mass * common.mass);
}
/* ---------------------------------------------------------------------
 * Derivatives wrt state
 * df/dX, where f = [fx, fl]
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::GetJacobianDynamics(real_type t,
                                     const real_type* x,
                                     const CommonVariables& common,
                                     const real_type* x_dot,
                                     MATRIXVAR& dFdX) {
    Vector3 R; R << x[0], x[1], x[2];
    Matrix3 dUdR = EllipticThreeBodyProblem::compute_gravity_gradient(constMu, R);    

    // Jacobian matrix dstate/dstate
    dFdX(0, 3) = 1;
    dFdX(1, 4) = 1;
    dFdX(2, 5) = 1;
    dFdX(3, 4) = 2;
    dFdX(4, 3) =-2;
    // dU/dR    
    dFdX(3, 0) = -dUdR(0,0);
    dFdX(3, 1) = -dUdR(0,1);
    dFdX(3, 2) = -dUdR(0,2);
    dFdX(4, 0) = -dUdR(1,0);
    dFdX(4, 1) = -dUdR(1,1);
    dFdX(4, 2) = -dUdR(1,2);
    dFdX(5, 0) = -dUdR(2,0);
    dFdX(5, 1) = -dUdR(2,1);
    dFdX(5, 2) = -dUdR(2,2);
    // dt/dX (last row)
    for (int idx = 0; idx < STATE_SIZE; ++idx) {
        dFdX(6, idx) = 0;
    }
       
    // last column: dF/dt
    //dFdX.block(0, 6, 6, 1) = dFdt;
    dFdX.block(0, 6, 3, 1) << x_dot[3], x_dot[4], x_dot[5];
    dFdX.block(3, 6, 3, 1) << 0, 0, 0;
    dFdX(6, 6) = x_dot[6];    // dt/dt

    // Jacobian matrix dstate/dcostate
    // x_dot[3..5] += common.U[0..2] * common.Thrust / common.mass;
    Matrix3x7 dU_LambdaV;
    optimalControl->ControlDerivativeLambda(common, dU_LambdaV);
    dFdX.block(3, 10, 3, 3) = dU_LambdaV.block(0, 3, 3, 3) * common.Thrust / common.mass;   
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::DynamicsTimeCoast(const real_type* x,
                                   real_type* x_dot,
                                   Vector7& reference_state,
                                   Vector7& modified_state,
                                   Vector7& dFdT) {
    double rx = x[0], ry = x[1], rz = x[2];
    double vx = x[3], vy = x[4];

    Vector3 R1; R1 << rx - body1_pos(0), ry - body1_pos(1), rz - body1_pos(2);
    Vector3 R2; R2 << rx - body2_pos(0), ry - body2_pos(1), rz - body2_pos(2);

    double r1 = R2.norm();
    double r13 = r1 * r1 * r1;
    double r2 = R2.norm();
    double r23 = r2 * r2 * r2;

    // 1st order partial derivatives of the potential U(x,y, z)
    double Ux = -rx + mu1*R1(1)/r13 + mu2*R2(1)/r23;
    double Uy = -ry + mu1*R1(2)/r13 + mu2*R2(2)/r23;
    double Uz = mu1*R1(3)/r13 + mu2*R2(3)/r23;

    dFdT(0) = x[3];
    dFdT(1) = x[4];
    dFdT(2) = x[4];
    dFdT(3) = 2*vy-Ux;
    dFdT(4) =-2*vx-Uy;
    dFdT(5) =-Uz;
    dFdT(6) = 0;
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
real_type EllipticThreeBodyProblemWithCostate::GetThrustAmplitude(real_type t, const real_type* state, const real_type* lambda) {
    return 1.;
}

/* ---------------------------------------------------------------------
 * Derivative: d/dR (G(R) * lv)
 * with G the gravitiy gradient matrix.
 * --------------------------------------------------------------------- */
void EllipticThreeBodyProblemWithCostate::GravityGradientTimeLambdadR(const Vector3& R,
                                                     const Vector3& lambdaV,
                                                     real_type mu,
                                                     /* OUTPUT */ Matrix3& Gr)
{
    real_type x = R[0];
    real_type x2 = x * x;
    real_type x3 = x2 * x;
    real_type y = R[1];
    real_type y2 = y * y;
    real_type y3 = y2 * y;
    real_type z = R[2];
    real_type z2 = z * z;
    real_type z3 = z2 * z;

    real_type r = R.norm();
    real_type r2 = r * r;
    real_type r4 = r * r * r * r;
    real_type r5 = r * r4;

    // computed with wxMaxima
    Matrix3 Gx, Gy, Gz;
    Gx(0, 0) = ((9 * x) - (15 * x3) / r2);     // Gxx/x
    Gx(1, 0) = ((3 * y) - (15 * x2 * y) / r2); // Gxx/y
    Gx(2, 0) = ((3 * z) - (15 * x2 * z) / r2); // Gxx/z
    Gx(0, 1) = ((3 * y) - (15 * x2 * y) / r2); // Gxy/x
    Gx(1, 1) = ((3 * x) - (15 * x * y2) / r2); // Gxy/y
    Gx(2, 1) = -(15 * x * y * z) / r2;         // Gxy/z
    Gx(0, 2) = ((3 * z) - (15 * x2 * z) / r2); // Gxz/x
    Gx(1, 2) = -(15 * x * y * z) / r2;         // Gxz/y
    Gx(2, 2) = ((3 * x) - (15 * x * z2) / r2); // Gxz/z
    //
    Gy(0, 0) = ((3 * y) - (15 * x2 * y) / r2); // Gyx/x
    Gy(1, 0) = ((3 * x) - (15 * x * y2) / r2); // Gyx/y
    Gy(2, 0) = -(15 * x * y * z) / r2;         // Gyx/z
    Gy(0, 1) = ((3 * x) - (15 * x * y2) / r2); // Gyy/x
    Gy(1, 1) = ((9 * y) - (15 * y3) / r2);     // Gyy/y
    Gy(2, 1) = ((3 * z) - (15 * y2 * z) / r2); // Gyy/z
    Gy(0, 2) = -(15 * x * y * z) / r2;         // Gyz/x
    Gy(1, 2) = ((3 * z) - (15 * y2 * z) / r2); // Gyz/y
    Gy(2, 2) = ((3 * y) - (15 * y * z2) / r2); // Gyz/z
    //
    Gz(0, 0) = ((3 * z) - (15 * x2 * z) / r2); // Gyx/x
    Gz(1, 0) = -(15 * x * y * z) / r2;         // Gyx/y
    Gz(2, 0) = ((3 * x) - (15 * x * z2) / r2); // Gyx/z
    Gz(0, 1) = -(15 * x * y * z) / r2;         // Gyy/x
    Gz(1, 1) = ((3 * z) - (15 * y2 * z) / r2); // Gyy/x
    Gz(2, 1) = ((3 * y) - (15 * y * z2) / r2); // Gyy/y
    Gz(0, 2) = ((3 * x) - (15 * x * z2) / r2); // Gyz/x
    Gz(1, 2) = ((3 * y) - (15 * y * z2) / r2); // Gyz/y
    Gz(2, 2) = ((9 * z) - (15 * z3) / r2);     // Gyz/z

    Gr.block(0, 0, 1, 3) = (mu / r5) * (Gx * lambdaV).transpose();
    Gr.block(1, 0, 1, 3) = (mu / r5) * (Gy * lambdaV).transpose();
    Gr.block(2, 0, 1, 3) = (mu / r5) * (Gz * lambdaV).transpose(); 

    // additional perturbations
    // Derivate: G(R)*lambda_R wrt R
    /*if(forces.size() > 0) {
        GenericDate date(0);

        for(std::vector<const Force*>::iterator f = forces.begin(); f != forces.end(); ++f) {
            // add jacobian of forces wrt position
            Matrix3 mHess[3];
            (*f)->getHessian(date, R, mHess);

            Gr.block(0, 0, 1, 3) += (mHess[0] * lambdaV).transpose();
            Gr.block(1, 0, 1, 3) += (mHess[1] * lambdaV).transpose();
            Gr.block(2, 0, 1, 3) += (mHess[2] * lambdaV).transpose();
        }
    }*/
}
/* ---------------------------------------------------------------------
 *
 * --------------------------------------------------------------------- */
int EllipticThreeBodyProblemWithCostate::CheckDerivatives(real_type t, const real_type* state, const real_type* lambda) {

    double mu_ratio = 0.01;
    real_type xyz[] = {0.95, 0.01, 0};
    Vector3 R; R << xyz[0], xyz[1], xyz[2];
    
    real_type* StateLagranges = new real_type[14];
    memcpy(StateLagranges, state, 7 * sizeof(real_type));
    memcpy(StateLagranges+7, lambda, 7 * sizeof(real_type));
    
    printf("x = [\n");    
    for (int idx=0; idx<14; ++idx) {
        printf("%8.6f ",  StateLagranges[idx]);
    }
    printf("]\n");
           
    CommonVariables common;
    ComputeCommonVariables(0, StateLagranges, common);
    
    MATRIXVAR dFdX_ref = MATRIXVAR::Zero(14, 14);
    MATRIXVAR dFdL_ref = MATRIXVAR::Zero(14, 14);
    JacobianMatrixState(common, dFdX_ref, 0, 0);
    JacobianMatrixCoState(common, dFdL_ref, 0, 0);    
    real_type* x_dot_ref = new real_type[14];
    real_type* l_dot_ref = new real_type[14];  
    GetStateDynamics(0, common, x_dot_ref);
    GetCostateDynamics(0, common, l_dot_ref);
    Map<Vector7> m_x_dot_ref(x_dot_ref);
    Map<Vector7> m_l_dot_ref(l_dot_ref + 7); 
    std::cout << "c_dot =  " << m_x_dot_ref.transpose() << std::endl;
    std::cout << "l_dot =  " << m_l_dot_ref.transpose() << std::endl;
        
    double h = 1e-7;

    Matrix14 dxdot_dx = Matrix14::Zero();    
    real_type* x_dot = new real_type[14];
    for (int idx=0; idx<14; ++idx) {
        StateLagranges[idx] += h;
    
        ComputeCommonVariables(0, StateLagranges, common);
        GetStateDynamics(0, common, x_dot);
        
        StateLagranges[idx] -= h;
                
        Map<Vector7> m_x_dot(x_dot);
        dxdot_dx.block(0, idx, 7, 1) = (m_x_dot - m_x_dot_ref) / h;
    } 
    Matrix14 err_dFdX = dxdot_dx - dFdX_ref;
    
    Matrix14 dlambdadot_dx = Matrix14::Zero();
    real_type* l_dot = new real_type[14];            
    for (int idx=0; idx<14; ++idx) {
        StateLagranges[idx] += h;
        
        ComputeCommonVariables(0, StateLagranges, common);
        GetCostateDynamics(0, common, l_dot);
        
        StateLagranges[idx] -= h;
             
        Map<Vector7> m_l_dot(l_dot + 7);
        dlambdadot_dx.block(0, idx, 7, 1) = (m_l_dot - m_l_dot_ref) / h;
    } 
    MATRIXVAR err_dFdL = dlambdadot_dx.block(0, 0, 7, 14) - dFdL_ref.block(7, 0, 7, 14);

    std::cout << "Error dFdX  : " << std::endl << err_dFdX.block(0, 0, 7, 14) << std::endl;
    std::cout << "By row: " << std::endl;
    for (int idx=0; idx<7; ++idx) {
        std::cout << "Line " << idx << std::endl;
        std::cout << std::setw(12) << dFdX_ref.row(idx) << std::endl;
        std::cout << std::setw(12) << dxdot_dx.row(idx) << std::endl;
    }

    
    std::cout << "Error dFdL  : " << std::endl << err_dFdL << std::endl;
    std::cout << "By row: " << std::endl;
    for (int idx=0; idx<7; ++idx) {
        std::cout << "Line " << idx << std::endl;
        std::cout << std::setw(12) << dFdL_ref.row(7 + idx) << std::endl;
        std::cout << std::setw(12) << dlambdadot_dx.row(idx) << std::endl;
    }

    return 0;
}

