// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class STM
 * return non-autonomous State Transition Matrix
 * The state is extended with the time (parameter).
 * This class implements all the dynamical equations for the problem.
 * Other class such as TPRIMERDYNAMICS or TSTMExt_t use static member
 * of this class for dynamics.
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.3
 * History:
 *        - change into a class
 *        - correct a buf related to the allocation of STM(i)
 *        - Add function GravityMatrix to be called by externally.
 *        - Use MATRIX macro to solve convention issue in the code.
 *        - Extended state Z = [X, t] with X = [R, V]. I included the time.
 *          STM can now be used for time derivatives.
 *        -
 * --------------------------------------------------------------------- */
#include "StateTransitionMatrixRtbpForLowThrust.hpp"

#include <float.h> // DBL_EPSILON
#include <math.h>  // fabs
#include <stdlib.h>
#include <string.h>

#include <cstdio>
#include <vector>

#include "maths/MatrixArray.hpp"

// ---------------------------------------------------------------------
/** Creator
 */
StateTransitionMatrixRtbpForLowThrust::StateTransitionMatrixRtbpForLowThrust(double mu_ratio, TDynamicsBase *dynamicalModel)
    : StateTransitionMatrix(mu_ratio, 0, 0), dynamicalModel((EllipticThreeBodyProblemWithCostate*)dynamicalModel) {
    uxyzDefault[0] = 0.;
    uxyzDefault[1] = 0.;
    uxyzDefault[2] = 0.;
    
    dFdX = MATRIXVAR::Zero(2 * STATE_SIZE, 2 * STATE_SIZE);
    ODESOLVER::nmax = 100000000;
}
// ---------------------------------------------------------------------
/**
 */
void StateTransitionMatrixRtbpForLowThrust::setDefaultControl(const Vector3& u) {
    uxyzDefault[0] = u(0);
    uxyzDefault[1] = u(1);
    uxyzDefault[2] = u(2);
}
// ---------------------------------------------------------------------
/**
 * Propagate STM and retrieve STM sub-blocks.
 */
int StateTransitionMatrixRtbpForLowThrust::getSTM(const Vector3d& r1, const Vector3d& v1, double mass,
                                           const Vector7& lambda,
                                           double t0,
                                           double &ti,
                                           /* OUTPUTS */
                                           Matrix7& STM1,
                                           Matrix7& STM2,
                                           Matrix7& STM3,
                                           Matrix7& STM4,
                                           Matrix14& STMf,
                                           Vector3dExt& P1,
                                           Vector3dExt& P2,
                                           double& P3,
                                           Vector3d& rf,
                                           Vector3d& vf) {
    double X0[2 * Xt_SIZE];
    // state (p, v)
    for (int i = 0; i < 3; ++i) {
        X0[i] = r1(i);
        X0[i + 3] = v1(i);
    }
    X0[6] = 0;    // time or mass
    // co-state
    for (int i = 0; i < Xt_SIZE; ++i) {    
        X0[Xt_SIZE + i] = lambda(i);
    }

    Matrix14 STM0_notused = Matrix14::Identity();
    double Xf[2 * Xt_SIZE];
    int ret = StateTransitionMatrix<2 * Xt_SIZE>::getSTM(X0, STM0_notused, t0, ti, STMf, Xf);

    // The result of solve() is PHI(tf, t0) when integrating from t0 to tf.
    //
    // use simplectic property for inversion for a better precision
    // STM = [STM1, STM2; STM3, STM4]
    // invSTM = [STM4, -STM2; -STM3, STM1]
    // inv(STM(t, t0)) = STM(t0, t)
    //        STM1 -> STM4'
    //        STM2 -> -STM2'
    //        STM3 -> -STM3'
    //        STM4 -> STM1'
    //
    STMdispatch(STMf, STM1, STM2, STM3, STM4);
    P1 = STMf.block(0, 6, 3, 1);
    P2 = STMf.block(3, 6, 3, 1);
    P3 = 1.;

    for (int i = 0; i < 3; ++i) {
        rf(i) = Xf[i];
        vf(i) = Xf[i + 3];
    }

    return ret;
}

// ---------------------------------------------------------------------
/**
 * Propagate STM and retrieve block and parameter gradients.
 */
int StateTransitionMatrixRtbpForLowThrust::getSTM(const Vector3d& r1, const Vector3d& v1, double mass,
                                           const Vector7& lambda,
                                           double t0,
                                           double &ti,
                                           /* OUTPUTS */
                                           Matrix7& STM1,
                                           Matrix7& STM2,
                                           Matrix7& STM3,
                                           Matrix7& STM4,
                                           Matrix14& STMf,
                                           Vector3d& rf,
                                           Vector3d& vf) {
    Vector3dExt P1, P2;
    Matrix14 STMf7;
    double P3 = 1;
    return getSTM(r1, v1, mass, lambda, t0, ti, STM1, STM2, STM3, STM4, STMf7, P1, P2, P3, rf, vf);
}

// ---------------------------------------------------------------------
/** DYNAMIC
 * Integrate state dynamic equation, and state transition matrix
 * simulteneously
 */
void StateTransitionMatrixRtbpForLowThrust::DiffEqn(real_type tau, const real_type* X, real_type* Xdot) {
    int sz[] = { 2 * Xt_SIZE, 2 * Xt_SIZE };

    // STMSYN
    // State Transition Matrix Dynamic
    // --------------------------------
    Matrix14 STM;
    for (int i = 0; i < 2 * Xt_SIZE; ++i)
        for (int j = 0; j < 2 * Xt_SIZE; ++j)
            STM(i, j) = MATRIX(X + 2 * Xt_SIZE, i, j, sz);

    MATRIXVAR dFdX = MATRIXVAR::Zero(2 * STATE_SIZE, 2 * STATE_SIZE);

    CommonVariables cV;
    if (dynamicalModel->ComputeCommonVariables(tau, X, cV)) {
        dynamicalModel->GetStateDynamics(tau, cV, Xdot);
        dynamicalModel->GetCostateDynamics(tau, cV, Xdot);
        
        dynamicalModel->JacobianMatrixState(cV, dFdX);
        dynamicalModel->JacobianMatrixCoState(cV, dFdX);

        /* diff(F, t) */
    //    Vector7 dFdT;
    //    dynamicalModel->DynamicsTime(X, Xdot, dFdT);
    //    dFdX(0, 6) /* A(1,7) */ = dFdT(0);
    //    dFdX(1, 6) /* A(2,7) */ = dFdT(1);
    //    dFdX(2, 6) /* A(3,7) */ = dFdT(2);
    //    dFdX(3, 6) /* A(4,7) */ = dFdT(3);
    //    dFdX(4, 6) /* A(5,7) */ = dFdT(4);
    //    dFdX(5, 6) /* A(6,7) */ = dFdT(5);
    //    dFdX(6, 6) = dFdT(6);

        if (forces.size() > 0) {
            for (std::vector<Force*>::iterator f = forces.begin(); f != forces.end(); ++f) {
                // TODO(joris) Update A with forces Jacobian
            }
        }

        Matrix14 dSTM = dFdX * STM;
        for (int i = 0; i < 2 * Xt_SIZE; ++i) {
            for (int j = 0; j < 2 * Xt_SIZE; ++j) {
                MATRIX(Xdot + 2 * Xt_SIZE, i, j, sz) = dSTM(i, j);  // transpose
            }
        }
    }
}

// ---------------------------------------------------------------------
/**
 * Dispatches elements regarding state [R, V]
 * These are submatrices of size 3 x 3
 */
void StateTransitionMatrixRtbpForLowThrust::STMdispatch(const Matrix14& STM,
                                                 Matrix7& STM1,
                                                 Matrix7& STM2,
                                                 Matrix7& STM3,
                                                 Matrix7& STM4) {
    STM1 = STM.block(0, 0, 7, 7);
    STM2 = STM.block(0, 7, 7, 7);
    STM3 = STM.block(7, 0, 7, 7);
    STM4 = STM.block(7, 7, 7, 7);
}

