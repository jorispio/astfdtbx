// $Id$
// ---------------------------------------------------------------------
/*
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
/* --------------------------------------------------------------------- *
 * @class TrajectoryPropageRtbpWithCostate
 *
 * Author: Joris Olympio
 * Date:
 * Last Revision:
 * Ver: 1.0
 * --------------------------------------------------------------------- */
#include "TrajectoryPropageRtbpWithCostate.hpp"

#include <stdlib.h>
#include <math.h>  /* fabs */
#include <float.h> /* DBL_EPSILON */

#include <cstdio>
#include <string>
#include <vector>

#include "DynamicsEllipticThreeBodyProblemWithCostate.hpp"

using HelioLib::MJD_2000;

/* ---------------------------------------------------------------------
 * Constructor
 * @param mu gravitational constant
 * @param q  fuel flow rate (default 0, constant mass)
 * --------------------------------------------------------------------- */
TrajectoryPropageRtbpWithCostate::TrajectoryPropageRtbpWithCostate(double mu1_, double mu2_, double ecc, const ThrusterData &ThrusterDef)
    : TrajectoryPropage(7, (mu1_ + mu2_) / 2., NULL, 0., 0., 0.), a_s(1) {
    controlDirection = CUSTOM;
    controlAmplitude = 1;
    uDefault[0] = 0.;
    uDefault[1] = 0.;
    uDefault[2] = 0.;

    nmax = 1e10;  // DOPRI5

    dynamicalModel = new EllipticThreeBodyProblemWithCostate(mu1_, mu2_, ecc, ThrusterDef);
    dynamicalModel->setTimeNormalisation(a_s);
}

/* ---------------------------------------------------------------------
 * Constructor
 * @param mu gravitational constant
 * @param q  fuel flow rate (default 0, constant mass)
 * --------------------------------------------------------------------- */
TrajectoryPropageRtbpWithCostate::TrajectoryPropageRtbpWithCostate(double mu_ratio_, double ecc, const ThrusterData &ThrusterDef)
    : TrajectoryPropage(7, mu_ratio_, NULL, 0., 0., 0.), a_s(1) {
    controlDirection = CUSTOM;
    controlAmplitude = 1;
    uDefault[0] = 0.;
    uDefault[1] = 0.;
    uDefault[2] = 0.;

    dynamicalModel = new EllipticThreeBodyProblemWithCostate(mu_ratio_, ThrusterDef);
    dynamicalModel->setTimeNormalisation(a_s);
}
// ---------------------------------------------------------------------
void TrajectoryPropageRtbpWithCostate::setDefaultControl(CONTROL_DIRECTION direction, double amplitude, const Vector3& u) {
    throw HelioLibException("setDefaultControl()", "Not implemented because we use the optimal control", "Not implemented");
}
/* ---------------------------------------------------------------------
 * Input must be 6 dim. We fill then the remaining dim.
 * Return 6x6 transition matrix STM(t0, ti)
 * contruct a multi impulse trajectory, with patched conic approximation.
 * Each legs are computed individually and then patched together.
 * @t0 in seconds
 * @ti in seconds
 * @steps in seconds
 * --------------------------------------------------------------------- */
int TrajectoryPropageRtbpWithCostate::getTrajectory(const Vector3d& r1,
                                              const Vector3d& v1,
                                              double m1,
                                              const Vector7& lambda,
                                              double t0,
                                              double ti,
                                              double step,
                                              /* OUTPUTS */
                                              std::vector<double>& trajectoryTime,
                                              std::vector<Vector7>& trajectory,
                                              double &finalTime,
                                              Vector3d& rf,
                                              Vector3d& vf,
                                              double& mf) {
    Vector7 initialState;
    Vector7 finalState;
    for (int i = 0; i < 3; ++i) {
        initialState(i) = r1(i);
        initialState(i + 3) = v1(i);
    }
    initialState(6) = m1;

    for (int idx = 0; idx < 7; ++idx) {
        TrajectoryPropage::AddAuxiliaryEquationInitialState(idx, lambda(idx));
    }
    
    int ret = TrajectoryPropage::getTrajectory(initialState, t0, ti, step,
                                                trajectoryTime, trajectory, finalTime, finalState);

    rf = finalState.block(0, 0, 3, 1);
    vf = finalState.block(3, 0, 3, 1);
    mf = finalState(6);

    return ret;
}
/* ---------------------------------------------------------------------
 * Input must be 6 dim. We fill then the remaining dim.
 * Return 6x6 transition matrix STM(t0, ti)
 * contruct a multi impulse trajectory, with patched conic approximation.
 * Each legs are computed individually and then patched together.
 *
 * --------------------------------------------------------------------- */
int TrajectoryPropageRtbpWithCostate::getTrajectory(const Vector3d& r1,
                                              const Vector3d& v1,
                                              double m1,
                                              const Vector7& lambda,
                                              std::vector<double>& tmesh,
                                              /* OUTPUTS */
                                              std::vector<Vector7>& trajectory) {
    Vector7 initialState;
    Vector7 finalState;
    for (int i = 0; i < 3; ++i) {
        initialState(i) = r1(i);
        initialState(i + 3) = v1(i);
    }
    initialState(6) = m1;
    
    for (int idx = 0; idx < 7; ++idx) {
        TrajectoryPropage::AddAuxiliaryEquationInitialState(idx, lambda(idx));
    }
    
    return TrajectoryPropage::getTrajectory(initialState, tmesh, trajectory);
}
// ---------------------------------------------------------------------
int TrajectoryPropageRtbpWithCostate::getFinalPoint(const Vector7& x0,
                      double t0,
                      double ti,
                      /* OUTPUTS */
                      double &finalTime,
                      Vector7& xf) { 
    throw HelioLibException("getFinalPoint()", "Not implemented", "Not implemented");                      
}
// ---------------------------------------------------------------------
/**
 */
int TrajectoryPropageRtbpWithCostate::getFinalPoint(const Vector7& x0, const Vector7& lambda,
                                              double t0,
                                              double ti,
                                              /* OUTPUTS */
                                              double &finalTime,
                                              Vector7& xf, Vector7& lambdaf) {
    Vector3d rf, vf;
    double mf;
    int ret = getFinalPoint(x0.segment(0, 3), x0.segment(3, 3), x0(6), lambda, t0, ti, finalTime, rf, vf, mf, lambdaf);
    xf.segment(0, 3) = rf;
    xf.segment(3, 3) = vf;
    xf(6) = mf;
    return ret;
}
// ---------------------------------------------------------------------
/**
 */
int TrajectoryPropageRtbpWithCostate::getFinalPoint(const Vector3d& r1,
                                              const Vector3d& v1,
                                              double m1,
                                              const Vector7& lambda,
                                              double t0,
                                              double ti,
                                              /* OUTPUTS */
                                              double &finalTime,
                                              Vector3d& rf,
                                              Vector3d& vf,
                                              double& mf, 
                                              Vector7& lambdaf) {
    return getTrajectory(r1, v1, m1, lambda, t0, ti, 0, trajectoryTime, trajectoryState, finalTime, rf, vf, mf);
}
// ---------------------------------------------------------------------
/** Integrate state dynamic equation, and state transition matrix
 * simulteneously.
 *
 * @param tau independant variable integration
 * @param X   state vector at tau
 * @param Xdot state derivative at tau
 *
 */
// ---------------------------------------------------------------------
void TrajectoryPropageRtbpWithCostate::DiffEqn(double tau, const double* X, double* Xdot) {
    CommonVariables cV;
    if (dynamicalModel->ComputeCommonVariables(tau, X, cV)) {
        dynamicalModel->GetStateDynamics(tau, cV, Xdot);
        dynamicalModel->GetCostateDynamics(tau, cV, Xdot);
    }

//printf("Xdot= %f %f %f %f %f %f %f\n", Xdot[0], Xdot[1], Xdot[2], Xdot[3], Xdot[4], Xdot[5], Xdot[6]);
//printf("Ldot= %f %f %f %f %f %f %f\n", Xdot[0+7], Xdot[1+7], Xdot[2+7], Xdot[3+7], Xdot[4+7], Xdot[5+7], Xdot[6+7]);

    // auxiliary equations
    if (nDimension > 2 * X_SIZE) {
        for (uint i = 2 * X_SIZE; i < nDimension; ++i) {
            Xdot[i] = auxiliaryEquationValue.at(i - X_SIZE);
        }
        auxiliaryEquationValue.clear();
    }
}
// ---------------------------------------------------------------------
/**
 */
void TrajectoryPropageRtbpWithCostate::getStateDynamics(double tau, const double* state, const double* uxyz, double* Xdot) {
    CommonVariables cV;
    if (dynamicalModel->ComputeCommonVariablesForStateOnly(tau, state, cV)) {
        if (uxyz != NULL) {
            cV.U << uxyz[0], uxyz[1], uxyz[2];
        }
        dynamicalModel->GetStateDynamics(tau, cV, Xdot);
    }

    // additional perturbations
    if(forces.size() > 0) {
        GenericDate date(tau);
		Vector3dExt pos; pos << state[0], state[1], state[2];
		Vector3dExt vel; vel << state[3], state[4], state[5];

        for(std::vector<const Force*>::iterator f = forces.begin(); f != forces.end(); ++f) {
            Vector3dExt facc = (*f)->getAcceleration(date, pos, vel);
            Xdot[3] += facc(0);
            Xdot[4] += facc(1);
            Xdot[5] += facc(2);
        }
    }
}
// ---------------------------------------------------------------------
/** DYNAMIC
 * dynamic equation, and state transition matrix
 * simulteneously
 */
void TrajectoryPropageRtbpWithCostate::getCoStateDynamics(double tau, const double* state, const double* uxyz, double* Ldot) {
    CommonVariables cV;
    if (dynamicalModel->ComputeCommonVariables(tau, state, cV)) {
        if (uxyz != NULL) {
            cV.U << uxyz[0], uxyz[1], uxyz[2];
        }
        dynamicalModel->GetCostateDynamics(tau, cV, Ldot);
    }
}
// ---------------------------------------------------------------------
/** DYNAMIC
 * Integrate state dynamic equation, and state transition matrix
 * simultaneously
 *
 * @param x  state vector, 6x1
 * @param uqsw  control vector, 3x1
 * @param mu  gravitational constant
 * @param J0  J2 zonal term
 * @param dx  state time-derivative
 */
// ---------------------------------------------------------------------
void TrajectoryPropageRtbpWithCostate::getStateDynamics(double tau,
                                                  const double* X,
                                                  double mass,
                                                  const double* uxyz,
                                                  double mu,
                                                  double J0,
                                                  std::vector<Force*> forces,
                                                  double qfuel,
                                                  double* Xdot,
                                                  bool withmass) {
    throw HelioLibException("getStateDynamics()", "Not implemented", "Not implemented");
}
/* ---------------------------------------------------------------------
 * DYNAMIC
 * Integrate state dynamic equation, and state transition matrix
 * simulteneously
 */
void TrajectoryPropageRtbpWithCostate::getCoStateDynamics(double tau,
                                                    const double* X,
                                                    const double* uxyz,
                                                    double mu,
                                                    double J0,
                                                    double q,
                                                    double* Ldot) {
    throw HelioLibException("getCoStateDynamics()", "Not implemented", "Not implemented");
}

/* --------------------------------------------------------------------- */
void TrajectoryPropageRtbpWithCostate::toFile(std::string filename,
                                        double a_m,
                                        double a_kg) {
    FILE *fid = fopen(filename.c_str(), "w");
    toFile(fid, trajectoryTime, trajectoryState, frame, frame, a_m, a_s, a_kg);
}
// ---------------------------------------------------------------------
void TrajectoryPropageRtbpWithCostate::toFile(std::string filename,
                                        const Frame *frameOut,
                                        double a_m,
                                        double a_kg) {
    FILE *fid = fopen(filename.c_str(), "w");
    toFile(fid, trajectoryTime, trajectoryState, frame, frameOut, a_m, a_s, a_kg);
}

/* --------------------------------------------------------------------- */
void TrajectoryPropageRtbpWithCostate::toFile(FILE* fid,
                                        std::vector<double>& trajectoryTime,
                                        std::vector<Vector7>& trajectoryState,
                                        const Frame *frameIn,
                                        const Frame *frameOut,
                                        double a_m,
                                        double a_s,
                                        double a_kg) {
    if (fid <= 0) {
        throw HelioLibException("Invalid file pointer");
    }

    double a_ms = a_m / a_s;

    fprintf(fid,
            "%%\n%% t (MJD2000)  X            Y            Z            Vx            Vy            Vz            m");
    fprintf(fid, "            lx            ly            lz            lvx            lvy            lvz            "
                 "lm          |u|\n");

    int iout = trajectoryTime.size();
    for (int i = 0; i < iout; i++) {
        double uampl = 0;
        Vector7 vec = trajectoryState.at(i);

        double tmjd2000 = trajectoryTime.at(i) * a_s / 86400.;
        Vector3dExt fpos, fvel;
        if ((frameIn != NULL) && (frameOut != NULL)) {
            GenericDate curDate(tmjd2000, MJD_2000);
            FrameTransform transform = frameIn->getTransformTo(frameOut, curDate);

            // transform position
            Vector3dExt position; position << vec(0), vec(1), vec(2);
            Vector3dExt velocity; velocity << vec(3), vec(4), vec(5);
            CartesianCoordinates pvCoords(position, velocity);
            CartesianCoordinates pvCoordsInFrame = transform.transform(pvCoords);

            fpos = pvCoordsInFrame.getPosition();
            fvel = pvCoordsInFrame.getVelocity();
        } else {
            fpos << vec(0), vec(1), vec(2);
            fvel << vec(3), vec(4), vec(5);
        }

        fprintf(fid,
                "%12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %12.8f %16.8f %16.8f %16.8f %16.8f %16.8f %16.8f "
                "%16.8f %12.8f\n",
                tmjd2000,  // t
                fpos(0) * a_m,
                fpos(1) * a_m,
                fpos(2) * a_m,  // R
                fvel(0) * a_ms,
                fvel(1) * a_ms,
                fvel(2) * a_ms,  // V
                vec(6) * a_kg,
                0.,
                0.,
                0.,  // vec(7), vec(8), vec(9),    /* lambda_R */
                0.,
                0.,
                0.,  // vec(10), vec(11), vec(12),    /* lambda_V */
                0.,  // vec(13), /* lambda_m */
                uampl);  // amplitude of the thrust
    }

    fprintf(fid, "\n%%END\n");
}
