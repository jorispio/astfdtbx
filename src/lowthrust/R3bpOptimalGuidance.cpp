// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2015 Joris Olympio
 *
 *   HelioLib is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   HelioLib is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with HelioLib.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "R3bpOptimalGuidance.hpp"
#include "HelioLib.hpp"

#define MIN_MAX2 -1. // to change sign of switching function TO REMOVE

// ---------------------------------------------------------------------
/** Creator.
 */
// ---------------------------------------------------------------------
R3bpOptimalGuidance::R3bpOptimalGuidance(const TDynamicsBase* dynamic)
    : TGuidanceControlBase(dynamic)
{
}

// ---------------------------------------------------------------------
/** Compute and return the optimal control direction.
 */
// ---------------------------------------------------------------------
Vector3 R3bpOptimalGuidance::GetControlDirection(const real_type* x, const real_type* lambda)
{
    Vector3 U;
    U << lambda[3], lambda[4], lambda[5];
    U.normalize();
    return -U;
}
// ---------------------------------------------------------------------
/** Derivative of the control vector, with respect to time
 * @param x      state vector
 * @param lambda costate vector
 * @param x_dot  time derivative
 * @return control time derivative
 */
// ---------------------------------------------------------------------
Vector3 R3bpOptimalGuidance::GetControlDirectionTimeDeriv(const real_type* x,
                                                                     const real_type* lambda,
                                                                     const real_type* xdot)
{
    Vector3 Udot;
    Udot.setZero();
    return Udot;
}

// ---------------------------------------------------------------------
/** Derivative of optimal control direction with respect to state.
 */
// ---------------------------------------------------------------------
void R3bpOptimalGuidance::ControlDerivativeState(const CommonVariables& cV, Matrix3x7& dU_State)
{
    dU_State.setZero();
}

// ---------------------------------------------------------------------
/** Derivative of the optimal control direction with respect to costate.
 *
 */
// ---------------------------------------------------------------------
void R3bpOptimalGuidance::ControlDerivativeLambda(const CommonVariables& cV, Matrix3x7& dU_Lambda)
{
    dU_Lambda.setZero();
    Vector3 lambdav = cV.lambda.segment(3, 3);
    double norm_lambdav2 = lambdav.dot(lambdav);
    double norm_lambdav = sqrt(norm_lambdav2);
    // derivative of d(-lambda_V / |lambda_V|) / dt
    Vector3 dnorm_lambdav = lambdav / norm_lambdav;
    dU_Lambda(0, 3) = -1. / (norm_lambdav) + lambdav(0) * dnorm_lambdav(0) / norm_lambdav2;
    dU_Lambda(0, 4) = lambdav(0) * dnorm_lambdav(1) / norm_lambdav2;    
    dU_Lambda(0, 5) = lambdav(0) * dnorm_lambdav(2) / norm_lambdav2;        
    dU_Lambda(1, 3) = lambdav(1) * dnorm_lambdav(0) / norm_lambdav2;
    dU_Lambda(1, 4) = -1. / (norm_lambdav) + lambdav(1) * dnorm_lambdav(1) / norm_lambdav2;
    dU_Lambda(1, 5) = lambdav(1) * dnorm_lambdav(2) / norm_lambdav2;        
    dU_Lambda(2, 3) = lambdav(2) * dnorm_lambdav(0) / norm_lambdav2;
    dU_Lambda(2, 4) = lambdav(2) * dnorm_lambdav(1) / norm_lambdav2;    
    dU_Lambda(2, 5) = -1. / (norm_lambdav) + lambdav(2) * dnorm_lambdav(2) / norm_lambdav2;
}
// ---------------------------------------------------------------------
/** Get the switching function value.
 *
 * @param  x      state vector
 * @param lambda costate vector
 * @param common
 */
// ---------------------------------------------------------------------
real_type R3bpOptimalGuidance::GetSwitchingFunction(real_type t,
                                                        const real_type* state,
                                                        const real_type* lambda,
                                                        const CommonVariables& common)
{
    return 0;
}

// ---------------------------------------------------------------------
/** Derivative of the Smoothing function, with respect to
 * mass, lm, lV and time.
 * On purpose, to ease debugging and reading of the code, we do not perform
 * factorisation/simplification of the equations. The overcost should be marginal.
 */
// ---------------------------------------------------------------------
void R3bpOptimalGuidance::GetSwitchingFunctionDerivatives(real_type t,
                                                              const real_type* state,
                                                              const Vector7& lambda,
                                                              const CommonVariables& common,
                                                              const Matrix7& jacobian,
                                                              SwitchingFunctionDerivatives& sd)
{
    sd.SwitchFun_dt = 0;
    sd.SwitchFun_dstate.setZero();
    sd.SwitchFun_dm = sd.SwitchFun_dstate(6);
	//
	sd.SwitchFun_dlambda.setZero();
	sd.SwitchFun_dlV.setZero();
	sd.SwitchFun_dlm = 0;
	// sd.SwitchFun_dstate_dlambda;
	//
	sd.SwitchFun_dm2 = 0;
	sd.SwitchFun_dm_dlV.setZero();
    sd.SwitchFun_dm_dlm = 0.;
    sd.SwitchFun_dm_dt = 0;
}

// ---------------------------------------------------------------------
/** Time Derivative of the Smoothing function.
 * By construction, we need the dynamics to compute those derivatives. Therefore,
 * it is not possible to compute them at the same time as other derivatives since
 * the dynamics (in particular costate) requires some derivatives of the switching function.
 * 
 * On purpose, to ease debugging and reading of the code, we do not perform
 * factorisation/simplification of the equations. The overcost should be marginal.
 * 
 * @param t current time
 * @param state state vector
 * @param lambda costate vector
 * @param x_dot time derivative of state/costate
 * @param jacobian jacobian of the state dynamic vector
 * @param sd switching function derivatives
 */
// ---------------------------------------------------------------------
void R3bpOptimalGuidance::GetSwitchingFunctionTimeDerivatives(real_type t,
                                                              const real_type* state,
                                                              const Vector7& lambda,
                                                              const CommonVariables& common,
                                                              const real_type* x_dot,
                                                              const Matrix7& jacobian,
                                                              SwitchingFunctionDerivatives& sd)
{
	// time derivative of the switching function
    sd.SwitchFun_dt = 0;
    sd.SwitchFun_dm_dt = 0;
}
