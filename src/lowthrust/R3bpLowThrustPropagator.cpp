// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "lowthrust/R3bpLowThrustPropagator.hpp"

#include <string>
#include <vector>

#include "R3bpProblemException.hpp"
#include "R3bpOptimalGuidance.hpp"
#include "DynamicsEllipticThreeBodyProblemWithCostate.hpp"
#include "StateTransitionMatrixRtbpForLowThrust.hpp"

//#define DEBUG

// -------------------------------------------------------------------------
R3bpLowThrustPropagator::R3bpLowThrustPropagator(double mu_ratio, const ThrusterData& thruster_data, double ecc)
    : TrajectoryPropageRtbpWithCostate(mu_ratio, ecc, thruster_data), mu_ratio(mu_ratio), propulsion_system(thruster_data) {
    
    mThrusterNep = new ThrusterNep(thruster_data);        
    dynamicalModel->SetThruster(mThrusterNep);
    
    R3bpOptimalGuidance* optmalControl = new R3bpOptimalGuidance(dynamicalModel);
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->SetControlInstance(optmalControl);
}
// -------------------------------------------------------------------------
R3bpLowThrustPropagator::~R3bpLowThrustPropagator() {
    delete mThrusterNep;
    mThrusterNep = NULL;
}
// -------------------------------------------------------------------------
std::vector<Vector7> R3bpLowThrustPropagator::propagate(const double x[7], const Vector7& lambda, double tspan[2], double timeStep) {
    Vector3d r1; r1 << x[0], x[1], x[2];
    Vector3d v1; v1 << x[3], x[4], x[5];
    double tf;
    Vector3d rf;
    Vector3d vf;
    double mf;
    Vector7 lambdaf;
    int n = getTrajectory(r1, v1, x[6], lambda, tspan[0], tspan[1], timeStep,
                                              trajectory_time, trajectory_state,
                                              tf, rf, vf, mf);
    return trajectory_state;
}
// -------------------------------------------------------------------------
S14MATRIX R3bpLowThrustPropagator::propagate_state_transition_matrix(const double x[7], const Vector7& lambda, double tspan[2], double timeStep) {
    Vector3d r1; r1 << x[0], x[1], x[2];
    Vector3d v1; v1 << x[3], x[4], x[5];
    double mass = x[6];
    Matrix7 STM1, STM2, STM3, STM4;
    S14MATRIX STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    Vector7 lambdaf;
    
#ifdef DEBUG    
    std::cout << "Derivative checking" << std::endl;
    const double *p = &lambda(0);
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->CheckDerivatives(0, x, p);
    std::cout << "Derivative checking complete" << std::endl;
#endif // DEBUG

    StateTransitionMatrixRtbpForLowThrust *propagator = new StateTransitionMatrixRtbpForLowThrust(constMu, dynamicalModel);
    propagator->getSTM(r1, v1, mass, lambda,
               tspan[0], tspan[1],
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);

    // when time is a parameter
    CommonVariables common;
    real_type xf[14];
    for (int idx=0; idx<3; ++idx) {
        xf[idx] = rf[idx];
        xf[idx+3] = vf[idx];
        xf[idx+7] = 0;
        xf[idx+10] = 0;
    }
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->ComputeCommonVariablesForStateOnly(0, xf, common);
    real_type x_dot[14];
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->GetStateDynamics(0, common, x_dot);
    Map<Vector7> m_x_dot(x_dot);
    STM.block(6,0,7,1) = m_x_dot;               

    delete propagator;

    return STM;
}
// -------------------------------------------------------------------------
Vector14 R3bpLowThrustPropagator::compute_time_derivative(const Vector7& state, const Vector7& lambda) {
    CommonVariables common;
    real_type xi[14];    
    memcpy(xi, &state[0], 7 * sizeof(double));
    memcpy(xi+7, &lambda[0], 7 * sizeof(double));    
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->ComputeCommonVariables(0, xi, common);
    real_type x_dot[14];
    ((EllipticThreeBodyProblemWithCostate*) dynamicalModel)->GetStateDynamics(0, common, x_dot);
    Map<Vector14> m_x_dot(x_dot);
    return m_x_dot;  
}    
// -------------------------------------------------------------------------
void R3bpLowThrustPropagator::get_ephemeris_file(std::string filename) {
    FILE *fid = fopen(filename.c_str(), "wt");
    if (fid < 0) {
        throw R3bpProblemException("Cannot create file");
    }

    const Frame *frameIn = new GcrfFrame();
    const Frame *frameOut = new GcrfFrame();

    toFile(fid, trajectory_time, trajectory_state, frameIn, frameOut, 1., 1., 1.);

    delete frameIn;
    delete frameOut;
    fclose(fid);
}
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
