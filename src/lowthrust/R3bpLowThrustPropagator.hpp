// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LOWTHRUST_PROPAGATOR_HPP__
#define __R3BP_LOWTHRUST_PROPAGATOR_HPP__

#include <ctime>
#include <string>
#include <vector>

#include "lowthrust/propulsion.hpp"
#include "ThrusterNep.hpp"
#include "TrajectoryPropageRtbpWithCostate.hpp"

#include "HelioLib.hpp"

class R3bpLowThrustPropagator : public TrajectoryPropageRtbpWithCostate {
 private:
    double mu_ratio;
    std::vector<double> trajectory_time;
    std::vector<Vector7> trajectory_state;
    ThrusterData propulsion_system;

    ThrusterNep* mThrusterNep;

 public:
    R3bpLowThrustPropagator(double mu_ratio, const ThrusterData& propulsion_system, double ecc = 0);
    ~R3bpLowThrustPropagator();

    std::vector<Vector7> propagate(const double x[7], const Vector7& lambda, double tspan[2], double timeStep);
    S14MATRIX propagate_state_transition_matrix(const double x[7], const Vector7& lambda, double tspan[2], double timeStep);

    Vector14 compute_time_derivative(const Vector7& state, const Vector7& lambda);

    std::vector<double> get_time() { return trajectory_time; }
    void get_ephemeris_file(std::string filename);
};

// ---------------------------------------------------------------------------
#endif  // __R3BP_LOWTHRUST_PROPAGATOR_HPP__
