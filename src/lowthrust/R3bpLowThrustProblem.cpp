// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "R3bpLowThrustProblem.hpp"


// ---------------------------------------------------------------------------
std::vector<double> R3bpLowThrustProblemConstraints::compute_transversality_conditions(Vector7& state, Vector7& costate, R3bpLowThrustTerminalConstraint constraint_type) {
    std::vector<double> cstr;

    if ((constraint_type == E_POSITION) || (constraint_type == E_RENDEZVOUS)) {
        cstr.push_back(state(0) - final_pos(0));
        cstr.push_back(state(1) - final_pos(1));
        cstr.push_back(state(2) - final_pos(2));
    }

    if (constraint_type == E_RENDEZVOUS) {
        cstr.push_back(state(3) - final_vel(0));
        cstr.push_back(state(4) - final_vel(1));
        cstr.push_back(state(5) - final_vel(2));    
    } 
    else if (constraint_type == E_POSITION) {
        cstr.push_back(costate(3));
        cstr.push_back(costate(4));
        cstr.push_back(costate(5));
    }       

    return cstr;
}

// ---------------------------------------------------------------------------
double R3bpLowThrustProblemConstraints::compute_time_optimal_condition(Vector7& state, Vector7& dstate_dt, Vector7& costate) {
    // TODO
    return 0;
}
// ---------------------------------------------------------------------------
void compute_jacobian(Vector7& state, Vector7& costate, MATRIXVAR &jacobian) {
    // TODO
}
    
// ---------------------------------------------------------------------------
R3bpLowThrustProblem::R3bpLowThrustProblem(double mu_ratio, const ThrusterData& propulsion_system, double ecc) 
    : R3bpLowThrustProblemConstraints(), mu_ratio(mu_ratio), propulsion_system(propulsion_system) { 


}


// ---------------------------------------------------------------------------
R3bpLowThrustProblemSolution R3bpLowThrustProblem::solve(const Vector7& x0, const Vector7& xf, double tof, Vector7& lambda0) {
    R3bpLowThrustProblemSolution sol;
    sol.x = lambda0;
    

    //StateTransitionMatrixRtbpWithCostate
    

    return sol;
}
// ---------------------------------------------------------------------------


