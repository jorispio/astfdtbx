// $Id$
// ---------------------------------------------------------------------
/* --------------------------------------------------------------------------- *
 *   This file is part of HelioLib.
 *
 *   Copyright (C) 2010-2015 Joris Olympio
 *
 * This Source Code Form is subject to the terms of the Mozilla
 * Public License v. 2.0. If a copy of the MPL was not distributed
 * with this file, You can obtain one at http://mozilla.org/MPL/2.0/
 */
#ifndef __R3BP_OPTIMAL_CONTROL_HPP__
#define __R3BP_OPTIMAL_CONTROL_HPP__

#include "propagation/dynamics/DynamicsBase.hpp"
#include "propagation/control/GuidanceControlBase.hpp"

class R3bpOptimalGuidance : public TGuidanceControlBase
{
private:
public:
    /** Creator. */
    R3bpOptimalGuidance(const TDynamicsBase* dynamic);
    ~R3bpOptimalGuidance() {}

    /** Compute and return the optimal control */
    Vector3 GetControlDirection(const real_type* x, const real_type* lambda);

    /** */
    real_type
    GetSwitchingFunction(real_type t, const real_type* x, const real_type* lambda, const CommonVariables& common);

    /** */
    void GetSwitchingFunctionDerivatives(real_type t,
                                         const real_type* state,
                                         const Vector7& lambda,
                                         const CommonVariables& common,
                                         const Matrix7& jacobian,
                                         SwitchingFunctionDerivatives& sd);

    /** Get the switching function time erivatives */
    void GetSwitchingFunctionTimeDerivatives(real_type t,
                                                              const real_type* state,
                                                              const Vector7& lambda,
                                                              const CommonVariables& common,
                                                              const real_type* x_dot,
                                                              const Matrix7& jacobian,
                                                              SwitchingFunctionDerivatives& sd);
    /** */
    void ControlDerivativeState(const CommonVariables& cV, Matrix3x7& QSW_State);

    /** */
    void ControlDerivativeLambda(const CommonVariables& cV, Matrix3x7& QSW_Lambda);

    /** */
    Vector3 GetControlDirectionTimeDeriv(const real_type* x, const real_type* lambda, const real_type* xdot);
};

#endif  // __R3BP_OPTIMAL_CONTROL_HPP__
