/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LibrationPoint.hpp"

#include <vector>

#include "R3bpProblemException.hpp"
#include "SpaceMechanicsToolBox.hpp"

#define eps 1e-11

// ------------------------------------------------------------------------
/**
 * Calculate ratio of Libration point distance from closest primary to distance
 * between two primaries,
 *
 */
double LibrationPointProperty::gamma() {
    if (is_gamma_computed) {
        return __cached_gamma;
    }

    double mu2 = 1 - mu;
    // get the 5 positions
    // format: {const x x^2 x^3 x^4 x^5}
    // And solve for roots of quintic polynomial
    std::vector<std::complex<double> > roots;
    if (id == LibrationPointId::L1) {
        double poly1[] = {-mu, 2*mu, -mu, (3-2*mu), -1*(3-mu), 1};
        roots = (new Polynomial(poly1, 6))->getRoots();
    }
    else if (id == LibrationPointId::L2) {
        double poly2[] = {-mu, -2*mu, -mu, (3-2*mu), (3-mu), 1};
        roots = (new Polynomial(poly2, 6))->getRoots();
    }
    else if (id == LibrationPointId::L3) {
        double poly3[] = {-mu2, -2*mu2, -mu2, (1+2*mu), (2+mu), 1};
        roots = (new Polynomial(poly3, 6))->getRoots();
    } else {
        throw R3bpProblemException("Gamma is not computed for L4 and L5.");
    }

    // keep only the real root (there are also two complex pairs)
    double gamma = 0;
    for (int k = 0; k < 5; ++k) {
       if (fabs(roots[k].imag()) < eps) gamma = roots[k].real();
    }

    is_gamma_computed = true;
    __cached_gamma = gamma;

    assert(gamma < 1);
    return gamma;
}

// ------------------------------------------------------------------------
/** Get position on the synodic line with respect to baryccenter.
 * This returns the same result as L[0], for L1, L2 and L3.
 * This function is undefined for L4 and L5.
 */
double LibrationPointProperty::gl() {
    if (id == LibrationPointId::L1) {
        return 1 - gamma() - mu;
    }
    else if (id == LibrationPointId::L2) {
        return 1 + gamma() - mu;
    }
    else if (id == LibrationPointId::L3) {
        return -gamma() - mu;
    } else {
        throw R3bpProblemException("GL is not computed for L4 and L5.");
        return 0;
    }
}
// ------------------------------------------------------------------------
/**
 *
 */
double LibrationPointProperty::calc_c2() {
    double g = gamma();  // dimensionality constant
    if (id == LibrationPointId::L1) {
        return mu / (g * g * g) + (1 - mu) / ((1 - g) * (1 - g) * (1 - g));
    }
    else if (id == LibrationPointId::L2) {
        return mu / (g * g * g) + (1 - mu) / ((1 + g) * (1 + g) * (1 + g));
    }
    else if (id == LibrationPointId::L3) {
        return (1 - mu) / (g * g * g) + (mu / ((1 + g) * (1 + g) * (1 + g)));
    } else {
        throw R3bpProblemException("C2 is not computed for L4 and L5.");
        return 0;
    }
}


namespace libration_point {
// -------------------------------------------------------------------------
/** Calculate the libration point position, and the Jacobi constants
 * inputs:
 *   r1      position of the 1st body
 *   r2      position of the 2nd body
 *   m1      mass of the 1st body
 *   m2      mass of the 2nd body
 *
 * outputs:
 *   L       Libration point position, consistent with distance unit of r1 and r2
 *   Cj      Jacobi constants
 *
 * Note that mu is
 *     mu = M2/(M1+M2)
 * with M1 the 'big' primary and 'M2' the small primary.
 *
 * non-dimensionnal constants
 *   G = G1 + G2;  // adimensionnal gravitationnal constant
 *   M = m1 + m2;  // adimmensionnal mass (kg)
 *   R = r2 + r1;  // adimmensionnal distance (m)
 */
void computeLibrationPoints(double r1, double r2, double m1, double m2, double Lxy[5][2], double Cj[5]) {
    double mu = m2 / (m1 + m2);
    computeLibrationPoints(mu, Lxy, Cj);
    double d = r1 + r2;

    for (int i = 0; i < 5; ++i) {
        for (int j = 0; j < 3; ++j) {
            Lxy[i][j] *= d;
        }
    }
}
// -------------------------------------------------------------------------
/** Calculate the libration point position, and the Jacobi constants
 * inputs:
 *   mu      mu ratio
 *
 * outputs:
 *   L       Libration point position
 *   Cj      Jacobi constants
 *
 * Note that mu is
 *     mu = M2/(M1+M2)
 * with M1 the 'big' primary and 'M2' the small primary.
 *
 */
void computeLibrationPoints(double mu, double Lxy[5][2], double Cj[5]) {
    double mu_3 = mu / 3.;
    double mu_3_pow13 = pow(mu_3, 1./3.);
    double mu_3_pow23 = pow(mu_3, 2./3.);
    double r2 = 1. - mu;

    // Equilibrium point (L1, L2, L3)
    // positioned at (xi, 0, 0)
    // assuming that the distance between (1) and (2) in normalised to 1.
    Lxy[0][0] = r2 - mu_3_pow13 - 1/3. * mu_3_pow23 + 28./9. * mu_3;
    Lxy[0][1] = 0;
    Lxy[1][0] = r2 + mu_3_pow13 - 1/3. * mu_3_pow23 + 26./9. * mu_3;
    Lxy[1][1] = 0;
    Lxy[2][0] =-1 - 5./4. * mu_3 + 1127./768. * mu_3 * mu_3 * mu_3;
    Lxy[2][1] = 0;

    // Equilibrium point (L4, L5)
    double x_tri = .5 - mu;  // -(x1+x2)/2; % from r1 = r2 = 1 & z = 0
    double y_tri = sqrt(3) / 2.;  // assuming omega = 1
    Lxy[3][0] = x_tri;
    Lxy[3][1] = y_tri;
    Lxy[4][0] = x_tri;
    Lxy[4][1] =-y_tri;

    // Jacobi constant at the equilibrium point
    // these are the critical value for which the velocity surface change
    Cj[0] = 3. + 9. * mu_3_pow23 - 11. * mu_3;
    Cj[1] = 3. + 9. * mu_3_pow23 - 7. * mu_3;
    Cj[2] = 3. + 2. * mu - 49./48. * mu * mu;
    Cj[3] = 3 - mu * (1 - mu);
    Cj[4] = 3 - mu * (1 - mu);
}

double computeGravitationalConstantRatio(double m1, double m2) {
    return m2 / (m1 + m2);
}
}  // namespace libration_point
