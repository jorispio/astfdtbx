// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "Manifold.hpp"

#include <Eigen/Eigenvalues>
#include <stdlib.h>

#include <cmath>
#include <string>
#include <vector>

#include "propagator/R3bpPropagator.hpp"
#include "SpaceMechanicsToolBox.hpp"

#define TOL  1.e-14


// ------------------------------------------------------------------------
/**
 * Find all the eigenvectors locally spanning the 3 subspaces for the phase
 * space in an infinitesimal region around an equilibrium point
 *
 */
bool EigenSpace::getManifolds(const Vector6 &ep, double mu_ratio,
                /* outputs */
                std::vector<std::complex<double> > &Es, std::vector<VectorXcd> &Vs,
                std::vector<std::complex<double> > &Eu, std::vector<VectorXcd> &Vu,
                std::vector<std::complex<double> > &Ec, std::vector<VectorXcd> &Vc) {
    // get the Jacobian matrix Df
    MATRIXVAR Df = MATRIXVAR::Zero(6, 6);
    CommonVariables cv;
    const real_type *X = &ep(0);

    EllipticThreeBodyProblem *dynamics = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    if (dynamics->ComputeCommonVariablesForStateOnly(0, X, cv)) {
        real_type x_dot[7];
        dynamics->GetJacobianDynamics(0, X, cv, x_dot, Df);

        // find the basis for each point type: stable, unstable, center
        bool res = getManifolds(Df, true, Es, Vs, Eu, Vu, Ec, Vc);

        delete dynamics;
    // i.e., local manifold approximations
    // give the +y directed column vectors
//    if (Vs(2)<0) Vs=-Vs;
//    if (Vu(2)<0) Vu=-Vu;
        return res;
    }

    printf("Cannot compute dynamical parameters.\n");
    return false;
}

// ------------------------------------------------------------------------

bool EigenSpace::getCenterManifolds(const Vector6 &ep, double mu_ratio,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center) {
    MATRIXVAR Df = MATRIXVAR::Zero(6, 6);
    CommonVariables cv;
    real_type X[7];
    X[0] = ep(0); X[1] = ep(1); X[2] = ep(2); X[3] = ep(3); X[4] = ep(4); X[5] = ep(5); X[6] = 1;

    EllipticThreeBodyProblem *dynamics = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    if (dynamics->ComputeCommonVariablesForStateOnly(0, X, cv)) {
        real_type x_dot[7];
        dynamics->GetJacobianDynamics(0, X, cv, x_dot, Df);

        // find the basis for each point type: stable, unstable, center
        bool res = getCenterManifolds(Df, true, cn, v_center);

        delete dynamics;
    // i.e., local manifold approximations
    // give the +y directed column vectors
//    if (Vs(2)<0) Vs=-Vs;
//    if (Vu(2)<0) Vu=-Vu;
        return res;
    }

    printf("Cannot compute dynamical parameters.\n");
    return false;
}
// ------------------------------------------------------------------------
/**
 * Compute the eigenvalues and eigenvectors of the matrix A spanning the
 * three local subspaces <Es,Eu,Ec> (stable, unstable, center)
 * where A is MxM and s+u+c=M, which
 * locally approximate the invariant manifolds <Ws,Wu,Wc>
 *
 * discrete = 1, if discrete time system
 */
bool EigenSpace::getManifolds(const Matrix6 &A, bool isContinuous,
                    std::vector<std::complex<double> > &sn, std::vector<VectorXcd> &v_stable,
                    std::vector<std::complex<double> > &un, std::vector<VectorXcd> &v_unstable,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center,
                    double tol) {
    // obtain eigenvectors (V) and eigenvalues (D)
    ComplexEigenSolver<MatrixXcd> ces(A);
    VectorXcd eigenValues = ces.eigenvalues();
    removeInfinitesimals(eigenValues);
    MatrixXcd eigenvectors = ces.eigenvectors();

    if (isContinuous) {
        for (uint idxEigValue = 0; idxEigValue < eigenValues.size(); ++idxEigValue) {
            std::complex<double> d = eigenValues(idxEigValue);
            double dreal = d.real();

            // stable: eigen value |lambda| < 0
            if (dreal < 0) {
                sn.push_back(d);
                VectorXcd ws = eigenvectors.col(idxEigValue);
                ws /= ws.norm();
                v_stable.push_back(ws);
            }

            // unstable: eigen value |lambda| > 0
            else if (dreal > 0) {
                un.push_back(d);
                VectorXcd wu = eigenvectors.col(idxEigValue);
                wu /= wu.norm();
                v_unstable.push_back(wu);
            }

            // center: eigen value |lambda| = 0
            else /* (abs(dreal) == 0) */ {
                cn.push_back(d);
                VectorXcd wc = eigenvectors.col(idxEigValue);
                v_center.push_back(wc);
            }
        }
    }
    else {
        // arbitrary small displacement for use in discrete case
        for (uint idxEigValue = 0; idxEigValue < eigenValues.size(); ++idxEigValue) {
            std::complex<double> d = eigenValues(idxEigValue);
            double dreal = d.real();

            // stable: eigen value |lambda| < 1
            if (dreal < 1 - tol) {
                sn.push_back(d);
                VectorXcd ws = eigenvectors.col(idxEigValue);
                v_stable.push_back(ws / ws.norm());
            }

            // unstable: eigen value |lambda| > 1
            else if (dreal > 1 + tol) {
                un.push_back(d);
                VectorXcd wu = eigenvectors.col(idxEigValue);
                v_unstable.push_back(wu / wu.norm());
            }

            // center: eigen value |lambda| = 1
            // describe tangent, energy variation, and quasi-halo
            else /* if (abs(dreal - 1) <= tol) */ {
                cn.push_back(d);
                VectorXcd wc = eigenvectors.col(idxEigValue);
                v_center.push_back(wc);
            }
        }
    }

    return true;
}

// ------------------------------------------------------------------------
/**
 * Compute the eigenvalues and eigenvectors of the matrix A spanning the
 * three local subspaces <Es,Eu,Ec> (stable, unstable, center)
 * where A is MxM and s+u+c=M, which
 * locally approximate the invariant manifolds <Ws,Wu,Wc>
 *
 * discrete = 1, if discrete time system
 */
bool EigenSpace::getCenterManifolds(const Matrix6 &A, bool isContinuous,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center, double tol) {
    // obtain eigenvectors (V) and eigenvalues (D)
    ComplexEigenSolver<MatrixXcd> ces(A);
    VectorXcd eigenValues = ces.eigenvalues();
    removeInfinitesimals(eigenValues);
    MatrixXcd eigenvectors = ces.eigenvectors();

    if (isContinuous) {
        for (uint idxEigValue = 0; idxEigValue < eigenValues.size(); ++idxEigValue) {
            std::complex<double> d = eigenValues(idxEigValue);
            double dreal = d.real();

            // center: eigen value |lambda| = 0
            if (fabs(dreal) == 0)  {
                cn.push_back(d);
                VectorXcd wc = eigenvectors.col(idxEigValue);
                v_center.push_back(wc);
            }
        }
    }
    else {
        // arbitrary small displacement for use in discrete case
        for (uint idxEigValue = 0; idxEigValue < eigenValues.size(); ++idxEigValue) {
            std::complex<double> d = eigenValues(idxEigValue);
            double dreal = d.real();

            // center: eigen value |lambda| = 1
            if (fabs(dreal - 1) <= tol) {
                cn.push_back(d);
                VectorXcd wc = eigenvectors.col(idxEigValue);
                v_center.push_back(wc);
            }
        }
    }

    return true;
}

// ------------------------------------------------------------------------
bool EigenSpace::getCenterManifolds(const OrbitSolution& lissajous,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &v_center,
                    double tol) {
    return getCenterManifolds(lissajous.monodromy, true, cn, v_center, tol);
}
// ------------------------------------------------------------------------
/**
 * Compute manifolds from Lissajous orbit.
 */
bool EigenSpace::getManifolds(const OrbitSolution& orbitSolution,
                    std::vector<std::complex<double> > &sn, std::vector<VectorXcd> &Ws,
                    std::vector<std::complex<double> > &un, std::vector<VectorXcd> &Wu,
                    std::vector<std::complex<double> > &cn, std::vector<VectorXcd> &Wc,
                    double tol) {
    return getManifolds(orbitSolution.monodromy, true, sn, Ws, un, Wu, cn, Wc, tol);
}
// ------------------------------------------------------------------------
/**
 * Remove all entries in matrix A with absolute value smaller than TOL
 * where TOL is set inside cleanUpMatrix.m
 *
 */
MatrixXcd EigenSpace::removeInfinitesimals(MatrixXcd &A) {
    Index n = A.cols();
    Index m = A.rows();

    for (Index k = 0; k < n; ++k) {
       for (Index l = 0; l < m; ++l) {
           std::complex<double> value = A(k, l);
          if (fabs(value.real()) < TOL)
             A(k, l) = std::complex<double>(0, value.imag());

          if (fabs(value.imag()) < TOL)
             A(k, l) = value.real();
       }
    }

    return A;
}
// ------------------------------------------------------------------------
/**
 * Remove all entries in matrix A with absolute value smaller than TOL
 * where TOL is set inside cleanUpMatrix.m
 *
 */
VectorXcd EigenSpace::removeInfinitesimals(VectorXcd &A) {
    Index n = A.size();
    for (Index k = 0; k < n; ++k) {
        std::complex<double> value = A(k);
        if (fabs(value.real()) < TOL)
            A(k) = std::complex<double>(0, value.imag());

        if (fabs(value.imag()) < TOL)
            A(k) = value.real();
    }

    return A;
}

// -------------------------------------------------------------------------
/**
 * Compute the directions of the manifold in the neighbourhood of a Lissajous.
 *
 */
std::vector<Vector7> EigenSpace::computeDirections(const double x[7], double ti, double timeStep, double mu_ratio,
            const Vector6 &Ws,
            double displacement,
            /* outputs */
            std::vector<Vector6> &directions) {
    Propagator *propagator = new Propagator(mu_ratio);
    std::vector<Vector7> displaced_states;

    double t0_tf[2] = {0, ti};
    std::vector<Vector7> trajectory = propagator->propagate(x, t0_tf, timeStep);

    for (uint idx = 0; idx < trajectory.size(); ++idx) {
        double ti = idx * timeStep;
        double tspan[2] = {0, ti};
        Matrix7 stm = propagator->propagate_state_transition_matrix(x, tspan, -1);

        Vector6 direction = stm.block(0, 0, 6, 6) * Ws;
        direction /= direction.tail(3).norm();
        directions.push_back(direction);

        Vector7 displaced_state = trajectory.at(idx);
        displaced_state.head(6) += displacement * direction;
        displaced_states.push_back(displaced_state);
    }

    delete propagator;
    return displaced_states;
}
