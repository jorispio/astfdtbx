// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_MANEUVER_ORBIT_SEGMENT_HPP_
#define __R3BP_MANEUVER_ORBIT_SEGMENT_HPP_

#include <iostream>  // ostream

#include "segments/PatchPoint.hpp"
#include "SpaceMechanicsToolBox.hpp"

struct Segment {
    Vector3 ri;
    Vector3 vi;
    double ti;
    Vector3 rf;
    Vector3 vf;
    double tf;

    Segment() { }
    Segment(Vector3 &ri_, Vector3 &vi_, Vector3 &rf_, Vector3 &vf_, double ti_, double tf_)
            : ri(ri_), vi(vi_), ti(ti_), rf(rf_), vf(vf_), tf(tf_) {
    }
    Segment(const PatchPoint &p1, const PatchPoint &p2);

    Vector6 get_init_state() const {
        Vector6 s; s << ri, vi;
        return s;
    }

    Vector6 get_final_state() const {
        Vector6 s; s << rf, vf;
        return s;
    }

    double get_tof() const {
        return tf - ti;
    }

    friend std::ostream& operator<<(std::ostream& os, const Segment& segment);
};

#endif  // __R3BP_MANEUVER_ORBIT_SEGMENT_HPP_
