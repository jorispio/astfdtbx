// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iostream>

#include "segments/ImpulsiveManeuver.hpp"
#include "segments/OrbitSegment.hpp"
#include "segments/PatchPoint.hpp"


PatchPoint::PatchPoint(const Segment &seg1, const Segment &seg2) : deltaV(seg1.vf, seg2.vi) {
    t = seg1.tf;
    rm = seg1.rf;
    rp = seg2.ri;
}

Segment::Segment(const PatchPoint &p1, const PatchPoint &p2) {
    ri = p1.rp;
    vi = p1.deltaV.Vp;
    ti = p1.t;
    rf = p2.rm;
    vf = p2.deltaV.Vm;
    tf = p2.t;
}

std::ostream& operator<<(std::ostream& os, const Segment& segment) {
    os << "r0 = [" << segment.ri.transpose() << "] v0 = " << segment.vi.transpose() << "] rf = ["
                << segment.rf.transpose() << "] vf = [" << segment.vf.transpose()
                << "] ti=" << segment.ti << " tf" << segment.tf;
    return os;
}
