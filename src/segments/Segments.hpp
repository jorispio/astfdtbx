// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_MANEUVER_SEGMENTS_HPP_
#define __R3BP_MANEUVER_SEGMENTS_HPP_

#include <vector>

#include "OrbitSegment.hpp"


/** list of segments class. */
class Segments {
    std::vector<Segment> segments;

 public:
    explicit Segments(const std::vector<Segment> &segments_) : segments(segments_) {
    }

    ~Segments() { }

    /**
     */
    std::vector<Vector3> compute_dv() {
        std::vector<Vector3> dvs;
        auto n_segments = segments.size();
        for (auto idx = 0; idx < n_segments - 1; ++idx) {
            if (idx == 0) {
                dvs.push_back(segments.at(0).vi - segments.at(n_segments - 1).vf);
            }
            else {
                dvs.push_back(segments.at(idx + 1).vi - segments.at(idx).vf);
            }
        }

        return dvs;
    }
};

#endif  // __R3BP_MANEUVER_SEGMENTS_HPP_
