// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_MANEUVER_HPP_
#define __R3BP_MANEUVER_HPP_

#include "SpaceMechanicsToolBox.hpp"

class ImpulsiveManeuver {
    Vector3 dv;

 public:
    ImpulsiveManeuver() { }
    explicit ImpulsiveManeuver(Vector3 &dv) : dv(dv) { }
    ~ImpulsiveManeuver() { }
};


struct DeltaV {
    Vector3 Vm;
    Vector3 Vp;
    DeltaV(const Vector3 &Vm_, const Vector3 &Vp_) : Vm(Vm_), Vp(Vp_) { }
};

// ------------------------------------------------------------------------

#endif  // __R3BP_MANEUVER_HPP_
