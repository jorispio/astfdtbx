// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "propagator/R3bpPropagator.hpp"

#include <string>
#include <vector>

#include "R3bpProblemException.hpp"

// -------------------------------------------------------------------------
Propagator::Propagator(double mu_ratio, double ecc)
    : TrajectoryPropageRtbp(mu_ratio, ecc), mu_ratio(mu_ratio) {
}
// -------------------------------------------------------------------------
Propagator::~Propagator() {
}
// -------------------------------------------------------------------------
void Propagator::add_detector(SimulationEventHandler* detector) {
    SimulationEventManager *eventManager = get_event_manager();
    if (eventManager == nullptr) {
        eventManager = new SimulationEventManager();
        set_event_manager(eventManager);
    }
    eventManager->addEventHandler(detector);
    set_event_manager(eventManager);
}
// -------------------------------------------------------------------------        
std::vector<Vector7> Propagator::propagate(const Vector6& x, double tspan[2], double timeStep) {
    Vector3d r1 = x.segment(0,3);
    Vector3d v1 = x.segment(3,3);
    double tf;
    Vector3d rf;
    Vector3d vf;
    double tfbis;
    int n = getTrajectory(r1, v1, 0., tspan[0], tspan[1], timeStep,
                                              trajectory_time, trajectory_state,
                                              tf, rf, vf, tfbis);
    return trajectory_state;
}
// -------------------------------------------------------------------------
Matrix7 Propagator::propagate_state_transition_matrix(const double x[7], double tspan[2], double timeStep) {
    Vector3d r1; r1 << x[0], x[1], x[2];
    Vector3d v1; v1 << x[3], x[4], x[5];
    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    StateTransitionMatrixRtbp *propagator = new StateTransitionMatrixRtbp(constMu);
    propagator->getSTM(r1, v1,
               tspan[0], tspan[1],
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    delete propagator;
    return STM;
}
// -------------------------------------------------------------------------
void Propagator::get_ephemeris_file(std::string filename) {
    FILE *fid = fopen(filename.c_str(), "wt");
    if (fid < 0) {
        throw R3bpProblemException("Cannot create file");
    }

    const Frame *frameIn = new GcrfFrame();
    const Frame *frameOut = new GcrfFrame();

    toFile(fid, trajectory_time, trajectory_state, frameIn, frameOut, 1., 1., 1.);

    delete frameIn;
    delete frameOut;
    fclose(fid);
}
// -------------------------------------------------------------------------
std::vector<Vector7> propagation(const double x[7], const double tspan[2], double mu_ratio, double timeStep) {
    ThrusterData ThrusterDef;
    TrajectoryPropageRtbp *propagator = new TrajectoryPropageRtbp(mu_ratio, 0.);

    Vector3d r1; r1 << x[0], x[1], x[2];
    Vector3d v1; v1 << x[3], x[4], x[5];
    std::vector<double> tmesh;
    std::vector<Vector7> trajectory;
    double tf;
    Vector3d rf;
    Vector3d vf;
    double tfbis;
    int n = propagator->getTrajectory(r1, v1, 0, tspan[0], tspan[1], timeStep,
                                              tmesh, trajectory,
                                              tf, rf, vf, tfbis);
    delete propagator;
    return trajectory;
}
// -------------------------------------------------------------------------
