// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "R3bpFrame.hpp"

#include "R3BProblem.hpp"

// ---------------------------------------------------------------------------
/** Construct frame from problem definition */
R3bpFrame::R3bpFrame(double mu_ratio, double omega, Vector3 pos)
    : Frame("R3BP", NULL), posBarycenter(pos), mu_ratio(mu_ratio), omg(omega) {
    referenceDate = GenericDate();
}
// ---------------------------------------------------------------------------
/** Frame transformation that brings a point in the R3BP rotating frame to a point in the P1
 * centered "inertial frame".
 */
FrameTransform* R3bpFrame::get_frame_transformation(const GenericDate& date) const {
    // multiply by the characteristic length
    // translation to P1: (-mu, 0, 0)
    // angular velocity of rotating frame wrt "inertial frame" : omega = 1

    //Vector3 pos = problem->get_primary_point(P1);
    Vector3 vel = Vector3::Zero();
    CartesianCoordinates cartesian = CartesianCoordinates(posBarycenter, vel);
    
    double angle =-omg * (date.getMJD() - referenceDate.getMJD());
    
    // assume that the RTBP rotating plane and the (X,Y) inertial plane coincide
    RotationQuaternion rotation = RotationQuaternion(Vector3(0, 0, 1), angle);
    Vector3 omega; omega << 0, 0, omg;    
    AngularCoordinates angular = AngularCoordinates(rotation, omega);

    FrameTransform *transform = new FrameTransform("Rotating_to_Fixed Frame",
               date,
               cartesian,
               angular);
    return transform;
}
