// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_PROPAGATOR_HPP_
#define __R3BP_PROPAGATOR_HPP_

#include <ctime>
#include <string>
#include <vector>

#include "SpaceMechanicsToolBox.hpp"

class Propagator : public TrajectoryPropageRtbp {
 private:
    double mu_ratio;
    std::vector<double> trajectory_time;
    std::vector<Vector7> trajectory_state;

 public:
    Propagator(double mu_ratio, double ecc = 0);
    ~Propagator();

    void add_detector(SimulationEventHandler* detector);
    
    /** State propagation. */
    std::vector<Vector7> propagate(const double x[7], double tspan[2], double timeStep) {
        Vector6 vect_x(x);
        return propagate(vect_x, tspan, timeStep);
    }
    
    /** State propagation. */    
    std::vector<Vector7> propagate(const Vector6& x, double tspan[2], double timeStep);    
    
    /** STM propagation. */    
    Matrix7 propagate_state_transition_matrix(const double x[7], double tspan[2], double timeStep);

    std::vector<double> get_time() { return trajectory_time; }
    void get_ephemeris_file(std::string filename);
};

/** Propagate state int R3BP */
std::vector<Vector7> propagation(const double x[7], const double tspan[2], double mu_ratio, double timeStep = -1);

// ---------------------------------------------------------------------------
#endif  // __R3BP_PROPAGATOR_HPP_
