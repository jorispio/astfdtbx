// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_FRAME_HPP_
#define __R3BP_FRAME_HPP_

#include "SpaceMechanicsToolBox.hpp"


class R3bProblem;

class R3bpFrame : public Frame {
 private:
    //R3bProblem *problem;
    GenericDate referenceDate;
    
    /** RTBP barycenter. */
    Vector3 posBarycenter;
    /** RTBP mu ratio. */
    double mu_ratio;
    /** mean motion. */
    double omg;

 public:
    R3bpFrame(double mu_ratio, double omega, Vector3 pos);
    ~R3bpFrame() { }

    /** Frame transformation that brings a point in the R3BP rotating frame to a point in the P1
     * centered "inertial frame".
     */
    FrameTransform* get_frame_transformation(const GenericDate& date) const;

    FrameTransform getTransform(const GenericDate& date) const {
        return *get_frame_transformation(date);
    }
};

#endif  // __R3BP_FRAME_HPP_
