// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_AXIS_CROSSING_DETECTOR_HPP_
#define __R3BP_AXIS_CROSSING_DETECTOR_HPP_

#include "SpaceMechanicsToolBox.hpp"

enum AxisId {
    X_AXIS = 0,
    Y_AXIS = 1,
    Z_AXIS = 2
};

class AxisCrossingDetector : public SimulationEventHandler {
 private:
    double target_value;
    int id_axis;

 public:
    AxisCrossingDetector() : SimulationEventHandler("axis-detector", ""), target_value(0), id_axis(X_AXIS) { }
    ~AxisCrossingDetector() { }

    void set_target_value(double value) { target_value = value; }
    void set_axis(AxisId id_axis_) { id_axis = id_axis_; }

    real_type switchingFunction(real_type , real_type* x) {
        return x[id_axis] - target_value;
    }
};

// ------------------------------------------------------------------------
#endif  // __R3BP_AXIS_CROSSING_DETECTOR_HPP_
