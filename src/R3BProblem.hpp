// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_PROBLEM_HPP
#define __R3BP_PROBLEM_HPP

#include <math.h>
#include <stdlib.h>
//#include <unistd.h>

#include <string>
#include <vector>

#include "Jacobi.hpp"
#include "PrimaryPoint.hpp"
#include "propagator/R3bpFrame.hpp"
#include "SpaceMechanicsToolBox.hpp"

class R3bProblem {
 private:
    double a_m;
    double a_s;
    double a_kg;
    double mu_ratio;
    double r12;
    double Lxy[5][2];
    double Cj[5];
    double omega;
    double synodic_period;
    
    R3bpFrame *r3bpFrame;
    Vector7 to_fixed_frame_(double date, const Vector7& state, FrameTransform* transform) const;

 public:
    R3bProblem(double r1, double m1, double r2, double m2);
    R3bProblem(double m1, double m2, double r12) : R3bProblem(0, m1, r12, m2) { }  // c++11
    explicit R3bProblem(double mu_ratio);
    R3bProblem(double m1, double m2) : R3bProblem(m2 / (m1 + m2)) { }  // c++11
    ~R3bProblem() { }

    /** Return the mass parameter */
    double get_mu_ratio() { return mu_ratio; }

    /** Return the R3BP frequency */
    double get_omega() {
        return omega;
    }

    /** Return the time normalised synodic period. */
    double get_synodic_period() {
        //return 2 * pi * sqrt((r12 * r12 * r12) / (m1 + m2) );
        return synodic_period;
    }
    
    Vector3 get_primary_point(PrimaryPointId primaryId) const;

    /** get the binary system barycenter in the given frame */
    Vector3 get_barycenter(double date, Frame *frame);

    /** get all libration points coordinats in R3bp frame, nd their respective Jacobi constant */
    void get_libration_points(double L[5][2], double Cj[5]) const;

    /** Get libration point info */
    LibrationPointProperty get_libration_point_info(LibrationPointId id) const;

    /** Get libration point coordinates */
    Vector3 get_libration_point_coordinates(LibrationPointId id) const { return Vector3(Lxy[id][0], Lxy[id][1], 0); }

    /** Nondimensional distance factor */
    void setNormalisationForDistance(double a_m_) { a_m = a_m_; }
    double getNormalisationForDistance() { return a_m; }

    /** Nondimensional time factor */
    double getNormalisationForTime() { return a_s; }

    /** Nondimensional mass factor */
    double getNormalisationForMass() { return a_kg; }

    /** Convert state in R3bp frame to the fixed frame */
    Vector7 to_fixed_frame(double date, const Vector7& state) const;
    /** Convert states list in R3bp frame to the fixed frame */
    std::vector<Vector7> to_fixed_frame(const std::vector<double>& dates, std::vector<Vector7>& states) const;
};

// ---------------------------------------------------------------------------
#endif  // __R3BP_PROBLEM_HPP
