// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LongShortPeriodicOrbit.hpp"

#include <complex>

#include "propagator/detectors/AxisCrossingDetector.hpp"
#include "R3bpOrbitException.hpp"

//#define DEBUG_PERIODIC_ORBIT

// ------------------------------------------------------------------------
/** Compute long- and short-period periodic orbit around triangular libration
 * points
 * References:
 *  K.E. Papadakis, Motion around the Triangular Equilibrium Points
 *    of the restricted three-body problem under angular velocity variation,
 *    Astrophysics and Space Science 299: 129–148, 2005
 *
 */
LongShortPeriodicOrbit::LongShortPeriodicOrbit(const LibrationPointProperty &librationPoint_,
                                                PeriodicOrbitClass periodicOrbitClass_)
    : R3bpTriangularPointOrbit(librationPoint_), periodicOrbitClass(periodicOrbitClass_) {
    solver = new PeriodicOrbitProblemSolver(librationPoint_.mu);
}
// ------------------------------------------------------------------------
LongShortPeriodicOrbit::~LongShortPeriodicOrbit() {
    if (solver) {
        delete solver;
    }
}
// ------------------------------------------------------------------------
bool LongShortPeriodicOrbit::is_symmetric() {
    return true;
}
// ------------------------------------------------------------------------
double LongShortPeriodicOrbit::compute_delta(double omg) {
    double w43 = pow(omg, 4. / 3.);
    return 9 * get_mu_ratio() * (1 - get_mu_ratio()) * (4 - w43) * w43;
}
// ------------------------------------------------------------------------
/** Return the R3BP frequency */
double LongShortPeriodicOrbit::get_omega() {
    return 1;  // default value for the circular R3BP
}
// ------------------------------------------------------------------------
double LongShortPeriodicOrbit::get_frequency_correction() {
    return 1;
}
// ------------------------------------------------------------------------
double LongShortPeriodicOrbit::get_inplane_frequency() {
    double domgt = get_frequency_correction();
    get_analytical_frequencies(Vector6::Zero(), w1, w2);
    if (periodicOrbitClass == PeriodicOrbitClass::LONG_PERIOD) {
       return w1 * domgt;
    }
    else if (periodicOrbitClass == PeriodicOrbitClass::SHORT_PERIOD) {
        return w2 * domgt;
    }
    else {
        throw R3bpOrbitException("Unknown Periodic Orbit class.");
    }
    return 0;
}
// ------------------------------------------------------------------------
/**
 * Compute the short and long period/frequency for the triangular point.
 * Note function prototype is for convenience (ep is not used) overriding parent class method.
 */
void LongShortPeriodicOrbit::get_analytical_frequencies(const Vector6&, double &w1, double &w2) {
    double omg = get_omega();
    double Delta = compute_delta(omg);
    w1 = omg / sqrt(2) * sqrt(1 - sqrt(1 - Delta));
    w2 = omg / sqrt(2) * sqrt(1 + sqrt(1 - Delta));
}
// ------------------------------------------------------------------------
/** */
void LongShortPeriodicOrbit::compute_third_order_approximation(double omg, double lambda) {
    double Delta = compute_delta(omg);
    get_analytical_frequencies(librationPoint.get_state(), w1, w2);
    double B3 = 3 * pow(omg, 8/3.) * (1 - 2 * get_mu_ratio()) * sqrt(4 - pow(omg, 4./3));

    c11 = c12 = c13 = c14 = 0;

    if (periodicOrbitClass == PeriodicOrbitClass::LONG_PERIOD) {
        double B1 = 3 * pow(omg, 10/3.) - 12 * omg*omg - 4*w1*w1;
        c11 = lambda * sqrt(librationPoint.L[0] * librationPoint.L[0] + librationPoint.L[1] * librationPoint.L[1]);
        c21 = 0;  // arbitrary
        c12 = (B3 * c11 - c21 * B1) / (8 * omg * w1);
        c22 = 1. / (2 * omg * w1 * B1) * (Delta * omg*omg*omg*omg * c11 + 2 * B3 * omg * w1 * c12 + 4 * w1*w1*c11*(3*omg*omg + w1*w1));
        c23 = 0;
        c24 = 0;
    }
    else if (periodicOrbitClass == PeriodicOrbitClass::SHORT_PERIOD) {
        double B2 = 3 * pow(omg, 10/3.) - 12 * omg*omg - 4*w2*w2;
        c13 = lambda * sqrt(librationPoint.L[0] * librationPoint.L[0] + librationPoint.L[1] * librationPoint.L[1]);
        c23 = 0;  // arbitrary
        c21 = 0;
        c22 = 0;
        c14 = (B3 * c13 - B2 * c23) / (8 * omg * w2);
        c24 = 1. / (2 * omg * w2 * B2) * (Delta * omg*omg*omg*omg * c13 + 2 * B3 * omg * w2 * c14 + 4 * w2*w2*c13*(3*omg*omg + w2*w2));
    }
#ifdef DEBUG_PERIODIC_ORBIT
    printf("Delta = %.8f\n", Delta);
    printf("c(long) = %.8f %.8f %.8f %.8f\n", c11, c12, c21, c22);
    printf("c(short). = %.8f %.8f %.8f %.8f\n", c13, c14, c23, c24);
    printf("w1 = %.8f (long period) w2 = %.8f (short period)\n", w1, w2);
#endif
}

// ------------------------------------------------------------------------
/**
 * Compute the state initial condition for the Halo orbit, given the x-
 * and z-excusion, and the pulsation.
 * @param lambda
 */
// ------------------------------------------------------------------------
bool LongShortPeriodicOrbit::get_initial_conditions(double lambda,
                                                    double Ax, double omega,
                                                    /* outputs */
                                                    double *x_guess, double &t_guess) {
    set_parameter(lambda);

    double t = 0;
    interpolate_initial_conditions(t, x_guess);

    // estimation of the period T
    // frequency correction
    double domgt = get_frequency_correction();
    if (periodicOrbitClass == PeriodicOrbitClass::LONG_PERIOD) {
        t_guess = 2 * M_PI / (w1 * domgt);
    }
    else if (periodicOrbitClass == PeriodicOrbitClass::SHORT_PERIOD) {
        t_guess = 2 * M_PI / (w2 * domgt);
    }

    // if symmetric orbit...
    if (is_symmetric()) {
//        double mass0 = 1;
//        double t_test = 500.0;
//        // Propagate with crossing event to guess the half period
//        TrajectoryPropageRtbp *state_propagator = new TrajectoryPropageRtbp(get_mu_ratio(), 0);
//        SimulationEventManager *eventManager = new SimulationEventManager();
//        AxisCrossingDetector *detector = new AxisCrossingDetector();
//        detector->set_axis(Y_AXIS);
//        eventManager->addEventHandler(detector);
//        state_propagator->setEventHandler(eventManager);
//        Vector7 x0; x0 << x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5], mass0;
//        Vector7 xf;
//        int iout = state_propagator->getFinalPoint(x0, 0, t_test, t_guess, xf);
//        t_guess *= 2;
//
//        if (iout != ODESOLVER::success::interrupted) {
//            printf("Warning: Failed finding an y-crossing initial guess!\n");
//            t_guess = 2 * M_PI / omega;
//        }
    }

#ifdef DEBUG_PERIODIC_ORBIT
    printf("omega* = %.8f\n", get_omega() * domgt);
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
            x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
    printf("Tguess = %.8f (T ~ %.8f)\n", t_guess, 2 * M_PI / omega);
    // std::cout << "xf = " << xf.transpose() << std::endl;
#endif

    return true;
}
// ------------------------------------------------------------------------
bool LongShortPeriodicOrbit::interpolate_initial_conditions(double t,
                                                            /* outputs */
                                                            double *x_guess) {
    double omega = get_omega();
    compute_third_order_approximation(omega, lambda);
    double domgt = get_frequency_correction();
    double cos_phi = cos(w1 * t);
    double sin_phi = sin(w1 * t);
    double cos_2phi = cos(w2 * t);
    double sin_2phi = sin(w2 * t);

    // x-y planar orbit
    x_guess[0] = c11 * cos_phi + c12 * sin_phi + c13 * cos_2phi + c14 * sin_2phi;
    x_guess[1] = c21 * cos_phi + c22 * sin_phi + c23 * cos_2phi + c24 * sin_2phi;
    x_guess[2] = 0;
    //
    x_guess[3] = w1 * (-c11 * sin_phi + c12 * cos_phi) + w2 * (-c13 * sin_2phi + c14 * cos_phi);
    x_guess[3] *= domgt;
    x_guess[4] = w1 * (-c21 * sin_phi + c22 * cos_phi) + w2 * (-c23 * sin_2phi + c24 * cos_phi);
    x_guess[4] *= domgt;
    x_guess[5] = 0;

    // place around the libration point
    Vector2 pos_libr = librationPoint.get_state().segment(0, 2);
    x_guess[0] += pos_libr(0);
    x_guess[1] += pos_libr(1);

#ifdef DEBUG_PERIODIC_ORBIT
    printf("omega* = %.8f\n", get_omega() * domgt);
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
            x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
#endif
    return false;
}
// ------------------------------------------------------------------------
/**
 * Compute the state initial condition for the orbit, given z-excusion.
 * Internal variables are set by computeThirdOrderApproximation(), so this
 * function shall not be called directly.
 */
// ------------------------------------------------------------------------
bool LongShortPeriodicOrbit::get_initial_conditions(double Az,
                                                    /* outputs */
                                                    double *xguess, double &Tguess) {
    double Ax = 1, omega = 1;  // FIXME(joris) compute omega
    get_initial_conditions(Az, Ax, omega, xguess, Tguess);

    return true;
}
// ------------------------------------------------------------------------
/**
 * This function finds a long/short periodic orbit around a triangular point
 * in the RTBP.
 *
 * inputs:
 *   lambda   lambda
 *
 * return OrbitSolution structure with:
 *   pos      initial position
 *   vel      initial velocity
 *   T        period of the orbit
 *   STM      transition matrix at T (monodromy matrix) to investigate
 *            first order stability of the orbit
 */
OrbitSolution LongShortPeriodicOrbit::find(double lambda,
                                        /* options */
                                        bool print_iteration,
                                        uint max_iterations,
                                        double xtol) {
    Ax = 1;
    double omega = 1;  // FIXME(joris) compute omega
    double xguess[6];  // [pos, vel]
    double Tguess;
    get_initial_conditions(lambda, Ax, omega, xguess, Tguess);

    VECTORVAR xInit;
    if (is_symmetric()) {
        xInit = VECTORVAR::Zero(3);
        xInit << xguess[3], xguess[4], Tguess / 2.;  // vx, vy, T
    } else {
        xInit = VECTORVAR::Zero(4);
        xInit << xguess[0], xguess[3], xguess[4], Tguess / 2.;  // rx, vx, vy, T
    }
    return find_(xguess, xInit, lambda, print_iteration, max_iterations, xtol);
}
// ------------------------------------------------------------------------
/**
 * Find the long/shor periodic orbit using a guess for initial state.
 * The continuation parameter is the x-position.
 */
// ------------------------------------------------------------------------
OrbitSolution LongShortPeriodicOrbit::find_by_continuation(double lambda,
                                                        OrbitSolution& initialGuess,
                                                        /* options */
                                                        bool print_iteration,
                                                        uint max_iterations,
                                                        double xtol) {
    // initial condition, modified with the updated orbit parameter
    double xguess[] = {
        lambda,
        initialGuess.r0(1),
        initialGuess.r0(2),
        initialGuess.v0(0),
        initialGuess.v0(1),
        initialGuess.v0(2)};  // [pos, vel]
    double Tguess = initialGuess.period ;

    VECTORVAR xInit = VECTORVAR::Zero(3); xInit << xguess[3], xguess[4], Tguess / 2.;  // Vx, Vy, T
    find_(xguess, xInit, lambda, print_iteration, max_iterations, xtol);

    continuation_history.push_back(OrbitContinuityCondition(lambda, orbitSolution));

    return orbitSolution;
}
// ------------------------------------------------------------------------
OrbitSolution LongShortPeriodicOrbit::find_(const double *xguess,
                                            VECTORVAR &xInit,
                                            double lambda,
                                            /* options */
                                            bool print_iteration,
                                            uint max_iterations,
                                            double xtol) {
    VECTORVAR xs; xs = xInit;
    Matrix6 phi = solve(xguess,
                        xInit, xs,
                        print_iteration, max_iterations, xtol);

    conclude(xs, phi, lambda, 0);

    return orbitSolution;
}
// ------------------------------------------------------------------------
/**
 */
Matrix6 LongShortPeriodicOrbit::solve(const double* xguess,
                                    const VECTORVAR &xInit, VECTORVAR &xs,
                                    bool print_iteration, uint max_iterations, double xtol) {
    Vector3 zz;
    Matrix3 J;
    Matrix6 phi;

    solver->set_initial_conditions(xguess);
    int ret = solver->solve(xInit, xs, print_iteration, max_iterations, xtol);

    // post process and compile outputs from the solution
    solver->zerof(xs, zz, J, phi);

    orbitSolution.is_valid = ret >= 1;
    return phi;
}
// ------------------------------------------------------------------------
void LongShortPeriodicOrbit::conclude(VECTORVAR &xs, const Matrix6 &phi, double Az, double Ax) {
    // fill in a property structure for other codes
    orbitSolution.librationPoint = librationPoint;
    orbitSolution.type = "long period orbit";
    orbitSolution.mu = get_mu_ratio();

    // initial position
    // pos = [Lx; Ly; Lz] + dpos;
    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM = A * phi.inverse() * A * phi;
    orbitSolution.monodromy = STM;        // monodromy matrix

    orbitSolution.period  = solver->get_duration(xs);
    orbitSolution.r0 = solver->get_initial_position(xs);
    orbitSolution.v0 = solver->get_initial_velocity(xs);
    orbitSolution.Ax = Ax;
    orbitSolution.Ay = 0;
    orbitSolution.Az = Az;
}
