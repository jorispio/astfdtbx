// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_LONG_SHORT_ORBIT_HPP_
#define __R3BP_LONG_SHORT_ORBIT_HPP_

#include <math.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "Jacobi.hpp"
#include "orbit/R3bpTriangularPointOrbit.hpp"
#include "solvers/PeriodicOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

enum PeriodicOrbitClass {
    LONG_PERIOD = 1,
    SHORT_PERIOD = 2
};

class LongShortPeriodicOrbit : public R3bpTriangularPointOrbit {
 private:
    PeriodicOrbitClass periodicOrbitClass;

    void compute_third_order_approximation(double omg, double lambda);
    double get_omega();
    double get_frequency_correction();
    double compute_delta(double omg);

    virtual bool get_initial_conditions(double lambda, double Ax, double omega,
            /* outputs */
            double *xguess, double &Tguess);

    OrbitSolution find_(const double *xguess, VECTORVAR &xInit,
            double lambda,
            /* options */
            bool print_iteration,
            uint max_iterations,
            double xtol);

 protected:
    PeriodicOrbitProblemSolver *solver;

    Matrix6 solve(const double* xguess,
            const VECTORVAR &x0, VECTORVAR &xs,
            bool print_iteration = true,
            uint max_iterations = 100,
            double xtol = 1e-10);

    void conclude(VECTORVAR &xs, const Matrix6 &phi, double Az, double Ax);

    double lambda;
    double c11, c12, c13, c14;
    double c21, c22, c23, c24;
    double w1, w2;

 public:
    LongShortPeriodicOrbit(const LibrationPointProperty &librationPoint, PeriodicOrbitClass periodicOrbitClass);
    virtual ~LongShortPeriodicOrbit();

    /** is symmetric orbit */
    bool is_symmetric();

    void set_parameter(double lambda_) { lambda = lambda_; }

    void get_analytical_frequencies(const Vector6 &ep, double &w1, double &w2) override;
    double get_inplane_frequency() override;
    double get_analytical_period() override {
        return 2 * M_PI / get_inplane_frequency();
    }

    bool get_initial_conditions(double Az,
            /* outputs */
            double *xguess, double &tguess);

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    OrbitSolution find(double lambda,
        /* options */
        bool print_iteration = false,
        uint max_iterations = 100,
        double xtol = 1e-10);

    OrbitSolution find_by_continuation(double lambda,
        OrbitSolution& initialGuess,
        /* options */
        bool print_iteration = false,
        uint max_iterations = 100,
        double xtol = 1e-10);
};

// ------------------------------------------------------------------------
#endif  // __R3BP_LONG_SHORT_ORBIT_HPP_
