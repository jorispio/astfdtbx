// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_SOLUTION_HPP__
#define __R3BP_ORBIT_SOLUTION_HPP__

#include <string>
#include <vector>

#include "LibrationPoint.hpp"
#include "propagator/R3bpPropagator.hpp"
//#include "analysis/OrbitMissionAnalysis.hpp"
#include "R3BProblem.hpp"
#include "segments/OrbitSegment.hpp"
#include "SpaceMechanicsToolBox.hpp"
#include "Jacobi.hpp"
#include "OrbitStability.hpp"


/** Orbit solution structure that store all necessary information on the orbit solution for analysis and post-processing.
 */
struct OrbitSolution : private OrbitStability {
    bool is_valid;

    LibrationPointProperty librationPoint;
    std::string type;
    double mu;          // mass ratio of the problem
    double Ax;          // x-excursion
    double Ay;          // y-excursion
    double Az;          // z-excursion
    double phi_x;
    double phi_y;
    double phi_z;

    double omega;       // in-plane frequency
    double nu;          // out-of-plane frequency
    double lambda;

    Vector3 r0;         // initial position at t0
    Vector3 v0;         // initial velocity at t0
    double period;      // period

    double jacobi_constant; // Jacobi constant value
    
    double resonance_y;   // ratio y:n reflecting the number, y, of completed revolutions 
    double resonance_n;   // along a specific libration orbit per n synodic cycles of the primary
    
    VECTORVAR xsol;     // solution to the problem that found this orbit
    MATRIXVAR jacobian; // jacobian from the solving process. dPsi/dxi, xi:unknown vector accordingly to the problem and symmetries.
    
    /** Constructor. */
    OrbitSolution() : OrbitStability(), is_valid(false), mu(0), 
                    Ax(0), Ay(0), Az(0), 
                    phi_x(0), phi_y(0), phi_z(0), omega(0), nu(0), 
                    lambda(0), period(0),
                    resonance_y(0), resonance_n(0) { }

    /** Constructor, from known solution. */
    OrbitSolution(double mu_ratio, Vector6 x_init, double solution_period) : OrbitStability(), is_valid(true), mu(mu_ratio),
                    Ax(0), Ay(0), Az(0), 
                    phi_x(0), phi_y(0), phi_z(0), omega(0), nu(0), 
                    lambda(0), period(solution_period),
                    resonance_y(0), resonance_n(0)  {
        type = "Abacus Solution";

        r0 = x_init.segment(0,3);
        v0 = x_init.segment(3,3);
        /*
        if (x_init != nullptr) {
            r0 << x_init[0], x_init[1], x_init[2];
            v0 << x_init[3], x_init[4], x_init[5];  
        }
        */
    } 

    /** Return the initial position vector in rotating frame, in normalized units. */
    Vector3 get_initial_position() const { return r0; }
    /** Return the initial velocity vector in rotating frame, in normalized units. */    
    Vector3 get_initial_velocity() const { return v0; }
    /** Return the initial state vector in rotating frame, in normalized units. */    
    Vector6 get_initial_state() const {
        Vector6 x0;
        x0 << r0, v0;
        return x0;
    }

    /** Return the orbit period, in normalized units. */
    double get_period() const { return period; }

    /** Return the Jacobi constant. */
    double get_jacobi_constant() {
        jacobi_constant = jacobi::JacobiC(mu, r0, v0);
        return jacobi_constant;
    }
    
    Matrix6 monodromy;  // monodromy matrix
    /** Return the monodromy matrix. */
    Matrix6 getMonodromyMatrix() const { return monodromy; }

    double mnu1;        // Hénon stability parameter
    double mnu2;        // Hénon stability parameter

    /**
     * The orbit is thus stable if |mnu_i| <= 1, and mnu_i is real
     * i = {1, 2}
     */
    void computeStabilityIndices() {
        OrbitStability::computeStabilityIndices(monodromy, mnu1, mnu2);
    }

    /**
     * Compute stability index of the Halo orbit.
     * Stability <= 1  => stable
     * Stability  > 1  => unstable
     */
    double get_stability_index() {
        return OrbitStability::get_stability_index(monodromy);
    }

    /** Construct a list of orbit segment from the initial conditions. */
    std::vector<Segment> get_solution_segments(Propagator *propagator, int n_segments) const {
        std::vector<Segment> segments;
        double step = period / static_cast<double>(n_segments);
        double ti, t = 0;
        std::vector<double> trajectoryTime;
        std::vector<Vector7> trajectoryState;
        Vector3 ri = r0, vi = v0, rf, vf;
        double tf, mf;
        for (int idx = 0; idx < n_segments + 1; ++idx) {
            propagator->getTrajectory(ri, vi, 1, 0, step, -1,
                                                  trajectoryTime, trajectoryState,
                                                  tf, rf, vf, mf);
            if (idx > 0) {
                Segment segment = Segment(ri, vi, rf, vf, ti, tf);
                segments.push_back(segment);
            }

            ti = t;
            ri = rf;
            vi = vf;
            t += step;
        }

        return segments;
    }

    /** Propagate the solution in the R3BP frame */
    void propagate(const double tspan[2], double timeStep, std::vector<double>& trajectoryTime, std::vector<Vector7>& trajectoryState) {
        double tf;
        Vector3d rf, vf;
        double mf;
        Propagator *propagator = new Propagator(mu, 0.);
        propagator->getTrajectory(r0, v0, 1, tspan[0], tspan[1], timeStep,
                                                  trajectoryTime, trajectoryState,
                                                  tf, rf, vf, mf);
        delete propagator;
    }

    /** Propagate the solution in the R3BP frame. In many cases, we do not care about the time variable... */
    std::vector<Vector7> propagate(const double tspan[2], double timeStep) {
        std::vector<double> trajectoryTime;
        std::vector<Vector7> trajectoryState;
        propagate(tspan, timeStep, trajectoryTime, trajectoryState);        
        return trajectoryState;
    }
    
    /** Propagate the solution in an (pseudo) inertial frame attached to the primary */
    std::vector<Vector7> propagateToFixedFrame(const R3bProblem *problem, const double tspan[2], double timeStep) {
        std::vector<Vector7> ptsInRotating = propagate(tspan, timeStep);

        std::vector<Vector7> ptsInFixedFrame;
        double time = tspan[0];
        for (std::vector<Vector7>::iterator it = ptsInRotating.begin(); it != ptsInRotating.end(); ++it) {
            ptsInFixedFrame.push_back(problem->to_fixed_frame(time, (*it)));
            time += timeStep;
        }

        return ptsInFixedFrame;
    }
    
    /**
     * Compute the periapsis distance to the primary
     */
    double get_periapsis(PrimaryPointId primary) {
        Vector3 rp0;
        if (primary == P1) {
            rp0 << -mu, 0, 0;
        }
        else {
            rp0 << 1-mu, 0, 0;
        }

        double rp_dist = 1e20;
        double tspan[] = {0, period/2.};
        std::vector<Vector7> pv = propagate(tspan, period / 180.);
        for(std::vector<Vector7>::iterator it = pv.begin(); it != pv.end(); ++it) {
            Vector3 r = (*it).segment(0, 3);
            double dist = (rp0 - r).norm();
            rp_dist = std::min(dist, rp_dist);
        }
        return rp_dist;
    }
};

// ------------------------------------------------------------------------
#endif  // __R3BP_ORBIT_SOLUTION_HPP__

