// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "DistantRetrogradeOrbit.hpp"

#include "propagator/detectors/AxisCrossingDetector.hpp"
#include "R3bpProblemException.hpp"

// ------------------------------------------------------------------------
/**
 * Construct a distant retrograde orbit around thhe secondary body of the
 * restricted three-body problem.
 */
DistantRetrogradeOrbit::DistantRetrogradeOrbit(double mu_ratio, PrimaryPointId primaryPointId_)
    : R3bpPrimaryPointOrbit(primaryPointId_, DRO), mu_ratio(mu_ratio), primaryPointId(primaryPointId_) {
    solver = new PlanarSymmetricOrbitProblemSolver(mu_ratio, 0,
                                        StateFilter(V_VY, V_TIME),
                                        ConstraintDerivativeFilter(C_RY, C_VX));
}

// ------------------------------------------------------------------------
DistantRetrogradeOrbit::~DistantRetrogradeOrbit() {
    if (solver != NULL) {
        delete solver;
    }
}
// ------------------------------------------------------------------------
/**
 */
bool DistantRetrogradeOrbit::interpolate_initial_conditions(double t, double *x_guess) {
    throw R3bpProblemException("No implementation");
    return false;
}
// ------------------------------------------------------------------------
/**
 */
bool DistantRetrogradeOrbit::get_initial_conditions(double xpos0,
            /* outputs */
            double *x_guess, double &t_guess) {
    double t_test = 500.0;  // take initially a large value. It will be corrected afterwards
    x_guess[0] = xpos0;
    x_guess[1] = 0;
    x_guess[2] = 0;
    x_guess[3] = 0;
    x_guess[4] =-0.7;  // Vy
    x_guess[5] = 0;
    double mass0 = 1;

    solver->set_initial_conditions(x_guess);

    // Propagate with crossing event to guess the half period
    TrajectoryPropageRtbp *state_propagator = new TrajectoryPropageRtbp(mu_ratio, 0);
    SimulationEventManager *eventManager = new SimulationEventManager();
    AxisCrossingDetector *detector = new AxisCrossingDetector();
    detector->set_axis(Y_AXIS);
    eventManager->addEventHandler(detector);
    state_propagator->set_event_manager(eventManager);
    Vector7 x0; x0 << x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5], mass0;
    Vector7 xf;
    state_propagator->getFinalPoint(x0, 0, t_test, t_guess, xf);
    t_guess *= 2;

    delete state_propagator;
    delete eventManager;
    delete detector;
    return true;
}
// ------------------------------------------------------------------------
/**
 */
Matrix6 DistantRetrogradeOrbit::solve(double xpos,
            const VECTORVAR &xInit, VECTORVAR &xs,
            bool print_iteration,
            uint max_iterations,
            double xtol) {
    solver->set_parameter(xpos);
    int ret = solver->solve(xInit, xs, print_iteration, max_iterations, xtol);
    orbitSolution.is_valid = ret >= 1;

    VECTORVAR zz = VECTORVAR::Zero(2);
    MATRIXVAR J = MATRIXVAR::Zero(2, 2);
    Matrix6 phi;
    solver->zerof(xs, zz, J, phi);

    return phi;
}

// ------------------------------------------------------------------------
/**
 * This function finds a Lissajous orbit for the RTBP.
 * Lissajous orbits, usually around L1,2,3, are the results of linearisation of
 * the dynamical equations around one of the Libration point.
 * They do not however exactly orbit the Libration points. They are the
 * general period solution of the R3BP
 *
 * inputs:
 *   mu       mass ratio
 *   Ax       x-excursion of the orbit
 *   Az       z-excursion of the orbit
 *
 * return OrbitSolution structure with:
 *   pos      initial position
 *   vel      initial velocity
 *   T        period of the orbit
 *   STM      transition matrix at T (monodromy matrix) to investigate
 *            first order stability of the orbit
 *
 */
OrbitSolution DistantRetrogradeOrbit::find(
    double xpos,
    /* options */
    bool print_iteration,
    uint max_iterations,
    double xtol) {

    // fill in a property structure for other codes
    orbitSolution.type = "dro";

    // initial condition
    double xguess[6];  // [pos, vel]
    double Tguess;
    get_initial_conditions(xpos, xguess, Tguess);

#ifdef DEBUG
    printf("Initial point:  %f  %f  %f  %f  %f  %f\n",
                xguess[0], xguess[1], xguess[2], xguess[3], xguess[4], xguess[5]);
    printf("Initial Period: %f\n", Tguess);
#endif

    VECTORVAR xInit = VECTORVAR::Zero(2); xInit << xguess[4], Tguess/2;  // Vy, T
    VECTORVAR xs; xs = xInit;
    Matrix6 phi = solve(xpos,
                xInit, xs,
                print_iteration, max_iterations, xtol);

    conclude(xs, phi, xpos);

    return orbitSolution;
}

// ------------------------------------------------------------------------
/**
 * The continuation parameter is the x-position.
 */
OrbitSolution DistantRetrogradeOrbit::find_by_continuation(double x_parameter,
        const OrbitSolution& droProperty,
        /* options */
        bool print_iteration,
        uint max_iterations,
        double xtol) {
    // initial condition, modified with the updated orbit parameter
    double xguess[] = {
        x_parameter,
        droProperty.r0(1),
        droProperty.r0(2),
        droProperty.v0(0),
        -0.7,  // TODO(joris): update with non hardcoded value
        droProperty.v0(2)};
    double Tguess = droProperty.period ;

    VECTORVAR xInit = VECTORVAR::Zero(2); xInit << xguess[4], Tguess/2;  // Vy, T
    VECTORVAR xs; xs = xInit;
    Matrix6 phi = solve(x_parameter,
                xInit, xs,
                print_iteration, max_iterations, xtol);

    conclude(xs, phi, x_parameter);

    continuation_history.push_back(OrbitContinuityCondition(x_parameter, orbitSolution));

    return orbitSolution;
}
// ------------------------------------------------------------------------
void DistantRetrogradeOrbit::conclude(const VECTORVAR &xs, const Matrix6 &phi, double xpos) {
    // fill in a property structure for other codes
    orbitSolution.type = "dro";
    orbitSolution.mu = mu_ratio;
    orbitSolution.period  = solver->get_duration(xs);
    orbitSolution.r0 = solver->get_initial_position(xs, xpos);  // initial position at t0
    orbitSolution.v0 = solver->get_initial_velocity(xs);  // initial velocity at t0

    // initial position
    // pos = [Lx; Ly; Lz] + dpos;
    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM = A * phi.inverse() * A * phi;
    orbitSolution.monodromy = STM;  // monodromy matrix
    orbitSolution.computeStabilityIndices();

    orbitSolution.Ax = 0;
    orbitSolution.Ay = 0;
    orbitSolution.Az = 0;
    orbitSolution.phi_x = 0;
    orbitSolution.phi_y = 0;
    orbitSolution.phi_z = 0;
}
