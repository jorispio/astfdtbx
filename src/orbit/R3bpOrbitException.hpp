// $Id: R3bpOrbitException.hpp 132 2015-12-29 20:57:47Z joris $
// ---------------------------------------------------------------------
/*
 *   This file is part of SpaceMechanicsToolBox.
 *
 *   Copyright (C) 2012 Joris Olympio
 *
 *   SpaceMechanicsToolBox is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   SpaceMechanicsToolBox is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with SpaceMechanicsToolBox.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_EXCEPTION_HPP_
#define __R3BP_ORBIT_EXCEPTION_HPP_

#include <exception>
#include <iostream>     // std::cout
#include <string>

class R3bpOrbitException : public std::exception {
 private:
    std::string message;
    std::string details;

 public:
    R3bpOrbitException(const std::string &details,
                                  const std::string &message = "R3BP Orbit exception: ")
        : message(message), details(details) {   }

    virtual ~R3bpOrbitException() {   }

    const char *what() const throw() {
        std::cout << details << std::endl;
        return message.c_str();
    }
};

#endif  // __R3BP_ORBIT_EXCEPTION_HPP_
