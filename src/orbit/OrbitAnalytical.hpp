// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_ANALYTICAL_HPP_
#define __R3BP_ORBIT_ANALYTICAL_HPP

#include <vector>

#include "LibrationPoint.hpp"
#include "segments/PatchPoint.hpp"
#include "PrimaryPoint.hpp"
#include "segments/OrbitSegment.hpp"

class OrbitAnalytical {
 private:
    LibrationPointProperty librationPoint;

 public:
    explicit OrbitAnalytical(PrimaryPointId ) {}
    explicit OrbitAnalytical(LibrationPointProperty librationPoint) : librationPoint(librationPoint) {}
    virtual ~OrbitAnalytical() {}

    void set_parameters(double Ax, double phix, double Az, double phiz);

    virtual void get_analytical_frequencies(const Vector6 &ep, double &omega, double &nu);
    virtual double get_inplane_frequency();

    /** Return the analytical approximation of the period */
    virtual double get_analytical_period();

    virtual bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) = 0;

    /** */
    std::vector<Segment> get_analytical_segments(int n_segments);

    double Ax /*final*/;
    double phix;
    double Az;
    double phiz;
};

#endif  // __R3BP_ORBIT_ANALYTICAL_HPP
