// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __LINEAR_SOLUTION_HPP_
#define __LINEAR_SOLUTION_HPP_

#include "SpaceMechanicsToolBox.hpp"

#include "LibrationPoint.hpp"

class LinearSolution {
 private:
    LibrationPointProperty lpoint;

    double calc_c(double gamma, double mu, double n);

 protected:
    double a1, a2, c2, c3, c4;
    double a21, a22, a23, a24, a31, a32;
    double b22, b21;
    double d1, d2, d21;
    double b31, b32;
    double d31, d32;
    double s1, s2;
    double l1, l2;

 public:
    LinearSolution(const LibrationPointProperty &librationPoint) : lpoint(librationPoint) { };
    ~LinearSolution() { };

    void compute_third_order_approximation(double mu, double lambda, double k, double gamma, double c2);
    double get_frequency_correction(double Ax, double Az);
};

#endif  // __LINEAR_SOLUTION_HPP_