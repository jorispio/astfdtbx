
#include "OrbitApproximation.hpp"

#include <math.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "R3bpOrbitException.hpp"


// ------------------------------------------------------------------------
double LinearSolution::calc_c(double gamma, double mu, double n) {
    if (n == 2) {
        return lpoint.calc_c2();
    }

    double gamma3 = gamma * gamma * gamma;
    if (lpoint.id == L1) {
        double f = pow(gamma / (1 - gamma), n + 1);
        return 1. / gamma3 * (mu + pow(-1, n) * (1 - mu) * f);
    }
    else if (lpoint.id == L2) {
        double f = pow(gamma / (1 + gamma), n + 1);
        return 1. / gamma3 * pow(-1, n) * (mu + (1 - mu) * f);
    }
    else if (lpoint.id == L3) {
        double f = pow(gamma / (1 + gamma), n + 1);
        return 1. / gamma3 * ((1 - mu) + mu * f);
    }

    throw R3bpOrbitException("Cannot compute c_n!");
}
// ------------------------------------------------------------------------
/**
 * Compute third order approximation for orbits around collinear points.
 * See: D.L. Richardson, Analytic Construction of Periodic Orbits About the Collinear Points,
 *              Celestial Mechanics 22, 1980, 241-253
 *
 * @param Az z-amplitude of the orbit
 * @param mu gravitational constant
 * @param lambda in-plane frequency
 * @param k
 * @param gamma
 * @param c2
 */
void LinearSolution::compute_third_order_approximation(double mu, double lambda, double k, double gamma, double c2) {
    if (lambda <= 0) {
        throw R3bpOrbitException("Pulsation cannot be negative!");
    }
    assert(k > 0);
    assert(gamma != 0);
    assert(gamma != 1);

    double lambda2 = lambda * lambda;
    double k2 = k * k;
    double k3 = k2 * k;

    c3 = calc_c(gamma, mu, 3);
    c4 = calc_c(gamma, mu, 4);

    d1 = 3. * lambda2 / k * (k * (6. * lambda2 - 1.) - 2. * lambda);
    d2 = 8. * lambda2 / k * (k * (11. * lambda2 - 1.) - 2. * lambda);

    d21 = -c3 / (2 * lambda2);
    a21 = (3 * c3 * (k2 - 2)) / (4 * (1 + 2 * c2));
    a22 = 3 * c3 / (4 * (1 + 2 * c2));
    a23 = -3 * c3 * lambda / (4 * k * d1) * (3 * k3 * lambda - 6 * k * (k - lambda) + 4);
    a24 = -3 * c3 * lambda / (4 * k * d1) * (2 + 3 * k * lambda);

    a1 =-1.5 * c3 * (2 * a21 + a23 + 5 * d21) - 3./8. * c4 * (12 - k2);
    a2 = 1.5 * c3 * (a24 - 2 * a22) + 9./8. * c4;

    b21 =-3 * c3 * lambda / (2 * d1) * (3 * k * lambda - 4);
    b22 = 3 * c3 * lambda / d1;

    a31 = -9 * lambda / (4 * d2) * (4 * c3 * (k * a23 - b21) + k * c4 * (4 + k*k))
        + (9 * lambda2 + 1 - c2) / (2 * d2) * (3 * c3 * (2 * a23 - k * b21) + c4 * (2 + 3 * k*k));
    a32 =-1. / d2 * (9 * lambda / 4. * (4 * c3 * (k * a24 - b22) + k * c4)
        + 1.5 * (9 * lambda2 + 1 - c2) * (c3 * (k * b22 + d21 - 2 * a24) - c4));

    b31 = 3. / (8. * d2) * (8 * lambda * (3 * c3 * (k * b21 - 2 * a23) - c4 * (2 + 3*k*k))
               + (9 * lambda2 + 1 + 2 * c2) * (4 * c3 * (k * a23 - b21) + k * c4 * (4 + k*k)));
    b32 = 1. / d2 * (9 * lambda * (c3 * (k * b22 + d21 - 2*a24) - c4) + 3./8. * (9 * lambda * lambda + 1 + 2 * c2) * (4 * c3 * (k * a24 - b22) + k * c4));

    d31 = 3 / (64. * lambda2) * (4 * c3 * a24 + c4);
    d32 = 3 / (64. * lambda2) * (4 * c3 * (a23 - d21) + c4 * (4 + k * k));


    double k4 = k3 * k;
    double sd = 2. * lambda * (lambda * (1. + k2) - 2. * k);

    s1 = 1. / sd * (1.5 * c3 * (2 * a21 * (k2 - 2) - a23 * (k2 + 2) - 2. * k * b21) - 3./8. * c4 * (3 * k4 - 8 * k2 + 8));
    s2 = 1. / sd * (1.5 * c3 * (2 * a22 * (k2 - 2) + a24 * (k2 + 2) + 2. * k * b22 + 5 * d21) + 3./8. * c4 * (12 - k2));
    l1 = a1 + 2 * lambda2 * s1;
    l2 = a2 + 2 * lambda2 * s2;

#ifdef DEBUG_
    printf("lambda = %.8f\n", lambda);
    printf("Delta = %.8f\n", Delta);
    printf("k = %.8f\n", k);
    printf("c2 = %.8f\n", c2);
    printf("c3 = %.8f\n", c3);
    printf("c4 = %.8f\n", c4);
    printf("d1 = %.9f  d2 = %.9f\n", d1, d2);
    printf("s1 = %.9f  s2 = %.9f\n", s1, s2);
    printf("l1 = %.9f  l2 = %.9f\n", l1, l2);
    printf("a1 = %.9f  a2 = %.9f\n", a1, a2);
    printf("a21 = %.9f, a22=%.9f, a23=%.9f, a24=%.9f, a31=%.9f, a32=%.9f\n", a21, a22, a23, a24, a31, a32);
    printf("b21 = %.9f, b22=%.9f, b31=%.9f, b32=%.9f\n", b21, b22, b31, b32);
    printf("d21 = %.9f, d31=%.9f, d32=%.9f\n", d21, d31, d32);
#endif
}
// ------------------------------------------------------------------------
/** */
double LinearSolution::get_frequency_correction(double Ax, double Az) {
    // frequency correction
    return (1 + s1 * Ax * Ax + s2 * Az * Az);
}
