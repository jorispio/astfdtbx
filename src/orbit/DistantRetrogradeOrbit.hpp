// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_DISTANT_RETROGRADE_ORBIT_HPP_
#define __R3BP_DISTANT_RETROGRADE_ORBIT_HPP_

#include "orbit/R3bpPrimaryPointOrbit.hpp"
#include "PrimaryPoint.hpp"
#include "solvers/SymmetricOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

class DistantRetrogradeOrbit : public R3bpPrimaryPointOrbit {
 private:
    double mu_ratio;
    PrimaryPointId primaryPointId;

    PlanarSymmetricOrbitProblemSolver *solver;

    void conclude(const VECTORVAR &xs, const Matrix6 &phi, double Ax);

 public:
    DistantRetrogradeOrbit(double mu_ratio, PrimaryPointId primaryPointId = PrimaryPointId::P1);
    ~DistantRetrogradeOrbit();

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    bool get_initial_conditions(double Az,
            /* outputs */
            double *xguess, double &Tguess);

    Matrix6 solve(double Ax,
            const VECTORVAR &x0, VECTORVAR &xs,
            bool print_iteration = true,
            uint max_iterations = 100,
            double xtol = 1e-10);


    OrbitSolution find(double Ax,
                bool print_iteration = false,
                uint max_iterations = 100,
                double xtol = 1e-10);

    OrbitSolution find_by_continuation(double Ax,
                const OrbitSolution& initialGuess,
                bool print_iteration = false,
                uint max_iterations = 100,
                double xtol = 1e-10);
};

#endif  // __R3BP_DISTANT_RETROGRADE_ORBIT_HPP_
