// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "OrbitAnalytical.hpp"

#include <vector>

#include "lissajous/R3bpLissajousSolverException.hpp"
#include "R3bpOrbitException.hpp"

// ------------------------------------------------------------------------
/** Set analytical solution parameters */
void OrbitAnalytical::set_parameters(double Ax_, double phix_, double Az_, double phiz_) {
    Az = Az_;
    Ax = Ax_;
    phix = phix_;
    phiz = phiz_;
}
// ------------------------------------------------------------------------
/** Compute linear solution frequencies, for orbit around collinear points */
void OrbitAnalytical::get_analytical_frequencies(const Vector6 &ep, double &omega, double &nu) {
    if ((librationPoint.id == L4) ||  (librationPoint.id == L5)) {
        throw R3bpOrbitException("Orbit of type {LYAPUNOV, HALO, LISSAJOUS} cannot be computed for L4 and L5!");
    }

    MATRIXVAR Df = MATRIXVAR::Zero(6, 6);
    CommonVariables cv;
    real_type X[7];
    X[0] = ep(0); X[1] = ep(1); X[2] = ep(2); X[3] = ep(3); X[4] = ep(4); X[5] = ep(5); X[6] = 1;

    EllipticThreeBodyProblem *dynamics = new EllipticThreeBodyProblem(librationPoint.mu, ThrusterData());
    if (dynamics->ComputeCommonVariablesForStateOnly(0, X, cv)) {
        real_type x_dot[7];
        dynamics->GetJacobianDynamics(0, X, cv, x_dot, Df);
    }

    delete dynamics;
    double b1 = 2. - 0.5 * (Df(3, 0) + Df(4, 1));
    double b2sq = -Df(3, 0) * Df(4, 1);
    omega = sqrt(b1 + sqrt(b1 * b1 + b2sq));
    nu = sqrt(fabs(Df(5, 2)));
}

// ------------------------------------------------------------------------
double OrbitAnalytical::get_analytical_period() {
    Vector6 ep = librationPoint.get_state();
    double omega, nu;
    get_analytical_frequencies(ep, omega, nu);
    return 2 * M_PI / omega;
}
// ------------------------------------------------------------------------
double OrbitAnalytical::get_inplane_frequency() {
    double omega, nu;
    get_analytical_frequencies(librationPoint.get_state(), omega, nu);
    return omega;
}
// ------------------------------------------------------------------------
/** Construct list of orbit segments, computing using analytical linear approximation of the solution */
std::vector<Segment> OrbitAnalytical::get_analytical_segments(int n_segments) {
    if (n_segments == 0) {
        throw R3bpLissajousSolverException("The number of segments cannot be 0.");
    }

    std::vector<Segment> segments;

    double period = get_analytical_period();
    double step = period / static_cast<double>(n_segments);

    double x_guess[7];
    double ti, t = 0;
    Vector3 ri, vi, rf, vf;
    for (int idx = 0; idx < n_segments + 1; ++idx) {
        // this function is generally overloaded, otherwise, first order approximation is used.
        interpolate_initial_conditions(t, x_guess);
        rf << x_guess[0], x_guess[1], x_guess[2];
        vf << x_guess[3], x_guess[4], x_guess[5];
        double tf = t;
        if (idx > 0) {
            Segment segment = Segment(ri, vi, rf, vf, ti, tf);
            segments.push_back(segment);
        }

        ti = tf;
        ri = rf;
        vi = vf;
        t += step;
    }

    return segments;
}
