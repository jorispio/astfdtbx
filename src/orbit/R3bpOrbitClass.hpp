// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_CLASS_HPP_
#define __R3BP_ORBIT_CLASS_HPP_

#include "LibrationPoint.hpp"
#include "orbit/continuation/OrbitContinuityCondition.hpp"
#include "OrbitAnalytical.hpp"
#include "OrbitStability.hpp"
#include "R3bpOrbitException.hpp"
#include "OrbitSolution.hpp"
#include "SpaceMechanicsToolBox.hpp"


/** Orbit types which have a dedicated class
 */
enum OrbitType {
    LYAPUNOV, LISSAJOUS, LISSAJOUS_EIGHT, HALO, DRO, TRIANGULAR
};

/** Parent class for all orbit classes. It implements basic method common to all orbit classes. */
class R3bpOrbitClass : public OrbitStability, public OrbitAnalytical {
 protected:
    OrbitType orbit_type;
    TrajectoryPropageRtbp *propagator;
    double init_date_mjd;
    std::vector<OrbitContinuityCondition> continuation_history;

 public:
    OrbitSolution orbitSolution;
    LibrationPointProperty librationPoint;

 public:
    R3bpOrbitClass(PrimaryPointId primary, OrbitType orbit_type)
            : OrbitStability(), OrbitAnalytical(primary), orbit_type(orbit_type) { }

    R3bpOrbitClass(const LibrationPointProperty& librationPoint_, OrbitType orbit_type = LISSAJOUS)
        : OrbitStability(), OrbitAnalytical(librationPoint_), orbit_type(orbit_type), librationPoint(librationPoint_) {
        init_date_mjd = 51544;  // 01/01/2000
    }

    virtual ~R3bpOrbitClass() { }

    void setOrbitType(OrbitType orbit_type_) {
        orbit_type = orbit_type_;
    }

    /**
     * Set initial date, MJD. In the CRTBP, propagation of orbit is not dependant on the epoch.
     */
    void set_init_date(double t_mjd) {
        init_date_mjd = t_mjd;
    }

    Vector6 get_initial_state() const {
        return orbitSolution.get_initial_state();
    }

    inline double get_mu_ratio() {
        return librationPoint.mu;
    }

    OrbitSolution get_orbit_property() const {
        return orbitSolution;
    }

    virtual OrbitSolution find(double* ,
            /* options */
            bool, uint, double) {
        throw R3bpOrbitException("find not implemented!");
    }
};

// ------------------------------------------------------------------------
#endif  // __R3BP_ORBIT_CLASS_HPP_
