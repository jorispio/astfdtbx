// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_PRIMARY_POINT_ORBIT_HPP_
#define __R3BP_PRIMARY_POINT_ORBIT_HPP_

#include "orbit/R3bpOrbitClass.hpp"
#include "PrimaryPoint.hpp"

class R3bpPrimaryPointOrbit : public R3bpOrbitClass {
 public:
    R3bpPrimaryPointOrbit(PrimaryPointId primary, OrbitType orbit_type) : R3bpOrbitClass(primary, orbit_type) {
    }
};

#endif  // __R3BP_PRIMARY_POINT_ORBIT_HPP_
