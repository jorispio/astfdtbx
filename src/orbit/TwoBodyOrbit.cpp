// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "TwoBodyOrbit.hpp"

#include "propagator/detectors/AxisCrossingDetector.hpp"
#include "R3bpProblemException.hpp"

 // ------------------------------------------------------------------------
 /**
  * Construct a two-body orbit around the primary or secondary body of the
  * restricted three-body problem.
  */
TwoBodyOrbit::TwoBodyOrbit(double mu_ratio, PrimaryPointId primaryPointId_)
    : R3bpPrimaryPointOrbit(primaryPointId_, TWO_BODY_ORBIT, mu_ratio), mu_ratio(mu_ratio), primaryPointId(primaryPointId_) {

    problem = new R3bProblem(mu_ratio);
    frame = new R3bpPrimaryBodyFrame(problem, primaryPointId_);
    r3bp_frame = new R3bpFrame(problem);
}
// ------------------------------------------------------------------------
/**
 * Construct a two-body orbit around the primary or secondary body of the
 * restricted three-body problem.
 */
TwoBodyOrbit::TwoBodyOrbit(double mu_ratio, const KeplerianOrbit *keplerianOrbit, PrimaryPointId primaryPointId_)
    : R3bpPrimaryPointOrbit(primaryPointId_, TWO_BODY_ORBIT, mu_ratio), mu_ratio(mu_ratio), primaryPointId(primaryPointId_) {

}

// ------------------------------------------------------------------------
TwoBodyOrbit::~TwoBodyOrbit() {
    delete problem;
    delete frame;
    delete r3bp_frame;
}

// ------------------------------------------------------------------------
/**
 */
bool TwoBodyOrbit::set_orbit(double sma, double ecc, double inc, double aop, double raan) {
    this->sma = sma;
    this->ecc = ecc;
    this->inc = inc;
    this->aop = aop;
    this->raan = raan;
    return true;
}
// ------------------------------------------------------------------------
bool TwoBodyOrbit::get_position_velocity(double t, Vector3& pos, Vector3& vel) const {

    return false;
}
// ------------------------------------------------------------------------
/**
 */
bool TwoBodyOrbit::interpolate_initial_conditions(double, double* x_guess) {
    throw R3bpProblemException("No implementation");
    return false;
}
// ------------------------------------------------------------------------
/**
 */
bool TwoBodyOrbit::get_initial_conditions() {

    KeplerianOrbit *kep = new KeplerianOrbit(sma, ecc, inc, aop, raan, 0, MEAN_ANOMALY, frame, GenericDate(), get_mu());
    CartesianCoordinates car = kep->getCartesianCoordinates(r3bp_frame);
    orbitSolution.period = 2 * M_PI * sqrt(sma * sma * sma / get_mu());

    orbitSolution.r0 = car.getPosition();
    orbitSolution.v0 = car.getVelocity();

    delete kep;
    return true;
}
// ------------------------------------------------------------------------
OrbitSolution TwoBodyOrbit::get_orbit_solution() {    
    get_initial_conditions();

    // fill in a property structure for other codes
    orbitSolution.type = "two_body";
    orbitSolution.mu = mu_ratio;


    // initial position
    // pos = [Lx; Ly; Lz] + dpos;
    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM;
    orbitSolution.monodromy = STM;  // monodromy matrix
    orbitSolution.computeStabilityIndices();
    orbitSolution.Ax = 0;
    orbitSolution.Ay = 0;
    orbitSolution.Az = 0;
    orbitSolution.phi_x = 0;
    orbitSolution.phi_y = 0;
    orbitSolution.phi_z = 0;

    return orbitSolution;
}
