// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LissajousOrbit.hpp"

#include <math.h>

#include "Jacobi.hpp"
#include "lissajous/R3bpLissajousSolverException.hpp"

#define eps 1e-12

// #define DEBUG_LISSAJOUS
LissajousOrbit::LissajousOrbit(const LibrationPointProperty& librationPoint)
                : R3bpOrbitClass(librationPoint, LISSAJOUS), LinearSolution(librationPoint) {
    solver = new TimeSymmetricOrbitProblemSolver(librationPoint.mu,
                                StateFilter(V_RX, V_VY, V_TIME),
                                ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
}
// ------------------------------------------------------------------------
/**
 * First order analytical approximation, for orbits around collinear points.
 *
 * @param Ax   amplitude input for analytical solution
 * @param Az   amplitude input for analytical solution
 * @param phix phasing angle input for analytical solution
 * @param phiz phasing angle input for analytical solution
 *
 */
bool LissajousOrbit::interpolate_initial_conditions(double t,
                                                    /* outputs */
                                                    double *x_guess) {
    Vector6 eq = librationPoint.get_state();
    double gamma = librationPoint.gamma();  // gamma

    double omega, nu;
    get_analytical_frequencies(eq, omega, nu);
    double c2_ = librationPoint.calc_c2();
    double k = (1. + 2 * c2_ + omega * omega) / (2. * omega);

    double cos_a = cos(omega * t + phix);
    double sin_a = sin(omega * t + phix);
    double cos_b = cos(nu * t + phiz);
    double sin_b = sin(nu * t + phiz);

    // first order approximation
    x_guess[0] = Ax * cos_a;
    x_guess[1] =-Ax * k * sin_a;
    x_guess[2] = Az * cos_b;
    x_guess[3] =-Ax * omega * sin_a;
    x_guess[4] =-Ax * k * omega * cos_a;
    x_guess[5] =-Az * nu * sin_b;

    // place around the libration point
    for (int idx = 0;  idx < 6; ++idx) {
        x_guess[idx] *= gamma;
    }
    double g = librationPoint.gl();
    x_guess[0] += g;

#ifdef DEBUG_LISSAJOUS
    double a_m = 1;
    printf("mu=%.8f; frequencies omega=%.8f nu=%.8f k=%.8f g=%.8f c2=%.8f\n", mu_ratio, omega, nu, k, g, c2_);
    printf("Initial Amplitude: Ax= %f\n", Ax * a_m);
    printf("Initial Amplitude: Ay= %f\n", k * Ax * a_m);
    printf("Initial Amplitude: Az= %f\n", Az * a_m);
    printf("omega = %f, Period=%f\n", omega, 2 * M_PI / omega);
    printf("c2 = %f  sqrt(c2) = %f\n", c2_, sqrt(c2));
#endif

    return true;
}
// ------------------------------------------------------------------------
/**
 * Compute the state initial condition for the Halo orbit, given the x-
 * and z-excusion, and the pulsation.
 * Internal variables are set by computeThirdOrderApproximation(), so this
 * function shall not be called directly.
 *
 * @param n_branch phase constraint. n = 1,3 for northern or southern solutions respectively
 */
// ------------------------------------------------------------------------
bool LissajousOrbit::get_initial_conditions(double Ax_, double phix_,
                                            double Az_, double phiz_,
                                            double omega_, double gamma_,
                                            /* outputs */
                                            double *x_guess, double &Tguess) {

    set_parameters(Ax_, phix_, Az_, phiz_);
    Tguess = 1; // FIXME
    return interpolate_initial_conditions(0, x_guess);
}

// ------------------------------------------------------------------------
/**
 * The trajectory is split in n_segments segments. Two consecutive
 * segments are patch with a patch point describing the desired position and
 * the delta-velocity.
 * The objective is to find a trajectory describing a Lissajous orbit, where there
 * is no position defect at patch point, and the delta-velocities are null.
 *
 * The resolution of the Lissajous orbit is done in two-passes:
 *  - the first pass tries to solve the position defects using as correction
 *     variables the velocity at the beginning of each segment, and the duration
 *     of each segment.
 *  - the second pass tries to minimize the delta-v occuring at each patch point,
 *     using as variable the position of the patch points and the intermediate dates.
 */
OrbitSolution LissajousOrbit::find(
                            double Ax_, double phix_,
                            double Az_, double phiz_,
                            /* options */
                            bool print_iteration,
                            uint max_iterations,
                            double xtol, double solver_max_step) {
    double gamma = librationPoint.gamma();
    double xguess[6];  // [pos, vel]
    double period_guess;
    get_initial_conditions(Ax_, phix_, Az_, phiz_, omega, gamma, xguess, period_guess);

    VECTORVAR xInit = VECTORVAR::Zero(3);
    xInit << xguess[0], xguess[4], period_guess / 2.;  // rx, vy, T

    return solve(xguess, period_guess, xInit, print_iteration, max_iterations, xtol, solver_max_step);
}
// ------------------------------------------------------------------------
/**
* The continuation parameter is the z-amplitude.
 */
OrbitSolution LissajousOrbit::find_by_continuation(double parameter,
        const OrbitSolution& initialGuess,
        /* options */
        bool print_iteration,
        uint max_iterations,
        double xtol,
        double solver_max_step) {
    // initial condition
    double *x_guess = &(initialGuess.get_initial_state())(0);
    double period_guess = initialGuess.period ;

    VECTORVAR xInit = VECTORVAR::Zero(3);
    xInit << x_guess[0], x_guess[4], period_guess / 2.;  // Rx, Vy, T
    OrbitSolution property = solve(x_guess, period_guess, xInit,
                                    print_iteration, max_iterations, xtol,
                                    solver_max_step);

    continuation_history.push_back(OrbitContinuityCondition(parameter, property));

    return property;
}
// ------------------------------------------------------------------------
OrbitSolution LissajousOrbit::solve(const double *state_init, double period_guess,
        const VECTORVAR &sol_guess,
        /* options */
        bool print_iteration,
        uint max_iterations,
        double xtol,
        double solver_max_step) {
    solver->set_initial_conditions(state_init);
//    solver->set_variable_minimum(const VECTORVAR& vminimum);
//    solver->set_variable_maximum(const VECTORVAR& vmaximum);
//    solver->set_maximum_step(const VECTORVAR& max_step_);

    VECTORVAR xs = VECTORVAR::Zero(3);
    xs << sol_guess(0), sol_guess(1), period_guess / 2.;
    int ret = solver->solve(sol_guess, xs, print_iteration, max_iterations, xtol, 10 * max_iterations, solver_max_step);
    orbitSolution.is_valid = ret >= 1;

    // post process and compile outputs from the solution
    MATRIXVAR jacobian = MATRIXVAR::Zero(3, 3);
    VECTORVAR zz = VECTORVAR::Zero(3);
    Matrix6 phi_stm;
    solver->zerof(xs, zz, jacobian, phi_stm);
    double period = solver->get_duration(xs);
    conclude(xs, period, phi_stm);

    return orbitSolution;
}
// ------------------------------------------------------------------------
void LissajousOrbit::conclude(const Vector6 &xs, double period, const Matrix6 &phi) {
    // fill in a property structure for other codes
    orbitSolution.librationPoint = librationPoint;
    orbitSolution.type = "lissajous";
    orbitSolution.mu = librationPoint.mu;

    // initial position
    // pos = [Lx; Ly; Lz] + dpos;
    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM = A * phi.inverse() * A * phi;
    orbitSolution.monodromy = STM;        // monodromy matrix

    orbitSolution.period  = period;
    orbitSolution.r0 = xs.segment(0, 3);
    orbitSolution.v0 = xs.segment(3, 3);
    orbitSolution.Ax = Ax;
    orbitSolution.Ay = 0;
    orbitSolution.Az = Az;
    orbitSolution.phi_x = phix;
    orbitSolution.phi_y = 0;
    orbitSolution.phi_z = phiz;
}
