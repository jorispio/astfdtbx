// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "BaseOrbitFinder.hpp"

#include <algorithm>
#include <functional>
#include <iostream>
#include <string>
#include <vector>

#include "orbit/R3bpOrbitException.hpp"

// ------------------------------------------------------------------------
/**
 * Solve the periodic symmetric problem where we target y(tf) = 0 and vx(tf) = 0
 * varying on initial condition vy0
 */
// ------------------------------------------------------------------------
BaseOrbitFinder::BaseOrbitFinder(double mu_ratio, const StateFilter &state_filter,
                                const ConstraintDerivativeFilter &constraint_filter) {
    problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    solvingMethod = SolvingMethod::BACKWARD_FORWARD;

    setStateFilter(state_filter);
    setConstraintFilter(constraint_filter);

    funZero = VECTORVAR::Zero(constraintFilter.size());
    matJacobian = MATRIXVAR::Zero(constraintFilter.size(), stateFilter.size());

    is_initial_condition_set = false;
}
// ------------------------------------------------------------------------
BaseOrbitFinder::~BaseOrbitFinder() {
    if (problem != NULL) {
        delete problem;
    }
    if (propagator != NULL) {
        delete propagator;
    }
}
// ------------------------------------------------------------------------
/** Set the solving formulation method, among:
     single shooting
     backward-forward
 */
void BaseOrbitFinder::set_solving_method(SolvingMethod solvingMethod_) {
    solvingMethod = solvingMethod_;
}
// ------------------------------------------------------------------------
/** Set the orbit initial conditions, that will be used, resp. partly updated
 * when integrating, resp. finding, an orbit
 */
void BaseOrbitFinder::set_initial_conditions(const double *x_init_) {
    memcpy(x_init, x_init_, 6 * sizeof(double));
    is_initial_condition_set = true;
}
// ------------------------------------------------------------------------
Vector3 BaseOrbitFinder::get_initial_position(const VECTORVAR &x) {
    Vector3 r0;
    int idx1 = stateFilter.index_of(V_RX);
    int idx2 = stateFilter.index_of(V_RY);
    int idx3 = stateFilter.index_of(V_RZ);
    r0 << ((idx1 >= 0)? x(idx1) : x_init[0]),
          ((idx2 >= 0)? x(idx2) : x_init[1]),
          ((idx3 >= 0)? x(idx3) : x_init[2]);
    return r0;
}
// ------------------------------------------------------------------------
/** Compute the initial velocity from the decision vector and the state filter
 */
Vector3 BaseOrbitFinder::get_initial_velocity(const VECTORVAR &x) {
    Vector3 v0;
    int idx1 = stateFilter.index_of(V_VX);
    int idx2 = stateFilter.index_of(V_VY);
    int idx3 = stateFilter.index_of(V_VZ);
    v0 << ((idx1 >= 0)? x(idx1) : x_init[3]),
          ((idx2 >= 0)? x(idx2) : x_init[4]),
          ((idx3 >= 0)? x(idx3) : x_init[5]);
    return v0;
}
// ------------------------------------------------------------------------
double BaseOrbitFinder::get_half_duration(const VECTORVAR &x) {
    int idx1 = stateFilter.index_of(V_TIME);
    if (idx1 < 0) {
        throw R3bpOrbitException("Not time variable for the problem!");
    }
    return fabs(x(idx1));
}
// ------------------------------------------------------------------------
/** valuefun Value function. This function is called by DifferentialCorrector.
    x      decision vector
    iret   true to stop the solver.
    @return the function value at x.
*/
// ------------------------------------------------------------------------
bool BaseOrbitFinder::valuefun(const VECTORVAR &x, VECTORVAR& f) {
    Matrix6 phix;
    zerof(x, f, matJacobian, phix);
    funZero = f;
    return true;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 * This function is called by DifferentialCorrector.
 */
// ------------------------------------------------------------------------
int BaseOrbitFinder::jacobian(const VECTORVAR &x, MATRIXVAR& Fjac) {
    Matrix6 phix;
    zerof(x, funZero, Fjac, phix);
    matJacobian = Fjac;
    return 0;
}


// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the inital y-component of the velocity
 *
 */
void BaseOrbitFinder::zerof(const VECTORVAR& x,
    /* outputs */
    VECTORVAR& z, MATRIXVAR& jac, Matrix6& phix) {
    zerof_single_shooting(x, z, jac, phix);
}
// ------------------------------------------------------------------------
void BaseOrbitFinder::zerof_single_shooting(const VECTORVAR &x,
                            /* outputs */
                            VECTORVAR &z, MATRIXVAR &jac, Matrix6 &phix) {
    Vector3 r0 = get_initial_position(x);
    Vector3 v0 = get_initial_velocity(x);
    double dt = BaseOrbitFinder::get_half_duration(x);

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, dt,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    phix = STM.block(0, 0, 6, 6);  // stm matrix after one revolution
    Vector7 rvf; rvf << rf, vf, dt;

    process_variable_constraints(x, rvf, phix, z, jac);
}
// ------------------------------------------------------------------------
void BaseOrbitFinder::zerof_backward_forward(const VECTORVAR& x,
    /* outputs */
    VECTORVAR& z, MATRIXVAR& J, Matrix6& phix) {

    Vector3 r0 = get_initial_position(x);
    Vector3 v0 = get_initial_velocity(x);
    double dt_2 = 0.5 * BaseOrbitFinder::get_duration(x);
    
    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, dt_2,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    phix = STM.block(0, 0, 6, 6);  // stm matrix after one revolution
    Vector7 rv_m1; rv_m1 << rf, vf, dt_2;
    
    
    Vector7 rv_m2;     
    // TODO
}

// ------------------------------------------------------------------------
void BaseOrbitFinder::process_variable_constraints(const VECTORVAR &x, const VECTORVAR &rvf, const MATRIXVAR &stmf,
                                                    VECTORVAR &z, MATRIXVAR &jac) {
                                                   
    // slicing
    int irow = 0;
    for (std::vector<ConstraintCoordinate>::iterator ipsi = constraintFilter.begin(); ipsi != constraintFilter.end(); ++ipsi) {
        if (*ipsi == C_CUSTOM) {
            if (custom_constraint == nullptr) {
                throw R3bpOrbitException("Custom constraint not set!");
            }
            z(irow) = custom_constraint(x, rvf); // default custom constraint value
        }
        else {
            z(irow) = rvf(*ipsi);
        }
        ++irow;
    }

    int jcol = 0;
    for (std::vector<Variable>::iterator ivar = stateFilter.begin(); ivar != stateFilter.end(); ++ivar) {
        VECTORVAR dconstraint;
        if (*ivar == V_TIME) {
            dconstraint = compute_time_sensitivities(0, rvf);
        } else {
            dconstraint = stmf.col(*ivar);
        }

        irow = 0;
        for (std::vector<ConstraintCoordinate>::iterator ipsi = constraintFilter.begin(); ipsi != constraintFilter.end(); ++ipsi) {
            if (*ipsi == C_CUSTOM) {
                if (custom_constraint_gradient == nullptr) {
                    throw R3bpOrbitException("Custom constraint gradient not set!");
                }
            
                VECTORVAR grad = custom_constraint_gradient(x, rvf, stmf);
                jac(irow, jcol) = grad(*ivar); // custom constraint gradient
            }
            else {
                jac(irow, jcol) = dconstraint(*ipsi);
            }
            ++irow;
        }
        ++jcol;
    }
}
// ------------------------------------------------------------------------
VECTORVAR BaseOrbitFinder::compute_time_sensitivities(double dt, const VECTORVAR &xf) {
    CommonVariables cV;
    real_type af[7];
    const real_type *x_array = &xf(0);
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array, cV)) {
        problem->GetStateDynamics(dt, cV, af);
    }
    return Map<Matrix<double, 7, 1> >(af);
}
// ------------------------------------------------------------------------
int BaseOrbitFinder::solve(const VECTORVAR& x0, VECTORVAR& sol, bool verbose,
                            int maxiter,
                            double tol,
                            int maxfunevals, double alphaInit) {
    if ((constraintFilter.size() == 0) || (stateFilter.size() == 0)) {
        throw R3bpOrbitException("Problem is not defiled! Add constraint and/or unknown state variable");
    }
    if (!is_initial_condition_set) {
        throw R3bpOrbitException("Solver initial conditions not set!");
    }
    double xtol = 1e-10;
    funZero = VECTORVAR::Zero(constraintFilter.size());
    matJacobian = MATRIXVAR::Zero(constraintFilter.size(), stateFilter.size());
    return DifferentialCorrector::solve(x0, sol, verbose, maxiter, tol, xtol, maxfunevals, alphaInit);
}
