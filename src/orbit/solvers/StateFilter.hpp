// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_STATE_DERIVATIVE_FILTER_HPP_
#define __R3BP_STATE_DERIVATIVE_FILTER_HPP_

#include <algorithm>  // std::find
#include <iterator>  // std::distance
#include <vector>

#include <Eigen/Core>
#include "SpaceMechanicsToolBox.hpp"

enum Variable {
    V_RX = 0,
    V_RY = 1,
    V_RZ = 2,
    V_VX = 3,
    V_VY = 4,
    V_VZ = 5,
    V_TIME = 6
};


class StateFilter : public std::vector<Variable> {
 public:
    StateFilter() { }
    ~StateFilter() { }

    StateFilter(Variable&& var1) {
        add(var1);
    }

    template<class ...Args>
    StateFilter(Variable&& var1, Args&&... variables) {
        add(var1);
        add(variables...);
    }

    void add(Variable var) { push_back(var); }

    template<class ...Args>
    void add(Variable var, Args&&... variables) {
        push_back(var);
        add(variables...);
    }

    Eigen::Index operator[] (Eigen::Index i) const {
        return (Eigen::Index) at(i);
    }

    Eigen::Index size() const {
        return (Eigen::Index) std::vector<Variable>::size();
    }

    int index_of(Variable variable) const {
        auto it = std::find(begin(), end(), variable);
        if (it == end()) {
            return -1;
        }
        return (int) std::distance(begin(), it);
    }

    /** Construct the decision vector */
    VECTORVAR vector(const double* x, double time) {
        VECTORVAR vec = VECTORVAR::Zero(size());
        int idx = 0;
        for (std::vector<Variable>::iterator i = begin(); i != end(); ++i) {
            if (*i == V_TIME) {
                vec(idx) = time;
            } else {
                vec(idx) = x[*i];
            }
            ++idx;
        }
        return vec;
    }
};

#endif  // __R3BP_DERIVATIVE_FILTER_HPP_
