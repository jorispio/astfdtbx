// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_BASE_ORBIT_FINDER_HPP__
#define __R3BP_BASE_ORBIT_FINDER_HPP__

#include <functional>  // std::function

#include "ConstraintDerivativeFilter.hpp"
#include "solver/DifferentialCorrector.hpp"
#include "SolvingMethod.hpp"
#include "SpaceMechanicsToolBox.hpp"
#include "StateFilter.hpp"

/** Base class for orbit solvers. Implement the differential corrector for constraints solving.
 * Child class shall implement the initial condition functions.
 */
class BaseOrbitFinder: public DifferentialCorrector {
 protected:
    double x_init[6];
    SolvingMethod solvingMethod;
    StateFilter stateFilter;
    ConstraintDerivativeFilter constraintFilter;

 private:
    EllipticThreeBodyProblem *problem;
    StateTransitionMatrixRtbp *propagator;

    double parameter;
    bool is_initial_condition_set;

    VECTORVAR funZero;
    MATRIXVAR matJacobian;

    void zerof_single_shooting(const VECTORVAR& x,
        /* outputs */
        VECTORVAR& z, MATRIXVAR& J, Matrix6& phix);

    void zerof_backward_forward(const VECTORVAR& x,
        /* outputs */
        VECTORVAR& z, MATRIXVAR& J, Matrix6& phix);

    VECTORVAR compute_time_sensitivities(double tf, const VECTORVAR &xf);
    
    void process_variable_constraints(const VECTORVAR &x, const VECTORVAR &xf, const MATRIXVAR &stmf,
                VECTORVAR &z, MATRIXVAR &J);

    /** custom constraint function. */
    std::function<double(const VECTORVAR&,const VECTORVAR&)> custom_constraint;
    /** custom constraint gradient function. */    
    std::function<VECTORVAR(const VECTORVAR&, const VECTORVAR&, const MATRIXVAR&)> custom_constraint_gradient;
        
 public:
    /** Default constructor. */
    BaseOrbitFinder(double mu_ratio,
                    const StateFilter &state_filter,
                    const ConstraintDerivativeFilter &constraint_filter);
    ~BaseOrbitFinder();

    /** Set the resolution method. */
    void set_solving_method(SolvingMethod solvingMethod_);

    /** Return the number of variable (state vector size). */
    uint get_n() { return (uint) stateFilter.size(); }

    /** Return the number of constraints (constraint function dimension). */
    uint get_m() { return (uint) constraintFilter.size(); }

    void set_parameter(double parameter_) { parameter = parameter_; }
    double get_parameter() { return parameter; }

    /** Set state derivative filter. It defines all the unknowns of the problem. */
    void setStateFilter(const StateFilter &filter) { 
        stateFilter = filter; 
        matJacobian = MATRIXVAR::Zero(constraintFilter.size(), stateFilter.size());
    }

    /** Set constraint derivative filter. It defines the constraints to solve.  */
    void setConstraintFilter(const ConstraintDerivativeFilter &filter) { 
        constraintFilter = filter; 
        funZero = VECTORVAR::Zero(constraintFilter.size());
        matJacobian = MATRIXVAR::Zero(constraintFilter.size(), stateFilter.size());        
    }

    /** Set custom constraint. C_CUSTOM shall be used when setting constraint filter. */
    void set_custom_constraint(const std::function<double(const VECTORVAR&, const VECTORVAR&)>& custom_constraint_, 
                               const std::function<VECTORVAR(const VECTORVAR&, const VECTORVAR&, const MATRIXVAR&)>& custom_constraint_gradient_) {
        custom_constraint = custom_constraint_;
        custom_constraint_gradient = custom_constraint_gradient_;
    }
    
    /** Return instantiated propagator */
    StateTransitionMatrixRtbp* get_propagator() const { return propagator; }

    /** Set initial initial conditions */
    void set_initial_conditions(const double *x_init_);

    virtual Vector3 get_initial_position(const VECTORVAR &x);
    virtual Vector3 get_initial_velocity(const VECTORVAR &x);
    double get_half_duration(const VECTORVAR &x);
    virtual double get_duration(const VECTORVAR& x) { return 2 * get_half_duration(x); }
    
    void zerof(const VECTORVAR &x,
            /* outputs */
            VECTORVAR &z, MATRIXVAR &jacobian, Matrix6 &phix);

    /** Value function (constraints).  */
    bool valuefun(const VECTORVAR& x, VECTORVAR& f) override;  // c++11

    /** jacobian of the constraints */
    int jacobian(const VECTORVAR &x, MATRIXVAR& Fjac) override;  // c++11

    /** Solve the problem */
    int solve(const VECTORVAR& x0, VECTORVAR& sol,
                bool verbose = true,
                int maxiter = 100,
                double tol = 1e-8,
                int maxfunevals = 5000,
                double alphaInit = 1);
};

#endif  // __R3BP_BASE_ORBIT_FINDER_HPP
