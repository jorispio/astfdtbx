// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "PeriodicOrbitProblemSolver.hpp"

#include <algorithm>
#include <cstring>
#include <functional>
#include <iostream>
#include <limits>
#include <string>
#include <vector>

// ------------------------------------------------------------------------
PeriodicOrbitProblemSolver::PeriodicOrbitProblemSolver(double mu_ratio_)
    : RAPHSON(), mu_ratio(mu_ratio_) {
    problem = new EllipticThreeBodyProblem(mu_ratio, ThrusterData());
    propagator = new StateTransitionMatrixRtbp(mu_ratio);
    matJacobian = Matrix3::Zero();
}
// ------------------------------------------------------------------------
PeriodicOrbitProblemSolver::~PeriodicOrbitProblemSolver() {
    if (problem != NULL) {
        delete problem;
    }
    if (propagator != NULL) {
        delete propagator;
    }
}
// ------------------------------------------------------------------------
/** Set the orbit initial conditions, that will be used, resp. partly updated
 * when integrating, resp. finding, an orbit
 */
void PeriodicOrbitProblemSolver::set_initial_conditions(const double *x_init_) {
    memcpy(x_init, x_init_, 6 * sizeof(double));
}

// ------------------------------------------------------------------------
/** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
*/
// ------------------------------------------------------------------------
VECTORVAR PeriodicOrbitProblemSolver::valuefun(const VECTORVAR& x, bool& iret) {
    iret = false;
    Vector3 z;
    Matrix6 phix;
    zerof(x, z, matJacobian, phix);
    return z;
}

// ------------------------------------------------------------------------
/**
 * Jacobian of valuefun() for the zero-finding solver.
 */
// ------------------------------------------------------------------------
int PeriodicOrbitProblemSolver::jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) {
    Vector3 z;
    double T;
    Matrix6 phix;
    zerof(x, z, matJacobian, phix);
    Fjac = matJacobian;
    return 0;
}
// ------------------------------------------------------------------------
Vector3 PeriodicOrbitProblemSolver::get_initial_position(const VECTORVAR &x) {
    Vector3 r0;
    double x0 = x_init[0];
    double y0 = x_init[1];
    double z0 = x_init[2];
    r0 <<  x0, y0, z0;
    return r0;
}
// ------------------------------------------------------------------------
Vector3 PeriodicOrbitProblemSolver::get_initial_velocity(const VECTORVAR &x) {
    Vector3 v0;
    double x0_dot = x(0);
    double y0_dot = x(1);
    v0 << x0_dot, y0_dot, 0;
    return v0;
}
// ------------------------------------------------------------------------
double PeriodicOrbitProblemSolver::get_duration(const VECTORVAR &x) {
    return 2. * x(2);
}
// ------------------------------------------------------------------------
/**
 * The unknown are the orbit period and the inital point (y,z)
 * The periodic Halo orbit should be symmetric with the x-z plane. Thus,
 * considering only half the orbit, the desired terminal conditions are of
 * the form
 *     [xd, 0, zd, 0, vyd, 0]
 * The same applies for the initial conditions then.
 * A is a parameter giving the size of the Halo.
 *
 */
Vector3 PeriodicOrbitProblemSolver::zerof(const VECTORVAR &x,
        /* outputs */
        Vector3 &z, Matrix3 &J, Matrix6 &phix) {
    Vector3 r0 = get_initial_position(x);
    Vector3 v0 = get_initial_velocity(x);
    double dt = get_duration(x);  // here we take one full revolution, as the orbit is not necessarily symmetric

    Matrix3 STM1, STM2, STM3, STM4;
    Matrix7 STM;
    Vector3dExt P1, P2;
    double P3;
    Vector3 rf, vf;
    propagator->getSTM(r0, v0,
               0, dt,
               STM1, STM2, STM3, STM4,
               STM,
               P1, P2, P3,
               rf, vf);
    phix = STM.block(0, 0, 6, 6);  // stm matrix after one revolution

    // periodicity - same point after a period dt
    z <<  rf(0) - x_init[0],
          vf(0) - x(0),   // yf = 0 to be back on the x-z plane
          vf(1) - x(1);   // vzf = 0

    CommonVariables cV;
    real_type af[7];
    VECTORVAR xf = VECTORVAR::Zero(7); xf << rf(0), rf(1), rf(2), vf(0), vf(1), vf(2), 1;
    const real_type *x_array = &xf(0);
    if (problem->ComputeCommonVariablesForStateOnly(0, x_array, cV)) {
        problem->GetStateDynamics(dt, cV, af);
    }

    J << phix(0, 3), phix(0, 4), vf(0),
         phix(3, 3) - 1, phix(3, 4), af[3],
         phix(4, 3), phix(4, 4) - 1, af[4];

    return z;
}
