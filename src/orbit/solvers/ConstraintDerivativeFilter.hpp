// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_CONSTRAINT_DERIVATIVE_FILTER_HPP_
#define __R3BP_CONSTRAINT_DERIVATIVE_FILTER_HPP_

#include <Eigen/Core>
#include <vector>

#include "orbit/R3bpOrbitException.hpp"
#include "orbit/solvers/StateFilter.hpp"

enum ConstraintCoordinate {
    C_RX = 0,
    C_RY = 1,
    C_RZ = 2,
    C_VX = 3,
    C_VY = 4,
    C_VZ = 5,
    C_TIME = 6,
    C_CUSTOM = 7
};


class ConstraintDerivativeFilter : public std::vector<ConstraintCoordinate>  {
 public:
    ConstraintDerivativeFilter() { }
    ~ConstraintDerivativeFilter() { }

    template<class ...Args>
    ConstraintDerivativeFilter(ConstraintCoordinate&& var1, Args&&... varsin) {
        add(var1);
        add(varsin...);
    }

    void add(ConstraintCoordinate var) {
        if (var == C_TIME) {
            throw R3bpOrbitException("Time not allowed for constraint");
        }
        push_back(var);
    }

    template<class... Args>
    void add(ConstraintCoordinate var, Args&&... variables) {
        push_back(var);
        add(variables...);
    }

    Eigen::Index operator[] (Eigen::Index i) const {
        return (Eigen::Index) at(i);
    }

    Eigen::Index size() const {
        return (Eigen::Index) std::vector<ConstraintCoordinate>::size();
    }
};

#endif  // __R3BP_CONSTRAINT_DERIVATIVE_FILTER_HPP
