// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_PERIODIC_ORBIT_PROBLEM_SOLVER_HPP_
#define __R3BP_PERIODIC_ORBIT_PROBLEM_SOLVER_HPP_

#include "SpaceMechanicsToolBox.hpp"

/**
 * This solver solves periodic (asymmetric) orbits in the restricted three-body problem.
 */
class PeriodicOrbitProblemSolver: public RAPHSON {
 private:
    EllipticThreeBodyProblem *problem;
    StateTransitionMatrixRtbp *propagator;

    double mu_ratio;
    double x_init[6];
    Matrix3 matJacobian;

 public:
    explicit PeriodicOrbitProblemSolver(double mu_ratio);
    ~PeriodicOrbitProblemSolver();

    void set_initial_conditions(const double *x_init);

    /** valuefun Value function.
        x      decision vector
        iret   true to stop the solver.
        @return the function value at x.
    */
    VECTORVAR valuefun(const VECTORVAR& x, bool& iret) override;  // c++11

    int jacobian(const VECTORVAR& x, MATRIXVAR& Fjac) override;  // c++11

    Vector3 get_initial_position(const VECTORVAR &x);
    Vector3 get_initial_velocity(const VECTORVAR &x);
    double get_duration(const VECTORVAR &x);

    Vector3 zerof(const VECTORVAR &x,
            /* outputs */
            Vector3 &z, Matrix3 &J, Matrix6 &phix);
};

#endif  // __R3BP_PERIODIC_ORBIT_PROBLEM_SOLVER_HPP_
