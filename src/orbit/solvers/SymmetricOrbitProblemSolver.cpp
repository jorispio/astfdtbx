// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "SymmetricOrbitProblemSolver.hpp"

#include <cstring>

#include "propagator/detectors/AxisCrossingDetector.hpp"
#include "R3bpProblemException.hpp"

// ------------------------------------------------------------------------
SymmetricOrbitProblemSolver::SymmetricOrbitProblemSolver(double mu_ratio_,
                        const StateFilter &state_filter,
                        const ConstraintDerivativeFilter &constraint_filter,
                        AxisId axisId)
    : BaseOrbitFinder(mu_ratio_, state_filter, constraint_filter) {

    int idx = stateFilter.index_of(V_TIME);
    if (idx >= 0) {
        throw R3bpProblemException("This solver does not allow time variable!");
    }

    SimulationEventManager *eventManager = new SimulationEventManager();
    AxisCrossingDetector *detector = new AxisCrossingDetector();
    detector->set_axis(axisId);
    eventManager->addEventHandler(detector);
    get_propagator()->setEventHandler(eventManager);
}
