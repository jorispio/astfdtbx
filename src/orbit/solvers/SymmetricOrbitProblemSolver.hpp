// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_SYMMETRIC_ORBIT_PROBLEM_SOLVER_HPP_
#define __R3BP_SYMMETRIC_ORBIT_PROBLEM_SOLVER_HPP_

#include "BaseOrbitFinder.hpp"
#include "propagator/detectors/AxisCrossingDetector.hpp"
#include "solver/DifferentialCorrector.hpp"
#include "SpaceMechanicsToolBox.hpp"

/** Solver for the symmetric orbit problem with fixed time
 * Initial position and velocity are adjusted during the orbit search process.
 * Time is corrected with event detector with plane crossing constraint for symmetric orbit.
 */
class SymmetricOrbitProblemSolver: public BaseOrbitFinder {
 private:
    double period;

 public:
    SymmetricOrbitProblemSolver(double mu_ratio,
                                const StateFilter &state_filter,
                                const ConstraintDerivativeFilter &constraint_filter,
                                AxisId symmetryAxisId = Y_AXIS);
    ~SymmetricOrbitProblemSolver() { }

    double get_duration(const VECTORVAR&) override { return period; }
    void set_duration(double period_) { period = period_; }
};


// ------------------------------------------------------------------------
/**
 * Solve the periodic symmetric problem where the initial condition are such
 * that the initial motion is on a plane.
 * The initial position is fully specified with a parameter, and it is thus
 * not adjusted during the orbit search process.
 * Used by: DRO
 */
class PlanarSymmetricOrbitProblemSolver: public BaseOrbitFinder {
 public:
    PlanarSymmetricOrbitProblemSolver(double mu_ratio, double parameter,
                                        const StateFilter &state_filter,
                                        const ConstraintDerivativeFilter &constraint_filter)
        : BaseOrbitFinder(mu_ratio, state_filter, constraint_filter) {
        set_parameter(parameter);
    }
    ~PlanarSymmetricOrbitProblemSolver() { }

    Vector3 get_initial_position(const VECTORVAR&) override {
        Vector3 r0; r0 << get_parameter(), 0, 0;
        return r0;
    }

    Vector3 get_initial_position(const VECTORVAR&, double parameter) {
        Vector3 r0; r0 << parameter, 0, 0;
        return r0;
    }

    double get_duration(const VECTORVAR &x) override {
        return 2 * BaseOrbitFinder::get_half_duration(x);
    }
};

// ------------------------------------------------------------------------
/** Solver for the symmetric orbit problem with free time
 * Initial position and velocity are adjusted during the orbit search process.
 * Used by: Halo, Lyapunov, eight-shape lissajous
 */
class TimeSymmetricOrbitProblemSolver: public BaseOrbitFinder {
 public:
    TimeSymmetricOrbitProblemSolver(double mu_ratio,
                                    const StateFilter &state_filter,
                                    const ConstraintDerivativeFilter &constraint_filter)
        : BaseOrbitFinder(mu_ratio, state_filter, constraint_filter) { }
    ~TimeSymmetricOrbitProblemSolver() { }

    double get_duration(const VECTORVAR &x) override {
        return 2 * BaseOrbitFinder::get_half_duration(x);
    }
};

// ------------------------------------------------------------------------
#endif  // __R3BP_SYMMETRIC_ORBIT_PROBLEM_SOLVER_HPP_
