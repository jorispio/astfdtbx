// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "HaloOrbit.hpp"

#include <cmath>

#include "R3bpOrbitException.hpp"


//#define DEBUG_HALO

// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
HaloOrbit::HaloOrbit(const LibrationPointProperty &librationPoint_, const HaloOrbitFamily &halo_type_)
    : R3bpCollinearPointOrbit(librationPoint_, HALO), LinearSolution(librationPoint),
     family(halo_type_) {
    solver = new TimeSymmetricOrbitProblemSolver(librationPoint_.mu,
                                StateFilter(V_RX, V_VY, V_TIME),
                                ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
}
// ------------------------------------------------------------------------
//
// ------------------------------------------------------------------------
HaloOrbit::~HaloOrbit() {
    if (solver) {
        delete solver;
    }
}
// ------------------------------------------------------------------------
/**
 * Set the orbit parameter value, and compute the intermediate orbit variables.
 * This setting is mandatory before performing any interpolation with interpolate_initial_conditions().
 * @see interpolate_initial_conditions
 */
void HaloOrbit::set_parameter(double amplitude_, double parameter_phi) {
    double gamma = librationPoint.gamma();
    Az = 0;
    Ax = 0;
    double amplitude = amplitude_ / gamma;  // normalize for linearization
    phiz = parameter_phi;
    compute_parameters(librationPoint.mu, gamma, amplitude, Ax, Az, omega);
}
// ------------------------------------------------------------------------
/**
 * @return the analytical solution orbital period
 */
double HaloOrbit::get_analytical_period() {
    return 2 * M_PI / (omega * dtau_dt);
}
// ------------------------------------------------------------------------
/**
 * fix the relationship between Ax and Az for a Halo orbit.
 * See: D.L. Richardson, Analytic Construction of Periodic Orbits About the Collinear Points,
 *              Celestial Mechanics 22, 1980, 241-253
 *
 * @param Az z-amplitude of the orbit
 * @param mu gravitational constant
 * @param lambda in-plane frequency
 * @param k
 * @param gamma
 * @param c2
 */
double HaloOrbit::compute_x_amplitude(double Az, double lambda, double c2) {
    double lambda2 = lambda * lambda;
    double Delta = lambda2 - c2;

    if (Delta < 0) {
        double Az_min = sqrt(fabs(Delta / l2));
        if (Az < Az_min) {
            printf("Az is too low. No Halo orbit can exist with this amplitude! Az = %f < Az_min=%f\n", Az, Az_min);
            throw R3bpOrbitException("Az is too low");
        }
    }

    double Ax2 =-(Delta + l2 * Az * Az) / l1;
    double Ax = sqrt(Ax2);

    double Ax_min = sqrt(fabs(Delta / l1));
    if (Ax < Ax_min) {
        // non-linearity effects are too small
        printf("Halo amplitude is too low! Ax = %f < Ax_min=%f\n", Ax, Ax_min);
        throw R3bpOrbitException("Halo amplitude is too low");
    }

    return Ax;
}
// ------------------------------------------------------------------------
/**
 * fix the relationship between Ax and Az for a Halo orbit.
 * See: D.L. Richardson, Analytic Construction of Periodic Orbits About the Collinear Points,
 *              Celestial Mechanics 22, 1980, 241-253
 *
 * @param Az z-amplitude of the orbit
 * @param mu gravitational constant
 * @param lambda in-plane frequency
 * @param k
 * @param gamma
 * @param c2
 */
double HaloOrbit::compute_z_amplitude(double Ax, double lambda, double c2) {
    double lambda2 = lambda * lambda;
    double Delta = lambda2 - c2;

    if (Delta < 0) {
        double Ax_min = sqrt(fabs(Delta / l1));
        printf("Ax is too low. No Halo orbit can exist with this amplitude! Ax = %f < Ax_min=%f\n", Ax, Ax_min);
        if (Ax < Ax_min) {
            printf("Ax is too low. No Halo orbit can exist with this amplitude! Ax = %f < Ax_min=%f\n", Ax, Ax_min);
            throw R3bpOrbitException("Ax is too low");
        }
    }

    double Az2 =-(Delta + l1 * Ax * Ax) / l2;
    double Az = sqrt(Az2);
    double Az_min = sqrt(fabs(Delta / l2));
    if (Az < Az_min) {
        // non-linearity effects are too small
        printf("Halo amplitude is too low! Az = %f < Az_min=%f\n", Az, Az_min);
        throw R3bpOrbitException("Halo amplitude is too low");
    }

    return Az;
}
// ------------------------------------------------------------------------
/**
 * Halo orbit initial guess requires a third order approximation, using the
 * Lindstedt-Poincaré method, for the differential-corrector that we will
 * use to refine the initial point. This is because the dynamics are highly
 * sensitive, and the Halo orbit is actually unstable.
 * see: Richardson.
 *
 *
 */
void HaloOrbit::compute_parameters(double mu_ratio, double gamma,
                                    double amplitude,
                                    /* outputs */
                                    double &Ax, double &Az, double &lambda) {
    c2 = librationPoint.calc_c2();

    // linearized frequency
    double lambda2 = 0.5 * (-c2 + 2 + sqrt(9. * c2 * c2 - 8 * c2));
    lambda = sqrt(lambda2);

    k = (1. + 2. * c2 + lambda2) / (2. * lambda);

    compute_third_order_approximation(mu_ratio, lambda, k, gamma, c2);

    if (l1 * l2 > 0) {
        throw R3bpOrbitException("Amplitude constraint does not have a solution!");
    }

    // Amplitude constraint, and phasing
    Az = amplitude;
    Ax = compute_x_amplitude(amplitude, lambda, c2);
    //Ax = amplitude;
    //Az = compute_z_amplitude(amplitude, lambda, c2);

    // frequency correction
    dtau_dt = (1 + s1 * Ax * Ax + s2 * Az * Az);  // 1 + sum(omg_n), and ds/dt = mm = 1 (owing to normalised GM)
}
// ------------------------------------------------------------------------
bool HaloOrbit::interpolate_initial_conditions(double t,
                                                /* outputs */
                                                double *x_guess) {
    double phi = 0;  // phasing arbitrarily set to 0 accordingly with initial conditions
    double dr = 2 - static_cast<double>(family);    // switch function, comes as a factor (+/-1) to z
                                                    // to flip the Halo around the x-y plane
    double tau1 = phi + omega * t;
    double cos_phi = cos(tau1);
    double sin_phi = sin(tau1);
    double cos_2phi = cos(2. * tau1);
    double sin_2phi = sin(2. * tau1);
    double cos_3phi = cos(3. * tau1);
    double sin_3phi = sin(3. * tau1);

    // initial guess with constraint on the symmetry wrt to xz-plane
    x_guess[0] =-Ax * cos_phi                                                                           // first order
                        + a21 * Ax * Ax + a22 * Az * Az + (a23 * Ax * Ax - a24 * Az * Az) * cos_2phi    // second order
                        + Ax * (a31 * Ax * Ax - a32 * Az * Az) * cos_3phi;                              // third order
    x_guess[1] = k * Ax * sin_phi
                        + (b21 * Ax * Ax - b22 * Az * Az) * sin_2phi
                        + Ax * (b31 * Ax * Ax - b32 * Az * Az) * sin_3phi;
    x_guess[2] = dr * Az * (cos_phi
                        + d21 * Ax * (cos_2phi - 3.)
                        + (d32 * Ax * Ax - d31 * Az * Az) * cos_3phi);

    //
    x_guess[3] = Ax * omega * sin_phi
                        - (a23 * Ax * Ax - a24 * Az * Az) * 2 * omega * sin_2phi
                        - Ax * (a31 * Ax * Ax - a32 * Az * Az) * 3 * omega * sin_3phi;
    x_guess[4] = k * Ax * omega * cos_phi
                        + (b21 * Ax * Ax - b22 * Az * Az) * 2 * omega * cos_2phi
                        + Ax * (b31 * Ax * Ax - b32 * Az * Az) * 3 * omega * cos_3phi;
    x_guess[5] =-dr * Az * omega * (sin_phi
                        + d21 * Ax * 2 * sin_2phi
                        + (d32 * Ax * Ax - d31 * Az * Az) * 3 * sin_3phi);

    // place around the libration point
    double gamma = librationPoint.gamma();
    for (int idx = 0;  idx < 3; ++idx) {
        x_guess[idx] *= gamma;
        x_guess[idx + 3] *= gamma * dtau_dt;
    }
    double g = librationPoint.gl();
    x_guess[0] += g;

    return false;
}
// ------------------------------------------------------------------------
/**
 * Compute the state initial condition for the Halo orbit, given the x-
 * and z-excusion, and the pulsation.
 * Internal variables are set by computeThirdOrderApproximation(), so this
 * function shall not be called directly.
 *
 * @param n_branch phase constraint. n = 1,3 for northern or southern solutions respectively
 */
bool HaloOrbit::get_initial_conditions(double Az,
                                        double Ax, double omega, double gamma,
                                        /* outputs */
                                        double *x_guess, double &Tguess) {
    set_parameters(Ax, 0, Az, 0);
    interpolate_initial_conditions(0, x_guess);

    // estimation of the period
    Tguess = get_analytical_period();
#ifdef DEBUG_HALO
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
                x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
    printf("Az = %.8f\n", Az);
    printf("Ax = %.8f\n", Ax);
    printf("omega* = %.8f\n", omega * dtau_dt);
    printf("g = %.8f (L%d)\n", g, librationPoint.id + 1);
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n",
                x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
    printf("Tguess = %.8f \n", Tguess);

    printf("mu=%.8f omega=%.8f k=%.8f g=%.8f c2=%.8f\n", get_mu_ratio(), omega, k, g, c2);
    printf("Initial Amplitude: Ax= %.6f Ay= %.6f Az= %.6f\n", Ax, k * Ax, Az);
#endif

    return true;
}

// ------------------------------------------------------------------------
/**
 * Compute the state initial condition for the Halo orbit, given the x-
 * and z-excusion, and the pulsation.
 * Internal variables are set by computeThirdOrderApproximation(), so this
 * function shall not be called directly.
 */
// ------------------------------------------------------------------------
bool HaloOrbit::get_initial_conditions(double amplitude_,
    /* outputs */
    double *xguess, double &Tguess) {
    double gamma = librationPoint.gamma();
    //double Ax, Az;
    double amplitude = amplitude_ / gamma;  // normalize for linearization
    compute_parameters(librationPoint.mu, gamma, amplitude, Ax, Az, omega);
    get_initial_conditions(Az, Ax, omega, gamma, xguess, Tguess);

    return true;
}
// ------------------------------------------------------------------------
bool HaloOrbit::update_initial_conditions(double parameter, Variable fixed_parameter, const double *x_init,
                                            /* outputs */
                                            double *x_guess_out) {
    // TODO(joris) use jacobian dx/dp to update initial condition
    memcpy(x_guess_out, x_init, 6 * sizeof(double));
    x_guess_out[fixed_parameter] = parameter; // Az or Ax
    return true;
}
// ------------------------------------------------------------------------
/**
 * This function finds a Halo orbit for the RTBP.
 *
 * inputs:
 *   Az       z-excursion  of the orbit
 *
 * return OrbitSolution structure with:
 *   pos      initial position
 *   vel      initial velocity
 *   T        period of the orbit
 *   STM      transition matrix at T (monodromy matrix) to investigate
 *            first order stability of the orbit
 */
OrbitSolution HaloOrbit::find(double parameter,
                                /* options */
                                bool print_iteration,
                                uint max_iterations,
                                double ftol,
                                double xtol,
                                double solver_max_step) {
    double gamma = librationPoint.gamma();
    Ax = 0;
    Az = parameter;
    double amplitude = Az / gamma;  // normalize for linearization
    compute_parameters(librationPoint.mu, gamma, amplitude, Ax, Az, omega);

    double x_init_condition[6];  // [pos, vel]
    double period_guess;
    get_initial_conditions(Az, Ax, omega, gamma, x_init_condition, period_guess);

    VECTORVAR xInit = VECTORVAR::Zero(3);
    xInit << x_init_condition[0], x_init_condition[4], period_guess / 2.;  // rx, vy, T

    solver->setStateFilter(StateFilter(V_RX, V_VY, V_TIME));  // rx, vy, T, RZ == parameter
    solver->setConstraintFilter(ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
    
    return solve(x_init_condition, period_guess, xInit, Ax, print_iteration, max_iterations, ftol, xtol, solver_max_step);
}
// ------------------------------------------------------------------------
/**
* The continuation parameter is the z-amplitude.
 */
OrbitSolution HaloOrbit::find_by_continuation(const OrbitSolution& orbit_initial_guess,
                                            ContinuationProcedure& procedure,
                                            /* options */
                                            bool print_iteration,
                                            uint max_iterations,
                                            double ftol,
                                            double xtol,
                                            double solver_max_step) {

    double x_init_condition[6];
    memcpy(x_init_condition, &(orbit_initial_guess.get_initial_state())[0], 6 * sizeof(double));
    
    double period_guess = orbit_initial_guess.period;
    
    procedure.set_solver(solver);
    VECTORVAR xInit = procedure.next(orbit_initial_guess);

    try {
        solve(x_init_condition, period_guess, xInit, Ax,
                                    print_iteration, max_iterations, ftol, xtol,
                                    solver_max_step);
    }
    catch(HelioLibException &e) {
        // capture exception to not interrupt the continuation.
        orbitSolution.is_valid = false;
    }
    
    OrbitSolution property = OrbitSolution(orbitSolution);
    continuation_history.push_back(OrbitContinuityCondition(procedure.get_parameter_value(), property));

    return property;
}
// ------------------------------------------------------------------------
/**
  x_init_condition  initial conditions to update
  period_guess      initial guess on orbit period
  xInit             initial guess on unknown parameters
  az                amplitude parameter (for info only)
 */
OrbitSolution HaloOrbit::solve(const double *x_init_condition, 
                                double period_guess,
                                const VECTORVAR &x_init_guess,
                                double Az,
                                /* options */
                                bool print_iteration,
                                uint max_iterations,
                                double ftol,
                                double xtol,
                                double solver_max_step) {
    solver->set_initial_conditions(x_init_condition);

    VECTORVAR xs = x_init_guess;

    int ret = solver->solve(x_init_guess, xs, print_iteration, max_iterations, ftol, 10 * max_iterations, solver_max_step);
    orbitSolution.is_valid = ret >= 1;

    // post process and compile outputs from the solution
    MATRIXVAR jacobian = MATRIXVAR::Zero(solver->get_m(), solver->get_n());
    VECTORVAR zz = VECTORVAR::Zero(solver->get_m()); // constraints
    Matrix6 phi_stm;
    solver->zerof(xs, zz, jacobian, phi_stm);
    double period = solver->get_duration(xs);
    conclude(xs, period, phi_stm, jacobian, Az, Ax);

#ifdef DEBUG_HALO
    std::cout << "x: " << x_init_guess.transpose() << "; " << xs.transpose() << std::endl;  
    std::cout << "z  : " << zz.transpose() << std::endl;
#endif
    return orbitSolution;
}
// ------------------------------------------------------------------------
void HaloOrbit::conclude(const VECTORVAR &xs, double period, const Matrix6& phi, const MATRIXVAR& jacobian, double Az_, double Ax_) {
    // fill in a property structure for other codes
    orbitSolution.librationPoint = librationPoint;
    orbitSolution.type = "halo";
    orbitSolution.mu = get_mu_ratio();

    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM = A * phi.inverse() * A * phi;
    orbitSolution.monodromy = STM;        // monodromy matrix
    if (orbitSolution.is_valid) {
        try {
            orbitSolution.computeStabilityIndices();
        }
        catch (R3bpProblemException &e) {
            orbitSolution.is_valid = false;
        }
    }

    orbitSolution.jacobian = jacobian;
    orbitSolution.xsol = xs;

    orbitSolution.period = solver->get_duration(xs);
    orbitSolution.r0 = solver->get_initial_position(xs);
    orbitSolution.v0 = solver->get_initial_velocity(xs);

    orbitSolution.Ax = Ax * librationPoint.gamma();
    orbitSolution.Ay = 0;
    orbitSolution.Az = Az * librationPoint.gamma();
    orbitSolution.phi_x = 0;
    orbitSolution.phi_y = 0;
    orbitSolution.phi_z = 0;

    orbitSolution.omega = omega;
    orbitSolution.nu = orbitSolution.omega;
}

