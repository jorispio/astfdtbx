// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __R3BP_LISSAJOUS_ORBIT_HPP_
#define __R3BP_LISSAJOUS_ORBIT_HPP_

#include <math.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "Jacobi.hpp"
#include "orbit/OrbitApproximation.hpp"
#include "orbit/R3bpCollinearPointOrbit.hpp"
#include "solvers/SymmetricOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

/**
 * Lissajous orbits, usually around L1,2,3, are the results of linearisation of
 * the dynamical equations around one of the Libration point.
 * They do not however exactly orbit the Libration points. They are the
 * general period solution of the R3BP
 */
class LissajousOrbit : public R3bpOrbitClass, public LinearSolution {
 private:
    TimeSymmetricOrbitProblemSolver *solver;

    OrbitSolution solve(const double *xguess, double Tguess,
            const VECTORVAR &xInit,
            /* options */
            bool print_iteration,
            uint max_iterations,
            double xtol,
            double solver_max_step);

    bool get_initial_conditions(double Ax, double phix,
                                double Az, double phiz,
                                double omega, double gamma,
                                double *x_guess, double &Tguess);

 protected:
    double omega;
    void conclude(const Vector6 &xs, double period, const Matrix6 &phi);

 public:
    explicit LissajousOrbit(const LibrationPointProperty& librationPoint);
    ~LissajousOrbit() { }

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    OrbitSolution find(
                double Ax, double phix,
                double Az, double phiz,
                /* options */
                bool print_iteration = false,
                uint max_iterations = 100,
                double xtol = 1e-10, double rtol = 1e-6);

    OrbitSolution find_by_continuation(double Az,
        const OrbitSolution& initialGuess,
        /* options */
        bool print_iteration = false,
        uint max_iterations = 100,
        double xtol = 1e-6,
        double solver_max_step = 0.1);
};

// ------------------------------------------------------------------------
#endif  // __R3BP_LISSAJOUS_ORBIT_HPP_
