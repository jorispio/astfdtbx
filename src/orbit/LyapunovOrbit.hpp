// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_LYAPUNOV_ORBIT_HPP_
#define __R3BP_LYAPUNOV_ORBIT_HPP_

#include "orbit/OrbitApproximation.hpp"
#include "orbit/R3bpCollinearPointOrbit.hpp"
#include "solvers/SymmetricOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

enum LyapunovOrbitFamilyType {
    PLANAR,
    VERTICAL
};

/** Construct a Lyapunov orbit, planar or vertical */
class LyapunovOrbit : public R3bpCollinearPointOrbit, LinearSolution {
 private:
    StateFilter stateFilter;
    ConstraintDerivativeFilter constraintsFilter;
    LyapunovOrbitFamilyType planarType;
    TimeSymmetricOrbitProblemSolver *solver;

    void get_frequencies(const Vector6 &ep, double &omega, double &nu);
    double get_xpos(double x_param);
    bool update_initial_conditions(double parameter, const double *x_init,
                                            double *x_guess_out);

    void conclude(VECTORVAR &xs, const Matrix6 &phi);

    double dtau_dt;

 public:
    LyapunovOrbit(const LibrationPointProperty &librationPoint,
                    LyapunovOrbitFamilyType planarType = LyapunovOrbitFamilyType::PLANAR);
    ~LyapunovOrbit();

    void set_parameter(double parameter, double parameter_phi);

    bool get_initial_conditions(double Az,
                /* outputs */
                double *xguess, double &Tguess);

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    Matrix6 solve(const VECTORVAR &x0, VECTORVAR &xs,
            bool print_iteration,
            uint max_iterations,
            double xtol, double solver_max_step);

    OrbitSolution find(double Ax,
                bool print_iteration = false,
                uint max_iterations = 100,
                double xtol = 1e-10,
                double solver_max_step = 1.0);

    OrbitSolution find_by_continuation(double Ax,
                OrbitSolution& initialGuess,
                bool print_iteration = false,
                uint max_iterations = 100,
                double xtol = 1e-10,
                double solver_max_step = 1.0);
};

#endif  // __R3BP_LYAPUNOV_ORBIT_HPP_
