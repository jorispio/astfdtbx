// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "LissajousEight.hpp"

#include <math.h>

// #define DEBUG_LISSAJOUS_EIGHT

// ------------------------------------------------------------------------
LissajousEight::LissajousEight(const LibrationPointProperty &librationPoint_, HaloOrbitFamily family)
    : HaloOrbit(librationPoint_, family) {
    setOrbitType(LISSAJOUS_EIGHT);
}
// ------------------------------------------------------------------------
/**
 * @return the analytical solution orbital period 
 */
double LissajousEight::get_analytical_period() {
    return 4 * M_PI / (omega * dtau_dt);  // note the period is double because of the vertical frequency (half the planar frequency)
}
// ------------------------------------------------------------------------
/**
 * fix the relationship between Ax and Az for a Halo orbit.
 * See: D.L. Richardson, Analytic Construction of Periodic Orbits About the Collinear Points, Celestial Mechanics 22, 1980, 241-253
 *
 * @param Az z-amplitude of the orbit
 * @param mu gravitational constant
 * @param lambda in-plane frequency
 * @param k
 * @param gamma
 * @param c2
 */
double LissajousEight::compute_x_amplitude(double Az, double lambda, double c2) {
    double lambda2 = lambda * lambda;
    double Delta = lambda2 - c2;

    if (Delta < 0) {
        double Az_min = sqrt(fabs(Delta / l2));
        if (Az < Az_min) {
            printf("Az is too low. No halo orbit can exist with this amplitude! Az = %f < Az_min=%f\n", Az, Az_min);
            throw R3bpOrbitException("Az is too low");
        }
    }

    double Ax2 =-(Delta + l2 * Az * Az) / l1;
    double Ax = sqrt(Ax2);

    double Ax_min = sqrt(fabs(Delta / l1));
    if (Ax < Ax_min) {
        // non-linearity effects are too small
        printf("Lissajous amplitude is too low! Ax = %f < Ax_min=%f\n", Ax, Ax_min);
        throw R3bpOrbitException("Orbit amplitude is too low");
    }

    return Ax;
}

// ------------------------------------------------------------------------
bool LissajousEight::interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) {
    // frequency correction
    double dtau_dt = (1 + s1 * Ax * Ax + s2 * Az * Az);  // 1 + sum(omg_n), and ds/dt = mm = 1 (owing to normalised GM)

    double m_phase = 2 - static_cast<double>(family);
    double dn = 2 - static_cast<double>(family);  // switch function

    // phasing
    double phi = 0.;
    double psi = phi + m_phase * M_PI / 2.;

    double tau1 = phi + omega * t;
    double cos_phi = cos(tau1);
    double sin_phi = sin(tau1);
    double cos_2phi = cos(2. * tau1);
    double sin_2phi = sin(2. * tau1);
    double cos_3phi = cos(3. * tau1);
    double sin_3phi = sin(3. * tau1);

    double nu = omega / 2.;
    double tau2 = psi + nu * t;
    double cos_psi = cos(tau2);
    double sin_psi = sin(tau2);
    double cos_2psi = cos(2. * tau2);
    double sin_2psi = sin(2. * tau2);
    double cos_3psi = cos(3. * tau2);
    double sin_3psi = sin(3. * tau2);

    // initial guess with constraint on the symmetry wrt to xz-plane
    x_guess[0] =-Ax * cos_phi                                                                           // first order
                        + (a21 * Ax * Ax + a22 * Az * Az) + (a23 * Ax * Ax - a24 * Az * Az) * cos_2phi  // second order
                        + Ax * (a31 * Ax * Ax - a32 * Az * Az) * cos_3phi;                              // third order
    x_guess[1] = k * Ax * sin_phi
                        + (b21 * Ax * Ax - b22 * Az * Az) * sin_2phi
                        + Ax * (b31 * Ax * Ax - b32 * Az * Az) * sin_3phi;
    x_guess[2] = dn * Az * (cos_psi
                        + d21 * Ax * (cos_2psi - 3.)
                        + (d32 * Ax * Ax - d31 * Az * Az) * cos_3psi);

    x_guess[3] = Ax * omega * sin_phi
                        - 2 * omega * (a23 * Ax * Ax - a24 * Az * Az) * sin_2phi
                        - 3 * omega * Ax * (a31 * Ax * Ax - a32 * Az * Az) * sin_3phi;
    x_guess[4] = k * Ax * omega * cos_phi
                        + 2 * omega * (b21 * Ax * Ax - b22 * Az * Az) * cos_2phi
                        + 3 * omega * Ax * (b31 * Ax * Ax - b32 * Az * Az) * cos_3phi;
    x_guess[5] =-dn * Az * nu * (sin_psi
                        + 2 * d21 * Ax * sin_2psi
                        + 3 * (d32 * Ax * Ax - d31 * Az * Az) * sin_3psi);

    // place around the libration point
    double gamma = librationPoint.gamma();
    for (int idx = 0;  idx < 3; ++idx) {
        x_guess[idx] *= gamma;
        x_guess[idx + 3] *= gamma * dtau_dt;
    }
    double g = librationPoint.gl();
    x_guess[0] += g;

    return false;
}
// ------------------------------------------------------------------------
/**
 * Orbit initial guess requires a third order approximation, using the
 * Lindstedt-Poincaré method, for the differential-corrector that we will
 * use to refine the initial point. This is because the dynamics are highly
 * sensitive, and the Halo orbit is actually unstable.
 * see Richardson.
 *
 * @param n_branch orbit class / phase constraint. n=1,3 for nothern or southern solutions respectively
 *
 */
bool LissajousEight::get_initial_conditions(double Az, double Ax, double omega, double gamma,
                                            /* outputs */
                                            double *x_guess, double &Tguess) {
    // estimation of the period
    Tguess = get_analytical_period();

    set_parameters(Ax, 0, Az, 0);
    double t = M_PI / omega;
    interpolate_initial_conditions(t, x_guess);

#ifdef DEBUG_LISSAJOUS_EIGHT
    printf("Az = %.8f\n", Az);
    printf("Ax = %.8f\n", Ax);
    printf("omega* = %.8f\n", omega * dtau_dt);
    printf("g = %.8f (L%d)\n", g, librationPoint.id + 1);
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n", x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
    printf("Tguess = %.8f \n", Tguess);

    printf("mu=%.8f omega=%.8f k=%.8f g=%.8f c2=%.8f\n", get_mu_ratio(), omega, k, g, c2);
    printf("Initial Amplitude: Ax= %.6f Ay= %.6f Az= %.6f\n", Ax, k * Ax, Az);
#endif
    return true;
}
// ------------------------------------------------------------------------
/**
 * The continuation parameter is the z-amplitude.
 */
OrbitSolution LissajousEight::find_by_continuation(double parameter,
        const OrbitSolution& initialGuess,
        /* options */
        ContinuationMethod method,
        bool print_iteration,
        uint max_iterations,
        double ftol,
        double xtol,
        double solver_max_step) {
        
    if (method != ContinuationMethod::AMPLITUDE_PARAMETER) {
        throw R3bpProblemException("Continuation scheme not applicable.");
    }
            
    // initial condition
    double x_guess[] = {
        initialGuess.r0(0),
        initialGuess.r0(1),
        parameter,  // initialGuess.r0(2),
        initialGuess.v0(0),
        initialGuess.v0(1),
        initialGuess.v0(2)};  // [pos, vel]
    double tguess = initialGuess.period ;

    VECTORVAR xInit = VECTORVAR::Zero(3);
    xInit << x_guess[0], x_guess[4], tguess / 2.;  // Rx, Vy, T

    TimeSymmetricOrbitProblemSolver *tsolver = new TimeSymmetricOrbitProblemSolver(librationPoint.mu,
                            StateFilter(V_RX, V_VY, V_TIME),
                            ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
    tsolver->set_initial_conditions(x_guess);

    VECTORVAR xs = xInit;
    int ret = tsolver->solve(xInit, xs, print_iteration, max_iterations, ftol, 10 * max_iterations, solver_max_step);
    orbitSolution.is_valid = ret >= 1;

    // post process and compile outputs from the solution
    MATRIXVAR jacobian = MATRIXVAR::Zero(3, 3);
    VECTORVAR zz = VECTORVAR::Zero(3);
    Matrix6 phi_stm;
    tsolver->zerof(xs, zz, jacobian, phi_stm);
    double half_period = xs(2);
    conclude(xs, 2 * half_period, phi_stm, jacobian, parameter, 0);

    delete tsolver;
    // return solve(xguess, tguess, xInit, parameter, print_iteration, max_iterations, xtol);

    continuation_history.push_back(OrbitContinuityCondition(parameter, orbitSolution));

    return orbitSolution;
}
// ------------------------------------------------------------------------
void LissajousEight::conclude(const VECTORVAR &xs, double period, const Matrix6 &phi, const MATRIXVAR& jacobian, double Az_, double Ax_) {
    HaloOrbit::conclude(xs, period, phi, jacobian, Az_, Ax_);
    orbitSolution.type = "lissajous-eight";
    orbitSolution.omega = omega;
    orbitSolution.nu = omega / 2.;
}
