// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_SPECIAL_LISSAJOUS_EIGHT_HPP_
#define __R3BP_SPECIAL_LISSAJOUS_EIGHT_HPP_

#include <string>
#include <vector>

#include "orbit/HaloOrbit.hpp"
#include "orbit/solvers/SymmetricOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

// ------------------------------------------------------------------------
// Doubly-Symmetric orbit "type C", or Lissajous-eight
//  P.G. Kazantzis, Numerical determination of families of three-dimensional double-symmetric periodic orbits
//                  in the restricted three-body problem
//  G. Archambeau, P. Augros, E. Trelat, Eight-shaped Lissajous orbits in the Earth-Moon system
// ------------------------------------------------------------------------
class LissajousEight : public HaloOrbit {
 private:
    void conclude(const VECTORVAR &xs, double period, const Matrix6& phi, const MATRIXVAR& jacobian, double Az, double Ax) override;

    double compute_x_amplitude(double Az, double lambda, double c2) override;

    bool get_initial_conditions(double Az, double Ax, double omega, double gamma,
            /* outputs */
            double *xguess, double &Tguess) override;  // c++11

 public:
    LissajousEight(const LibrationPointProperty &librationPoint, HaloOrbitFamily family = HaloOrbitFamily::NORTHERN);
    ~LissajousEight() { }

    double get_analytical_period() override;

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    OrbitSolution find_by_continuation(double Az,
        const OrbitSolution& initialGuess,
        /* options */
        ContinuationMethod method = AMPLITUDE_PARAMETER,
        bool print_iteration = false,
        uint max_iterations = 100,
        double ftol = 1e-6,
        double xtol = 1e-8,
        double solver_max_step = 0.1);// override;
};

// ------------------------------------------------------------------------
#endif  // __R3BP_SPECIAL_LISSAJOUS_EIGHT_HPP_
