// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "OrbitStability.hpp"


void OrbitStability::computeStabilityIndices(const Matrix6 &STM, double &mnu1, double&mnu2) {
   VectorXcd diagonal = STM.eigenvalues();

   // two of the eigen values are always 1, and we are not interested in them.
   // We focus on the remaining 4. The for are reciprocal pairs, such that
   // l1 = 1/l2 and l3 = 1/l4. 
    std::vector< std::pair<std::complex<double>,std::complex<double>> > pairs;
    int ip = 0;
    double tol = 1e-2;
    for (int idx = 0; idx < 5; ++idx) {
        std::complex<double> v1 = diagonal(idx);
        double a1 = v1.real();
        if (!std::isnan(a1)) {
            double b1 = v1.imag();          
            for (int jdx = idx + 1; jdx < 6; ++jdx) {
                std::complex<double> v2 = diagonal(jdx);
                if (!std::isnan(v2.real())) {
                    double a2 = v2.real();
                    double b2 = v2.imag();               
                    if (fabs(b1 *a2 - a1 * b2) < tol) {
                        pairs.push_back(std::pair<std::complex<double>,std::complex<double>>(v1, v2));
                        diagonal(idx) = NAN;
                        diagonal(jdx) = NAN;
                        ++ip;
                        break;
                   }
               }
           }
       }
    }
    
    if ((ip < 2)) {
        char str[1024];
        diagonal = STM.eigenvalues();
        snprintf(str, 1024, "Could not idendify pairs: {(%f,%f); (%f,%f); (%f,%f); (%f,%f); (%f,%f); (%f,%f)}   (pair found: %d)\n", 
            diagonal(0).real(), diagonal(0).imag(),
            diagonal(1).real(), diagonal(1).imag(),
            diagonal(2).real(), diagonal(2).imag(),
            diagonal(3).real(), diagonal(3).imag(),
            diagonal(4).real(), diagonal(4).imag(),
            diagonal(5).real(), diagonal(5).imag(), 
            ip);
        //printf(str);
        //pairs.push_back(std::pair<std::complex<double>,std::complex<double>>(NAN, NAN));
        throw R3bpProblemException(str);
   }
   
   VectorXcd D4 = diagonal;
   mnu1 = 0.5 * (std::get<0>(pairs.at(0)) + std::get<1>(pairs.at(0))).real();
   mnu2 = 0.5 * (std::get<0>(pairs.at(1)) + std::get<1>(pairs.at(1))).real();
}

