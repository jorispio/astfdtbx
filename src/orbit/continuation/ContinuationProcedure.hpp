// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef __R3BP_ORBIT_CONTINUATION_PROCEDURE_HPP__
#define __R3BP_ORBIT_CONTINUATION_PROCEDURE_HPP__


#include "orbit/continuation/ContinuationMethod.hpp"
#include "orbit/OrbitSolution.hpp"
#include "orbit/solvers/BaseOrbitFinder.hpp"
#include "orbit/solvers/StateFilter.hpp"

#include "R3bpProblemException.hpp"

class ContinuationProcedure
{
 private:
    BaseOrbitFinder *solver;
    
    ContinuationMethod method;
    double tol;
    int max_iter;
    bool verbose;
    
    /** continuation procedure parameters */
    double cp_start_value;
    double cp_end_value;    
    double cp_step; // startup continuation step > cp_min_step
    double cp_min_step;
    double reduction_factor;

    double last_accepted_step;
    double last_accepted_parameter_value;

    int continuation_iter;  // current continuation iteration < max_iter
    double continuation_step; // current continuation step > cp_min_step
    double continuation_parameter_value; // current continuation parameter value
    
    OrbitSolution last_orbit_solution;
    VECTORVAR measured_gradient;
    VECTORVAR x_init_condition;
    VECTORVAR old_dxds;
 
 public:
    ContinuationProcedure(ContinuationMethod method, double s_start, double s_stop, double s_max_step, double s_min_step=1e-6, double tol=1e-9, int max_iter=10000, double redfac=2.0, bool verbose=false) 
            : method(method), tol(tol), max_iter(max_iter), verbose(verbose),
            cp_start_value(s_start), cp_end_value(s_stop), cp_step (s_max_step), cp_min_step(s_min_step),
            reduction_factor(redfac) { 

        continuation_iter = 0;
        continuation_step = cp_step;
        continuation_parameter_value = cp_start_value;

        double direction = ((cp_end_value - cp_start_value) > 0) ? 1 : -1;
    
        if (direction * s_max_step <= 0) {
            throw R3bpProblemException("Continuation initialisation failed. step not consistent with s_start, s_stop.");
        }
    };

    ~ContinuationProcedure() { };

    /** Set solver used for finding solution. This call is mandatory. */
    void set_solver(BaseOrbitFinder *_solver) {
        solver = _solver;
    }
    
    void set(StateFilter filter, double s_start, double s_stop, double s_max_step) {
        cp_start_value = s_start;
        cp_end_value = s_stop;
        cp_step = s_max_step;
    }
    
    /** Set the continuation starting point. This call is mandatory. */
    void set_start_state(VECTORVAR &x_init_condition_) {
        x_init_condition = x_init_condition_;
        continuation_iter = 0;
        continuation_step = cp_step;
        continuation_parameter_value = cp_start_value;
    }
    
    /** Get continuation parameter value. */
    double get_parameter_value() {
        return continuation_parameter_value;
    }
    
    /** Indicates whether the continuation scheme is complete. */
    bool is_completed();
    
    /** Return next initial guess. */
    VECTORVAR next(const OrbitSolution& orbit_initial_guess);
};

#endif  // __R3BP_ORBIT_CONTINUATION_PROCEDURE_HPP__

