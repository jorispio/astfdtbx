// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#include "ContinuationProcedure.hpp" 


#define DEBUG_CONTINUATION
//#define DEBUG_HALO_ARC_LENGTH_CONTINUATION
// ------------------------------------------------------------------------
/**
 * 
 */
bool ContinuationProcedure::is_completed() {
    double direction = ((cp_end_value - cp_start_value) > 0) ? 1 : -1;
    return ((cp_end_value - continuation_parameter_value) * direction < tol) || (continuation_iter > max_iter);
}
// ------------------------------------------------------------------------
/**
 * 
 */
VECTORVAR ContinuationProcedure::next(const OrbitSolution& orbit_initial_guess) {
    
    if (is_completed()) {
        throw R3bpProblemException("Continuation complete. Use is_complete() in your continuation loop.");
    }

    if (solver == NULL) {
        throw R3bpProblemException("Orbit solver not set!");
    }

    // step control
    char flag = ' ';
    if (orbit_initial_guess.is_valid) {       
        if (last_orbit_solution.xsol.rows() == orbit_initial_guess.xsol.rows()) {
            measured_gradient = (orbit_initial_guess.xsol - last_orbit_solution.xsol);
        }
        
        last_orbit_solution = orbit_initial_guess;
        last_accepted_step = continuation_step;
        last_accepted_parameter_value = continuation_parameter_value;
        continuation_step = cp_step;
        continuation_parameter_value += continuation_step;        
    }
    else {
        flag = 'r';
        continuation_step /= reduction_factor;    
        continuation_parameter_value = last_accepted_parameter_value + continuation_step;
    }
    
    if (fabs(continuation_step) < cp_min_step) {
        throw R3bpProblemException("Continuation failed. min_step reached.");
    }
    

    VECTORVAR xInit;
    VECTORVAR x_init_condition = last_orbit_solution.get_initial_state();
    
    if ((method == ContinuationMethod::AMPLITUDE_PARAMETER) | (method == ContinuationMethod::POSITION) | (method == ContinuationMethod::VELOCITY)) {
        if (verbose) {
            printf("%5d %8.3g %c\n", continuation_iter, continuation_step, flag);
        }    

        xInit = VECTORVAR::Zero(3);
        xInit << x_init_condition[0], x_init_condition[4], last_orbit_solution.get_period() / 2.;  // Rx, Vy, T
        solver->setStateFilter(StateFilter(V_RX, V_VY, V_TIME));  // RZ == continuation parameter
        solver->setConstraintFilter(ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
        
        if(method == ContinuationMethod::POSITION) {
//        Az = parameter;
        //double amplitude = Az / gamma;  // normalize for linearization
        //compute_parameters(librationPoint.mu, gamma, amplitude, Ax, Az, omega);

        // initial condition        
//        update_initial_conditions(parameter, V_RZ, &(orbit_initial_guess.get_initial_state())[0], x_init_condition);
        }
        else if(method == ContinuationMethod::POSITION) {
            xInit[0] += cp_step;            
        }
        else if (method == ContinuationMethod::VELOCITY) {
            xInit[1] += cp_step;
        }
    }
    else {
        xInit = VECTORVAR::Zero(4);        
        xInit << x_init_condition[0], x_init_condition[2], x_init_condition[4], last_orbit_solution.get_period() / 2.;

        if (method == ContinuationMethod::PSEUDO_ARC_LENGTH) {
            solver->setStateFilter(StateFilter(V_RX, V_RZ, V_VY, V_TIME)); 

            if (last_orbit_solution.jacobian.cols() == 3) {
                solver->setConstraintFilter(ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
                
            }
            else if ((last_orbit_solution.jacobian.cols() == xInit.rows()) && (measured_gradient.rows()  == xInit.rows()))  {
                solver->setConstraintFilter(ConstraintDerivativeFilter(C_RY, C_VX, C_VZ, C_CUSTOM));
            
                MATRIXVAR jacobian = last_orbit_solution.jacobian.block(0, 0, 3, 4);
                FullPivLU<MATRIXVAR> lu_decomp(jacobian);
                MATRIXVAR kernel = lu_decomp.kernel();

                #ifdef DEBUG_HALO_ARC_LENGTH_CONTINUATION
                //std::cout << "jacobian: " << last_orbit_solution.jacobian << std::endl;
                std::cout << "kernel: " << kernel.normalized().transpose() << std::endl;
                std::cout << "jacobian*kernel: " << (jacobian*kernel).transpose() << std::endl;
                #endif

                if (kernel.norm() == 0) {
                    throw R3bpProblemException("Singularity detected. Cannot continue tracking...");
                }
                else if (kernel.cols() > 1) {
                    throw R3bpProblemException("Bifurquation detected. Need branching...");
                }

                
                VECTORVAR dxds = kernel.col(0);
                VECTORVAR dxdsn = dxds.normalized();
                //dxds = measured_gradient.normalized();
                
                // prevent U turn in track following
                if (old_dxds.rows() == dxdsn.rows()) {
                    dxdsn *= (dxdsn.dot(old_dxds) > 0) ? 1 : -1; // step should be sufficiently small
                }
                //dxds *= ((measured_gradient(0) > 0)? 1 : -1);
                #ifdef DEBUG_HALO_ARC_LENGTH_CONTINUATION
                std::cout << "dxds: " << measured_gradient.transpose() << std::endl;
                #endif
                
                // store gradient for later use
                if (orbit_initial_guess.is_valid) {
                    old_dxds = dxdsn;
                }
                
                VECTORVAR xs_prev = xInit;
                double parameter = continuation_step;
                auto f = [dxdsn, xs_prev, parameter](const VECTORVAR& x, const VECTORVAR& rvf) { 
                    return dxdsn.dot(x - xs_prev) / parameter - 1;  // arc length constraint
                }; 
                auto df = [dxdsn, xs_prev, parameter](const VECTORVAR& x, const VECTORVAR& rvf, const MATRIXVAR& m) {                 
                    VECTORVAR g = VECTORVAR::Zero(7);
                    g(Variable::V_RX) = dxdsn(0);  // / rx
                    g(Variable::V_RZ) = dxdsn(1);  // / rz
                    g(Variable::V_VY) = dxdsn(2);  // / vy
                    g(Variable::V_TIME) = dxdsn(3);  // / t
                    return g / parameter;
                }; 
                solver->set_custom_constraint(f, df);

                // prediction step
                xInit += parameter * dxdsn;

                if (verbose) {
                    printf("%5d %8.3g %8.6g %c\n", continuation_iter, continuation_step, dxds.norm(), flag);
                }
            }
        }
        else if (method == ContinuationMethod::JACOBI_CONSTANT) {
            // take orbit init guess initial conditions
//            memcpy(x_init_condition, &(last_orbit_solution.get_initial_state())[0], 6 * sizeof(double));
            // take orbit init guess unknown vector solution
            xInit = VECTORVAR::Zero(4);        
            xInit << x_init_condition[0], x_init_condition[2], x_init_condition[4], last_orbit_solution.get_period() / 2.;
        
            solver->setStateFilter(StateFilter(V_RX, V_RZ, V_VY, V_TIME)); 
            solver->setConstraintFilter(ConstraintDerivativeFilter(C_RY, C_VX, C_VZ));
            
            Vector6 gradient_s = jacobi::JacobiC_gradient(last_orbit_solution.mu, x_init_condition);
            Vector6 gradient_sn = gradient_s.normalized();

            #ifdef DEBUG_HALO_ARC_LENGTH_CONTINUATION            
            std::cout << "gradient_s: " << gradient_s.transpose() << std::endl;
            #endif
                        
            xInit(0) += gradient_sn(0) * continuation_step;
            xInit(1) += gradient_sn(2) * continuation_step;
            xInit(2) += gradient_sn(4) * continuation_step;  
            
            if (verbose) {
                printf("%5d %8.3g %8.6g  %c\n", continuation_iter, continuation_step, gradient_s.norm(), flag);
            }            
        }
        else {
            throw R3bpProblemException("Continuation scheme not applicable.");
        }    
    }
    
    ++continuation_iter;

    return xInit;
}


