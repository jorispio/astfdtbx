// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_CONTINUITY_CONDITION_HPP_
#define __R3BP_ORBIT_CONTINUITY_CONDITION_HPP_

#include "orbit/OrbitSolution.hpp"
#include "orbit/continuation/ContinuationMethod.hpp"

/**
 * This class stores an element of a continuation history.
 */
class OrbitContinuityCondition {
 private:
    double parameter;
    OrbitSolution solution;
    ContinuationMethod method;
 
 public:
    OrbitContinuityCondition(double parameter, OrbitSolution& solution, ContinuationMethod method = ContinuationMethod::AMPLITUDE_PARAMETER)
        : parameter(parameter), solution(solution), method(method) {
    }

    ~OrbitContinuityCondition() { };

    double get_parameter() {
        return parameter;
    }

    OrbitSolution get_orbit_property() {
        return solution;
    }
    
    double get_method() {
        return method;
    }    
};

#endif  // __R3BP_ORBIT_CONTINUITY_CONDITION_HPP_
