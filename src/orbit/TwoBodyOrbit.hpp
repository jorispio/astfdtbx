// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_TWOBODY_ORBIT_HPP_
#define __R3BP_TWOBODY_ORBIT_HPP_

#include "orbit/R3bpPrimaryPointOrbit.hpp"
#include "PrimaryPoint.hpp"


#include "frame/R3bpPrimaryBodyFrame.hpp"




#include "HelioLib.hpp"


class TwoBodyOrbit : public R3bpPrimaryPointOrbit {
private:
    double mu_ratio;
    PrimaryPointId primaryPointId;

    R3bProblem* problem;
    R3bpPrimaryBodyFrame* frame;
    R3bpFrame* r3bp_frame;

    double sma;
    double ecc;
    double inc;
    double aop;
    double raan;

    bool get_initial_conditions();
public:
    TwoBodyOrbit(double mu_ratio=1, PrimaryPointId primaryPointId = PrimaryPointId::P1);
    TwoBodyOrbit(double mu_ratio, const KeplerianOrbit* keplerianOrbit, PrimaryPointId primaryPointId_ = PrimaryPointId::P1);
    
    ~TwoBodyOrbit();

    bool set_orbit(double sma, double ecc, double inc, double aop, double raan);

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double* x_guess) override;

    bool get_position_velocity(double t, Vector3& pos, Vector3& vel) const;

    OrbitSolution get_orbit_solution();
};

#endif  // __R3BP_TWOBODY_ORBIT_HPP_
