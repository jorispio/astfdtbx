// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_STABILITY_HPP_
#define __R3BP_ORBIT_STABILITY_HPP_

#include "LibrationPoint.hpp"
#include "R3bpProblemException.hpp"

class OrbitStability {
 public:
    OrbitStability() { }
    ~OrbitStability() { }

    void computeStabilityIndices(const Matrix6 &STM, double &mnu1, double&mnu2) {
       VectorXcd diagonal = STM.eigenvalues();

       // two of the eigen values are always 1, and we are not interested in them.
       // We focus on the remaining 4. The for are reciprocal pairs, such that
       // l1 = 1/l2 and l3 = 1/l4. 
        std::vector< std::pair<std::complex<double>,std::complex<double>> > pairs;
        int ip = 0;
        double tol = 1e-2;
        for (int idx = 0; idx < 6; ++idx) {
            std::complex<double> v1 = diagonal(idx);
            double a1 = v1.real();
            double b1 = v1.imag();    
            double c1 = a1 * a1 - b1 * b1;
            a1 /= c1;
            b1 /= c1;        
            for (int jdx = idx + 1; jdx < 6; ++jdx) {
                std::complex<double> v2 = diagonal(jdx);
                if (!std::isnan(v2.real())) {
                    double a2 = v2.real();
                    double b2 = v2.imag();
                    if ((fabs((a1 - a2) / std::max(1., a1)) < tol) && (fabs((b1 + b2) / std::max(1., b1)) < tol)) {
                        pairs.push_back(std::pair<std::complex<double>,std::complex<double>>(v1, v2));
                        diagonal(jdx) = NAN;
                        ++ip;
                        break;
                   }
               }
           }
        }
        
        if ((ip < 2)) {
            char str[1024];
            snprintf(str, 1024, "Could not idendify pairs: {(%f,%f); (%f,%f); (%f,%f); (%f,%f); (%f,%f); (%f,%f)}   (pair found: %d)\n", 
                diagonal(0).real(), diagonal(0).imag(),
                diagonal(1).real(), diagonal(1).imag(),
                diagonal(2).real(), diagonal(2).imag(),
                diagonal(3).real(), diagonal(3).imag(),
                diagonal(4).real(), diagonal(4).imag(),
                diagonal(5).real(), diagonal(5).imag(), 
                ip);
            throw R3bpProblemException(str);
       }
       
       VectorXcd D4 = diagonal;
       mnu1 = 0.5 * (std::get<0>(pairs.at(0)) + std::get<1>(pairs.at(0))).real();
       mnu2 = 0.5 * (std::get<0>(pairs.at(1)) + std::get<1>(pairs.at(1))).real();

    #ifdef DEBUG
       printf("Stability indices:  %f  %f", mnu1, mnu2);
    #endif
    }

    /**
     * Compute stability index of the Halo orbit.
     * Stability <= 1  => stable
     * Stability  > 1  => unstable
     *
     * This is the simplification of computeStabilityIndices, for conjugate pair.
     *
     * @param monodromy 6x6 monodromy matrix
     * @param i  index of the stability indices, i={0,1}
     */
    double get_stability_index(const Matrix6 &monodromy) {
        VectorXcd eigenValues = monodromy.eigenvalues();
        double lambda_max = eigenValues.array().abs().maxCoeff();
        return 0.5 * (lambda_max + 1. / lambda_max);
    }
};
#endif  // __R3BP_ORBIT_STABILITY_HPP_
