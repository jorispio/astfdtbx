// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_HALO_ORBIT_HPP_
#define __R3BP_HALO_ORBIT_HPP_

#include <math.h>
#include <stdlib.h>

#include <string>
#include <vector>

#include "Jacobi.hpp"
#include "orbit/continuation/ContinuationProcedure.hpp" 
#include "orbit/OrbitApproximation.hpp"
#include "orbit/R3bpCollinearPointOrbit.hpp"
#include "solvers/SymmetricOrbitProblemSolver.hpp"
#include "SpaceMechanicsToolBox.hpp"

/** Halo orbit family. North/South refer to the SK maneuver necessary to maintain the unstable Halo orbit. */
enum HaloOrbitFamily {
    NORTHERN = 1,  // class I, clockwise
    SOUTHERN = 3   // class II, counter-clockwise
};


class HaloOrbit : public R3bpCollinearPointOrbit, public LinearSolution {
 private:
    virtual bool get_initial_conditions(double Az, double Ax, double omega, double gamma,
            /* outputs */
            double *xguess, double &Tguess);

    virtual double compute_x_amplitude(double Az, double lambda, double c2);
    virtual double compute_z_amplitude(double Az, double lambda, double c2);

    bool update_initial_conditions(double parameter, Variable fixed_parameter, const double *x_init,
                                            /* outputs */
                                            double *x_guess_out);

    OrbitSolution solve(const double *xguess, double Tguess,
            const VECTORVAR &xInit,
            double Az,
            /* options */
            bool print_iteration,
            uint max_iterations,
            double ftol,            
            double xtol,
            double solver_max_step);
    
 protected:
    HaloOrbitFamily family;

    TimeSymmetricOrbitProblemSolver *solver;

    void compute_parameters(double mu_ratio, double g, double amplitude, double &Ax, double &Az, double &omega);
    virtual void conclude(const VECTORVAR &xs, double period, const Matrix6& phi, const MATRIXVAR& jacobian, double Az, double Ax);

    double omega;
    double k;

    double dtau_dt;

 public:
    HaloOrbit(const LibrationPointProperty &librationPoint, const HaloOrbitFamily &family = HaloOrbitFamily::NORTHERN);
    virtual ~HaloOrbit();

    HaloOrbitFamily get_branch() const { return family; }

    void set_parameter(double Az, double phi = 0);

    /** Return the analytical approximation of the period */
    double get_analytical_period() override;

    bool get_initial_conditions(double Az,
            /* outputs */
            double *xguess, double &tguess);

    bool interpolate_initial_conditions(double t,
        /* outputs */
        double *x_guess) override;

    OrbitSolution find(double Az,
        /* options */
        bool print_iteration = false,
        uint max_iterations = 100,
        double ftol = 1e-6,
        double xtol = 1e-9,
        double solver_max_step = 0.1);

    virtual OrbitSolution find_by_continuation(
        const OrbitSolution& initialGuess,
        ContinuationProcedure& procedure,
        /* options */
        bool print_iteration = false,
        uint max_iterations = 100,
        double ftol = 1e-6,
        double xtol = 1e-9,
        double solver_max_step = 0.1);
};

// ------------------------------------------------------------------------
#endif  // __R3BP_HALOORBIT_HPP_
