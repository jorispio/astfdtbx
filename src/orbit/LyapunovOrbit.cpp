// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "LyapunovOrbit.hpp"

#include "propagator/detectors/AxisCrossingDetector.hpp"


// #define DEBUG_LYAPUNOV
// ------------------------------------------------------------------------
/**
 * Construct a distant retrograde orbit around thhe secondary body of the
 * restricted three-body problem.
 */
LyapunovOrbit::LyapunovOrbit(const LibrationPointProperty &librationPoint_, LyapunovOrbitFamilyType planarType)
    : R3bpCollinearPointOrbit(librationPoint_, LYAPUNOV), LinearSolution(librationPoint), planarType(planarType) {
    if (planarType == PLANAR) {
        stateFilter = StateFilter(V_VY, V_TIME);   // unknown parameters are vy and T
        constraintsFilter = ConstraintDerivativeFilter(C_RY, C_VX);  // constraint is ry = 0, vx = 0
    }
    else {  // VERTICAL
        stateFilter = StateFilter(V_RZ, V_VY, V_TIME);   // unknown parameters are vy, vz and T
        constraintsFilter = ConstraintDerivativeFilter(C_RY, C_VX, C_VZ);  // constraint is rz = 0, vx = 0
    }
    solver = new TimeSymmetricOrbitProblemSolver(get_mu_ratio(),
                            stateFilter,
                            constraintsFilter);
}

// ------------------------------------------------------------------------
LyapunovOrbit::~LyapunovOrbit() {
    if (solver != NULL) {
        delete solver;
    }
}
// ------------------------------------------------------------------------
void LyapunovOrbit::get_frequencies(const Vector6 &ep, double &omega, double &nu) {
    MATRIXVAR Df = MATRIXVAR::Zero(6, 6);
    CommonVariables cv;
    real_type X[7];
    X[0] = ep(0); X[1] = ep(1); X[2] = ep(2); X[3] = ep(3); X[4] = ep(4); X[5] = ep(5); X[6] = 1;

    EllipticThreeBodyProblem *dynamics = new EllipticThreeBodyProblem(get_mu_ratio(), ThrusterData());
    if (dynamics->ComputeCommonVariablesForStateOnly(0, X, cv)) {
        real_type x_dot[7];
        dynamics->GetJacobianDynamics(0, X, cv, x_dot, Df);
    }

    // in-plane frequency
    double b1 = 2. - 0.5 * (Df(3, 0) + Df(4, 1));
    double b2sq = -Df(3, 0) * Df(4, 1);
    omega = sqrt(b1 + sqrt(b1 * b1 + b2sq));

    // out-of-plane frequency
    nu = sqrt(fabs(Df(5, 2)));

    delete dynamics;
}
// ------------------------------------------------------------------------
/**
 * Set the orbit parameter value, and compute the intermediate orbit variables.
 * This setting is mandatory before performing any interpolation with interpolate_initial_conditions().
 * @see interpolate_initial_conditions
 */
void LyapunovOrbit::set_parameter(double parameter_az, double parameter_phi) {
    double gamma = librationPoint.gamma();
#ifdef DEBUG_HALO
    printf("gamma = %.8f\n", gamma);
#endif
    if (planarType == PLANAR) {
        Ax = parameter_az;
        Az = 0;
    } else {  // VERTICAL
        Ax = 0;
        Az = parameter_az;
    }
    phiz = parameter_phi;

    double c2_ = librationPoint.calc_c2();
    // linearized frequency
    double lambda2 = 0.5 * (-c2_ + 2 + sqrt(9. * c2_ * c2_ - 8 * c2_));
    double lambda = sqrt(lambda2);
    double k = (1. + 2. * c2_ + lambda2) / (2. * lambda);
    compute_third_order_approximation(librationPoint.mu, lambda, k, gamma, c2_);

    dtau_dt = (1 + s1 * Ax * Ax + s2 * Az * Az);  // 1 + sum(omg_n), and ds/dt = mm = 1 (owing to normalised GM)
}
// ------------------------------------------------------------------------
/**
 */
bool LyapunovOrbit::get_initial_conditions(double xpos0,
                                            /* outputs */
                                            double *x_guess, double &t_guess) {
    double omega, nu;
    get_frequencies(librationPoint.get_state(), omega, nu);
    double frequency = 0;

    double c2_ = librationPoint.calc_c2();
    double lambda2 = 0.5 * (-c2_ + 2 + sqrt(9. * c2_ * c2_ - 8 * c2_));
    double lambda = sqrt(lambda2);
    double gamma = librationPoint.gamma();

    omega = lambda;
    frequency = lambda;
    double k;
    if (planarType == PLANAR) {
        frequency = lambda; //omega;
    } else {
        frequency = nu;
    }
    k = (frequency * frequency + 1 + 2 * c2_) / (2. * frequency);
    compute_third_order_approximation(librationPoint.mu, lambda, k, gamma, c2_);

    x_guess[1] = 0;
    x_guess[2] = 0;
    x_guess[5] = 0;
    if (planarType == PLANAR) {
        Az = 0;
        Ax =-xpos0 / gamma;
        frequency = omega;
        double cos_phi = 1;
        double cos_2phi = 1;
        double cos_3phi = 1;

        lambda2 = 0.5 * (-c2_ + 2 + sqrt(9. * c2_ * c2_ - 8 * c2_));
        lambda = sqrt(lambda2);
        k = (1. + 2. * c2_ + lambda2) / (2. * lambda);

        x_guess[0] =-Ax * cos_phi
                    + a21 * Ax * Ax + (a23 * Ax * Ax) * cos_2phi
                    + (a31 * Ax * Ax * Ax) * cos_3phi;
        x_guess[3] = 0;
        x_guess[4] = k * Ax * omega * cos_phi
                    + (b21 * Ax * Ax) * 2 * omega * cos_2phi
                    + Ax * (b31 * Ax * Ax) * 3 * omega * cos_3phi;
    } else {  // VERTICAL
        Ax = 0;
        Az = xpos0 / gamma;
        frequency = omega / 2;
        double dr = 1;
        double phi = 0;
        double cos_phi = cos(phi);
        double cos_2phi = cos(2 * phi);
        double cos_3phi = cos(3 * phi);

        lambda2 = 0.5 * (-c2_ + 2 + sqrt(9. * c2_ * c2_ - 8 * c2_));
        lambda = sqrt(lambda2);
        k = (1. + 2. * c2_ + lambda2) / (2. * lambda);

        x_guess[0] = a22 * Az * Az - a24 * Az * Az * cos_2phi;
        x_guess[1] = 0;
        x_guess[2] = dr * Az * (cos_phi - d31 * Az * Az * cos_3phi);
        x_guess[3] = 0;
        x_guess[4] = -2 * b22 * Az * Az * omega * cos_2phi;  // initial guess vy
        x_guess[5] = 0;  // initial guess vz
    }
    double mass0 = 1;

    dtau_dt = get_frequency_correction(Ax, Az);
    for (int idx = 0;  idx < 3; ++idx) {
        x_guess[idx] *= gamma;
        x_guess[idx + 3] *= gamma * dtau_dt;
    }
    // place around the libration point
    x_guess[0] += librationPoint.gl();


    // Propagate with crossing event to guess the half period
    double t_test = 1.5 * M_PI / (frequency * dtau_dt);  // guess. It will be corrected afterwards
    TrajectoryPropageRtbp *state_propagator = new TrajectoryPropageRtbp(get_mu_ratio(), 0);
    SimulationEventManager *eventManager = new SimulationEventManager();
    AxisCrossingDetector *detector = new AxisCrossingDetector();
    detector->set_axis(Y_AXIS);
    eventManager->addEventHandler(detector);
    state_propagator->set_event_manager(eventManager);
    Vector7 x0; x0 << x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5], mass0;
    Vector7 xf;
    int iout = state_propagator->getFinalPoint(x0, 0, t_test, t_guess, xf);
    t_guess *= 2;
    if (iout != OdeSuccess::interrupted) {
        printf("Warning: Failed finding a plane-crossing initial guess!\n");
        t_guess = 2 * M_PI / (frequency * dtau_dt);
    }

    delete state_propagator;
    delete eventManager;
    delete detector;

    if (planarType == VERTICAL) {
        t_guess *= 2;
    }

#ifdef DEBUG_LYAPUNOV
    printf("iout = %d\n", iout);
    printf("xpos0 = %.8f\n", xpos0);
    printf("L = %f %f %f\n", librationPoint.L[0], librationPoint.L[1], librationPoint.L[2]);
    printf("omega* = %.8f  k = %.8f\n", omega, k);
    printf("xguess = %.8f %.8f %.8f %.8f %.8f %.8f\n", x_guess[0], x_guess[1], x_guess[2], x_guess[3], x_guess[4], x_guess[5]);
    printf("Tguess = %.8f  (T ~ %.8f) \n", t_guess, 2 * M_PI / frequency);
    std::cout << "xf = " << xf.transpose() << std::endl;
#endif

    return true;
}
// ------------------------------------------------------------------------
bool LyapunovOrbit::interpolate_initial_conditions(double t,
                                                    /* outputs */
                                                    double *x_guess) {
    double omega, nu;
    get_frequencies(librationPoint.get_state(), omega, nu);

    double psi = 0;
    double phi = 0;  // phasing arbitrarily set to 0 accordingly with initial conditions
    double dr = 1;
    double tau1 = phi + omega * t;
    double cos_phi = cos(tau1);
    double sin_phi = sin(tau1);
    double cos_2phi = cos(2. * tau1);
    double sin_2phi = sin(2. * tau1);
    double cos_3phi = cos(3. * tau1);
    double sin_3phi = sin(3. * tau1);
    double tau2 = psi + nu * t;
    double cos_psi = cos(tau2);
    double sin_psi = sin(tau2);
    double cos_2psi = cos(2. * tau2);
    double sin_2psi = sin(2. * tau2);
    double cos_3psi = cos(3. * tau2);
    double sin_3psi = sin(3. * tau2);

    double c2_ = librationPoint.calc_c2();
    double lambda2 = 0.5 * (-c2_ + 2 + sqrt(9. * c2_ * c2_ - 8 * c2_));
    double lambda = sqrt(lambda2);
    double k = (1. + 2. * c2_ + lambda2) / (2. * lambda);

    double frequency;
    if (planarType == PLANAR) {
        frequency = omega;
    } else {  // VERTICAL
        frequency = omega;
    }

    // initial guess with constraint on the symmetry wrt to xz-plane
    x_guess[0] =-Ax * cos_phi                                                                           // first order
                        + a21 * Ax * Ax + a22 * Az * Az + (a23 * Ax * Ax - a24 * Az * Az) * cos_2phi    // second order
                        + Ax * (a31 * Ax * Ax - a32 * Az * Az) * cos_3phi;                              // third order
    x_guess[1] = k * Ax * sin_phi
                        + (b21 * Ax * Ax - b22 * Az * Az) * sin_2phi
                        + Ax * (b31 * Ax * Ax - b32 * Az * Az) * sin_3phi;
    x_guess[2] = dr * Az * (cos_psi
                        + d21 * Ax * (cos_2psi - 3.)
                        + (d32 * Ax * Ax - d31 * Az * Az) * cos_3psi);

    //
    x_guess[3] = Ax * omega * sin_phi
                        - (a23 * Ax * Ax - a24 * Az * Az) * 2 * omega * sin_2phi
                        - Ax * (a31 * Ax * Ax - a32 * Az * Az) * 3 * omega * sin_3phi;
    x_guess[4] = k * Ax * omega * cos_phi
                        + (b21 * Ax * Ax - b22 * Az * Az) * 2 * omega * cos_2phi
                        + Ax * (b31 * Ax * Ax - b32 * Az * Az) * 3 * omega * cos_3phi;
    x_guess[5] =-dr * Az * nu * (sin_psi
                        + d21 * Ax * 2 * sin_2psi
                        + (d32 * Ax * Ax - d31 * Az * Az) * 3 * sin_3psi);


    // place around the libration point
    double gamma = librationPoint.gamma();
    for (int idx = 0;  idx < 3; ++idx) {
        x_guess[idx] *= gamma;
        x_guess[idx + 3] *= gamma * dtau_dt;
    }
    double g = librationPoint.gl();
    x_guess[0] += g;

    return false;
}
// ------------------------------------------------------------------------
double LyapunovOrbit::get_xpos(double Ax_) {
    return librationPoint.L[0] + Ax_;
}
// ------------------------------------------------------------------------
bool LyapunovOrbit::update_initial_conditions(double parameter, const double *x_init,
                                            /* outputs */
                                            double *x_guess_out) {
    // TODO(joris) use jacobian dx/dp to update properly initial conditions
    memcpy(x_guess_out, x_init, 6 * sizeof(double));
    if (planarType == PLANAR) {
        x_guess_out[0] = get_xpos(parameter);
    } else {
        x_guess_out[2] = parameter;
    }
    return true;
}
// ------------------------------------------------------------------------
/**
 * This function finds a Lissajous orbit for the RTBP.
 * Lissajous orbits, usually around L1,2,3, are the results of linearisation of
 * the dynamical equations around one of the Libration point.
 * They do not however exactly orbit the Libration points. They are the
 * general period solution of the R3BP
 *
 * inputs:
 *   mu       mass ratio
 *   Ax       x-excursion of the orbit
 *   Az       z-excursion of the orbit
 *
 * return OrbitSolution structure with:
 *   pos      initial position
 *   vel      initial velocity
 *   T        period of the orbit
 *   STM      transition matrix at T (monodromy matrix) to investigate
 *            first order stability of the orbit
 *
 */
OrbitSolution LyapunovOrbit::find(double xpos,
                                    /* options */
                                    bool print_iteration,
                                    uint max_iterations,
                                    double xtol,
                                    double solver_max_step) {
    double x_guess[6];  // [pos, vel]
    double t_guess;
    get_initial_conditions(xpos, x_guess, t_guess);

    solver->set_parameter(x_guess[0]);
    solver->set_initial_conditions(x_guess);

    VECTORVAR xInit = stateFilter.vector(x_guess, t_guess/2);
    VECTORVAR xs; xs = xInit;
    Matrix6 phi = solve(xInit, xs,
                        print_iteration, max_iterations, xtol, solver_max_step);

    conclude(xs, phi);

    return orbitSolution;
}

// ------------------------------------------------------------------------
/**
 * The continuation parameter is the x-position.
 */
OrbitSolution LyapunovOrbit::find_by_continuation(double parameter,
                                                    OrbitSolution& initialGuess,
                                                    /* options */
                                                    bool print_iteration,
                                                    uint max_iterations,
                                                    double xtol,
                                                    double solver_max_step) {
    double x_guess[6];
    update_initial_conditions(parameter, &(initialGuess.get_initial_state())[0], x_guess);

    solver->set_parameter(get_xpos(parameter));
    solver->set_initial_conditions(x_guess);

    VECTORVAR xInit = stateFilter.vector(x_guess, initialGuess.period /2);
    VECTORVAR xs; xs = xInit;
    Matrix6 phi = solve(xInit, xs,
                        print_iteration, max_iterations, xtol, solver_max_step);

    conclude(xs, phi);

    continuation_history.push_back(OrbitContinuityCondition(parameter, orbitSolution));

    return orbitSolution;
}
// ------------------------------------------------------------------------
/**
 */
Matrix6 LyapunovOrbit::solve(const VECTORVAR &xInit, VECTORVAR &xs,
                                bool print_iteration,
                                uint max_iterations,
                                double xtol, double solver_max_step) {
    int ret = solver->solve(xInit, xs, print_iteration, max_iterations, xtol, 10 * max_iterations, solver_max_step);
    orbitSolution.is_valid = ret >= 1;

    VECTORVAR zz = VECTORVAR::Zero(constraintsFilter.size());
    MATRIXVAR J = MATRIXVAR::Zero(constraintsFilter.size(), xs.rows());
    Matrix6 phi;
    solver->zerof(xs, zz, J, phi);

    return phi;
}

// ------------------------------------------------------------------------
void LyapunovOrbit::conclude(VECTORVAR &xs, const Matrix6 &phi) {
    // fill in a property structure for other codes
    orbitSolution.type = "lyapunov";

    orbitSolution.mu = get_mu_ratio();
    orbitSolution.period  = solver->get_duration(xs);
    orbitSolution.r0 = solver->get_initial_position(xs);  // initial position at t0
    orbitSolution.v0 = solver->get_initial_velocity(xs);  // initial velocity at t0

    // initial position
    // pos = [Lx; Ly; Lz] + dpos;
    // phi is indeed the stm at T/2. To get the monodromy matrix (t=T), we use
    // the property of the stm, and the symmetries of the problem.
    Vector6 diagonalElements; diagonalElements << 1, -1, 1, -1, 1, -1;
    Matrix6 A = diagonalElements.asDiagonal();
    Matrix6 STM = A * phi.inverse() * A * phi;
    orbitSolution.monodromy = STM;        // monodromy matrix

    orbitSolution.Ax = 0;
    orbitSolution.Ay = 0;
    orbitSolution.Az = 0;
    orbitSolution.phi_x = 0;
    orbitSolution.phi_y = 0;
    orbitSolution.phi_z = 0;
}
