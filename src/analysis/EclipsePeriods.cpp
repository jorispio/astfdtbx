// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "EclipsePeriods.hpp"

#include "propagator/R3bpPropagator.hpp"
#include "propagator/detectors/EclipseDetector.hpp"

// --------------------------------------------------------------------------- 
std::vector<EclipsePeriods> EclipsePeriodsPrediction::compute_eclipse_periods(OrbitSolution &orbit_solution);
    std::vector<EclipsePeriods> eclipses;
    
    // propagate orbit solution, with eclipse event detection    
    std::vector<double> trajectoryTime;
    std::vector<Vector7> trajectoryState;
    double tf;
    Vector3d rf, vf;
    double mf;
    Vector3 r0 = orbit_solution.r0;
    Vector3 r0 = orbit_solution.v0;
    
    Propagator *propagator = new Propagator(orbit_solution.mu, 0.);

    propagator->addEventDetector(new EclipseDetector());

    propagator->getTrajectory(r0, v0, 1, tspan[0], tspan[1], timeStep,
                                              trajectoryTime, trajectoryState,
                                              tf, rf, vf, mf);
    delete propagator;
    
    return eclipses;
}
