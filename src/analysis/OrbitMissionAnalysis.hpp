// $Id$
/* --------------------------------------------------------------------------- *
 *   This file is part of R3BPTOOLBOX.
 *
 *   Copyright (C) 2013 Joris Olympio
 *
 *   R3BPTOOLBOX is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Lesser General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   R3BPTOOLBOX is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Lesser General Public License for more details.
 *
 *   You should have received a copy of the GNU Lesser General Public License
 *   along with R3BPTOOLBOX.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef __R3BP_ORBIT_MISSION_ANALYSIS_HPP_
#define __R3BP_ORBIT_MISSION_ANALYSIS_HPP_

#include "EclipsePeriods.hpp"
#include "LibrationPoint.hpp"
#include "PrimaryPoint.hpp"
#include "SpaceMechanicsToolBox.hpp"


class OrbitMissionAnalysis {
private:
    Vector3d get_primary_point_position(PrimaryPointId primaryPointId, double mu) {
        Vector3d primaryPointPosition;
        if (primaryPointId == P1) {
            primaryPointPosition << -mu, 0, 0;
        }
        else {
            primaryPointPosition << 1 - mu, 0, 0;
        }
        return primaryPointPosition;
    }

 public:
    OrbitMissionAnalysis() { }
    ~OrbitMissionAnalysis() { }

    /**
     * Compute closest distance to the primary
     */
    double get_closest_distance(PrimaryPointId primaryPointId, double mu, std::vector<Vector7> &points) {
        Vector3d primaryPointPosition = get_primary_point_position(primaryPointId, mu);
        double rpmin = 1e20;        
        for(std::vector<Vector7>::iterator p = points.begin(); p != points.end(); ++p) { 
            rpmin = std::min(rpmin, ((*p).segment(0, 3) - primaryPointPosition).norm());
        }
        return rpmin;
    }
    
    /**
     * Compute angle from primary body
     */
    std::vector<double> compute_angle(PrimaryPointId primaryPointId, double mu, std::vector<Vector7> &points) {
        Vector3d primaryPointPosition = get_primary_point_position(primaryPointId, mu);
        std::vector<double> angles;        
        for(std::vector<Vector7>::iterator p = points.begin(); p != points.end(); ++p) { 
            double angle =  ((*p).segment(0, 3) - primaryPointPosition).norm() ;
            angles.push_back(angle);
        }
        return angles;
    }
    
    /**
     * Compute distance to primary body
     */
    std::vector<double> compute_distance(PrimaryPointId primaryPointId, double mu, std::vector<Vector7> &points) {
        Vector3d primaryPointPosition = get_primary_point_position(primaryPointId, mu);
        std::vector<double> distance;        
        for(std::vector<Vector7>::iterator p = points.begin(); p != points.end(); ++p) { 
            distance.push_back( ((*p).segment(0, 3) - primaryPointPosition).norm() );
        }
        return distance;
    }
    
    /**
     * Compute ground track (latitude, longitude)
     */
    std::vector<Vector3d> compute_ground_track(PrimaryPointId primaryPointId, double mu, std::vector<Vector7> &points/*BodyEllipsoidShape &shape*/) {
        Vector3d primaryPointPosition = get_primary_point_position(primaryPointId, mu);
        std::vector<Vector3d> groundTrack;
        for(std::vector<Vector7>::iterator p = points.begin(); p != points.end(); ++p) { 
            Vector3d point = (*p).segment(0, 3) - primaryPointPosition;
            GeodeticCoordinates *geoCoords = new GeodeticCoordinates(point);
            
            Vector3d llh;
            llh << geoCoords->getLatitude(), geoCoords->getLongitude(), geoCoords->getAltitude();
            groundTrack.push_back(llh);
        }
        return groundTrack;
    }

    /**
     * Compute eclipses
     * @param primaryPointId primary object ID
     * @param occultingRadius object occulting radius, m
     * @param points current points in the relative frame
     */
    std::vector<EclipsePeriod> compute_eclipse_periods(PrimaryPointId primaryPointId, double occultingRadius, double mu,
                                                        std::vector<double>& time,
                                                        std::vector<Vector3>& sunPos,
                                                        std::vector<Vector7>& points) {
        assert(sunPos.size() == time.size());
        assert(points.size() == time.size());

        std::vector<EclipsePeriod> periods;
        
        // evaluate the shadowing condition for every points
        // sort out and extract eclipse periods
        Vector3 rplanet = get_primary_point_position(primaryPointId, mu);

        EclipseCondition condition0 = EclipseCylindrical::getEclipseCondition(sunPos.at(0), rplanet, points.at(0).head(3), occultingRadius);
        bool isInEclipse = condition0 != EclipseCondition::NotInEclipse;
        EclipsePeriod period;
        for (int i = 1; i < points.size(); ++i) {
            Vector3 rposition = points.at(i).head(3);
            EclipseCondition condition = EclipseCylindrical::getEclipseCondition(sunPos.at(i), rplanet, rposition, occultingRadius);
            if ((!isInEclipse) && (condition0 != EclipseCondition::NotInEclipse)) {
                isInEclipse = true;
                period.start = time.at(i);
            }
            else if ((isInEclipse) && (condition0 != EclipseCondition::NotInEclipse)) {
                isInEclipse = false;
                period.end = time.at(i);
                periods.push_back(period);
            }
        }
        if (isInEclipse) {
            period.end = time.at(time.size() - 1);
            periods.push_back(period);
        }
        return periods;
    }
};
#endif  // __R3BP_ORBIT_MISSION_ANALYSIS_HPP_
